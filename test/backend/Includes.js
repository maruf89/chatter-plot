'use strict';

process.env.NODE_ENV = 'test';

var path              = require('path');
var libDir            = path.resolve(process.cwd(), 'lib');
var Fetch             = require(path.join(libDir, 'Fetcher'));

var dummyData = {
    users: [{
        firstName: "joe",
        lastName: "dirt",
        userID: 0,
        email: "joe_dirt@test.com"
    }, {
        firstName: "mary",
        lastName: "ann",
        userID: -1,
        email: "mary_dirt@dmx.test.com"
    }],
    userSearchResponse: {
        "took" : 66,
        "timed_out" : false,
        "_shards" : {
            "total" : 1,
            "successful" : 1,
            "failed" : 0
        },
        "hits" : {
            "total" : 1,
            "max_score" : 5.108718,
            "hits" : [{
                "_index" : "users",
                "_type" : "user",
                "_id" : "0",
                "_score" : 5.108718,
                "_source": {"name":"English + bob"}
            }]
        }
    }
};

module.exports = {
    Fetch: Fetch,
    dummyData: dummyData,
    elasticsearch: Fetch.database('elasticsearch').client,
};