'use strict';

import test from 'blue-tape';

import Q from 'q';
import _ from 'lodash-node/modern';

import Includes from '../includes';

const before = test;
const after = test;

const Fetch = Includes.Fetch;
const dummyData = Includes.dummyData;
const esClient = Includes.elasticsearch;
const db = Fetch.config('misc/constants').elasticsearch.USER;

const userController = Fetch.controller('user');

/*  DUMMY DATA SETUP    */
const user1 = dummyData.users[0];
const user2 = dummyData.users[1];
const deleteUsers = function () {
    return Q.all([
        esClient.delete({
            index: db.INDEX,
            type: db.TYPE,
            id: String(user1.userID),
        }),
        esClient.delete({
            index: db.INDEX,
            type: db.TYPE,
            id: String(user2.userID),
        })
    ]);
};

const indexUsers1 = function () {
    return esClient.bulk({
        refresh: true,
        body: [
            {index: {_index: db.INDEX, _type: db.TYPE, _id: user1.eID}},
            user1,
            {index: {_index: db.INDEX, _type: db.TYPE, _id: user2.eID}},
            user2,
        ]
    })
};

test("bControllerUser", function (t) {
    t.test('get()', function (st) {
        var beforeAddUsers = function () {
                this.test('before', function (assert) {
                    deleteUsers().finally(indexUsers1).finally(assert.end.bind(assert));
                });
            },
            afterAddUsers = function () {
                this.test('after', function (assert) {
                    deleteUsers().finally(assert.end.bind(assert));
                });
            };

        beforeAddUsers.call(st);
        st.test('Should return a single insecure user as an array { multi: true }', function (assert) {
            var data = {
                userID: user1.userID,
                multi: true
            };

            var promise = userController.get(data, false);

            return promise.then(function (person) {
                assert.deepEqual(person, [{
                    firstName: user1.firstName,
                    lastName: user1.lastName,
                    userID: user1.userID
                }], 'Users should match');

                return true;
            });
        });
        afterAddUsers.call(st);

        beforeAddUsers.call(st);
        st.test('Should return a single secure user as an array { multi: false }', function (assert) {
            var data = {
                userID: user1.userID,
                multi: undefined,
                userPermissions: 93
            };

            var promise = userController.get(data, true, data.userID);

            return promise.then(function (person) {
                assert.deepEqual(person, {
                    firstName: user1.firstName,
                    lastName: user1.lastName,
                    email: user1.email,
                    userID: user1.userID,
                    permissions: data.userPermissions
                }, 'Users should match');

                return true;
            });
        });
        afterAddUsers.call(st);

        beforeAddUsers.call(st);
        st.test('Should return an array of 2 users', function (assert) {
            var data = {
                userIDs: [user1.userID, user2.userID]
            };

            var promise = userController.get(data);

            return promise.then(function (people) {
                assert.deepEqual(people, [{
                    firstName: user1.firstName,
                    lastName: user1.lastName,
                    userID: user1.userID
                }, {
                    firstName: user2.firstName,
                    lastName: user2.lastName,
                    userID: user2.userID
                }], 'Should return an array of 2 deep linked users');

                return true;
            });
        });
        afterAddUsers.call(st);
    });

    t.test('formatSearchResults()', function (st) {
        const data = dummyData.userSearchResponse;

        // The source response body of our dummy data
        const source = data.hits.hits[0]._source;

        st.test('Should return a formatted search results object without markers', function (assert) {
            const result = userController.formatSearchResults(data);
            const expected = {
                users: [source],
                total: 1
            };

            assert.deepEqual(result, expected, 'Should return an object with just the users array');
            assert.end();
        });

        st.test('Should return a formatted search results object with markers', function (assert) {
            const parameters = {
                responses: [data, data]
            };
            const result = userController.formatSearchResults(parameters);
            const expected = {
                users: [source],
                markers: data.hits.hits,
                total: 1
            };

            assert.deepEqual(result, expected, 'Should return an object with both the users & markers array');
            assert.end();
        });
    });
});