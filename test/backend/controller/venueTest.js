'use strict';

import test from 'blue-tape';

import Q from 'q';
import _ from 'lodash-node/modern';

import Includes from '../includes';
import {venues} from './dummy/venue';

const Fetch = Includes.Fetch;
const dummyData = Includes.dummyData;
const esClient = Includes.elasticsearch;
const db = Fetch.config('misc/constants').elasticsearch.VENUE;

const venueController = Fetch.controller('venue');

/*  DUMMY DATA SETUP    */
const venue1 = venues[0];
const venue2 = venues[1];
const venue3 = venues[2];
const deleteFn = function () {
    return Q.all([
        esClient.delete({
            index: db.INDEX,
            type: db.TYPE,
            id: String(venue1.vID),
        }),
        esClient.delete({
            index: db.INDEX,
            type: db.TYPE,
            id: String(venue2.vID),
        }),
        esClient.delete({
            index: db.INDEX,
            type: db.TYPE,
            id: String(venue3.vID),
        })
    ]);
};

const createFn = function () {
    return esClient.bulk({
        refresh: true,
        body: [
            {index: {_index: db.INDEX, _type: db.TYPE, _id: venue1.vID}},
            venue1,
            {index: {_index: db.INDEX, _type: db.TYPE, _id: venue2.vID}},
            venue2,
            {index: {_index: db.INDEX, _type: db.TYPE, _id: venue3.vID}},
            venue3,
        ]
    })
};

test("bControllerVenue", function (t) {
    t.test('searchForExisting()', function (st) {
        var before = function () {
                this.test('before', function (assert) {
                    deleteFn().finally(createFn).finally(assert.end.bind(assert));
                });
            },
            after = function () {
                this.test('after', function (assert) {
                    deleteFn().finally(assert.end.bind(assert));
                });
            };

        before.call(st);
        st.test('Should return all 3 matching venues from the database', function (assert) {
            var data = [{
                google: venue1.google
            }, {
                google: venue2.google
            }, {
                yelp: venue3.yelp
            }];

            var promise = venueController.searchForExisting(data);

            return promise.then(function (response) {
                var venues = _.pluck(response.hits.hits, '_source');
                assert.deepEqual(venues, [venue1, venue2, venue3], 'Should return all venues that have matching ids');
                return true;
            });
        });
        after.call(st);

    });
});