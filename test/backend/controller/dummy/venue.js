'use strict';

const venues = [{
    vID: -1010,
    coords: {
        lat: 34,
        lon: 34
    },
    type: "business",
    google: "aoeu123nth987",
    name: "Bobs testing"
}, {
    vID: -1991,
    coords: {
        lat: 34.004392,
        lon: 34.004392
    },
    type: "home",
    google: "aoeu123nth986"
}, {
    vID: -9918,
    coords: {
        lat: 33.904392,
        lon: 33.104301
    },
    type: "business",
    name: "yelp name",
    yelp: "aoeu123nth986"
}];

export {venues};