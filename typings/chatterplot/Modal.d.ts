/// <reference path="./Chatterplot.d.ts" />


declare module chatterplot.modal {
    interface IModal {
        activate:(opts?:any) => ng.IPromise<any>
        deactivate:(noReject?:boolean) => void
    }

    interface IService extends IModal {

    }
}

declare module chatterplot.modal.flow {
    interface IRequestService extends IModal {
        activate:(opts:any) => ng.IPromise<string>
        edit:(reqID:string) => ng.IPromise<string>
        deny:(datum:{ reqID:string; status:string }) => ng.IPromise<any>
        deactivate:(noReject:boolean) => void
    }

    interface IRequestScope extends cp.IScope {
        shared:any
    }

    interface IFullRequestObj {
        request:cp.interaction.IRequest
        otherID:number
        users:cp.user.IUserCache
    }

    interface IServiceScope extends cp.IScope {
        modal:any
    }

    interface IEventService extends IModal {
        view:(eID:number, subType:string) => ng.IPromise<any>
        duplicate:(eID:number, subType:string) => ng.IPromise<any>
    }
}
