/// <reference path="./Chatterplot.d.ts" />

declare module chatterplot.notification {
    interface IUser {
        firstName:string
        lastName?:string
    }

    interface IUserMap {
        [userID:number]:IUser
    }

    interface INotificationsResponse {
        notifications:INotification[]
        total?:number
        users?:IUserMap
    }

    interface INotification {
        nID:string
        source:number
        sourceType:number
        sourceID:string
        when:string
        from?:number
        read?:number
        snippet?:string
        translate?:string
        icon?:string
        link?:string
    }

    interface INotificationMap {
        [nID:string]:INotification
    }

    interface ICache {
        all:INotificationMap
        unread:string[]
        sorted:INotification[]
        total:number
        loaded:number
        allLoaded:() => boolean
    }

    interface IFetchOpts {
        from?:number
        size?:number
        withUsers?:boolean
    }

    interface ISourceTypeMap {
        [sourceType:string]: ISourceType
    }

    interface ISourceType {
        translate?:(User:cp.user.IService, notification:INotification) => { [key:string]:string }
        link?:($state:ng.ui.IStateService, notification:INotification) => string
    }

    interface ITypeMap {
        [source:string]: IType
    }

    interface IType {
        source:number
        icon:string
        link?:($state:ng.ui.IStateService, notification:INotification) => string
    }

    /**
     * Notifications Service
     */
    interface IService {
        isReady:() => boolean
        fetch:(opts:cp.notification.IFetchOpts, force?:boolean) => ng.IPromise<ICache>
        markAsRead:(nIDs:string[], force?:boolean) => ng.IPromise<boolean>
        nID2ObjArr:(nIDs:string[]) => cp.notification.INotification[]
        notificationsViewed:() => ng.IPromise<cp.IServerResponse>
    }

    /**
     * These are the settings passed from the backend as CP.Settings.NOTIFICATION
     */
    interface ISettingsNOTIFICATION {
        source:ISettingsNOTIFICATIONSource
        sourceTypes:ISettingsNOTIFICATIONSourceTypes
    }

    interface ISettingsNOTIFICATIONSource {
        [source:number]:string
    }

    interface ISettingsNOTIFICATIONSourceTypes {
        [group:number]:ISettingsNOTIFICATIONSourceType
    }

    interface ISettingsNOTIFICATIONSourceType {
        [sourceType:number]:string
    }

    /**
     * And this is the reverse
     */
    interface ISettingsMAP {
        source:ISettingsMAPSource
        sourceTypes:ISettingsMAPSourceTypes
    }

    interface ISettingsMAPSource {
        [sourceKey:string]:number
    }

    interface ISettingsMAPSourceTypes {
        [sourceKey:string]:ISettingsMAPSourceType
    }

    interface ISettingsMAPSourceType {
        [sourceType:string]:number
    }
}