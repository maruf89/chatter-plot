/// <reference path="./Chatterplot.d.ts" />

declare module chatterplot.interaction {
    interface IConversationCache {
        [msgID:string]: IConversation
    }

    interface IConversation {
        msgID:string
        participants:number[]
        excluded?:number[]
        updated:Date
        snippet?:string
        read:number[]
    }

    interface ILog {
        timestamp:Date
        message:string
        from:number
    }

    interface IRequest {
        type:string
        creator:number
        participants:number[]
        status:string
        expired:boolean
        reqID?:string
        oldReqID?:string
        venue?:ILocation
        when?:Date
        until?:Date
        created?:Date
    }

    /**
     * @description a request object that gets sent to the server
     */
    interface IRequestBuild extends IRequest {
        type:string
        creator:number
        participants:number[]
        status:string
        message?:string
        oldRequest?:IRequest
    }

    interface IFormattedRequest extends IRequest, cp.activities.IActivityItem {
        date:string
        time:string
        endDate:string
        endTime:string
        formatted:boolean
        isMine:boolean
        expired:boolean
        otherUser?:cp.user.IUser
        venueDetails?:cp.venue.IVenueFull
    }

    interface IServerResponse {
        requests?:IRequest[]
        results?:IRequest[]
        users?:cp.user.IUser
        conversations?:IConversation[]
        venues?:cp.venue.IVenue[]
    }

    interface IRequestCache {
        [reqID:string]: IFormattedRequest
    }

    interface ILocation {
        vID?:number
        google?:string
        yelp?:string
        coords:cp.map.ICoordinates
    }

    interface ISearchOptions {
        type:string
        withVenues?:boolean
        withUsers?:boolean
        opts:any
        filterTypes?:string[]
    }

    interface IServiceHelper {
        fetchRecent:(latest:boolean, size?:number, from?:number) => ng.IPromise<IServerResponse>
        formatNewRequest:(serverRequest:cp.interaction.IFormattedRequest) => cp.interaction.IFormattedRequest
        syncRequests:() => ng.IPromise<IFormattedRequest[]>
    }

    interface IServiceMap extends cp.map.IServiceMarker {

    }

    interface IService {
        map:any
        people:cp.user.IUserCache
        conversations:IConversationCache
        convosLen:number
        helper:IServiceHelper
        requests:IRequestCache
        defunctRequests:cp.interaction.IRequestCache
        clearCache:(which:string) => void
        cacheRequest:(request:IFormattedRequest) => boolean
        getType:<T>(opts:any, serverForce?:boolean) => ng.IPromise<T>
        fetch:(opts?:any, force?:boolean) => ng.IPromise<IConversationCache>
        fetchType:(opts?:{ type:string }) => ng.IPromise<IServerResponse>
        formatRequest:(request:IRequest) => IFormattedRequest
        sendRequest:(request:IRequestBuild) => ng.IPromise<string>
        respondRequest:(datum:{ reqID:string; status:string; message?:string }) => ng.IPromise<boolean>
    }

    interface IServiceHelper {
        requestWith:(request:IFormattedRequest, user:cp.user.IUser, noLink?:boolean) => string
    }
}