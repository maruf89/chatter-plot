/// <reference path="./Chatterplot.d.ts" />

declare module chatterplot.event {
    interface IEventBuild {
        name:string
        locationDisplayName?:string
        description:string
        aboutHost?:string
        paymentInfo?:string
        priceString?:string
        venue:IVenue
    }

    interface IEventBase extends IEventBuild {
        eID:number
        languages:number[]
        when:Date
        until?:Date
        created:Date
        lastUpdate?:Date
        organizers?:number[]
        levels:number[]
        hasPhoto?:boolean
        venueDetails?:cp.venue.IVenueFull
        published:boolean
        goingList:number[]
        stillIndexing?:boolean
        indexType:string
    }

    interface IVenue {
        vID:number
        coords:cp.map.ICoordinates
        rsvpOnlyRadius?:number
    }

    interface IFormattedVenue extends IVenue {
        hidden?:boolean
    }

    interface IEvent extends IEventBase {
        creator:number
        waitList?:number[]
        maxGoing?:number
        aboutHost:string
    }

    interface IListing extends IEventBase {
        registrationURL?:string
    }

    interface IEventFormatted extends IEventBase, cp.activities.IActivityItem {
        formatted?:boolean
        vars:any
        formattedAddress:string
        date:string
        time:string
        endDate?:string
        endTime?:string
        uiSref:string
        href:string
        canonicalURL:string
        stillIndexing?:boolean
        goingCount:number
        displayLocation:string
        gmapLink:string
        imAttending:boolean
        imWaiting:boolean
        whenISO:string
        expired:boolean
        untilISO?:string
        capGoing?:boolean
        waitList?:number[]
        host?:cp.user.IUser
        aboutHost?:string
        maxGoing?:number
        creator?:number
        venue:IFormattedVenue
        __V?:IFormattedVenue
        imHosting:boolean
    }

    interface IEVType {
        eID:number
        type:string
    }

    interface IGetParameters {
        evTypes: IEVType[]
        withVenues?:boolean
        _source?:string[]
    }

    interface IGetHomogeneous {
        eIDs:number
        type:string
        withVenues?:boolean
        _source?:string[]
    }

    interface IEventListingCache {
        [id:number]:IEventBase
    }

    interface IEventCache {
        [id:number]:IEvent
    }

    interface IServerResponse {
        events:IEventBase[]
        total?:number
    }

    interface IService {
        helper:IServiceHelper
        map:any
        modify:IModifyService
        cache:IEventListingCache
        eventCache:IEventCache
        create:(eventObj:cp.event.IEventBase, place?:cp.venue.IVenueFull) => ng.IPromise<cp.event.IEventBase>
        fetch:(opts:any) => ng.IPromise<IEvent[]>
        update:(data:any) => ng.IPromise<boolean>
        get:(data:cp.event.IGetParameters | cp.event.IGetHomogeneous) => ng.IPromise<IEventBase>
        format:(event:cp.event.IEventFormatted, force?:boolean) => cp.event.IEventFormatted
        updateAttendance:(event:cp.event.IEventFormatted, attending:boolean, asWaitList?:boolean) => cp.IServerResponse
        publish:(event:cp.event.IEventFormatted, type?:string) => ng.IPromise<cp.IServerResponse>
        "delete":(event:cp.event.IEventFormatted) => ng.IPromise<cp.IServerResponse>
        checkIndexedEvent:(eID:number) => boolean | cp.event.IEventBase
        deleteIndexedEvent:(eID:number, force:boolean) => void
        amIAttending:(eID:number) => boolean
    }

    interface IServiceHelper {
        fetchRecent:(latest:boolean, size?:number, from?:number) => ng.IPromise<IServerResponse>
    }

    interface IModifyService {
        defaultSections:string[]
        initVars:() => IModifyVariables
        parseEdit:(Event:cp.event.IEvent, sections?:string[]) => ng.IPromise<IModifyVariables>
        formatEdit:(Event:IEvent, sections:string[], vars:IModifyVariableVars) => any
    }

    interface IModifyVariables {
        vars:IModifyVariableVars
        newEvent:IEventBuild
    }

    interface IModifyVariableVars {
        eventAsListing:boolean
        regRequired:boolean
        event:IModifyVariableVarsDate
        rsvpOnlyRadius:boolean
        showDateEnd:boolean
        location:cp.venue.IVenueFull
        locationClone:cp.venue.IVenueFull
        venueTz:number
        updateLocation:boolean
        languages:cp.user.ILanguage
        allLanguages:boolean
        skill:IModifyVariableVarsSkill
        allSkills:boolean
        priceFree:boolean
        maxGoing:boolean
    }

    interface IModifyVariableVarsDate {
        minDate:number
        minTime:string
        date:Date
        time:string|Date
        endDate:Date
        endTime:string|Date
    }

    interface IModifyVariableVarsSkill {
        from:IModifyVariableVarsSkillLevel
        to:IModifyVariableVarsSkillLevel
    }

    interface IModifyVariableVarsSkillLevel {
        level:number
    }
}