/// <reference path="./Chatterplot.d.ts" />
/// <reference path="../angularjs/angular.d.ts" />

declare module chatterplot.map {
    interface IConditionalLocation {
        coords?: ICoordinates
    }

    interface ILocation {
        coords: ICoordinates
    }

    interface ICoordinates {
        lat:number
        lon:number
    }

    interface ICoordinatesLong {
        latitude:number
        longitude:number
    }

    interface IZoomCoordinates extends ICoordinates {
        zoom?:number
    }

    interface IPreMarker {
        lat:number
        lon:number
        title:string
        fit:boolean
        icon:string
        id:string
    }

    interface IMarkerSet {
        name:string
        events:any
        markers:any[]
        circle:google.maps.CircleOptions
    }

    interface IMarker {
        id:string
        coords:ICoordinatesLong
        data:any
        icon:string
    }

    interface IMapCtrl {
        currentSet:IMarkerSet
        center:() => void
        loaded:() => ng.IPromise<cp.map.IMapCtrl>
        createMarker:(marker:IMarker, identifier?:string, replace?:boolean) => IMapCtrl
        clearMarkers:(identifier?:string) => void
        pushMarkers:(markers:IMarker[], identifier?:string) => IMapCtrl
        centerMap:(coords:ILocation, zoom?:number, init?:boolean) => cp.map.IMapCtrl
        selectSet:(identifier?:string, opts?:any, forceOpts?:boolean) => void
        getSet:(identifier:string) => IMarkerSet
        deselectSet:(identifier?:string) => void
        updateScope:() => void
    }

    interface IService {
        circle:any
        defaults:any
        locationInaccurate:boolean
        getInstance:() => IMapCtrl
        normalizeCoords:(coords:any, elastic:boolean, c?:any) => {lat?:number;lon?:number;latitude?:number;longitude?:number;zoom?:number}
    }

    interface IServiceMarker {
        formatWindow:(event:any, options:any) => ng.IPromise<any>
    }
}