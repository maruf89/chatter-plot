// Type definitions for Chatterplot
// Project: https://www.chatterplot.com
// Definitions by: Marius Miliunas

/// <reference path="../jquery/jquery.d.ts" />
/// <reference path="../angularjs/angular.d.ts" />
/// <reference path="../angular-ui-router/angular-ui-router.d.ts" />
/// <reference path="../browserify/browserify.d.ts" />
/// <reference path="../lodash/lodash.d.ts" />
/// <reference path="../modernizr/modernizr.d.ts" />
/// <reference path="../ckeditor/ckeditor.d.ts" />
/// <reference path="../googlemaps/google.maps.d.ts" />
/// <reference path="../angulartics/angulartics.d.ts" />
/// <reference path="../moment/moment-node.d.ts" />

/// <reference path="./Misc.d.ts" />

/// <reference path="./Map.d.ts" />
/// <reference path="./Activities.d.ts" />
/// <reference path="./User.d.ts" />
/// <reference path="./Venue.d.ts" />
/// <reference path="./Notification.d.ts" />
/// <reference path="./Interaction.d.ts" />
/// <reference path="./Event.d.ts" />
/// <reference path="./Modal.d.ts" />
/// <reference path="./Action.d.ts" />
/// <reference path="./Services.d.ts" />

declare var CP:cp.IGlobalCP;
declare var p:(num:(string|number)) => number;
declare var InfoBox:any;

// Collapse angular into ng
import cp = chatterplot;

// Support AMD require
declare module 'chatterplot' {
    export = chatterplot;
}

///////////////////////////////////////////////////////////////////////////////
// Chatterplot
///////////////////////////////////////////////////////////////////////////////
declare module chatterplot {
    interface IGlobalCP {
        Global:IGlobalCPGlobal
        Cache:any
        Settings:any
        ready:boolean
        locale:string
        site:IGlobalCPSite
        language:string
        production:boolean
        deployVersion:number
        releases:string[]
    }

    interface IGlobalCPGlobal {
        regex:any
        vars:any
    }

    interface IGlobalCPSite {
        name:string
        host:string
        prefix:string
        subdomains:string[]
        views:string
        routes:any
    }

    interface IScope extends ng.IScope {
        $root:IRootScopeService
        vars?:any
    }

    interface IControllerScope extends IScope {
        data?:any
    }

    interface IDirectiveScope extends IScope {
        vars?:any
    }

    interface IAugmentedJQuery extends ng.IAugmentedJQuery {
        scope(): IScope;
        isolateScope(): IScope;
    }

    interface IRootScopeService extends ng.IRootScopeService {
        mustBeLoadedIn:(ms:number) => void
        CP:IGlobalCP
        $state:ng.ui.IStateService
        global:IGlobalCPGlobal
        size:any
        safeApply:(fn?:() => void, method?:string) => void
        safeDigest:(scope:ng.IScope) => void
        goBack:() => void
        stripHtml:(text:string) => string
        trim:(text:string, length:number, ellipsify?:boolean) => string
        pageClasses:string
        addPageClass:(className:string) => void
        removePageClass:(className:string) => void
        hideKeyboard:() => void
        showKeyboard:() => void
        ckEditorOptions:any
        isHeadlessBrowser:boolean
        _data:any
        sessionID:string
        userID?:number
    }

    interface IAngularBlock {
        $inject?: string[];
    }

    interface IServerResponse {
        type:string
        response:number
        vars?:any
    }
}