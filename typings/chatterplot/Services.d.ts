/// <reference path="./Chatterplot.d.ts" />

declare module chatterplot.services {
    interface iTalkiService {
        buildURL:(language) => string
        getLanguage:(langID:number) => string
    }
}