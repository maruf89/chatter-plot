/// <reference path="./Chatterplot.d.ts" />


///////////////////////////////////////////////////////////////////////////////
// Search & activity related
///////////////////////////////////////////////////////////////////////////////
declare module chatterplot.activities {
    /**
     * The top level search controller that manages the search
     */
    interface ISearchCtrl {
        searchGroups:string[]
        searchType:string
        instance:any
    }

    interface ISearchObj {
        type:string
        group:string
        search:ISearchOptionsOpts
    }

    /**
     * The search object that gets passed to the server with all encompassing info
     */
    interface ISearchOptions {
        opts:ISearchOptionsOpts
        filterTypes:string[]
        queryTypes?:string[]
        sortTypes?:string[]
        initialFitAll?:boolean
        fitAll?:boolean
        venues?:boolean
    }

    /**
     * A subset of the search options that contains more specific details on what to get from the server
     */
    interface ISearchOptionsOpts {
        location:chatterplot.map.IConditionalLocation
        languages?:number[]
        bounds?:any
        allLangs?:boolean
        pagination?:number
        incrementBy?:number
        all?:boolean
        firstSearch?:boolean
        noMap?:boolean
        query?:string
    }

    /**
     * The directives for lists like tandemsList/eventsList
     */
    interface IListDirective {
        defaultSearch:any
        pre:(scope:IListDirectiveScope,
             iElem:cp.IAugmentedJQuery,
             iAttrs:ng.IAttributes,
             Ctrl:cp.activities.IListCtrl) => void

        postCallback:(callback:(scope:IListDirectiveScope,
                                iElem:cp.IAugmentedJQuery,
                                iAttrs:ng.IAttributes,
                                Ctrl:cp.activities.IListCtrl) => void)
            => void
    }

    /**
     * Scope of the directives list (tandemsList/eventsList etc…)
     */
    interface IListDirectiveScope extends cp.IScope {
        options:any
        vars:any
        id:string
        onRefresh:(post:any) => void
        zeroDataDirective?:string
        stack?:any
        group?:string
        methods?:any
    }

    interface IListDirectiveActivityScope extends cp.IScope {
        Activity:any
    }

    /**
     * {object} scope.setOptions
     *     For when 2 directories need the same map stack layer or you want extra control
     *     over the map stack layer
     *
     *     static: doesn't change
     *     one-time: changes once
     *     volatile: can change many times
     */
    interface IListStack {
        name:string                     // static - how to identify the stack
        initiated?:boolean              // one-time - If set to true will not create a new map layer but use the existing one
        destroyed?:boolean              // one-time - whether this instance has been destroyed
        revertStack?:boolean            // volatile - used when changing states and loading the previous results when hitting back
        mapOpts?:any                    // static - any options to pass to MapCtrl#selectSet (the second parameter)
        _exitMapCoords?:any             // volatile - the user hits forward/back they don't have to reload the results & re center/zoom the map
        _enterMapCoords?:any            // volatile - if we're reverting to a previous stack (after hitting forward/back)
        anyLoad?:(response:any) => any  // static - any callback
        markerSearch?:boolean           // volatile - Whether to do a 1-time marker search
        markerSearchRepeat?:boolean     // static - Whether search for markers with every search
        markerReplace?:boolean          // static - Whether when adding markers to replace the current ones (useful if 2 list instances sharing 1 stack)
    }

    /**
     * Directive Controller class of the lists
     */
    interface IListCtrl {
        listName:string
        resultsKey:string
        stack:IListStack
    }

    /**
     * The directive controller that handles changes in search options
     */
    interface IDirectiveSearchCtrl {

    }

    interface IDirectiveSearchScope extends cp.IScope {

    }

    interface IActivityItem {
        activityType:string
        activityID:string | number
    }

    interface IMarker extends cp.map.IMarker {
        _id:string          // the ID of the activity
        _class:string       // tandem/event/request
        _type:string        // subtype of class
        radius?:number
    }

    interface IService {
        map:any
        search:(search:ISearchOptionsOpts, type:string, group:string) => void
    }
}