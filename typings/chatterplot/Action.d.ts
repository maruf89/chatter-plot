/// <reference path="./Chatterplot.d.ts" />
/// <reference path="./Map.d.ts" />


///////////////////////////////////////////////////////////////////////////////
// Search & activity related
///////////////////////////////////////////////////////////////////////////////
declare module chatterplot.action {
    interface IAction {
        type:string
        action:string
        objID:string
        service?:string
    }

    interface IMarker extends cp.map.IMarker {
        _id?:string
        _class?:string
        _type?:string
        radius?:number
        __C?:string
    }

    interface IService {
        executeAction:(action:cp.action.IAction) => ng.IPromise<IAction>
    }
}