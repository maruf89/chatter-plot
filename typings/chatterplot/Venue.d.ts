/// <reference path="./Chatterplot.d.ts" />

declare module chatterplot.venue {
    interface IVenue {
        coords:cp.map.ICoordinates
        vID?:number
        name?:string
        google?:string
        yelp?:string
        comments?:number
        tz?:(string|number)
    }

    interface IVenueFull extends IVenue {
        coords:cp.map.IZoomCoordinates
        address:string
        sublocality:string
        serviceName:string
        specificity?:string
        type?:string
        city?:string
        postal?:string
        country?:string
        specObj?:any
        picture?:string
        vID?:number
        tz?:number
    }

    interface IPartialVenueObj {
        service?:google.maps.places.PlaceResult
        user?:cp.user.ILocation
        venue:IVenueFull
    }

    interface IVenueObj extends IPartialVenueObj {
        service:google.maps.places.PlaceResult
        user:cp.user.ILocation
    }

    interface ISpecificity {
        level:number
        type:string
        zoom:number
        name:boolean
        radius:number
    }

    /**
     * 'venueListings' - $scope.var definition
     */
    interface IListingsDirectiveVar {
        isHome:boolean
        isLoaded:boolean
        showInfo:boolean
        showRemove:boolean
        venue:any
        ourVenue?:any
        gmapUrl?:string
    }

    /**
     * 'venuesSelect' - $scope definition
     */
    interface ISelectDirectiveScope extends cp.IScope {
        venueData?:cp.venue.IVenueObj[]
        suggested?:cp.venue.IVenue[]
        selectedObj:any
        vars:ISelectDirectiveVars
    }

    interface ISelectDirectiveVars {
        loaded:boolean
        unique:string
        selected:any
        emptyInput:number
        venueData?:cp.venue.IVenueObj[]
        onLocSelect?:(place:google.maps.places.PlaceResult) => void
    }

    interface IVenueCache {
        [vID:number]:IPartialVenueObj
    }

    interface IService {
        venues:IVenueCache
        defaultFields:cp.venue.IVenueFull

        save:(venues:any, combine?:boolean) => ng.IPromise<cp.venue.IVenue[]>
        sanitize:(venues:cp.venue.IVenue[]) => cp.venue.IVenue[]
        combineVenueArray:(source:cp.venue.IVenue[], extended:cp.venue.IVenue[]) => void
        getPhotoSrc:(venue:google.maps.places.PlaceResult, height, width) => string
        getPlace:(source:(cp.venue.IVenue|cp.user.ILocation)) => ng.IPromise<google.maps.places.PlaceResult>
        getPlaceBulk:(Q:ng.IQService, venues:cp.venue.IVenue[], format?:boolean) => ng.IPromise<cp.venue.IVenueObj[]>

        getCoords:(place:google.maps.places.PlaceResult, serviceName?:string) => cp.map.ICoordinates
        scrapePlace:(
            place:google.maps.places.PlaceResult,
            serviceName:string,
            getSpecificity?:number
        ) => cp.venue.IVenueFull
        addressStringToUrl:(serviceName:string, ...args:string[]) => string
        scrapeSpecificity:(
            place:google.maps.places.PlaceResult,
            serviceName?:string
        ) => cp.venue.ISpecificity
        place2VenueObj:(place:google.maps.places.PlaceResult, serviceName:string) => cp.venue.IVenueObj
        addressObjToUrl:(serviceName:string, addressObj:any) => string
        formatBulk:(
            venues:cp.venue.IVenue[],
            format:number,
            fetchedLocations:google.maps.places.PlaceResult[]
        ) => any[]
        getCacheFormat:(venue:cp.venue.IVenue, withPlace?:boolean) => ng.IPromise<cp.venue.IVenueObj>
        cacheVenue:(venueObj:cp.venue.IVenue) => void
        getTz:(vID:number) => ng.IPromise<number>
    }
}