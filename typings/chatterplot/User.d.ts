/// <reference path="./Chatterplot.d.ts" />

declare module chatterplot.user {
    interface IService {
        userID:number
        data:IVolatileSecure
        format:any
        users:IUserCache
        addPeople:(userObj:cp.user.IUser, userIDs?:number[], asObject?:boolean) => cp.user.IUserCache
        update:(updates:any, persistent?:boolean) => ng.IPromise<boolean>
        cacheGet:(userIDs:number[], source?:string[], algorithm?:string) => ng.IPromise<IUserCache>
    }

    interface IPeopleService {
        map:any
        helper:IPeopleServiceHelper
    }

    interface IPeopleServiceHelper {

    }

    interface IUser {
        userID:number
        firstName:string
        lastName?:string
        fullName?:string
        joinDate?:Date
        features?:string[]
        birthday?:Date
        picture?:IPicture
        languages?:ILanguage[]
        locations?:ILocation[]
        personal?:string
        interests?:IInterest[]
        details?:IDetails
        settings?:ISettings
    }

    interface ISecure extends IUser {
        permission:number
        unreadNotification?:boolean
        email:string
        notifications:cp.notification.INotification
        settings?:ISettingsSecure
        userGroup:string
        attending:IAttending
        accounts?:IAccounts[]
        favorite?:IFavorite
    }

    /**
     * Could be an empty user object, could be completely filled out
     */
    interface IVolatileSecure {
        userID?:number
        firstName?:string
        joinDate?:Date
        permission?:number
        email?:string
        unreadNotification?:boolean
        notifications?:cp.notification.INotification
        settings?:ISettingsSecure
        userGroup?:string
        attending?:IAttending
        accounts?:IAccounts[]
        favorite?:IFavorite
        lastName?:string
        fullName?:string
        features?:string[]
        birthday?:Date
        picture?:IPicture
        languages?:ILanguage[]
        locations?:ILocation[]
        personal?:string
        interests?:IInterest[]
        details?:IDetails
    }

    interface IUserCache {
        [name:number]:IUser;
    }

    interface ILocation extends cp.venue.IVenue {
        primary?:boolean
        type?:string
    }

    interface IPicture {
        default?:string
        thumb?:string
        hasPhoto?:boolean
        service?:string
        id?:string
    }

    interface IFavorite {
        [name:string]:number[];
    }

    interface ILanguage {
        languageID:number
        level?:number
        learning?:boolean
        teaching?:boolean
        native?:boolean
        name?:string
    }

    interface IInterest {
        question:string
        answer:string
    }

    interface IDetails {
        gender?:string
    }

    interface IAccounts {
        type:string
        id:string
    }

    interface IAttending {
        events?:number[]
        eventsWaitList?:number[]
        attended?:number[]
    }

    interface ISettings {
        notifs:ISettingsNotifs
        requests:ISettingsReqs
    }

    interface ISettingsSecure {
        locale:string
        notifs:ISettingsNotifsSecure
        requests:ISettingsReqs
        timeZone:number
        formats:ISettingsFormats
        unit?:number
    }

    interface ISettingsFormats {
        date:number
        time:number
    }

    interface ISettingsNotifs {
        email:ISettingsNotifsEmail
        messaging:ISettingsNotifsMessaging
    }

    interface ISettingsReqs {
        teacher:boolean
        tandem:boolean
    }

    interface ISettingsNotifsSecure extends ISettingsNotifs {
        email:ISettingsNotifsEmailSecure
    }

    interface ISettingsNotifsEmail {
        newMsg:boolean
    }

    interface ISettingsNotifsEmailSecure extends ISettingsNotifsEmail {
        eventsILearn:boolean
        eventsISpeak:boolean
        eventsAllLang:boolean
        updates:boolean
        newMsg:boolean
        requestTandem:boolean
        requestTeacher:boolean
    }

    interface ISettingsNotifsMessaging {
        acceptAll:boolean
        acceptFollowing:boolean
    }

    interface IProfileCtrl {

    }

    interface IProfileCtrlScope extends cp.IScope {
        profile:IProfile
        user:IUser
    }

    interface IProfileFactory {
        create:(user:IUser) => IProfile
    }

    interface IProfile extends NodeJS.EventEmitter {
        isSelf:boolean
        options:any
        user:cp.user.IUser
        isMe:(forceCheck?:boolean) => boolean
        messageUser:() => void
        uploadPhoto:(userID:number, opts:any) => ng.IPromise<{picture:cp.user.IPicture}>
        editSection:(section:string) => void
        save:(updateObj:any) => ng.IPromise<cp.user.IUser>
        destroy:() => void
    }
}


