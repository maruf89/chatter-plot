"use strict";

const fs = require('fs');
const md5 = require('MD5');
const path = require('path');
const Q = require('q');
const _ = require('lodash-node/modern');

module.exports = function (grunt) {
    grunt.registerMultiTask('setHashVar', 'A hack to hashres files and set the hash to `grunt.option()` variable', function () {
        var done = this.async(),
            keys = {};

        this.files.forEach(function (file) {
            var hashVar = file.orig.hashVar;

            // if not there, add it
            if (!keys[hashVar]) {
                keys[hashVar] = {
                    files: [file],
                    varName: hashVar,
                };
            } else {
                keys[hashVar].files.push(file);
            }
        });

        // hash the first file in the group.
        // Assume if the first file in the group changed, then the rest did & vice-versa
        Q.all(_.map(keys, function (hashee) {
            var hashTest = hashee.files[0],
                deferred = Q.defer(),
                hash;

            fs.readFile(hashTest.src[0], function(err, buf) {
                if (err) {
                    return done();
                }

                hash = '_' + md5(buf).substr(0, 10);

                grunt.option(hashee.varName, '.' + hash);
                console.log(hashee.varName, '.' + hash);

                // Now rename each file
                Q.all(_.map(hashee.files, function (_file) {
                    var _deferred = Q.defer(),
                        file = _file.src[0].match(/[^/]+$/)[0],
                        fileLen = file.length,
                        ext = file.match(/[^\.]+$/)[0],
                        fileBase = file.substr(0, fileLen - ext.length),
                        newFile = fileBase + hash + '.' + ext;

                    fs.rename(_file.src[0], _file.src[0].replace(file, newFile), function (err) {
                        if (err) {
                            console.log(err);
                            return _deferred.reject(err);
                        }

                        return _deferred.resolve();
                    });

                    return _deferred.promise;
                })).then(deferred.resolve);
            });

            return deferred.promise;
        })).finally(done);
    });
};