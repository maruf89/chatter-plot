/// <reference path="../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
var _ = require('lodash-node/modern'), cloudinary = exports, _transformations = {
    crop: 'c_crop',
    scale: 'c_scale',
    mfit: 'c_mfit',
    fit: 'c_fit',
    fill: 'c_fill',
    limit: 'c_limit',
    thumb: 'c_thumb',
    width: function (val) {
        return 'w_' + val;
    },
    height: function (val) {
        return 'h_' + val;
    }
}, CLOUD_NAME, SRC_URL = '', TYPE_POSTFIX = '';
/**
 * @description needed to set some startup variables
 * @param {object} opts - must contain cloudName
 * @returns {cloudinary}
 */
exports.init = function (opts) {
    CLOUD_NAME = opts.cloudName;
    SRC_URL = 'https://res.cloudinary.com/' + CLOUD_NAME + '/image/<service><transform>/v<time>/<path>.jpg';
    // If we're on a non-production environment we will need to prefix images folders with the subdomain
    if (opts.typePostfix) {
        TYPE_POSTFIX = opts.typePostfix + '/';
    }
    cloudinary.init = function () { return cloudinary; };
    return cloudinary.init();
};
exports.SIZE_PRESETS = {
    "thumb": {
        width: 42,
        height: 42,
        thumb: true
    },
    "default": {
        width: 150,
        height: 150,
        thumb: true
    }
};
/**
 * @description generates the image for the cloudinary object
 * @name generateSrc
 * @param {string} publicID - the unique photo identifier
 * @param {string=} service - whether we're using either of their 3rd party services like Facebook
 * @param {string=} type - which folder type it belongs to (profile, events, etc…)
 * @param {(object|string)} opts - if a string, expects a key to `optsPresets` otherwise transformation options
 * @returns {string}
 */
exports.generateSrc = function (publicID, service, type, opts) {
    type = type || '';
    var time = cloudinary.timestamp(), path = '', transform = '', src = SRC_URL;
    if (opts) {
        if (typeof opts === 'string' && cloudinary.SIZE_PRESETS[opts]) {
            transform = '/' + cloudinary.buildTransform(cloudinary.SIZE_PRESETS[opts]);
        }
        else if (opts = cloudinary.buildTransform(opts)) {
            transform = '/' + opts;
        }
    }
    if (!service) {
        service = 'upload';
        if (type) {
            type += '/' + TYPE_POSTFIX;
            path = type + publicID;
        }
    }
    return src
        .replace('<service>', service)
        .replace('<transform>', transform)
        .replace('<time>', time)
        .replace('<path>', path || publicID);
};
exports.buildTransform = function (opts) {
    var baseTransformation, pieces, result;
    if (_.isArray(opts)) {
        result = (function () {
            var i = 0, len = opts.length, results = [];
            for (; i < len; i++) {
                baseTransformation = opts[i];
                results.push(cloudinary.buildTransform(_.clone(baseTransformation)));
            }
            return results;
        })();
        return result.join('/');
    }
    pieces = [];
    _.each(opts, function (val, key) {
        var prop = _transformations[key];
        if (prop) {
            return pieces.push(typeof prop === 'function' ? prop(val) : prop);
        }
        return pieces.push(key + "_" + val);
    });
    return pieces.join(',');
};
exports.buildArray = function (arg) {
    if (arg == null) {
        return [];
    }
    else if (Array.isArray(arg)) {
        return arg;
    }
    else {
        return [arg];
    }
};
exports.timestamp = function () {
    return Math.floor(new Date().getTime() / 1000);
};
exports.transform = function (transforms) {
    transforms = _.map(transforms, cloudinary.buildTransform);
    return transforms.join('|');
};
exports.servicePhoto = function (service, serviceID, transforms) {
    var base = "https://res.cloudinary.com/" + CLOUD_NAME + "/image/" + service + "/", transform = transforms ? cloudinary.buildTransform(transforms) + '/' : '', user = serviceID + '.jpg';
    return base + transform + user;
};
exports.fbPhoto = _.partial(cloudinary.servicePhoto, 'facebook');
//# sourceMappingURL=cloudinary.js.map