/// <reference path="../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
var sortFunctions = {
    date: {
        key: 'date',
        sortAsc: function (a, b) {
            if (a.when < b.when) {
                return -1;
            }
            if (a.when > b.when) {
                return 1;
            }
            else {
                return 0;
            }
        },
        sortDesc: function (a, b) {
            if (a.when > b.when) {
                return -1;
            }
            if (a.when < b.when) {
                return 1;
            }
            else {
                return 0;
            }
        },
    }
};
/**
 * @description Groups an array of event items based on a key
 *
 * @param {array<array>} groups - top level groups which will hold subgroups of items | *MODIFIES OBJECT*
 * @param {array<array>} subGroups - array of arrays (that have length > 1) that will need sub sorting | *MODIFIES OBJECT*
 * @param {string} key - any property that each item has that is sortable
 * @param {object} item
 * @returns {array<array>} returns groups
 * @private
 */
function _groupItems(groups, subGroups, key, opts, item) {
    item = opts.onEachFn(item) || item;
    if (!groups[item[key]]) {
        // If the first item in a group
        groups[item[key]] = [item];
    }
    else {
        groups[item[key]].push(item);
        // AFTER pushing - if is second item
        // then we know that this array's items will need to be sorted
        // -> push this groups identifier to the subgroups for processing
        if (groups[item[key]].length === 2) {
            subGroups.push(groups[item[key]]);
        }
    }
    return groups;
}
/**
 * @description Breaks an Array of items into groups (whatever `sortFunctions` relates to)
 * it then sorts those groups and sorts the items within the groups based on a key
 * Example need: we want to sort a list of events by date (array of dates containing an array of items on that date)
 *
 * @param  {array<object>} items - Array of objects to group sort
 * @param {object} opts
 * @param {string} opts.groupBy - a string key to sort objects by (so far only date)
 * @param {function=} opts.onEachFn - a function to run on each item - useful for formatting
 * @return {array<array<object>>} Array of groups containing Arrays of events all sorted
 */
exports.formatItems = function (items, opts) {
    var groupType, groups, subSort, subSortFn, itemLength = items.length, values, sortFn = "sort" + (opts.order === 'desc' ? 'Desc' : 'Asc');
    if (!itemLength) {
        return [];
    }
    opts.onEachFn = opts.onEachFn || function () { };
    groups = {};
    groupType = sortFunctions[opts.groupBy];
    // an array that holds all sub arrays of values that have a length > 1
    // so that we can sort sub array
    subSort = [];
    _.each(items, _.partial(_groupItems, groups, subSort, groupType.key, opts), this);
    values = _.values(groups);
    values.sort(function (a, b) {
        return groupType[sortFn](a[0], b[0]);
    });
    // check if there's a sub sort method defined, otherwise use base
    subSortFn = groupType.subSort || groupType[sortFn];
    _.each(subSort, function (group) {
        return group.sort(subSortFn);
    });
    return values;
};
/**
 * @description the idea is to combine 2 sorted group arrays.
 * Example: there are 3 events on Tuesday but the source array only has 2 of them, and the addition array has the other
 * We want to be sure that they are grouped together
 * @param {array<object>} sourceArr - the source array which will have addArr added to it | *OBJECT MODIFIED*
 * @param {array<object>} addArr - the later addition array | *OBJECT MODIFIED*
 */
exports.stitchGroups = function (sourceArr, addArr, key) {
    var sourceLen = sourceArr && sourceArr.length, firstSeam, sourceLastArr, secondSeam;
    if (!sourceLen && addArr && addArr.length) {
        // if we have addArr but not sourceArr
        [].push.apply(sourceArr, addArr);
        return sourceArr;
    }
    else if (!(sourceLen && addArr && addArr.length)) {
        // otherwise we have nothing, return source
        return sourceArr;
    }
    sourceLastArr = sourceArr[sourceLen - 1];
    // get the first event of the last group
    firstSeam = sourceLastArr[0];
    // get the first event of the sourceArr group
    secondSeam = addArr[0][0];
    if (firstSeam[key] === secondSeam[key]) {
        // If the dates are the same
        // move the contents of addArr's first array onto the last array of source
        [].push.apply(sourceLastArr, addArr.shift());
    }
    // finally add the rest of the contents to the source
    [].push.apply(sourceArr, addArr);
    return sourceArr;
};
//# sourceMappingURL=groupSort.js.map