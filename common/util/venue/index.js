/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
var _ = require('lodash-node/modern'), common = exports, commonGeneral = require('../general'), Services = {
    google: require('./google')
}, _getServiceName = function (data) {
    return data.scope === 'GOOGLE' ? 'google' : 'yelp';
}, _getService = function (serviceName, place) {
    serviceName = serviceName || _getServiceName(place);
    return Services[serviceName];
}, 
// Set in config
Q = null;
exports.serviceConfig = function (serviceName, opts) {
    _getService(serviceName).config(opts);
    if (opts.Q) {
        Q = opts.Q;
    }
};
/**
 * @description the default fields of an venue
 * @type {object}
 */
exports.DEFAULT_FIELDS = {
    name: null,
    coords: null,
    address: null,
    sublocality: null,
    city: null,
    state: null,
    country: null,
    postal: null
};
exports.DEFAULT_SPECIFICITY = {
    type: 'coords',
    zoom: 15,
    name: false,
    radius: 1650
};
/**
 * Accepts a location ID for a specific service and fetches it
 *
 * @param  {object} source  Object containing the necessary fields for the requested service
 * @return {Promise}        Promise will contain the request location
 */
exports.getPlace = function (source) {
    var serviceName = source.google ? 'google' :
        source.yelp ? 'yelp' : null, service = _getService(serviceName);
    return service.getPlace(source[serviceName]);
};
/**
 * @description returns in bulk multiple locations based on their ID
 * @param {array<object>} venues
 * @param {boolean=} format - whether to format the objects as cp.user.ILocation
 * @returns {IPromise<T[]>}
 */
exports.getPlaceBulk = function (venues, format) {
    var promise = Q.all(_.map(venues, common.getPlace, this));
    if (format) {
        promise.then(_.partial(common.formatBulk, venues, 1));
    }
    return promise;
};
/**
 * @description formats bulk events
 * @param {array<object>} venues - array of source venues (whatever was passed to getPlaceBulk as the first arg)
 * @param {number} format - 1 = cp.venue.IVenueObj | 2 = cp.venue.IVenueFull
 * @param {array<object>} fetchedLocations - array of google/yelp source objects
 * @returns {array<object>}
 */
exports.formatBulk = function (venues, format, fetchedLocations) {
    if (format === 1) {
        return _.map(venues, function (location, index) {
            var obj = {
                venue: common.scrapePlace(fetchedLocations[index]),
                user: location,
                service: fetchedLocations[index]
            };
            if (location.vID) {
                obj.venue.vID = location.vID;
            }
            return obj;
        });
    }
    else if (format === 2) {
        return _.map(venues, function (location, index) {
            var obj = common.scrapePlace(fetchedLocations[index]);
            if (location.vID) {
                obj.vID = location.vID;
            }
            return obj;
        });
    }
};
/**
 * @description Scrapes the address from the location object for the service
 *
 * @param  {object} place - Location object with all address info
 * @param  {string} serviceName - So far only `Google` but open for other services later
 * @param  {(boolean|number)} getSpecificity - Whether to exctract the specificity of the location
 *                            If === 2, then saves the type name as a String
 * @return {cp.venue.IVenueFull} - Contains all the address parts
 */
exports.scrapePlace = function (place, serviceName, getSpecificity) {
    var serviceName = (serviceName || _getServiceName(place)).toLocaleLowerCase(), service = Services[serviceName], street = [], fields = service.FIELDS, data = _.clone(common.DEFAULT_FIELDS), specificity;
    service.prepareVenue(place, data);
    data.coords = common.getCoords(place, serviceName);
    data.type = service.isBusiness(place) ? 'business' : 'home';
    _.each(place[fields.address], function (comp) {
        return _.each(comp.types, function (type) {
            var key, 
            // We want the long form of certain address fields (Get 'Potsdam' instead of 'P')
            propName;
            if (key = fields.keys[type]) {
                propName = fields.fieldsGetLong.indexOf(type) !== -1 ? 'long_name' : 'short_name';
                if (typeof key === 'number') {
                    return street[key - 1] = comp[propName].trim();
                }
                else {
                    return data[key] = comp[propName].trim();
                }
            }
        });
    });
    if (street.length) {
        data.address = street.join(' ');
    }
    if (data.sublocality) {
        // remove unnecessary text
        data.sublocality = data.sublocality.replace('Bezirk ', '');
    }
    if (getSpecificity) {
        specificity = common.scrapeSpecificity(place, serviceName);
        // check if we want just the string value here
        if (getSpecificity === 2) {
            data.specificity = specificity.type;
        }
        else {
            data.specObj = specificity;
        }
        data.coords.zoom = specificity.zoom;
    }
    return data;
};
/**
 * @description returns a coordinates object from a place
 *
 * @param {object} place - the service object with all of the essential venue data
 * @param {string} serviceName - google or yelp
 * @returns {cp.map.ICoordinates}
 */
exports.getCoords = function (place, serviceName) {
    var service = _getService(serviceName, place);
    return {
        lat: service.getCoord(place, 'lat'),
        lon: service.getCoord(place, 'lng')
    };
};
/**
 * @description simply transform a service venue object to IVenueObj
 * @param {object} place - the service object with all of the essential venue data
 * @param {string} serviceName - google or yelp
 * @returns {cp.venue.IVenueObj}
 */
exports.place2VenueObj = function (place, serviceName) {
    var venue = common.scrapePlace(place, serviceName);
    return {
        venue: venue,
        user: _formatUserLocation(venue, serviceName),
        service: place
    };
};
var _formatUserLocation = function (venue, serviceName) {
    var obj = {
        coords: venue.coords,
        primary: false
    };
    obj[serviceName] = venue[serviceName];
    return obj;
};
/**
 * @description converts an array of address parts to a URL for the provided service
 *
 * @param {string} serviceName - google or yelp
 * @param {array<string>} args
 * @returns {string} - returns a full URL
 */
exports.addressStringToUrl = function (serviceName) {
    var args = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        args[_i - 1] = arguments[_i];
    }
    return _getService(serviceName).addressArrayToUrl(args);
};
/**
 * @description converts an address object to a URL for the provided service
 *
 * @param {string} serviceName - google or yelp
 * @param {object} addressObj
 * @returns {string} - returns a full URL
 */
exports.addressObjToUrl = function (serviceName, addressObj, type) {
    type = type || 'default';
    var addressArr;
    if (((addressObj.name || addressObj.address) &&
        addressObj.state && addressObj.country) ||
        !addressObj.coords) {
        addressArr = ['address', 'sublocality', 'city', 'state', 'country', 'postal'];
        if (addressObj.name) {
            addressArr.unshift(addressObj.name);
        }
    }
    else {
        addressArr = [addressObj.coords.lat + ',' + addressObj.coords.lon];
    }
    return _getService(serviceName).addressArrayToUrl(addressArr, type);
};
/**
 * @description converts an array with address fields into an address string
 *
 * @param {object} object - the object containing the fields
 * @param {array<string>=} fields - the fields to pluck if they exist
 * @param {string=} separator - what to use to join the arrays into a string
 * @param {number=} zoom - if passed will apply #addressFieldsFromZoom to the fields array
 * @returns {string}
 */
exports.objToAddress = function (object, fields, separator, zoom) {
    fields = fields || ['address', 'sublocality', 'city', 'state', 'country', 'postal'];
    separator = separator || ' ';
    if (zoom) {
        fields = common.addressFieldsFromZoom(fields, zoom);
    }
    return _.reduce(fields, function (prev, cur) {
        if (object[cur]) {
            prev.push(object[cur]);
        }
        return prev;
    }, []).join(separator);
};
/**
 * @description returns just the address fields corresponding to a zoom level
 * example: say we have a complete address object, but we don't want the street address. We only want
 *          as much as would show at a zoom level of 10 => this would return just ['city', 'state']
 *
 * @param {array<string>} fieldsArr - array containing any combination of:
 *                                      (address|sublocality|postal|city|state)
 * @param {number} zoom - a google maps zoom level
 * @returns {array<string>} deduced fields array
 */
exports.addressFieldsFromZoom = function (fieldsArr, zoom) {
    var fields = _.clone(fieldsArr), map = {
        '16': 'address',
        '14': 'sublocality',
        '12': 'postal',
        '10': 'city',
        '8': 'state'
    };
    _.each(map, function (fieldName, zoomLevel) {
        if (zoom < parseInt(zoomLevel, 10)) {
            commonGeneral.array.remove(fields, fieldName);
        }
    });
    return fields;
};
exports.address2Spec = function (address, serviceName) {
    serviceName = serviceName || 'google';
    if (address.specificity) {
        return _spec2SpecObj(address.specificity, serviceName);
    }
    var addressSpecs = _getService(serviceName).ADDRESS_2_SPEC, i = 0, len = addressSpecs.length, type;
    for (; i < len; i++) {
        type = addressSpecs[i];
        if (address[type.attr]) {
            return _spec2SpecObj(type.type, serviceName);
        }
    }
    return common.DEFAULT_SPECIFICITY;
};
var _spec2SpecObj = function (specificity, serviceName) {
    var specLevel = _getService(serviceName).SPEC_LEVEL, i = 0, len = specLevel.length, level;
    for (; i < len; i++) {
        level = specLevel[i];
        if (level.type === specificity) {
            return level;
        }
    }
    return common.DEFAULT_SPECIFICITY;
};
/**
 * Accepts a specificity level string and returns a corresponding spec object
 *
 * @param  {object} place
 * @param  {string} serviceName - which service to check
 * @return {object} - the correct spec, or the default spec if none found
 */
exports.scrapeSpecificity = function (place, serviceName) {
    var service = _getService(serviceName, place), specificity = common.DEFAULT_SPECIFICITY;
    if (_.isArray(place.types)) {
        _.each(service.SPEC_LEVEL, function (spec) {
            if (place.types.indexOf(spec.type) !== -1) {
                return specificity = spec;
            }
        });
    }
    return specificity;
};
exports.getTimezoneURL = function (coords, date) {
    return Services.google.getTimezoneURL(coords, date);
};
//# sourceMappingURL=index.js.map