/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

"use strict";

var _:_.LoDashStatic = require('lodash-node/modern'),

    common = exports,
    commonGeneral = require('../general'),

    Services = {
        google: require('./google')
    },

    _getServiceName = function (data:any):string {
        return data.scope === 'GOOGLE' ? 'google' : 'yelp';
    },

    _getService = function (serviceName?:string, place?:any):any {
        serviceName = serviceName || _getServiceName(place);
        return Services[serviceName];
    },

    // Set in config
    Q = null;

exports.serviceConfig = function (serviceName:string, opts:any):void {
    _getService(serviceName).config(opts);

    if (opts.Q) {
        Q = opts.Q;
    }
};

/**
 * @description the default fields of an venue
 * @type {object}
 */
exports.DEFAULT_FIELDS = {
    name: null,
    coords: null,
    address: null,
    sublocality: null,
    city: null,
    state: null,
    country: null,
    postal: null
};

exports.DEFAULT_SPECIFICITY = {
    type: 'coords',
    zoom: 15,
    name: false,
    radius: 1650
};

/**
 * Accepts a location ID for a specific service and fetches it
 *
 * @param  {object} source  Object containing the necessary fields for the requested service
 * @return {Promise}        Promise will contain the request location
 */
exports.getPlace = function (source:(cp.venue.IVenue|cp.user.ILocation)):ng.IPromise<google.maps.places.PlaceResult> {
    var serviceName = source.google ? 'google' :
                 source.yelp ? 'yelp' : null,

        service = _getService(serviceName);

    return service.getPlace(source[serviceName]);
};

/**
 * @description returns in bulk multiple locations based on their ID
 * @param {array<object>} venues
 * @param {boolean=} format - whether to format the objects as cp.user.ILocation
 * @returns {IPromise<T[]>}
 */
exports.getPlaceBulk = function (venues:cp.venue.IVenue[], format?:boolean):ng.IPromise<cp.venue.IVenueObj[]> {
    var promise = Q.all(_.map(venues, common.getPlace, this));

    if (format) {
        promise.then(_.partial(common.formatBulk, venues, 1));
    }

    return promise;
};

/**
 * @description formats bulk events
 * @param {array<object>} venues - array of source venues (whatever was passed to getPlaceBulk as the first arg)
 * @param {number} format - 1 = cp.venue.IVenueObj | 2 = cp.venue.IVenueFull
 * @param {array<object>} fetchedLocations - array of google/yelp source objects
 * @returns {array<object>}
 */
exports.formatBulk = function (
    venues:cp.venue.IVenue[],
    format:number,
    fetchedLocations:google.maps.places.PlaceResult[]
):any[] {
    if (format === 1) {
        return _.map(venues, function (location:cp.venue.IVenue, index:number) {
            var obj:cp.venue.IVenueObj = {
                venue: common.scrapePlace(fetchedLocations[index]),
                user: location,
                service: fetchedLocations[index]
            };

            if (location.vID) {
                obj.venue.vID = location.vID;
            }

            return obj;
        })
    } else if (format === 2) {
        return _.map(venues, function (location:cp.venue.IVenue, index:number) {
            var obj:cp.venue.IVenueFull = common.scrapePlace(fetchedLocations[index]);

            if (location.vID) {
                obj.vID = location.vID;
            }

            return obj;
        })
    }
};

/**
 * @description Scrapes the address from the location object for the service
 *
 * @param  {object} place - Location object with all address info
 * @param  {string} serviceName - So far only `Google` but open for other services later
 * @param  {(boolean|number)} getSpecificity - Whether to exctract the specificity of the location
 *                            If === 2, then saves the type name as a String
 * @return {cp.venue.IVenueFull} - Contains all the address parts
 */
exports.scrapePlace = function (
    place:google.maps.places.PlaceResult,
    serviceName:string,
    getSpecificity?:number
):cp.venue.IVenueFull {
    var serviceName:string = (serviceName || _getServiceName(place)).toLocaleLowerCase(),
        service = Services[serviceName],
        street:string[] = [],
        fields:any = service.FIELDS,
        data:cp.venue.IVenueFull = _.clone(common.DEFAULT_FIELDS),
        specificity:cp.venue.ISpecificity;

    service.prepareVenue(place, data);

    data.coords = common.getCoords(place, serviceName);
    data.type = service.isBusiness(place) ? 'business' : 'home';

    _.each(place[fields.address], function (comp: any) {
        return _.each(comp.types, function (type: string) {
            var key:(number|string),

                // We want the long form of certain address fields (Get 'Potsdam' instead of 'P')
                propName:string;

            if (key = fields.keys[type]) {
                propName = fields.fieldsGetLong.indexOf(type) !== -1 ? 'long_name' : 'short_name';
                if (typeof key === 'number') {
                    return street[key - 1] = comp[propName].trim();
                } else {
                    return data[key] = comp[propName].trim();
                }
            }
        });
    });

    if (street.length) {
        data.address = street.join(' ');
    }

    if (data.sublocality) {
        // remove unnecessary text
        data.sublocality = data.sublocality.replace('Bezirk ', '');
    }

    if (getSpecificity) {
        specificity = common.scrapeSpecificity(place, serviceName);

        // check if we want just the string value here
        if (getSpecificity === 2) {
            data.specificity = specificity.type;
        } else {
            data.specObj = specificity;
        }

        data.coords.zoom = specificity.zoom;
    }

    return data;
};

/**
 * @description returns a coordinates object from a place
 *
 * @param {object} place - the service object with all of the essential venue data
 * @param {string} serviceName - google or yelp
 * @returns {cp.map.ICoordinates}
 */
exports.getCoords = function (place:google.maps.places.PlaceResult, serviceName?:string):cp.map.ICoordinates {
    var service = _getService(serviceName, place);

    return {
        lat: service.getCoord(place, 'lat'),
        lon: service.getCoord(place, 'lng')
    };
};

/**
 * @description simply transform a service venue object to IVenueObj
 * @param {object} place - the service object with all of the essential venue data
 * @param {string} serviceName - google or yelp
 * @returns {cp.venue.IVenueObj}
 */
exports.place2VenueObj = function (place:google.maps.places.PlaceResult, serviceName:string):cp.venue.IVenueObj {
    var venue:cp.venue.IVenueFull = common.scrapePlace(place, serviceName);

    return {
        venue: venue,
        user: _formatUserLocation(venue, serviceName),
        service: place
    }
};

var _formatUserLocation = function (venue:cp.venue.IVenueFull, serviceName:string):cp.user.ILocation {
    var obj:cp.user.ILocation = {
        coords: venue.coords,
        primary: false
    };

    obj[serviceName] = venue[serviceName];

    return obj;
};

/**
 * @description converts an array of address parts to a URL for the provided service
 *
 * @param {string} serviceName - google or yelp
 * @param {array<string>} args
 * @returns {string} - returns a full URL
 */
exports.addressStringToUrl = function (serviceName:string, ...args:string[]):string {
    return _getService(serviceName).addressArrayToUrl(args);
};

/**
 * @description converts an address object to a URL for the provided service
 *
 * @param {string} serviceName - google or yelp
 * @param {object} addressObj
 * @returns {string} - returns a full URL
 */
exports.addressObjToUrl = function (serviceName:string, addressObj:any, type?:string):string {
    type = type || 'default';

    var addressArr:string[];

    if (((addressObj.name || addressObj.address) &&
        addressObj.state && addressObj.country) ||
        !addressObj.coords
    ) {
        addressArr = ['address', 'sublocality', 'city', 'state', 'country', 'postal'];

        if (addressObj.name) {
            addressArr.unshift(addressObj.name);
        }
    } else {
        addressArr = [addressObj.coords.lat + ',' + addressObj.coords.lon];
    }

    return _getService(serviceName).addressArrayToUrl(addressArr, type);
};

/**
 * @description converts an array with address fields into an address string
 *
 * @param {object} object - the object containing the fields
 * @param {array<string>=} fields - the fields to pluck if they exist
 * @param {string=} separator - what to use to join the arrays into a string
 * @param {number=} zoom - if passed will apply #addressFieldsFromZoom to the fields array
 * @returns {string}
 */
exports.objToAddress = function (object:any, fields?:string[], separator?:string, zoom?:number):string {
    fields = fields || ['address', 'sublocality', 'city', 'state', 'country', 'postal'];

    separator = separator || ' ';

    if (zoom) {
        fields = common.addressFieldsFromZoom(fields, zoom);
    }

    return _.reduce(fields, function (prev, cur) {
        if (object[cur]) {
            prev.push(object[cur]);
        }
        return prev;
    }, []).join(separator);
};

/**
 * @description returns just the address fields corresponding to a zoom level
 * example: say we have a complete address object, but we don't want the street address. We only want
 *          as much as would show at a zoom level of 10 => this would return just ['city', 'state']
 *
 * @param {array<string>} fieldsArr - array containing any combination of:
 *                                      (address|sublocality|postal|city|state)
 * @param {number} zoom - a google maps zoom level
 * @returns {array<string>} deduced fields array
 */
exports.addressFieldsFromZoom = function (fieldsArr, zoom) {
    var fields = _.clone(fieldsArr),
        map = {
            '16': 'address',
            '14': 'sublocality',
            '12': 'postal',
            '10': 'city',
            '8': 'state'
        };

    _.each(map, function (fieldName, zoomLevel) {
        if (zoom < parseInt(zoomLevel, 10)) {
            commonGeneral.array.remove(fields, fieldName);
        }
    });

    return fields;
};

exports.address2Spec = function (address:any, serviceName?:string):cp.venue.ISpecificity {
    serviceName = serviceName || 'google';

    if (address.specificity) {
        return _spec2SpecObj(address.specificity, serviceName);
    }

    var addressSpecs = _getService(serviceName).ADDRESS_2_SPEC,
        i:number = 0,
        len:number = addressSpecs.length,
        type:any;

    for (;i < len; i++) {
        type = addressSpecs[i];
        if (address[type.attr]) {
            return _spec2SpecObj(type.type, serviceName);
        }
    }

    return common.DEFAULT_SPECIFICITY;
};

var _spec2SpecObj = function (specificity:string, serviceName?:string):cp.venue.ISpecificity {
    var specLevel:cp.venue.ISpecificity[] = _getService(serviceName).SPEC_LEVEL,
        i:number = 0,
        len:number = specLevel.length,
        level:cp.venue.ISpecificity;

    for (; i < len; i++) {
        level = specLevel[i];
        if (level.type === specificity) {
            return level;
        }
    }

    return common.DEFAULT_SPECIFICITY;
};

/**
 * Accepts a specificity level string and returns a corresponding spec object
 *
 * @param  {object} place
 * @param  {string} serviceName - which service to check
 * @return {object} - the correct spec, or the default spec if none found
 */
exports.scrapeSpecificity = function (
    place:google.maps.places.PlaceResult,
    serviceName?:string
):cp.venue.ISpecificity {

    var service = _getService(serviceName, place),
        specificity:cp.venue.ISpecificity = common.DEFAULT_SPECIFICITY;

    if (_.isArray(place.types)) {
        _.each(service.SPEC_LEVEL, function (spec:cp.venue.ISpecificity):any {
            if (place.types.indexOf(spec.type) !== -1) {
                return specificity = spec;
            }
        });
    }

    return specificity;
};

exports.getTimezoneURL = function (coords, date) {
    return Services.google.getTimezoneURL(coords, date);
};
