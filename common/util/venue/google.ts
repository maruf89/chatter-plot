/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />

"use strict";

var _:_.LoDashStatic = require('lodash-node/modern'),

    common = exports,

    NAME = 'google',

    URL_BASE = 'https://www.google.com/maps',
    queryTypes = {
        "default": '?q=',
        directions: '/dir/Current+Location/',
    };

exports.name = NAME;

exports.config = function (settings) {
    common.getPlace = settings.getPlace;
};

/**
 * Will be set from exports.config
 */
exports.getPlace = function (sourceID:string) {
    console.log('#getPlace not set yet')
};

exports.getCoord = function (place:google.maps.places.PlaceResult, key:string):cp.map.ICoordinates {
    if (typeof place.geometry.location[key] === 'function') {
        return place.geometry.location[key]();
    }

    return place.geometry.location[key];
};

exports.isBusiness = function (place:google.maps.places.PlaceResult):boolean {
    return _.isArray(place.types) && place.types.indexOf('establishment') !== -1;
};

exports.prepareVenue = function (place:google.maps.places.PlaceResult, location:cp.venue.IVenueFull):cp.venue.IVenueFull {
    // Only save the name if it's an establishment
    if (place.types.indexOf('establishment') !== -1) {
        location.name = place.name;
    }

    location.serviceName = NAME;

    // Get the serviceNames unique id
    location[NAME] = place[common.placeID];

    var tzModifer:string;

    if (place.tz) {
        // We must convert "GMT+0200" => 120
        tzModifer = place.tz.substr(-5, 1);
        location.tz = (parseInt(place.tz.substr(-4, 2), 10) * 60) * (tzModifer === '-' ? -1 : 1);
    }

    return location;
};

exports.addressArrayToUrl = function (stringArr:string[], type?:string):string {
    var string:string = stringArr.join(' ').trim();
    return URL_BASE + queryTypes[type || 'default'] + string.replace(/,?\s/g, '+');
};

exports.placeID = 'place_id';

exports.FIELDS = {
    address: 'address_components',
    fields: ['address', 'sublocality', 'city', 'state', 'country', 'postal'],
    fieldsGetLong: ['locality', 'administrative_area_level_1'],
    keys: {
        "establishment": "locationName",
        "street_number": 1,
        "route": 2,
        "sublocality_level_1": "sublocality",
        "sublocality_level_2": "sublocality",
        "neighbourhood": "sublocality",
        "neighborhood": "sublocality",
        "locality": "city",
        "postal_town": "city",
        "administrative_area_level_1": "state",
        "country": "country",
        "postal_code": "postal",
        "tz": "tz"
    },
};

exports.SPEC_LEVEL = [{
    level: 1,
    type: 'street_address',
    zoom: 16,
    name: false,
    radius: 1000
}, {
    level: 1,
    type: 'establishment',
    zoom: 16,
    name: true,
    radius: 1000
}, {
    level: 2,
    type: 'neighbourhood',
    zoom: 15,
    name: true,
    radius: 1650
}, {
    level: 3,
    type: 'postal_code',
    zoom: 15,
    name: true,
    radius: 1650
}, {
    level: 3,
    type: 'sublocality_level_2',
    zoom: 14,
    name: true,
    radius: 1715
}, {
    level: 3,
    type: 'sublocality_level_1',
    zoom: 14,
    name: true,
    radius: 2520
}, {
    level: 4,
    type: 'locality',
    zoom: 12,
    name: true,
    radius: 10001
}, {
    level: 5,
    type: 'administrative_area_level_2',
    zoom: 10,
    name: true,
    radius: 30000
}, {
    level: 6,
    type: 'administrative_area_level_1',
    zoom: 8,
    name: false,
    radius: 187500
}, {
    level: 7,
    type: 'country',
    zoom: 6,
    name: false,
    radius: 270000
}];

exports.ADDRESS_2_SPEC = [{
    attr: 'locationName',
    type: 'establishment'
}, {
    attr: 'sublocality',
    type: 'neighbourhood'
}, {
    attr: 'address',
    type: 'street_address'
}, {
    attr: 'postal',
    type: 'postal_code'
}, {
    attr: 'city',
    type: 'locality'
}, {
    attr: 'state',
    type: 'administrative_area_level_1'
}, {
    attr: 'country',
    type: 'country'
}];
