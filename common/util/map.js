/// <reference path="../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
var _ = require('lodash-node/modern'), common = exports;
/**
 * @description Normalized a coords object for Elasticsearch
 * @name #normalizeCoords
 * @param  {object} coords
 * @return {object}         a clean object with `lat` and `lon`
 */
exports.normalizeCoords = function (coords) {
    return {
        lat: coords.lat || coords.latitude,
        lon: coords.lon || coords.longitude
    };
};
exports.toRadians = function (num) {
    return num * Math.PI / 180;
};
exports.bounds2Diameter = function (bounds) {
    var R = 6371, a, c, ref = bounds.split(',').map(function (z) {
        return parseInt(z, 10);
    }), lat1 = ref[0], lon1 = ref[1], lat2 = ref[2], lon2 = ref[3], 
    // km
    dLat = common.toRadians(lat2 - lat1), dLon = common.toRadians(lon2 - lon1);
    lat1 = common.toRadians(lat1);
    lat2 = common.toRadians(lat2);
    a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) *
        Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
    c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return R * c;
};
//# sourceMappingURL=map.js.map