/// <reference path="../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
var commonCloudinary = require('./cloudinary'), common = exports, IMG_FALLBACK = '', IMG_MIME = 'svg', IMG_BASE = 'https://www.chatterplot.com';
exports.init = function (opts) {
    if (opts.noSVG) {
        IMG_FALLBACK = '/fallback';
        IMG_MIME = 'png';
    }
    common.init = function () { return common; };
    return common.init();
};
exports.defaultAvatar = function (gender) {
    gender = gender || 'm';
    gender = gender[0].toLowerCase();
    return IMG_BASE + '/images/profile' + IMG_FALLBACK + '/default-' + gender + '.' + IMG_MIME;
};
/**
 * @description returns an image avatar to use for a user for a given size.
 * Defaults to an illustration based on gender if none available
 *
 * @name User.format#photoSrc
 * @param {User} user
 * @param {(string|object)=} [size=thumb] - photo size OR transform options
 * @param {boolean=} [defaultAvatar=false] - if true and no photo exists, will return an avatar
 * @returns {string} url of the image src
 */
exports.photoSrc = function (user, size, defaultAvatar) {
    var picture = user.picture || {};
    size = size || 'thumb';
    if (typeof size === 'string' && picture[size]) {
        return picture[size];
    }
    if (picture.hasPhoto) {
        return commonCloudinary.generateSrc(user.userID, picture.service, 'profile', size);
    }
    if (defaultAvatar) {
        return common.defaultAvatar(user.details && user.details.gender || 'm');
    }
    return '';
};
//# sourceMappingURL=user.js.map