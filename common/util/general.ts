/// <reference path="../../typings/chatterplot/Chatterplot.d.ts" />

"use strict";

var _:_.LoDashStatic = require('lodash-node/modern'),
    moment:moment.MomentStatic = require('moment');

exports.format = {
    dateString: function (format, time) {
        if (time instanceof Date) {
            time = moment(time);
        } else {
            time = time || moment();
        }
        switch (format) {
            case 'basic_date_time':
                return time.format('YYYYMMDDTHHmmss.SSSZ');
            case 'basic_date_time_no_millis':
                return time.format('YYYYMMDDTHHmmssZ');
        }
    }
};

exports.string = {
    isEmail: function (email) {
        return /^[_a-z0-9-+]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/i.test(email);
    },

    base64Encode: function (string) {
        return (new Buffer(string)).toString('base64');
    },

    base64Decode: function (string) {
        return (new Buffer(string, 'base64')).toString('ascii');
    },

    /**
     * @description converts an array of strings to a list separating the last item with an AND
     *
     * @param {array<string>} strs
     * @returns {string} a single formatted string
     */
    strArray2AndList: function (strs, connector) {
        var len = strs.length,
            list = '',
            end = '';

        if (len === 1) {
            list = strs[0];
        } else if (len > 1) {
            end = ` ${connector} ${strs[len - 1]}`; // produces something like ` and english`
            list = strs.slice(0, -1).join(', ') + end; // joins everything but the last item in the array
        }

        return list;
    },
};

exports.array = {
    remove: function (array, what) {
        var index = _.indexOf(array, what);
        if (index > -1) {
            return array.splice(index, 1);
        }
    }
};
