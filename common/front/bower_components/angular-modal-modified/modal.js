/*
 * @license
 * angular-modal v0.4.0
 * (c) 2013 Brian Ford http://briantford.com
 * License: MIT
 */

'use strict';

angular.module('btford.modal', []).
factory('btfModal', ['$animate', '$compile', '$rootScope', '$controller', '$q', '$http', '$templateCache', function ($animate, $compile, $rootScope, $controller, $q, $http, $templateCache) {
    return function modalFactory (config) {
        if (!(!config.template ^ !config.templateUrl)) {
            throw new Error('Expected modal to have exactly one of either `template` or `templateUrl`');
        }

        var template        = config.template,
            controller      = config.controller || angular.noop,
            controllerAs    = config.controllerAs,
            container       = angular.element(config.container || document.body),
            element         = null,
            newScope        = true,
            html,
            scope;

        if (template) {
            var deferred = $q.defer();
            deferred.resolve(template);
            html = deferred.promise;
        } else {
            html = $http.get(config.templateUrl, {
                cache: $templateCache
            }).then(function (response) {
                return response.data;
            });
        }

        function activate (locals, scope, force) {
            return html.then(function (html) {
                if (!element) {
                    return attach(html, locals, scope);
                }
                if (force) {
                    return deactivate().then(function () {
                        return attach(html, locals, scope);
                    });
                }
            });
        }

        function attach (html, locals, _scope) {
            element = angular.element(html)

            if (!element.length) {
                throw new Error('The template contains no elements; you need to wrap text nodes')
            }

            if (_scope) {
                newScope = false;
            }

            scope = _scope || $rootScope.$new();

            if (newScope && locals) {
                for (var prop in locals) {
                    scope[prop] = locals[prop];
                }
            }

            if (controller !== angular.noop) {
                var ctrl = $controller(controller, {$scope: scope});
                if (controllerAs) {
                    scope[controllerAs] = ctrl;
                }
            }

            var $elem = $compile(element)(scope);

            setTimeout(function () {
                $animate.enter($elem, (container || angular.element(config.container)));
            }, 0);
        }

        function deactivate () {
            var deferred = $q.defer();
            if (element) {
                $animate.leave(element).then(function () {
                    element = null;
                    deferred.resolve();

                    // Add custom conditional to remove reference to container elem's which might not exist on page change
                    if (config.container) {
                      container = null;
                    }

                    // Move this below the resolve because otherwise in some cases
                    // we run into an infinite loop where the destroy $method triggers
                    // this deactivate method again, and it screws up the next modal that activates
                    if (newScope) {
                        scope.$destroy();
                    }
                });
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        }

        function active () {
            return !!element;
        }

        return {
            activate: activate,
            deactivate: deactivate,
            active: active
        };
    };
}]);