/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

"use strict";

module.exports = {
    tasks: [{
        // Name
        name: 'name',
        key: "profile.ADD_NAME",
        weight: 4,
        expr: function (user) {
            return user.firstName || user.lastName;
        },
        trigger: function (profile) {
            return profile.editSection('basic');
        }
    }, {
        // Desired meeting locations
        name: 'desiredLocation',
        key: "profile.ADD_DESIRED_LOC",
        weight: 6,
        expr: function (user) {
            var locations = user.locations,
                length;
            return _.isArray(locations) && (length = locations.length) && (length > 1 || locations[0].type !== 'temp');
        },
        trigger: function (profile) {
            return profile.editSection('desiredLocation');
        }
    }, {
        // Native Language
        name: 'native',
        key: "profile.ADD_NATIVE_LANG",
        weight: 2,
        expr: function (user) {
            return _.isArray(user.languages) && _.some(user.languages,
                    function (lang) {

                        // Check that they're at least C2 level, since in some untranslated langs

                        // the translations aren't clear
                        return lang.level >= 6;
                    });
        },
        trigger: function (profile) {
            return profile.editSection('languages');
        }
    }, {
        // Profile Photo
        name: 'profilePhoto',
        key: "profile.UPLOAD_PROFILE",
        weight: 6,
        expr: function (user) {
            return !!(user.picture && (user.picture["default"] ||
            user.picture.hasPhoto));
        },
        trigger: function (profile) {
            return profile.uploadPhoto();
        }
    }, {
        // Personal Info 
        name: 'personal',
        key: "profile.ADD_PERSONAL",
        weight: 2,
        expr: function (user) {
            return !!user.personal;
        },
        trigger: function (profile) {
            return profile.editSection('personal');
        }
    }]
};
