/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
var link = {
    pre: function (scope) {
        scope.vars = {
            unsub: null,
            fields: _.cloneDeep(scope.source)
        };
    },
    post: function (scope, Util) {
        scope.vars.unsubAll = function () {
            _.each(scope.vars.fields, function (group) {
                _.each(group, function (val, key) {
                    group[key] = !scope.vars.unsub;
                });
            });
            scope.$root.safeDigest(scope);
        };
        scope.vars.save = function () {
            var changes = Util.objectDifferences(scope.source, scope.vars.fields);
            if (!Object.keys(changes).length) {
                return false;
            }
            scope.onSave({ updates: changes });
        };
    }
};
exports.email = function (Util) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: CP.site.views + 'account/settings/directives/preferences/EmailTemplate.html',
        scope: {
            source: '=',
            onSave: '&'
        },
        link: {
            pre: link.pre,
            post: function (scope) { link.post(scope, Util); }
        }
    };
};
exports.requests = function (Util) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: CP.site.views + 'account/settings/directives/preferences/RequestTemplate.html',
        scope: {
            source: '=',
            onSave: '&'
        },
        link: {
            pre: link.pre,
            post: function (scope) { link.post(scope, Util); }
        }
    };
};
//# sourceMappingURL=preferencesDir.js.map