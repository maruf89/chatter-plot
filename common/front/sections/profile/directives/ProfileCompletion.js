/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var taskList = require('root/profile/config/CompletionTasks');
/**
 * @classdesc This service is used to open the profile completion nagger
 * @class
 */
var ProfileCompletion = (function () {
    function ProfileCompletion(_btfModal, _$rootScope, _$templateCache) {
        this._btfModal = _btfModal;
        this._$rootScope = _$rootScope;
        this._$templateCache = _$templateCache;
    }
    /**
     * @description Opens a profile completion modal
     *
     * @name ProfileCompletion#openCompletion
     * @param {Profile} profile - A profile user object
     * @param {boolean} force - whether to force the modal open (it could be open already)
     */
    ProfileCompletion.prototype.openCompletion = function (profile, force) {
        this.profile = profile;
        var userTasks = this.buildTasks(taskList.tasks), modal;
        if (!force && userTasks.total === userTasks.completed) {
            return this.deactivate();
        }
        this.modal = modal = this._btfModal({
            template: this._$templateCache.get('views/profile/modals/completion/Template.html'),
            container: '.Map'
        });
        this._onUpdateFn = this.onUpdate.bind(this, userTasks);
        profile.on('/updated', this._onUpdateFn);
        if (modal.active()) {
            return false;
        }
        return modal.activate({
            vars: {
                percentComplete: this.percentComplete,
                getWeight: this.getWeight,
                taskList: userTasks,
                doTask: this.doTask
            },
            modal: {
                deactivate: function () {
                    return modal.active() && modal.deactivate();
                }
            }
        });
    };
    /**
     * Builds & formats a userTasks object specific to the current user
     *
     * @name ProfileCompletion#buildTasks
     * @param  {object} tasks  the source tasks file which will be cloned and formatted
     * @returns {object}        a formatted userTasks object just for the current user
     */
    ProfileCompletion.prototype.buildTasks = function (tasks) {
        var userTasks = {
            total: 0,
            completed: 0,
            tasks: null
        }, Profile = this.profile;
        userTasks.tasks = _.reduce(_.cloneDeep(tasks), function (all, task) {
            if (task.completed = task.expr(Profile.user)) {
                if (task.ifMissing) {
                    return all;
                }
                userTasks.completed += task.weight;
            }
            userTasks.total += task.weight;
            all.push(task);
            return all;
        }, []);
        return userTasks;
    };
    /**
     * @description Any updates to the current profile user will trigger this
     *
     * @callback
     * @name ProfileCompletion#onUpdate
     * @param  {object} userTasks  reference to the current users tasks object
     */
    ProfileCompletion.prototype.onUpdate = function (userTasks) {
        userTasks.completed = 0;
        this._$rootScope.safeApply(function () {
            _.each(userTasks.tasks, function (task) {
                if (task.completed = task.expr(this.profile.user)) {
                    userTasks.completed += task.weight;
                }
            }, this);
            if (userTasks.completed === userTasks.total) {
                this.deactivate();
            }
        }.bind(this));
    };
    /**
     * @description Returns a percentage of how much of the profile has been completed
     *
     * @mixin
     * @name ProfileCompletion#percentComplete
     * @param {object} tasks - will first check a property unique to a single task, then the `userTasks` object
     * @param {(Number|Object)} total - out of what to test
     */
    ProfileCompletion.prototype.percentComplete = function (tasks, total) {
        return Math.ceil(((tasks.weight || tasks.completed) / (total || tasks.total)) * 100) + '%';
    };
    ProfileCompletion.prototype.getWeight = function (tasks, index) {
        return this.percentComplete(tasks.tasks[index], tasks.total);
    };
    ProfileCompletion.prototype.doTask = function (task) {
        return task.trigger(this.profile);
    };
    ProfileCompletion.prototype.deactivate = function () {
        this.modal && this.modal.deactivate();
        // Remove on update listener 
        this.profile && this._onUpdateFn && this.profile.removeListener('/updated', this._onUpdateFn);
        this.profile = null;
    };
    return ProfileCompletion;
})();
module.exports = ProfileCompletion;
//# sourceMappingURL=ProfileCompletion.js.map