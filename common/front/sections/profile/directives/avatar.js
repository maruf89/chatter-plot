/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var AvatarCtrl = (function () {
    function AvatarCtrl(_User, _Config, _$compile, _$scope, ProfilePhoto) {
        this._User = _User;
        this._Config = _Config;
        this._$compile = _$compile;
        this._$scope = _$scope;
        this.uploadPhoto = ProfilePhoto.activate.bind(ProfilePhoto);
        this.removePhoto = ProfilePhoto.remove.bind(ProfilePhoto);
        this['default'] = null;
    }
    AvatarCtrl.prototype.init = function (vars) {
        var mediaSize = this._$scope.$root.size.single, opts = vars.opts, user;
        this._user = user = vars.user || this._User.data;
        this._gender = user.details && user.details.gender;
        this._iElem = vars.iElem;
        if (!opts || !opts.width || !opts.height) {
            this.size = vars.size || 'default';
            this.dimens = vars.imageSizes[this.size][mediaSize];
        }
        else {
            this.size = 'custom';
            this.dimens = {
                width: opts.width,
                'width@2x': opts.width * 2,
                height: opts.height,
                'height@2x': opts.height * 2,
            };
        }
        this._updateValues();
    };
    AvatarCtrl.prototype._updateValues = function () {
        var user = this._user = this._$scope.user || this._User.data;
        this.photo = user.picture || {};
        this.cloudPhoto = this.photo.hasPhoto;
        this.hasPhoto = !!this.photo[this.size] || this.cloudPhoto;
        this.photoID = this.photo.id || user.userID;
    };
    AvatarCtrl.prototype.setAvatar = function () {
        this._updateValues();
        var src = this.photo[this.size];
        this.cloudPhoto = this._user.picture && this._user.picture.hasPhoto;
        this.hasPhoto = src || this.cloudPhoto;
        this["default"] = src || this._getDefaultFoto();
        this._buildAvatar();
    };
    AvatarCtrl.prototype.isSelf = function (checkID) {
        return this._user.userID === (checkID || this._User.userID);
    };
    AvatarCtrl.prototype._buildAvatar = function () {
        this._iElem.find('.img-wrapper').empty().append(this._compileAvatar());
    };
    AvatarCtrl.prototype._compileAvatar = function () {
        return this._$compile('<img ' +
            'width="{{::viewModel.opts.width || viewModel.dimens.width}}" ' +
            'height="{{::viewModel.opts.height || viewModel.dimens.height}}" ' +
            'cloud-img="{{::viewModel.photoID}}" ' +
            'data-service="{{::viewModel.photo.service}}" ' +
            'data-type="profile" ' +
            'data-opt-preset="{{::viewModel.size}}" ' +
            'data-opts="viewModel.opts" ' +
            'data-cond="viewModel.cloudPhoto" ' +
            'data-default="{{viewModel.default}}" ' +
            '/>')(this._$scope);
    };
    AvatarCtrl.prototype.updateGender = function (gender) {
        this._gender = gender;
    };
    AvatarCtrl.prototype._getDefaultFoto = function () {
        // If current user's avatar & they don't have a photo - show the ugly avatar
        return this._User.userID && this._user.userID === this._User.userID ?
            '/images/profile/no-photo-avatar.svg' : this._Config.profile.defaultAvatar(this._gender);
    };
    return AvatarCtrl;
})();
exports.AvatarCtrl = AvatarCtrl;
//# sourceMappingURL=avatar.js.map