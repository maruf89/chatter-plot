/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
var MapControlCtrl = (function () {
    function MapControlCtrl(_$scope, _$analytics, _Maps, _DataBus) {
        this._$scope = _$scope;
        this._$analytics = _$analytics;
        this._Maps = _Maps;
        this._DataBus = _DataBus;
        this.name = null;
        this.instance = _Maps.getInstance();
        _$scope.Control = this;
    }
    MapControlCtrl.prototype.zoom = function (amount) {
        var mapObject = this.instance.rootObject(), curZoom = mapObject.getZoom(), newZoom = curZoom + (amount === 'minus' ? -1 : 1);
        // Make sure to not trigger anything if zoomed to the max
        if (newZoom > mapObject.maxZoom) {
            newZoom = mapObject.maxZoom;
        }
        else if (newZoom < mapObject.minZoom) {
            newZoom = mapObject.minZoom;
        }
        if (newZoom === curZoom) {
            return true;
        }
        mapObject.setZoom(newZoom);
    };
    MapControlCtrl.prototype.locate = function () {
        var context = this;
        this._DataBus.emit('progressLoader', { start: true });
        return this.instance.getLocation()
            .then(function (coords) {
            context.instance.centerMap(coords, 15);
            context._DataBus.emit('progressLoader');
            context._$analytics.eventTrack('click', {
                category: 'maps',
                label: 'show current location'
            });
        });
    };
    MapControlCtrl.prototype.moveSearchInit = function () {
        var context = this;
        this.vars = {
            autoSearch: this._Maps.defaults.mapMoveSearch,
            mapMoved: null,
            enabled: true
        };
        this.name = 'moveSearch';
        this.instance.linkMethod(this.name, 'toggleButton', function (value) {
            context.vars.enabled = value === void 0 ? !context.vars.enabled : value;
            context._$scope.$root.safeDigest(context._$scope);
        });
        this.instance.data.mapMoveChecks.push(this._onMapMove.bind(this));
    };
    /**
     * A check to see if we should update the map
     *
     * @callback
     * @param  {object}     map       Map reference
     * @param  {Function}   callback  reference to the callback if we have to stop it
     * @return {Boolean}              Whether it's ok to continue
     */
    MapControlCtrl.prototype._onMapMove = function (map, callback) {
        if (!this.instance.currentSet.moveSearch || !this.vars.enabled) {
            return false;
        }
        if (this.vars.autoSearch) {
            return true;
        }
        else {
            this.vars.mapMoveCb = callback;
            this._$scope.$root.safeDigest(this._$scope);
            return false;
        }
    };
    MapControlCtrl.prototype.manualMapRefresh = function () {
        this.vars.mapMoveCb();
        this.vars.mapMoveCb = null;
    };
    return MapControlCtrl;
})();
exports.MapControlCtrl = MapControlCtrl;
//# sourceMappingURL=MapControlCtrl.js.map