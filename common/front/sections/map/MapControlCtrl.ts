/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

"use strict";

interface IMapControlScope extends cp.IScope {
    Control:any
}

export class MapControlCtrl {
    public name:string = null;
    public instance:any;
    public vars:any;

    constructor(
        private _$scope:IMapControlScope,
        private _$analytics:Angulartics.IAnalyticsService,
        private _Maps:cp.map.IService,
        private _DataBus:NodeJS.EventEmitter
    ) {
        this.instance = _Maps.getInstance();
        _$scope.Control = this;
    }

    public zoom(amount:string):void {
        var mapObject = this.instance.rootObject(),
            curZoom = mapObject.getZoom(),
            newZoom = curZoom + (amount === 'minus' ? -1 : 1);

        // Make sure to not trigger anything if zoomed to the max
        if (newZoom > mapObject.maxZoom) {
            newZoom = mapObject.maxZoom;
        } else if (newZoom < mapObject.minZoom) {
            newZoom = mapObject.minZoom;
        }

        if (newZoom === curZoom) {
            return true;
        }

        mapObject.setZoom(newZoom);
    }

    public locate():ng.IPromise<any> {
        var context = this;

        this._DataBus.emit('progressLoader', { start: true });

        return this.instance.getLocation()
            .then(function (coords) {
                context.instance.centerMap(coords, 15);
                context._DataBus.emit('progressLoader');
                context._$analytics.eventTrack('click', {
                    category: 'maps',
                    label: 'show current location'
                });
            });
    }

    protected moveSearchInit():void {
        var context = this;

        this.vars = {
            autoSearch: this._Maps.defaults.mapMoveSearch,
            mapMoved: null,
            enabled: true
        };

        this.name = 'moveSearch';

        this.instance.linkMethod(this.name, 'toggleButton', function (value) {
            context.vars.enabled = value === void 0 ? !context.vars.enabled : value;
            context._$scope.$root.safeDigest(context._$scope);
        });

        this.instance.data.mapMoveChecks.push(this._onMapMove.bind(this));
    }

    /**
     * A check to see if we should update the map
     *
     * @callback
     * @param  {object}     map       Map reference
     * @param  {Function}   callback  reference to the callback if we have to stop it
     * @return {Boolean}              Whether it's ok to continue
     */
    private _onMapMove(map:any, callback:Function):boolean {
        if (!this.instance.currentSet.moveSearch || !this.vars.enabled) {
            return false;
        }

        if (this.vars.autoSearch) {
            return true;
        } else {
            this.vars.mapMoveCb = callback;
            this._$scope.$root.safeDigest(this._$scope);
            return false;
        }
    }

    public manualMapRefresh():void {
        this.vars.mapMoveCb();
        this.vars.mapMoveCb = null;
    }
}
