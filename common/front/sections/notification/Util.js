/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var _mapSource = CP.Settings.NOTIFICATION, _map = {
    source: {},
    sourceTypes: {}
};
_.each(_mapSource.source, function (val, key) {
    _map.source[val] = key;
    _map.sourceTypes[val] = {};
});
_.each(_mapSource.sourceTypes, function (groups, parent) {
    var obj = _map.sourceTypes[_mapSource.source[parent]];
    _.each(groups, function (val, key) {
        obj[val] = key;
    });
});
/**
 * @description converts a notification object to a string
 *
 * @param {number} userID
 * @param {object} data
 * @param {string} data.source
 * @param {string} data.sourceType
 * @param {(string|number)} data.sourceID
 * @returns {string}
 */
function toNID(userID, data) {
    var source = _map.source[data.source], sourceType = _map.sourceTypes[data.source][data.sourceType];
    return userID + '-' + source + '_' + sourceType + '_' + data.sourceID;
}
exports.toNID = toNID;
;
//# sourceMappingURL=Util.js.map