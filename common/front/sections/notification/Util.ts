/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var _mapSource:cp.notification.ISettingsNOTIFICATION = CP.Settings.NOTIFICATION,

    _map:cp.notification.ISettingsMAP = {
        source: {},
        sourceTypes: {}
    };

_.each(_mapSource.source, function (val:string, key:any):void {
    _map.source[val] = key;
    _map.sourceTypes[val] = {};
});

_.each(_mapSource.sourceTypes, function (groups:cp.notification.ISettingsMAPSourceTypes, parent:any):void {
    var obj:cp.notification.ISettingsMAPSourceType = _map.sourceTypes[_mapSource.source[parent]];
    _.each(groups, function (val:string, key:any):void {
        obj[val] = key;
    });
});

/**
 * @description converts a notification object to a string
 *
 * @param {number} userID
 * @param {object} data
 * @param {string} data.source
 * @param {string} data.sourceType
 * @param {(string|number)} data.sourceID
 * @returns {string}
 */
export function toNID(userID:number, data:cp.notification.INotification):string {
    var source:number = _map.source[data.source],
        sourceType:number = _map.sourceTypes[data.source][data.sourceType];

    return userID + '-' + source + '_' + sourceType + '_' + data.sourceID;
};
