/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var EventEmitter_1 = require('common/front/modules/EventEmitter');
var Util_1 = require('root/notification/Util');
var moment = require('moment'), 
/**
 * @name _hasNotifications
 * @description These variables will be initiated on auth change
 * @var {boolean} _hasNotifications - Will store whether we have already loaded notifications
 */
_hasNotifications = false, 
/**
 * @name notifications
 * @var {object} notifications
 * @var {object} notifications.all - Will hold all of the notifications
 * @var {number} notifications.total - the total # of notifications this user has
 * @var {number} notifications.loaded - you can guess what this means
 * @var {array}  notifications.unread - Array of unread notifications
 */
notifications = null, 
/**
 * @name _types
 * @description If the translation is one of these, than we need to add additional parameters
 * @var {object<function>} each key corresponds to a notification type.
 */
_types = {
    'LISTING': {
        source: null,
        icon: 'note',
        link: function ($state, notification) {
            return $state.href('activity.listings.single', {
                eID: notification.sourceID
            });
        }
    },
    'EVENT': {
        source: null,
        icon: 'calendar',
        link: function ($state, notification) {
            return $state.href('activity.events.single', {
                eID: notification.sourceID
            });
        }
    },
    'INTERACTIONS': {
        source: null,
        icon: function (notification) {
            return {
                1: 'mail',
                2: 'tandem',
                3: 'teacher',
                4: 'tandem',
                5: 'cancel-big',
                6: 'tandem',
                7: 'cancel-big',
                8: 'tandem',
                9: 'teacher',
            }[notification.sourceType];
        }
    },
    // TODO Build the section out
    'USER': {
        source: null,
        icon: 'tandem',
        link: function ($state, notification) {
            return $state.href('pStandalone.messages.single', {
                msgID: notification.sourceID
            });
        }
    }
}, 
/**
 * @method
 * @param {object} User - UserService
 * @param {object} notification - a single notification object
 * @returns {string} a translated value for this notification type
 */
_interpolateName = function (User, notification) {
    var user = _users[notification.from];
    if (!user.fullName) {
        user.fullName = User.format.name(user, '');
    }
    return {
        person: "<strong>" + user.fullName + "</strong>"
    };
}, _linkRequest = function ($state, notification) {
    return $state.href('dashboard.myActivities', {
        action: 'type:request|action:view|objID:' + notification.sourceID
    });
}, _interactionObj = {
    translate: _interpolateName,
    link: _linkRequest
}, _sourceTypes = {
    'INTERACTIONS-MESSAGE': {
        translate: _interpolateName,
        link: function ($state, notification) {
            return $state.href('pStandalone.messages.single', {
                msgID: notification.sourceID
            });
        }
    },
    'INTERACTIONS-REQUEST_TANDEM': _interactionObj,
    'INTERACTIONS-REQUEST_TANDEM_DENY': _interactionObj,
    'INTERACTIONS-REQUEST_TANDEM_ACCEPT': _interactionObj,
    'INTERACTIONS-REQUEST_TANDEM_CANCEL': _interactionObj,
    'INTERACTIONS-REQUEST_TEACHER': _interactionObj,
    'INTERACTIONS-REQUEST_TEACHER_DENY': _interactionObj,
    'INTERACTIONS-REQUEST_TEACHER_ACCEPT': _interactionObj,
    'INTERACTIONS-REQUEST_TEACHER_CANCEL': _interactionObj,
    'INTERACTIONS-REQUEST_TANDEM_REMINDER': _interactionObj,
    'INTERACTIONS-REQUEST_TEACHER_REMINDER': _interactionObj,
}, _readyListeners = [], _map = CP.Settings.NOTIFICATION, 
/**
 * Shared data reference to `skimUsers`, stored on DataBus
 */
_users;
_.defer(function () {
    _.each(_map.source, function (key, id) {
        _types[key].source = id;
    });
});
/**
 * @ngdoc class
 * @class
 * @classdesc This class fetches/listens to notifications from the backend and
 * organizes them into read/unread for other modules to use
 */
var Notifications = (function (_super) {
    __extends(Notifications, _super);
    function Notifications(DataBus, _$q, _SocketIo, _locale, _User, _Config, _$state) {
        _super.call(this);
        this._$q = _$q;
        this._SocketIo = _SocketIo;
        this._locale = _locale;
        this._User = _User;
        this._Config = _Config;
        this._$state = _$state;
        /**
         * @member {User}
         */
        _users = _User.users;
        _locale.ready('notifications');
        _locale.ready('activities');
        DataBus.on('/site/authChange', this._onAuthChange.bind(this));
        DataBus.on('/notification/viewed', function (notification) {
            this.markAsRead([Util_1.toNID(_User.userID, notification)]);
        }.bind(this));
        // if authenticated will store reference to the unbind method
        this._notifListener = null;
    }
    Notifications.prototype.isReady = function () {
        return _hasNotifications;
    };
    /**
     * @ngdoc method
     * @method
     * @name Notifications#_onReady
     * @description returns a resolved promise after the first set of notifications
     * has returned with a reference to the notifications object
     *
     * @fires Notifications#emit('/ready', {@link notifications}) if first time
     * @returns {Promise<object>} returns {@link notifications}
     */
    Notifications.prototype._onReady = function () {
        var deferred;
        if (_hasNotifications) {
            if (_readyListeners.length) {
                this.emit('/ready', notifications);
                _.each(_readyListeners, function (deferred) {
                    return deferred.resolve(notifications);
                });
                _readyListeners.length = 0;
            }
            return this._$q.when(notifications);
        }
        else {
            deferred = this._$q.defer();
            _readyListeners.push(deferred);
            return deferred.promise;
        }
    };
    /**
     * @ngdoc method
     * @method
     * @name Notifications#fetch
     * @description fetches the desired number notifications from the backend
     * for this user
     *
     * @param {object=} opts - additional options to search
     * @param {number} opts.from - get notification offset
     * @param {number} opts.size - how many notifications to ask for
     * @param {boolean=} force - whether to force a new request instead of checking the cache (default:false)
     * @returns {Promise<object>} returns {@link notifications}
     */
    Notifications.prototype.fetch = function (opts, force) {
        opts = opts || {};
        var doLoadMore, length;
        // Here we check that we're not asking for any more
        // notifications than we already have. If so toggle load more
        if (_hasNotifications &&
            opts.size &&
            (length = notifications.loaded) &&
            (opts.size > length || (opts.from && opts.size + opts.from > length))) {
            doLoadMore = true;
        }
        if (!_hasNotifications || doLoadMore || force) {
            // If property not set, assume we want it
            if (!opts.hasOwnProperty('withUsers')) {
                opts.withUsers = true;
            }
            return this._SocketIo.onEmitSock('/notification/fetch', opts)
                .then(this._prepareUnmap.bind(this))
                .then(this._unmap.bind(this));
        }
        return this._onReady();
    };
    /**
     * @ngdoc method
     * @method
     * @description searches through all of the incoming notifications for users that
     * we don't have data for and requests their data
     * @name Notifications#_prepareUnmap
     * @param {object} data
     * @param {array<object>} data.notifications - array of all of the notifications
     * @param {number} data.total - number of notifications for this user this has
     * @param {object} data.users - a {<userID>: { firstName, lastName }} map of users
     * @return {Promise}
     */
    Notifications.prototype._prepareUnmap = function (data) {
        var notifs = data.notifications, usersToFetch = [];
        if (data.total) {
            notifications.total = data.total;
        }
        if (typeof data.users === 'object') {
            _.extend(_users, data.users);
        }
        // Iterate over the notifications to see if we're missing any user data
        _.each(notifs, function (note) {
            if (note.from && !_users[note.from]) {
                return usersToFetch.push(note.from);
            }
        });
        // We are missing users, fetch them
        if (usersToFetch.length) {
            usersToFetch = _.uniq(usersToFetch);
            return this._User.get({
                userIDs: usersToFetch,
                _source: [
                    'firstName',
                    'lastName',
                    'picture',
                    'userID'
                ]
            })
                .then(function (newUsers) {
                this._User.addPeople(newUsers, usersToFetch, true);
                return notifs;
            }.bind(this));
        }
        // We have all the users, continue...
        return this._$q.when(notifs);
    };
    /**
     * @description Assembles/combines the notifications. Calculates `unread`, creates a sorted array.
     * @name Notifications#unmap
     * @param {array<object>} notifs - array of notifications to be unmaped
     * @returns {object} notifications object
     */
    Notifications.prototype._unmap = function (notifs) {
        var allNotifs = notifications.all;
        _.each(notifs, function (notif) {
            var nID = notif.nID;
            if (!notif.read) {
                notifications.unread.push(nID);
            }
            if (!allNotifs[nID]) {
                // If the notification doesn't exist yet increment the # of loaded
                notifications.loaded++;
            }
            allNotifs[nID] = this._unmapSingle(notif);
        }, this);
        notifications.unread = _.uniq(notifications.unread);
        // Turn an object of notifications into a sorted array (newest -> oldest)
        notifications.sorted = _.map(Object.keys(allNotifs), function (nID) {
            return allNotifs[nID];
        }).sort(function (a, b) {
            return a.when < b.when;
        });
        _hasNotifications = true;
        this.emit('/updated', notifications);
        return notifications;
    };
    Notifications.prototype._unmapSingle = function (notification) {
        // if type is LISTING, copy the same event values over
        var source = _map.source[notification.source] === 'EVENTS.LISTING' ?
            _types['EVENTS.EVENT'].source
            :
                notification.source, 
        // Generate a key for this type of notification
        // Will look something like 'INTERACTIONS-REQUEST_TANDEM'
        sourceTypeKey = _map.source[source].replace('.', '_') +
            '-' + _map.sourceTypes[source][notification.sourceType || 1], 
        // if exists
        sourceType = _sourceTypes[sourceTypeKey], 
        // Generate any extra data needed for the translation key interpolation
        dataKey = sourceType && typeof sourceType.translate === 'function' ?
            sourceType.translate(this._User, notification) : null, 
        // Check if there's a unique link method for different source types
        linkFn = sourceType && typeof sourceType.link === 'function' ?
            sourceType.link : _types[_map.source[notification.source]].link;
        // Translate the string
        notification.translate = this._locale.getString('notifications.' + sourceTypeKey, dataKey);
        // Get the correct icon
        notification.icon = _types[_map.source[notification.source]].icon;
        if (typeof notification.icon === 'function') {
            notification.icon = notification.icon(notification);
        }
        // Link to the correct page
        notification.link = linkFn(this._$state, notification);
        // Temporary overflow fix until the new notification designs are built
        notification.snippet = notification.snippet.substr(0, 15);
        // So we don't get any duplicate errors
        notification.when = notification.when || notification.nID;
        return notification;
    };
    Notifications.prototype.markAsRead = function (nIDs, force) {
        /**
         * @name confirmedIDs
         * @var {array<string>}
         * @description array of nIDs to be marked as read
         */
        var confirmedIDs = [], deferred = this._$q.defer(), 
        /**
         * @name markUserNotifications
         * @var {boolean}
         * @description whether to update the user object with a new notification (turn the blue box => red)
         */
        markUserNotifications = false, timestamp = this._Config.format.date.toString('basic_date_time_no_millis', [timestamp || moment()]);
        _.each(nIDs, function (nID) {
            // Check if the nID is not in the unread array
            var index;
            if (!force &&
                (index = notifications.unread.indexOf(nID)) === -1) {
                return;
            }
            notifications.all[nID].read = timestamp;
            // If we got this far than mark to continue and remove the unread notification
            confirmedIDs.push(nID);
            notifications.unread.splice(index, 1);
        });
        // If nothing to update, quit early
        if (!confirmedIDs.length) {
            return this._$q.when(false);
        }
        if (!notifications.unread.length) {
            markUserNotifications = true;
            this._User.data.unreadNotification = false;
        }
        // Emit the {@link confirmedIDs}
        this.emit('/mark/read', confirmedIDs);
        this._SocketIo.onEmitSock('/notification/mark', {
            mark: 'read',
            nIDs: confirmedIDs,
            value: timestamp,
            userNotifications: markUserNotifications
        })
            .then(deferred.resolve, function (err) {
            // in case of error, put back everything to the way it was
            _.each(confirmedIDs, function (nID) {
                notifications.unread.push(nID);
                delete notifications.all[nID].read;
            });
            throw err;
        });
        return deferred.promise;
    };
    /**
     * @ngdoc method
     * @method
     * @name Notifications#nID2ObjArr
     * @description converts an array of notification IDs to an array of notification objects
     *
     * @param {array<string>} nIDs - containing notification IDs
     * @returns {array<object>} returns an array of notification objects
     */
    Notifications.prototype.nID2ObjArr = function (nIDs) {
        return _.map(nIDs, function (nID) {
            return notifications.all[nID];
        });
    };
    /**
     * @description Processes a new notification for the logged in user and broadcasts it
     *
     * @callback
     * @param a
     * @param data
     * @private
     */
    Notifications.prototype._onNotification = function (a, data) {
        this._prepareUnmap(data)
            .then(this._unmap.bind(this))
            .then(function () {
            this._User.data.unreadNotification = true;
            this.emit('/new', data.notifications);
        }.bind(this));
    };
    Notifications.prototype._onAuthChange = function (authenticated) {
        _hasNotifications = false;
        notifications = {
            all: {},
            unread: [],
            sorted: [],
            total: 0,
            loaded: 0,
            allLoaded: function () {
                return notifications.loaded && notifications.loaded === notifications.total;
            }
        };
        if (authenticated) {
            this._notifListener = this._SocketIo.onSock('/notification', this._onNotification.bind(this), null, true);
        }
        else if (this._notifListener) {
            this._notifListener();
        }
    };
    Notifications.prototype.notificationsViewed = function () {
        this._User.data.unreadNotification = false;
        return this._SocketIo.onEmitSock('/notification/viewed');
    };
    return Notifications;
})(EventEmitter_1.EventEmitter);
exports.Notifications = Notifications;
//# sourceMappingURL=Service.js.map