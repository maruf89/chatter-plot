/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
/**
 * TODO: Build in a memoize option to improve performance
 */
module.exports = function () {
    return function (items, field, reverse) {
        var filtered = [];
        _.each(items, function (item) {
            return filtered.push(item);
        });
        filtered.sort(function (a, b) {
            if (a[field] > b[field]) {
                return 1;
            }
            else {
                return -1;
            }
        });
        if (reverse) {
            filtered.reverse();
        }
        return filtered;
    };
};
//# sourceMappingURL=orderObjectBy.js.map