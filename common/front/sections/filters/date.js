/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
module.exports = function () {
    return {
        DOB: function (year, month, day, validate) {
            var date, flag, today;
            if (!month || !day || !year) {
                return false;
            }
            month = +month;
            day = +day;
            year = +year;
            date = new Date();
            year = year > (date.getYear() - 100) ? +("19" + year) :
                +("20" + year);
            flag = true;
            if (month < 1 || month > 12) {
                flag = false;
            }
            if (day < 1 || day > 31) {
                flag = false;
            }
            if ((month === 4 || month === 6 || month === 9 || month ===
                11) && (day >= 31)) {
                flag = false;
            }
            if (month === 2) {
                if (year % 4 !== 0) {
                    if (day > 28) {
                        flag = false;
                    }
                }
                if (year % 4 === 0) {
                    if (day > 29) {
                        flag = false;
                    }
                }
            }
            date.setFullYear(year, month - 1, day);
            today = new Date();
            if (date > today) {
                flag = false;
            }
            if (validate) {
                return flag;
            }
            return year + "/" + month + "/" + day;
        }
    };
};
//# sourceMappingURL=date.js.map