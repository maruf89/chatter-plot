/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
module.exports = function () {
    return function (input, exclude) {
        if (exclude == null) {
            exclude = [];
        }
        if (typeof exclude === 'string') {
            exclude = [exclude];
        }
        return _.difference(input, exclude);
    };
};
//# sourceMappingURL=exclude.js.map