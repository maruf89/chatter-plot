/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

"use strict";

/**
 * @ngdoc filter
 * @module trustFilter
 * Do not sanitize html filter
 */
module.exports = function ($sce) {
    return $sce.trustAsHtml;
};
