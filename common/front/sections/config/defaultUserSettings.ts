/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

var exports = {
        map: {
            unit: {
                "1": 'M',
                "2": 'FT'
            },
            formats: {
                date: {
                    "1": 'DD-MM-YYYY',
                    "2": 'MM-DD-YYYY',
                    "3": 'YYYY-MM-DD'
                },
                time: {
                    "1": '24',
                    "2": '12'
                }
            }
        },
        mapLocale: {
            "en-US": {
                unit: 2,
                formats: {
                    date: 2,
                    hours: 2
                }
            },
            "en-GB": {
                unit: 1,
                formats: {
                    date: 1,
                    hours: 1
                }
            }
        },
        settings: {
            format: {
                date: null,
                time: null
            },
            timeZone: null,
            requests: {
                tandem: true,
                teacher: true
            },
            notifs: {
                email: {
                    eventsILearn: true,
                    eventsISpeak: true,
                    eventsAllLang: true,
                    updates: true,
                    newMsg: true,
                    requestTandem: true,
                    requestTeacher: true
                },
                messaging: {
                    acceptAll: true,
                    acceptFollowing: true
                }
            }
        }
    },

locale = exports.mapLocale;

locale.en = locale["en-US"];

locale["de-DE"] = locale.de = locale["en-GB"];
locale["es-ES"] = locale.es = locale["en-GB"];
locale["lt-LT"] = locale.lt = locale["en-GB"];
locale["ru-RU"] = locale.ru = locale["en-GB"];
locale["pl-PL"] = locale.pl = locale["en-GB"];
locale["no-NO"] = locale.no = locale["en-GB"];
locale["it-IT"] = locale.it = locale["en-GB"];
locale["fr-FR"] = locale.fr = locale["en-GB"];
locale["eo-XX"] = locale.eo = locale["en-GB"];
locale["zh-CN"] = locale.zh = locale["en-GB"];
locale["zh-TW"] = locale.tw = locale["en-GB"];

module.exports = exports;
