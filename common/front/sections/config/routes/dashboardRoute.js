/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
var commonUser = require('common/type/user');
"use strict";
module.exports = function (routes) {
    var map = routes.routes;
    return {
        name: 'dashboard',
        url: '',
        abstract: true,
        views: {
            "site": {
                templateProvider: [
                    '$templateCache',
                    function ($templateCache) {
                        return $templateCache.get('views/dashboard/Template.html');
                    }
                ],
                controller: 'MapCtrl as Map',
            }
        },
        // role: [] - We do not add this because the profile needs to be visible to non logged-in users
        children: [{
                name: 'myActivities',
                url: '/my-activities?action&state',
                roles: ['SELF_EDIT'],
                views: {
                    "dashboard@dashboard": {
                        templateProvider: ['$templateCache', function ($templateCache) {
                                return $templateCache.get('views/dashboard/myActivities/Template.html');
                            }],
                        controller: 'MyActivitiesCtrl as My',
                    }
                },
                data: {
                    tabClass: 'my-activities'
                }
            }, {
                name: 'profile',
                url: '/profile',
                views: {
                    "dashboard@dashboard": {
                        templateProvider: ['$templateCache', function ($templateCache) {
                                return $templateCache.get('views/profile/View.html');
                            }],
                    }
                },
                data: {
                    tabClass: 'profile'
                },
                children: [{
                        name: 'user',
                        url: '/user/{userID}?action',
                        views: {
                            "profile@dashboard.profile": {
                                templateProvider: [
                                    '$templateCache',
                                    function ($templateCache) {
                                        return $templateCache.get('views/profile/Template.html');
                                    }
                                ],
                                controller: 'ProfileCtrl as Profile',
                            }
                        },
                        resolve: {
                            CurUser: [
                                '$stateParams', 'User', '$state',
                                function ($stateParams, User, $state) {
                                    var userID = p($stateParams.userID);
                                    if ($stateParams.userID === 'me' && !User.userID) {
                                        throw $state.go('account.auth', { page: 'login' });
                                    }
                                    if ($stateParams.userID === 'me' && User.userID) {
                                        _.defer(function () {
                                            $state.go('dashboard.profile.user', { userID: User.userID }, {
                                                notify: false,
                                                reload: true
                                            });
                                        });
                                        userID = User.userID;
                                    }
                                    if (userID === User.userID) {
                                        return User.data;
                                    }
                                    return User.cacheGet([userID], commonUser.insecureFields).then(function (res) {
                                        return res[userID];
                                    });
                                }
                            ]
                        }
                    }]
            }, {
                name: 'eventCreate',
                url: '/create?eID&fromStore&hide&type',
                roles: ['CREATE_EVENT'],
                views: {
                    "dashboard@dashboard": {
                        templateProvider: ['$templateCache', function ($templateCache) {
                                return $templateCache.get('views/event/create/Template.html');
                            }],
                        controller: 'EventCreateCtrl as Create',
                    }
                },
                data: {
                    tabClass: 'classes'
                },
                children: [{
                        name: 'basicInfo',
                        url: '/basic-info',
                        views: {
                            "eventCreate@dashboard.eventCreate": {
                                templateProvider: ['$templateCache', function ($templateCache) {
                                        return $templateCache.get('views/event/create/basicInfo/Template.html');
                                    }]
                            }
                        },
                    }, {
                        name: 'languages',
                        url: '/languages',
                        views: {
                            "eventCreate@dashboard.eventCreate": {
                                templateProvider: ['$templateCache', function ($templateCache) {
                                        return $templateCache.get('views/event/create/languages/Template.html');
                                    }]
                            }
                        },
                    }, {
                        name: 'details',
                        url: '/details',
                        views: {
                            "eventCreate@dashboard.eventCreate": {
                                templateProvider: ['$templateCache', function ($templateCache) {
                                        return $templateCache.get('views/event/create/details/Template.html');
                                    }]
                            }
                        },
                    }],
                resolve: {
                    ClassLearn: [
                        'NewFeature',
                        function (NewFeature) {
                            var campaign = 'teacherCampaign';
                            if (!NewFeature.prototype.check(false, campaign)) {
                                return {
                                    module: NewFeature.prototype.getModule(campaign),
                                    name: campaign,
                                };
                            }
                            return false;
                        }
                    ],
                    EditEvent: [
                        '$stateParams', 'Events',
                        function ($stateParams, Events) {
                            var type;
                            if ($stateParams.eID) {
                                type = $stateParams.type || 'event';
                                return Events.get({
                                    evTypes: [{
                                            eID: [$stateParams.eID],
                                            type: type,
                                        }],
                                    description: true,
                                    venues: true,
                                })
                                    .then(function (event) {
                                    return event[0];
                                }, function (notFound) {
                                    throw notFound;
                                });
                            }
                            else {
                                return false;
                            }
                        }
                    ]
                }
            }, {
                name: 'favorites',
                url: '/favorites',
                roles: ['SELF_VIEW'],
                views: {
                    "dashboard@dashboard": {
                        templateProvider: ['$templateCache', function ($templateCache) {
                                return $templateCache.get('views/dashboard/favorite/Template.html');
                            }],
                        controller: 'FavoriteCtrl as Favorite',
                    }
                },
                data: {
                    tabClass: 'fave-tab'
                }
            }]
    };
};
//# sourceMappingURL=dashboardRoute.js.map