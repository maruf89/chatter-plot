/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

"use strict";

module.exports = function (routes) {
    var map = routes.routes;

    return {
        name: 'account',
        url: '',
        abstract: true,
        views: {
            "site": {
                template: '<div class="height-100" ui-view="account"></div>',
            }
        },
        children: [{
            name: 'auth',
            url: '/{page:(?:login|signup|password-reset|email-signup)}?redirect',
            loggedinRedirect: ['dashboard.profile.user', { userID: "me" }],
            views: {
                "account@account": {
                    templateProvider: ['$templateCache', function ($templateCache) {
                        return $templateCache.get('views/account/auth/Template.html');
                    }],
                    controller: ['$scope', '$stateParams', 'AuthenticateModal',
                        function ($scope:cp.IScope, $stateParams:ng.ui.IStateParamsService, AuthenticateModal:any) {
                            AuthenticateModal.activate(
                                $stateParams.page, {
                                    dedicated: true,
                                    modalClass: 'auth-page'
                                });

                            var pageClass = 'page-auth',
                                $root = $scope.$root;

                            $root.addPageClass(pageClass);

                            $scope.$on('$destroy', function () {
                                _.defer(function () {
                                    AuthenticateModal.deactivate();
                                    $root.removePageClass(pageClass);
                                });
                            });
                        }
                    ],
                }
            },
            resolve: {
                current: [
                    '$stateParams', 'AuthenticateModal',
                    function ($stateParams, AuthenticateModal) {

                        return true;
                    }
                ]
            }
        }, {
            name: 'deauth',
            url: '/logout',
            views: {
                "account@account": {
                    template: '<div></div>',
                    controller: ['Auth', 'DataBus', '$state', 'Config', function (Auth, DataBus, $state, Config) {
                        DataBus.once('/site/unauthenticated/ready', function () {
                            $state.go(Config.routes.onLogout);
                        });

                        Auth.deauthenticate();
                    }]
                }
            },
        }]
    };
};
