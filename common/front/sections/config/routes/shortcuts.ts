/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

"use strict";

var EventID = 8001 /* <-- CHANGE THIS Number to edit */,

    _ = require('lodash-node/modern');


module.exports = function (routes) {
    var map = routes.routes,
        eventData = require('root/config/routes/activityRoute')(routes)._data.eventData,
        SLEChild = _.cloneDeep(eventData.children[0]),
        SLEChild2 = _.cloneDeep(eventData.children[0]),
        SLEChild3 = _.cloneDeep(eventData.children[0]),
        SLEChild4 = _.cloneDeep(eventData.children[0]),
        SLEChild5 = _.cloneDeep(eventData.children[0]);

    SLEChild.url = '/savannah-language-exchange';
    SLEChild.name = 'SLE1';
    SLEChild2.url = '/SLX';
    SLEChild2.name = 'SLE2';
    SLEChild3.url = '/slx';
    SLEChild3.name = 'SLE3';
    SLEChild4.url = '/SLE';
    SLEChild4.name = 'SLE4';
    SLEChild5.url = '/sle';
    SLEChild5.name = 'SLE5';

    return {
        name: 'shortcutMap',
        url: '',
        abstract: true,
        templateProvider: ['$templateCache', function ($templateCache) {
            return $templateCache.get('views/map/Template.html');
        }],
        controller: 'MapCtrl as Map',
        children: [{
            name: 'SLE',
            url: '',
            abstract: true,
            template: eventData.template,
            data: {
                type: 'event',
                eID: EventID,
            },
            children: [
                SLEChild,
                SLEChild2,
                SLEChild3,
                SLEChild4,
                SLEChild5,
            ]
        }]
    };
};