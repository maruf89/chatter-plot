/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
var _event = {
    views: {
        "map@activity": {
            templateProvider: ['$templateCache', function ($templateCache) {
                    return $templateCache.get('views/event/View.html');
                }],
        }
    },
    children: [{
            name: 'single',
            url: '/{eID:[0-9]+}?action',
            views: {
                "event": {
                    controller: 'EventSingleCtrl as Event',
                    templateProvider: ['$templateCache', function ($templateCache) {
                            return $templateCache.get('views/event/single/Template.html');
                        }],
                }
            },
            resolve: {
                Datum: ['$state', 'User', 'DataBus', '$q', 'Events',
                    function ($state, User, DataBus, $q, Events) {
                        var event = null, deferred = $q.defer(), getParams = {
                            evTypes: [{
                                    type: $state.next.data.type,
                                    eID: $state.toParams.eID || $state.next.data.eID,
                                }],
                            venues: true,
                            host: $state.next.data.type === 'event',
                            description: true,
                        };
                        Events.get(getParams)
                            .then(function (_event) {
                            return event = _event[0];
                        })["finally"](function () {
                            // Must wait til the site's ready to see if the user can see this event
                            // depending on whether it's published and/or it's the user's own unpublished event 
                            var onReady = function () {
                                if (!event) {
                                    return deferred.reject(new Error('Not Found'));
                                }
                                else if (!event || (!event.published && event.creator !== User.userID)) {
                                    return deferred.reject(new Error('Page is not accessible'));
                                }
                                return deferred.resolve(Events.format(event));
                            };
                            if (CP.ready) {
                                return onReady();
                            }
                            else {
                                return DataBus.on('/site/ready', onReady);
                            }
                        });
                        return deferred.promise;
                    }
                ]
            }
        }]
};
module.exports = function (routes) {
    var map = routes.routes;
    return {
        name: 'activity',
        url: '',
        abstract: true,
        views: {
            "site": {
                templateProvider: ['$templateCache', function ($templateCache) {
                        return $templateCache.get('views/map/Template.html');
                    }],
                controller: 'MapCtrl as Map',
            }
        },
        children: [{
                name: 'search',
                url: '?action&args&search&group',
                abstract: true,
                views: {
                    "map@activity": {
                        controller: 'ActivitySearchCtrl as Search',
                        templateProvider: ['$templateCache', function ($templateCache) {
                                return $templateCache.get('views/activities/search/Template.html');
                            }],
                    }
                },
                children: [{
                        name: 'default',
                        url: '/search?type',
                        reloadOnSearch: false,
                    }, {
                        name: 'type',
                        url: '/{type:(?:tandem-partners|language-events|language-teachers)}-in-{city:[a-z-]+$}',
                        reloadOnSearch: false,
                        resolve: {
                            prepare: ['$stateParams', function (params) {
                                    params.type = params.type === 'tandem-partners' ? 'tandems' : 'events';
                                    return true;
                                }]
                        }
                    }, {
                        name: 'language',
                        url: '/{language:[a-z]+}-{type:(?:tandem-partners|language-events|language-teachers)}',
                        reloadOnSearch: false,
                        resolve: {
                            prepare: ['$stateParams', 'Util', function (params, Util) {
                                    params.type = params.type === 'tandem-partners' ? 'tandems' : 'events';
                                    params.languageID = Util.language.i18n2ID(params.language.replace('-', '_').toUpperCase());
                                    return true;
                                }]
                        }
                    }]
            }, {
                name: 'events',
                url: '/events',
                abstract: true,
                data: {
                    type: 'event'
                },
                views: _event.views,
                children: _.cloneDeep(_event.children)
            }, {
                name: 'listings',
                url: '/listings',
                abstract: true,
                data: {
                    type: 'listing'
                },
                views: _event.views,
                children: _.cloneDeep(_event.children)
            }],
        _data: {
            eventData: _event
        }
    };
};
//# sourceMappingURL=activityRoute.js.map