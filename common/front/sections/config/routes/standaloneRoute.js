/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
module.exports = function (routes) {
    var map = routes.routes, path = routes.path;
    return {
        name: 'standalone',
        url: '',
        abstract: true,
        views: {
            "site": {
                templateProvider: ['$templateCache', function ($templateCache) {
                        return $templateCache.get('views/standalone/View.html');
                    }],
            }
        },
        cache: false,
        children: [{
                name: 'about',
                url: '/' + map.about,
                views: {
                    "standalone@standalone": {
                        templateUrl: 'views/standalone/pages/aboutTemplate.html',
                    }
                },
            }, {
                name: 'privacyPolicy',
                url: '/' + map.privacyPolicy,
                views: {
                    "standalone@standalone": {
                        templateUrl: "views/" + path + "standalone/pages/privacyPolicyTemplate.html"
                    }
                },
            }, {
                name: 'termsService',
                url: '/' + map.termsService,
                views: {
                    "standalone@standalone": {
                        templateUrl: "views/" + path + "standalone/pages/termsServiceTemplate.html"
                    }
                },
            }, {
                name: 'notFound',
                url: '/404',
                views: {
                    "standalone@standalone": {
                        templateUrl: 'views/standalone/404/Template.html',
                    }
                },
            }, {
                name: 'translator',
                url: '/' + map.translate,
                views: {
                    "standalone@standalone": {
                        templateProvider: ['$templateCache', function ($templateCache) {
                                return $templateCache.get('views/standalone/translator/Template.html');
                            }],
                        controller: 'TranslatorThankyouCtrl as Translator'
                    }
                },
            }, {
                name: 'eventIdeas',
                url: '/' + map.eventIdeas,
                views: {
                    "standalone@standalone": {
                        templateProvider: ['$templateCache', function ($templateCache) {
                                return $templateCache.get('views/standalone/eventIdeas/Template.html');
                            }],
                        controller: 'EventIdeasCtrl'
                    }
                },
            }, {
                name: 'settings',
                url: '/' + map.settings,
                roles: ['SELF_EDIT'],
                views: {
                    "standalone@standalone": {
                        templateProvider: ['$templateCache', function ($templateCache) {
                                return $templateCache.get('views/account/settings/Template.html');
                            }],
                        controller: 'SettingsCtrl as Settings',
                    }
                },
                resolve: {
                    passwordNotSet: [
                        'SocketIo',
                        function (SocketIo) {
                            return SocketIo.onEmitSock('/user/verify', {
                                persistent: true,
                                verify: {
                                    password: ''
                                }
                            });
                        }
                    ]
                }
            }, {
                name: 'learnMoreEventHost',
                url: '/' + map.learnMoreEventHost,
                views: {
                    "standalone@standalone": {
                        templateProvider: ['$templateCache', function ($templateCache) {
                                return $templateCache.get('views/landing/modals/eventHost/Template.html');
                            }]
                    }
                },
            }, {
                name: 'learnMoreTandem',
                url: '/' + map.learnMoreTandem,
                views: {
                    "standalone@standalone": {
                        templateProvider: ['$templateCache', function ($templateCache) {
                                return $templateCache.get('views/landing/modals/tandem/Template.html');
                            }]
                    }
                },
            }, {
                name: 'learnMoreTeacher',
                url: '/' + map.learnMoreTeacher,
                views: {
                    "standalone@standalone": {
                        templateProvider: ['$templateCache', function ($templateCache) {
                                return $templateCache.get('views/landing/modals/teacher/Template.html');
                            }]
                    }
                },
            }, {
                name: 'addYourself',
                url: '/add-yourself',
                loggedinRedirect: [
                    'dashboard.profile.user', {
                        userID: "me"
                    }
                ],
                views: {
                    "standalone@standalone": {
                        templateProvider: ['$templateCache', function ($templateCache) {
                                return $templateCache.get('views/standalone/addYourself/Template.html');
                            }]
                    }
                },
            }, {
                name: 'FAQ',
                url: '/' + map.FAQ,
                views: {
                    "standalone@standalone": {
                        templateUrl: 'views/standalone/FAQ/Template.html',
                        controller: 'FAQCtrl as FAQ'
                    }
                },
            }, {
                name: 'contact',
                url: '/' + map.contact + '?redirect&action',
                views: {
                    "standalone@standalone": {
                        controller: ['ModalFactory', function (ModalFactory) {
                                return (new ModalFactory()).load('contactModal');
                            }],
                        templateProvider: ['$templateCache', function ($templateCache) {
                                return $templateCache.get('views/modals/standalone/contact/Template.html');
                            }]
                    }
                },
            }, {
                name: 'feedback',
                url: '/' + map.feedback + '?redirect&action',
                views: {
                    "standalone@standalone": {
                        controller: ['ModalFactory', function (ModalFactory) {
                                return (new ModalFactory()).load('feedbackModal');
                            }],
                        templateProvider: ['$templateCache', function ($templateCache) {
                                return $templateCache.get('views/modals/standalone/feedback/Template.html');
                            }]
                    }
                },
            }, {
                name: 'abuse',
                url: '/' + map.abuse + '?redirect&action',
                views: {
                    "standalone@standalone": {
                        controller: ['ModalFactory', function (ModalFactory) {
                                return (new ModalFactory()).load('abuseModal');
                            }],
                        templateProvider: ['$templateCache', function ($templateCache) {
                                return $templateCache.get('views/modals/standalone/abuse/Template.html');
                            }]
                    }
                },
            }, {
                name: 'classLearn',
                url: '/' + map.learnMoreClass,
                views: {
                    "standalone@standalone": {
                        templateProvider: ['$templateCache', function ($templateCache) {
                                return $templateCache.get('views/event/directives/classLearn/Standalone.html');
                            }]
                    }
                },
            }, {
                name: 'donate',
                url: '/' + map.donate,
                views: {
                    "standalone@standalone": {
                        templateProvider: ['$templateCache', function ($templateCache) {
                                return $templateCache.get('views/standalone/donate/Template.html');
                            }]
                    }
                },
            }, {
                name: 'donateSubmitted',
                url: '/' + map.donateSubmitted,
                views: {
                    "standalone@standalone": {
                        templateProvider: ['$templateCache', function ($templateCache) {
                                return $templateCache.get('views/standalone/donate/SubmittedTemplate.html');
                            }]
                    }
                },
            }, {
                name: 'teacher',
                url: '/teach-a-class',
                views: {
                    "standalone@standalone": {
                        templateProvider: ['$templateCache', function ($templateCache) {
                                return $templateCache.get('views/landing/teacher/Template.html');
                            }]
                    }
                },
            }]
    };
};
//# sourceMappingURL=standaloneRoute.js.map