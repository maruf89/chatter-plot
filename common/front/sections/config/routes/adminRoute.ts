/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

"use strict";

module.exports = function (routes) {
    var map = routes.routes;

    return {
        name: 'admin',
        url: '/admin',
        isAdmin: true,
        views: {
            "site": {
                templateUrl: 'views/admin/index',
                controller: [
                    '$state', '$scope',
                    function ($state, $scope) {
                        return $scope.$state = $state;
                    }
                ],
            }
        },
        children: [{
            name: 'users',
            url: '/users',
            roles: ['USER_VIEW'],
            templateUrl: 'views/admin/users/index',
            children: [{
                name: 'edit',
                url: '/edit?from&size&sort&sortBy&empty',
                roles: ['USER_EDIT', 'USER_VIEW'],
                templateUrl: 'views/admin/users/edit/Template',
                controller: 'adminUsersEditCtrl as uAdmin',
                resolve: {
                    users: [
                        '$q', 'SocketIo', '$location',
                        function ($q, socket, $location) {
                            return socket.onEmitSock(
                                '/user/allUsers', _.pick(
                                    $location.search(), [
                                        'from', 'size',
                                        'sortBy', 'sort',
                                        'empty'
                                    ]));
                        }
                    ]
                }
            }, {
                name: 'add',
                url: '/add',
                roles: ['USER_ADD'],
                templateUrl: 'views/admin/users/add/Template',
                controller: 'adminUsersAddCtrl as uAdmin'
            }]
        }, {
            name: 'abuse',
            url: '/abuse',
            roles: ['USER_EDIT'],
            templateUrl: 'views/admin/abuse/index',
            children: [{
                name: 'messages',
                url: '/messages',
                templateUrl: 'views/admin/abuse/messages/Template',
                controller: 'adminAbuseMessagesCtrl as uAdmin',
                children: [{
                    name: 'single',
                    url: '/:msgID',
                    controller: 'MessageSingleCtrl as Single',
                    templateProvider: [
                        '$templateCache',
                        function ($templateCache) {
                            return $templateCache.get(
                                'views/services/Interaction/msgWindow/Template.html'
                            );
                        }
                    ],
                    resolve: {
                        Conversation: [
                            '$stateParams', 'Interaction',
                            function ($stateParams,
                                      Messaging) {
                                return Messaging.get({
                                    msgID: $stateParams
                                        .msgID,
                                    conversation: true,
                                    messages: true,
                                    withUsers: true
                                });
                            }
                        ]
                    }
                }]
            }]
        }, {
            name: 'event',
            url: '/event',
            roles: ['USER_EDIT'],
            templateUrl: 'views/admin/event/index',
            children: [{
                name: 'bulkAdd',
                url: '/bulkAdd',
                templateUrl: 'views/admin/event/bulkAdd/Template',
                controller: 'adminEventBulkAddCtrl as uAdmin'
            }]
        }, {
            name: 'misc',
            url: '/misc',
            roles: ['USER_EDIT'],
            templateUrl: 'views/admin/misc/index',
            children: [{
                name: 'feature',
                url: '/feature',
                templateUrl: 'views/admin/misc/feature/Template',
                controller: 'adminMiscFeatureCtrl as Admin'
            }, {
                name: 'siteMsg',
                url: '/siteMsg',
                templateUrl: 'views/admin/misc/siteMsg/Template',
                controller: 'adminMiscSiteMsgCtrl as Admin'
            }, ]
        }]
    };
};
