/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

"use strict";

module.exports = function (routes) {
    var map = routes.routes;

    return {
        name: 'email',
        url: '/email',
        abstract: true,
        views: {
            "site": {
                templateProvider: ['$templateCache', function ($templateCache) {
                    return $templateCache.get('views/email/View.html');
                }],
            }
        },
        children: [{
            name: 'verified',
            url: '/verify_user',
            views: {
                "email@email": {
                    templateUrl: 'views/email/verified/Template',
                    controller: 'EmailCtrl as Email'
                }
            },
        }, {
            name: 'passwordReset',
            url: '/password_reset',
            views: {
                "email@email": {
                    templateUrl: 'views/email/passwordResetVerified/Template',
                    controller: 'EmailCtrl as Email'
                }
            },
        }, {
            name: 'invalid',
            url: '/invalid?type&source',
            views: {
                "email@email": {
                    templateUrl: 'views/email/invalid/Template',
                    controller: 'EmailCtrl as Email'
                }
            },
        }]
    };
};
