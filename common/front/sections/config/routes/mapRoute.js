/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
var commonUser = require('common/type/user');
var _event = {
    templateProvider: ['$templateCache', function ($templateCache) {
            return $templateCache.get('views/event/View.html');
        }],
    children: [{
            name: 'single',
            url: '/{eID:[0-9]+}?action',
            views: {
                "event": {
                    templateProvider: ['$templateCache', function ($templateCache) {
                            return $templateCache.get('views/event/single/Template.html');
                        }],
                    controller: 'EventSingleCtrl as Event',
                }
            },
            resolve: {
                Datum: ['$state', 'User', 'DataBus', '$q', 'Events',
                    function ($state, User, DataBus, $q, Events) {
                        var event = null, deferred = $q.defer(), getParams = {
                            evTypes: [{
                                    type: $state.next.data.type,
                                    eID: $state.toParams.eID || $state.next.data.eID,
                                }],
                            venues: true,
                            host: $state.next.data.type === 'event',
                            description: true,
                        };
                        Events.get(getParams)
                            .then(function (_event) {
                            return event = _event[0];
                        })["finally"](function () {
                            // Must wait til the site's ready to see if the user can see this event
                            // depending on whether it's published and/or it's the user's own unpublished event
                            var onReady = function () {
                                if (!event) {
                                    return deferred.reject(new Error('Not Found'));
                                }
                                else if (!event || (!event.published && event.creator !== User.userID)) {
                                    return deferred.reject(new Error('Page is not accessible'));
                                }
                                return deferred.resolve(Events.format(event));
                            };
                            if (CP.ready) {
                                return onReady();
                            }
                            else {
                                return DataBus.on('/site/ready', onReady);
                            }
                        });
                        return deferred.promise;
                    }
                ]
            }
        }]
};
module.exports = function (routes) {
    var map = routes.routes;
    return {
        name: 'map',
        url: '',
        abstract: true,
        views: {
            "site": {
                templateProvider: ['$templateCache', function ($templateCache) {
                        return $templateCache.get('views/map/Template.html');
                    }],
                controller: 'MapCtrl as Map',
            }
        },
        children: [{
                name: 'activity',
                url: '',
                abstract: true,
                views: {
                    "map@map": {
                        templateProvider: ['$templateCache', function ($templateCache) {
                                return $templateCache.get('views/activities/View.html');
                            }]
                    }
                },
                children: [{
                        name: 'search',
                        url: '?action&args&search&group',
                        abstract: true,
                        views: {
                            "activity@map.activity": {
                                templateProvider: ['$templateCache', function ($templateCache) {
                                        return $templateCache.get('views/activities/search/Template.html');
                                    }],
                                controller: 'ActivitySearchCtrl as Search',
                            }
                        },
                        children: [{
                                name: 'default',
                                url: '/search?type',
                                reloadOnSearch: false,
                            }, {
                                name: 'type',
                                url: '/{type:(?:tandem-partners|language-events)}-in-{city:[a-z-]+$}',
                                reloadOnSearch: false,
                                resolve: {
                                    prepare: ['$stateParams', function (params) {
                                            params.type = params.type === 'tandem-partners' ? 'tandems' : 'events';
                                            return true;
                                        }]
                                }
                            }, {
                                name: 'language',
                                url: '/{language:[a-z]+}-{type:(?:tandem-partners|language-events)}',
                                reloadOnSearch: false,
                                resolve: {
                                    prepare: ['$stateParams', 'Util', function (params, Util) {
                                            params.type = params.type === 'tandem-partners' ? 'tandems' : 'events';
                                            params.languageID = Util.language.i18n2ID(params.language.replace('-', '_').toUpperCase());
                                            return true;
                                        }]
                                }
                            }]
                    }, {
                        name: 'events',
                        url: '/events',
                        abstract: true,
                        data: {
                            type: 'event'
                        },
                        views: {
                            "map@map": {
                                templateProvider: _event.templateProvider,
                            }
                        },
                        children: _.cloneDeep(_event.children)
                    }, {
                        name: 'listings',
                        url: '/listings',
                        abstract: true,
                        data: {
                            type: 'listing'
                        },
                        views: {
                            "map@map": {
                                templateProvider: _event.templateProvider,
                            }
                        },
                        children: _.cloneDeep(_event.children)
                    }],
            }, {
                name: 'dashboard',
                url: '',
                abstract: true,
                views: {
                    "map@map": {
                        templateProvider: ['$templateCache', function ($templateCache) {
                                return $templateCache.get('views/dashboard/Template.html');
                            }]
                    }
                },
                children: [{
                        name: 'myActivities',
                        url: '/my-activities?eID&fromStore&hide&type&action',
                        roles: ['SELF_EDIT'],
                        views: {
                            "dashboard@map.dashboard": {
                                templateProvider: ['$templateCache', function ($templateCache) {
                                        return $templateCache.get('views/dashboard/myActivities/Template.html');
                                    }],
                                controller: 'MyActivitiesCtrl as My',
                            }
                        },
                        data: {
                            tabClass: 'my-activities'
                        }
                    }, {
                        name: 'profile',
                        url: '/profile',
                        views: {
                            "dashboard@map.dashboard": {
                                templateProvider: ['$templateCache', function ($templateCache) {
                                        return $templateCache.get('views/profile/View.html');
                                    }],
                            }
                        },
                        data: {
                            tabClass: 'profile'
                        },
                        children: [{
                                name: 'user',
                                url: '/user/{userID}?action',
                                views: {
                                    "profile@map.dashboard.profile": {
                                        templateProvider: ['$templateCache', function ($templateCache) {
                                                return $templateCache.get('views/profile/Template.html');
                                            }],
                                        controller: 'ProfileCtrl as Profile',
                                    }
                                },
                                resolve: {
                                    CurUser: [
                                        '$stateParams', 'User', '$state',
                                        function ($stateParams, User, $state) {
                                            var userID = p($stateParams.userID);
                                            if ($stateParams.userID === 'me' && !User.userID) {
                                                throw $state.go('account.auth', { page: 'login' });
                                            }
                                            if ($stateParams.userID === 'me' && User.userID) {
                                                _.defer(function () {
                                                    $state.go('map.dashboard.profile.user', { userID: User.userID }, {
                                                        notify: false,
                                                        reload: true
                                                    });
                                                });
                                                userID = User.userID;
                                            }
                                            if (userID === User.userID) {
                                                return User.data;
                                            }
                                            return User.cacheGet([userID], commonUser.insecureFields).then(function (res) {
                                                return res[userID];
                                            });
                                        }
                                    ]
                                }
                            }]
                    }, {
                        name: 'eventCreate',
                        url: '/create?eID&fromStore&hide&type',
                        roles: ['CREATE_EVENT'],
                        data: {
                            tabClass: 'hosting'
                        },
                        views: {
                            "dashboard@map.dashboard": {
                                templateProvider: ['$templateCache', function ($templateCache) {
                                        return $templateCache.get('views/event/create/Template.html');
                                    }],
                                controller: 'EventCreateCtrl as Create',
                            }
                        },
                        children: [{
                                name: 'basicInfo',
                                url: '/basic-info',
                                views: {
                                    "create@map.dashboard.eventCreate": {
                                        templateProvider: ['$templateCache', function ($templateCache) {
                                                return $templateCache.get('views/event/create/basicInfo/Template.html');
                                            }],
                                    }
                                },
                            }, {
                                name: 'languages',
                                url: '/languages',
                                views: {
                                    "create@map.dashboard.eventCreate": {
                                        templateProvider: ['$templateCache', function ($templateCache) {
                                                return $templateCache.get('views/event/create/languages/Template.html');
                                            }],
                                    }
                                },
                            }, {
                                name: 'details',
                                url: '/details',
                                views: {
                                    "create@map.dashboard.eventCreate": {
                                        templateProvider: ['$templateCache', function ($templateCache) {
                                                return $templateCache.get('views/event/create/details/Template.html');
                                            }],
                                    }
                                },
                            }],
                        resolve: {
                            EditEvent: [
                                '$stateParams', 'Events',
                                function ($stateParams, Events) {
                                    var type;
                                    if ($stateParams.eID) {
                                        type = $stateParams.type || 'event';
                                        return Events.get({
                                            evTypes: [{
                                                    eID: [$stateParams.eID],
                                                    type: type,
                                                }],
                                            description: true,
                                            venues: true,
                                        })
                                            .then(function (event) {
                                            return event[0];
                                        }, function (notFound) {
                                            throw notFound;
                                        });
                                    }
                                    else {
                                        return false;
                                    }
                                }
                            ]
                        }
                    }, {
                        name: 'favorites',
                        url: '/favorites',
                        roles: ['SELF_VIEW'],
                        data: {
                            tabClass: 'fave-tab'
                        },
                        views: {
                            "dashboard@map.dashboard": {
                                templateProvider: ['$templateCache', function ($templateCache) {
                                        return $templateCache.get('views/dashboard/favorite/Template.html');
                                    }],
                                controller: 'FavoriteCtrl as Favorite',
                            }
                        },
                    }]
            }]
    };
};
//# sourceMappingURL=mapRoute.js.map