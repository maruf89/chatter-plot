"use strict";

require('moment/locale/eo');

CP.site.routes = require('root/config/routes/translations/eo-XX');
CP.site.language = 'ESPERANTO';
