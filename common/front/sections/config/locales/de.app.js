"use strict";

require('moment/locale/de');

CP.site.routes = require('root/config/routes/translations/de-DE');
CP.site.language = 'GERMAN';
