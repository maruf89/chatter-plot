"use strict";

require('moment/locale/es');

CP.site.routes = require('root/config/routes/translations/es-ES');
CP.site.language = 'SPANISH';
