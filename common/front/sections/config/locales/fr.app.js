"use strict";

require('moment/locale/fr');

CP.site.routes = require('root/config/routes/translations/fr-FR');
CP.site.language = 'FRENCH';
