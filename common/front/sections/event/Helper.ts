/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

"use strict";

var moment = require('moment'),
    commonGeneral = require('common/util/general');

class Helper implements cp.event.IServiceHelper {
    constructor (
        private _Events:cp.event.IService,
        private _$state:ng.ui.IStateService,
        private _DataBus:NodeJS.EventEmitter,
        private _Util:any
    ) {}

    /**
     * @description
     * @param {boolean} current - whether to get upcoming events or past ones
     * @param {number=} size
     * @param {number=} from
     * @returns {ng.IPromise<IServerResponse>}
     */
    public fetchRecent(current:boolean, size?:number, from?:number):ng.IPromise<cp.interaction.IServerResponse[]> {
        var when:string,
            whenDate:string = null;

        if (current) {
            when = 'current';
            whenDate = moment().subtract(3, 'hours');
        } else {
            when = 'past';
        }

        return this._Events.fetch({
            size: size || 10,
            filterTypes: [when, 'attending'],
            opts: {
                // Get all events within the past 3 hours
                when: commonGeneral.format.dateString('basic_date_time_no_millis', whenDate),
            },
            venues: true,
            from: typeof from === 'number' ? from : 0,
        });
    }

    public publish($scope, event):void {
        var DataBus = this._DataBus;
        this._Events.publish(event, event.indexType)
            .then(function (response) {
                event.published = true;
                $scope.$root.safeDigest($scope);
                DataBus.emit('yapServerResponse', response);
            })
            .catch(function (err) {
                DataBus.emit('yapServerResponse', err);
            });
    }

    /**
     * @description builds a url for an interaction action
     * @param {object} request - the requset object
     * @param {string} action - one of (accept|deny|edit)
     * @returns {string}
     */
    public generateLink(event:cp.event.IEventFormatted, action:string):string {
        return this._$state.href('dashboard.myActivities', {
            action: this._Util.encodeActionArgs({
                action: action,
                objID: event.eID,
                type: 'event',
                subType: event.indexType,
            })
        });
    }
}

exports.Helper = function (
    $state:ng.ui.IStateService,
    $q:ng.IQService,
    DataBus:any,
    Util:any,
    Config:any
) {
    var instance = null;

    return {
        get: function (Events) {
            return instance || (instance = new Helper(
                Events,
                $state,
                $q,
                DataBus,
                Util,
                Config
            ));
        }
    };
};

exports.Map = function (
    Util:any
) {
    return function (Events:cp.event.IService) {
        this.getWindow = function (marker) {
            var _source = ['name'];

            if (!marker.hidden) {
                _source.push('locationDisplayName');
            }

            return Events.get({
                _source: _source,
                venues: !marker.hidden,
                evTypes: [{
                    type: marker._type || 'event',
                    eID: marker._id,
                }]
            })
                .then(function (event) {
                    if (marker.hidden) {
                        // If we're not searching for venues we'll get back an object
                        event.venue = {
                            coords: marker.coords,
                            eID: p(marker._id)
                        };
                    } else {
                        // otherwise we'll be getting back an array
                        event = event[0];
                    }

                    event.venue.coords = marker.coords;

                    return event;
                });
        };

        this.formatWindow = function (activity, options) {
            var address, opts,
                locName = address = '';

            if (!activity.venue.hidden) {
                locName = activity.locationDisplayName || activity.venueDetails.name;
                address = Util.geo.format.address(activity.venueDetails);
            }

            opts = {
                coords: {
                    latitude: activity.venue.coords.lat || activity.venue.coords.latitude,
                    longitude: activity.venue.coords.lon || activity.venue.coords.longitude
                },
                template: 'views/activities/templates/addressWindow.html',
                scope: {
                    activity: {
                        name: activity.name,
                        locationName: locName,
                        address: address
                    }
                }
            };

            if (options && options.showDirections) {
                opts.scope.activity.directionsLink = Util.geo.toGMapUrl(activity.venueDetails, 'directions');
            }

            return opts;
        };
    };
};
