/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
var TimePicker = require('root/directives/TimePicker');
var Modify = (function () {
    function Modify(_$q, _Venues, _Config, _Util) {
        this._$q = _$q;
        this._Venues = _Venues;
        this._Config = _Config;
        this._Util = _Util;
        this._variables = {
            vars: {
                eventAsListing: false,
                regRequired: false,
                event: {
                    minDate: null,
                    minTime: null,
                    date: null,
                    time: null,
                    endDate: null,
                    endTime: null,
                },
                rsvpOnlyRadius: false,
                showDateEnd: false,
                location: null,
                locationClone: null,
                venueTz: null,
                updateLocation: null,
                languages: null,
                allLanguages: false,
                skill: {
                    from: { level: 1 },
                    to: { level: 6 },
                },
                allSkills: true,
                priceFree: true,
                maxGoing: false // {Boolean} - whether there is a cap
            },
            newEvent: {
                name: null,
                locationDisplayName: null,
                description: null,
                paymentInfo: null,
                aboutHost: null,
                venue: {
                    vID: null,
                    coords: {
                        lat: null,
                        lon: null,
                    }
                }
            },
        };
        this.defaultSections = [
            'date',
            'location',
            'languages',
            'copy',
        ];
    }
    Modify.prototype.initVars = function () {
        var variables = _.cloneDeep(this._variables), date = variables.vars.event, 
        // Set the minimum possible start time/date
        minTime = (new Date());
        // Min time to 2 hours ahead of now
        minTime.setHours(minTime.getHours() + 2);
        date.minDate = (new Date()).setHours(0, 0, 0, 0);
        date.minTime = TimePicker.dateTimeQuarters(minTime).join('|');
        return variables;
    };
    Modify.prototype._iterateSections = function (type, sections, Event, variables) {
        var context = this;
        return this._$q.all(_.map(sections, function (sec) {
            var method = "_" + type + "_" + sec;
            return context[method] && context[method](Event, variables) || context._$q.when();
        }));
    };
    Modify.prototype.parseEdit = function (Event, sections) {
        var eventData = this.initVars(), sections = sections || this.defaultSections.slice();
        return this._iterateSections('parse', sections, Event, eventData)
            .then(function () {
            var vars = eventData.vars;
            vars.eventAsListing = Event.indexType === 'listing';
            vars.regRequired = !!Event.registrationURL;
            vars.priceFree = !Event.priceString;
            vars.maxGoing = !!Event.maxGoing;
            return eventData;
        });
    };
    /**
     * TODO:    -    Fix loading in an event which has end date/times set
     */
    Modify.prototype._parse_date = function (Event, data) {
        var date = this._Config.format.date.toObj('basic_date_time_no_millis', Event.when, false).toDate(), endDate = Event.until ?
            this._Config.format.date.toObj('basic_date_time_no_millis', Event.until, false).toDate()
            : null, dateObj = data.vars.event;
        dateObj.date = dateObj.time = date;
        dateObj.endDate = dateObj.endTime = endDate;
        dateObj.showDateEnd = !!endDate;
        return true;
    };
    Modify.prototype._parse_location = function (Event, data) {
        var vars = data.vars;
        vars.rsvpOnlyRadius = !!Event.venue.rsvpOnlyRadius;
        vars.location = _.clone(Event.venueDetails);
        vars.locationClone = _.clone(Event.venueDetails);
    };
    Modify.prototype._parse_languages = function (Event, data) {
        var skillLength = Event.levels.length, vars = data.vars;
        return this._Util.language.expand(null, Event.languages)
            .then(function (languages) {
            vars.languages = languages;
            vars.allLanguages = Event.languages.length && Event.languages[0] === 0;
            vars.skill = {
                from: {
                    level: Event.levels[0]
                },
                to: {
                    level: Event.levels[skillLength - 1]
                }
            };
            vars.allSkills = skillLength === 6;
            return true;
        });
    };
    Modify.prototype._parse_copy = function (Event, data) {
        data.newEvent = this._Util.cloneXDeep(Event, 2);
        return true;
    };
    Modify.prototype._parse_duplicate = function (Event, data) {
        data.newEvent = {
            duplicate: Event.duplicate || Event.eID,
            indexType: Event.indexType,
            languages: Event.languages,
            when: Event.when,
            until: Event.until,
            levels: Event.levels,
            name: Event.name,
            hasPhoto: Event.hasPhoto,
            description: Event.description,
            venue: Event.venue,
            locationDisplayName: Event.locationDisplayName,
            maxGoing: Event.maxGoing,
            paymentInfo: Event.paymentInfo,
            priceString: Event.priceString,
        };
        if (Event.indexType === 'listing') {
            data.newEvent.registrationURL = Event.registrationURL;
        }
        else {
            data.newEvent.aboutHost = Event.aboutHost;
        }
        return true;
    };
    Modify.prototype.formatEdit = function (Event, sections, vars) {
        var context = this;
        return this._iterateSections('format', sections, Event, vars)
            .then(function () {
            return sections.indexOf('date') !== -1 ? context._post_format_date(Event, vars) : context._$q.when();
        })
            .then(function () {
            Event.indexType = vars.eventAsListing ? 'listing' : 'event';
            if (!vars.regRequired && Event.registrationURL) {
                Event.registrationURL = null;
                try {
                    delete Event.registrationURL;
                }
                catch (e) { }
            }
            if (vars.eventAsListing || !vars.maxGoing) {
                Event.maxGoing = null;
                try {
                    delete Event.maxGoing;
                }
                catch (e) { }
            }
            if (vars.eventAsListing) {
                Event.aboutHost = null;
                try {
                    delete Event.aboutHost;
                }
                catch (e) { }
            }
            if (!Event.paymentInfo && Event.paymentInfo) {
                Event.paymentInfo = null;
                try {
                    delete Event.paymentInfo;
                }
                catch (e) { }
            }
            if (!vars.rsvpOnlyRadius) {
                Event.venue.rsvpOnlyRadius = null;
            }
            return Event;
        });
    };
    /**
     * @description diffes from the other _past_{section} methods because it has
     * to happen after location is set so that we have the timezone
     * @private
     */
    Modify.prototype._post_format_date = function (Event, vars) {
        var time = vars.event.time.split('|'), tzPromise = typeof vars.venueTz === 'number' ?
            this._$q.when(vars.venueTz)
            :
                this._Venues.getTz(Event.venue.vID);
        return tzPromise.then(function (tz) {
            Event.when = this._Config.format.date.toString('basic_date_time_no_millis', [vars.event.date, time, tz]);
            Event.until = this._extractUntilDate(vars, time);
            return true;
        }.bind(this));
    };
    Modify.prototype._extractUntilDate = function (vars, startTime) {
        if (!vars.showDateEnd || (!vars.event.endDate && !vars.event.endTime)) {
            return null;
        }
        var date = vars.event.endDate || new Date(), untilTime = vars.event.endTime ? vars.event.endTime.split('|') : null;
        // The date is automatically +1 than the end date, so if both are set then we have everything
        if (vars.event.endDate && untilTime) {
            return this._Config.format.date.toString('basic_date_time_no_millis', [
                vars.event.endDate, untilTime
            ]);
        }
        // Check if the end time < start time, if so than increment the date by +1
        if (untilTime) {
            if (p(untilTime[0]) < p(startTime[0]) ||
                (p(untilTime[0]) === p(startTime[0]) &&
                    p(untilTime[1]) <= p(startTime[1]))) {
                date.setDate(vars.event.date.getDate() + 1);
            }
            else {
                date.setDate(vars.event.date.getDate());
            }
        }
        return this._Config.format.date.toString('basic_date_time_no_millis', [date, untilTime]);
    };
    Modify.prototype._format_location = function (Event, vars) {
        var promise = vars.updateLocation && Event.venue ?
            this._Venues.save([vars.location])
            :
                this._$q.when([Event.venue]);
        return promise.then(function (locArray) {
            // set the venue id to the location
            var loc = locArray[0], locType = loc.google ? 'google' : 'yelp', venue;
            Event.venue.vID = loc.vID;
            Event.venue.coords = vars.location.coords;
            Event.venue[locType] = loc[locType];
            venue = loc.vID ? loc : loc[Event.venue.vID];
            vars.venueTz = venue.tz;
            return true;
        });
    };
    Modify.prototype._format_languages = function (Event, vars) {
        Event.languages = vars.allLanguages ? [0] : _.filter(_.pluck(vars.languages, 'languageID'), _.identity);
        Event.levels = (function () {
            var levels = [], from = vars.skill.from.level, to = vars.skill.to.level;
            do {
                levels.push(from++);
            } while (from <= to);
            return levels;
        })();
        return true;
    };
    return Modify;
})();
module.exports = Modify;
//# sourceMappingURL=Modify.js.map