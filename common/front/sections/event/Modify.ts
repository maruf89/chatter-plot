/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

var TimePicker = require('root/directives/TimePicker');

class Modify implements cp.event.IModifyService {
    private _variables:cp.event.IModifyVariables = {
        vars: {
            eventAsListing: false,          // {Boolean}
            regRequired: false,             // {Boolean}
            event: {
                minDate: null,              // {Number}
                minTime: null,              // {String}
                date: null,                 // {Date}
                time: null,                 // {Date}
                endDate: null,              // {Date}
                endTime: null,              // {Date}
            },
            rsvpOnlyRadius: false,          // {Boolean}
            showDateEnd: false,             // {Boolean}
            location: null,                 // {Object}
            locationClone: null,            // {Object}
            venueTz:null,                   // {Number}
            updateLocation: null,           // {Boolean}
            languages: null,                // {Array<object>}
            allLanguages: false,            // {Boolean}
            skill: {
                from: { level: 1 },         // {Number}
                to: { level: 6 },           // {Number}
            },
            allSkills: true,                // {Boolean}
            priceFree: true,                // {Boolean}
            maxGoing: false                 // {Boolean} - whether there is a cap
        },
        newEvent: {
            name: null,                     // {String}
            locationDisplayName: null,      // {String}
            description: null,              // {String}
            paymentInfo: null,              // {String}
            aboutHost: null,                // {String}
            venue: {
                vID: null,                  // {Number}
                coords: {
                    lat: null,
                    lon: null,
                }
            }
        },
    };

    public defaultSections:string[] = [
        'date',
        'location',
        'languages',
        'copy',
    ];

    constructor(
        private _$q:ng.IQService,
        private _Venues:cp.venue.IService,
        private _Config:any,
        private _Util:any
    ) {}

    public initVars():cp.event.IModifyVariables {
        var variables:cp.event.IModifyVariables = _.cloneDeep(this._variables),
            date:cp.event.IModifyVariableVarsDate = variables.vars.event,

            // Set the minimum possible start time/date
            minTime = (new Date());

        // Min time to 2 hours ahead of now
        minTime.setHours(minTime.getHours() + 2);

        date.minDate = (new Date()).setHours(0,0,0,0);
        date.minTime = TimePicker.dateTimeQuarters(minTime).join('|');

        return variables;
    }

    private _iterateSections(
        type:string,
        sections:string[],
        Event:cp.event.IEvent,
        variables:cp.event.IModifyVariablesVars
    ):ng.IPromise<any> {
        var context = this;

        return this._$q.all(
            _.map(sections, function (sec:string):ng.Ipromise<any> {
                var method:string = `_${type}_${sec}`;
                return context[method] && context[method](Event, variables) || context._$q.when();
            })
        );
    }

    public parseEdit(Event:cp.event.IEvent, sections?:string[]):ng.IPromise<cp.event.IModifyVariables> {
        var eventData = this.initVars(),
            sections = sections || this.defaultSections.slice();

        return this._iterateSections('parse', sections, Event, eventData)
            .then(function () {
                var vars:cp.event.IModifyVariableVars = eventData.vars;

                vars.eventAsListing = Event.indexType === 'listing';
                vars.regRequired = !!Event.registrationURL;
                vars.priceFree = !Event.priceString;
                vars.maxGoing = !!Event.maxGoing;

                return eventData;
            });
    }

    /**
     * TODO:    -    Fix loading in an event which has end date/times set
     */
    private _parse_date(Event, data):boolean {
        var date:Date = this._Config.format.date.toObj('basic_date_time_no_millis', Event.when, false).toDate(),
            endDate:Date = Event.until ?
                           this._Config.format.date.toObj('basic_date_time_no_millis', Event.until, false).toDate()
                : null,

            dateObj = data.vars.event;

        dateObj.date = dateObj.time = date;
        dateObj.endDate = dateObj.endTime = endDate;
        dateObj.showDateEnd = !!endDate;

        return true;
    }

    private _parse_location(Event, data):boolean {
        var vars = data.vars;

        vars.rsvpOnlyRadius = !!Event.venue.rsvpOnlyRadius;
        vars.location = _.clone(Event.venueDetails);
        vars.locationClone = _.clone(Event.venueDetails);
    }

    private _parse_languages(Event, data):ng.IPromise<boolean> {
        var skillLength:number = Event.levels.length,
            vars = data.vars;

        return this._Util.language.expand(null, Event.languages)
            .then(function (languages) {
                vars.languages = languages;
                vars.allLanguages = Event.languages.length && Event.languages[0] === 0;
                vars.skill = {
                    from: {
                        level: Event.levels[0]
                    },
                    to: {
                        level: Event.levels[skillLength - 1]
                    }
                };
                vars.allSkills = skillLength === 6;

                return true;
            });
    }

    private _parse_copy(Event, data):boolean {
        data.newEvent = this._Util.cloneXDeep(Event, 2);

        return true;
    }

    private _parse_duplicate(Event, data):boolean {
        data.newEvent = {
            duplicate: Event.duplicate || Event.eID,
            indexType: Event.indexType,
            languages: Event.languages,
            when: Event.when,
            until: Event.until,
            levels: Event.levels,
            name: Event.name,
            hasPhoto: Event.hasPhoto,
            description: Event.description,
            venue: Event.venue,
            locationDisplayName: Event.locationDisplayName,
            maxGoing: Event.maxGoing,
            paymentInfo: Event.paymentInfo,
            priceString: Event.priceString,
        };

        if (Event.indexType === 'listing') {
            data.newEvent.registrationURL = Event.registrationURL;
        } else {
            data.newEvent.aboutHost = Event.aboutHost;
        }

        return true;
    }

    public formatEdit(Event:cp.event.IEvent, sections:string[], vars:cp.event.IModifyVariableVars) {
        var context = this;

        return this._iterateSections('format', sections, Event, vars)

            // If we're parsing date, then we need it to come after #_format_location
            .then(function () {
                return sections.indexOf('date') !== -1 ? context._post_format_date(Event, vars) : context._$q.when();
            })

            .then(function () {
                Event.indexType = vars.eventAsListing ? 'listing' : 'event';

                if (!vars.regRequired && Event.registrationURL) {
                    Event.registrationURL = null;
                    try {
                        delete Event.registrationURL;
                    } catch (e) {}
                }
                if (vars.eventAsListing || !vars.maxGoing) {
                    Event.maxGoing = null;
                    try {
                        delete Event.maxGoing;
                    } catch (e) {}
                }
                if (vars.eventAsListing) {
                    Event.aboutHost = null;
                    try {
                        delete Event.aboutHost;
                    } catch (e) {}
                }
                if (!Event.paymentInfo && Event.paymentInfo) {
                    Event.paymentInfo = null;
                    try {
                        delete Event.paymentInfo;
                    } catch (e) {}
                }
                if (!vars.rsvpOnlyRadius) {
                    Event.venue.rsvpOnlyRadius = null;
                }

                return Event;
            });
    }

    /**
     * @description diffes from the other _past_{section} methods because it has
     * to happen after location is set so that we have the timezone
     * @private
     */
    private _post_format_date(Event, vars):ng.IPromise<boolean> {
        var time = vars.event.time.split('|'),
            tzPromise = typeof vars.venueTz === 'number' ?
                this._$q.when(vars.venueTz)
                    :
                this._Venues.getTz(Event.venue.vID);

        return tzPromise.then(function (tz:number) {
            Event.when = this._Config.format.date.toString(
                'basic_date_time_no_millis',
                [vars.event.date, time, tz]
            );

            Event.until = this._extractUntilDate(vars, time);

            return true;
        }.bind(this));
    }

    private _extractUntilDate(vars:cp.event.IModifyVariableVars, startTime:string[]):string {
        if (!vars.showDateEnd || (!vars.event.endDate && !vars.event.endTime)) {
            return null;
        }

        var date = vars.event.endDate || new Date(),
            untilTime = vars.event.endTime ? vars.event.endTime.split('|') : null;

        // The date is automatically +1 than the end date, so if both are set then we have everything
        if (vars.event.endDate && untilTime) {
            return this._Config.format.date.toString('basic_date_time_no_millis', [
                vars.event.endDate, untilTime
            ]);
        }

        // Check if the end time < start time, if so than increment the date by +1
        if (untilTime) {
            if (p(untilTime[0]) < p(startTime[0]) ||
                (p(untilTime[0]) === p(startTime[0]) &&
                p(untilTime[1]) <= p(startTime[1]))
            ) {
                date.setDate(vars.event.date.getDate() + 1);
            } else {
                date.setDate(vars.event.date.getDate());
            }
        }

        return this._Config.format.date.toString('basic_date_time_no_millis', [date, untilTime]);
    }

    private _format_location(Event, vars):ng.IPromise<boolean> {
        var promise = vars.updateLocation && Event.venue ?
                      this._Venues.save([vars.location])
                    :
                      this._$q.when([Event.venue]);

        return promise.then(function (locArray) {
            // set the venue id to the location
            var loc = locArray[0],
                locType = loc.google ? 'google' : 'yelp',
                venue;

            Event.venue.vID = loc.vID;
            Event.venue.coords = vars.location.coords;
            Event.venue[locType] = loc[locType];

            venue = loc.vID ? loc : loc[Event.venue.vID];
            vars.venueTz = venue.tz;

            return true
        });
    }

    private _format_languages(Event, vars):boolean {
        Event.languages = vars.allLanguages ? [0] : _.filter(_.pluck(vars.languages, 'languageID'), _.identity);

        Event.levels = (function () {
            var levels = [],
                from = vars.skill.from.level,
                to = vars.skill.to.level;

            do { levels.push(from++) } while (from <= to);

            return levels;
        })();

        return true;
    }
}

module.exports = Modify;
