/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

import List = require('../../search/AbstractListCtrl');

class EventListCtrl extends List.ListCtrl {
    public vars:any;

    constructor(
        protected _$q:ng.IQService,
        protected _$scope:cp.activities.IListDirectiveScope,
        protected _Activities:any,
        protected _DataBus:NodeJS.EventEmitter,
        protected _eventsMethods:any,
        protected _Util:any,
        protected _Maps:any,
        protected _locStorage:any,
        protected _AdSlot:any,
        Events:any
    ) {


        // call super class
        super({
                listName:'event',
                resultsKey: 'events',
                service: Events,
            },
            _$scope,
            _$q,
            _Activities,
            _AdSlot,
            _Maps,
            _DataBus,
            _Util,
            _locStorage
        );

        this._init();
    }

    private _init() {
        var self = this,
            scope = this._$scope;

        _.defer(function () {
            scope.methods = new self._eventsMethods({}, scope.vars.items);
        });
    }

    /**
     * @description formats event items
     * @param {array<object>} items
     * @returns {array<object>}
     * @private
     */
    protected _formatItems(items) {
        var methods = this._$scope.methods;

        methods.addEvents(items);
        return methods.format(items);
    }

    public updateList(items, replace) {
        return this._$scope.methods[replace ? 'replaceGroups' : 'stitchGroups'](this.vars.items, items);
    }

    public loadItemFromMarker(marker) {
        var scope = this._$scope,
            DataBus = this._DataBus,
            deferred = this._$q.defer();

        this.loadSingle(marker.model._id, marker.model._type)
            .then(function (eventGroups:cp.event.IEventFormatted[][]) {
                scope.vars.items = eventGroups;

                _.defer(deferred.resolve);

                scope.$root.safeDigest(scope);
            }).catch(function (err) {
                err.type = 'activities.ERROR-EVENT_NOTFOUND';
                DataBus.emit('yapServerResponse', err);
                deferred.reject();
            });

        return deferred.promise;
    }

    /**
     * @description called when a cluster is clicked and we are going to fetch the events from the server
     * using the id's and types passed in from this marker set
     * @param markerSet
     * @returns {{eID: any, type: any}[]}
     * @private
     */
    protected _pluckClusterIDs(markerSet:any[]) {
        return _.map(markerSet, function (marker) {
            return {
                eID: marker._id,
                type: marker._type,
            }
        });
    }

    public loadSingle(eID, type) {
        var DataBus = this._DataBus;

        DataBus.emit('progressLoader', { start: true });

        return this._$scope.methods.loadEvent([eID], type)["finally"](function () {
            return DataBus.emit('progressLoader');
        });
    }
}

module.exports = EventListCtrl;
