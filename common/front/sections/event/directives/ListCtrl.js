/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var List = require('../../search/AbstractListCtrl');
var EventListCtrl = (function (_super) {
    __extends(EventListCtrl, _super);
    function EventListCtrl(_$q, _$scope, _Activities, _DataBus, _eventsMethods, _Util, _Maps, _locStorage, _AdSlot, Events) {
        // call super class
        _super.call(this, {
            listName: 'event',
            resultsKey: 'events',
            service: Events,
        }, _$scope, _$q, _Activities, _AdSlot, _Maps, _DataBus, _Util, _locStorage);
        this._$q = _$q;
        this._$scope = _$scope;
        this._Activities = _Activities;
        this._DataBus = _DataBus;
        this._eventsMethods = _eventsMethods;
        this._Util = _Util;
        this._Maps = _Maps;
        this._locStorage = _locStorage;
        this._AdSlot = _AdSlot;
        this._init();
    }
    EventListCtrl.prototype._init = function () {
        var self = this, scope = this._$scope;
        _.defer(function () {
            scope.methods = new self._eventsMethods({}, scope.vars.items);
        });
    };
    /**
     * @description formats event items
     * @param {array<object>} items
     * @returns {array<object>}
     * @private
     */
    EventListCtrl.prototype._formatItems = function (items) {
        var methods = this._$scope.methods;
        methods.addEvents(items);
        return methods.format(items);
    };
    EventListCtrl.prototype.updateList = function (items, replace) {
        return this._$scope.methods[replace ? 'replaceGroups' : 'stitchGroups'](this.vars.items, items);
    };
    EventListCtrl.prototype.loadItemFromMarker = function (marker) {
        var scope = this._$scope, DataBus = this._DataBus, deferred = this._$q.defer();
        this.loadSingle(marker.model._id, marker.model._type)
            .then(function (eventGroups) {
            scope.vars.items = eventGroups;
            _.defer(deferred.resolve);
            scope.$root.safeDigest(scope);
        }).catch(function (err) {
            err.type = 'activities.ERROR-EVENT_NOTFOUND';
            DataBus.emit('yapServerResponse', err);
            deferred.reject();
        });
        return deferred.promise;
    };
    /**
     * @description called when a cluster is clicked and we are going to fetch the events from the server
     * using the id's and types passed in from this marker set
     * @param markerSet
     * @returns {{eID: any, type: any}[]}
     * @private
     */
    EventListCtrl.prototype._pluckClusterIDs = function (markerSet) {
        return _.map(markerSet, function (marker) {
            return {
                eID: marker._id,
                type: marker._type,
            };
        });
    };
    EventListCtrl.prototype.loadSingle = function (eID, type) {
        var DataBus = this._DataBus;
        DataBus.emit('progressLoader', { start: true });
        return this._$scope.methods.loadEvent([eID], type)["finally"](function () {
            return DataBus.emit('progressLoader');
        });
    };
    return EventListCtrl;
})(List.ListCtrl);
module.exports = EventListCtrl;
//# sourceMappingURL=ListCtrl.js.map