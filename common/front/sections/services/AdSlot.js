/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var instance = null, slots = {
    peopleListtandems: null,
    peopleListteachers: [{
            index: 1,
            template: 'views/services/AdSlot/templates/addSelfSpeakerList/Template.html',
            expr: function (listLength) {
                // Only show for unauthenticated users if 1 item on the Activity search page
                return listLength === 1 &&
                    !instance._Auth.isAuthenticated() &&
                    /^activity\.search/.test(instance._$state.$current);
            }
        }, {
            index: 2,
            template: 'views/services/AdSlot/templates/addSelfSpeakerList/Template.html',
            expr: function (listLength) {
                // Only run for unauthenticated users on the Activity search page
                return !instance._Auth.isAuthenticated() &&
                    /^activity\.search/.test(instance._$state.$current);
            }
        }]
};
slots.peopleListtandems = slots.peopleListteachers;
var AdSlot = (function () {
    function AdSlot(_Auth, _$state) {
        this._Auth = _Auth;
        this._$state = _$state;
        instance = this;
    }
    AdSlot.prototype.getSlots = function (name) {
        return slots[name];
    };
    return AdSlot;
})();
module.exports = AdSlot;
//# sourceMappingURL=AdSlot.js.map