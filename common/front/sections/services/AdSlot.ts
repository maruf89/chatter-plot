/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var instance = null,

    slots = {
        peopleListtandems:null,
        peopleListteachers: [{
            index: 1,
            template: 'views/services/AdSlot/templates/addSelfSpeakerList/Template.html',
            expr: function (listLength:number) {
                // Only show for unauthenticated users if 1 item on the Activity search page
                return listLength === 1 &&
                    !instance._Auth.isAuthenticated() &&
                    /^activity\.search/.test(instance._$state.$current);
            }
        }, {
            index: 2,
            template: 'views/services/AdSlot/templates/addSelfSpeakerList/Template.html',
            expr: function (listLength:number) {
                // Only run for unauthenticated users on the Activity search page
                return !instance._Auth.isAuthenticated() &&
                    /^activity\.search/.test(instance._$state.$current);
            }
        }]
    };

slots.peopleListtandems = slots.peopleListteachers;

class AdSlot {
    constructor(
        private _Auth:any,
        private _$state:ng.ui.IStateService
    ) {
        instance = this;
    }

    getSlots(name) {
        return slots[name];
    }
}

module.exports = AdSlot;
