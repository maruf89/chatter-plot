/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

"use strict";

/**
 * @namespace Util~languages
 */

var Util,
    langTranslations = Object.freeze(require('root/config/languages')),
    translated;

export var Language = {

    /**
     * @ngdoc method
     * @description converts an languageIDs into an objects with the `languageID` key
     *
     * @param   {array} langKeys  {array} of {number} languageIDs
     * @returns {array}           returns an {array} of {object}s
     */
    arrayToObject: function (langKeys) {
        return _.map(langKeys, function (key) {
            return {
                languageID: key
            };
        });
    },

    /**
     * @ngdoc method
     * @description Same as the below method except that some, if not all the passed in
     * languages may already have their translations
     *
     * @param  {array} languages  Array of language objects w/ out `name` property
     * @returns {Promise<object>} Complete object
     */
    expandPossible: function (languages) {
        var deferred = Util._$q.defer(),
            missingKeys = [],
            missing = [],
            complete = [];

        _.each(languages, function (lang) {
            if (lang.name) {
                // Append a variable denoting the language level if it exists to trigger
                // any listeners check to see if the object changed at all
                lang.llID = lang.languageID + '-' + (lang.level || '');
                return complete.push(lang);
            } else {
                missingKeys.push(p(lang.languageID));
                return missing.push(lang);
            }
        });

        if (missingKeys.length) {
            Util.language.expand(missing, missingKeys).then(function (obj) {
                return deferred.resolve(complete.concat(obj));
            });
        } else {
            deferred.resolve(complete);
        }

        return deferred.promise;
    },

    /**
     * @ngdoc method
     * @description Taken either an array of object with `languageID` or an array of ID's
     * Will return a complete array of object with names attached to ID's
     *
     * @param  {array}  languages  Array of language Objects containing `languageID` key
     * @param  {Array=} langKeys   Array of Integers
     * @returns {array}            Array of objects with `name` and `languageID` - If languages
     *                             were passed in, then `name` will be added to those objects
     */
    expand: function (languages, langKeys) {
        var deferred = Util._$q.defer(),
            cacheKey;

        // Extract languageID's from each languag object, then make sure they're all numbers, then sort for the server
        langKeys = langKeys || _.filter(_.pluck(languages, 'languageID'), function (key) {
                return typeof key === 'number';
            });

        cacheKey = langKeys.sort().join(',');
        Util._locale.ready('languages').then(function () {
            var complete = _.map(langKeys, function (key) {
                var obj = null,
                    name = Util._locale.getString('languages.' + langTranslations[key]);

                // if we have the original language object, let's use it
                if (languages) {
                    _.each(languages, function (source) {
                        if (source.languageID === key) {
                            obj = source;
                            return false;
                        }
                    });

                    if (!obj) {
                        return null;
                    }

                    obj.llID = obj.languageID + '-' + obj.level;
                    obj.name = name;
                    return obj;
                }

                // Otherwise return a new object
                return {
                    llID: key + '-',
                    languageID: key,
                    name: name
                };
            });

            Util._locStorage.set('profile:languages:cache', cacheKey);
            Util._locStorage.set('profile:languages', complete);
            return deferred.resolve(complete);
        });
        return deferred.promise;
    },

    /**
     * @description Saves the complete language object to cache to prevent a server request on later calls.
     *
     * @param  {array} complete - Array of language objects with `name` properties & everything
     */
    save2Storage: function (complete) {
        var cacheKey,
            langKeys = _.filter(_.pluck(complete, 'id'), function (key) {
                return typeof key === 'number';
            });

        cacheKey = langKeys.sort().join(',');
        Util._locStorage.set('profile:languages:cache', cacheKey);
        Util._locStorage.set('profile:languages', complete);
    },

    /**
     * @description converts and array of keys
     * @param {array<string>} languages
     * @returns {array<string>}
     */
    getTranslations: function (languages) {
        return Util._locale.ready('languages')
            .then(function () {
                return _.map(languages, function (lang) {
                    return Util._locale.getString(Util.language.getI18nKey(lang));
                });
            });
    },

    /**
     * @ngdoc method
     * @description converts a language ID into an i18n key
     * @param {number} languageID
     * @returns {string} i18n key
     */
    getI18nKey: function (languageID) {
        return 'languages.' + langTranslations[languageID];
    },

    i18n2ID: function (i18nKey) {
        return langTranslations.indexOf(i18nKey);
    },

    /**
     * @ngdoc property
     * @description holds reference to the immutable languages object. Useful for getting the english translation
     * @type {array}
     */
    baseLanguagesArr: langTranslations,

    /**
     * @ngdoc method
     * @description Loads the translated languages file and returns it
     *
     * @param {boolean=} withAll  Whether to return languageID index 0 which is 'ALL_LANGS' (default:false)
     * @returns {Promise}
     * @returns {array} array of translated languages
     */
    getTranslated: function (withAll) {
        if (translated) {
            return Util._$q.when(Util.language._getTranslated(withAll));
        }
        return Util._locale.ready('languages')
            .then(function () {
                translated = [];
                _.each(langTranslations, function (key, index) {
                    return translated[index] = Util._locale.getString(
                        "languages." + key);
                });
                return Util.language._getTranslated(withAll);
            });
    },

    _getTranslated: function (withAll) {
        if (withAll) {
            return translated;
        } else {
            return translated.slice(1);
        }
    },

    /**
     * @ngdoc method
     * @description clears any cached translations. Useful if the locale changes and a retranslation is needed
     */
    clearTranslations: function () {
        return translated = null;
    },
};

export function get(_Util) {
    Util = _Util;

    return {
        language: Language
    };
};
