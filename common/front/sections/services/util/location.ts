/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

interface ILocationHistState {
    count:number
    type:string
}

var Util,
    currentState,
    isBack,
    isForward,
    counter = 0,
    initial = 0,
    _search = 0,
    states = {
        back: [],
        forward: []
    },

    _histType = {
        hist: [],
        get: function ():(ILocationHistState|boolean) {
            var last:ILocationHistState,
                len:number = this.hist.length;

            if (!len) {
                return false;
            }

            last = this.hist[len - 1];

            if (!last.count) {
                this.hist.pop();
                return this.get();
            }

            return last;
        },

        replace: function (type:string):void {
            var current:ILocationHistState = this.get();
            current.type = type;
        },

        add: function (type:string):number {
            var current:ILocationHistState = this.get();

            if (current.type === type) {
                return current.count++;
            }

            return this.hist.push(<ILocationHistState>{
                count: 1,
                type: type
            });
        }
    };



export var Location = _.extend({
    isBack: function () {
        return !!isBack;
    },

    isForward: function () {
        return !!isForward;
    },

    isHistory: function () {
        return !!(isBack || isForward);
    },

    canShowBack: function () {
        return !!states.back.length;
    },

    /**
     * @description On history change, we check whether it's a forward event.
     * If not, we deduce that it's a back event
     */
    _popstateChange: function () {
        isBack = isForward = null;

        var histType = _histType.get(),
            isSearch = histType && histType.type === 'search',
            now = decodeURIComponent(isSearch ? window.location.pathname + window.location.search : currentState),
            current;

        if (states.forward.length) {
            if (now === decodeURIComponent(states.forward[0])) {
                isForward = true;
                currentState = states.forward.shift();
                return Util.location.emit('/history/forward', decodeURIComponent(currentState));
            }
        }
        if (states.back.length) {

            // if now is decodeURIComponent(states.back[length - (if isSearch then 1 else 2)])
            isBack = true;
            if (isSearch) {
                states.forward.unshift(currentState);
                current = _histType.get();
                current && current.count--;
            } else {
                states.forward.unshift(states.back.pop());
            }
            currentState = states.back.pop();
            return Util.location.emit('/history/back', decodeURIComponent(currentState));
        }
    },

    _stateChange: function (event, toState, toParams) {
        isBack = isForward = null;

        if (toParams.redirect) {
            currentState = null;
            return false;
        }

        var url = Util._$state.href(toState.name, toParams);

        return this._pushState(url, 'page');
    },

    _pushState: function (url, type) {
        _histType.add(type);
        url = url || window.location.pathname + window.location.search;

        if (currentState) {
            counter = states.back.push(currentState);
        }

        return currentState = url;
    },

    _replaceState: function (url, type) {
        _histType.replace(type);

        url = url || window.location.pathname + window.location.search;

        currentState = url;
    },

    /**
     * @description Removes the query params from the url and optionally replaces the state
     *
     * @param  {boolean} replace - whether to replace the current history state
     * @param  {array<string>} removeKeys - keys to remove from the url
     */
    stripQP: function (replace:boolean, removeKeys?:string[]):void {
        // Util.$state.transitionTo Util.$state.current.name, {},
        //     location: if replace then 'replace' else false # Set to false if you want it to exist in window.history
        //     inherit: false # inherit URL parameters/resolves
        //     relative: Util.$state.$current
        //     notify: false # If `true` will broadcast $stateChangeStart and $stateChangeSuccess events.
        //     reload: true
        Util._$state.params = Util._$stateParams = Util._$location.$$search = !removeKeys ? {}
            : _.reduce(removeKeys, function (obj, key) {
            delete obj[key];
            return obj;
        }, Util._$location.$$search);

        Util._$location.$$compose();

        if (replace) {
            Util._$location.replace();
        }
    },

    /**
     * @description Same as updateSearch except you can pass an whole object
     * and this will JSON encode and object properties
     * @param  {object}         obj     Object to encode
     * @param  {(boolean|string)} replace pass `replace` to replace window.history
     */
    searchObj: function (obj:any, replace?:boolean) {
        obj = Util.location.encodeParams(obj);
        Util._$location.search(obj);
        //Util._$state.transitionTo(Util._$state.current.name, obj, {
        //    location: replace,
        //    inherit: false,
        //    relative: Util._$state.$current,
        //    notify: false
        //});

        if (replace) {
            this._replaceState('search');
            Util._$location.replace();
        } else {
            _.defer(function () {
                Util.location._pushState(null, 'search');
            });
        }
    },

    encodeParams: function (obj) {
        var search:any = {};

        _.each(obj, function (_obj, key) {
            if (typeof _obj === 'object') {
                return search[key] = JSON.stringify(_obj);
            } else if (_obj) {
                return search[key] = _obj;
            }
        });

        return search;
    },

    /**
     * @description pulls variables from the url & can parse them if they're JSON
     * By default checks the $state.params object for variables
     * @param {string} key - the key to check for
     * @param {boolean} force - if true will decode the actual URL instead of checking $state.params
     * @param {boolean} parse - if true and a value exists will attempt to JSON.parse
     * @returns {*}
     */
    decodeParams: function (key, force, parse) {
        var endOffset,
            match,
            matchLength,
            startOffset,

            search = Util._$stateParams[key];

        if (force) {
            search = decodeURIComponent(location.search.substr(1));

            // If we have a match, we want to only get results of the one query param
            if (search) {
                match = key + "=";
                matchLength = match.length;

                if ((startOffset = search.indexOf(match)) !== -1) {
                    endOffset = search.indexOf('&', startOffset);
                    endOffset = endOffset === -1 ? search.length : endOffset;
                    search = search.substr(startOffset + matchLength, endOffset - startOffset - matchLength);
                } else {
                    search = null;
                }
            }
        }

        if (search && parse) {
            try {
                search = JSON.parse(decodeURIComponent(search));
            } catch (e) {}
        }

        return search;
    }
}, require('node-event-emitter').prototype);

export function get(_Util:any):{ location:any } {
    Util = _Util;

    Util._$rootScope.$on('$stateChangeStart', Location._stateChange.bind(Location));

    _.defer(function () {
        return CP.Cache.$window.on('popstate', Location._popstateChange.bind(Location));
    });

    return { location: Location };
};
