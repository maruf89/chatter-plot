/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

"use strict";

/**
 * @namespace geo
 * @memberof Util
 */
var Util,

    commonVenue = require('common/util/venue/index'),
    commonMap = require('common/util/map'),

    _currentLocation = {
        latitude: null,
        longitude: null,
        innacurate: null,
        zoom: 9
    },

    _getLocInited = false,

    _getMyLocDeferred = [],

    _resolveMyLoc = function (key) {
        var clone = _.clone(_currentLocation);

        // Update Util to resolve the promise right away
        this[key] = function () {
            return Util._$q.when(clone);
        };

        _getMyLocDeferred.length && _.each(_getMyLocDeferred, function (deferred) {
            return deferred.resolve(clone);
        });
    };

export var Geo = {
    getCurrentLocation: function (timeout) {
        if (!navigator.geolocation) {
            return Util._$q.reject('Geolocation not supported');
        }

        var deferred = Util._$q.defer();
        navigator.geolocation.getCurrentPosition(function (geo) {
            var coords = geo.coords;
            if (coords && coords.latitude && coords.longitude) {
                _currentLocation.innacurate = false;
                _currentLocation.latitude = geo.coords.latitude;
                _currentLocation.longitude = geo.coords.longitude;
                return deferred.resolve(_currentLocation);
            }
            return deferred.reject('Location not found');
        });
        if (typeof timeout === 'number') {
            setTimeout(deferred.reject, timeout);
        }
        return deferred.promise;
    },

    /**
     * Starts trying to get the users coordinates
     *
     * @name geo#initGetCurrentCoords
     * @param {Object} User        Preferrable a `User` inject object
     * @param {String} userField   Property in User to check for coords. User[userField] should be an object
     */
    initGetCurrentCoords: function (User) {
        _getLocInited = true;

        if (!Util._$rootScope.isHeadlessBrowser) {
            return Util.geo.getCurrentLocation(5000).then(_resolveMyLoc)["catch"](function () {
                var locs = User && User.data && User.data.locations,
                    coords = _.isArray(locs) && locs[0].coords;

                if (coords) {
                    _currentLocation.latitude = coords.lat || coords.latitude;
                    _currentLocation.longitude = coords.lon || coords.longitude;
                    _currentLocation.zoom = 12;
                    _currentLocation.innacurate = false;
                    return true;
                }

                _currentLocation.innacurate = true;
                _currentLocation.zoom = 8;

                return $.ajax({
                        type: 'GET',
                        url: '//freegeoip.net/json/',
                        async: false,
                        contentType: 'application/json',
                        timeout: 15000,
                        dataType: 'json',
                        cache: true
                    })
                    .fail(function (err) {
                        return console.log(err);
                    })
                    .then(function (res) {
                        _currentLocation.latitude = res.latitude;
                        return _currentLocation.longitude = res.longitude;
                    })
                    .always(_resolveMyLoc);
            });
        }
    },

    /**
     * @description Most stubborn location that checks:
     *      1: Geolocation
     *      2: User's coords
     *      3: IP address
     *      4: Fails :(
     *
     * Returns a promise when something is retrieved
     *
     * @name geo#getMyLocation
     * @returns {Promise<object>} location
     * @returns {number} location.zoom
     * @returns {float} location.latitude
     * @returns {float} location.longitude
     * @returns {boolean} innacurate - whether the returned coordinates are not accurate
     */
    getMyLocation: function () {
        var deferred = Util._$q.defer();

        _getMyLocDeferred.push(deferred);

        if (!_getLocInited) {
            Util.geo.initGetCurrentCoords();
        }

        return deferred.promise;
    },

    /**
     * Returns a new lat/lng at a random point within a set radius
     *
     * @param  {number}  within - radius in meters
     * @param  {number} within - within how may kilometers
     * @param  {number=} format - null/0 = {latitude, longitude} | 1 = {lat, lon} | 2 = [latitude, longitude]
     * @return {(object|array)} depending on the requested format returns 'latitude' and 'longitude'
     */
    rndPtWithin: function (coords, within, format) {
        var r, t, u, v, w, x, x0, x1, y0, y1;
        r = within / 111300;
        y0 = coords.lat || coords.latitude;
        x0 = coords.lon || coords.longitude;
        u = Math.random();
        v = Math.random();
        w = r * Math.sqrt(u);
        t = 2 * Math.PI * v;
        x = w * Math.cos(t);
        y1 = w * Math.sin(t);
        x1 = x / Math.cos(y0);
        switch (format) {
            case 1:
                return {
                    lat: y0 + y1,
                    lon: x0 + x1
                };
            case 2:
                return [y0 + y1, x0 + x1];
            default:
                return {
                    latitude: y0 + y1,
                    longitude: x0 + x1
                };
        }
    },

    bounds2Diameter: commonMap.bounds2Diameter,

    /**
     * Converts 4 coordinates represented as a String separated by commas (no spaces)
     * and returns a LatLngBounds object
     *
     * @param   {string} string     the coordinates
     * @returns {LatLngBounds}
     */
    bounds2GoogleBounds: function (string) {
        var bounds, ne, sw;
        bounds = _.map(string.split(','), function (a) {
            return parseFloat(a, 10);
        });
        sw = new google.maps.LatLng(bounds[0], bounds[1]);
        ne = new google.maps.LatLng(bounds[2], bounds[3]);
        return new google.maps.LatLngBounds(sw, ne);
    },

    latlng2Address: function (latLng) {
        if (typeof latLng === 'object') {
            latLng = (latLng.lat || latLng.latitude) + ',' + (latLng.lon ||
                latLng.longitude);
        }

        return Util._$http.get(
                "//maps.googleapis.com/maps/api/geocode/json?latlng=" +
                latLng + "&sensor=true")
            .then(function (results) {
                return results.data.results;
            });
    },

    toGMapUrl: _.partial(commonVenue.addressObjToUrl, 'google'),

    format: {
        address: commonVenue.objToAddress,
    },

    bounds: {
        goog2Es: function (bounds) {
            var NE = bounds.getNorthEast(),
                SW = bounds.getSouthWest();
            return {
                top_left: {
                    lat: NE.lat(),
                    lon: SW.lng()
                },
                bottom_right: {
                    lat: SW.lat(),
                    lon: NE.lng()
                },
                toUrl: function () {
                    return this.top_left.lat + "," + this.top_left.lon +
                        "," + this.bottom_right.lat + "," + this.bottom_right
                            .lon;
                }
            };
        }
    },

    getTimezone: function (coords, date, withoutDST) {
        var url = '//api.timezonedb.com?format=json' +
            '&timestamp=' + date.getTime() +
            '&key=' + CP.Settings.timezoneDB.apiKey +
            '&lat=' + coords.lat +
            '&lng=' + coords.lon;

        return Util._$http({
            method: 'GET',
            url: url,
        }).then(function (body) {
            var response = body.data,
                offset;

            if (response.status !== 'OK') {
                return deferred.reject(response);
            }

            offset = parseInt(response.gmtOffset, 10) / 60;

            // if we want the locations tz without daylight savings & DST is active
            if (withoutDST && response.dst === '1') {
                offset -= 60;
            }

            return offset;
        });
    },
};

export function get(_Util) {
    Util = _Util;

    _resolveMyLoc = _resolveMyLoc.bind(Geo, 'getMyLocation');

    return { geo: Geo };
};
