/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
// There are utility methods for specific sections. attach them
var Language = require('root/services/util/language');
var Location = require('root/services/util/location');
var Geo = require('root/services/util/geo');
var moment = require('moment'), commonGeneral = require('common/util/general'), helpers = [
    Language.get,
    Location.get,
    Geo.get,
];
/**
 * @class
 * @classdesc everything that's utility
 */
exports.Util = function (_locStorage, _$q, _$rootScope, _locale, _$location, _$state, _$stateParams, _$http, _$scroll) {
    this._locStorage = _locStorage;
    this._$q = _$q;
    this._$rootScope = _$rootScope;
    this._locale = _locale;
    this._$location = _$location;
    this._$state = _$state;
    this._$stateParams = _$stateParams;
    this._$http = _$http;
    this._$scroll = _$scroll;
    // Initiate each helper
    _.each(helpers, function (helper) {
        return exports.Util.prototype = _.extend(exports.Util.prototype, helper(this));
    }, this);
    this.deferX = this.deferX.bind(this);
};
exports.Util.prototype = _.extend(exports.Util.prototype, {
    /**
     * @description Saves the complete language object to cache to prevent a server request on later calls.
     *
     * @name Util#saveLanguagesObj
     * @param {array<number>} complete - Array of language objects with `name` properties & everything
     */
    saveLanguagesObj: function (complete) {
        var langKeys = _.filter(_.pluck(complete, 'id'), function (key) {
            return typeof key === 'number';
        }), cacheKey = langKeys.sort().join(',');
        this._locStorage.set('profile:languages:cache', cacheKey);
        this._locStorage.set('profile:languages', complete);
    },
    /**
     * @description Pads a string with leading characters
     *
     * @name Util#pad
     * @param  {string} str - String to pad
     * @param  {number} max - The length the string should be
     * @param  {string=} what - what to pad the string with (default: '0')
     * @returns {string} Padded string
     */
    pad: function (str, max, what) {
        if (what == null) {
            what = '0';
        }
        str = str.toString();
        return str.length < max ? this.pad(what + str, max) : str;
    },
    /**
     * Checks a passed in object that it has all the fields in
     *
     * @name Util#lacksFields
     * @param  {array}  fields    Array of strings or object
     *                                Objects contain: { name: <{string} >, children: <{array}> }
     *                                children recursively calls this
     * @param  {object} object    The object to check over
     * @return {Null|String}      Null if nothing missing OR string of the missing field
     */
    lacksFields: function (fields, object) {
        var missingField;
        if (_.every(fields, function (field) {
            if (typeof field === 'string') {
                return object[missingField = field] != null;
            }
            else {
                return (object[missingField = field.name] != null) &&
                    !(missingField = this.lacksFields(field.children, object[field.name]));
            }
        })) {
            return null;
        }
        return missingField;
    },
    /**
     * Basically _.defaults but deep
     *
     * @param  {object} obj - the object to extend
     * @param  {object} source - the source object to extend from
     */
    defaultsDeep: function (obj, source) {
        return _.each(source, function (val, key) {
            var type;
            if (_.isPlainObject(val)) {
                return this.defaultsDeep((obj[key] = obj[key] || {}), val);
            }
            else {
                if (!obj[key] && (type = typeof obj[key]) !==
                    'string' && type !== 'number' && type !==
                    'boolean') {
                    return obj[key] = val;
                }
            }
        }, this);
    },
    queryStringSerialize: function (obj, prefix) {
        var k, p, v, str = [];
        for (p in obj) {
            if (obj.hasOwnProperty(p)) {
                k = (prefix ? prefix + "[" + p + "]" : p);
                v = obj[p];
                str.push(typeof v === 'object' ?
                    this.queryStringSerialize(v, k)
                    :
                        encodeURIComponent(k) + '=' + encodeURIComponent(v));
            }
        }
        return str.join('&');
    },
    encodeSearchParams: function (obj) {
        return encodeURIComponent(JSON.stringify(obj));
    },
    /**
     * @description encodes an action object to a string.
     * IMPORTANT: if encoding action, objID and/or type ALWAYS encode in the correct order:
     *
     *          action -> objID -> type
     *
     * @param {object} data
     * @returns {string}
     */
    encodeActionArgs: function (data) {
        return _.map(data, function (value, key) {
            return key + ':' + value;
        }).join('|');
    },
    /**
     * @description Parses a string formatted like `key:value|key:value` etc… into an object
     * Used for query string action + args
     *
     * @param  {string} args
     * @return {object}
     */
    parseActionArgs: function (args) {
        args = decodeURIComponent(args);
        return _.reduce(args.split('|'), function (obj, str) {
            var ref = str.split(':');
            obj[ref[0]] = ref[1];
            return obj;
        }, {});
    },
    /**
     * Recursively iterates the 2 objects and gets the differences
     *
     * @param  {object} prev  The previous object to check against
     * @param  {object} now   New object
     * @return {object}       Returns a new object with just the differences
     */
    objectDifferences: function (prev, now) {
        var c, prop, changes = {};
        for (prop in now) {
            if (!prev || prev[prop] !== now[prop]) {
                if (_.isPlainObject(now[prop])) {
                    c = this.objectDifferences(prev[prop], now[prop]);
                    // underscore
                    if (!_.isEmpty(c)) {
                        changes[prop] = c;
                    }
                }
                else if (_.isArray(now[prop])) {
                    if (prev[prop].length !== _.union(changes[prop], now[prop]).length) {
                        changes[prop] = now[prop];
                    }
                }
                else {
                    changes[prop] = now[prop];
                }
            }
        }
        return changes;
    },
    /**
     * @description clones an object only `depth` levels deep. Beyond that returns shallow copies
     * @param {(object|array)} obj - what to clone
     * @param {number} depth - how many levels deep to clone maximum
     * @returns {(object|array)}
     */
    cloneXDeep: function recurse(obj, depth) {
        var clone;
        if (typeof obj === 'object') {
            clone = _.isArray(obj) ? [] : {};
            _.each(obj, function (val, field) {
                if (depth && typeof val === 'object') {
                    clone[field] = recurse(val, depth - 1);
                }
                else {
                    clone[field] = val;
                }
            });
            return clone;
        }
        return obj;
    },
    /**
     * Converts an array of language keys to an array of translated values
     *
     * @param  {array} langKeys  keys
     * @return {array}           translated
     */
    concatTranslations: function (langKeys) {
        var context = this;
        return this._$q.all(_.map(langKeys, function (key) {
            return context._locale.ready(context._locale.getPath(key));
        }))
            .then(function () {
            return _.map(langKeys, function (key) {
                return context._locale.getString(key);
            });
        });
    },
    /**
     * Removes an element from an array if it exists
     *
     * @param  {array<*>} array - the array to modify
     * @param  {*} what - the value to remove from the array if it exists
     */
    arrRemove: commonGeneral.array.remove,
    obj: {
        notEmpty: function (obj) {
            var key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) {
                    return true;
                }
            }
            return false;
        },
        size: function (obj) {
            var key, size;
            size = 0;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) {
                    size++;
                }
            }
            return size;
        }
    },
    $parentScroll: function ($parent, relative, amount, duration) {
        var base = relative ? $parent.scrollTop() : 0;
        $parent.animate({
            scrollTop: base + amount
        }, duration || 350);
    },
    /**
     * @description returns whether an element is hidden in the browser (not visible)
     * @name Util#$isHidden
     * @param {object} $elem     The element we're checking
     * @returns {boolean}
     */
    $isHidden: function ($elem) {
        var elem;
        elem = $elem instanceof jQuery ? $elem[0] : $elem;
        return elem.offsetParent === null;
    },
    $hilite: function ($elem, delay) {
        if (delay == null) {
            delay = 1000;
        }
        $elem.addClass('can-hilite hilite');
        return setTimeout($elem.removeClass.bind($elem, 'hilite'), delay);
    },
    /**
     * @description accepts an i18n key and an optional array of dynamic variables to set the key string to
     * @name Util#getI18nVars
     *
     * @example
     *      // returns 'Find Tandem Partners'
     *      Util.getI18nVars('activities.FIND_X', [{
     *          key: 'x'
     *          value: 'tandem.PARTNERS'
     *      }])
     *
     * @param {string} key - i18n key
     * @param {array<object>=} variables
     * @param {string} variables[0].key - the key of the object
     * @param {string} variables[0].value - i18n key
     */
    getI18nVars: function (key, variables) {
        var hasVars = null, promises = [this._locale.ready(this._locale.getPath(key))], context = this;
        if (_.isArray(variables) && variables.length) {
            hasVars = true;
            _.each(variables, function (obj) {
                return promises.push(this._locale.ready(this._locale.getPath(obj.value)));
            }, this);
        }
        return this._$q.all(promises)
            .then(function () {
            var args = [key];
            if (hasVars) {
                args.push(_.reduce(variables, function (obj, next) {
                    obj[next.key] = context._locale.getString(next.value);
                    return obj;
                }, {}));
            }
            return context._locale.getString.apply(context._locale, args);
        });
    },
    /**
     * @description runs _.defer recursively for x + 1 times
     * @param {function} fn - callback
     * @param {number=} timesPlus1 - will call defer for this number + 1 times
     */
    deferX: function (fn, timesPlus1) {
        if (!timesPlus1) {
            _.defer(fn);
        }
        var context = this;
        _.defer(function () {
            context.deferX(fn, --timesPlus1);
        });
    }
});
//# sourceMappingURL=index.js.map