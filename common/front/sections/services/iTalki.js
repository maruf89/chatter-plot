/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
var iTalki = (function () {
    function iTalki(_Util) {
        this._Util = _Util;
        this._base = 'http://www.italki.com';
        this._path = '/teachers/professional';
        this._affString = '?ref=affchatterplot&utm_source=affchatterplot&utm_medium=partner&utm_campaign=affiliate';
        this._searchHash = '#teach=<teach>&availability=9&within=-1&speak=<speak>&page=1&tags=&child-tags=';
        this._langMap = {
            'ALL_LANGS': 'all',
            'SCOTTISH_GAELIC': 'gaelic (scottish)',
            'MANDARIN': 'chinese',
            'CHINESE_TAIWAN': 'chinese(taiwanese)',
            'CANTONESE': 'chinese(cantonese)',
            'IRISH': 'gaelic(irish)',
            'PERSIAN': 'persian(farsi)',
        };
        this._searchHash = this._searchHash.replace('<speak>', this._langKey2Text(CP.site.language));
    }
    iTalki.prototype.buildURL = function (language) {
        if (typeof language !== 'number') {
            language = 0;
        }
        return this._base +
            this._path +
            this._affString +
            this._searchHash.replace('<teach>', this.getLanguage(language));
    };
    iTalki.prototype.getLanguage = function (langID) {
        var key = this._Util.language.baseLanguagesArr[langID];
        return this._langKey2Text(key);
    };
    iTalki.prototype._langKey2Text = function (key) {
        return this._langMap[key] || key.toLowerCase();
    };
    return iTalki;
})();
module.exports = iTalki;
//# sourceMappingURL=iTalki.js.map