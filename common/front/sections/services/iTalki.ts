/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

"use strict";

class iTalki implements chatterplot.services.iTalkiService {
    private _base:string = 'http://www.italki.com';
    private _path:string = '/teachers/professional';
    private _affString:string = '?ref=affchatterplot&utm_source=affchatterplot&utm_medium=partner&utm_campaign=affiliate';
    private _searchHash:string = '#teach=<teach>&availability=9&within=-1&speak=<speak>&page=1&tags=&child-tags=';

    private _langMap:any = {
        'ALL_LANGS': 'all',
        'SCOTTISH_GAELIC': 'gaelic (scottish)',
        'MANDARIN': 'chinese',
        'CHINESE_TAIWAN': 'chinese(taiwanese)',
        'CANTONESE': 'chinese(cantonese)',
        'IRISH': 'gaelic(irish)',
        'PERSIAN': 'persian(farsi)',
    };

    constructor(
        private _Util:any
    ) {
        this._searchHash = this._searchHash.replace('<speak>', this._langKey2Text(CP.site.language));
    }

    public buildURL(language):string {
        if (typeof language !== 'number') { language = 0; }

        return  this._base +
                this._path +
                this._affString +
                this._searchHash.replace('<teach>', this.getLanguage(language));
    }

    public getLanguage(langID:number):string {
        var key = this._Util.language.baseLanguagesArr[langID];

        return this._langKey2Text(key);
    }

    private _langKey2Text(key:string):string {
        return this._langMap[key] || key.toLowerCase();
    }
}

module.exports = iTalki;
