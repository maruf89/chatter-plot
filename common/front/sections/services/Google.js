/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var Google = (function () {
    function Google(_$q, _GoogleMapApi, _Maps) {
        this._$q = _$q;
        this._GoogleMapApi = _GoogleMapApi;
        this._Maps = _Maps;
        this._searchAPI = null;
    }
    /**
     * @description A Google Maps Places request
     * Accepts a location with sourceID and fetches it's entire object
     *
     * @param  {object} source
     * @param  {string} source.ID - the places unique Google ID (ChIJDUPkT0a7woARX2WjOrRbgGM)
     * @return {Promise<object>} Contains the location
     */
    Google.prototype.getMapsPlace = function (sourceID) {
        var deferred = this._$q.defer(), instance;
        this._GoogleMapApi.then(function (maps) {
            var map;
            if (!this._searchAPI) {
                if (instance = this._Maps.getInstance()) {
                    map = instance.rootObject();
                    // we want to unset this variable on destroy
                    instance.addOnDestroyFn(this._mapDestroyed.bind(this));
                }
                else {
                    map = new google.maps.Map(document.createElement('div'), {
                        center: { lat: -33.8688, lng: 151.2195 },
                        zoom: 13
                    });
                }
                this._searchAPI = new maps.places.PlacesService(map);
            }
            return this._searchAPI.getDetails({
                placeId: sourceID
            }, function (place, res) {
                if (res === 'OK') {
                    return deferred.resolve(place);
                }
                return deferred.reject(place);
            });
        }.bind(this));
        return deferred.promise;
    };
    Google.prototype._mapDestroyed = function () {
        this._searchAPI = null;
    };
    return Google;
})();
module.exports = Google;
//# sourceMappingURL=Google.js.map