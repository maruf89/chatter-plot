/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var commonVenue = require('common/util/venue/index');

/**
 * @module fVenues
 * @ngdoc controller
 * @param {$q} $q
 * @param {SocketIo} SocketIo
 * @param {Util} Util
 * @returns {Venues}
 * @constructor
 */
class Venues implements cp.venue.IService {
    public defaultFields:cp.venue.IVenueFull = commonVenue.DEFAULT_FIELDS;

    public getCoords:(
        place:google.maps.places.PlaceResult,
        serviceName?:string
    ) => cp.map.ICoordinates = commonVenue.getCoords;

    public scrapePlace:(
        place:google.maps.places.PlaceResult,
        serviceName:string,
        getSpecificity?:number
    ) => cp.venue.IVenueFull = commonVenue.scrapePlace;

    public addressStringToUrl:(
        serviceName:string,
        ...args:string[]
    ) => string = commonVenue.addressStringToUrl;

    public scrapeSpecificity:(
        place:google.maps.places.PlaceResult,
        serviceName?:string
    ) => cp.venue.ISpecificity = commonVenue.scrapeSpecificity;

    public place2VenueObj:(
        place:google.maps.places.PlaceResult,
        serviceName:string
    ) => cp.venue.IVenueObj = commonVenue.place2VenueObj;

    public addressObjToUrl:(
        serviceName:string,
        addressObj:any
    ) => string = commonVenue.addressObjToUrl;

    public getPlace:(
        source:(cp.venue.IVenue|cp.user.ILocation)
    ) => ng.IPromise<google.maps.places.PlaceResult> = commonVenue.getPlace;

    public getPlaceBulk:(
        Q:ng.IQService,
        venues:cp.venue.IVenue[],
        format?:boolean
    ) => ng.IPromise<cp.venue.IVenueObj[]> = commonVenue.getPlaceBulk;

    public formatBulk:(
        venues:cp.venue.IVenue[],
        format:number,
        fetchedLocations:google.maps.places.PlaceResult[]
    ) => any[] = commonVenue.formatBulk;

    public venues:cp.venue.IVenueCache = {};

    constructor(
        private _SocketIo:any,
        private _$q:ng.IQService,
        Google
    ) {
        // Register the google get places fn for front end
        commonVenue.serviceConfig('google', {
            getPlace: Google.getMapsPlace.bind(Google),
            Q: _$q,
        });
    }

    /**
     * @description returns a venue cached in memory or fetches it, saves it and returns it
     *
     * @param {object} venue
     * @param {number} venue.vID
     * @param {boolean=} withPlace - if passed will require that the venue have the source service data
     * @returns {object} - the complete venue Object (or what's there of it)
     */
    public getCacheFormat(venue:cp.venue.IVenue, withPlace?:boolean):ng.IPromise<cp.venue.IVenueObj> {
        var venues = this.venues,
            cached = venues[venue.vID];

        // if we have the entire object, or don't need entire venue but have a part then return that
        if (cached && (cached.service || !withPlace)) {
            return this._$q.when(venues[venue.vID]);
        }

        return this.getPlace(venue)
            .then(this.place2VenueObj.bind(this))
            .then(function (venueObj:cp.venue.IVenueObj) {
                venues[venue.vID] = venueObj;
                return venueObj;
            })
    }

    /**
     * @description stashes a venue in memory
     *
     * @param {object} venueObj
     */
    public cacheVenue(venueObj:cp.venue.IVenueFull):void {
        var venues:cp.venue.IVenueCache = this.venues;

        // If there's no provided venue ID, or we already have the complete object
        // Do Nothing!
        if (!venueObj.vID ||
            (venues[venueObj.vID] && venues[venueObj.vID].service)
        ) { return; }

        venues[venueObj.vID] = {
            service: null,
            venue: venueObj,
            user: venueObj,
        }
    }

    /**
     * @description Saves the event to the database
     *
     * @param  {Array} venues - Array of venue Objects
     * @param {boolean} combine - whether to extend the passed in objects with the returned saved objects
     * @return {Promise}       Promise will return on object like: { <venueID>: <{object} details> }
     */
    public save(venues:any, combine?:boolean):ng.IPromise<cp.venue.IVenue[]> {
        if (!_.isArray(venues)) {
            venues = [venues];
        }

        var saveVenues = this.sanitize(venues);

        var request:ng.IPromise<any> = this._SocketIo.onEmitSock('/venue/save', saveVenues),
            self = this;

        if (combine) {
            request.then(function (extendedVenues) {
                return self.combineVenueArray(venues, extendedVenues);
            });
        }

        return request;
    }

    public sanitize(venues:cp.venue.IVenue[]):cp.venue.IVenue[] {
        var fields = [
            'coords',
            'name',
            'type',
            'google',
            'yelp',
        ];

        return _.map(venues, function (venue:cp.venue.IVenue):any {
            return _.pick(venue, fields);
        });
    }

    /**
     * @description Iterates over 2 arrays of venues, and extends the source array with the extended
     * arrays values
     *
     * @param {Array<object>} source
     * @param {Array<object>} extended
     */
    public combineVenueArray(source:cp.venue.IVenue[], extended:cp.venue.IVenue[]):void {
        _.each(source, function (venue:cp.venue.IVenue):void {
            _.each(extended, function (fullVenue:cp.venue.IVenue):boolean {
                if ((venue.google && venue.google === fullVenue.google) ||
                    (venue.yelp && venue.yelp === fullVenue.yelp)
                ) {
                    // This adds a [0] property when extending - avoid
                    //_.extend(venue, extended);

                    venue.vID = fullVenue.vID;
                    venue.tz = fullVenue.tz;
                    return false;
                }
            });
        });
    }

    public getPhotoSrc(venue:google.maps.places.PlaceResult, height, width):string {
        var isHome:boolean,
            photo:google.maps.places.PlacePhoto;

        if (_.isArray(venue.photos) && (photo = venue.photos[0])) {
            return photo.getUrl({
                maxHeight: height,
                maxWidth: width
            });
        } else {
            isHome = venue.types && venue.types.indexOf('establishment') === -1;
            return '/images/venue/' + (isHome ? 'home' : 'business')+ '.svg';
        }
    }

    /**
     * @description gets the timezone for a specific venue
     * @param vID
     */
    public getTz(vID:number):ng.IPromise<number> {
        var cached = this.venues[vID];

        if (cached && (cached = cached.venue) && cached.tz) {
            return this._$q.when(cached.tz);
        }

        return this._SocketIo.onEmitSock('/venue/getTz', vID).then(function (tz:number) {
            if (cached) {
                cached.tz = tz;
            }

            return tz;
        }).catch(function (err) {
            throw err;
        });
    }
}

module.exports = Venues;
