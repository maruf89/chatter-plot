/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

import {EventEmitter} from 'common/front/modules/EventEmitter';

export class DataBus extends EventEmitter {
    constructor() {
        super();
        this.setMaxListeners(15);
    }
}
