/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var commonVenue = require('common/util/venue/index');
/**
 * @module fVenues
 * @ngdoc controller
 * @param {$q} $q
 * @param {SocketIo} SocketIo
 * @param {Util} Util
 * @returns {Venues}
 * @constructor
 */
var Venues = (function () {
    function Venues(_SocketIo, _$q, Google) {
        this._SocketIo = _SocketIo;
        this._$q = _$q;
        this.defaultFields = commonVenue.DEFAULT_FIELDS;
        this.getCoords = commonVenue.getCoords;
        this.scrapePlace = commonVenue.scrapePlace;
        this.addressStringToUrl = commonVenue.addressStringToUrl;
        this.scrapeSpecificity = commonVenue.scrapeSpecificity;
        this.place2VenueObj = commonVenue.place2VenueObj;
        this.addressObjToUrl = commonVenue.addressObjToUrl;
        this.getPlace = commonVenue.getPlace;
        this.getPlaceBulk = commonVenue.getPlaceBulk;
        this.formatBulk = commonVenue.formatBulk;
        this.venues = {};
        // Register the google get places fn for front end
        commonVenue.serviceConfig('google', {
            getPlace: Google.getMapsPlace.bind(Google),
            Q: _$q,
        });
    }
    /**
     * @description returns a venue cached in memory or fetches it, saves it and returns it
     *
     * @param {object} venue
     * @param {number} venue.vID
     * @param {boolean=} withPlace - if passed will require that the venue have the source service data
     * @returns {object} - the complete venue Object (or what's there of it)
     */
    Venues.prototype.getCacheFormat = function (venue, withPlace) {
        var venues = this.venues, cached = venues[venue.vID];
        // if we have the entire object, or don't need entire venue but have a part then return that
        if (cached && (cached.service || !withPlace)) {
            return this._$q.when(venues[venue.vID]);
        }
        return this.getPlace(venue)
            .then(this.place2VenueObj.bind(this))
            .then(function (venueObj) {
            venues[venue.vID] = venueObj;
            return venueObj;
        });
    };
    /**
     * @description stashes a venue in memory
     *
     * @param {object} venueObj
     */
    Venues.prototype.cacheVenue = function (venueObj) {
        var venues = this.venues;
        // If there's no provided venue ID, or we already have the complete object
        // Do Nothing!
        if (!venueObj.vID ||
            (venues[venueObj.vID] && venues[venueObj.vID].service)) {
            return;
        }
        venues[venueObj.vID] = {
            service: null,
            venue: venueObj,
            user: venueObj,
        };
    };
    /**
     * @description Saves the event to the database
     *
     * @param  {Array} venues - Array of venue Objects
     * @param {boolean} combine - whether to extend the passed in objects with the returned saved objects
     * @return {Promise}       Promise will return on object like: { <venueID>: <{object} details> }
     */
    Venues.prototype.save = function (venues, combine) {
        if (!_.isArray(venues)) {
            venues = [venues];
        }
        var saveVenues = this.sanitize(venues);
        var request = this._SocketIo.onEmitSock('/venue/save', saveVenues), self = this;
        if (combine) {
            request.then(function (extendedVenues) {
                return self.combineVenueArray(venues, extendedVenues);
            });
        }
        return request;
    };
    Venues.prototype.sanitize = function (venues) {
        var fields = [
            'coords',
            'name',
            'type',
            'google',
            'yelp',
        ];
        return _.map(venues, function (venue) {
            return _.pick(venue, fields);
        });
    };
    /**
     * @description Iterates over 2 arrays of venues, and extends the source array with the extended
     * arrays values
     *
     * @param {Array<object>} source
     * @param {Array<object>} extended
     */
    Venues.prototype.combineVenueArray = function (source, extended) {
        _.each(source, function (venue) {
            _.each(extended, function (fullVenue) {
                if ((venue.google && venue.google === fullVenue.google) ||
                    (venue.yelp && venue.yelp === fullVenue.yelp)) {
                    // This adds a [0] property when extending - avoid
                    //_.extend(venue, extended);
                    venue.vID = fullVenue.vID;
                    venue.tz = fullVenue.tz;
                    return false;
                }
            });
        });
    };
    Venues.prototype.getPhotoSrc = function (venue, height, width) {
        var isHome, photo;
        if (_.isArray(venue.photos) && (photo = venue.photos[0])) {
            return photo.getUrl({
                maxHeight: height,
                maxWidth: width
            });
        }
        else {
            isHome = venue.types && venue.types.indexOf('establishment') === -1;
            return '/images/venue/' + (isHome ? 'home' : 'business') + '.svg';
        }
    };
    /**
     * @description gets the timezone for a specific venue
     * @param vID
     */
    Venues.prototype.getTz = function (vID) {
        var cached = this.venues[vID];
        if (cached && (cached = cached.venue) && cached.tz) {
            return this._$q.when(cached.tz);
        }
        return this._SocketIo.onEmitSock('/venue/getTz', vID).then(function (tz) {
            if (cached) {
                cached.tz = tz;
            }
            return tz;
        }).catch(function (err) {
            throw err;
        });
    };
    return Venues;
})();
module.exports = Venues;
//# sourceMappingURL=Venues.js.map