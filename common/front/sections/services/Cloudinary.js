/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var requestURL = "https://api.cloudinary.com/v1_1/" + CP.Settings.cloudinary.cloudName + "/image/upload", commonCloudinary = require('common/util/cloudinary').init({
    cloudName: CP.Settings.cloudinary.cloudName,
    typePostfix: CP.production ? null : CP.site.prefix,
});
/**
 * @class
 * @classdesc handles everything on the front end with Cloudinary and image generation stuff
 */
var Cloudinary = (function () {
    function Cloudinary(_SocketIo) {
        this._SocketIo = _SocketIo;
    }
    Cloudinary.prototype.flowConfig = function () {
        return {
            target: requestURL,
            singleFile: true,
            uploadMethod: 'POST',
            query: {}
        };
    };
    Cloudinary.prototype.extendFlowConfig = function ($flow, opts) {
        return this._SocketIo.onEmitSock('api/cloudinary/signRequest', opts).then(function (params) {
            return $flow.opts.query = params;
        });
    };
    return Cloudinary;
})();
Cloudinary.prototype.generateSrc = commonCloudinary.generateSrc;
exports.service = Cloudinary;
exports.directive = function (Cloudinary) {
    return {
        restrict: 'A',
        scope: {
            opts: '=?',
            optPreset: '@',
            type: '@',
            publicID: '@cloudImg',
            service: '@',
            value: '@',
            cond: '=?',
            "default": '@'
        },
        link: {
            pre: function (scope, iElem, iAttrs) {
                var build, noImg = scope["default"] || '';
                // add a preset
                if (!scope.opts && scope.optPreset) {
                    scope.opts = commonCloudinary.SIZE_PRESETS[scope.optPreset];
                }
                build = function (doBuild) {
                    var src = Cloudinary.generateSrc(scope.publicID, scope.service, scope.type, scope.opts);
                    if (doBuild) {
                        if (iElem[0].nodeName !== 'IMG') {
                            return iElem[0].style.background = "url('" + src + "')";
                        }
                        else {
                            return iElem[0].setAttribute('src', src);
                        }
                    }
                    else {
                        if (iElem[0].nodeName !== 'IMG') {
                            return iElem[0].style.background = "url('" + noImg + "')";
                        }
                        else {
                            return iElem[0].setAttribute('src', noImg);
                        }
                    }
                };
                if (iAttrs.cond) {
                    scope.$watch('cond', function (post, prev) {
                        if (post !== prev) {
                            return build(post);
                        }
                    });
                    return build(scope.cond);
                }
                else {
                    return build(scope.value === 'true');
                }
            }
        }
    };
};
//# sourceMappingURL=Cloudinary.js.map