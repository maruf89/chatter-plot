/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var eventEmitter = require('node-event-emitter'),

    /**
     * Whether the current user is authenticated
     * @type {Boolean}
     */
    _authenticated = false,

    _cookieTokenOpts = {
        domain: '.chatterplot.com',
        secure: true,
        path: '/',
        expires: 14,
    },

    /**
     * Recurses through a state and it's ancestors to get all required roles
     *
     * @param  {State} state
     * @return {array}          Array of roles if they exist
     */
    _recurseStateRoles = function (state:ng.ui.IState, states?:string[]) {
        if (!states) {
            states = [];
        }
        if (state['roles']) {
            states = states.concat(state['roles']);
        }
        if (state.parent) {
            return _recurseStateRoles(state.parent, states);
        } else {
            return _.uniq(states);
        }
    },

    /**
     * A function to be called upon authentication
     * @type {object}
     */
    _onAuthHook = null,

    /**
     * A variable that states whether the onAuthHook is empty/noop or has an actual hook
     * @type {Boolean}
     */
    _onAuthEmpty = true;

import {EventEmitter} from 'common/front/modules/EventEmitter';

export class AuthService extends EventEmitter {
    public userID:number;
    public permissions:number;
    public email:string;
    public notActivated:boolean;

    constructor(
        private _DataBus:NodeJS.EventEmitter,
        private _locStorage:any,
        private _SocketIo:any,
        private _Request:any,
        private _$q:ng.IQService,
        private _Permissions:any,
        private _$rootScope:cp.IRootScopeService,
        private _$http:ng.IHttpService,
        private _Config:any,
        private _$state:ng.ui.IStateService,
        private _Util:any,
        private _AuthenticateModal:any,
        private _ipCookie:any
    ) {
        super();
        _SocketIo.on('upgradeSocket/connected', this._onSocketAuth.bind(this));
        _SocketIo.on('/error/connect', this.resetOnAuthHook);
        _DataBus.on('/site/deauth', this.deauthenticate.bind(this, null));
    }

    /**
     * Initiates socket connection request.
     * This is the main ignition for the client side
     */
    onInit() {
        var token = this._ipCookie('userToken'),
            tokenData;

        if (token) {
            tokenData = {
                token: token
            };

            // set dummy hook with empty set to true
            this.setOnAuthHook();
        } else {
            this._DataBus.emit('/site/unauthenticated/init');
            this._SocketIo.once('connected', this._DataBus.emit.bind(this._DataBus, '/site/unauthenticated/ready'));
        }
        return this._SocketIo.onInit(tokenData);
    }

    public login(email, password) {
        if (!(email || password)) {
            this._$q.reject({
                type: 'updates.ERROR-MISSING_REQUIRED_FIELDS',
                response: 400
            });
        }

        return this._SocketIo.onEmitSock('/user/login', {
                email: email,
                password: password
            })
            .then(function (response) {
                var user;
                if (response.error) {
                    throw response.error;
                }
                if (user = this.authResponse(response)) {
                    return user;
                }
            }.bind(this));
    }

    /**
     * Called when the user is successfully authenticated
     * and we have userData pertaining to them
     *
     * @private
     * @param  {object} userData
     */
    private _readySelf(userData) {
        if (_authenticated) {
            return true;
        }

        // Permissions not loading…
        if (!userData || (userData.permissions == null) || userData.permissions === void 0) {
            return this._SocketIo.onEmitSock('/user/get', {
                account: true
            }, true).then(this._readySelf.bind(this));
        }

        this._locStorage.set('email', userData.email);
        this._Permissions.defineUserPermissions(userData.permissions);
        this.userID = userData.userID;
        this.email = userData.email;
        this.permissions = parseInt(userData.permissions, 10);

        if ((this.notActivated = userData.notActivated)) {
            this._activationBumper()
        }

        // this property does not belong on the public user object
        userData.notActivated = null;

        _authenticated = true;

        this.emit('/authenticated/ready', userData);
        this.emit('/authChange', true);
        this._DataBus.emit('/site/authChange', true);
        this._onAuthReady(userData);
    }

    private _activationBumper() {
        this._DataBus.emit('/nav/subheader/change', ['<activation-bumper></activation-bumper>'])
    }

    public accountActivated() {
        this.notActivated = false;
    }

    /**
     * @return {Boolean}  Whether the current user is authenticated
     */
    public isAuthenticated() {
        return _authenticated;
    }

    /**
     * Called on response from the servers Auth Controller
     *
     * @param  {object} response  Authentication response
     * @return {object}           userData
     */
    public authResponse(response, forceConnect) {
        var user = response.data;

        // Return false if:
        //       - neither `user` nor `user.userID` exist
        //       - `forceConnect` not set
        //       - we're already `_authenticate`ed
        if (!((user && user.userID) || forceConnect || !_authenticated)) {
            return false;
        }

        // Next unbind/bind our authenticated listeners
        this.removeAllListeners('/authenticated/start');
        this.once('/authenticated/start', function () {
            user = user || {};
            this._ipCookie('userToken', response.token, _.clone(_cookieTokenOpts));
            this._readySelf(user);
        }.bind(this));

        // Sometimes the socket is already connect for some reason, if so trigger ready
        if (!this._SocketIo.upgradeSock(response)) {
            this._readySelf(user);
        }

        return user;
    }

    /**
     * Called on successful socket upgrade
     *
     * @private
     */
    private _onSocketAuth() {
        if (_authenticated) {
            return false;
        }

        this.emit('/authenticated/start');

        _.defer(function () {
            if (!_authenticated) {
                this._SocketIo.onEmitSock('/user/get', { account: true }, true)
                    .then(this._readySelf.bind(this));
            }
        }.bind(this));
    }

    /**
     * @description Deauthenticates the current user
     *
     * @param  {boolean=} keepStorage   Whether NOT to delete the users local storage user data
     * @param  {string=}  source        Where the call originated from: server, user
     */
    public deauthenticate(keepStorage, source) {
        if (source !== 'user') {
            this.emit('/deauthenticated/start');
        }

        this.resetOnAuthHook();
        return this._$rootScope.safeApply(function () {
            if (!keepStorage) {
                this._$http.get('/auth/clear_session');
                this._locStorage.clearAll();
                this._ipCookie.remove('userToken', _.clone(_cookieTokenOpts));

                // If a user disconnects - don't auto reconnect with facebook (or any other 3rd party auto login)
                this._ipCookie('disableSocialConnect', true, _.clone(_cookieTokenOpts));
            }

            // Downgrade from secure socket connection
            this._SocketIo.downgradeSock();
            _authenticated = false;

            this.userID = this.email = this.userData = null;
            this.emit('/deauthenticated');
            this._DataBus.emit('/site/deauthenticated');
            this._DataBus.emit('/site/authChange');
            this.emit('/authChange', false);
            this._DataBus.emit('/site/unauthenticated/init');

            return this._SocketIo.once('connected', function () {
                return this._DataBus.emit('/site/unauthenticated/ready');
            }.bind(this));
        }.bind(this));
    }

    /**
     * Authenticates the current state object
     *
     * @param  {object} state  The state to authenticate
     * @return {Promise}       options including optional data like replace, or error
     */
    public stateAuthentication(prevent, toState, toParams, fromState, fromParams, error) {
        var forbidden, paramsToObj, roles;

        /**
         * @return {object}  Returns the parent functions arguments as an object to pass
         */
        paramsToObj = function () {
            return {
                toState: toState,
                toParams: toParams,
                fromState: fromState,
                fromParams: fromParams
            };
        };

        forbidden = {
            type: this.notActivated ? 'ERROR-INVALID_PERMISSIONS_NOT_ACTIVE' : 'ERROR-FORBIDDEN_PAGE',
            response: 403
        };

        roles = this.getStateRoles(toState);

        if (_authenticated && typeof this.permissions !== 'number') {

            // A disconnect between login and missing permissions
            prevent();
            return this._$q.reject({
                initData: {
                    state: toState,
                    params: toParams
                }
            });
        } else if (toState.loggedinRedirect && (_authenticated || !window.CP.ready)) {

            // If going to a state that is only visible if not logged in & authenticated or site not ready
            if (_authenticated) {
                prevent();
                return this._$q.reject({
                    replace: toState.loggedinRedirect,
                    params: paramsToObj()
                });
            }
        } else if (_authenticated) {

            // If already logged in
            if (roles && !this._Permissions.can(roles, this.permissions)) {
                prevent();
                return this._$q.reject(forbidden);
            }
        } else if (roles) {
            // Not Authenticated
            prevent();
            if (!window.CP.ready) {
                return this._$q.reject({
                    initData: {
                        state: toState,
                        params: toParams
                    }
                });
            } else {
                return this._$q.reject(forbidden);
            }
        }

        return this._$q.when();
    }

    public manualSignup(userData) {
        var requiredFields = ['firstName', 'lastName', 'email', 'password'];

        if (this._Util.lacksFields(requiredFields, userData)) {
            throw new Error('Missing manual signup required fields');
        }

        return this._SocketIo.onEmitSock('/user/signup', userData)
            .then(function (response) {
                if (this.authResponse(response)) {
                    return response.data;
                }
                throw response;
            }.bind(this))
            .catch(function (err) {
                err.duration = 3500;
                this._DataBus.emit('yapServerResponse', err);
                throw err;
            }.bind(this));
    }

    /**
     * @description authenticates a user with a given token
     * @param {object} tokenData
     * @param {string} tokenData.token
     * @returns {object}
     */
    public authenticateWithToken(tokenData) {
        // quit if no token passed
        if (!tokenData.token) {
            return false;
        }

        return this.authResponse(tokenData, true);
    }

    /**
     * @description Activates a user's account with a token
     * @param {object} data
     * @param {string} data.token - jwt token
     * @returns {Promise}
     */
    public activateUser(data) {
        if (!data.token) {
            return this._$q.reject();
        }
        return this._Request.onEmitSock('/user/activate', data);
    }

    public passwordResetEmail(email) {
        var userData = {
            email: email
        };

        return this._SocketIo.onEmitSock('/email/password-reset', userData)
            .then(null, function (err) {
                err.duration = 3500;
                this._DataBus.emit('yapServerResponse', err);
                throw err;
            }.bind(this));
    }

    private _passwordCall(data, emitString) {
        if (!(data.password || data.token)) {
            return this._$q.reject(false);
        }
        return this._SocketIo.onEmitSock(emitString, data);
    }

    public passwordReset(data) {
        return this._passwordCall(data, '/user/passwordReset');
    }

    public deleteAccount(opts) {
        var options:any = {
            userID: this.userID
        };

        if (opts) {
            options = _.extend(options, opts);
        }

        return this._SocketIo.onEmitSock('/user/delete', options)
            .then(function () {
                this.deauthenticate();
                return _.defer(this.onInit.bind(this));
            }.bind(this));
    }

    public resendActivationEmail() {
        this._DataBus.emit('progressLoader', {
            start: true
        });

        return this._SocketIo.onEmitSock('/email/activation/resend')
            ["catch"](function (err) {
            if (err.notify) {
                return this._DataBus.emit('yap', err.notify);
            }
        }.bind(this))
            ["finally"](this._DataBus.emit.bind(this._DataBus, 'progressLoader'));
    }

    /**
     * @description does nothing if the user's already logged in
     * Otherwise leads the user through a login (or signup) then resolves
     * @param {object} params
     * @param {boolean} params.signup - whether to load the signup view
     * @param {object} params.loginParams - anything to pass as parameters
     * @returns {any}
     */
    public isLoggedIn(params?:any):ng.IPromise<any> {
        if (_authenticated) {
            return this._$q.when();
        }

        params = params || {};

        var deferred:ng.IDeferred<any> = this._$q.defer(),
            signup:boolean = params.signup,
            loginParams:any = params.loginParams || {
                    message: 'auth.MODAL_BAR_DEFAULT'
                },
            $state = this._$state;

        loginParams = _.extend(loginParams, params);
        loginParams.reject = deferred.reject;

        if (params.redirectBack) {
            params.redirectFn = $state.go.bind($state, $state.current.name, $state.params);
        }

        this.setOnAuthHook(function () {
            deferred.resolve();

            this._AuthenticateModal.deactivate();

            params.redirectFn && params.redirectFn();
        }.bind(this), true);

        this._AuthenticateModal.activate((signup ? 'signup' : 'login'), loginParams);

        return deferred.promise;
    }

    public resetOnAuthHook() {
        _onAuthHook = null;
        return _onAuthEmpty = true;
    }

    /**
     * Sets a hook to be called upon user authentication
     *
     * If overwrite is not true, and a previous hook exists, the new hook will not be set
     *
     * @param   {Function} fn         function to be called
     * @param   {boolean=} overwrite  If a previous hook exists, overwrite it
     * @param   {boolean=} overwriteDummy - will overwrite the default hook if set
     * @returns {boolean}             Whether the hook was set
     */
    public setOnAuthHook(fn?:Function, overwrite?:boolean, overwriteDummy?:boolean) {
        if (_onAuthHook && !overwrite && (!overwriteDummy || !
                _onAuthEmpty)) {
            return false;
        }
        if (typeof fn === 'function') {
            _onAuthEmpty = false;
        } else {
            fn = function () {
                var state = this._$state,
                    curState = state.current,
                    redirect = curState.loggedinRedirect;

                if (!state.params.redirect) {
                    if (redirect) {
                        return state.go(redirect[0], redirect[1]);
                    }

                    if (this.getStateRoles(curState)) {
                        state.go(this._Config.routes.onLogin);
                    }
                }
            }.bind(this);
        }
        _onAuthHook = fn;
        return true;
    }

    /**
     * Upon site authentication, this method is called
     *
     * If a hook was set via #setOnAuthHook, then that will be called
     * otherwise it defaults to going to the onLogin state set in Config
     */
    private _onAuthReady(userData) {
        if (_onAuthHook) {
            _onAuthHook(userData);
            return _onAuthHook = null;
        }
    }

    /**
     * Recurses through a state and it's ancestors to get all required roles
     *
     * @param  {State}      state
     * @return {Array|NULL}        Array if roles exist, otherwise null
     */
    public getStateRoles(state) {
        var roles = _recurseStateRoles(state);

        return roles.length ? roles : null;
    }
}

