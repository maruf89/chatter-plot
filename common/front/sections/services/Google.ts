/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

interface IGoogle {}

'use strict';

class Google implements IGoogle {
    private _searchAPI:google.maps.places.PlacesService;

    constructor(
        private _$q:ng.IQService,
        private _GoogleMapApi:any,
        private _Maps:any
    ) {
        this._searchAPI = null;
    }

    /**
     * @description A Google Maps Places request
     * Accepts a location with sourceID and fetches it's entire object
     *
     * @param  {object} source
     * @param  {string} source.ID - the places unique Google ID (ChIJDUPkT0a7woARX2WjOrRbgGM)
     * @return {Promise<object>} Contains the location
     */
    public getMapsPlace(sourceID:string):ng.IPromise<google.maps.places.PlaceResult> {
        var deferred:ng.IDeferred<google.maps.places.PlaceResult> = this._$q.defer(),
            instance;

        this._GoogleMapApi.then(function (maps) {
            var map;

            if (!this._searchAPI) {
                if (instance = this._Maps.getInstance()) {
                    map = instance.rootObject();

                    // we want to unset this variable on destroy
                    instance.addOnDestroyFn(this._mapDestroyed.bind(this));
                } else {
                    map = new google.maps.Map(document.createElement('div'), {
                        center: {lat: -33.8688, lng: 151.2195},
                        zoom: 13
                    });
                }

                this._searchAPI = new maps.places.PlacesService(map);
            }

            return this._searchAPI.getDetails({
                placeId: sourceID
            }, function (place, res) {
                if (res === 'OK') {
                    return deferred.resolve(place);
                }

                return deferred.reject(place);
            });
        }.bind(this));

        return deferred.promise;
    }

    private _mapDestroyed():void {
        this._searchAPI = null;
    }
}

module.exports = Google;
