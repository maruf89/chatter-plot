/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var EventEmitter_1 = require('common/front/modules/EventEmitter');
var DataBus = (function (_super) {
    __extends(DataBus, _super);
    function DataBus() {
        _super.call(this);
        this.setMaxListeners(15);
    }
    return DataBus;
})(EventEmitter_1.EventEmitter);
exports.DataBus = DataBus;
//# sourceMappingURL=DataBus.js.map