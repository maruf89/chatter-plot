/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
function AddYourself($templateCache) {
    return {
        restrict: 'E',
        scope: {
            formOnly: '@',
        },
        replace: true,
        controller: 'AddYourselfCtrl as Add',
        template: $templateCache.get('views/landing/directives/addYourself/Template.html'),
    };
}
exports.AddYourself = AddYourself;
;
//# sourceMappingURL=AddYourself.js.map