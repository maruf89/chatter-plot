/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

export function AddYourself($templateCache) {
    return {
        restrict: 'E',
        scope: {
            formOnly:   '@',      // {boolean} if true will hide everything but the form
        },
        replace: true,
        controller: 'AddYourselfCtrl as Add',
        template: $templateCache.get('views/landing/directives/addYourself/Template.html'),
    };
};
