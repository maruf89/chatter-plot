/// <reference path="../../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
/**
 * cpSubHeader Directive
 *
 * It is a placeholder for the Page Sub Header.

 * The content inside will get compiled in the jpPageSubHeader
 *
 * @param DataBus
 * @returns {{restrict: string, compile: Function}}
 */
module.exports = function (DataBus) {
    return {
        restrict: 'E',
        compile: function (tElement, tAttrs) {
            var child = tElement.children()[0], template = (child.tagName === 'SCRIPT' ? child : tElement[0]).innerHTML, emitKey = tAttrs.emitTarget || '/nav/subheader/change';
            return {
                pre: function ($scope, iElem, iAttrs) {
                    setTimeout(function () {
                        DataBus.emit(emitKey, [template, $scope, iAttrs, child.id]);
                    }, 0);
                    // remove it from the DOM
                    tElement.remove();
                    $scope.$on('$destroy', function () {
                        DataBus.emit(emitKey);
                    });
                }
            };
        }
    };
};
//# sourceMappingURL=Dir.js.map