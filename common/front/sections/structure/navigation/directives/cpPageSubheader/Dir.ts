/// <reference path="../../../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';


/**
 * cpPageSubHeader Directive
 *
 * Renders the Subheader set inside each ion-view
 *
 * @param $compile
 * @param $rootScope
 * @param $templateCache
 * @param DataBus
 * @returns {{restrict: string, scope: boolean, link: Function}}
 */

module.exports = function (
    $compile:ng.ICompileService,
    $rootScope:cp.IRootScopeService,
    $templateCache:ng.ITemplateCacheService,
    DataBus:any
) {

    var defaultSubHeader:any = {
            template: '',
            $scope:   {
                $id: null,
                $root: $rootScope,
            }
        },

        defaultEmptyState:string = 'empty',
        hiddenClass:string = 'hidden',

        lastSubHeader:any = defaultSubHeader;

    function isDifferentThanLast(subHeader = defaultSubHeader) {
        return lastSubHeader.template !== subHeader.template
            || lastSubHeader.$scope.$id !== subHeader.$scope.$id;
    }

    function toObj(array) {
        var obj = null;

        if (_.isArray(array)) {
            obj = {
                template: array[0],
                $scope: array[1] || $rootScope.$new(),
                attrs: array[2] || { 'class': ''},
                id: null,
            };

            obj.id = array[3] || obj.attrs.id;
        }

        return obj;
    }

    /**
     * @description Updates the class of the current subheader by removing the old & adding the new
     *
     * @param {array<string>} oldClasses
     * @param {array<string>} newClasses
     * @param iElem
     * @returns {array<string>}
     */
    function updateClasses(oldClasses:string[], newClasses:string[], iElem:JQuery):string[] {
        oldClasses && _.each(oldClasses, iElem.removeClass.bind(iElem));
        newClasses && _.each(newClasses, iElem.addClass.bind(iElem));
        return newClasses;
    }

    return {
        restrict: 'AC',
        scope: {
            visibleClass:   '@',                       // {string} class to add when visible
            onKey:          '@cpPageSubheader',        // {string}
        },
        compile: function (tElem, tAttrs) {
            var defaultContentStr:string = null;

            if (tAttrs.hasDefault) {
                defaultContentStr = tElem[0].innerHTML;
                tElem.empty();
            }

            return {
                pre: function (scope, iElem) {
                    scope.hasSubHeader = false;
                    scope.classes = [];
                    scope.visibleClass = typeof scope.visibleClass === 'string' ? scope.visibleClass : 'has-subheader';

                    var key:string = scope.onKey || '/nav/subheader/change',
                        defaultContent,

                        // value used to check what was the last action ran - uses the ID of the template
                        curActive:string;


                    if (defaultContentStr) {
                        defaultContent = $compile(defaultContentStr)(scope.$root.$new(false));
                        setTimeout(function () {
                            iElem.append(defaultContent);
                        }, 0);
                    }

                    DataBus.on(key, (subHeaderArr:any[]) => {
                        var subHeader = toObj(subHeaderArr),
                            classes = [];

                        // Checks that duplicate requests activating the same template don't get called
                        if (curActive && subHeader && subHeader.id === curActive) {
                            return false;
                        }

                        if (!subHeader) {
                            // check if we already emptied it out
                            if (curActive === defaultEmptyState) {
                                return false;
                            }

                            if (defaultContent) {
                                defaultContent.siblings().remove()
                            } else {
                                iElem.empty();
                            }

                            // Unhide the default content if it exists
                            defaultContent && defaultContent.removeClass(hiddenClass);

                            scope.hasSubHeader = false;
                            iElem.removeClass(scope.visibleClass);
                            curActive = defaultEmptyState;
                        } else if (isDifferentThanLast(subHeader)) {
                            var compiledTpl = $compile(subHeader.template)(subHeader.$scope)[0];
                            subHeader.attrs.class.split(' ');

                            if (defaultContent) {
                                // if we have default content to show - hide it
                                defaultContent.addClass(hiddenClass);
                            } else {
                                // Otherwise fuck it
                                iElem.empty();
                            }

                            // add the new content
                            iElem.append(compiledTpl);

                            classes = subHeader.attrs.class.split(' ');
                            scope.hasSubHeader = true;
                            iElem.addClass(scope.visibleClass);

                            // cache the id so we don't repeat unneccesarily
                            curActive = subHeader.id;
                        }

                        scope.classes = updateClasses(scope.classes, classes, iElem);

                        lastSubHeader = subHeader || defaultSubHeader;
                    });
                }
            }
        }
    };
};


