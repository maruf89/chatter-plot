/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
// directive: activityListing
module.exports = function (Maps, $templateCache) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            type: '@',
            Activity: '=data',
            listing: '@',
            handler: '=',
            subType: '@',
        },
        template: function (elem, attrs) {
            return $templateCache.get("views/" + attrs.type + "/directives/listing/Template.html");
        },
        link: {
            pre: function (scope) {
                scope.vars = { isList: !!scope.listing };
            },
            post: function (scope) {
                scope.Map = Maps.getInstance();
            }
        }
    };
};
//# sourceMappingURL=listing.js.map