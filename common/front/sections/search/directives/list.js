/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
module.exports = function ($templateCache) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            type: '=',
            options: '=',
            onRefresh: '=',
            stack: '=',
            group: '=',
        },
        template: $templateCache.get('views/activities/search/directives/list/Template.html')
    };
};
//# sourceMappingURL=list.js.map