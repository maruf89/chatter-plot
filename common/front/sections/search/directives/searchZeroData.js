/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var tandemNeg = {
    key: 'activities',
    value: 'activities.TANDEM_PARTNERS_NEG'
}, eventNeg = {
    key: 'activities',
    value: 'activities.EVENTS_NEG'
}, teacherNeg = {
    key: 'activities',
    value: 'activities.TEACHERS_NEG'
}, userOpts = [{
        illo: '/images/landing/tandem/_Top-1.svg',
        button: {
            key: 'activities.SEARCH_ALL_TANDEMS',
            className: 'btn-blue search-all',
            link: "" // Added via Config
        }
    }, {
        notLoggedIn: true,
        illo: '/images/AdSlot/addSelfSpeakerList/map.svg',
        button: {
            key: 'campaign.ADD_SELF',
            className: 'btn-yellow',
            link: "standalone.addYourself"
        }
    }], teacherOpts = [{
        illo: '/images/landing/root/_Top-3.svg',
        button: {
            key: 'activities.SEARCH_ALL_TEACHERS',
            className: 'btn-blue search-all',
            link: "" // Added via Config
        }
    }, {
        notLoggedIn: true,
        illo: '/images/AdSlot/addSelfSpeakerList/map.svg',
        button: {
            key: 'campaign.ADD_SELF',
            className: 'btn-yellow',
            link: "standalone.addYourself"
        }
    }], eventOpts = [{
        illo: '/images/landing/tandem/_Top-1.svg',
        button: {
            key: 'activities.SEARCH_ALL_EVENTS',
            className: 'btn-blue search-all',
            link: "" // Added via Config
        }
    }, {
        illo: '/images/AdSlot/addSelfSpeakerList/map.svg',
        button: {
            key: 'events.CREATE_EVENT',
            className: 'btn-yellow',
            link: 'dashboard.eventCreate'
        }
    }], types = {
    tandems_language: {
        title: {
            key: 'activities.ZERO-TITLE_LANG_MIXIN',
            variables: [tandemNeg]
        },
        copy: {
            key: 'campaign.ADD_SELF_TANDEM',
        },
        options: userOpts
    },
    tandems_name: {
        title: {
            key: 'activities.ZERO-TITLE_NAME_MIXIN',
            variables: [
                tandemNeg, {
                    key: 'name_email',
                    value: 'activities.NAME_O_EMAIL'
                }
            ]
        },
        copy: {
            key: 'campaign.ADD_SELF_TANDEM',
        },
        options: userOpts
    },
    teachers_language: {
        title: {
            key: 'activities.ZERO-TITLE_LANG_MIXIN',
            variables: [teacherNeg]
        },
        copy: {
            key: 'campaign.ADD_SELF_TANDEM',
        },
        options: teacherOpts
    },
    teachers_name: {
        title: {
            key: 'activities.ZERO-TITLE_NAME_MIXIN',
            variables: [
                teacherNeg, {
                    key: 'name_email',
                    value: 'activities.NAME_O_EMAIL'
                }
            ]
        },
        copy: {
            key: 'campaign.ADD_SELF_TANDEM',
        },
        options: teacherOpts
    },
    events_language: {
        title: {
            key: 'activities.ZERO-TITLE_LANG_MIXIN',
            variables: [eventNeg]
        },
        options: eventOpts
    },
    events_name: {
        title: {
            key: 'activities.ZERO-TITLE_NAME_MIXIN',
            variables: [
                eventNeg, {
                    key: 'name_email',
                    value: 'activities.NAME'
                }
            ]
        },
        options: eventOpts
    }
};
module.exports = function ($templateCache, Util, User, $q, Activities, Config, iTalki) {
    userOpts[0].button.link = Config.routes.search + "({type:'tandems'})";
    teacherOpts[0].button.link = Config.routes.search + "({type:'teachers'})";
    eventOpts[0].button.link = Config.routes.search + "({type:'events'})";
    return {
        restrict: 'E',
        replace: true,
        scope: {
            type: '@',
            group: '=?',
            lang: '=?',
        },
        template: $templateCache.get('views/activities/search/directives/zeroData/Template.html'),
        link: {
            pre: function (scope, iElem, iAttrs) {
                var _title = iElem.find('.cta-head')[0], $copy = iElem.find('.copy'), key = scope.type + '_' + (scope.group || 'language'), buildVars;
                if (iAttrs.group) {
                    scope.$watch('group', function (post) {
                        return post && buildVars(scope.type + '_' + scope.group);
                    });
                }
                scope.vars = {
                    title: null,
                    copy: null,
                    options: [],
                    affLink: null,
                    loggedIn: function () {
                        return !!User.userID;
                    }
                };
                iElem.on('click', '.search-all', function () {
                    return Activities.search({ all: true });
                });
                buildVars = function (key) {
                    var curData = types[key];
                    Util.getI18nVars(curData.title.key, curData.title.variables).then(function (text) {
                        return _title.innerText = text;
                    });
                    if (curData.copy) {
                        Util.getI18nVars(curData.copy.key, curData.copy.variables)
                            .then(function (text) {
                            $copy[0].innerText = text;
                            return $copy.show();
                        });
                    }
                    else {
                        $copy.hide();
                    }
                    return $q.all(_.map(curData.options, function (option) {
                        return Util.getI18nVars(option.button.key, option.button.variables)
                            .then(function (text) {
                            return {
                                illo: option.illo,
                                notLoggedIn: option.notLoggedIn,
                                btn: {
                                    text: text,
                                    className: option.button.className,
                                    link: option.button.link
                                }
                            };
                        });
                    }))
                        .then(function (options) {
                        return scope.vars.options = options;
                    });
                };
                buildVars(key);
            },
            post: function (scope) {
                var vars = scope.vars, setiTalkiURL = function () {
                    vars.affLink = iTalki.buildURL(scope.lang[0]);
                    scope.$root.safeDigest(scope);
                };
                if (scope.lang) {
                    scope.$watch('lang', function (post, prev) {
                        if (post[0] !== prev[0]) {
                            setiTalkiURL();
                        }
                    });
                }
                setiTalkiURL();
            }
        }
    };
};
//# sourceMappingURL=searchZeroData.js.map