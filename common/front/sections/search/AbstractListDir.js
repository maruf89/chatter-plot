/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
// default directive to load when no events
var _defaultZeroDataDirective = 'zero-data', _baseSearch = {
    size: 10,
    from: 0,
    initialFitAll: true,
    opts: {
        location: {
            coords: {}
        }
    }
};
var AbstractDirective = (function () {
    function AbstractDirective(_config, defaultSearch, _DataBus, _$analytics, _$compile, _$q, _Util, _ScrollService) {
        this._config = _config;
        this._DataBus = _DataBus;
        this._$analytics = _$analytics;
        this._$compile = _$compile;
        this._$q = _$q;
        this._Util = _Util;
        this._ScrollService = _ScrollService;
        var base = _.cloneDeep(_baseSearch);
        this.defaultSearch = defaultSearch ?
            _.defaults(_.cloneDeep(defaultSearch), base) : base;
    }
    AbstractDirective.prototype.pre = function (scope, iElem, iAttrs, Ctrl) {
        // Make sure it's safe for usage - will look like `activitySearch` or `attending` or `activityTandem`
        scope.id = $.trim(scope.id) || '_';
        var moduleName = this._config.moduleName, moduleID = moduleName + (scope.subType || '');
        // Variables to be shared between the Class
        scope.vars = {
            ownID: moduleName + scope.$id,
            showLoadMore: null,
            searching: true,
            initiated: false,
            cluster: null,
            items: [],
            stack: scope.stack,
            itemKeyID: this._config.itemKeyID,
            instanceName: scope.id,
            moduleType: this._config.moduleType,
            moduleID: moduleID,
            searchType: scope.subType || this._config.moduleType,
        };
        // link them
        Ctrl.scopeInit(scope.vars);
    };
    AbstractDirective.prototype.postCallback = function (callback) {
        return function (scope, iElem, iAttrs, Ctrl) {
            var vars = scope.vars, modType = vars.moduleType, $scrollParent = iElem.closest('.pane-wrapper-scroll'), self = this, doWeShowLoadMore;
            if (!scope.options) {
                scope.options = {};
            }
            _.defaults(scope.options, _.clone(this.defaultSearch));
            // Stupid fucking bug I can't find where opts.location.coords.zoom is being removed but it's
            // needed on the first search
            var _zoom = scope.options.opts.location, initialZoom = _zoom.coords && _zoom.coords.zoom;
            /**
             * Register a function on the controller to initiate a new search
             */
            Ctrl.registerSearch(function (mustBTrue, disableMarkers, replace) {
                if (mustBTrue && (!vars.searching || !vars.initiated) && scope.options) {
                    vars.searching = true;
                    return Ctrl.search(scope.options, true, initialZoom, disableMarkers)
                        .then(Ctrl.process.bind(Ctrl, scope.options))
                        .then(_.partialRight(Ctrl.onLoaded.bind(Ctrl), replace))
                        .then(doWeShowLoadMore);
                }
                return self._$q.reject();
            });
            if (iAttrs.onRefresh) {
                scope.$watch('onRefresh', function (mustBTrue) {
                    vars.searching = false;
                    Ctrl.newSearch(mustBTrue, false, true).then(function () {
                        // scroll to the top of the page
                        _.defer(function () {
                            self._Util.$parentScroll($scrollParent, false, 0);
                        });
                    });
                });
            }
            doWeShowLoadMore = function (response) {
                vars.showLoadMore = (scope.options.size + scope.options.from) < response.total;
                scope.$root.safeDigest(scope);
                scope.options.from === 0 && vars.scrollTop();
            };
            // scroll to top of list upon any map update other than load more
            vars.scrollTop = !this._ScrollService ? function () { } : function () {
                self._ScrollService.scrollTop(true);
            };
            vars.loadMore = function () {
                scope.options.from += scope.options.size;
                if (vars.cluster) {
                    vars.cluster.pagination++;
                }
                // We have ditched pagination at the moment since the results are random
                // you will get different results for each page
                scope.options.opts.pagination++;
                return Ctrl.newSearch(true, true).then(function () {
                    _.delay(function () {
                        self._Util.$parentScroll($scrollParent, true, 400);
                    }, 600);
                });
            };
            vars.scrollToView = function (marker, countDown) {
                if (countDown === 0) {
                    console.log('error loading event from marker');
                    return false;
                }
                else if (typeof countDown === 'undefined') {
                    countDown = 5;
                }
                var rowName = "#" + modType + "Row-" + marker.model._id, $row = $(rowName), offset;
                if (!$row.length) {
                    return Ctrl.loadItemFromMarker(marker)
                        .then(vars.scrollToView.bind(self, marker, --countDown));
                }
                offset = CP.Global.vars.navHeight;
                self._Util.$scrollTo($row, $scrollParent, { padding: offset });
                self._Util.$hilite($row, 1000);
                self._$analytics.eventTrack('click', {
                    category: 'maps',
                    label: modType + ' results: marker click',
                    value: p(marker.model._id)
                });
            };
            _.defer(function () {
                // defer building the zero results directive
                this._buildZeroData(scope, iElem);
                // Race Condition: wait until the Controller resolves whether this was a history call
                Ctrl.newSearch(!Ctrl.fromHistory);
                vars.initiated = true;
            }.bind(this));
            this._DataBus.on("/map/" + modType + "/click", vars.scrollToView);
            scope.$on('$destroy', function () {
                self._DataBus.removeListener("/map/" + modType + "/click", vars.scrollToView);
            });
            callback && callback(scope, iElem, iAttrs, Ctrl);
        }.bind(this);
    };
    AbstractDirective.prototype._buildZeroData = function (scope, iElem) {
        var zeroData = scope.zeroDataDirective || _defaultZeroDataDirective, zeroEndTag = zeroData.match(/^[^\s]+/)[0];
        if (scope.group) {
            zeroData += " data-group='group'";
        }
        if (scope.options.opts.languages) {
            zeroData += " data-lang='options.opts.languages'";
        }
        zeroData = this._$compile("<" + zeroData + " data-type='" + scope.vars.searchType + "'></" + zeroEndTag + ">")(scope);
        iElem.find('.no-activities').append(zeroData);
    };
    return AbstractDirective;
})();
exports.AbstractDirective = AbstractDirective;
//# sourceMappingURL=AbstractListDir.js.map