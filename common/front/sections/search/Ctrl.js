/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
exports.defaults = {
    events: {
        opts: {
            location: {
                coords: null
            },
            languages: [],
            bounds: null,
            allLangs: true,
            pagination: 0,
            incrementBy: 10,
            all: false,
            firstSearch: null
        },
        from: 0,
        size: 10,
        markerSize: 150,
        queryTypes: ['name'],
        filterTypes: [
            'current',
            'location',
            'language',
            'published',
        ],
        venues: true,
        initialFitAll: null,
        fitAll: null
    },
    tandems: {
        opts: {
            location: null,
            bounds: null,
            languages: [],
            pagination: 0,
            incrementBy: 15,
            all: false,
            firstSearch: null
        },
        filterTypes: [
            'location',
            'hasLocation',
            'language',
            'hasName',
            'acceptMsg',
        ],
        from: 0,
        size: 15,
        markerSize: 150,
        queryTypes: ['name', 'email'],
        sortTypes: ['nativeSpeaker', 'shuffle'],
        initialFitAll: null,
        fitAll: null
    },
    teachers: {
        opts: {
            location: null,
            bounds: null,
            languages: [],
            pagination: 0,
            incrementBy: 15,
            all: false,
            firstSearch: null
        },
        filterTypes: [
            'location',
            'hasLocation',
            'teachesLanguage',
            'hasName',
            'acceptMsg',
        ],
        from: 0,
        size: 15,
        markerSize: 150,
        queryTypes: ['name', 'email'],
        sortTypes: ['shuffle'],
        initialFitAll: null,
        fitAll: null
    },
};
exports.setDefaultSize = function (event, tandem, teacher) {
    exports.defaults.events.opts.incrementBy = exports.defaults.events.size = event;
    exports.defaults.tandems.opts.incrementBy = exports.defaults.tandems.size = tandem || event;
    exports.defaults.teachers.opts.incrementBy = exports.defaults.teachers.size = teacher || tandem || event;
};
exports.setMarkerSize = function (event, tandem, teacher) {
    exports.defaults.events.markerSize = event;
    exports.defaults.tandems.markerSize = tandem || event;
    exports.defaults.teachers.markerSize = teacher || tandem || event;
};
exports.groupOpts = {
    language: {
        moveSearch: true
    },
    name: {
        moveSearch: false
    }
};
exports.urlMap = {
    'tandem-partners': 'tandems',
    'language-events': 'events',
    'language-teachers': 'teachers',
};
var cities = require('root/search/config/cityCoords'), 
/**
 * @memberof ActivitySearchCtrl
 * @description called once per page load, defines whether this in fact is the first search
 * @name _firstSearch
 * @var {boolean}
 */
_firstSearch = true, 
/**
 * @description any updates to state params before grabbing variables goes here
 * @param $state
 * @private
 */
_preProcessUrls = function ($state) {
    $state.params.type = exports.urlMap[$state.params.type];
};
/**
 * @ngdoc controller
 * @description This controller is the main activity search controller
 *
 * @class
 * @classdesc this class handles
 * @requires $scope
 * @requires Util
 * @requires Maps
 * @requires $state
 * @requires DataBus
 * @requires Activities
 */
var ActivitySearchCtrl = (function () {
    function ActivitySearchCtrl(_$scope, _Util, _DataBus, _Activities, Maps, $state) {
        this._$scope = _$scope;
        this._Util = _Util;
        this._DataBus = _DataBus;
        this._Activities = _Activities;
        var coords, currentSearch, key, options, type, self = this;
        this.vars = this.data = {
            types: Object.keys(exports.defaults).join(',')
        };
        _preProcessUrls($state);
        this.searchGroups = ['language', 'name'];
        this.searchOptions = options = this._resetSearchDefaults();
        this.searchGroup = _Util.location.decodeParams('group') || 'language';
        this.searchType = type = _Util.location.decodeParams('type') || 'tandems';
        currentSearch = options[type];
        // If we have a languageID set, add that, otherwise search all = 0
        currentSearch.opts.languages.push($state.params.languageID || 0);
        // EventsList watches this and updates on change
        this.sync = 0;
        this._onRefresh = 0;
        this._tempStore = {};
        this.stack = {
            markerSearchRepeat: true,
            markerReplace: true,
            name: 'search',
            revertStack: true,
            mapOpts: {
                moveSearch: this._mapMoveUpdate.bind(this)
            }
        };
        this.instance = Maps.getInstance();
        // hide the map move button on the map if it's not supported with the current search
        this._showMoveSearch();
        // Check if searching a city via the url ex. /search/vilnius
        if ((key = $state.params.city) && (coords = cities[key = key.toUpperCase().replace(/-/g, '_')])) {
            // prepare the translation key
            this.data.cityKey = "cities." + key;
            // prepare the coordinates from the flat file for this city
            coords = coords.split(/,\s?/);
            currentSearch.opts.location.coords = {
                lat: parseFloat(coords[0]),
                lon: parseFloat(coords[1])
            };
            // Update the map center
            this.instance.centerMap(currentSearch.opts.location.coords, 12);
            // Tell the map to fit around any results that come in for this location
            currentSearch.initialFitAll = true;
        }
        else if (this._readSearchParams(false)) {
            // Check if search parameters exist
            // disable first search if we load the page with specific search parameters
            _firstSearch = false;
        }
        else {
            // If an empty (everything) search than set map to fit around the bounds
            currentSearch.opts.all = true;
        }
        // A hook to call after performing the first search
        // And do specific actions depending on the outcome (no results on first load? then show all if firstSearch)
        if (_firstSearch) {
            _firstSearch = false;
            /**
             * @ngdoc method
             * @description a callback that receives the results from the very
             * first search, and if no results are returned, it will initiate a search all
             *
             * @callback
             * @param {Array} results  Array of listings (Events|Tandems)
             */
            currentSearch.firstSearch = _.once(function (results) {
                if (!results.length) {
                    return self.allSearch(true);
                }
            });
        }
        // Watch when switching between event|tandem search
        _$scope.$watch('Search.searchType', function (post, prev) {
            if (post !== prev) {
                self.searchType = type = post;
                options[type].initialFitAll = true;
                self.urlUpdate();
            }
        });
        // Call the rest of the startup methods
        this._bindListeners();
    }
    /**
     * @ngdoc function
     * @description setups the controllers listeners (here because the startup was getting too large)
     */
    ActivitySearchCtrl.prototype._bindListeners = function () {
        var allSearch, externalSearch, onUrlUpdate, self = this;
        // On history state change, call our @private history method
        this._onHistory = this._onHistory.bind(this);
        CP.Cache.$window.on('popstate', this._onHistory);
        // Update search when URL updates
        onUrlUpdate = this.urlUpdate.bind(this);
        this._DataBus.on('/search/urlUpdate', onUrlUpdate);
        // Watch when the group changes
        this._$scope.$watch('Search.searchGroup', this._onGroupChange.bind(this));
        // Search everything when called
        allSearch = this.allSearch.bind(this);
        this._DataBus.on('/search/all', allSearch);
        CP.Cache.$document.on('click', '#searchLink', allSearch);
        externalSearch = this.externalSearch.bind(this);
        this._Activities.on('/search', externalSearch);
        // Prerender safety
        this._$scope.$root.mustBeLoadedIn(6000);
        this._$scope.$on('$destroy', function () {
            CP.Cache.$window.off('popstate', self._onHistory);
            self._DataBus.removeListener('/search/urlUpdate', onUrlUpdate);
            self._DataBus.removeListener('/search/all', allSearch);
            CP.Cache.$document.off('click', '#searchLink', allSearch);
            self._Activities.removeListener('/search', externalSearch);
        });
    };
    /**
     * @description called on history change, updates the search
     * @callback
     * @param event
     * @private
     */
    ActivitySearchCtrl.prototype._onHistory = function (event) {
        if (!this._readSearchParams(true)) {
            this._resetSearchDefaults();
        }
        this.filterUpdate(null, true);
    };
    /**
     * @description trigger a new search via DataBus
     * @param filters
     * @param noUrlUpdate
     * @returns {*}
     */
    ActivitySearchCtrl.prototype.externalSearch = function (filters, noUrlUpdate) {
        var coords, group, languages, location, search, thisSearch, type;
        if (filters) {
            if (search = filters.search) {
                if (search.all) {
                    return this.allSearch(true);
                }
                thisSearch = this.searchOptions[this.searchType].opts;
                if (search.languages) {
                    (languages = thisSearch.languages).length = 0;
                    _.each(search.languages, function (langID, index) {
                        return languages[index] = langID;
                    });
                }
                if ((location = search.location) && (coords = location.coords)) {
                    _.extend(thisSearch.location.coords, coords);
                }
            }
            // conditionally pdate the search type
            if ((type = filters.type) && type !== this.searchType) {
                this.searchType = type;
            }
            // conditionally pdate the search group
            if ((group = filters.group) && group !== this.searchGroup) {
                this.searchGroup = group;
            }
        }
        this.filterUpdate(filters.search, noUrlUpdate);
        this.sync++;
        this._$scope.$root.safeDigest(this._$scope);
    };
    /**
     * @description reads the current URL's search parameters and if they exist will
     * update the current search object with them
     * @name ActivitySearchCtrl#_readSearchParams
     * @param {boolean} forceSearch - force read the URL's current search query:
     *                                  By default it reads the search parameters extracted by $stateParams
     *                                  but those values don't update when the URL updates.
     * @returns {boolean} false - if empty | true if extended
     */
    ActivitySearchCtrl.prototype._readSearchParams = function (forceSearch) {
        var currentSearch, searchOpts = this._Util.location.decodeParams('search', forceSearch, true);
        if (!searchOpts) {
            return false;
        }
        // If the location is set to null => throw an error
        if (!searchOpts.location && searchOpts.hasOwnProperty('location')) {
            delete searchOpts.location;
        }
        currentSearch = this.searchOptions[this.searchType];
        // Combine any search query parameters with our defaults
        _.merge(currentSearch.opts, searchOpts);
        // If there's bounds present, append it to filterTypes so the backend knows to search it
        if (searchOpts.bounds) {
            currentSearch.filterTypes.push('withinBounds');
        }
        return true;
    };
    ActivitySearchCtrl.prototype.filterUpdate = function (filters, noUrlUpdate) {
        // Modify the filters depending on whether we're text searching
        this._preSearchParse(filters);
        var options = this.searchOptions[this.searchType].opts, searchObj = {
            type: this.searchType,
            group: this.searchGroup,
            search: filters ? _.extend(options, { bounds: null }, filters) : options
        };
        options.all = false;
        options.noMap = !filters.location.coords;
        // Update any query types depending on the current search + group
        this._adjustQueryTypes(searchObj);
        this._refresh();
        if (!noUrlUpdate) {
            this._Util.location.searchObj(searchObj, true);
        }
    };
    /**
     * @description depending on whether we're text searching, we want to hide the coordinates/languages
     * @param {object} filters
     * @private
     */
    ActivitySearchCtrl.prototype._preSearchParse = function (filters) {
        var temp = this._tempStore;
        if (filters.query) {
            if (!temp.coords) {
                temp.location = filters.location;
            }
            if (!temp.languages) {
                temp.languages = filters.languages;
            }
            filters.location = {};
            filters.languages = null;
        }
        else {
            if (!filters.location.coords && temp.location) {
                filters.location = temp.location;
            }
            if (!filters.languages && temp.languages) {
                filters.languages = temp.languages;
            }
            temp.location = temp.languages = null;
        }
    };
    /**
     * @description adds/removes types to the filterTypes array depending on the
     * current search parameters
     * @private
     * @type {function}
     * @context {ActivitySearchCtrl}
     * @param {object} searchObj - the current search parameters
     */
    ActivitySearchCtrl.prototype._adjustQueryTypes = function (searchObj) {
        // If no bounds exist then remove `withinBounds`
        var types = this.searchOptions[this.searchType].filterTypes, indexBounds = types.indexOf('withinBounds');
        if (!searchObj.search.bounds && indexBounds !== -1) {
            types.splice(indexBounds, 1);
        }
    };
    ActivitySearchCtrl.prototype.urlUpdate = function (filters) {
        var opts = this.searchOptions[this.searchType].opts, search = {
            search: filters ? _.extend(opts, filters) : opts,
            group: this.searchGroup,
            type: this.searchType
        };
        this._Util.location.searchObj(search, true);
    };
    ActivitySearchCtrl.prototype.allSearch = function (updateURL) {
        var search;
        this._resetSearchDefaults();
        this.searchOptions.tandems.opts.all = this.searchOptions.events.opts.all =
            this.searchOptions[this.searchType].initialFitAll = true;
        this._DataBus.emit('progressLoader', { start: true });
        if (updateURL) {
            search = {
                search: {
                    location: { coords: null },
                    all: true
                },
                group: this.searchGroup,
                type: this.searchType
            };
            this._Util.location.searchObj(search, true);
        }
        this._refresh();
    };
    ActivitySearchCtrl.prototype._refresh = function () {
        this._onRefresh++;
        this._DataBus.emit('/search/refresh', this.searchOptions[this.searchType].opts);
        this._$scope.$root.safeDigest(this._$scope);
    };
    ActivitySearchCtrl.prototype._resetSearchDefaults = function () {
        var opts = this.searchOptions = _.cloneDeep(exports.defaults);
        // Make sure they reference the same object
        opts.tandems.opts.location = opts.teachers.opts.location = opts.events.opts.location;
        return opts;
    };
    /**
     * @description Called when the group changes. Runs group specific tasks
     * @callback
     * @param {string} post - the new group name
     * @param {string} prev - the old group name
     * @private
     */
    ActivitySearchCtrl.prototype._onGroupChange = function (post, prev) {
        if (post === prev) {
            return;
        }
        this._showMoveSearch();
        var searchObj = this.searchOptions[this.searchType];
        if (post === 'language') {
            // remove the query search
            searchObj.opts.query = null;
            this._refresh();
        }
        // make sure the URL is updated after we possibly modified the query
        this.urlUpdate();
    };
    /**
     * Show/Hide the map move button if it's not supported on the current search
     */
    ActivitySearchCtrl.prototype._showMoveSearch = function () {
        var group;
        if (exports.groupOpts[group = this.searchGroup]) {
            this.instance.loaded().then(function (instance) {
                return instance.callMethod('moveSearch', 'toggleButton', exports.groupOpts[group].moveSearch);
            });
        }
    };
    ActivitySearchCtrl.prototype._mapMoveUpdate = function (map) {
        if (!map) {
            return;
        }
        var mapOpts = map, options = this.searchOptions[this.searchType];
        options.opts.all = false;
        options.opts.pagination = 0;
        options.fitAll = options.initialFitAll = false;
        if (options.filterTypes.indexOf('withinBounds') === -1) {
            options.filterTypes.push('withinBounds');
        }
        this.filterUpdate({
            bounds: this._Util.geo.bounds.goog2Es(map.getBounds()).toUrl(),
            location: {
                coords: {
                    lat: mapOpts.center.lat(),
                    lon: mapOpts.center.lng(),
                    zoom: map.getZoom()
                }
            }
        });
    };
    return ActivitySearchCtrl;
})();
exports.ActivitySearchCtrl = ActivitySearchCtrl;
//# sourceMappingURL=Ctrl.js.map