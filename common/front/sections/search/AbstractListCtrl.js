/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
/**
 * @description a map - If a google zoom is passed, this converts them to Km radius to search with
 * { <zoom_level>: <kilometer_radius>
 * @type {object}
 */
var zoom2Km = {
    "3": 1750,
    "4": 1000,
    "5": 550,
    "6": 270,
    "7": 225,
    "8": 187.5,
    "9": 115,
    "10": 75,
    "11": 30,
    "12": 10,
    "13": 9,
    "14": 6,
    "15": 3,
    "16": 1.5,
    "17": 1
}, _defaultCenters = require('root/search/config/allSearchDefaults.json'), _getrandomCenter = function () {
    var max = _defaultCenters.length - 1;
    return _defaultCenters[Math.ceil((max + 1) * Math.random() - 1)];
}, _defaultStack = {
    initiated: false,
    destroyed: false,
    markerSearch: true,
    markerSearchRepeat: false,
    name: 'list',
    markerReplace: true,
    revertStack: null,
    anyLoad: null,
    _exitMapCoords: null,
    _enterMapCoords: null,
    mapOpts: {}
};
var ListCtrl = (function () {
    function ListCtrl(params, _$scope, _$q, _Activities, _AdSlot, _Maps, _DataBus, _Util, _locStorage) {
        this._$scope = _$scope;
        this._$q = _$q;
        this._Activities = _Activities;
        this._AdSlot = _AdSlot;
        this._Maps = _Maps;
        this._DataBus = _DataBus;
        this._Util = _Util;
        this._locStorage = _locStorage;
        this._controllerBound = false;
        this.listName = params.listName;
        this.resultsKey = params.resultsKey;
        this._service = params.service;
        this._bindListeners();
    }
    ListCtrl.prototype.scopeInit = function (vars) {
        this.vars = vars;
        // Extend or set stack defaults
        this.stack = this.vars.stack ? _.defaults(this.vars.stack, _defaultStack)
            : _.clone(_defaultStack);
        if (!this.stack.initiated) {
            this._Activities.map.initStack(this.stack.name, this.stack.mapOpts, true);
            this.stack.initiated = true;
        }
        // if there are markers shown from something previous - clear them
        var instance = this._Maps.getInstance();
        if (instance && instance.getSet(this.stack.name)) {
            instance.clearMarkers(this.stack.name);
        }
    };
    ListCtrl.prototype._bindListeners = function () {
        this._DataBus.emit('progressLoader', { start: true });
        // if cluster content load on click is enabled
        this._clusterBound = this.clusterLoad.bind(this);
        this._DataBus.on('/map/' + this.listName + '/clusterClick', this._clusterBound);
        this._unbindDestroy = this._$scope.$root.$on('$stateChangeStart', this._onDestroy.bind(this));
        _.defer(function () {
            this.checkHistory();
            this._controllerBound = true;
        }.bind(this));
    };
    ListCtrl.prototype.registerSearch = function (fn) {
        this.newSearch = fn;
    };
    ListCtrl.prototype.checkHistory = function () {
        var key = this._getStorageKey(), data, instance;
        // 1. we enabled the stack to be able to revert
        // 2. it is a history forward or back event
        // 3. we have data in local storage
        if (this.stack.revertStack &&
            this._Util.location.isHistory() &&
            (data = this._locStorage.get(key))) {
            this.fromHistory = true;
            this.vars.searching = false;
            // Make sure not to break reference
            [].push.apply(this.vars.items, data);
            // position the map if we have the coordinates
            if (!this.stack._enterMapCoords) {
                instance = this._Maps.getInstance();
                instance.centerMap(instance.currentSet.center, instance.currentSet.zoom);
                // In case the data is an array of arrays, make it flat
                this.setMarkers(_.flatten(data), this._$scope.options);
                this.stack._enterMapCoords = true;
            }
            // stop the loader
            this._DataBus.emit('progressLoader');
            this._locStorage.remove(key);
        }
    };
    /**
     * Initiates a search for tandems
     *
     * @param {object} options - search parameters
     * @param {boolean=} updateMap - whether to update the map if coords are provided
     * @param {number=} zoom - if updating the map, what the zoom level should be
     * @param {boolean=} disableMarkers - will disable the marker search
     * @return {Promise<object>}
     */
    ListCtrl.prototype.search = function (options, updateMap, zoom, disableMarkers) {
        this._DataBus.emit('progressLoader', { start: true });
        var pagination = options.opts.pagination, stack = this.stack;
        // Set the starting `from`
        options.from = pagination ? options.size * pagination : 0;
        // if loading more & load more replace is true OR not loading more and we're searching for markers
        if (stack.markerSearch || stack.markerSearchRepeat && !disableMarkers) {
            // tell the map to load with all of the markers
            options.markerSearch = true;
            stack.markerSearch = false;
        }
        else {
            options.markerSearch = false;
        }
        // let this function determine whether to center the map & execute
        this._possiblyUpdateMap(options.opts, !updateMap, zoom);
        return this._service.fetch(this._preSearchFilter(options));
    };
    /**
     * @description sanitizes the search options right before sending to the server
     *  MODIFIES the passed in object
     * @param {object} options - unsanitized options
     * @returns {object} sanitized options
     * @private
     */
    ListCtrl.prototype._preSearchFilter = function (options) {
        var opts = options.opts, coords = opts.location.coords;
        if (coords && zoom2Km[coords.zoom]) {
            opts.location.distance = zoom2Km[coords.zoom];
        }
        return options;
    };
    ListCtrl.prototype._possiblyUpdateMap = function (opts, dontUpdateMap, zoom) {
        // If not updating map or searching for everything, then user's location doesn't matter
        if (dontUpdateMap || opts.all || opts.noMap) {
            return this._$q.when();
        }
        var coords = opts.location.coords, promise = coords && coords.lat ? this._$q.when(coords) : this._Util.geo.getMyLocation(), self = this;
        return promise.then(function (coords) {
            _.defer(function () {
                var instance = self._Maps.getInstance();
                instance && instance.centerMap(coords, coords.zoom || zoom || 13);
            });
        });
    };
    /**
     * @description Calls any stack related callbacks as well as formats the markers & tandems
     * @param {object=} options - search options
     * @param {object} response - server response
     * @param {array<object>} response[this.resultsKey] - the items
     * @param {array<object>=} response.markers - map marker objects (in higher quantity than item results
     * @returns {object}
     */
    ListCtrl.prototype.process = function (options, response) {
        // Defer if not ready yet
        if (!this._controllerBound) {
            return _.defer(this.process.bind(this, options, response));
        }
        this._DataBus.emit('progressLoader');
        if (this.stack.anyLoad) {
            this.stack.anyLoad(response);
        }
        if (response.markers) {
            this.setMarkers(response.markers, options);
        }
        if (options) {
            if (options.opts.query) {
                this._fitAllMarkers();
            }
            if (typeof options.firstSearch === 'function') {
                options.firstSearch(response[this.resultsKey]);
            }
        }
        return response;
    };
    ListCtrl.prototype._fitAllMarkers = function () {
        var instance = this._Maps.getInstance();
        instance && _.defer(instance.center.bind(instance));
    };
    ListCtrl.prototype.setMarkers = function (markers, options) {
        var self = this, stack = this.stack;
        this._Activities.map.setMarkers(markers, {
            classType: this.listName,
            replace: stack.markerReplace,
            identifier: stack.name
        })
            .then(function () {
            if (options.initialFitAll ||
                options.fitAll) {
                // defer it so that if it's the first load it gets called after all the initial setup
                _.defer(function () {
                    self._Maps.getInstance().center();
                });
                options.initialFitAll = false;
            }
            if (options && options.opts.all) {
                self._Maps.getInstance().centerMap(_getrandomCenter(), 3);
            }
        });
    };
    /**
     * @description Upon cluster click this method loads the elements within the cluster
     * @param  {object=} cluster - clusters object
     * @param  {number=} increment - (if called not as a callback) will fetch the next set of markers
     */
    ListCtrl.prototype.clusterLoad = function (cluster, increment) {
        var center, clusterModel, clusterObj, currentSet, start, self = this, contentSize = this._$scope.options.size, instance = this._Maps.getInstance();
        // call this to prevent a map move callback
        instance.awaitZoomCallback();
        if (cluster) {
            clusterObj = cluster[0];
            clusterModel = cluster[1];
        }
        if (clusterModel) {
            cluster = this.vars.cluster = {
                length: clusterModel.length,
                model: clusterModel,
                pagination: 0
            };
        }
        else if ((cluster = this.vars.cluster) && increment) {
            cluster.pagination++;
        }
        else {
            return false;
        }
        this.vars.searching = true;
        // Update url
        center = clusterObj.getCenter();
        this._DataBus.emit('/search/urlUpdate', {
            pagination: cluster.pagination,
            bounds: this._Util.geo.bounds.goog2Es(clusterObj.bounds_).toUrl(),
            location: {
                coords: {
                    lat: center.lat(),
                    lon: center.lng(),
                    zoom: clusterObj.getMap().getZoom() + 1
                }
            }
        });
        start = cluster.pagination * contentSize || 0;
        currentSet = cluster.model.slice(start, start + contentSize);
        var pluckedIDs = this._pluckClusterIDs(currentSet);
        return this._service.get(pluckedIDs).then(function (items) {
            var response = {};
            response[this.resultsKey] = items;
            this.process(null, response);
            this.onLoaded(response, true);
            this._$scope.$root.safeDigest(this._$scope);
        }.bind(this));
    };
    /**
     * @description pulls relevant IDs needed for the Service#get method to pull items from the current set
     * markers look like:
     *      {
     *          _class: "event"
     *          _id: "12002"
     *          _type: "event"
     *          coords: Object
     *          icon: "/map/icon/xs.svg"
     *          id: "event-12002"
     *          radius: null
     *      }
     *
     * @param {array<object>} markerSet
     * @returns {any[]}
     * @private
     */
    ListCtrl.prototype._pluckClusterIDs = function (markerSet) {
        return _.pluck(markerSet, '_id');
    };
    ListCtrl.prototype.onLoaded = function (response, replace) {
        var itemList = response[this.resultsKey];
        itemList = this._formatItems(itemList);
        this.updateList(itemList, replace);
        this._appendAdSlots(this._AdSlot.getSlots(this.vars.moduleID));
        _.defer(function () {
            this.vars.searching = false;
            this._$scope.$root.safeDigest(this._$scope);
        }.bind(this));
        return response;
    };
    ListCtrl.prototype._formatItems = function (items) {
        return this._service.format(items);
    };
    /**
     * @description inserts an ad unit into a list of items
     * If neither the adSlot nor items exist -> does nothing
     * @param {array<object>=} adSlots
     * @returns {array<object>}
     */
    ListCtrl.prototype._appendAdSlots = function (adSlots) {
        var added = 0, items = this.vars.items, listLength = items.length;
        if (!_.isArray(adSlots) || !listLength) {
            return items;
        }
        _.each(adSlots, function (slot) {
            var ad, adID = 'adSlot-' + added;
            if (slot.index <= listLength &&
                slot.expr(listLength)) {
                // if we already have the ad slot added then skip
                if (items[slot.index] && items[slot.index][this.vars.itemKeyID] === adID) {
                    return true;
                }
                ad = {
                    template: slot.template,
                    isAd: true,
                    ref: slot,
                };
                ad[this.vars.itemKeyID] = adID;
                return items.splice(slot.index + added++, 0, ad);
            }
        }, this);
        return items;
    };
    ListCtrl.prototype.updateList = function (items, replace) {
        if (replace) {
            this.vars.items.length = 0;
        }
        [].push.apply(this.vars.items, items);
    };
    ListCtrl.prototype.loadItemFromMarker = function (marker) {
        // interface fn
    };
    /**
     * @description a key generated to cache items in between going forward/backward in states
     * @returns {string}
     * @private
     */
    ListCtrl.prototype._getStorageKey = function () {
        return this.vars.moduleID + ':content:' + this.stack.name + ':' + this.vars.instanceName;
    };
    ListCtrl.prototype._onDestroy = function () {
        // remove listener
        var instance, key, mapObj;
        if (this._clusterBound) {
            this._DataBus.removeListener("/map/" + this.listName + "/clusterClick", this._clusterBound);
        }
        this._unbindDestroy && this._unbindDestroy();
        if (this.stack.revertStack) {
            key = this._getStorageKey();
            this._locStorage.set(key, this.vars.items);
            // If we haven't stored the map coordinates yet
            if (!this.stack._exitMapCoords) {
                instance = this._Maps.getInstance();
                mapObj = instance.rootObject();
                instance.currentSet.center = mapObj.getCenter();
                instance.currentSet.zoom = mapObj.getZoom();
                this.stack._exitMapCoords = true;
            }
        }
        else if (!this.stack.destroyed) {
            this._Activities.map.destroyStack();
            this.stack.destroyed = true;
        }
    };
    return ListCtrl;
})();
exports.ListCtrl = ListCtrl;
//# sourceMappingURL=AbstractListCtrl.js.map