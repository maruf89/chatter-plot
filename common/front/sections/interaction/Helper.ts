/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var moment = require('moment'),
    commonVenue = require('common/util/venue'),
    commonGeneral = require('common/util/general');

class Helper {
    private _statuses:any = {
        'pending': 'STATUS_PENDING',
        'exp_pending': 'STATUS_EXPIRED',
        'action_required': 'STATUS_ACTN_REQ',
        'accept': 'STATUS_ACCEPT',
        'exp_accept': 'STATUS_OCCURRED',
        'deny': 'STATUS_DENY',
        'cancel': 'STATUS_CANCEL',
        'edit': 'STATUS_CANCEL',
    };

    constructor (
        private _Interaction:cp.interaction.IService,
        private _$state:ng.ui.IStateService,
        private _User:cp.user.IService,
        private _Venues:cp.venue.IService,
        private _locale:any,
        private _Util:any,
        private _Config:any
    ) {
        _locale.ready('activities');
    }

    /**
     * @description
     * @param {boolean} current - whether to get upcoming requests or past ones
     * @param {number=} size
     * @param {number=} from
     * @returns {ng.IPromise<IServerResponse>}
     */
    public fetchRecent(current:boolean, size?:number, from?:number):ng.IPromise<cp.interaction.IServerResponse[]> {
        var whenRequest:string,
            whenDate:string = null;

        if (current) {
            whenRequest = 'currentRequest';
            whenDate = moment().subtract(3, 'hours')
        } else {
            whenRequest = 'pastRequest';
        }

        return this._Interaction.fetchType({
            type: 'REQUEST',
            withVenues: true,
            withUsers: true,
            filterTypes: [whenRequest, 'requestOfStatus'],
            opts: {
                ofStatus: ['pending', 'accept'],

                // Get all requests within the past 3 hours
                when: commonGeneral.format.dateString('basic_date_time_no_millis', whenDate),
            },
            size: size || 10,
            from: typeof from === 'number' ? from : 0,
        });
    }

    public formatNewRequest(serverReq:any):cp.interaction.IFormattedRequest {
        var venue = serverReq.venueDetails;
        venue.vID = serverReq.venue.vID;
        this._Venues.cacheVenue(venue);

        delete serverReq.venueDetails;
        return this._Interaction.formatRequest(serverReq);
    }

    public otherParticipants(request:cp.interaction.IFormattedRequest):cp.user.IUser {
        if (request.otherUser) {
            return request.otherUser;
        }

        var myID = this._User.userID,
            otherID = _.filter(request.participants, function (userID) {
                return userID !== myID;
            })[0];

        return request.otherUser = this._User.users[otherID];
    }

    public formatLocation(venue:cp.venue.IVenueFull):string {
        return commonVenue.objToAddress(venue);
    }

    private _linkText(text:string, className:string, href:string):string {
        // only link the text if there's a href
        return !href ? text : '<a class="' + className + '" ' +
        'href="' + href + '">' + text + '</a>';
    }

    /**
     * @description takes a `tandem session with: {name}` string and wraps each part in it's own link
     * regardless of whether {name} is in the beginning, middle or end of the sentence
     */
    public requestWith(request:cp.interaction.IFormattedRequest, user:cp.user.IUser, noLink?:boolean):string {
        var text:string = '',
            trimmedText:string,
            viewLink:string = !noLink ? this.generateLink(request, 'view') : '',
            personLink:string = !noLink ? this._linkText(
                user.fullName,
                'styled-link person-link',
                this._$state.href(this._Config.routes.profile, { userID: user.userID })
            ) : user.fullName,

            // temporarily store the translated name as this and replace after
            namePattern:string = '*name',

            // This matches everything BUT the namePattern sequence
            nameRegex:RegExp = /((?!\*name).)*/g,

            // since the ?! will still match the name that comes after *name we will need to trim it
            trimLength:number = namePattern.length - 1,

            // Get the translation
            requestWithText:string = this._locale.getString('activities.TANDEM_SES_WITH', { name: namePattern }),

            // will return something like the following if matched:
            //
            // Option 1: `Tandem session with: {name} after text`
            // ["Tandem session with: ", "", "name after text", ""]
            //
            // Option 2: `{name} Tandem session with`
            // ["", "name Tandem session with", ""]
            matches:string[] = requestWithText.match(nameRegex);

        if (matches[0]) {
            // Option 1 Matched

            text = this._linkText(matches[0], 'view-link', viewLink);
            trimmedText = matches[2].substr(trimLength);
        } else {
            // Option 2 Matched

            // "name Tandem session with" -> " Tandem session with"
            trimmedText = matches[1].substr(trimLength);
            matches.shift();
            matches.shift();
        }

        text += personLink;

        if (trimmedText.length) {
            text += this._linkText(trimmedText, 'view-link', viewLink);
        }

        return text;
    }

    /**
     * @description returns an i18n key corresponding to a possible request status
     * @param {object} request
     * @returns {string}
     */
    public getStatus(request:cp.interaction.IFormattedRequest):string {
        var key:string = request.status,
            prefix = 'activities.';

        if (key === 'pending' && !request.isMine && !request.expired) {
            key = 'action_required';
        }

        if (request.expired) {
            key = 'exp_' + key;
        }

        return prefix + this._statuses[key];
    }

    /**
     * @description builds a url for an interaction action
     * @param {object} request - the requset object
     * @param {string} action - one of (accept|deny|edit)
     * @returns {string}
     */
    public generateLink(request:cp.interaction.IFormattedRequest, action:string):string {
        return this._$state.href(this._Config.routes.dashboard, {
            action: this._Util.encodeActionArgs({
                action: action,
                objID: request.reqID,
                type: 'request',
            })
        });
    }

    public getDenyType(request:cp.interaction.IFormattedRequest):string {
        return  request.isMine || request.status === 'accept' ? 'cancel' : 'deny';
    }
}

exports.Map = function (
    $state:ng.ui.IStateService,
    User:cp.user.IService,
    Venues:cp.venue.IService,
    locale:any,
    Util:any,
    Config:any
) {
    var instance = null;

    return {
        get: function (Interaction) {
            return instance || (instance = new Helper(
                    Interaction,
                    $state,
                    User,
                    Venues,
                    locale,
                    Util,
                    Config
                ));
        }
    };
};

exports.Helper = function (
    Venues:cp.venue.IService,
    $q:ng.IQService
) {
    return function (Interaction:cp.interaction.IService) {

        this.getWindow = function (marker:cp.activities.IMarker):ng.IPromise<cp.interaction.IFormattedRequest> {
            var request:cp.interaction.IFormattedRequest = Interaction.requests[marker._id];

            if (!request.venueDetails) {
                return Venues.getCacheFormat(request.venue)
                    .then(function (obj:cp.venue.IVenueObj) {
                        request.venueDetails = obj.venue;
                        return request;
                    });
            }

            return $q.when(request);
        };

        this.formatWindow = function (activity:cp.interaction.IFormattedRequest, options:any):any {
            var venue = activity.venueDetails,
                address = commonVenue.objToAddress(venue, null, ', '),

                opts = {
                    coords: {
                        latitude: activity.venue.coords.lat,
                        longitude: activity.venue.coords.lon,
                    },
                    template: 'views/activities/templates/addressWindow.html',
                    scope: {
                        activity: {
                            name: Interaction.helper.requestWith(activity, activity.otherUser, true),
                            locationName: venue.name,
                            address: address,
                            directionsLink: null,
                        }
                    }
                };

            if (options && options.showDirections) {
                opts.scope.activity.directionsLink = commonVenue.addressStringToUrl(address);
            }

            return opts;
        };
    };
};
