/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var moment = require('moment'), commonVenue = require('common/util/venue'), commonGeneral = require('common/util/general');
var Helper = (function () {
    function Helper(_Interaction, _$state, _User, _Venues, _locale, _Util, _Config) {
        this._Interaction = _Interaction;
        this._$state = _$state;
        this._User = _User;
        this._Venues = _Venues;
        this._locale = _locale;
        this._Util = _Util;
        this._Config = _Config;
        this._statuses = {
            'pending': 'STATUS_PENDING',
            'exp_pending': 'STATUS_EXPIRED',
            'action_required': 'STATUS_ACTN_REQ',
            'accept': 'STATUS_ACCEPT',
            'exp_accept': 'STATUS_OCCURRED',
            'deny': 'STATUS_DENY',
            'cancel': 'STATUS_CANCEL',
            'edit': 'STATUS_CANCEL',
        };
        _locale.ready('activities');
    }
    /**
     * @description
     * @param {boolean} current - whether to get upcoming requests or past ones
     * @param {number=} size
     * @param {number=} from
     * @returns {ng.IPromise<IServerResponse>}
     */
    Helper.prototype.fetchRecent = function (current, size, from) {
        var whenRequest, whenDate = null;
        if (current) {
            whenRequest = 'currentRequest';
            whenDate = moment().subtract(3, 'hours');
        }
        else {
            whenRequest = 'pastRequest';
        }
        return this._Interaction.fetchType({
            type: 'REQUEST',
            withVenues: true,
            withUsers: true,
            filterTypes: [whenRequest, 'requestOfStatus'],
            opts: {
                ofStatus: ['pending', 'accept'],
                // Get all requests within the past 3 hours
                when: commonGeneral.format.dateString('basic_date_time_no_millis', whenDate),
            },
            size: size || 10,
            from: typeof from === 'number' ? from : 0,
        });
    };
    Helper.prototype.formatNewRequest = function (serverReq) {
        var venue = serverReq.venueDetails;
        venue.vID = serverReq.venue.vID;
        this._Venues.cacheVenue(venue);
        delete serverReq.venueDetails;
        return this._Interaction.formatRequest(serverReq);
    };
    Helper.prototype.otherParticipants = function (request) {
        if (request.otherUser) {
            return request.otherUser;
        }
        var myID = this._User.userID, otherID = _.filter(request.participants, function (userID) {
            return userID !== myID;
        })[0];
        return request.otherUser = this._User.users[otherID];
    };
    Helper.prototype.formatLocation = function (venue) {
        return commonVenue.objToAddress(venue);
    };
    Helper.prototype._linkText = function (text, className, href) {
        // only link the text if there's a href
        return !href ? text : '<a class="' + className + '" ' +
            'href="' + href + '">' + text + '</a>';
    };
    /**
     * @description takes a `tandem session with: {name}` string and wraps each part in it's own link
     * regardless of whether {name} is in the beginning, middle or end of the sentence
     */
    Helper.prototype.requestWith = function (request, user, noLink) {
        var text = '', trimmedText, viewLink = !noLink ? this.generateLink(request, 'view') : '', personLink = !noLink ? this._linkText(user.fullName, 'styled-link person-link', this._$state.href(this._Config.routes.profile, { userID: user.userID })) : user.fullName, 
        // temporarily store the translated name as this and replace after
        namePattern = '*name', 
        // This matches everything BUT the namePattern sequence
        nameRegex = /((?!\*name).)*/g, 
        // since the ?! will still match the name that comes after *name we will need to trim it
        trimLength = namePattern.length - 1, 
        // Get the translation
        requestWithText = this._locale.getString('activities.TANDEM_SES_WITH', { name: namePattern }), 
        // will return something like the following if matched:
        //
        // Option 1: `Tandem session with: {name} after text`
        // ["Tandem session with: ", "", "name after text", ""]
        //
        // Option 2: `{name} Tandem session with`
        // ["", "name Tandem session with", ""]
        matches = requestWithText.match(nameRegex);
        if (matches[0]) {
            // Option 1 Matched
            text = this._linkText(matches[0], 'view-link', viewLink);
            trimmedText = matches[2].substr(trimLength);
        }
        else {
            // Option 2 Matched
            // "name Tandem session with" -> " Tandem session with"
            trimmedText = matches[1].substr(trimLength);
            matches.shift();
            matches.shift();
        }
        text += personLink;
        if (trimmedText.length) {
            text += this._linkText(trimmedText, 'view-link', viewLink);
        }
        return text;
    };
    /**
     * @description returns an i18n key corresponding to a possible request status
     * @param {object} request
     * @returns {string}
     */
    Helper.prototype.getStatus = function (request) {
        var key = request.status, prefix = 'activities.';
        if (key === 'pending' && !request.isMine && !request.expired) {
            key = 'action_required';
        }
        if (request.expired) {
            key = 'exp_' + key;
        }
        return prefix + this._statuses[key];
    };
    /**
     * @description builds a url for an interaction action
     * @param {object} request - the requset object
     * @param {string} action - one of (accept|deny|edit)
     * @returns {string}
     */
    Helper.prototype.generateLink = function (request, action) {
        return this._$state.href(this._Config.routes.dashboard, {
            action: this._Util.encodeActionArgs({
                action: action,
                objID: request.reqID,
                type: 'request',
            })
        });
    };
    Helper.prototype.getDenyType = function (request) {
        return request.isMine || request.status === 'accept' ? 'cancel' : 'deny';
    };
    return Helper;
})();
exports.Map = function ($state, User, Venues, locale, Util, Config) {
    var instance = null;
    return {
        get: function (Interaction) {
            return instance || (instance = new Helper(Interaction, $state, User, Venues, locale, Util, Config));
        }
    };
};
exports.Helper = function (Venues, $q) {
    return function (Interaction) {
        this.getWindow = function (marker) {
            var request = Interaction.requests[marker._id];
            if (!request.venueDetails) {
                return Venues.getCacheFormat(request.venue)
                    .then(function (obj) {
                    request.venueDetails = obj.venue;
                    return request;
                });
            }
            return $q.when(request);
        };
        this.formatWindow = function (activity, options) {
            var venue = activity.venueDetails, address = commonVenue.objToAddress(venue, null, ', '), opts = {
                coords: {
                    latitude: activity.venue.coords.lat,
                    longitude: activity.venue.coords.lon,
                },
                template: 'views/activities/templates/addressWindow.html',
                scope: {
                    activity: {
                        name: Interaction.helper.requestWith(activity, activity.otherUser, true),
                        locationName: venue.name,
                        address: address,
                        directionsLink: null,
                    }
                }
            };
            if (options && options.showDirections) {
                opts.scope.activity.directionsLink = commonVenue.addressStringToUrl(address);
            }
            return opts;
        };
    };
};
//# sourceMappingURL=Helper.js.map