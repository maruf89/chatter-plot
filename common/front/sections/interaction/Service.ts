/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var SOURCE:string = 'INTERACTIONS',
    SOURCE_TYPE:string = 'MESSAGE',

    _fetchedMessages:boolean = false;

/**
 * Handles fetching/sending messages as well as marking read
 * @class
 */
export class Interaction implements cp.interaction.IService {
    public people:cp.user.IUserCache;
    public conversations:cp.interaction.IConversationCache = {};
    public totalConvosLen:number = 0;
    public convosLen:number = 0;

    public SOURCE:string = SOURCE;
    public SOURCE_TYPE:string = SOURCE_TYPE;

    public helper:cp.interaction.IServiceHelper;
    public map:cp.interaction.IServiceMap;

    /**
     * @description keep an internal cache of the requests for when
     * we need to update them
     *
     * @type {object}
     * @private
     */
    public requests:cp.interaction.IRequestCache = {};
    public defunctRequests:cp.interaction.IRequestCache = {};

    constructor (
        private _$q:ng.IQService,
        private _SocketIo:any,
        private _User:any,
        private _Config:any,
        private _DataBus:NodeJS.EventEmitter,
        private _$sce:ng.ISCEService,
        Helper:any,
        InteractionMap:any
    ) {
        this.people = _User.users;
        this.helper = Helper.get(this);
        this.map = new InteractionMap(this);

        // and add ourselves to people
        if (_User.userID) {
            this.people[_User.userID] = _User.data;
        }

        _DataBus.on('/site/authChange', function (authenticated) {
            if (authenticated) {
                return this.people[_User.userID] = _User.data;
            } else {
                this.conversations = {};
                return _fetchedMessages = false;
            }
        }.bind(this));
    }

    public send(message) {
        if (!_.isArray(message.to) || !message.to.length) {
            return this._$q.reject();
        }

        return this._SocketIo.onEmitSock('/message/send', message)
            ["catch"](function (err) {
            err.duration = 350000;
            throw this._DataBus.emit('yapServerResponse', err);
        }.bind(this));
    }

    /**
     * @param {object} opts
     * @param {string} opts.msgID
     * @returns {Promise<T>}
     * @deprecated
     */
    public get<T>(opts:any):ng.IPromise<T> {
        if (!opts.msgID) {
            throw new Error('Interaction#get requires msgID as a parameter property');
        }

        opts.objID = opts.msgID;

        var self = this,
            convos = this.conversations;

        // Check if we already have the requested conversation
        // If so remove it from the request
        if (opts.conversation) {
            if (convos[opts.msgID]) {
                opts.pullConvo = true;
                delete opts.conversation;
            }
        }

        return this._SocketIo.onEmitSock('/message/get', opts)
            .then(this._appendConversation.bind(this, opts))
            .catch(function (err) {
                self._DataBus.emit('yapServerResponse', err);
                throw err;
            });
    }

    private _appendConversation(opts, data) {
        var convos = this.conversations;
        data.conversation = opts.conversation && data.conversations[0];

        // Append the conversation to the end if we already have it
        if (opts.pullConvo) {
            data.conversation = convos[opts.objID];
        } else if (data.conversation && !convos[data.conversation.msgID]) {
            this.convosLen++;
            convos[data.conversation.msgID] = this.prune(data.conversation);
        }

        return data;
    }

    /**
     * @description gets a single interaction (or a group of messages belonging to a single conversation)
     *
     * @param {object} opts
     * @param {string} opts.objID - either the msgID or reqID
     * @param {string} opts.type - (request|message)
     * @param {boolean} opts.withUsers
     * @param {boolean} opts.messages
     * @param {boolean} opts.conversation
     * @returns {Promise<T>}
     */
    public getType<T>(opts:any, serverForce?:boolean):ng.IPromise<T> {
        if (!opts.type || !opts.objID) {
            throw new Error('Missing `type` and-or `objID` for Interaction#getType request');
        }

        var cache:any,
            callback:Function;

        if (opts.type === 'request') {
            callback = this._formatRequests.bind(this, true);
            cache = this.requests;
        } else {
            callback = this._appendConversation.bind(this, opts);
            cache = this.conversations;
        }

        if (!serverForce && cache && cache[opts.objID]) {
            return this._$q.when(cache[opts.objID]);
        }

        return this._SocketIo.onEmitSock('/interaction/get', opts).then(callback);
    }

    public fetchType(options:cp.interaction.ISearchOptions):ng.IPromise<cp.interaction.IRequest[]> {
        return this._SocketIo.onEmitSock('/interaction/fetch', options);
    }

    private _formatRequests(
        isSingle:boolean | cp.interaction.IServerResponse,
        response:cp.interaction.IServerResponse
    ):cp.interaction.IFormattedRequest|cp.interaction.IFormattedRequest[] {

        if (typeof isSingle !== 'boolean') {
            response = <cp.interaction.IRequest> isSingle;
            isSingle = false;
        }

        var formatted:cp.interaction.IFormattedRequest[] = _.map(response.requests, this.formatRequest, this);

        if (isSingle) {
            return formatted[0];
        }

        return formatted;
    }

    /**
     * @description Fetches the messages for the current user and saves those messages to a cache
     *
     * @name Interaction#fetch
     * @param {object=} opts - options
     * @param {number} opts.from - offset (default:0)
     * @param {number} opts.size - how many results (default:10)
     * @param {boolean=} force - Whether to force another API call, otherwise subsequent calls load from cache
     * @returns {Promise}
     */
    public fetch(opts?:any, force?:boolean) {
        if (_fetchedMessages && !force) {
            // If we're not trying to get even newer convos than we have
            if (!(opts.size && opts.from && (opts.from + opts.size) > this.convosLen)) {
                return this._$q.when(this.conversations);
            }
        }

        return this._SocketIo.onEmitSock('/message/fetch', opts)
            .then(function (data) {
                if (data.users) {
                    this._User.addPeople(data.users);
                }

                this.totalConvosLen = data.total;
                _.each(data.conversations, function (conversation:cp.interaction.IConversation) {
                    if (!this.conversations[conversation.msgID]) {
                        this.convosLen++;
                        return this.conversations[conversation.msgID] = this.prune(conversation);
                    }
                }, this);

                _fetchedMessages = true;
                return this.conversations;
            }.bind(this));
    }

    public generateKey(msgID:string):string {
        return "/msg/" + msgID;
    }

    /**
     * Formats/removes extra data from conversation object
     *
     * @name Interaction#prune
     * @param  {object} conversation   single conversation
     * @returns {object}                the some obj
     */
    public prune(conversation) {
        conversation.participants = _.filter(conversation.participants, function (userID) {
            return userID !== this._User.userID;
        }, this);

        conversation.fromNow = this.whenFrom(conversation.updated, true);
        conversation.upDate = this.upDate(conversation.updated);
        return conversation;
    }

    private _decodeID(msgID:string):string[] {
        return msgID.split('_');
    }

    /**
     * Converts a timestamp to a relative 'Time ago' string
     *
     * @name Interaction#whenFrom
     * @param  {string} timestamp  date time elasticsearch string
     * @param  {Boolean} noPrefix  whether to make a shorter version
     * @returns {string}
     */
    public whenFrom(timestamp, noPrefix) {
        return this._Config.format.date.toObj(null, timestamp).fromNow(noPrefix);
    }

    public upDate(timestamp) {
        return this._Config.format.date.toObj('basic_date_time', timestamp, false).toDate();
    }

    public isUnread(conversation) {
        var read = conversation.read;
        return !_.isArray(read) || read.indexOf(this._User.userID) === -
                1;
    }

    /**
     * ngdoc method
     * @description Notifies the Notification service that a message has been viewed to
     * keep the read/unread notifications in sync
     *
     * @private
     * @name Interaction#_notificationNotify
     * @param {string} msgID - sourceID of the string
     * @param {string=} sourceType - defaults to 'MESSAGE' add to override
     */
    private _notificationNotify(msgID:string, sourceType?:string) {
        return this._DataBus.emit('/notification/viewed', {
            source: SOURCE,
            sourceType: sourceType || SOURCE_TYPE,
            sourceID: msgID
        });
    }

    /**
     *
     * @param message
     * @returns {*}
     */
    public parseMessage(message) {
        var match = message.match(CP.Global.regex.url);

        if (match) {
            _.each(match, function (url) {
                message = message.replace(url, '<a href="' + url + '" target=_blank class=styled-link>' + url + '</a>');
            })
        }

        message = message.replace(/\r\n/g, '<br>').replace(/\n/g, '<br>');

        return this._$sce.trustAsHtml(message);
    }

    /**
     * @ngdoc method
     * @description marks a conversation as read if it's not already and notifies the Notification module
     * TODO: change _notificationNotify to not be so tightly coupled to the Notification module
     *
     * @name Notification#markRead
     * @param {string} msgID - conversation ID
     * @returns {number} if newly modified will return a number, otherwise false if nothing happened
     */
    public markRead(msgID:string):number {
        var conversation:cp.interaction.IConversation = this.conversations[msgID],
            read:number[];

        if (conversation &&
            _.isArray(read = conversation.read) &&
            read.indexOf(this._User.userID) === -1
        ) {
            this._notificationNotify(msgID);
            return read.push(this._User.userID);
        }

        return 0;
    }

    public usersFromID(msgID:string, excludeSelf?:boolean):number[] {
        var users = _.map(msgID.split('_'), function (id:string) {
            return p(id);
        });

        if (excludeSelf) {
            return _.filter(users, function (id) {
                return id !== this._User.userID;
            }, this);
        }

        return users;
    }

    public sendRequest(request:cp.interaction.IRequestBuild):ng.IPromise<string> {
        return this._SocketIo.onEmitSock('/interaction/request/new', request)
            .then(this.helper.formatNewRequest.bind(this.helper))
    }

    /**
     * @description clears the cache for requests/oldRequests/messages/conversations
     * TODO: build out for everything else when needed
     * @param {string} which
     */
    public clearCache(which:string):void {
        switch (which) {
            case 'request':
                this.requests = {};
        }
    }

    public formatRequest(request:cp.interaction.IFormattedRequest):cp.interaction.IFormattedRequest {
        if (request.formatted) {
            return request;
        }

        var date = this._Config.format.date.toObj('basic_date_time_no_millis', request.when),
            endDate;

        request.activityType = 'request';

        // unique ID for this
        request.activityID = `rq-${request.reqID}`;

        request.date = this._Config.format.date.display(date, 'longDate');
        request.time = this._Config.format.date.displayTime(date);
        request.isMine = request.creator === this._User.userID;

        if (request.until) {
            endDate = this._Config.format.date.toObj('basic_date_time_no_millis', request.until);
            request.endDate = this._Config.format.date.display(endDate, 'longDate');
            request.endTime = this._Config.format.date.displayTime(endDate);
        }

        request.expired = (endDate || date).isBefore(new Date());

        this.cacheRequest(request);

        request.formatted = true;

        return request;
    }

    /**
     * @description saves a request to the internal cache. Caches the request under 2 values:
     *              - Full ID: 3032-152_842
     *              - Short ID: 3032 (unique to the request)
     *
     * @param request
     * @returns {boolean}
     */
    public cacheRequest(request:cp.interaction.IFormattedRequest):boolean {
        var status:string = request.status,
            shortID:string = request.reqID.match(/^\d+/)[0],
            cache:cp.interaction.IRequestCache =
                status === 'accept' || status === 'pending' ? this.requests : this.defunctRequests;

        // cache the requests
        if (!cache[request.reqID]) {
            cache[request.reqID] = cache[shortID] = request;
            return true;
        }

        return false
    }

    public respondRequest(datum:{ reqID:string; status:string; message?:string }):ng.IPromise<boolean> {
        // If we don't even have a reference ID we can't do anything
        if (!datum.reqID) {
            return this._$q.reject();
        }

        var reqID:string = datum.reqID,
            status:string = datum.status,
            reqObj:cp.interaction.IFormattedRequest = this.requests[reqID],
            prevStatus:string,
            DataBus:any = this._DataBus;


        if (reqObj) {
            prevStatus = reqObj.status;
            reqObj.status = status;
        }

        return this._SocketIo.onEmitSock('/interaction/request/respond', {
                reqID: reqID,
                status: status,
                message: datum.message,
            })
            .catch(function (error) {
                DataBus.emit('yapServerResponse', error);

                if (reqObj) {
                    reqObj.status = prevStatus;
                }

                throw error;
            });
    }

    /**
     *
     * @param action
     * @param items
     */
    public parseSortRequests(action:any, items:cp.interaction.IRequest[]):ng.IPromise<cp.interaction.IFormattedRequest[]> {
        var request = this.requests[action.objID],
            returnList:cp.interaction.IRequest[],
            status:string = action.action,
            objIndex:number;

        if (!request) {
            return this._$q.when([]);
        }

        request.status = status;

        returnList = _.map(items, function (item:cp.interaction.IRequest, index:number) {
            if (item.reqID === action.objID) {
                objIndex = index;
            }

            // ensure that the list uses the same reference to the object
            return this.requests[item.reqID];
        }, this);

        // Remove the item from our list if declined
        if (status === 'deny' || status === 'cancel' || status === 'edit') {
            if (status === 'edit' && action.request) {
                returnList.push(action.request);
            }

            delete this.requests[request.reqID];
            this.defunctRequests[request.reqID] = request;

            if (typeof objIndex === 'number') {
                returnList.splice(objIndex, 1);
            }
        }

        return this._$q.when(returnList);
    }
}
