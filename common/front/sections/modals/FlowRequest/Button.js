/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
/**
 * Directive: flowRequestButton
 */
"use strict";
var commonUser = require('common/type/user');
module.exports = function (FlowRequest, User, Auth) {
    return {
        restrict: 'E',
        replace: true,
        template: '<div><button class="btn-yellow fat" ng-click="vars.initFlow()">Send Tandem Request</button></div>',
        scope: {
            reqUser: '=',
            type: '@',
            sections: '@',
        },
        link: function (scope) {
            scope.vars = {
                initFlow: function () {
                    Auth.isLoggedIn().then(function () {
                        var reqLocs = scope.reqUser.locations, myLocs = User.data.locations, combinedLocations = commonUser.combineUserLocations(myLocs, reqLocs);
                        FlowRequest.activate({
                            type: scope.type,
                            sections: scope.sections.split(/,\s?/),
                            data: {
                                suggestedVenues: combinedLocations
                            },
                            reqUserID: scope.reqUser.userID,
                        });
                    });
                }
            };
        }
    };
};
//# sourceMappingURL=Button.js.map