/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

/**
 * Directive: flowRequestButton
 */

"use strict";

interface ISectionTandemRequestScope extends cp.IDirectiveScope {
    reqUser:cp.user.IUser
    type:string
    sections:string
}

var commonUser = require('common/type/user');

module.exports = function (
    FlowRequest:any,
    User:any,
    Auth:any
) {
    return {
        restrict: 'E',
        replace: true,
        template: '<div><button class="btn-yellow fat" ng-click="vars.initFlow()">Send Tandem Request</button></div>',
        scope: {
            reqUser:    '=',    // {object} the user that is being requested
            type:       '@',    // {string} tandem|teacher
            sections:   '@',    // {string} comma separated list of section types
        },
        link: function (scope:ISectionTandemRequestScope) {
            scope.vars = {
                initFlow: function () {
                    Auth.isLoggedIn().then(function () {
                        var reqLocs = scope.reqUser.locations,
                            myLocs = User.data.locations,
                            combinedLocations:cp.user.ILocation[] =
                                commonUser.combineUserLocations(myLocs, reqLocs);


                        FlowRequest.activate({
                            type: scope.type,
                            sections: scope.sections.split(/,\s?/),
                            data: {
                                suggestedVenues: combinedLocations
                            },
                            reqUserID: scope.reqUser.userID,
                        });
                    });
                }
            }
        }
    };
};
