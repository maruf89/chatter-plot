/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
/**
 * Services and angular service names
 * @type {object}
 */
var _analyticsTypes, _clearEmpty, _linkifyLocaleString, services;
services = {
    'facebook': '_FB',
    'google': '_GPlus',
    'linkedin': '_LinkedIn'
};
_analyticsTypes = {
    signup: {
        category: 'acquisition',
        label: 'successfull signup: '
    },
    login: {
        category: 'authentication',
        label: 'successfull login: '
    }
};
_clearEmpty = function (obj) {
    var retObj = {}, has = null;
    _.each(obj, function (val, key) {
        if (val !== void 0 && val !== null) {
            retObj[key] = val;
            return has = true;
        }
    });
    return has && retObj;
};
_linkifyLocaleString = function (locale, key, link) {
    return locale.ready(locale.getPath(key))
        .then(function () {
        var string;
        string = locale.getString(key);
        return string.replace('<a>', '<a class="styled-link" href="' +
            link + '">');
    });
};
var AuthenticateModalCtrl = (function () {
    function AuthenticateModalCtrl(_FB, _LinkedIn, _GPlus, _$scope, _Auth, _$state, _DataBus, _Config, _$location, _$analytics, _locale) {
        this._FB = _FB;
        this._LinkedIn = _LinkedIn;
        this._GPlus = _GPlus;
        this._$scope = _$scope;
        this._Auth = _Auth;
        this._$state = _$state;
        this._DataBus = _DataBus;
        this._Config = _Config;
        this._$location = _$location;
        this._$analytics = _$analytics;
        this._locale = _locale;
        var self = this, listener;
        _.extend(_$scope.vars, {
            urlSearch: _$location.search(),
            email: null
        });
        this._onError = this._onError.bind(this);
        this.user = {
            firstName: null,
            lastName: null,
            email: null,
            password: null,
            settings: _Config.settings
        };
        this._extendVars(_$scope.vars || {}, true, true);
        _$scope.modal.onReactivate = this._extendVars.bind(this);
        // if this is a redirect we don't want the on login callbacks to fire
        this._isRedirect = !!_$state.params.redirect;
        _.defer(function () {
            listener = _$scope.$root.$on('$stateChangeStart', self._stateChange.bind(self));
            _$scope.$on('$destroy', listener);
        });
    }
    AuthenticateModalCtrl.prototype._extendVars = function (vars, dontHideMsg, dontDigest) {
        var userData, $scope = this._$scope;
        // If we have additional data passed in to extend the user's profile
        if (_.isPlainObject(userData = vars.userData)) {
            _.extend(this.user, userData);
        }
        if (vars.view) {
            $scope.vars.view = vars.view;
        }
        $scope.vars.hideMsg = !dontHideMsg;
        if (!dontDigest) {
            $scope.$root.safeDigest($scope);
        }
    };
    AuthenticateModalCtrl.prototype._onError = function (err) {
        err.duration = 3000;
        return this._DataBus.emit('yapServerResponse', err);
    };
    AuthenticateModalCtrl.prototype.signup = function (userData) {
        var self = this;
        return this._Auth.manualSignup(userData)
            .then(function (user) {
            self._sendTracking('signup', 'Email');
            // New e-mail users get a message to let them know to check their e-mail for a confirmation/activation email
            _.delay(function () {
                return self._DataBus.emit('yap', {
                    type: 'success',
                    title: 'updates.SUCCESS_EXCL',
                    body: 'auth.ACTIVATION_SENT',
                    duration: 7500
                });
            }, 2250);
            // When ready redirect to the correct page w/ firstTime variable
            return self._Auth.setOnAuthHook(self._onLoginRedirect.bind(self, user, true));
        })["catch"](this._onError);
    };
    AuthenticateModalCtrl.prototype.passwordReset = function (email) {
        var self = this;
        this._DataBus.emit('progressLoader', {
            start: true
        });
        return this._Auth.passwordResetEmail(email)
            .then(function () {
            self.passwordResetSent = true;
            self._DataBus.emit('progressLoader');
            self._$scope.safeDigest(self._$scope);
        });
    };
    AuthenticateModalCtrl.prototype.login = function (email, password) {
        var self = this;
        return this._Auth.login(email, password)
            .then(function () {
            self._sendTracking('login', 'Email');
            return self._Auth.setOnAuthHook(self._onLoginRedirect.bind(self));
        })["catch"](function (err) {
            var resetLink = self._$state.href('account.auth', {
                page: 'password-reset'
            });
            return _linkifyLocaleString(self._locale, err.type, resetLink)
                .then(function (bodyText) {
                return self._DataBus.emit('yap', {
                    type: 'error',
                    title: 'updates.ERROR_EXCL',
                    body: bodyText,
                    duration: 6000,
                    disableClick: true
                });
            });
        });
    };
    AuthenticateModalCtrl.prototype._onLoginRedirect = function (user, signup) {
        var attending, opts;
        if (this._isRedirect) {
            return false;
        }
        if (signup || !(attending = user.attending) || !attending.events.length) {
            opts = { userID: user.userID };
            if (signup) {
                opts.action = 'firstTime:yup';
            }
            return this._$state.go(this._Config.routes.profile, opts);
        }
        return this._$state.go(this._Config.routes.onLogin);
    };
    /**
     * On successfull account login
     *
     * @param  {object} service  Server response
     */
    AuthenticateModalCtrl.prototype.onVerify = function (service) {
        if (service.success) {
            this._sendTracking('login', service.name);
        }
        this._Auth.authResponse(service);
        return this._Auth.setOnAuthHook(this._onLoginRedirect.bind(this));
    };
    /**
     * On unsuccessfull account login
     *
     * @param  {object} service  Server response with `error` property
     */
    AuthenticateModalCtrl.prototype.onVerifyError = function (service) {
        var connectLink, self;
        self = this;
        connectLink = this._$state.href('standalone.addYourself');
        return _linkifyLocaleString(this._locale, 'auth.NOT_CONNECTED_BODY', connectLink)
            .then(function (bodyText) {
            return self._DataBus.emit('yap', {
                type: 'error',
                title: 'auth.NOT_CONNECTED',
                body: bodyText,
                duration: 6000,
                disableClick: true
            });
        });
    };
    AuthenticateModalCtrl.prototype._sendTracking = function (what, type) {
        var category = _analyticsTypes[what].category, label = _analyticsTypes[what].label;
        this._$analytics.eventTrack('click', {
            category: category,
            label: label + type
        });
    };
    /**
     * @description On successfull signup
     * @param {object} service - Server response
     * @param {object} service.data - the generating user object (without the addYourself data)
     * @param {boolean} service.success
     * @param {boolean} service.newUser
     * @param {string} service.name - the name of the service (facebook|google|linkedin)
     * @returns {Promise}
     * @callback
     */
    AuthenticateModalCtrl.prototype.onConnect = function (service) {
        var self = this, updates;
        // If they logged in with the signup buttons, use the verify callback
        if (!service.newUser) {
            return this.onVerify(service);
        }
        if (service.success) {
            this._sendTracking('signup', service.name);
        }
        // this line determines if there are any fields to extend the backend profile with
        updates = _clearEmpty(this._$scope.vars.userData);
        // extend the returned user with that data
        if (updates) {
            _.extend(service.data, updates);
        }
        // init processing oauth response
        this._Auth.authResponse(service);
        // Force overwrite if we have updates passed
        return this._Auth.setOnAuthHook(function (user) {
            self._onLoginRedirect(user, true);
            // lastly update ourselves with the missing updates fields (which we couldn't pass via oAuth
            if (updates) {
                self._Auth.emit('/connect/extendAccnt', updates);
            }
        }, !!updates);
    };
    /**
     * On unsuccessfull signup
     *
     * @param  {object} service  Server response with `error` property
     */
    AuthenticateModalCtrl.prototype.onConnectError = function (service) {
        service.error.duration = 3500;
        return this._DataBus.emit('yapServerResponse', service.error);
    };
    /**
     * Abstract method to initiate login or signup with the correct service
     *
     * @abstract
     * @param  {string} action   `connect` or `verify`
     * @param  {string} service  Third party service `facebook`, `google` or `linkedin`
     */
    AuthenticateModalCtrl.prototype.action = function (action, service) {
        var onAction, onError, onSuccess;
        if (this[services[service]]) {
            onAction = 'on' + action[0].toUpperCase() + action.substr(1);
            onSuccess = this[onAction].bind(this);
            onError = this[onAction + "Error"].bind(this);
            this[services[service]][action]().then(onSuccess, onError);
        }
        else {
            throw new Error('Trying to ' + action + ' with non-existent service.');
        }
    };
    /**
     * @description changes the view of the authentication modal
     * @param page
     * @param ignore
     * @returns {*}
     */
    AuthenticateModalCtrl.prototype.changeView = function (page, ignore) {
        if (!ignore && this._$scope.vars.dedicated) {
            return this._$state.go('account.auth', {
                page: page
            });
        }
        return this._$scope.vars.view = page;
    };
    AuthenticateModalCtrl.prototype._stateChange = function (event, to, toParams) {
        if (to.name === 'account.auth') {
            this.changeView(toParams.page, true);
            return;
        }
        this.destroy(!this._$scope.vars.dedicated && to.name === this._Config.routes.register);
    };
    AuthenticateModalCtrl.prototype.destroy = function (hideModal) {
        var method = hideModal ? 'hide' : 'deactivate';
        this._$scope.modal[method]();
    };
    return AuthenticateModalCtrl;
})();
exports.AuthenticateModalCtrl = AuthenticateModalCtrl;
//# sourceMappingURL=AuthenticateCtrl.js.map