/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

exports.dateTimeQuarters = function (date, start, end) {
    var nhour, quarter;
    quarter = Math.round(date.getMinutes() / 15) * 15;
    nhour = date.getHours();

    // When rounding up to the nearest hour we increment the hour and reset the quarter 
    if (quarter === 60) {
        nhour++;
        quarter = 0;
    }
    if (typeof start === 'number' && (nhour < start)) {
        nhour = start;
        quarter = 0;
    } else if (typeof end === 'number' && (nhour >= end)) {
        nhour = end - 1;
        quarter = 45;
    }
    return [nhour, quarter];
};

/**
 * TODO: Add ng-required
 *
 * Build an iAttrs.disableDefault option
 * that requires an input from the user depending on ng-required
 */
exports.directive = function (
    Bus:any,
    Config:any,
    $templateCache:ng.ITemplateCacheService
) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            source:                 '=',
            onChange:               '&',
            required:               '=?',
            watchMinTime:           '=?', // {Date} - will set the min time to this
            conditional:            '=?', // {expr} - if set will only apply the watches if this is true
            defaultHourOffset:      '@',  // {number} how much the default hour should be offset by
            defaultMinuteOffset:    '@',  // {number} how much the default time should be offset by
            disableDefault:         '@',
            startFrom:              '@',
            endAt:                  '@',
        },
        template: $templateCache.get('views/misc/form/timePicker/Template.html'),
        link: {
            post: function (scope, iElem, iAttrs) {
                scope.vars = {
                    onChange: $.noop,
                    times: []
                };

                var start:number,
                    startMinute:number,
                    end:number,
                    source:number[] = [],

                    defaultDate:Date,
                    hour:number,
                    quarter:number,

                    setDefaults = function () {
                        start = scope.startFrom ? p(scope.startFrom) : 0;
                        startMinute = 0;
                        end = scope.endAt ? p(scope.endAt) : 24;
                    },

                    /**
                     * If source is a date object, convert it to a usable time
                     */
                    setCurrent = function () {
                        if (scope.source instanceof Date) {
                            source = exports.dateTimeQuarters(scope.source, start, end);
                        } else if (typeof scope.source === 'string') {
                            source = scope.source.split('|');
                            source[0] = p(source[0]);
                            source[1] = p(source[1]);
                        }

                        hour = source[0];
                        quarter = source[1];
                    },

                    setTimes = function (start:number, end:number, startMinute?:number) {
                        var i:number = start,
                            j:number,
                            times:any[] = scope.vars.times;

                        times.length = 0;

                        // Build each hour with 4 quarters
                        for (;i < end; i++) {
                            j = i === start && startMinute ? startMinute : 0;
                            for (;j < 60; j+= 15) {
                                (function (_hour, _quarter) {
                                    var val = {
                                        value: `${_hour}|${_quarter}`,
                                        display: Config.format.time(_hour, _quarter),
                                    };

                                    if (hour === _hour && quarter === _quarter) {
                                        scope.source = val.value;
                                    }

                                    times.push(val);
                                })(i, j);
                            }
                        }
                    },

                    minTimeWatch = function (post:string, prev:string) {
                        // if our conditional doesn't match, or nothing changed, or invalid num
                        if (!scope.conditional ||
                            post === prev
                        ) { return; } // do nothing

                        var time:string[] = post.split('|');
                        start = p(time[0]);
                        startMinute = p(time[1]);

                        setCurrent();
                        setTimes(start, end, startMinute);
                    };

                setDefaults();

                if (scope.source) {
                    setCurrent();
                } else if (!scope.source && !iAttrs.disableDefault) {
                    // If no source, nor disableDefault set, set default time to now (or closest to now)
                    defaultDate = new Date();

                    if (scope.defaultHourOffset) {
                        defaultDate.setHours(defaultDate.getHours() + p(scope.defaultHourOffset));
                    }

                    if (scope.defaultMinuteOffset) {
                        defaultDate.setMinutes(defaultDate.getMinutes() + p(scope.defaultMinuteOffset));
                    }

                    source = exports.dateTimeQuarters(defaultDate, start, end);
                    hour = source[0];
                    quarter = source[1];
                    defaultDate = source = null;
                }

                if (iAttrs.onChange) {
                    scope.vars.onChange = function (event) {
                        scope.onChange({ event: event });
                    }
                }

                if (iAttrs.conditional) {
                    scope.$watch('conditional', function (post, prev) {
                        if (post !== prev) {
                            if (post) {
                                minTimeWatch(scope.watchMinTime, null)
                            } else {
                                setDefaults();
                                setTimes(start, end);
                            }
                        }
                    })
                } else {
                    scope.conditional = true;
                }

                if (iAttrs.watchMinTime) {
                    scope.$watch('watchMinTime', minTimeWatch);
                }

                setTimes(start, end, startMinute);
            }
        }
    };
};
