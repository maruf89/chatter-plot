/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
module.exports = function (DataBus) {
    var onCall = function (iElem, obj) {
        iElem[0].innerHTML = obj.text;
    };
    return {
        restrict: 'A',
        scope: {
            channel: '@textBroadcast'
        },
        link: {
            post: function (scope, iElem) {
                var callback = _.partial(onCall, iElem);
                DataBus.on(scope.channel, callback);
                scope.$on('$destroy', function () {
                    DataBus.removeListener(scope.channel, callback);
                });
            }
        }
    };
};
//# sourceMappingURL=TextBroadcast.js.map