/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
module.exports = function () {
    return {
        restrict: 'EA',
        replace: true,
        templateUrl: 'views/misc/peopleGoing/Template.html',
        scope: {
            count: '=',
            went: '@',
        },
        link: {
            pre: function (scope) {
                scope.vars = {
                    tense: scope.went === 'true' ? 'WENT' : 'GOING',
                    pluralalize: function (count) {
                        if (count === 1) {
                            return '1';
                        }
                        else {
                            return 'other';
                        }
                    }
                };
            }
        }
    };
};
//# sourceMappingURL=PeopleGoing.js.map