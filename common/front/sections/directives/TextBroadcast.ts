/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

"use strict";

module.exports = function (DataBus:any) {
    var onCall = function (iElem:JQuery, obj:{text:string}) {
        iElem[0].innerHTML = obj.text;
    };

    return {
        restrict: 'A',
        scope: {
            channel: '@textBroadcast'
        },
        link: {
            post: function (scope:cp.IScope, iElem:JQuery) {
                var callback = _.partial(onCall, iElem);
                DataBus.on(scope.channel, callback);

                scope.$on('$destroy', function () {
                    DataBus.removeListener(scope.channel, callback);
                });
            }
        }
    };
};
