/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var groupSort = require('common/util/groupSort'), listUtil = require('common/front/util/list'), moment = require('moment');
/**
 * @class
 * @classdesc handles all events that said user is taking part in or hosting
 */
var MyActivitiesCtrl = (function () {
    function MyActivitiesCtrl(_$scope, _$q, _$state, _Util, _Events, _Interaction, _User, _Maps, _Activities, _DataBus, _Action, _Venues, _locStorage) {
        this._$scope = _$scope;
        this._$q = _$q;
        this._$state = _$state;
        this._Util = _Util;
        this._Events = _Events;
        this._Interaction = _Interaction;
        this._User = _User;
        this._Maps = _Maps;
        this._Activities = _Activities;
        this._DataBus = _DataBus;
        this._Action = _Action;
        this._Venues = _Venues;
        this._locStorage = _locStorage;
        this.data = {
            isSearching: false,
            stack: {
                revertStack: true,
                markerSearch: true,
                name: 'myActivities',
                scrollOffset: -150,
            },
            activities: null,
            whenState: 'current',
        };
        this._cacheKey = {
            request: '_requests',
            event: '_events',
        };
        var action = _$state.params.action && _Util.parseActionArgs(_$state.params.action), state = _$state.params.state, ignoreUrlChange = [/action=.*action:([a-z]+).*objID:([0-9-_]+)/], queryChanged;
        // Deduce whether we want current activities or activity history
        if (state && (state === 'current' || state === 'past')) {
            this.data.whenState = state;
        }
        // Order is important: Create cache listeners
        action && this._cacheCheck(action);
        // check cache
        this._hasCachedOrFetch(this.data.whenState === 'current');
        // call action
        action && this._callAction(action);
        this.data.users = _User.users;
        // Matches /my-activities which is needed in the _queryChanged method
        // and formats it to a regex => \/my-activities
        this._curURLRegex = new RegExp(_$state.current.url.match(/[^?]+/)[0].replace('/', '\/'));
        _Activities.map.initStack(this.data.stack.name);
        queryChanged = this._queryChanged.bind(this, ignoreUrlChange);
        _.defer(function () {
            _DataBus.emit('/state/url/watch', ignoreUrlChange);
            _DataBus.on('/state/url/prevented', queryChanged);
        });
        // Destroy the listener on scope destroy
        _$scope.$on('$destroy', function () {
            _DataBus.emit('/state/url/unwatch', ignoreUrlChange);
        });
    }
    MyActivitiesCtrl.prototype._hasCachedOrFetch = function (current) {
        var sessionID = this._$scope.$root.sessionID, locStorage = this._locStorage, 
        // different storage key depending on whether we're getting current/past activities
        storageKey = "myActiv:" + (current ? 'cur' : 'past') + ":", sessionKey = storageKey + "session", cacheKey = storageKey + "cache", combined, formatted, cached;
        this._storageKey = storageKey;
        if (locStorage.get(sessionKey) === sessionID &&
            (cached = locStorage.get(cacheKey))) {
            combined = this._loadFromCache(cached);
            formatted = this._formatResults(current, combined, true);
            this._setActivities(formatted);
            return;
        }
        locStorage.set(sessionKey, sessionID);
        locStorage.remove(cacheKey);
        this.fetchAll(current);
    };
    MyActivitiesCtrl.prototype._loadFromCache = function (cached) {
        var checkFn = function (key, arr, objID) {
            if (this[key][objID]) {
                arr.push(this[key][objID]);
            }
            return arr;
        };
        this._events = _.reduce(cached.events, _.partial(checkFn, 'cache'), [], this._Events);
        this._requests = _.reduce(cached.requests, _.partial(checkFn, 'requests'), [], this._Interaction);
        return this._requests.concat(this._events);
    };
    MyActivitiesCtrl.prototype._cacheResults = function () {
        var toCache = {
            events: _.pluck(this._events, 'eID'),
            requests: _.pluck(this._requests, 'reqID'),
        };
        this._locStorage.set(this._storageKey + "cache", JSON.stringify(toCache));
        // TODO: build out since cache
        // Whenever we come back to this logic it should check the server with newer > Date
        //this._locStorage.set(`${this._storageKey}since`, (new Date()).getTime());
    };
    MyActivitiesCtrl.prototype.fetchAll = function (current) {
        this.data.isSearching = true;
        return this._fetchAll(current)
            .then(this._organizeResults.bind(this))
            .then(this._formatResults.bind(this, current))
            .then(this._setActivities.bind(this));
    };
    /**
     * @description fetches the activities, I mean duh…
     * @returns {IPromise<T[]>}
     * @private
     */
    MyActivitiesCtrl.prototype._fetchAll = function (current) {
        return this._$q.all([
            this._Interaction.helper.fetchRecent(current),
            this._Events.helper.fetchRecent(current),
        ]);
    };
    /**
     * @description Accepts an array of request objects ->
     *              Caches them (+ any people) ->
     *              Combines them ->
     *              Formats them
     * @param result
     * @returns {{activities, flat}|{activities: any, flat: any[]}}
     * @private
     */
    MyActivitiesCtrl.prototype._organizeResults = function (result) {
        var requests = result[0], events = result[1];
        if (requests.venues) {
            _.each(requests.venues, this._Venues.cacheVenue, this._Venues);
        }
        if (requests.users) {
            this._User.addPeople(requests.users, null, true);
        }
        // moving this here as a result of refactoring events service to cache events on receiving them
        // this ensures that the same version of events gets used as the cached ones
        this._requests = _.map(requests.results, this._Interaction.formatRequest, this._Interaction);
        this._events = _.map(events.events, this._Events.format, this._Events);
        return this._requests.concat(this._events);
    };
    /**
     * @description Formats an unsorted array of possibly different kinds of items
     *
     * @param {boolean} current - whether we're formatting current or past activities
     * @param {array<object>} items - could be both events and requests
     * @param {boolean=} fromCache - whether the results are coming straight from the cache
     * @returns {object}
     * @private
     */
    MyActivitiesCtrl.prototype._formatResults = function (current, items, fromCache) {
        var order = current ? 'asc' : 'desc', cacheChange = this._onRequestLoad && this._onRequestLoad(items);
        cacheChange = this._onEventLoad && this._onEventLoad(items) || cacheChange;
        if (cacheChange || !fromCache) {
            this._cacheResults();
        }
        // group the items in a hashed array
        var grouped = groupSort.formatItems(items, { groupBy: 'date', order: order });
        // next add alternate odd/even classes to each item
        listUtil.alternateGroupsArray(grouped, items.length);
        return {
            activities: grouped,
            flat: items,
        };
    };
    MyActivitiesCtrl.prototype._setActivities = function (items) {
        var Maps = this._Maps;
        this.data.activities = items.activities;
        this.data.isSearching = false;
        this._$scope.$root.safeDigest(this._$scope);
        this._Activities.map.setMarkers(items.flat, {
            replace: true,
            identifier: this.data.stack.name
        }).then(function () {
            _.defer(function () {
                Maps.getInstance().center();
            });
        });
    };
    MyActivitiesCtrl.prototype._queryChanged = function (curPatternArr, event) {
        // If switching off the current state, then sync URL
        if (!this._curURLRegex.test(event.toURL)) {
            return event.sync();
        }
        if (!event || !event.pattern || event.pattern !== curPatternArr[0]) {
            return;
        }
        var state = this._Util.location.decodeParams('state', true), action, decoded;
        if (state && state !== this.data.whenState) {
            return event.sync();
        }
        action = this._Util.location.decodeParams('action', true),
            decoded = action && this._Util.parseActionArgs(action);
        if (action) {
            return this._callAction(decoded);
        }
    };
    MyActivitiesCtrl.prototype._cacheCheck = function (action) {
        if (action.action === 'view') {
            if (action.type === 'request') {
                this._cacheLoadRequest(p(action.objID));
            }
            if (action.type === 'event') {
                this._cacheLoadEvent(p(action.objID));
            }
        }
    };
    /**
     * @param {number} sortReqID - pulls the short ID from a request (only the first group of numbers)
     * @private
     */
    MyActivitiesCtrl.prototype._cacheLoadRequest = function (sortReqID) {
        /**
         * @description checks if the cached request exists in the list and adds it if it doesn't
         * @type {any}
         * @private
         */
        this._onRequestLoad = function (combined) {
            this._onRequestLoad = null;
            var requests = this._requests, thisReq = this._Interaction.requests[sortReqID];
            if (!thisReq) {
                return false;
            }
            if (_.every(requests, function (request) {
                return request.reqID !== thisReq.reqID;
            })) {
                combined.push(thisReq);
                this._requests.push(thisReq);
                return true;
            }
        }.bind(this);
    };
    MyActivitiesCtrl.prototype._cacheLoadEvent = function (objID) {
        /**
         * @description checks if the cached request exists in the list and adds it if it doesn't
         * @type {any}
         * @private
         */
        this._onEventLoad = function (combined) {
            this._onEventLoad = null;
            var thisReq = this._Events.cache[objID];
            if (!thisReq) {
                return false;
            }
            if (_.every(this._events, function (activity) {
                return activity.eID !== objID;
            })) {
                combined.push(thisReq);
                this._events.push(thisReq);
                return true;
            }
        }.bind(this);
    };
    MyActivitiesCtrl.prototype._callAction = function (action) {
        this._Action.executeAction(action).then(this._onListUpdate.bind(this));
    };
    MyActivitiesCtrl.prototype._onListUpdate = function (datum) {
        var service = this['_' + datum.service], scope = this._$scope;
        if (!service) {
            throw new Error('Expecting the response to receive a {string} service parameter');
        }
        service.parseSortRequests(datum, this[this._cacheKey[datum.type]])
            .then(function (results) {
            this[this._cacheKey[datum.type]] = results;
            var isCurrent = this.data.whenState === 'current';
            return this._$q.when(this._formatResults(isCurrent, this._requests.concat(this._events)))
                .then(this._setActivities.bind(this));
        }.bind(this))
            .then(function () {
            scope.$root.safeDigest(scope);
        });
    };
    return MyActivitiesCtrl;
})();
exports.MyActivitiesCtrl = MyActivitiesCtrl;
//# sourceMappingURL=MyActivitiesCtrl.js.map