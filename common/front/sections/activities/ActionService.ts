/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

"use strict";

interface IActionResponse {
    completed:boolean
    status?:string
    request?:cp.interaction.IFormattedRequest
}

class Action implements cp.action.IService {
    constructor(
        private _FlowRequest:cp.modal.flow.IRequestService,
        private _FlowEvent:cp.modal.flow.IEventService,
        private _People:cp.modal.IPeopleService,
        private _Util:any,
        private _DataBus:NodeJS.EventEmitter
    ) {

    }

    private _getActionType(type:string):string {
        if (type === 'request') {
            return 'FlowRequest';
        } else if (type === 'event') {
            return 'FlowEvent';
        }

        return 'People';
    }

    private _callbackServiceType(type:string):string {
        if (type === 'request') {
            return 'Interaction';
        } else if (type === 'event') {
            return 'Events';
        }

        return 'People';
    }

    private _action(service:any, action:string, reqID:string, subtype?:string):ng.IPromise<any> {
        var data:any;

        switch (action) {
            case 'accept':
            case 'deny':
            case 'cancel':
                data = {
                    status: action,
                    reqID: reqID,
                };

                return action === 'accept' ? service.respondRequest(data) : service.deny(data);
            case 'edit':
                return service.edit(reqID, subtype);
            case 'duplicate':
                return service.duplicate(reqID, subtype);
            case 'view':
            default:
                return service.view(reqID, subtype);
        }
    }

    public executeAction(action:cp.action.IAction):ng.IPromise<any> {
        // Runs an action
        var serviceName:string = this._getActionType(action.type),
            callbackService:string = this._callbackServiceType(action.type),
            Util = this._Util,
            Bus = this._DataBus,

            // returns only the first part: Interaction => _InteractionListing
            service = this['_' + serviceName];

        // Update the list upon completing the action
        return this._action(service, action.action, action.objID, action.subtype)
            .then(function (response:IActionResponse) {
                var passObj:any = _.clone(action),
                    statusMsgKey:string;

                passObj.service = callbackService;

                // If for some reason the action did not accomplish the request
                // could be if we're accepting a request that's already been accepted
                if (!response.completed) {
                    // if for ex. we're accepting an event that's already been declined
                    if (response.status !== action.action) {
                        // update with the new status
                        passObj.action = response.status;

                        statusMsgKey = 'activities.ERROR-REQUEST_' + (response.status === 'edit' ? 'EDITED' : 'DELETED');

                        Bus.emit('yap', {
                            type: 'warning',
                            title: 'common.UPDATE',
                            body: statusMsgKey,
                            duration: 10000,
                        });
                    }
                }

                if (response.request) {
                    passObj.request = response.request;
                }

                return passObj;
            }).finally(function () {
                Util.location.stripQP(true, ['action']);
            });
    }
}

module.exports = Action;
