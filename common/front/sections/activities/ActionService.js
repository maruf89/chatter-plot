/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
var Action = (function () {
    function Action(_FlowRequest, _FlowEvent, _People, _Util, _DataBus) {
        this._FlowRequest = _FlowRequest;
        this._FlowEvent = _FlowEvent;
        this._People = _People;
        this._Util = _Util;
        this._DataBus = _DataBus;
    }
    Action.prototype._getActionType = function (type) {
        if (type === 'request') {
            return 'FlowRequest';
        }
        else if (type === 'event') {
            return 'FlowEvent';
        }
        return 'People';
    };
    Action.prototype._callbackServiceType = function (type) {
        if (type === 'request') {
            return 'Interaction';
        }
        else if (type === 'event') {
            return 'Events';
        }
        return 'People';
    };
    Action.prototype._action = function (service, action, reqID, subtype) {
        var data;
        switch (action) {
            case 'accept':
            case 'deny':
            case 'cancel':
                data = {
                    status: action,
                    reqID: reqID,
                };
                return action === 'accept' ? service.respondRequest(data) : service.deny(data);
            case 'edit':
                return service.edit(reqID, subtype);
            case 'duplicate':
                return service.duplicate(reqID, subtype);
            case 'view':
            default:
                return service.view(reqID, subtype);
        }
    };
    Action.prototype.executeAction = function (action) {
        // Runs an action
        var serviceName = this._getActionType(action.type), callbackService = this._callbackServiceType(action.type), Util = this._Util, Bus = this._DataBus, 
        // returns only the first part: Interaction => _InteractionListing
        service = this['_' + serviceName];
        // Update the list upon completing the action
        return this._action(service, action.action, action.objID, action.subtype)
            .then(function (response) {
            var passObj = _.clone(action), statusMsgKey;
            passObj.service = callbackService;
            // If for some reason the action did not accomplish the request
            // could be if we're accepting a request that's already been accepted
            if (!response.completed) {
                // if for ex. we're accepting an event that's already been declined
                if (response.status !== action.action) {
                    // update with the new status
                    passObj.action = response.status;
                    statusMsgKey = 'activities.ERROR-REQUEST_' + (response.status === 'edit' ? 'EDITED' : 'DELETED');
                    Bus.emit('yap', {
                        type: 'warning',
                        title: 'common.UPDATE',
                        body: statusMsgKey,
                        duration: 10000,
                    });
                }
            }
            if (response.request) {
                passObj.request = response.request;
            }
            return passObj;
        }).finally(function () {
            Util.location.stripQP(true, ['action']);
        });
    };
    return Action;
})();
module.exports = Action;
//# sourceMappingURL=ActionService.js.map