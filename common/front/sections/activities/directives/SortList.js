/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var templates = {
    request: 'views/interaction/directives/listing/Template.html',
    event: 'views/event/directives/listing/Template.html'
};
function SortListDirective($templateCache, Interaction, Events, People, Venues, DataBus, Util, $analytics) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            items: '=',
            headDisplay: '@',
            users: '=?',
            stack: '=',
            isSearching: '=',
        },
        transclude: true,
        template: $templateCache.get('views/activities/directives/sortList/Template.html'),
        link: {
            pre: function (scope) {
                scope.vars = {
                    getTemplate: function (item) {
                        return templates[item.activityType];
                    },
                    headDisplay: scope.sortKey || 'date',
                };
                scope.cache = {
                    venues: Venues.venues,
                };
                scope.Interaction = Interaction.helper;
                scope.Event = Events.helper;
                scope.Tandem = People.helper;
            },
            post: function (scope, iElem) {
                var vars = scope.vars, listeners = _.map([
                    'event',
                    'request',
                ], function (type) { return "/map/" + type + "/click"; }), $scrollParent = iElem.closest('.pane-wrapper-scroll');
                vars.scrollToView = function (marker) {
                    var $row = $("#activityRow-" + marker.model._id), offset = CP.Global.vars.navHeight;
                    if (!$row.length) {
                        return false;
                    }
                    Util.$scrollTo($row, $scrollParent, { padding: offset });
                    Util.$hilite($row, 1000);
                    $analytics.eventTrack('click', {
                        category: 'maps',
                        label: marker.model._class + ' results: marker click',
                        value: p(marker.model._id),
                    });
                };
                DataBus.on(listeners[0], vars.scrollToView);
                DataBus.on(listeners[1], vars.scrollToView);
                scope.$on('$destroy', function () {
                    DataBus.removeListener(listeners[0], vars.scrollToView);
                    DataBus.removeListener(listeners[1], vars.scrollToView);
                });
            }
        },
    };
}
exports.SortListDirective = SortListDirective;
//# sourceMappingURL=SortList.js.map