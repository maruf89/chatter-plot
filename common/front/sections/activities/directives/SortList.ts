/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var templates:any = {
    request: 'views/interaction/directives/listing/Template.html',
    event: 'views/event/directives/listing/Template.html'
};

export function SortListDirective (
    $templateCache:ng.ITemplateCacheService,
    Interaction:cp.interaction.IService,
    Events:cp.event.IService,
    People:cp.user.IPeopleService,
    Venues:cp.venue.IService,
    DataBus:NodeJS.EventEmitter,
    Util:any,
    $analytics:Angulartics.IAnalyticsService
) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            items:          '=',        // {array<object>}
            headDisplay:    '@',        // {string} the key of the item to that the groups are sorted by (date)
            users:          '=?',       // {object} a users map
            stack:          '=',        // {object}
            isSearching:    '=',        // {boolean} whether to show the loader
        },
        transclude: true,
        template: $templateCache.get('views/activities/directives/sortList/Template.html'),
        link: {
            pre: function (scope) {
                scope.vars = {
                    getTemplate: function (item:cp.activities.IActivityItem):string {
                        return templates[item.activityType];
                    },
                    headDisplay: scope.sortKey || 'date',
                };

                scope.cache = {
                    venues: Venues.venues,
                };

                scope.Interaction = Interaction.helper;
                scope.Event = Events.helper;
                scope.Tandem = People.helper;
            },

            post: function (scope, iElem) {
                var vars = scope.vars,

                    listeners:string[] = _.map([
                        'event',
                        'request',
                    ], function (type:string):string { return `/map/${type}/click`; }),

                    $scrollParent = iElem.closest('.pane-wrapper-scroll');

                vars.scrollToView = function (marker:cp.activities.IMarker) {
                    var $row = $(`#activityRow-${marker.model._id}`),
                        offset:number = CP.Global.vars.navHeight;

                    if (!$row.length) {
                        return false
                    }

                    Util.$scrollTo($row, $scrollParent, { padding: offset });
                    Util.$hilite($row, 1000);

                    $analytics.eventTrack('click', {
                        category: 'maps',
                        label: marker.model._class + ' results: marker click',
                        value: p(marker.model._id),
                    });
                };


                DataBus.on(listeners[0], vars.scrollToView);
                DataBus.on(listeners[1], vars.scrollToView);
                scope.$on('$destroy', function () {
                    DataBus.removeListener(listeners[0], vars.scrollToView);
                    DataBus.removeListener(listeners[1], vars.scrollToView);
                });
            }
        },
    };
}
