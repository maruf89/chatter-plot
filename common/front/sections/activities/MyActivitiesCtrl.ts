/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var groupSort = require('common/util/groupSort'),
    listUtil = require('common/front/util/list'),
    moment = require('moment');

/**
 * @class
 * @classdesc handles all events that said user is taking part in or hosting
 */
export class MyActivitiesCtrl {
    public data:any = {
        isSearching: false,
        stack: {
            revertStack: true,
            markerSearch: true,
            name: 'myActivities',
            scrollOffset: -150,
        },
        activities: null,
        whenState: 'current',
    };

    private _storageKey:string;
    private _requests:cp.interaction.IRequest[];
    private _events:cp.event.IEventBase[];
    private _curURLRegex:RegExp;
    private _onRequestLoad:(combined:any[]) => boolean;
    private _onEventLoad:(combined:any[]) => boolean;
    private _cacheKey:any = {
        request: '_requests',
        event: '_events',
    };

    constructor(
        private _$scope:cp.IControllerScope,
        private _$q:ng.IQService,
        private _$state:ng.ui.IStateService,
        private _Util:any,
        private _Events:cp.event.IService,
        private _Interaction:cp.interaction.IService,
        private _User:cp.user.IService,
        private _Maps:cp.map.IService,
        private _Activities:cp.activities.IService,
        private _DataBus:NodeJS.EventEmitter,
        private _Action:cp.action.IService,
        private _Venues:cp.venue.IService,
        private _locStorage:any
    ) {
        var action:cp.action.IAction = _$state.params.action && _Util.parseActionArgs(_$state.params.action),
            state:string = _$state.params.state,
            ignoreUrlChange:RegExp[] = [/action=.*action:([a-z]+).*objID:([0-9-_]+)/],
            queryChanged;

        // Deduce whether we want current activities or activity history
        if (state && (state === 'current' || state === 'past')) {
            this.data.whenState = state;
        }

        // Order is important: Create cache listeners
        action && this._cacheCheck(action);

        // check cache
        this._hasCachedOrFetch(this.data.whenState === 'current');

        // call action
        action && this._callAction(action);

        this.data.users = _User.users;

        // Matches /my-activities which is needed in the _queryChanged method
        // and formats it to a regex => \/my-activities
        this._curURLRegex = new RegExp(_$state.current.url.match(/[^?]+/)[0].replace('/', '\/'));

        _Activities.map.initStack(this.data.stack.name);

        queryChanged = this._queryChanged.bind(this, ignoreUrlChange);

        _.defer(function () {
            _DataBus.emit('/state/url/watch', ignoreUrlChange);
            _DataBus.on('/state/url/prevented', queryChanged);
        });

        // Destroy the listener on scope destroy
        _$scope.$on('$destroy', function () {
            _DataBus.emit('/state/url/unwatch', ignoreUrlChange);
        });
    }

    private _hasCachedOrFetch(current:boolean):void {
        var sessionID:string = this._$scope.$root.sessionID,
            locStorage = this._locStorage,

            // different storage key depending on whether we're getting current/past activities
            storageKey:string = `myActiv:${current ? 'cur' : 'past'}:`,
            sessionKey:string = `${storageKey}session`,
            cacheKey:string = `${storageKey}cache`,

            combined:any[], formatted:any,
            cached:any;

        this._storageKey = storageKey;

        if (locStorage.get(sessionKey) === sessionID &&
            (cached = locStorage.get(cacheKey))
        ) {
            combined = this._loadFromCache(cached);
            formatted = this._formatResults(current, combined, true);
            this._setActivities(formatted);
            return;
        }

        locStorage.set(sessionKey, sessionID);
        locStorage.remove(cacheKey);
        this.fetchAll(current);
    }

    private _loadFromCache(cached:any):any[] {
        var checkFn = function (key, arr, objID) {
            if (this[key][objID]) {
                arr.push(this[key][objID])
            }

            return arr;
        };

        this._events = _.reduce(cached.events, _.partial(checkFn, 'cache'), [], this._Events);
        this._requests = _.reduce(cached.requests, _.partial(checkFn, 'requests'), [], this._Interaction);

        return this._requests.concat(this._events);
    }

    private _cacheResults() {
        var toCache:any = {
            events: _.pluck(this._events, 'eID'),
            requests: _.pluck(this._requests, 'reqID'),
        };

        this._locStorage.set(`${this._storageKey}cache`, JSON.stringify(toCache));

        // TODO: build out since cache
        // Whenever we come back to this logic it should check the server with newer > Date
        //this._locStorage.set(`${this._storageKey}since`, (new Date()).getTime());
    }

    public fetchAll(current:boolean) {
        this.data.isSearching = true;
        return this._fetchAll(current)
            .then(this._organizeResults.bind(this))
            .then(this._formatResults.bind(this, current))
            .then(this._setActivities.bind(this));
    }

    /**
     * @description fetches the activities, I mean duh…
     * @returns {IPromise<T[]>}
     * @private
     */
    private _fetchAll<T>(current:boolean):ng.IPromise<T[]> {
        return this._$q.all([
            this._Interaction.helper.fetchRecent(current),
            this._Events.helper.fetchRecent(current),
        ])
    }

    /**
     * @description Accepts an array of request objects ->
     *              Caches them (+ any people) ->
     *              Combines them ->
     *              Formats them
     * @param result
     * @returns {{activities, flat}|{activities: any, flat: any[]}}
     * @private
     */
    private _organizeResults(result:any[]) {
        var requests:cp.interaction.IServerResponse = result[0],
            events:cp.event.IServerResponse = result[1];

        if (requests.venues) {
            _.each(requests.venues, this._Venues.cacheVenue, this._Venues);
        }

        if (requests.users) {
            this._User.addPeople(requests.users, null, true);
        }

        // moving this here as a result of refactoring events service to cache events on receiving them
        // this ensures that the same version of events gets used as the cached ones
        this._requests = _.map(
            requests.results,
            this._Interaction.formatRequest,
            this._Interaction
        );

        this._events = _.map(events.events, this._Events.format, this._Events);

        return this._requests.concat(this._events);
    }

    /**
     * @description Formats an unsorted array of possibly different kinds of items
     *
     * @param {boolean} current - whether we're formatting current or past activities
     * @param {array<object>} items - could be both events and requests
     * @param {boolean=} fromCache - whether the results are coming straight from the cache
     * @returns {object}
     * @private
     */
    private _formatResults(current:boolean, items:any[], fromCache?:boolean) {
        var order:string = current ? 'asc' : 'desc',
            cacheChange:boolean = this._onRequestLoad && this._onRequestLoad(items);
        cacheChange = this._onEventLoad && this._onEventLoad(items) || cacheChange;

        if (cacheChange || !fromCache) {
            this._cacheResults();
        }

        // group the items in a hashed array
        var grouped = groupSort.formatItems(items, { groupBy: 'date', order: order });

        // next add alternate odd/even classes to each item
        listUtil.alternateGroupsArray(grouped, items.length);

        return {
            activities: grouped,
            flat: items,
        };
    }

    private _setActivities(items:{ activities:any; flat:cp.activities.IActivityItem[]; }):void {
        var Maps:cp.map.IService = this._Maps;

        this.data.activities = items.activities;
        this.data.isSearching = false;
        this._$scope.$root.safeDigest(this._$scope);

        this._Activities.map.setMarkers(items.flat, {
            replace: true,
            identifier: this.data.stack.name
        }).then(function () {
            _.defer(function () {
                Maps.getInstance().center();
            });
        })
    }

    private _queryChanged(curPatternArr:RegExp[], event:any) {
        // If switching off the current state, then sync URL
        if (!this._curURLRegex.test(event.toURL)) {
            return event.sync();
        }

        if (!event || !event.pattern || event.pattern !== curPatternArr[0]) {
            return;
        }

        var state = this._Util.location.decodeParams('state', true),
            action,
            decoded;

        if (state && state !== this.data.whenState) {
            return event.sync();
        }

        action = this._Util.location.decodeParams('action', true),
            decoded = action && this._Util.parseActionArgs(action);

        if (action) {
            return this._callAction(decoded);
        }
    }

    private _cacheCheck(action:cp.action.IAction):void {
        if (action.action === 'view') {
            if (action.type === 'request') {
                this._cacheLoadRequest(p(action.objID));
            }

            if (action.type === 'event') {
                this._cacheLoadEvent(p(action.objID));
            }
        }
    }

    /**
     * @param {number} sortReqID - pulls the short ID from a request (only the first group of numbers)
     * @private
     */
    private _cacheLoadRequest(sortReqID:number):void {
        /**
         * @description checks if the cached request exists in the list and adds it if it doesn't
         * @type {any}
         * @private
         */
        this._onRequestLoad = function (combined:any[]):boolean {
            this._onRequestLoad = null;

            var requests = this._requests,
                thisReq:cp.interaction.IFormattedRequest = this._Interaction.requests[sortReqID];

            if (!thisReq) {
                return false;
            }

            if (_.every(requests, function (request:cp.interaction.IFormattedRequest) {
                    return request.reqID !== thisReq.reqID;
                })) {
                combined.push(thisReq);
                this._requests.push(thisReq);
                return true
            }
        }.bind(this);
    }

    private _cacheLoadEvent(objID:number):void {
        /**
         * @description checks if the cached request exists in the list and adds it if it doesn't
         * @type {any}
         * @private
         */
        this._onEventLoad = function (combined:any[]):boolean {
            this._onEventLoad = null;

            var thisReq:cp.event.IEvent = this._Events.cache[objID];

            if (!thisReq) {
                return false;
            }

            if (_.every(this._events, function (activity:cp.interaction.IFormattedRequest) {
                    return activity.eID !== objID;
                })) {
                combined.push(thisReq);
                this._events.push(thisReq);
                return true;
            }
        }.bind(this);
    }

    private _callAction(action:cp.action.IAction):ng.IPromise<cp.action.IAction> {
        this._Action.executeAction(action).then(this._onListUpdate.bind(this));
    }

    private _onListUpdate(datum:cp.action.IAction):void {
        var service = this['_' + datum.service],
            scope = this._$scope;

        if (!service) {
            throw new Error('Expecting the response to receive a {string} service parameter')
        }

        service.parseSortRequests(datum, this[this._cacheKey[datum.type]])
            .then(function (results) {
                this[this._cacheKey[datum.type]] = results;

                var isCurrent = this.data.whenState === 'current';

                return this._$q.when(
                    this._formatResults(isCurrent, this._requests.concat(this._events))
                    )
                    .then(this._setActivities.bind(this));
            }.bind(this))

            .then(function () {
                scope.$root.safeDigest(scope);
            });
    }
}