/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var People = (function () {
    function People(_SocketIo, _User, Helper, PeopleMap) {
        this._SocketIo = _SocketIo;
        this._User = _User;
        this.helper = Helper.get(this);
        this.map = new PeopleMap(this);
    }
    People.prototype.fetch = function (opts) {
        return this._SocketIo.onEmitSock('/user/fetch', opts, true);
    };
    People.prototype.format = function (people) {
        var noPhoto = [], userID = this._User.userID;
        if (!_.isArray(people)) {
            people = [people];
        }
        return _.reduce(people, function (all, person) {
            person = person._source || person;
            if (person.userID === userID) {
                return all;
            }
            person.fullName = this._User.format.name(person);
            var which = person.picture ? all : noPhoto;
            which.push(person);
            return all;
        }, [], this)
            .concat(noPhoto);
    };
    People.prototype.get = function (data) {
        return this._User.get(data);
    };
    return People;
})();
module.exports = People;
//# sourceMappingURL=Service.js.map