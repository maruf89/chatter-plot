/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

import {ListCtrl} from 'front/sections/search/AbstractListCtrl';

export class PeopleListCtrl extends ListCtrl {
    private _destroyed:boolean;

    constructor(
        protected _$q:ng.IQService,
        protected _$scope:cp.activities.IListDirectiveScope,
        protected _Activities:any,
        protected _DataBus:NodeJS.EventEmitter,
        protected _Util:any,
        protected _User:any,
        protected _Maps:any,
        protected _locStorage:any,
        protected _AdSlot:any,
        People:any
    ) {
        super(
            {
                listName: 'people',
                resultsKey: 'users',
                service: People,
            },
            _$scope,
            _$q,
            _Activities,
            _AdSlot,
            _Maps,
            _DataBus,
            _Util,
            _locStorage
        );

        var mapObj,
            prevMaxZoom,
            self = this;

        _Maps.getInstance()
            .rootObjPromise()
            .then(function (map) {
                if (self._destroyed) {
                    return true;
                }

                mapObj = map;
                prevMaxZoom = mapObj.maxZoom;
                mapObj.maxZoom = 15;

                if (mapObj.getZoom() > mapObj.maxZoom) {
                    return mapObj.setZoom(mapObj.maxZoom);
                }
            });

        _$scope.$on('$destroy', function () {
            mapObj && (mapObj.maxZoom = prevMaxZoom);
            self._destroyed = true;
        });
    }

    loadItemFromMarker(marker):ng.IPromise<any> {
        var scope = this._$scope,
            userID:number = scope.$root.userID,
            deferred = this._$q.defer();

        if (userID && String(userID) === marker.model._id) {
            return this._$q.reject();
        }

        this.loadSingle(marker.model._id)
            .then(this.updateList.bind(this))
            .then(function () {
                scope.$root.safeDigest(scope);
                _.defer(deferred.resolve);
            });

        return deferred.promise;
    }

    loadSingle(ID) {
        return this._User.get(ID).then(this._formatItems.bind(this))
    }
}
