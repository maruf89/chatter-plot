/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var AbstractListCtrl_1 = require('front/sections/search/AbstractListCtrl');
var PeopleListCtrl = (function (_super) {
    __extends(PeopleListCtrl, _super);
    function PeopleListCtrl(_$q, _$scope, _Activities, _DataBus, _Util, _User, _Maps, _locStorage, _AdSlot, People) {
        _super.call(this, {
            listName: 'people',
            resultsKey: 'users',
            service: People,
        }, _$scope, _$q, _Activities, _AdSlot, _Maps, _DataBus, _Util, _locStorage);
        this._$q = _$q;
        this._$scope = _$scope;
        this._Activities = _Activities;
        this._DataBus = _DataBus;
        this._Util = _Util;
        this._User = _User;
        this._Maps = _Maps;
        this._locStorage = _locStorage;
        this._AdSlot = _AdSlot;
        var mapObj, prevMaxZoom, self = this;
        _Maps.getInstance()
            .rootObjPromise()
            .then(function (map) {
            if (self._destroyed) {
                return true;
            }
            mapObj = map;
            prevMaxZoom = mapObj.maxZoom;
            mapObj.maxZoom = 15;
            if (mapObj.getZoom() > mapObj.maxZoom) {
                return mapObj.setZoom(mapObj.maxZoom);
            }
        });
        _$scope.$on('$destroy', function () {
            mapObj && (mapObj.maxZoom = prevMaxZoom);
            self._destroyed = true;
        });
    }
    PeopleListCtrl.prototype.loadItemFromMarker = function (marker) {
        var scope = this._$scope, userID = scope.$root.userID, deferred = this._$q.defer();
        if (userID && String(userID) === marker.model._id) {
            return this._$q.reject();
        }
        this.loadSingle(marker.model._id)
            .then(this.updateList.bind(this))
            .then(function () {
            scope.$root.safeDigest(scope);
            _.defer(deferred.resolve);
        });
        return deferred.promise;
    };
    PeopleListCtrl.prototype.loadSingle = function (ID) {
        return this._User.get(ID).then(this._formatItems.bind(this));
    };
    return PeopleListCtrl;
})(AbstractListCtrl_1.ListCtrl);
exports.PeopleListCtrl = PeopleListCtrl;
//# sourceMappingURL=ListCtrl.js.map