/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

class People implements cp.user.ITandemsService {
    public map:any;
    public helper:cp.user.ITandemsServiceHelper;

    constructor(
        private _SocketIo:any,
        private _User:cp.user.IService,
        Helper,
        PeopleMap
    ) {
        this.helper = Helper.get(this);
        this.map = new PeopleMap(this);
    }

    fetch(opts) {
        return this._SocketIo.onEmitSock('/user/fetch', opts, true);
    }

    format(people) {
        var noPhoto = [],
            userID = this._User.userID;

        if (!_.isArray(people)) {
            people = [people];
        }

        return _.reduce(people, function (all, person:any) {
            person = person._source || person;

            if (person.userID === userID) {
                return all;
            }

            person.fullName = this._User.format.name(person);

            var which = person.picture ? all : noPhoto;
            which.push(person);

            return all;
        }, [], this)
            .concat(noPhoto);
    }

    get(data) {
        return this._User.get(data);
    }
}

module.exports = People;
