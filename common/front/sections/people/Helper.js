/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var Helper = (function () {
    function Helper(_People) {
        this._People = _People;
    }
    return Helper;
})();
exports.Helper = function () {
    var instance = null;
    return {
        get: function (People) {
            return instance || (instance = new Helper(People));
        }
    };
};
exports.Map = function (Util) {
    return function (People) {
        this.getWindow = function (marker) {
            var _source = ['name'];
            if (!marker.hidden) {
                _source.push('locationDisplayName');
            }
            return People.get({
                _source: _source,
                venues: !marker.hidden,
                type: marker._type || 'tandem',
                eIDs: [marker._id]
            })
                .then(function (tandem) {
                // If we're not searching for venues we'll get back an object
                if (marker.hidden) {
                    tandem.venue = {
                        coords: marker.coords,
                        eID: p(marker._id)
                    };
                }
                else {
                    // otherwise we'll be getting back an arry
                    tandem = tandem[0];
                }
                return tandem;
            });
        };
        this.formatWindow = function (activity, options) {
            var address, opts, locName = address = '';
            if (!activity.venue.hidden) {
                locName = activity.locationDisplayName || activity.venue.name;
                address = Util.geo.format.address(activity.venue);
            }
            opts = {
                coords: {
                    latitude: activity.venue.coords.lat || activity.venue.coords.latitude,
                    longitude: activity.venue.coords.lon || activity.venue.coords.longitude
                },
                template: 'views/activities/templates/addressWindow.html',
                scope: {
                    activity: {
                        name: activity.name,
                        locationName: locName,
                        address: address
                    }
                }
            };
            if (options && options.showDirections) {
                opts.scope.activity.directionsLink = Util.geo.toGMapUrl(activity.venue, 'directions');
            }
            return opts;
        };
    };
};
//# sourceMappingURL=Helper.js.map