/// <reference path="../../../typings/node/node.d.ts" />

angular.module('sexyDatepicker', [])
    .directive('sexyDatepicker', ['$parse', function ($parse) {
        var Datepicker = (function () {

            var DAYS_A_WEEK = 7
                , MAX_WEEKS_IN_MONTH = 6
                , months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
                , days = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa']
                , today = new Date()
                , dayClass = 'days-cell'
                , jqSupport = !!window.jQuery;

            today.setHours(0,0,0,0);

            function isLeapYear(year) {
                return year % 4 === 0 && !(year % 100 === 0 && year % 400 != 0)
            }

            function monthDays(date) {
                var month = date.getMonth()
                    , monthDays = [31, isLeapYear(date.getFullYear()) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
                return monthDays[month]
            }

            function buildDay(node) {
                var _this = this
                    , firstDay = new Date(this.selectedDate)
                    , currentDay = new Date(this.selectedDate)
                    , data = node['data']
                    , cssClass = dayClass
                    , offset, index;

                firstDay.setDate(1);
                index = (data.col + 1 + (data.row * DAYS_A_WEEK)) - firstDay.getDay();
                currentDay.setDate(index);
                value = currentDay.getDate();

                if (this.disablePast && currentDay < this.minDate) {
                    cssClass += ' past'
                } else {
                    /* Setup class */
                    if (currentDay.getTime() === this.selectedDate.getTime()) {
                        cssClass += ' selected';
                    }
                    if (index <= 0 || index > monthDays(this.selectedDate)) {
                        cssClass += ' off';
                    }
                    if (!jqSupport) {
                        node['onclick'] = function () {
                            _this.setSelectedDate(currentDay);
                        };
                    } else {
                        data.currentDay = currentDay;
                    }
                }
                node['class'] = cssClass;
                
                node['value'] = currentDay.getDate();
            }

            function bindJQuery(_this) {
                if (_this.jqBound || !jqSupport) { return false; }

                var selector = '.' + dayClass + ':not(.past)';

                $(_this.container).on('click', selector, { _this: _this }, onJQClick);
                _this.jqBound = true;
            }

            function onJQClick(event) {
                var _this = event.data._this,
                    currentDay = event.target.data.currentDay;

                _this.setSelectedDate(currentDay);
            }

            function buildNode(node) {
                var _this = this;

                function _build(node) {
                    var element, i, max;

                    /* Run before build */
                    if ('beforebuild' in node) {
                        node['beforebuild'].call(_this, node);
                    }

                    /* Create the element */
                    if (!('element' in node)) {
                        node['element'] = document.createElement(node['tag']);
                    }

                    /* Attach a data hash to the element */
                    if ('data' in node) {
                        node['element'].data = node['data'];
                    }

                    /* Set css class. */
                    if ('class' in node) {
                        node['element'].setAttribute('class', node['class']);
                    }

                    /* Set inner html. */
                    if ('value' in node) {
                        if (typeof node['value'] === 'function') {
                            node['element'].innerHTML = node['value']();
                        }
                        else {
                            node['element'].innerHTML = node['value'];
                        }
                    }

                    /* Set onclick */
                    if ('onclick' in node) {
                        node['element'].onclick = node['onclick'];
                    }

                    /* Go through children. */
                    if ('children' in node) {
                        for (i = 0, max = node['children'].length; i < max; i++) {
                            node['element'].appendChild(_build(node['children'][i]));
                        }
                    }

                    return node['element'];
                }
                return _build(node);
            }

            function destroyNode(node) {
                function _destroy(node) {
                    if ('children' in node) {
                        for (i = 0, max = node['children'].length; i < max; i++) {
                            _destroy(node['children'][i]);
                        }
                    }

                    /* Remove element from DOM. */
                    node['element'].parentNode.removeChild(node['element']);
                    /* Remove reference to invisible DOM element. */
                    node['element'] = null;
                    delete node['element'];

                    return node;
                }

                return _destroy(node);
            }

            function Datepicker(element, options) {
                var _this = this;

                options = options || {};

                this.onDateChanged  = options.onDateChanged;
                this.format         = options.format;
                this.container      = element;
                this.monthNames     = options.monthNames || months;
                this.dayNames       = options.dayNames || days;
                this.disablePast    = !!options.disablePast;
                this.jqBound        = !jqSupport; // If no jQuery will not bind anyways
                this.minDate        = options.minDate || today; // defaults to undefined

                /* Calendar DOM element tree */
                this.elements       = [];
                /* Year */
                this.elements[0] = { tag: 'div', children: [], class: 'year' };
                this.elements[0]['children'][0] = { tag: 'a', class: 'dec', onclick: function () { _this.incrementMonth(-12); return false; } };
                this.elements[0]['children'][1] = { tag: 'a', class: 'inc', onclick: function () { _this.incrementMonth(12); return false; } };
                this.elements[0]['children'][2] = { tag: 'label', value: function () { return _this.selectedDate.getFullYear(); } };
                /* Month */
                this.elements[1] = { tag: 'div', children: [], class: 'month' };
                this.elements[1]['children'][0] = { tag: 'a', class: 'dec', onclick: function () { _this.incrementMonth(-1); return false; } };
                this.elements[1]['children'][1] = { tag: 'a', class: 'inc', onclick: function () { _this.incrementMonth(1); return false; } }
                this.elements[1]['children'][2] = { tag: 'label', value: function () { return _this.monthNames[_this.selectedDate.getMonth()]; } };
                /* Day names */
                this.elements[2] = { tag: 'div', children: [], class: 'days-names' }
                for (var i = 0; i < DAYS_A_WEEK; i++) {
                    this.elements[2]['children'][i] = { tag: 'label', value: _this.dayNames[i] };
                }
                /* Dates */
                this.elements[3] = { tag: 'div', children: [], class: 'days' };
                for (var i = 0; i < MAX_WEEKS_IN_MONTH; i++) {
                    this.elements[3]['children'][i] = { tag: 'div', children: [], class: 'days-row' }
                    for (var j = 0; j < DAYS_A_WEEK; j++) {
                        this.elements[3]['children'][i]['children'][j] = { tag: 'div', data: { row: i, col: j }, beforebuild: buildDay };
                    }
                }

                /* Link DOM element with Datepicker instance. */
                this.container.datepicker = this;

                /* Initialize selected date */
                if (options.selectedDate) {
                    this.setSelectedDate(options.selectedDate, true);
                }
                else {
                    this.setSelectedDate(this.minDate);
                }
            }

            Datepicker.prototype.incrementMonth = function (amount) {
                /* Increase by one if no amount is passed. */
                amount = amount || 1;

                var month = this.selectedDate.getMonth()
                    , newDate = new Date(this.selectedDate);

                newDate.setMonth(month + amount);

                return this.setSelectedDate(newDate, true);
            };

            Datepicker.prototype.setSelectedDate = function (date, suppressCallback) {
                /* Throw error if no date is passed */
                if (!date) { return; }

                this.selectedDate = date;
                this.build();
                if (typeof this.onDateChanged === 'function' && !suppressCallback) {
                    this.onDateChanged(date);
                }

                return this.selectedDate;
            };

            Datepicker.prototype.build = function () {
                /* Build one by one */
                for (var i = 0, max = this.elements.length; i < max; i++) {
                    this.container.appendChild(buildNode.call(this, this.elements[i]));
                }

                bindJQuery(this)

                return this.container;
            };

            Datepicker.prototype.destroy = function () {
                for (var i = 0, max = this.elements.length; i < max; i++) {
                    destroyNode.call(this, this.elements[i]);
                }

                /* Remove from DOM. */
                this.container.parentNode.removeChild(this.container);;
                /* Destroy reference to invisible DOM element. */
                this.container = null;

                return this.selectedDate;
            };

            return Datepicker;

        }());

        return {
            restrict: 'E',
            link: function (scope, elem, attrs) {
                var options = scope.options || {},
                    datepicker;

                if (!options.monthNames) { options.monthNames = scope[attrs.monthNames]; }
                if (!options.dayNames) { options.dayNames = scope[attrs.dayNames]; }

                /* Watch ngModel */
                if (!!attrs.ngModel) {
                    if (scope.minDate) {
                        options.minDate = scope.minDate;
                    }

                    options.selectedDate = scope[attrs.ngModel] || scope.minDate || new Date();
                    options.selectedDate.setHours(0,0,0,0)
                    scope.$watch(attrs.ngModel, function (newDate) {
                        datepicker.setSelectedDate(newDate);
                    });

                    options.onChange = options.onChange || function () {};

                    options.onDateChanged = function (selectedDate) {
                        $parse(attrs.ngModel).assign(scope, selectedDate);
                        if (scope.$root.$$phase === '$apply' || scope.$root.$$phase === '$digest') {
                            options.onChange(selectedDate);
                        } else {
                            scope.$apply(function () {
                                options.onChange(selectedDate);
                            });
                        }
                    };
                }

                datepicker = new Datepicker(elem[0], options);

                if (scope.minDate) {
                    scope.$watch('minDate', function (post, prev) {
                        if (post !== prev && post instanceof Date) {
                            datepicker.minDate = post;

                            if (options.selectedDate instanceof Date && post > options.selectedDate) {
                                datepicker.setSelectedDate(post, true);
                                options.onDateChanged(post);
                            } else if (!options.selectedDate instanceof Date) {
                                datepicker.setSelectedDate(post, true);
                            }
                        }
                    });
                }
            }
        };
    }]);
