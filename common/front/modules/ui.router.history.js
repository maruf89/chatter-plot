/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />
angular.module("ui.router.history", ["ui.router"])
    .service("$history", ['$state', function ($state) {
        var history = [], maxLength = 3;
        angular.extend(this, {
            push: function (state, params) {
                if (history.unshift({ state: state, params: params }) > maxLength) {
                    history.length = maxLength;
                }
            },
            all: function () {
                return history;
            },
            go: function (step) {
                // TODO:
                // (1) Determine # of states in stack with URLs, attempt to
                //    shell out to $window.history when possible
                // (2) Attempt to figure out some algorthim for reversing that,
                //     so you can also go forward
                // needs testing - I swapped from push to unshift in the push fn
                var prev = this.previous(step || 1);
                return $state.go(prev.state, prev.params);
            },
            previous: function (step) {
                return history[step || 0];
            },
            back: function () {
                return this.go(-1);
            }
        });
    }]).run(['$history', '$state', '$rootScope', function ($history, $state, $rootScope) {
        $rootScope.$on("$stateChangeSuccess", function (event, to, toParams, from, fromParams) {
            if (!from.abstract) {
                $history.push(from, fromParams);
            }
        });
        $history.push($state.current, $state.params);
    }]);
//# sourceMappingURL=ui.router.history.js.map