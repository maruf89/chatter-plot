/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />
(function () {
    'use strict';
    var source = {}, _imageString = '<width>x<height>.jpg', 
    // See end of script for usage in SEO'ifying images
    isHeadless = /\sPrerender\s/.test(window.navigator.userAgent);
    angular.module('maruf89.ResponsiveImage', [])
        .provider('ResponsiveImage', function () {
        this.setResponsiveSource = function (_source) {
            source = _source;
        };
        this.setImageString = function (string) {
            _imageString = string;
        };
        this.$get = [function () {
                return {};
            }];
        return this;
    })
        .directive('responsiveImage', ['$rootScope', function ($rootScope) {
            return {
                restrict: 'EA',
                replace: true,
                scope: {
                    imgSize: '@',
                    imgBase: '@',
                    breakpoints: '@',
                    staticPath: '@',
                    staticType: '@',
                },
                template: '<figure class="responsive-image ri-{{::imgSize}}">' +
                    '<span ng-repeat="bp in ::vars.breakpoints" class="visible-{{::bp}}-block img-{{::bp}}">' +
                    '<span class="non-retina-block">' +
                    '<span class="img" ng-style="::{\'background-image\':\'url(\'+vars.getImageUrl(bp,false)+\')\' }">' +
                    '</span>' +
                    '</span>' +
                    '<span class="retina-block">' +
                    '<span class="img" ng-style="::{\'background-image\':\'url(\'+vars.getImageUrl(bp,true)+\')\' }">' +
                    '</span>' +
                    '</span>' +
                    '</span>' +
                    '</figure>',
                link: {
                    pre: function (scope) {
                        if (!scope.imgBase) {
                            throw new Error('ResponsiveImages requires an imgBase attribute be passed');
                        }
                        scope.vars = {
                            size: source[scope.imgSize],
                            breakpoints: scope.breakpoints ? scope.breakpoints.split(',') : ['xs', 'sm', 'md', 'lg'],
                        };
                    },
                    post: function (scope, iElem) {
                        var vars = scope.vars, isStatic = !!scope.staticPath, 
                        // If there are multiple break points
                        multPoints = vars.breakpoints.length > 1;
                        vars.getImageUrl = function (bp, retina) {
                            return isStatic ? vars.getStatic(bp, retina) : vars.getSize(bp, retina);
                        };
                        /**
                         * !IMPORTANT If there's only 1 bp, then the bp size won't be added to the file name
                         *
                         * @param bp
                         * @param retina
                         * @returns {string}
                         */
                        vars.getStatic = function (bp, retina) {
                            // If there are mulitple break points add path
                            var sizePath = multPoints ? bp + '/' : '', postfix = retina ? '@2x' : '', fileType = '.' + (scope.staticType || 'jpg'), 
                            // compiles to something like 'image@2x.jpg'
                            image = scope.imgBase + postfix + fileType;
                            return scope.staticPath + sizePath + image;
                        };
                        vars.getSize = function (bp, retina) {
                            var postfix = retina ? '@2x' : '', _source = scope.vars.size[bp], width = _source['width' + postfix], height = _source['height' + postfix];
                            return scope.imgBase + _imageString.replace(/<width>/, width).replace(/<height>/, height);
                        };
                        /**
                         * If this instance is being run in a headless browser for SEO
                         * purposes then render the images as actual <img> tags
                         */
                        if (!isStatic && isHeadless) {
                            iElem.empty();
                            vars.img = $('<img />');
                            vars.img[0].src = vars.getImageUrl(vars.breakpoints[vars.breakpoints.length - 1], false);
                            iElem.append(vars.img);
                        }
                    }
                }
            };
        }]);
})();
//# sourceMappingURL=ResponsiveImage.js.map