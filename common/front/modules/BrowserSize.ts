/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var BrowserSize = function ($window):void {
        this._$window = $window;
    },

    screenSizes = {
        'xs': function (width) { return width <= 480; },
        'grey-zone': function (width) { return width > 480 && width < 768; },
        'sm': function (width) { return width >= 768; },
        'md': function (width) { return width >= 992; },
        'lg': function (width) { return width >= 1200; },
    };

BrowserSize.prototype.getSizes = function () {
    var width = Math.max(document.documentElement.clientWidth, this._$window.innerWidth || 0),
        sizes = { current: [], single: null };

    angular.forEach(screenSizes, function (func, key) {
        sizes[key] = func(width);
        if (sizes[key]) {
            sizes.current.push(key);
        }
    });

    sizes.single = sizes.current[sizes.current.length - 1];

    return sizes;
};

angular.module('maruf89.BrowserSize', [])
    .provider('BrowserSize', function () {
        this.$get = ['$window', function ($window) {
            return new BrowserSize($window);
        }];

        return this;
    });