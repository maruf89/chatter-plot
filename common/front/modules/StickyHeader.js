/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
window.requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame;
angular.module('maruf89.StickyHeader', [])
    .directive('stickyHeader', [
    function () {
        return {
            restrict: 'EA',
            replace: false,
            priority: -100,
            scope: {
                scrollBody: '@',
                scrollStop: '@',
                scrollableContainer: '@',
                scrollableClosestContainer: '@',
                scrollOffset: '@'
            },
            link: function (scope, element, attributes) {
                return _.defer(function () {
                    var bottomPin, bottomPinHeader, bottomUnpinHeader, content, determineVisibility, header, offset, pinHeader, pinned, scrollableContainer, throttle, unpinHeader;
                    header = $(element, this);
                    content = $(scope.scrollBody);
                    scrollableContainer = null;
                    scrollableContainer = scope.scrollableClosestContainer ?
                        content.closest(scope.scrollableClosestContainer) :
                        $(scope.scrollableContainer) || $(window);
                    offset = scope.scrollOffset && !isNaN(+scope
                        .scrollOffset) ? +scope.scrollOffset :
                        0;
                    pinned = null;
                    bottomPin = null;
                    scope.scrollStop = isNaN(+scope.scrollStop) ?
                        0 : +scope.scrollStop;
                    pinHeader = function (left) {
                        header.addClass('sticky-pinned');
                        header.removeClass('sticky-unstuck');
                        header[0].style.left = left + "px";
                        return pinned = true;
                    };
                    unpinHeader = function () {
                        header.removeClass('sticky-pinned');
                        header.addClass('sticky-unstuck');
                        return pinned = false;
                    };
                    bottomPinHeader = function () {
                        header.addClass('sticky-bottom');
                        return bottomPin = true;
                    };
                    bottomUnpinHeader = function () {
                        header.removeClass('sticky-bottom');
                        return bottomPin = false;
                    };
                    throttle = function () {
                        return requestAnimationFrame(determineVisibility);
                    };
                    determineVisibility = function () {
                        var bottom, bounds, top;
                        bounds = content[0].getBoundingClientRect();
                        top = bounds.top;
                        bottom = bounds.bottom;
                        if (offset) {
                            top = bounds.top - offset;
                            bottom = bounds.bottom - offset;
                        }
                        if ((top <= 0) && (bottom >= 0)) {
                            if (!pinned) {
                                pinHeader(bounds.left);
                            }
                            if (scope.scrollStop && bottom <=
                                scope.scrollStop) {
                                if (!bottomPin) {
                                    bottomPinHeader();
                                }
                            }
                            else {
                                if (bottomPin) {
                                    bottomUnpinHeader();
                                }
                            }
                        }
                        else {
                            if (pinned) {
                                unpinHeader();
                            }
                            if (bottomPin) {
                                bottomUnpinHeader();
                            }
                        }
                        return true;
                    };
                    return scrollableContainer[0].addEventListener('scroll', throttle);
                    // scrollableContainer.resize(determineVisibility); 
                });
            }
        };
    }
]);
//# sourceMappingURL=StickyHeader.js.map