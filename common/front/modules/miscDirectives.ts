/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />


'use strict';

var hideTitle = function () {
    return {
        restrict: 'CA',
        scope: false,
        compile: function (tElem) {
            if (tElem[0].getAttribute('title') === null) {
                return tElem[0].setAttribute('title', '');
            }
        }
    };
};

angular.module('chatterplot.miscDirectives', [])
    .directive('editLink', [hideTitle])
    .directive('saveLink', [hideTitle])
    .directive('cancelLink', [hideTitle])
    .directive('hideTitle', [hideTitle])
    .directive('goBack', [
        '$window',
        function ($window) {
            return {
                restrict: 'C',
                scope: false,
                link: function (scope, iElem) {
                    return $(document)
                        .off('click.goBack')
                        .on('click.goBack', '.go-back', function () {
                            return $window.history.back();
                        });
                }
            };
        }
    ])
    .directive('eatEventIf', [
        '$parse', '$rootScope',
        function ($parse, $rootScope) {
            return {

                // this ensure eatEventIf be compiled before ngClick 
                priority: 100,
                restrict: 'A',
                compile: function ($element, attr) {
                    var fn, link;
                    fn = $parse(attr.eatEventIf);
                    return {
                        pre: link = function (scope, element) {
                            var eventType;
                            eventType = attr.eventType || 'click';
                            element.on(eventType, function (event) {
                                var callback;
                                callback = function () {
                                    if (fn(scope, {
                                            $event: event
                                        })) {

                                        // prevents ng-click to be executed 
                                        event.stopImmediatePropagation();

                                        // prevents href 
                                        event.preventDefault();
                                        return false;
                                    } else {
                                        return true;
                                    }
                                };
                                if ($rootScope.$$phase) {
                                    scope.$evalAsync(callback);
                                } else {
                                    scope.$apply(callback);
                                }
                            });
                        }
                    };
                }
            };
        }
    ])
    .directive('conditionalAttr', [
        '$parse', '$interpolate',
        function ($parse, $interpolate) {

            /**
             * See README under `Conditional attributes directive` for usage
             */
            return {
                priority: 101,
                restrict: 'A',
                compile: function ($element, attr) {
                    var fn;
                    fn = $parse(attr.conditionalAttr);
                    return {
                        pre: function (scope, iElem) {
                            var attribute, key, ref, res, value;
                            res = fn(scope);
                            key = _.keys(res)[0];
                            if (res[key]) {
                                ref = key.split('|'), attribute = ref[0],
                                    value = ref[1];
                                value = value || res[attribute];
                                if (res.parseValue) {
                                    value = $interpolate(value)(scope);
                                }
                                return iElem[0].setAttribute(attribute,
                                    value);
                            }
                        }
                    };
                }
            };
        }
    ])
    .directive('dotdotdot', [
        '$timeout',
        function ($timeout) {
            return {
                restrict: 'A',
                scope: {
                    dotdotdot: '@',
                    ellipses: '@',
                    after: '@'
                },
                priority: -1,
                link: function (scope, iElem) {
                    if (scope.dotdotdot) {
                        iElem.prepend(scope.dotdotdot);
                    }

                    // Defer the event until all element classes are resolved 
                    return $timeout(function () {
                        var opts;
                        opts = {
                            ellipses: scope.ellipses || '…'
                        };
                        if (scope.after) {
                            opts.after = scope.after;
                        }
                        return iElem.dotdotdot(opts);
                    });
                }
            };
        }
    ])

    /**
     * @description upon being triggered will scroll the element to the bottom
     */
    .directive('scrollBottom', [
        function () {
            return {
                restrict: 'A',
                scope: {
                    scrollBottom: '='
                },
                link: function (scope, iElem) {
                    scope.$watch('scrollBottom', function (post, prev) {
                        if (post !== prev) {
                            setTimeout(function () {
                                iElem.closest('.scroller').scrollTop(iElem[0].scrollHeight + 10);
                            }, 0);
                        }
                    });
                }
            };
        }
    ])
    .directive('autofocus', [
        '$timeout',
        function ($timeout) {
            return {
                restrict: 'AC',
                link: function (scope, iElem) {
                    return $timeout(function () {
                        return iElem[0].focus();
                    }, 0);
                }
            };
        }
    ]);
