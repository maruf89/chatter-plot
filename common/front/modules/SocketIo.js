/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />
/**
 * @module SocketIo
 */
'use strict';
var SocketIoProvider, 
/**
 * Interval length while waiting for the global `io` namespace to become available
 * @type {number}
 */
ioTimer = 50, 
/**
 * Since we have 2 different types of socket connections
 * we will store any extra data about them here
 * @type {object}
 */
namespaces = {
    '/': {
        secure: false,
        emitOnConnect: 'connected'
    },
    '/secure': {
        secure: true,
        emitOnConnect: 'upgradeSocket/connected',
        reconnectError: '/error/reconnect'
    }
}, 
/**
 * Stores any method calls before the socket connection is availabe
 * @type {array}
 */
onReady = [], 
/**
 * Upon socket connection (both secure/insecure) will reattach any listeners
 * that were in the queue for whatever reason and update the socket's current state
 *
 * @type {function}
 * @param  {Socket} socket  socket instance
 */
onConnect = function (socket) {
    socket.removeAllListeners('error');
    socket.removeAllListeners('connect');
    socket.removeAllListeners('reconnect');
    socket.once('connect', function () {
        var namespace = namespaces[this.socket.nsp];
        // If we are already securely connected and we get
        // another socket connect message, then disconnect the new
        // connection, because we only want 1 socket connection per user/tab 
        if (this.inited && namespace.secure) {
            return this.disconnectSocket();
        }
        this.connected = this.socket.connected;
        this.inited = true;
        this.secure = namespace.secure;
        this.isTiming = false;
        // Update the methods now that we are ready to roll
        this.onSock = this._onSock;
        this.emitSock = this._emitSock;
        this.onReady = this._$q.when;
        // Call any methods that may have been called before the socket was ready 
        _.each(onReady, function (func) {
            if (typeof func === 'function') {
                func();
                return;
            }
            var method = _.keys(func)[0];
            this[method].apply(this, func[method]);
        }, this);
        // Clear up any memory 
        onReady.length = 0;
        // Let the world know! 
        this.emit(namespace.emitOnConnect);
        this.emit('/socketConnect');
        return socket.removeAllListeners('error');
    }.bind(this));
    socket.once('error', _.debounce(function (err) {
        // remove connect listener 
        var msg = err.msg || err.message;
        socket.removeAllListeners('connect');
        // We must manually set the expired message 
        if (msg === 'jwt expired') {
            err = {
                response: 400,
                type: 'updates.ERROR-LOGIN_EXPIRED'
            };
        }
        // if token expired or was incorrectly generated/spoofed 
        if (msg === 'incorrectly_generated_token' || msg === 'jwt expired') {
            // broadcast that there was an oauth error 
            this.emit('/error/expiredToken');
        }
        else if (msg === 'user_suspended') {
            this.emit('/disconnect/user', err);
        }
        this.emit('/error/connect', err);
        // connect over non-secure 
        return this.downgradeSock(true);
    }.bind(this), 250));
    /**
     * On disconnect, emit this via socket it
     */
    socket.once('/disconnect/user', function (opts) {
        if (opts == null) {
            opts = {};
        }
        this.emit('/disconnect/user', opts);
        _.defer(function () {
            if (this.socket) {
                return this.socket.disconnect();
            }
        }.bind(this));
    }.bind(this));
    socket.on('reconnect', _.debounce(function () {
        if (!this.secure) {
            return;
        }
        this.reconnect = true;
        return _.each(roomsImIn, function (val, channel) {
            var channelKey;
            channelKey = this._generateChannelKey(channel);
            return this.onEmitSock('/socket/channel/join', {
                channel: channelKey
            });
        }, this);
    }.bind(this), 250));
}, 
/**
 * Stores listening keys so duplicate callbacks aren't called
 * @type {array}
 */
listeningKeys = [], 
/**
 * Will store { <room-name> : [callback1, callback] }
 * to keep track of what we can remove on joining/leaving rooms
 */
roomsImIn = {}, 
/**
 * @class
 * @classdesc handles all socket calls to the server including upgrading/downgrading
 * to authorized handles
 * @requires $q
 */
SocketIo = function (options, $q) {
    this.inited = false;
    this.connected = false;
    this.reconnect = false;
    /**
     * @descrition this variable holds whether either a timeout is set waiting for a socket response
     * (to test if it's taking to long and we should reconnect) or if the connection has not been made yet
     * @type {boolean}
     */
    this.isTiming = true;
    this.secure = false;
    this._$q = $q;
    this._options = options;
    // proxy methods
    this.onReady = this._onReadyPre;
    this.onSock = this._onSockPre;
    this.emitSock = this._emitSockPre;
    // if no timeout fn then set our functions to noops
    if (!options.socketTimeout) {
        this._onSocketTimeout = this._clearSocketTimeout = function () { };
    }
    else {
        this._clearSocketTimeout = this._clearSocketTimeout.bind(this);
    }
    // unique request ID's for requests so concurrent requests to
    // the same endpoints don't result in the same callback for multiple requests 
    this.rID = 0;
};
SocketIo.prototype = _.extend(SocketIo.prototype, require('node-event-emitter').prototype, {
    _onReadyPre: function () {
        var deferred = this._$q.defer();
        onReady.push(deferred.resolve);
        return deferred.promise;
    },
    /**
     * @description a proxy method which gets updated depending on the connectedness of the socket
     */
    onReady: function () { },
    /**
     * Sometimes the page loads and the script ain't ready
     * Calls this function recursively until it's ready
     *
     * @name SocketIo#onInit
     * @param {object} secure - If exists then do secure connection and pass vars to #upgradeSock
     * @param {object} options - Additional options to extend socket with
     */
    onInit: function (secure, options) {
        var opts;
        if (window.io && !this.socket) {
            // Store the credentials in case we need to restart the connection
            this.reconnectOptions = arguments;
            if (secure) {
                return this.upgradeSock(secure);
            }
            else {
                opts = {
                    'secure': true,
                    'sync disconnect on unload': true
                };
                if (options) {
                    _.extend(opts, options);
                }
                this.socket = io.connect('/', opts);
                return onConnect.call(this, this.socket);
            }
        }
        if (!window.io) {
            return setTimeout(_.bind(this.onInit, this, secure), 50);
        }
    },
    /**
     * If on/emit are called before the socket connection has been established
     * we want to save the method + arguments to be called when we're ready
     * Return a promise if no callback passed in
     *
     * @name SocketIo#_onSockPre
     * @param  {string}    eventName  event to listen on
     * @param  {function=} callback
     * @return {Promise=}             If no callback passed, will return a promise
     */
    _onSockPre: function (eventName, callback, rID, persist) {
        var willReturn = null, args = [].slice.call(arguments), deferred;
        if (typeof callback !== 'function') {
            deferred = this._$q.defer();
            args[1] = deferred;
            willReturn = deferred.promise;
        }
        onReady.push({
            '_onSock': args
        });
        return willReturn;
    },
    /**
     * Adds arguments to the list
     *
     * @name SocketIo#_emitSockPre
     * @return {number}  result of pushing to an array
     */
    _emitSockPre: function () {
        return onReady.push({
            '_emitSock': arguments
        });
    },
    /**
     * Upgrades a socket connection from an insecure '/' namespace to a
     * secure one '/secure'
     *
     * @name SocketIo#upgradeSock
     * @param  {object} data
     * @option {string} data.token  Unique JSON Web Token generated by the server
     * @return {boolean}            Wether the socket is upgradable
     */
    upgradeSock: function (data) {
        // Do nothing if connection is already upgraded
        if (this.secure) {
            return false;
        }
        // Trigger a hook
        this.emit('upgradeSocket/init', this.socket);
        _.defer(function () {
            this.inited = false;
            if (this.socket) {
                this.disconnectSocket();
            }
            this.socket = io.connect('/secure', {
                'query': "token=" + data.token,
                'forceNew': true,
                'secure': true,
                'sync disconnect on unload': true
            });
            return onConnect.call(this, this.socket);
        }.bind(this));
        return true;
    },
    /**
     * Downgrades the socket connection from secure to unsecure
     *
     * @name SocketIo#downgradeSock
     * @return {boolean}            Whether the action was successfull
     */
    downgradeSock: function (force) {
        if (!(this.secure || force)) {
            return false;
        }
        this.emit('downgradeSocket/init', this.socket);
        _.defer(function () {
            this.inited = this.secure = false;
            if (this.socket) {
                this.socket.disconnect();
            }
            this.socket = null;
            this.onInit(null, {
                'forceNew': true
            });
        }.bind(this));
        return true;
    },
    /**
     * @description Completely removes any traces of being connected to anything
     */
    disconnectSocket: function () {
        this.socket && this.socket.disconnect();
        this.socket = this.secure = null;
        this.onSock = this._onSockPre;
        this.emitSock = this._emitSockPre;
        this.onReady = this._$q.when;
    },
    /**
     * @description To be called when the client loses the connect with the server even though
     * the socket object still claims to be connected and `socket.on('disconnect'` doesn't get called
     * @returns {Promise}
     */
    forceReconnect: function () {
        var deferred = this._$q.defer(), 
        // {arguments} with up to a length of 2
        options = this.reconnectOptions;
        this.on('/socketConnect', deferred.resolve);
        this.disconnectSocket();
        _.defer(this.onInit.bind(this, options[0], options[1]));
        return deferred.promise;
    },
    /**
     * listen for `eventName` from the server
     *
     * @private
     * @name SocketIo#_onSock
     * @param  {string}              eventName  name of event to listen to the server for
     * @param  {Function=|Deferred=} callback   Either a function, or a deferred object
     *                                          if called from on ready callback
     *
     *                                          OR if persist, then an unbind method
     */
    _onSock: function (eventName, callback, rID, persist) {
        var deferred = null, willReturn = null, isDeferred = false, listener;
        // We don't want to double bind an already listening event
        if (persist) {
            if (_.indexOf(listeningKeys, eventName) > -1) {
                if (isDeferred) {
                    deferred.reject('Key already Exists in SocketIo#onSock');
                }
            }
            listeningKeys.push(eventName);
        }
        listener = function (res) {
            // Check whether there's a request ID attached
            if (rID && _.isArray(res) && typeof res[2] === 'number') {
                // if the request ID's don't match then skip
                if (rID !== res[2]) {
                    return false;
                }
            }
            // Remove the listener if not persistent
            if (!persist) {
                this._removeListener(eventName, listener);
            }
            // Apply callback if applies
            if (callback) {
                callback(eventName, res);
            }
            // Resolve if is deferred
            if (isDeferred) {
                return deferred.resolve(res);
            }
        }.bind(this);
        this.socket.on(eventName, listener);
        if (persist) {
            willReturn = this._removeListener.bind(this, eventName, listener);
        }
        else if (isDeferred = callback &&
            typeof callback !== 'function' &&
            callback.resolve) {
            deferred = callback;
            callback = null;
        }
        else if (!callback) {
            deferred = this._$q.defer();
            willReturn = deferred.promise;
            isDeferred = true;
        }
        return willReturn;
    },
    _generateChannelKey: function (channel) {
        return 'cl:' + channel;
    },
    /**
     * Joins a room
     *
     * @name SocketIo#join
     * @param  {string} channel - a socket channel
     * @param  {function} callback
     * @param  {boolean} noEmit - If true the callback object will not have a working emit method
     * @return {object} handle
     * @return {function} handle.emit - method to notify other channel recipients (only if noEmit = falsey)
     * @return {function} handle.remove - a function to stop listening to channel updates
     * @return {string} handle.channel
     */
    join: function (channel, callback, noEmit) {
        if (typeof callback !== 'function') {
            throw new Error('callback is required for SocketIo#join');
        }
        var channelKey = this._generateChannelKey(channel), emitFn = noEmit ? function () { } : this.socket.emit.bind(this.socket, channelKey);
        // if we don't have the channel in the `roomsImIn` it means we aren't listening and must create
        if (!_.isArray(roomsImIn[channel])) {
            this.socket.on(channelKey, this._channelCallback.bind(this, channel));
            roomsImIn[channel] = [];
        }
        roomsImIn[channel].push(callback);
        this.onEmitSock('/socket/channel/join', {
            channel: channelKey
        });
        // Pass a way to emit and to remove this fn callback
        return {
            emit: emitFn,
            remove: this.leave.bind(this, channel, callback),
            channel: channelKey
        };
    },
    /**
     * Leaves a room
     * If called with no callback, will remove all callbacks for that
     * Also if no more callbacks exist, will send a request to remove the listener from the backend
     *
     * @name SocketIo#leave
     * @param  {string} channel - a socket channel
     * @param  {function} callback
     */
    leave: function (channel, callback) {
        var index, channelKey = this._generateChannelKey(channel);
        // If no callback (remove all), or there is only 1 callback remaining,
        // remove the listener and remove the reference in `roomsImIn`
        if (!callback || roomsImIn[channel].length <= 1) {
            this.socket.removeAllListeners(channelKey);
            delete roomsImIn[channel];
            return this.onEmitSock('/socket/channel/leave', {
                channel: channelKey
            });
        }
        // Otherwise just remove the callback from our reference
        if (~(index = _.indexOf(roomsImIn[channel], callback))) {
            roomsImIn[channel].splice(index, 1);
        }
        return this._$q.when();
    },
    _channelCallback: function (channel, args) {
        _.each(roomsImIn[channel], function (fn) {
            return fn(args, channel);
        });
    },
    /**
     * Emit the event name + data to the server
     *
     * @private
     * @name SocketIo#_emitSock
     * @param  {string}        eventName  event to emit to the server
     * @param  {String|Object} data       data to pass to the server
     */
    _emitSock: function (eventName, data) {
        return this.socket.emit(eventName, data);
    },
    /**
     * Utility method which combines Socket#_emitSock & Socket#_onSock and returns a promise
     *
     * @example
     *     # Client Side
     *     ->
     *         onEmitSock('myEvent', dataToServer, false).then (response) ->
     *             console.log(response)
     *
     *     # Server Side
     *     Socket.on 'myEvent>', (dataToServer) ->
     *         # ... do something with data
     *         socket.emit('myEvent<', dataToClient)
     *
     * @name SocketIo#onEmitSock
     * @param  {string}  eventName  Base event name. `>` will be added to `emit` and `<` will be added to `on`
     * @param  {Data}    data       Any data to pass to the server
     * @param  {boolean=} rID - if passed will include a key to identify this request
     * @return {Promise<*>}
     */
    onEmitSock: function (eventName, data, rID) {
        var deferred = this._$q.defer(), emitEvent = eventName + ">", onEvent = eventName + "<", 
        // in case of timeout
        timeout = this._onSocketTimeout(arguments, deferred);
        if (rID && typeof data === 'object') {
            rID = data.__rID = ++this.rID;
        }
        /**
         * !IMPORTANT! - if the response returned by the server is an Array, it will assume
         * that the first argument is an error, and second is the success response
         */
        this.onSock(onEvent, null, rID).then(function (response) {
            var error, result;
            if (_.isArray(response)) {
                error = response[0];
                result = response[1];
                if (error) {
                    return deferred.reject(error);
                }
                else {
                    return deferred.resolve(result);
                }
            }
            else {
                return deferred.resolve(response);
            }
        }).catch(function (err) {
            if (CP.Settings.isDev) {
                console.log(err);
            }
            return deferred.reject(err);
        }).finally(_.partial(this._clearSocketTimeout, timeout));
        this.emitSock(emitEvent, data);
        return deferred.promise;
    },
    /**
     * @description Returns a timeout that if not cleared will disconnect & trigger a complete socket reconnection
     * @param {(arguments|array)} args - the arguments to proxy to #onEmitSock in case of timeout
     * @param {Deferred} deferred
     * @returns {Object|number|NodeJS.Timer}
     * @private
     */
    _onSocketTimeout: function (args, deferred) {
        var self = this;
        // Don't run duplicate timeout requests
        // TODO: currently this throws away what to run on reconnection - store these arguments to be run on reconnect
        if (this.isTiming) {
            return null;
        }
        this.isTiming = true;
        return setTimeout(function () {
            self.forceReconnect().then(function () {
                self.onEmitSock(args[0], args[1], args[2]).then(deferred.resolve).catch(deferred.reject);
            });
        }, this._options.socketTimeoutReconnect);
    },
    _clearSocketTimeout: function (timer) {
        if (typeof timer === 'number') {
            clearInterval(timer);
            this.isTiming = false;
        }
    },
    /**
     * Proxy method to remove a eventname + listener
     *
     * @private
     * @name SocketIo#_removeListener
     * @param  {string}   eventName
     * @param  {Function} listener
     */
    _removeListener: function (eventName, listener) {
        return this.socket.removeListener(eventName, listener);
    },
    /**
     * Proxy method to remove all listeners for a single event name
     *
     * @private
     * @name SocketIo#_removeAllListeners
     * @param  {string} eventName
     */
    _removeAllListeners: function (eventName) {
        return this.socket.removeAllListeners(eventName);
    }
});
angular.module('maruf89.SocketIo', ['btford.socket-io'])
    .provider('SocketIo', SocketIoProvider = function () {
    var options = {
        socketTimeoutReconnect: 4000,
        socketTimeout: true
    };
    this.configureNamespace = function (options) {
        return angular.extend(namespaces, options);
    };
    /**
     * @description either sets the timeout length or disables it if val is falsey
     * @param {number=} val - milliseconds
     */
    this.setTimeout = function (val) {
        if (!val) {
            return options.socketTimeout = false;
        }
        options.socketTimeoutReconnect = val;
    };
    this.$get = ['$q', function ($q) {
            return new SocketIo(options, $q);
        }];
    return this;
});
//# sourceMappingURL=SocketIo.js.map