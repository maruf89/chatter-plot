/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var _defaultGetFields, eventEmitter;

_defaultGetFields = ['email', 'id'];

eventEmitter = require('node-event-emitter');

angular.module('FacebookProvider', [])
    .factory('Facebook', [
        '$q',
        function ($q) {
            var Facebook;
            Facebook = function () {
                return this.state = null;
            };
            Facebook.prototype = angular.extend(Facebook.prototype, eventEmitter
                .prototype, {
                    setState: function (response) {
                        return this.state = response;
                    },
                    getSelf: function (fields) {
                        if (fields == null) {
                            fields = _defaultGetFields;
                        }
                        if (!this.state || !this.state.authResponse) {
                            return $q.reject();
                        }
                        return this.request('me', {
                            fields: fields,
                            access_token: this.state.authResponse
                                .accessToken
                        });
                    },
                    getLoginStatus: function (force) {
                        var deferred;
                        deferred = $q.defer();
                        FB.getLoginStatus(function (response) {
                            return deferred.resolve(response);
                        }, force);
                        return deferred.promise;
                    },
                    login: function () {
                        var self;
                        self = this;
                        FB.getLoginStatus((function (response) {
                            switch (response.status) {
                                case 'connected':
                                    return self.emit(
                                        'fbConnected',
                                        response.authResponse
                                    );
                                case 'not_authorized':
                                case 'unknown':
                                    return FB.login((
                                        function (
                                            response
                                        ) {
                                            if (
                                                response
                                                .authResponse
                                            ) {
                                                self
                                                    .emit(
                                                        'fbConnected', {
                                                            response: response
                                                                .authResponse,
                                                            userNotAuthorized: true
                                                        }
                                                    );
                                            } else {
                                                self
                                                    .emit(
                                                        'fbLoginFailed'
                                                    );
                                            }
                                        }), {
                                        scope: 'read_stream, publish_stream, email'
                                    });
                                default:
                                    return FB.login(function (
                                        response) {
                                        if (response
                                            .authResponse
                                        ) {
                                            self.emit(
                                                'fbConnected', {
                                                    response: response
                                                        .authResponse
                                                }
                                            );
                                            self.emit(
                                                'fbGetLoginStatus'
                                            );
                                        } else {
                                            self.emit(
                                                'fbLoginFailed'
                                            );
                                        }
                                    });
                            }
                        }), true);
                    },
                    logout: function () {
                        var self;
                        self = this;
                        FB.logout(function (response) {
                            if (response) {
                                self.emit('fbLogoutSucceeded');
                            } else {
                                self.emit('fbLogoutFailed');
                            }
                        });
                    },
                    request: function () {
                        var args, callback, deferred;
                        deferred = $q.defer();
                        callback = function (response) {
                            if (!response || response.error) {
                                return deferred.reject(response);
                            }
                            return deferred.resolve(response);
                        };
                        args = [].slice.call(arguments);
                        args.push(callback);
                        FB.api.apply(FB, args);
                        return deferred.promise;
                    },
                    unsubscribe: function () {
                        var self = this;
                        FB.api('/me/permissions', 'DELETE', function (response) {
                            self.emit('fbGetLoginStatus');
                        });
                    }
                });
            return new Facebook();
        }
    ]);
