/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
var commonGeneral = require('common/util/general'), _ = require('lodash-node/modern'), googleSearch, serviceMap = {
    geocode: function () { return new google.maps.Geocoder(); },
    places: function () { return new google.maps.places.AutocompleteService(); }
};
exports.setDefault = function (scope, Util) {
    if (_.isPlainObject(scope.source || scope.displaySrc)) {
        scope.vars.defaultValue = Util.geo.format.address(scope.source || scope.displaySrc, [
            'name',
            'address',
            'sublocality',
            'city',
            'state',
            'country'
        ], ', ', scope.zoomLevel);
    }
};
/**
 * @description Checks over the specificity types associated with a location
 * and returns true if the location is lacking any of those types
 * @param {object} place - a google places object
 * @returns {boolean}
 * @private
 */
exports.isLocationVague = function (place) {
    if (!place.types) {
        throw new Error('Google place does not have a `types` value');
    }
    var requires = [
        'street_address',
        'establishment',
        'point_of_interest',
        'route',
    ];
    return _.every(requires, function (type) {
        return place.types.indexOf(type) === -1;
    });
};
exports.getCurrentLocation = function (Util) {
    return Util.geo.getMyLocation()
        .then(Util.geo.latlng2Address)
        .then(function (places) {
        if (!places[0]) {
            throw null;
        }
        return places[0];
    });
};
/**
 * @description holds an array of vars objects which will get updated on sync change
 * @type {array}
 * @private
 */
exports.syncVars = [];
exports.initSync = function (syncKey, vars, $root) {
    var syncObj;
    if (syncKey) {
        if (!$root._data[syncKey]) {
            $root._data[syncKey] = {
                callback: exports.onSyncCallback
            };
        }
        exports.syncVars.push(vars);
        vars.syncObj = syncObj = $root._data[syncKey];
        if (syncObj && syncObj.defaultValue) {
            vars.defaultValue = syncObj.defaultValue;
        }
    }
};
/**
 * @description on sync change, iterates over the array of vars objects and calls a callback
 * for all the other instances
 *
 * @param GMap - the this object called from the location selected callback
 * @param vars - the scope.vars of the triggerer
 * @private
 */
exports.onSyncCallback = function (GMap, vars) {
    _.each(exports.syncVars, function (varsObj) {
        if (vars !== varsObj) {
            varsObj.locationSelected.call(GMap, true);
        }
    });
};
exports.locationSelected = function (location, vars, isSync) {
    var syncObj = vars.syncObj;
    if (syncObj) {
        // Check if this is a synchronized instance
        if (isSync) {
            // if this is triggered by another instance, update the value
            vars.defaultValue = syncObj.defaultValue;
        }
        else {
            // Otherwise trigger the other instances
            syncObj.defaultValue = location.name;
            syncObj.callback(location, vars);
        }
    }
};
/**
 * @description removes a vars object from _syncVars
 * @param {object} vars
 * @private
 */
exports.removeSync = function (vars) {
    commonGeneral.array.remove(exports.syncVars, vars);
};
googleSearch = function (service, method, type, opts, callback) {
    var search = opts || {}, instance = serviceMap[service]();
    search[type] = null;
    return function (query, deferred) {
        if (!query) {
            return deferred && deferred.reject();
        }
        var _callback = deferred && deferred.resolve || callback;
        search[type] = query;
        instance[method](search, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                return _callback(results);
            }
            if (deferred) {
                deferred.reject();
            }
            console.log(service, 'was not successful for the following reason:', status);
        });
    };
};
exports.geocode = _.partial(googleSearch, 'geocode', 'geocode');
exports.places = _.partial(googleSearch, 'places', 'getQueryPredictions');
//# sourceMappingURL=googleLocation.js.map