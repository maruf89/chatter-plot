/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
/**
 * @description Iterates through an array of array of objects and adds even/odd
 * classes to each element
 *
 * @param  {array} values - Sorted array of groups containing arrays of events
 * @param  {number} length - The number of total events
 */
exports.alternateGroupsArray = function (values, length) {
    var item, group, i = 0, curGroup = 0, curIndex = 0;
    for (; i < length;) {
        group = values[curGroup];
        if (!(item = group[curIndex])) {
            group = values[++curGroup];
            item = group[curIndex = 0];
        }
        item.alternate = i++ % 2 ? 'even' : 'odd';
        curIndex++;
    }
};
//# sourceMappingURL=list.js.map