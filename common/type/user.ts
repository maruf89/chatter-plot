/// <reference path="../../typings/chatterplot/Chatterplot.d.ts" />

"use strict";

var _:_.LoDashStatic = require('lodash-node/modern');

exports.insecureFields = [
    'userID',
    'birthday',
    'details',
    'firstName',
    'lastName',
    'picture',
    'joinDate',
    'availabilityText',
    'follows',
    'languages',
    'personal',
    'interests',
    'locations',
    'settings.notifs.messaging',
    'settings.requests',
];

exports.combineUserLocations = function (locsA, locsB) {
    locsA = locsA || [];
    locsB = locsB || [];

    var idMap:any = {};

    /**
     * After combining the locations, make sure there are no duplicates
     */
    return _.reduce(locsA.concat(locsB), function (arr, location:cp.user.ILocation) {
        var ID:string = location.google || location.yelp;
        if (ID && !idMap[ID]) {
            idMap[ID] = true;
            arr.push(location);
        }
        return arr;
    }, []);
};
