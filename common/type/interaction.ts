/// <reference path="../../typings/chatterplot/Chatterplot.d.ts" />

"use strict";

var controller = exports;

exports.generateID = function (participants, prefix) {
    return (prefix ? prefix + '-' : '') + participants.sort().join('_');
};

/*
 * @description Eats a request ID and returns the relevant users from that
 *
 * @param {string} requestID - looks something like 624_719 or 1029-12_125
 *                              (1029 is the request ID - 12 & 125 are the user ID's)
 * @param {number=} exclude - a single userID to exclude (used to exclude the sender)
 * @return {array<number>}
 */
exports.getParticipants = function (requestID, exclude):number[] {
    return requestID.replace(/^\d+-/, '').split('_').reduce(function (arr, userID) {
        var ID = parseInt(userID, 10);
        ID !== exclude && arr.push(ID);
        return arr;
    }, []);
};

exports.getIndexType = function (requestID):string {
    return /^\d+-/.test(requestID) ? 'REQUEST' : 'MESSAGE';
};

exports.isParticipant = function (requestID, userID):boolean {
    var participants = controller.getParticipants(requestID);

    return participants.indexOf(userID) !== -1;
};

/**
 * @description compares 2 request objects by the
 * @param reqA
 * @param reqB
 * @returns {boolean}
 */
exports.requestsDiffer = function (reqA:cp.interaction.IRequest, reqB:cp.interaction.IRequest):boolean {
    var locA:cp.interaction.ILocation = reqA.venue,
        locB:cp.interaction.ILocation = reqB.venue,
        haveVenues:boolean = !!(locA && locB),
        hasVenue:boolean = !!(locA || locB);

    if ((haveVenues !== hasVenue) || locA.vID !== locB.vID) {
        return true;
    }

    return reqA.when !== reqB.when || reqA.type !== reqB.type;
};
