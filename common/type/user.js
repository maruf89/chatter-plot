/// <reference path="../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
var _ = require('lodash-node/modern');
exports.insecureFields = [
    'userID',
    'birthday',
    'details',
    'firstName',
    'lastName',
    'picture',
    'joinDate',
    'availabilityText',
    'follows',
    'languages',
    'personal',
    'interests',
    'locations',
    'settings.notifs.messaging',
    'settings.requests',
];
exports.combineUserLocations = function (locsA, locsB) {
    locsA = locsA || [];
    locsB = locsB || [];
    var idMap = {};
    /**
     * After combining the locations, make sure there are no duplicates
     */
    return _.reduce(locsA.concat(locsB), function (arr, location) {
        var ID = location.google || location.yelp;
        if (ID && !idMap[ID]) {
            idMap[ID] = true;
            arr.push(location);
        }
        return arr;
    }, []);
};
//# sourceMappingURL=user.js.map