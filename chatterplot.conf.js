// Karma configuration
// Generated on Sun Nov 16 2014 00:23:06 GMT+0200 (EET)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
        'app/third_partty/socket.io-1.1.0.js',
        'app/third_party/jquery-2.1.1.min.js',
        'https://code.angularjs.org/1.3.1/angular.min.js',
        'https://code.angularjs.org/1.3.1/angular-mocks.js',
        'https://code.angularjs.org/1.3.1/i18n/angular-locale_en-EN.js',
        'https://code.angularjs.org/1.3.1/angular-animate.min.js',
        'https://code.angularjs.org/1.3.1/angular-messages.min.js',
        'app/third_party/moment-2.8.3.js',
        'app/third_party/angulartics/dist/angulartics.min.js',
        'app/third_party/angulartics/dist/angulartics-ga.min.js',
        'app/bower_components/angular-local-storage/angular-local-storage.js',
        'app/bower_components/angular-ui-router/release/angular-ui-router.js',
        'app/bower_components/angular-ui-router.stateHelper/statehelper.min.js',
        'app/bower_components/angular-modal-modified/modal.js',
        'app/bower_components/angular-socket-io/socket.js',
        'app/bower_components/angular-localization/angular-localization.js',
        'app/bower_components/flow.js/dist/flow.js',
        'app/bower_components/angular-cookies/angular-cookies.min.js',
        'app/bower_components/angular-sanitize/angular-sanitize.min.js',
        'app/bower_components/underscore/underscore.js',
        'app/third_party/angular.datepicker.js',

        'test/mocks/*.js',

        'app/scripts/plugins.js',
        'app/scripts/bundle.js',

        'test/spec/**/*.js'
    ],
    
    // files: [
    //     'app/bower_components/angular/angular.js',
    //     'app/bower_components/angular-mocks/angular-mocks.js',
    //     'app/third_party/jquery-2.1.1.min.js',
    //     'test/mocks/init-setup.js',

    //     'test/spec/**/*.js'
    // ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
        // 'test/**/*.js': [ 'browserify' ]
    },

    browserify: {
        debug: true,
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_DEBUG,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'], // , 'Chrome', 'ChromeCanary', 'Firefox', 'Safari', 'Opera'


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false
  });
};
