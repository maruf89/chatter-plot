/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var atlasText = require('root/landing/ABText/atlas');
module.exports = function (routes) {
    var map = routes.routes;
    return {
        name: 'landing',
        abstract: true,
        url: '',
        views: {
            "site": {
                templateProvider: ['$templateCache', function ($templateCache) {
                        return $templateCache.get('views/landing/Template.html');
                    }],
                controller: 'LandingCtrl as Landing',
            }
        },
        children: [{
                name: 'default',
                url: '/',
                views: {
                    "landing": {
                        template: '<div></div>',
                    }
                },
            }, {
                name: 'atlas',
                url: '/atlas-of-language-speakers',
                views: {
                    "landing": {
                        template: '<div></div>',
                        controller: ['$scope', function ($scope) {
                                $scope = $scope.$parent;
                                $scope.AB = $scope.AB || { text: null };
                                $scope.AB.text = atlasText;
                                $scope.AB.hideSearch = true;
                            }]
                    }
                },
            }]
    };
};
//# sourceMappingURL=landingRoute.js.map