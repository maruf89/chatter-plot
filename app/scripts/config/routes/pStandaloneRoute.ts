/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

"use strict";

module.exports = function (routes) {
    var map = routes.routes,
        path = routes.path;

    return {
        name: 'pStandalone',
        url: '',
        abstract: true,
        views: {
            "site": {
                template: '<div class="Standalone" ui-view="standalone" autoscroll="true"></div>',
            }
        },
        children: [{
            name: 'messages',
            url: '/messages',
            roles: ['SELF_VIEW'],
            views: {
                "standalone@pStandalone": {
                    templateProvider: ['$templateCache', function ($templateCache) {
                        return $templateCache.get('views/interaction/message/list/Template.html');
                    }],
                    controller: 'MessagingListCtrl as Interaction',
                }
            },
            children: [{
                name: 'single',
                url: '/:msgID',
                views: {
                    "messageList": {
                        controller: 'MessageSingleCtrl as Single',
                        templateProvider: ['$templateCache', function ($templateCache) {
                            return $templateCache.get('views/interaction/message/single/Template.html');
                        }],
                    }
                },
                resolve: {
                    Conversation: [
                        '$stateParams', 'Interaction', 'User', '$state',
                        function ($stateParams, Messaging, User, $state) {
                            var users = Messaging.usersFromID($stateParams.msgID, true),
                                withUsers = !_.some(users, function (
                                    userID) {
                                    var user = User.users[userID];
                                    return user && (user.picture || user.details);
                                });

                            return Messaging.get({
                                msgID: $stateParams.msgID,
                                conversation: true,
                                messages: true,
                                withUsers: withUsers
                            }).catch(function () {
                                throw $state.go('pStandalone.messages');
                            });
                        }
                    ]
                }
            }]
        }]
    }
};
