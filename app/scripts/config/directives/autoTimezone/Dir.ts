/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var moment = require('moment');

/**
 * autoTimezone Directive
 *
 * Gives the user the option to update the timezone to match the current timezone
 * (Uses Moment.js to detect the timezone + DST)
 *
 * @param User
 * @param Util
 * @param Config
 * @param locale
 * @param DataBus
 * @param locStorage
 * @returns {{restrict: string, scope: boolean, link: Function}}
 */

var AutoTimezoneDirective = function (
    User:cp.user.IService,
    Util:any,
    Config:any,
    locale:any,
    DataBus:any,
    locStorage:any
) {
    var locStorageKey:string = 'autoTimezone:ignore',
        hide = DataBus.emit.bind(DataBus, '/nav/subheader/change'),

        disabled:boolean = false;

    return {
        restrict: 'E',
        scope:    true,
        replace: true,
        templateUrl: 'views/config/directives/autoTimezone/Template.html',
        link: {
            pre: function (scope, iElem) {
                scope.vars = {
                    doUpdate: null,
                    tzValue: null,
                    settingsState: Config.routes.settings,
                };

                if (locStorage.get(locStorageKey)) {
                    disabled = true;
                    iElem.remove();
                    return hide();
                }

                locale.ready('settings');

                var now:moment.Moment = moment(),
                    isDST:boolean = now.isDST(),
                    offset:number = now.utcOffset(),
                    withoutDST:number = isDST ? offset - 60 : offset,

                    modifier:string = withoutDST < 0 ? '-' : '+',
                    displayNum:string = Util.pad(withoutDST ? Math.abs(withoutDST / 60) : '00', 2),

                    value:string = `${modifier}${displayNum}:00`;

                scope.vars.tzValue = withoutDST;

                locale.ready('settings').then(function () {
                    // Set the timezone text value
                    iElem.children('.tz-value').text(
                        locale.getString('settings.UPDATE_TZ', { time_zone: value })
                    );
                });
            },
            post: function (scope) {
                if (disabled) {
                    return;
                }

                var vars = scope.vars;

                vars.doAction = function () {
                    Config.updateSettings({
                        timeZone: vars.tzValue,
                    }).then(vars.hide);
                };

                vars.hide = function (remember) {
                    hide();

                    if (remember) {
                        locStorage.set(locStorageKey, true);
                    }
                };
            }
        }
    };
};

require('../../../app').directive('autoTimezone', [
    'User',
    'Util',
    'Config',
    'locale',
    'DataBus',
    'localStorageService',
    AutoTimezoneDirective
]);
