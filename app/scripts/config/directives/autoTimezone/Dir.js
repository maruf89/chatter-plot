/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var moment = require('moment');
/**
 * autoTimezone Directive
 *
 * Gives the user the option to update the timezone to match the current timezone
 * (Uses Moment.js to detect the timezone + DST)
 *
 * @param User
 * @param Util
 * @param Config
 * @param locale
 * @param DataBus
 * @param locStorage
 * @returns {{restrict: string, scope: boolean, link: Function}}
 */
var AutoTimezoneDirective = function (User, Util, Config, locale, DataBus, locStorage) {
    var locStorageKey = 'autoTimezone:ignore', hide = DataBus.emit.bind(DataBus, '/nav/subheader/change'), disabled = false;
    return {
        restrict: 'E',
        scope: true,
        replace: true,
        templateUrl: 'views/config/directives/autoTimezone/Template.html',
        link: {
            pre: function (scope, iElem) {
                scope.vars = {
                    doUpdate: null,
                    tzValue: null,
                    settingsState: Config.routes.settings,
                };
                if (locStorage.get(locStorageKey)) {
                    disabled = true;
                    iElem.remove();
                    return hide();
                }
                locale.ready('settings');
                var now = moment(), isDST = now.isDST(), offset = now.utcOffset(), withoutDST = isDST ? offset - 60 : offset, modifier = withoutDST < 0 ? '-' : '+', displayNum = Util.pad(withoutDST ? Math.abs(withoutDST / 60) : '00', 2), value = "" + modifier + displayNum + ":00";
                scope.vars.tzValue = withoutDST;
                locale.ready('settings').then(function () {
                    // Set the timezone text value
                    iElem.children('.tz-value').text(locale.getString('settings.UPDATE_TZ', { time_zone: value }));
                });
            },
            post: function (scope) {
                if (disabled) {
                    return;
                }
                var vars = scope.vars;
                vars.doAction = function () {
                    Config.updateSettings({
                        timeZone: vars.tzValue,
                    }).then(vars.hide);
                };
                vars.hide = function (remember) {
                    hide();
                    if (remember) {
                        locStorage.set(locStorageKey, true);
                    }
                };
            }
        }
    };
};
require('../../../app').directive('autoTimezone', [
    'User',
    'Util',
    'Config',
    'locale',
    'DataBus',
    'localStorageService',
    AutoTimezoneDirective
]);
//# sourceMappingURL=Dir.js.map