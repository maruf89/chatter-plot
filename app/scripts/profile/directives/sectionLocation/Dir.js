/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../../app.js').directive('sectionLocation', [
    'Venues', 'Util', 'Maps', 'API', '$q', 'Config', 'uiGmapGoogleMapApi',
    '$analytics', '$templateCache', 'DataBus',
    function (Venues, Util, Maps, API, $q, Config, GoogleMapApi, $analytics, $templateCache, DataBus) {
        /**
         * @description Updates the current locations object
         * @param  {object} vars       contains reference to the temp + current locations
         * @param  {Array} locations  The new locations to bring in
         * @param  {boolean} clear - whether to remove the current markers
         * @param  {boolean} prefix - whether to prefix the marker ID with something unique
         */
        var generateMarkers = function (vars, locations, clear) {
            var instance = Maps.getInstance();
            if (clear && instance) {
                instance.clearMarkers();
            }
            var premarkers = _.map(locations, function (venue, index) {
                var user = venue.user || venue, venue = venue.venue || venue, iconType = user.primary ? 'home' : 'away';
                return {
                    lat: venue.coords.lat,
                    lon: venue.coords.lon,
                    icon: Config.map.icon[iconType],
                    id: "vID-" + (venue.vID || index) + iconType
                };
            });
            // Format them into map ready markers
            instance.createMarkers(premarkers);
            instance.loaded().then(function () {
                _.defer(function () {
                    instance.center();
                });
            });
        };
        return {
            restrict: 'E',
            replace: true,
            template: $templateCache.get('views/profile/directives/sectionLocation/Template.html'),
            scope: {
                onSave: '&',
                locations: '=',
                defaultOpen: '=?',
                isMe: '=?',
            },
            link: {
                pre: function (scope, iElem, iAttrs) {
                    var vars;
                    scope.vars = vars = {
                        doEmpty: 0,
                        gotLocations: [],
                        stillLoading: true,
                        getMainLocTT: function (isPrimary) {
                            return 'activities.' + (isPrimary ? 'MAIN_LOC' : 'SET_MAIN_LOC');
                        },
                        mergeLocations: function (fetchedLocations) {
                            scope.vars.gotLocations = _.map(scope.locations, function (location, index) {
                                var obj = {
                                    venue: Venues.scrapePlace(fetchedLocations[index]),
                                    user: location,
                                    service: fetchedLocations[index]
                                };
                                if (location.vID) {
                                    obj.venue.vID = location.vID;
                                }
                                return obj;
                            });
                            generateMarkers(vars, vars.gotLocations, true);
                        },
                        updateLocs: function (locations) {
                            vars.stillLoading = true;
                            if (_.isArray(locations) && locations.length) {
                                $q.all(_.map(scope.locations, Venues.getPlace, Venues))
                                    .then(scope.vars.mergeLocations)
                                    .finally(function () {
                                    vars.stillLoading = false;
                                    scope.$root.safeDigest(scope);
                                });
                            }
                            else {
                                scope.vars.gotLocations = [];
                                vars.stillLoading = false;
                            }
                        }
                    };
                    vars.updateLocs(scope.locations);
                    //// If we ever need to externally watch for user location
                    //// updates externally then uncomment this
                    //
                    //scope.$watch('locations', function (post, prev) {
                    //    if (post !== prev) {
                    //        scope.vars.updateLocs(post);
                    //    }
                    //}, true);
                },
                post: function (scope, iElem, iAttrs) {
                    var instance = instance || Maps.getInstance(), vars = scope.vars;
                    vars.showLocation = function (location) {
                        return location && instance.centerMap(location.coords);
                    };
                    if (!iAttrs.onSave) {
                        return;
                    }
                    vars.removeVenue = function (venue) {
                        _.each(vars.gotLocations, function (location, index) {
                            if (location === venue) {
                                vars.gotLocations.splice(index, 1);
                                // If the location we removed was the primary location, set a default
                                if (location.user.primary && vars.gotLocations.length) {
                                    vars.setPrimary(vars.gotLocations[0], true);
                                }
                                return false;
                            }
                        });
                        $analytics.eventTrack('click', {
                            category: 'profile',
                            label: 'locations: removed a location'
                        });
                        return vars.save();
                    };
                    /**
                     * When a location is autocompleted
                     * @param {object} place
                     * @param {string} service
                     */
                    vars.locationSet = function (place, service) {
                        // Check if we already have the exact same venue
                        if (!_.every(vars.gotLocations, function (loc) {
                            return loc.service.place_id !== place.place_id;
                        })) {
                            return;
                        }
                        var scraped = Venues.scrapePlace(place, service), location = {
                            service: place,
                            venue: scraped,
                            user: scraped,
                        };
                        // if it's the first location - set as primary
                        if (vars.gotLocations.push(location) === 1) {
                            location.user.primary = true;
                        }
                        // trigger the googleLocation directive to empty itself
                        vars.doEmpty++;
                        $analytics.eventTrack('click', {
                            category: 'profile',
                            label: 'locations: added a location'
                        });
                        // automatically save
                        vars.save();
                    };
                    /**
                     * @description sets the passed in venue object as the primary & can update the venues
                     * @param {object} venueObj - the venue to promote to primary
                     * @param {boolean} dontSave - if true will not trigger vars.save
                     */
                    vars.setPrimary = function (venueObj, dontSave) {
                        var user = venueObj.user, index = null;
                        vars.primaryID = user.google || user.yelp;
                        _.each(vars.gotLocations, function (_venueObj, _index) {
                            if (_venueObj.user.primary = user === _venueObj.user) {
                                // This must be set because it's not copied over originally
                                // If the coords don't exist, the primary won't save
                                user.coords = venueObj.venue.coords;
                                user.sublocality = venueObj.venue.sublocality;
                                user.city = venueObj.venue.city;
                                user.state = venueObj.venue.state;
                                index = _index;
                            }
                        });
                        if (index > 0) {
                            vars.gotLocations.unshift(vars.gotLocations.splice(index, 1)[0]);
                        }
                        $analytics.eventTrack('click', {
                            category: 'profile',
                            label: 'locations: set primary location'
                        });
                        if (!dontSave) {
                            vars.save();
                        }
                    };
                    vars.save = function () {
                        var update = {
                            locations: null
                        }, 
                        // Check if any of the venues are lacking a Venue ID (vid)
                        newVenues = _.reduce(vars.gotLocations, function (arr, locObj) {
                            if (!locObj.venue.vID) {
                                arr.push(locObj.venue);
                            }
                            return arr;
                        }, []), 
                        // Check if there are venues to save
                        promise = newVenues.length ? Venues.save(newVenues, true) : $q.when();
                        generateMarkers(vars, vars.gotLocations, true);
                        return promise.then(function () {
                            var locs = vars.gotLocations, length = locs.length;
                            if (length) {
                                update.locations = _.pluck(locs, 'user');
                            }
                            // set default primary if only 1
                            if (length === 1) {
                                locs[0].user.primary = true;
                            }
                            // attempt save
                            return scope.onSave({ updates: update });
                        })
                            .then(function () {
                            vars.activeEdit = false;
                            $analytics.eventTrack('click', {
                                category: 'profile',
                                label: 'edit profile: updated desired locations'
                            });
                            scope.$root.safeDigest(scope);
                        });
                    };
                    var toggleTrigger = function (toggle) {
                        var $hilite = iElem.closest('.can-hilite');
                        Util.$hilite($hilite);
                        // If toggle is boolean, will set edit to that, otherwise toggles
                        if ((vars.activeEdit = typeof toggle === 'boolean' ? toggle : !vars.activeEdit)) {
                            _.defer(function () {
                                Util.$scrollTo(iElem, null, {
                                    padding: 30
                                });
                                return iElem.find('.input-location').focus();
                            });
                            return scope.$root.safeDigest(scope);
                        }
                        $analytics.eventTrack('click', {
                            category: 'profile',
                            label: 'locations: ' + (vars.activeEdit ? 'enable' : 'disable') + ' edit locations'
                        });
                    };
                    DataBus.on('/profile/desiredLocation/toggle', toggleTrigger);
                    return scope.$on('$destroy', function () {
                        return DataBus.removeListener('/profile/desiredLocation/toggle', toggleTrigger);
                    });
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map