/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var imageSizes = require('../../../config/imageSizes.json');
var avatar_1 = require('front/sections/profile/directives/avatar');
require('../../../app').directive('avatar', [
    'DataBus', '$templateCache', 'User',
    function (DataBus, $templateCache, User) {
        return {
            restrict: 'E',
            replace: true,
            template: $templateCache.get('views/profile/directives/avatar/Template.html'),
            scope: {
                user: '=?',
                size: '@',
                opts: '=?',
                editable: '@',
                doWatch: '@',
                canRemove: '@',
            },
            controller: [
                'User',
                'Config',
                '$compile',
                '$scope',
                'ProfilePhoto',
                avatar_1.AvatarCtrl,
            ],
            controllerAs: 'viewModel',
            link: {
                pre: function (scope, iElem, iAttrs, Avatar) {
                    Avatar.init({
                        imageSizes: imageSizes,
                        user: scope.user,
                        size: scope.size,
                        opts: scope.opts,
                        iElem: iElem,
                    });
                    var genderFn = function () {
                        var genderWatch;
                        if (!genderListener && vars.editable) {
                            genderWatch = scope.gender ? 'gender' : 'user.details.gender';
                            return genderListener = scope.$watch(genderWatch, function (post, prev) {
                                Avatar.updateGender(post);
                                if (!Avatar.photo[Avatar.size] && post !== prev) {
                                    return Avatar.setAvatar();
                                }
                            });
                        }
                    }, onAuthChange, vars, genderListener = null;
                    scope.vars = vars = {
                        editable: !!scope.editable && Avatar.isSelf(),
                        showEdit: false,
                        canRemove: iAttrs.canRemove === 'true',
                    };
                    if (scope.editable || scope.doWatch) {
                        onAuthChange = function (auth) {
                            if (!auth && genderListener) {
                                genderListener();
                            }
                            vars.editable = !!scope.editable && Avatar.isSelf();
                            return genderFn();
                        };
                        DataBus.on('/site/authChange', onAuthChange);
                        scope.$on('$destroy', function () {
                            return DataBus.removeListener('/site/authChange', onAuthChange);
                        });
                    }
                    genderFn();
                    Avatar.setAvatar();
                },
                post: function (scope, iElem, iAttrs, Avatar) {
                    var photoUpdateCheck;
                    if (scope.editable || scope.doWatch) {
                        photoUpdateCheck = function (userID, updates) {
                            if (Avatar.isSelf(userID) && 'picture' in updates) {
                                Avatar.photo = updates.picture || {};
                                Avatar.photoID = userID;
                                Avatar.setAvatar(true);
                            }
                        };
                        User.on('/updated', photoUpdateCheck);
                        scope.$on('$destroy', function () {
                            User.removeListener('/updated', photoUpdateCheck);
                        });
                    }
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map