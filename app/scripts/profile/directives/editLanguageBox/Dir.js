/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var languageLevels = require('root/config/languageLevels'), 
// To prevent duplicate language keys we parse the languages array and weed em out
_uberprufLangs = function (langs) {
    var ids = [];
    return _.reduce(langs, function (obj, lang) {
        if (ids.indexOf(lang.languageID) === -1) {
            ids.push(lang.languageID);
            obj.push(lang);
        }
        return obj;
    }, []);
};
require('../../../app').directive('editLanguageBox', [
    'Config', 'SocketIo', 'Util', 'locale', '$templateCache',
    function (Config, SocketIo, Util, locale, $templateCache) {
        return {
            restrict: 'E',
            replace: true,
            template: $templateCache.get('views/profile/directives/editLanguageBox/Template.html'),
            scope: {
                source: '=',
                onSave: '&',
                onCancel: '&'
            },
            link: {
                pre: function (scope) {
                    scope.langs = {
                        levels: languageLevels,
                        exclude: [],
                        temp: {
                            level: null,
                            learning: false,
                            teaching: false,
                        },
                        length: 0,
                    };
                    // Get the complete languages object with display names
                    Util.language.expand(scope.source)
                        .then(function (complete) {
                        scope.langs.key = complete;
                        scope.langs.length = complete.length;
                        // We must modify the existing array instead of replacing it with a new reference
                        // since the autocomplete directive is listening to this reference
                        return [].push.apply(scope.langs.exclude, _.pluck(complete, 'name'));
                    });
                    // Make all of the language levels integers
                    return _.each(scope.source, function (lang) {
                        var level = p(lang.level);
                        return lang.level = !isNaN(level) ? level : lang.level;
                    });
                },
                post: function (scope) {
                    var langs = scope.langs;
                    langs.onAdd = function (name, languageID) {
                        langs.key.push({
                            languageID: languageID,
                            name: name,
                            level: langs.temp.level,
                            learning: langs.temp.learning,
                            teaching: langs.temp.teaching
                        });
                        langs.exclude.push(name);
                        langs.temp.level = null;
                        langs.temp.learning = false;
                        langs.temp.teaching = false;
                        langs.length++;
                        // this directive's scope is higher than the scope of just the language input
                        // and won't get updated on it's own so we digest here
                        scope.$root.safeDigest(scope);
                        // return 1 so that the placeholder text gets updated as well
                        // See addTerm in 'langComplete' to see full hook response type
                        return 1;
                    };
                    langs.onRemove = function (index) {
                        langs.key.splice(index, 1);
                        langs.length--;
                        return langs.exclude.splice(index, 1);
                    };
                    langs.onCancel = function () {
                        return scope.onCancel();
                    };
                    langs.onSave = function () {
                        langs.key = _uberprufLangs(langs.key);
                        var languages = _.map(langs.key, _.partialRight(_.pick, [
                            'languageID',
                            'level',
                            'learning',
                            'teaching'
                        ]));
                        return scope.onSave({
                            languages: languages,
                            complete: langs.key
                        });
                    };
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map