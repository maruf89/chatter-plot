/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../../app').directive('sectionContact', [
    '$q', '$templateCache', 'locale', 'DataBus',
    function ($q, $templateCache, locale, DataBus) {
        return {
            restrict: 'E',
            replace: true,
            template: $templateCache.get(
                'views/profile/directives/sectionContact/Template.html'),
            scope: {
                onSave: '&',
                source: '='
            },
            link: {
                pre: function (scope, elem, attr) {
                    var onAuthChange;
                    scope.section = {
                        editView: attr.onSave,
                        noAcceptMsg: null,
                        show: scope.$root.userID !== scope.source.userID
                    };
                    onAuthChange = function () {
                        return scope.section.show = scope.$root.userID !==
                            scope.source.userID;
                    };
                    DataBus.on('/site/authChange', onAuthChange);
                    if (!scope.source.settings.notifs.messaging.acceptAll) {
                        $q.all([locale.ready('common'), locale.ready(
                                'notifications')])
                            .then(function () {
                                var person;
                                person = scope.source.firstName ||
                                    locale.getString(
                                        'common.THIS_PERSON');
                                scope.section.noAcceptMsg = locale.getString(
                                    'notifications.BOB_NO_ACCEPT_MSG', {
                                        PERSON: person
                                    });
                                return scope.$root.safeDigest(scope);
                            });
                    }
                    return scope.$on('$destroy', function () {
                        return DataBus.removeListener(
                            '/site/authChange', onAuthChange
                        );
                    });
                },
                post: function (scope) {
                    var section;
                    section = scope.section;
                    section.save = function (what) {
                        var update;
                        update = {};
                        update[what] = !!p(scope.source[what]);
                        return scope.onSave({
                            updates: update
                        });
                    };
                    return section.messageUser = function () {
                        return DataBus.emit('/message/user', {
                            to: [scope.source]
                        });
                    };
                }
            }
        };
    }
]);
