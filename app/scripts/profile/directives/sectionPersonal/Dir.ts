/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../../app').directive('sectionPersonal', [
    '$analytics', '$templateCache', 'DataBus', 'Util',
    function ($analytics, $templateCache, DataBus, Util) {
        return {
            restrict: 'E',
            replace: true,
            template: $templateCache.get(
                'views/profile/directives/sectionPersonal/Template.html'
            ),
            scope: {
                onSave: '&',
                source: '=',
                defaultOpen: '=?',
            },
            link: {
                pre: function (scope) {
                    return scope.vars = {
                        activeEdit: scope.defaultOpen || false,
                        editDescription: scope.source.personal,
                        id: "secPersonal-" + scope.$id
                    };
                },
                post: function (scope, iElem) {
                    var toggleTrigger, vars;
                    vars = scope.vars;
                    vars.save = function () {
                        var update;
                        if (scope.source.personal === vars.editDescription) {
                            return vars.activeEdit = false;
                        }
                        update = {
                            personal: vars.editDescription.replace(
                                /\n\r|\n|\r/g, '<br>')
                        };
                        return scope.onSave({
                                updates: update
                            })
                            .then(function () {
                                vars.activeEdit = false;
                                return $analytics.eventTrack(
                                    'click', {
                                        category: 'profile',
                                        label: 'edit profile: personal'
                                    });
                            });
                    };
                    toggleTrigger = function (toggle) {
                        var $hilite;
                        $hilite = iElem.closest('.can-hilite');
                        Util.$hilite($hilite);

                        // If toggle is boolean, will set edit to that, otherwise toggles
                        if ((vars.activeEdit = typeof toggle ===
                                'boolean' ? toggle : !vars.activeEdit
                            )) {
                            _.defer(function () {
                                Util.$scrollTo(iElem, null, {
                                    padding: 30
                                });
                                return iElem.find(
                                        '.edit-input')
                                    .focus();
                            });
                            return scope.$root.safeDigest(scope);
                        }
                    };
                    DataBus.on('/profile/personal/toggle', toggleTrigger);
                    return scope.$on('$destroy', function () {
                        return DataBus.removeListener(
                            '/profile/personal/toggle',
                            toggleTrigger);
                    });
                }
            }
        };
    }
]);
