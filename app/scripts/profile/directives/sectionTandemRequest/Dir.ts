/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../../app.js').directive('sectionTandemRequest', ['$templateCache',
    function ($templateCache:ng.ITemplateCacheService) {
        return {
            restrict: 'E',
            replace: true,
            template: $templateCache.get('views/profile/directives/sectionTandemRequest/Template.html'),
            scope: {
                reqUser:    '=',    // {object} the user that is being requested
            },
        };
    }
]);
