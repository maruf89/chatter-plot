/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
/**
 * @ngdoc directive
 * @module peopleDisplayDirective
 */
'use strict';
/**
 * @description What we need from each user
 *
 * @type {array<string>}
 * @memberof PeopleDisplayDirective
 */
var _requiredUserFields = [
    'userID',
    'firstName',
    'lastName',
    'languages',
    'details',
    'city',
    'state',
    'country',
    'picture',
    'locations'
];
require('../../../app').directive('peopleDisplay', ['$templateCache', 'User', '$q',
    function ($templateCache, User, $q) {
        /**
         * @description parses an array of user display sections and converts the userIDs => user objects
         *
         * @name parseSections
         * @membefor PeopleDisplayDirective
         * @param {array<object>} sections
         * @param {function} reject - function to call if no users & is empty
         * @returns {Promise<array>} returns a new array of sections with complete user data
         */
        var parseSections = function (sections, reject) {
            // Get all of the userIDs of all of the sections into 1 array (don't worry if there's duplicates)
            var allUsers = _.filter([].concat.apply([], _.pluck(sections, 'userIDs')), _.identity);
            // If there's nothing to iterate then exit early
            if (!allUsers.length) {
                return reject();
            }
            // the first time around request all of the users
            return User.cacheGet(allUsers, _requiredUserFields, 'breakOnFirst')
                .then(function () {
                // next iterate over all of sections
                return $q.all(_.map(sections, function (section) {
                    // Instead of modifying the passed in section, we will create our own copy
                    var parsed = _.clone(section), userIDs = section.userIDs;
                    // If nothing to fetch than return empty section
                    if (!userIDs || !userIDs.length) {
                        parsed.users = {};
                        parsed.empty = true;
                        return parsed;
                    }
                    // Call the same method again, but this time only with the
                    // users per each group. It will return the cached version of the users
                    return User.cacheGet(userIDs)
                        .then(function (users) {
                        // Will hold all of the deleted users
                        var deleted = [], hasDuds = null;
                        // append the returned users
                        parsed.users = _.filter(users, function (user, userID) {
                            if (user.isDud) {
                                hasDuds = deleted.push(p(userID));
                                return false;
                            }
                            return true;
                        });
                        if (hasDuds) {
                            parsed.deleted = deleted;
                        }
                        // packaged :)
                        return parsed;
                    });
                }));
            });
        };
        return {
            restrict: 'E',
            replace: true,
            template: $templateCache.get('views/profile/directives/peopleDisplay/Template.html'),
            scope: {
                sections: '=',
                // {string=} sections.title - i18n key
                // {string=} sections.translatedTitle - if no title passed, will not translate this
                // {boolean=} sections.hideEmpty - if true, and section is empty, will hide the section
                onBuild: '&',
                onUpdate: '=?',
                transclude: '@',
                linkClass: '@',
                category: '@'
            },
            transclude: true,
            link: {
                pre: function (scope, iElem, iAttrs, Ctrl, transclude) {
                    var doParsing, vars, notStarted = true;
                    scope.vars = vars = {
                        sections: null
                    };
                    scope.linkClass = scope.linkClass || 'p-display';
                    if (iAttrs.transclude) {
                        transclude(scope, function (clone) {
                            iElem.find('.transclude').append(clone);
                        });
                    }
                    /**
                     * Parses the passed in sections and updates the directive
                     */
                    doParsing = function () {
                        return parseSections(scope.sections, $q.reject, scope.linkClass)
                            .then(function (parsedSections) {
                            vars.sections = parsedSections;
                            iElem.removeClass('is-empty');
                            if (scope.onBuild) {
                                return scope.onBuild({
                                    sections: parsedSections
                                });
                            }
                            scope.$root.safeDigest(scope);
                        }).finally(function () {
                            iElem.removeClass('is-loading');
                        });
                    };
                    if (iAttrs.onUpdate) {
                        scope.$watch('onUpdate', function (post, prev) {
                            if ((!notStarted && post) || post !== prev) {
                                doParsing();
                                notStarted = false;
                            }
                        });
                    }
                    _.defer(function () {
                        if (notStarted) {
                            notStarted = false;
                            doParsing();
                        }
                    });
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map