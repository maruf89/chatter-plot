/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var name = 'userLocation';

require('../../../app').directive(name, ['Config', 'Venues', 'uiGmapGoogleMapApi', 'Util', '$analytics', '$templateCache',
    function (Config, Venues, GoogleMapApi, Util, $analytics, $templateCache) {
        // the primary location will always be the first item in the array
        var getPrimary = function (locations) {
            return _.isArray(locations) && locations[0];
        };

        return {
            restrict: 'E',
            replace: true,
            template: $templateCache.get('views/profile/directives/userLocation/Template.html'),
            scope: {
                source: '=',
                doWatch:  '@',
            },
            link: {
                pre: function (scope) {
                    var locations = scope.source,
                        displayFields = ['sublocality', 'city', 'state'],
                        primary = getPrimary(locations),
                        displayLoc = function (post) {
                            if (primary = getPrimary(post)) {
                                vars.displayAddress = Util.geo.format.address(primary, displayFields, ', ')
                                    || primary.country;
                                scope.$root.safeDigest(scope);
                            }
                        },
                        vars;

                    scope.vars = vars = {
                        displayAddress: null
                    };

                    if (scope.doWatch) {
                        scope.$watch('source', displayLoc, true);
                    }

                    if (primary) {
                        vars.displayAddress = Util.geo.format.address(primary, displayFields, ', ')
                            || primary.country;
                    }
                }
            }
        };
    }
]);
