/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var tagsList:any = require('root/config/paidTags');

class PaidTagsCtrl {
    public data:any;
    public valChanged:boolean;
    public hasSelected:boolean;

    private _priceVal:number = tagsList.tagsPrice; // $ dollars
    private _sendAnalytic:(obj:any) => void;

    constructor(
        private _$analytics:Angulartics.IAnalyticsService,
        $state:ng.ui.IStateService,
        User:cp.user.IService,
        Util:any
    ) {
        this.data = {
            tags: _.map(tagsList.tags, function (val:any, key:string) {
                return _.extend({
                    selected: false,
                    key: key,
                }, val)
            }),
            price: 0,
            validated: false,
            feedbackUrl: $state.href('standalone.feedback', {
                redirect: 'dashboard.profile.user',
                action: Util.encodeActionArgs({ userID: User.userID })
            }),
        };

        this._sendAnalytic = _.debounce(this.__sendAnalytic.bind(this), 301);
    }

    public valClicked(obj:any):void {
        this.valChanged = true;
        this._sendAnalytic(obj);
    }

    private __sendAnalytic(obj:any):void {
        var addRemove:string = obj.selected ? 'add' : 'remove';

        this._$analytics.eventTrack('click', {
            category: 'acquisition',
            label: `validation: tags: ${obj.key}: ${addRemove}`,
        });
    }

    private _calculateVal(priceVal?:number):number {
        return _.reduce(this.data.tags, function (number:number, obj:any):number {
            if (obj.selected) {
                number += priceVal || p(obj.value);
            }

            return number;
        }, 0);
    }

    public toPrice():string {
        if (this.valChanged) {
            this.data.price = this._calculateVal(this._priceVal);
            this.hasSelected = !!this.data.price;
        }

        return this.data.price.toFixed(2);
    }

    public validate():void {
        if (!this.data.price) { return; }

        this.data.validated = true;

        this._$analytics.eventTrack('click', {
            category: 'acquisition',
            label: 'validation: tags: PayPal',
            value: this._calculateVal(),
        });
    }
}

PaidTagsCtrl.$inject = ['$analytics', '$state', 'User', 'Util'];

require('../../../app').controller('PaidTagsCtrl', PaidTagsCtrl);
