/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../../app').factory('profileEditBasic', [
    'btfModal', '$templateCache',
    function (Modal, $templateCache) {
        return Modal({
            template: $templateCache.get('views/profile/modals/edit/basic.html'),
            container: '.map-pane'
        });
    }
]);
//# sourceMappingURL=Factory.js.map