/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../../app').service('ProfileCompletion', [
    'btfModal',
    '$rootScope',
    '$templateCache',
    require('root/profile/directives/ProfileCompletion'),
]);
