/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../../app').factory('editLanguagesModal', [
    'btfModal', '$templateCache',
    function (Modal, $templateCache) {
        return Modal({
            template: $templateCache.get(
                'views/profile/modals/editLanguages/Template.html'
            )
        });
    }
]);
