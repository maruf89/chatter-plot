/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
/**
 * @description checks whether the current user has any (non-temporary) locations set
 * @param {(null|array)} locs
 * @returns {boolean}
 * @private
 */
var _hasLocationsCheck = function (locs) {
    var length;
    return _.isArray(locs) && (length = locs.length) && (length > 1 || locs[0].type !== 'temp');
}, 
/**
 * The name given to the map stack layer
 * @type {string}
 */
setName = 'user', commonGeneral = require('common/util/general');
/**
 * @ngdoc controller
 * @class
 * @classdesc Handles view only as well and supports edit profile capabilities through the {@link Profile} factory
 */
var ProfileCtrl = (function () {
    function ProfileCtrl(_$scope, _Util, _Maps, _ProfileCompletion, _Meta, _$stateParams, _User, _$q, _$analytics, _locale, CurUser, Auth, Profile, _Modal) {
        this._$scope = _$scope;
        this._Util = _Util;
        this._Maps = _Maps;
        this._ProfileCompletion = _ProfileCompletion;
        this._Meta = _Meta;
        this._$stateParams = _$stateParams;
        this._User = _User;
        this._$q = _$q;
        this._$analytics = _$analytics;
        this._locale = _locale;
        this._Modal = _Modal;
        this._stateAction = {};
        _$scope.profile = this.profile = Profile.create(CurUser);
        this._user = _$scope.user = this.profile.user;
        this._initData();
        this.profile.on('/updated', this._onProfileUpdate.bind(this));
        var toggleUserProfile = this._toggleProfChecks.bind(this);
        Auth.on('/authChange', toggleUserProfile);
        _$scope.$on('$destroy', function () {
            var instance = this.mapInstance;
            this.profile.destroy();
            Auth.removeListener('/authChange', toggleUserProfile);
            _ProfileCompletion.deactivate();
            if (instance) {
                instance.deselectSet();
            }
        }.bind(this));
    }
    ProfileCtrl.prototype._initData = function () {
        var _action = this._$stateParams['action'], $scope = this._$scope, meta;
        this._mapSet = {
            disableCluster: true
        };
        this.mapInstance = this._Maps.getInstance();
        this.mapInstance.selectSet(setName, this._mapSet);
        this.mapInstance.clearMarkers();
        // Save any state param actions
        if (_action) {
            this._stateAction = this._Util.parseActionArgs(_action);
        }
        this.vars = this.data = {
            parts: {
                personal: !!this._user.personal,
                interests: _.isArray(this._user.interests) && this._user.interests.length,
                locations: _hasLocationsCheck(this._user.locations),
                availability: false,
                contact: !!this._user.settings.notifs.messaging.acceptAll,
                canTeach: null,
            },
            userPicture: this._User.format.photoSrc(this._user, 'default'),
        };
        this._canTeachCheck();
        this._toggleProfChecks();
        this._setTempLoc();
        this.meta = meta = {
            name: this._user.fullName,
            description: '',
            canonical: this._Meta.canonical(),
            picture: this.vars.userPicture,
        };
        this._metaText().then(function (_meta) {
            _.extend(meta, _meta);
            $scope.$root.safeDigest($scope);
        });
        this._$scope.$root.mustBeLoadedIn(2500);
    };
    /**
     * @description builds the meta description for this profile
     *
     * @private
     * @returns {Promise<string>} returns the translated description
     */
    ProfileCtrl.prototype._metaText = function () {
        var descKey = 'meta.PROFILE_OG_DESC', titleKey = 'meta.PROFILE_OG_TITLE';
        return this._$q.all([
            this._locale.ready('meta'),
            this._locale.ready('languages'),
            this._locale.ready('common'),
        ])
            .then(function () {
            var C2 = [], native = [], learning = [], and = this._locale.getString('common.AND'), data = {
                n_language: null,
                l_language: null,
            }, meta = {
                description: null,
                title: this._locale.getString(titleKey, {
                    city: this.profile.user.locations[0].city
                }),
            };
            _.each(this.profile.user.languages, function (lang) {
                // Grab C2 language level if they don't have a native language set
                var name = this._locale.getString(this._Util.language.getI18nKey(lang.languageID));
                if (lang.level === 6) {
                    C2.push(name);
                }
                else if (lang.level === 7) {
                    native.push(name);
                }
                if (lang.learning) {
                    learning.push(name);
                }
            }, this);
            if (learning.length && (native.length || C2.length)) {
                data.n_language = commonGeneral.string.strArray2AndList(native || C2, and);
                data.l_language = commonGeneral.string.strArray2AndList(learning, and);
                meta.description = this._locale.getString(descKey, data);
            }
            return meta;
        }.bind(this))
            .catch(function (err) {
            console.log('error building profile meta description', err);
        });
    };
    /**
     * @description updates the canTeach value to whether a person is teaching languages
     * & they have their settings set to accept students
     * @private
     */
    ProfileCtrl.prototype._canTeachCheck = function (languages) {
        this.data.parts.canTeach = this._user.settings.requests.teacher &&
            _.some(languages || this._user.languages, function (lang) {
                return lang.teaching;
            });
    };
    /**
     * @description checks whether the profile completion should be loaded for a user
     * @returns {*}
     * @private
     */
    ProfileCtrl.prototype._toggleProfChecks = function () {
        var isMe = this.profile.isMe();
        if (isMe &&
            this._User.userID) {
            if (!this._stateAction.firstTime) {
                _.defer(function () {
                    this._ProfileCompletion.openCompletion(this.profile);
                }.bind(this));
            }
            else {
                this._firstTimeLoad();
            }
        }
        this._ProfileCompletion.deactivate();
    };
    ProfileCtrl.prototype._firstTimeLoad = function () {
        var $analytics = this._$analytics, picture = this.profile.user.picture;
        // $PAID TAGS: Validation
        if (CP.releases.indexOf('paidTags') !== -1) {
            this._Modal.activate({
                template: 'views/profile/modals/paidTags/Template.html',
                'class': 'paid-tag-cont',
            });
        }
        else if (this._$scope.$root.size.sm && (!picture ||
            !(picture.hasPhoto || picture.default))) {
            // if: atleast tablet AND we don't have a photo -> open photo upload modal
            this.profile.uploadPhoto(null, { introTemplate: 'views/profile/modals/profilePhotoSignup/Template.html' })
                .then(function () {
                $analytics.eventTrack('click', {
                    category: 'profile',
                    label: 'signup photo: successfully uploaded'
                });
            })
                .catch(function () {
                $analytics.eventTrack('click', {
                    category: 'profile',
                    label: 'signup photo: skipped'
                });
            });
        }
    };
    ProfileCtrl.prototype._onProfileUpdate = function (updates) {
        var scope = this._$scope;
        if ('picture' in updates) {
            this.vars.userPicture = !!updates.picture && this._User.format.photoSrc(this._user, 'default');
            scope.$root.safeDigest(scope);
        }
        if ('languages' in updates) {
            this._canTeachCheck(updates.languages);
            scope.$root.safeDigest(scope);
        }
    };
    ProfileCtrl.prototype.editSection = function (event) {
        var section = event.target.getAttribute('data-section');
        if (!section) {
            return;
        }
        this.profile.editSection(section);
    };
    ProfileCtrl.prototype._setTempLoc = function () {
        var temp, instance = this.mapInstance, curSet, coords, defaultCircle;
        // Check that we have a temporary location
        if (!_.isArray(this._user.locations) ||
            !this._user.locations.length ||
            (temp = this._user.locations[0]).type !== 'temp') {
            return;
        }
        curSet = instance.getSet(setName);
        coords = this._Maps.normalizeCoords(temp.coords);
        defaultCircle = this._Maps.circle.nonEditable;
        instance.loaded().then(function () {
            curSet.circle = _.extend({
                center: coords,
                radius: 5000
            }, defaultCircle);
            _.defer(instance.centerMap.bind(instance, coords, 12));
        });
    };
    return ProfileCtrl;
})();
ProfileCtrl.$inject = [
    '$scope',
    'Util',
    'Maps',
    'ProfileCompletion',
    'Meta',
    '$stateParams',
    'User',
    '$q',
    '$analytics',
    'locale',
    'CurUser',
    'Auth',
    'Profile',
    'ModalService',
];
require('../app').controller('ProfileCtrl', ProfileCtrl);
//# sourceMappingURL=Ctrl.js.map