/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var dudAccount:any = {
        fullName: null,
        isDud: true,
        details: {
            gender: 'MALE'
        }
    },

    User,

    commonUser = require('common/util/user').init({ noSVG: !Modernizr.svg });

exports.combineLanguages = function (langA, langB) {
    if (!langA || langA === langB) {
        return langB;
    }

    if (!langB || !langB.length) {
        return langA;
    }

    return _.uniq(langB.concat(langA), 'languageID');
};


/**
 * @class
 * @classdesc Stores the logged in user's information. Handles user updates and includes helper
 * methods for formatting users
 *
 * @requires {DataBus} DataBus
 * @requires {localStorageService} localStorageService
 * @requires {SocketIo} SocketIo
 * @requires {Request} Request
 * @requires {Auth} Auth
 * @requires {$q} $q
 * @requires {locale} locale
 * @requires {$rootScope} $rootScope
 * @requires {Config} Config
 */

User = function (
    DataBus:any,
    locStorage:any,
    SocketIo:any,
    Request:any,
    Auth:any,
    $q:ng.IQService,
    locale:any,
    $rootScope:cp.IRootScopeService,
    Config:any,
    Util:any,
    Cloudinary:any
):cp.user.IService {

    this._DataBus = DataBus;
    this._SocketIo = SocketIo;
    this._Request = Request;
    this._Auth = Auth;
    this._$q = $q;
    this._locale = locale;
    this._$rootScope = $rootScope;
    this._Config = Config;
    this._Util = Util;
    this._Cloudinary = Cloudinary;

    this.data = <cp.user.IVolatileSecure> {};

    var self = this;
    User = this;

    // Will store in-memory reference to other users 
    this.users = <cp.user.IUserCache>{
        'null': dudAccount
    };

    Auth.on('/authenticated/ready', function (userData) {
        Auth.removeAllListeners('/connect/extendAccnt');
        Auth.once('/connect/extendAccnt', function (updates) {
            var langA, langUpdates;
            if ((langA = updates.languages) &&
                (langUpdates = exports.combineLanguages(langA, userData.languages))) {
                updates.languages = langUpdates;
            }
            self.update(updates);
        });

        $rootScope.safeApply(function () {
            self.userID = self._$rootScope.userID = userData.userID;
            self._initSelf(userData);
            locStorage.set('userData', userData);
            DataBus.emit('/settings/ready', userData.settings);
        });
    }.bind(this));

    this._Auth.on('/deauthenticated', function () {
        self.userID = self._$rootScope.userID = null;
        self.data = {};
    });

    this._locale.ready('auth').then(function () {
        dudAccount.fullName = self._locale.getString('auth.A_DELETED_USER');
    });
};

User.prototype = _.extend(User.prototype, require('node-event-emitter').prototype, {
    _initSelf: function (userData:cp.user.ISecure):void {
        var attending, favorite;
        this.data = userData;
        this.data.fullName = this.format.name(userData);

        // Make all of the language levels integers
        _.each(this.data.languages, function (lang:any) {
            return lang.level = !isNaN(parseInt(lang.level, 10)) ? parseInt(lang.level, 10) : lang.level;
        });

        userData.locations = userData.locations || [];

        userData.settings = userData.settings || this._Config.settings;
        attending = userData.attending = userData.attending || {};
        attending.events = attending.events || [];
        attending.eventsWaitList = attending.eventsWaitList || [];
        favorite = userData.favorite = userData.favorite || {};
        favorite.all = favorite.all || [];
    },

    /**
     * Updates the user in the database
     *
     * @name User#update
     * @param  {object}          updates
     * @param  {String|Boolean}  persistent  whether to update the persistent database
     * @returns {Promise}
     */
    update: function (updates:any, persistent?:boolean):ng.IPromise<boolean> {
        if (!_.isPlainObject(updates)) {
            throw new Error('Expecting updates parameter to be an object');
        }

        var emitCall = typeof persistent === 'string' ? persistent :
            persistent ? '/user/persistentEdit' : '/user/edit',

            opts = {
                edits: updates,
                userID: this.data.userID
            };

        this._preprocessUpdates(updates);

        return this._SocketIo.onEmitSock(emitCall, opts)
            .then(function () {
                this._DataBus.emit('yap', {
                    type: 'success',
                    title: 'updates.UPDATED_EXCL',
                    body: 'updates.SUCCESS-FIELDS_UPDATED'
                });

                var processedUpdates = this._postprocessUpdates(updates);

                _.merge(this.data, processedUpdates);

                this.emit('/updated', opts.userID, updates);
                return true;
            }.bind(this))["catch"](function (err) {
                this._DataBus.emit('yap', {
                    type: 'error',
                    title: 'updates.ERROR_EXCL',
                    body: "updates.ERROR-" + err.type
                });
                throw false;
            }.bind(this));
    },

    _preprocessUpdates: function (updates:any):void {
        if ('locations' in updates) {
            updates.locations = this._preprocess.locations(updates.locations);
        }
    },

    _preprocess: {
        locations: function (locations) {
            if (!locations) {
                return locations;
            }

            var fields = [
                'vID',
                'google',
                'yelp',
                'primary',
                'coords',
                'sublocality',
                'city',
                'state',
                'country',
            ];

            return _.map(locations, function (location) {
                return _.pick(location, fields);
            });
        }
    },

    _postprocessUpdates: function (updates:any):any {
        var processed = updates;

        if ('locations' in updates) {
            processed = _.cloneDeep(updates);
            delete processed.locations;
            this.data.locations = updates.locations;
        }

        return processed;
    },

    /**
     * @description Formats a user for appearing in the messages list
     *
     * @method
     * @name User#addPeople
     * @param {object} userObj - the users to format
     * @param {array<number>=} userIDs - userIds of the passed in users
     * @param {boolean=} asObject - whether the passed in object is to be parsed as an object
     * @returns {object} - the users cache
     */
    addPeople: function (userObj:cp.user.IUser, userIDs?:number[], asObject?:boolean):cp.user.IUserCache {
        var isArray = _.isArray(userObj);

        if (isArray) {
            userIDs = _.clone(userIDs);
        }

        _.each(userObj, function (user:cp.user.IUser, key?:(number|any)) {
            var index,
                userID = user && isArray ? user.userID : key,
                userRef;

            if (!userID || (!user && asObject)) {
                return true;
            }

            if (this.users[userID]) {
                _.merge(this.users[userID], user);
            } else {
                this.users[userID] = user;
            }

            userRef = this.users[userID];
            userRef.fullName = user.fullName || this.format.name(user);
            userRef.userID = userID;

            if (userIDs && (index = _.indexOf(userIDs, userID)) !== -1) {
                userIDs.splice(index, 1);
            }
        }, this);

        if (userIDs && userIDs.length) {
            _.each(userIDs, function (dudID:number):any {
                return this.users[dudID] = dudAccount;
            }, this);
        }

        return this.users;
    },

    /**
     * @description Fetches either the current user (if not options passed), a specific user (only number passed)
     * Or a group of users {array} userIDs [{number}, {number}, etc…]
     *
     * @method
     * @name User#get
     * @param {(object|number)} opts - if number then assuming it's a userID, else:
     * @param {number} opts.userID - if getting a SINGLE user
     * @param {array<number>} opts.userIDs - if getting MULTIPLE users
     * @param {array<string>} opts._source - the fields to fetch
     * @returns {Promise}
     */
    get: function (opts:any):ng.IPromise<cp.user.IUser[]> {
        var num = p(opts);

        if (_.isArray(opts)) {
            opts = { userIDs: opts };
        } else if (!opts || (!isNaN(num) && num)) {
            opts = { userID: num || this.data.userID };
        }

        return this._Request.onEmitSock('/user/get', opts, true);
    },

    /**
     * Same as get except that it checks the current cache for whether we
     * already have information for the users
     *
     * @name User#cacheGet
     * @param {array<number>} userIDs
     * @param {array<string>} source - the fields that are required (default:breakOnFirst)
     */
    cacheGet: function (userIDs:number[], source?:string[], algorithm?:string):ng.IPromise<cp.user.IUserCache> {
        var breakFn, hasCached, missingIDs, numUsers, returnUsers,
            usersToFetch;

        source = source || [];
        algorithm = typeof algorithm === undefined ? 'breakOnFirst' : algorithm;

        hasCached = true;
        userIDs = _.uniq(userIDs);
        returnUsers = {};
        missingIDs = null;

        // If `algorithm` isn't 'breakOnFirst' then this will be accurate
        numUsers = 0;

        if (algorithm === 'breakOnFirst') {
            breakFn = function () {
                return hasCached = false;
            };
        } else {
            missingIDs = [];
            breakFn = function (userID) {
                hasCached = false;
                return missingIDs.push(userID);
            };
        }

        _.each(userIDs, function (userID:number):any {
            var user:cp.user.IUser = this.users[userID];

            numUsers++;

            if (user && !this._Util.lacksFields(source, user)) {
                return returnUsers[userID] = user;
            }
            return breakFn(userID);
        }, this);

        // if we have everything then return the users
        if (hasCached) {
            return this._$q.when(returnUsers);
        }

        // otherwise fetch
        usersToFetch = missingIDs || userIDs;
        return this.get({
            userIDs: usersToFetch,
            _source: source,
            multi: true
        })
        .then(function (newUsers) {
            this.addPeople(newUsers, usersToFetch, true);
            return this._userID2User(usersToFetch);
        }.bind(this));
    },

    /**
     * Converts an array of userIDs to an object with those users
     *
     * @name User#_userID2User
     * @private
     * @param {array<number>} userIDs - an array of userIDs
     * @returns {object} mapping of { userID: userData }
     */
    _userID2User: function (userIDs:number[]):cp.user.IUserCache {
        return _.reduce(userIDs, function (userObj:any, userID:number) {
            userObj[userID] = this.users[userID];
            return userObj;
        }, {}, this);
    },

    /**
     * Object containing utility methods for formatting user objects
     *
     * @name User.format
     * @property {object}
     */
    format: {
        /**
         * returns the full name of a user
         *
         * @name User.format#name
         * @param {User} user
         * @param {string=} [ifEmpty=---] - what to set the string to if no name available
         * @returns {string} the full name
         */
        name: function (user:cp.user.IUser, ifEmpty:string):string {
            if (ifEmpty == null) {
                ifEmpty = '---';
            }

            user = user || User.data;

            var name = [];

            if (user.firstName) {
                name.push(user.firstName);
            }

            if (user.lastName) {
                name.push(user.lastName);
            }

            return name.join(' ') || ifEmpty;
        },

        /**
         * returns an image avatar to use for a user for a given size.
         * Defaults to an illustration based on gender if none available
         *
         * @name User.format#photoSrc
         * @param {User} user
         * @param {string=} [size=thumb] - photo size
         * @param {boolean=} [defaultAvatar=false] - if true and no photo exists, will return an avatar
         * @returns {string} url of the image src
         */
        photoSrc: commonUser.photoSrc,

        /**
         * @description returns the current age from a birthdate
         *
         * @name User.format#birthday2Age
         * @param {string} birthdate - Any value that is acceptable by the Date object such as "1987-02-13"
         * @returns {number} the age
         */
        birthday2Age: function (birthdate:string) {
            return (birthdate && new Date(Date.now() - <any>(new Date(birthdate))).getUTCFullYear() - 1970) || '';
        }
    }
});

User.$inject = [
    'DataBus',
    'localStorageService',
    'SocketIo',
    'Request',
    'Auth',
    '$q',
    'locale',
    '$rootScope',
    'Config',
    'Util',
    'Cloudinary'
];

require('../app.js').service('User', User);
