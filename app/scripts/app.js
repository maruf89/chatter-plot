/// <reference path="../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
/**
 * Stores all route objects to be loaded
 * @type {Array}
 */
require('./chatterplot.templates');
require('front/modules/Facebook');
require('front/modules/miscDirectives');
require('front/modules/SocketIo');
require('front/modules/StickyHeader');
require('front/modules/ui.router.history');
require('front/modules/ResponsiveImage');
require('front/modules/BrowserSize');
require('front/modules/angular.datepicker');
var misc = require('front/modules/misc.js'), supportedLocales = require('common/config/locales'), routeMap = _.extend(CP.site.routes, { path: '' }), routeConfig = [
    require('./config/routes/landingRoute')(routeMap),
    require('./config/routes/pStandaloneRoute')(routeMap),
    require('root/config/routes/accountRoute')(routeMap),
    require('root/config/routes/emailRoute')(routeMap),
    require('root/config/routes/adminRoute')(routeMap),
    require('root/config/routes/standaloneRoute')(routeMap),
    require('root/config/routes/activityRoute')(routeMap),
    require('root/config/routes/dashboardRoute')(routeMap),
    require('root/config/routes/shortcuts')(routeMap),
];
module.exports = angular.module('Chatterplot', ['angulartics',
    'angulartics.google.analytics', 'ngAnimate', 'ngMessages', 'ipCookie',
    'ngSanitize', 'ui.router', 'ui.router.stateHelper', 'ui.router.history', 'LocalStorageModule',
    'uiGmapgoogle-maps', 'btford.modal', 'maruf89.SocketIo',
    'FacebookProvider', 'ngCkeditor', 'ngJcrop', 'flow', 'ngLocalize',
    'sexyDatepicker', 'chatterplot.miscDirectives',
    'chatterplot.templates', 'headroom', 'maruf89.StickyHeader', 'maruf89.ResponsiveImage',
    'maruf89.BrowserSize',
])
    .value('localeConf', {
    basePath: 'languages',
    defaultLocale: 'en-US',
    sharedDictionary: 'common',
    fileExtension: '.lang.json?v=' + CP.deployVersion,
    persistSelection: true,
    cookieName: 'COOKIE_LOCALE_LANG',
    observableAttrs: new RegExp('^data-(?!ng-|i18n)'),
    delimiter: '||'
})
    .value('localeSupported', supportedLocales.list)
    .value('localeFallbacks', supportedLocales.defaultMap)
    .config([
    '$stateProvider', '$locationProvider', '$urlRouterProvider',
    'stateHelperProvider', 'localStorageServiceProvider',
    'flowFactoryProvider', '$provide', 'uiGmapGoogleMapApiProvider',
    'SocketIoProvider', 'ngJcropConfigProvider', 'ResponsiveImageProvider',
    'BrowserSizeProvider',
    function ($stateProvider, $locationProvider, $urlRouterProvider, stateHelperProvider, localStorageServiceProvider, flowFactoryProvider, $provide, GoogleMapApi, SocketIo, ngJcropConfigProvider, ResponsiveImageProvider, BrowserSizeProvider) {
        flowFactoryProvider.defaults = {
            permanentErrors: [404, 500, 501],
            maxChunkRetries: 1,
            chunkRetryInterval: 5000,
            testChunks: false,
            simultaneousUploads: 4
        };
        localStorageServiceProvider.setPrefix('cp');
        // If Internet Explorer Or iPhone then disable the socket timeout reconnect attempt
        if (misc.getInternetExplorerVersion() !== -1 || misc.isIphone()) {
            SocketIo.setTimeout(false);
        }
        ResponsiveImageProvider.setResponsiveSource(require('./config/imageSizes.json'));
        // !Load Routes
        _.each(routeConfig, function (route) {
            return stateHelperProvider.setNestedState(route);
        });
        routeConfig = null;
        $urlRouterProvider.otherwise('/404');
        $urlRouterProvider.deferIntercept(true);
        // IE9 doesn't support history state so we need this hack
        if (!Modernizr.history) {
            if (!/\/#\//.test(window.location.href)) {
                window.location = '/#!/';
            }
        }
        GoogleMapApi.configure({
            key: CP.Settings.google.apiKey,
            v: '3.20',
            libraries: 'places',
            language: CP.locale.substr(0, 2),
        });
        $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('!');
        ngJcropConfigProvider.setJcropConfig({
            bgColor: 'black',
            bgOpacity: .4,
            aspectRatio: 1,
            maxWidth: 468,
            maxHeight: 600,
            minSize: [150, 150]
        });
        // Workaround for bug #1404 - ngForm name not interpolating
        // https://github.com/angular/angular.js/issues/1404
        // Source: http://plnkr.co/edit/hSMzWC?p=preview
        $provide.decorator("ngModelDirective", function ($delegate) {
            var ngModel = $delegate[0], controller = ngModel.controller;
            ngModel.controller = [
                "$scope", "$element", "$attrs", "$injector",
                function (scope, element, attrs, $injector) {
                    var $interpolate = $injector.get("$interpolate");
                    attrs.$set("name", $interpolate(attrs.name || "")(scope));
                    return $injector.invoke(controller, this, {
                        $scope: scope,
                        $element: element,
                        $attrs: attrs
                    });
                }
            ];
            return $delegate;
        });
        $provide.decorator("formDirective", function ($delegate) {
            var form = $delegate[0], controller = form.controller;
            form.controller = [
                "$scope", "$element", "$attrs", "$injector",
                function (scope, element, attrs, $injector) {
                    var $interpolate;
                    $interpolate = $injector.get("$interpolate");
                    attrs.$set("name", $interpolate(attrs.name ||
                        attrs.ngForm || "")(scope));
                    return $injector.invoke(controller, this, {
                        $scope: scope,
                        $element: element,
                        $attrs: attrs
                    });
                }
            ];
            return $delegate;
        });
        return $provide.decorator('$state', function ($delegate, $rootScope) {
            $rootScope.$on('$stateChangeStart', function (event, state, params) {
                $delegate.next = state;
                $delegate.toParams = params;
            });
            return $delegate;
        });
    }
]);
//# sourceMappingURL=app.js.map