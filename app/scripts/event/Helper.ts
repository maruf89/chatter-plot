/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var app = require('../app'),
    Helper = require('root/event/Helper');

app.factory('EventHelper', [
    '$state',
    'DataBus',
    'Util',
    Helper.Helper,
]);

app.factory('EventMap', [
    'Util',
    Helper.Map,
]);

app.service('EventModify', [
    '$q',
    'Venues',
    'Config',
    'Util',
    require('root/event/Modify'),
]);
