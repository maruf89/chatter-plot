/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../app').factory('eventsMethods', [
    'DataBus',
    '$q',
    'Events',
    'Auth',
    'Util',
    '$rootScope',
    'locale',
    'User',
    require('root/event/EventsMethods')
]);
