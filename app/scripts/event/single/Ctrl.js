/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var stackName = 'eventSingle';
var EventSingleCtrl = (function () {
    function EventSingleCtrl(_$scope, _$stateParams, _$sce, _DataBus, _Util, _Events, _eventsMethods, _Activities, _Maps, _photoUploadFlow, _modalConfirm, _locale, _Meta, _User, _Modal, Datum) {
        this._$scope = _$scope;
        this._$stateParams = _$stateParams;
        this._$sce = _$sce;
        this._DataBus = _DataBus;
        this._Util = _Util;
        this._Events = _Events;
        this._eventsMethods = _eventsMethods;
        this._Activities = _Activities;
        this._Maps = _Maps;
        this._photoUploadFlow = _photoUploadFlow;
        this._modalConfirm = _modalConfirm;
        this._locale = _locale;
        this._Meta = _Meta;
        this._User = _User;
        this._Modal = _Modal;
        this.Datum = Datum;
        this.data = {
            trustedDescription: null
        };
        this._init();
        // Map Stack Init 
        this._mapInit();
        var context = this, listActions = {
            'single_attendEvent': function () {
                var objArgs = _$stateParams;
                return context.methods.toggleAttending({ eID: Datum.eID }, true, objArgs && objArgs.waitList).then(function () {
                    return _Util.location.stripQP(true);
                });
            }
        }, attendanceChange = this.attendanceChange.bind(this);
        _$scope.$watch('Event.data.imAttending', attendanceChange);
        _$scope.$watch('Event.data.imWaiting', attendanceChange);
        // Run any saved functions
        if (listActions[_$stateParams.action]) {
            listActions[_$stateParams.action](this._$stateParams);
        }
        // unbind listener upon death 
        _$scope.$on('$destroy', function () {
            _Activities.map.destroyStack();
            if (context._infoWindow && context._infoWindow.close) {
                context._infoWindow.close();
            }
        });
        this._$scope.$root.mustBeLoadedIn(3000);
    }
    EventSingleCtrl.prototype._init = function () {
        this.data.trustedDescription = this._$sce.trustAsHtml(this.Datum.description);
        this.meta = {
            name: this._locale.getString('events.NEW_EVENT', {
                event_name: this.Datum.name
            }),
            description: this._Meta.stripHTML(this.Datum.description),
            image: this.Datum.hasPhoto ? this._Meta.image(this.Datum.eID, 'events') : '',
            hostImg: this.Datum.indexType === 'event' && this._User.format.photoSrc(this.Datum.host),
        };
        this.methods = new this._eventsMethods({
            hooks: {
                toggleAttendingReject: function (eID, waitList) {
                    return {
                        action: 'single_attendEvent',
                        eID: eID,
                        args: "waitList:" + waitList
                    };
                }
            }
        }, [this.Datum]);
    };
    EventSingleCtrl.prototype._mapInit = function () {
        this._Activities.map.initStack(stackName, {
            disableCluster: true
        });
        this._Activities.map.setMarkers([this.Datum], {
            classType: 'event',
            replace: true,
            identifier: stackName
        });
        this._Maps.getInstance().center(15);
        // Load the address window 
        this.buildWindow();
    };
    EventSingleCtrl.prototype.attendanceChange = function (attending, prev) {
        if (typeof attending === 'boolean' && attending !== prev) {
            this.Datum.vars.attending = String(attending);
            this._Activities.updateMarkers(attending);
            if (this.Datum.venue.rsvpOnlyRadius) {
                this.Datum.__V = this.Datum.__V || _.cloneDeep(this.Datum.venue);
                if (attending) {
                    this.Datum.venue = _.cloneDeep(this.Datum.__V);
                }
                else {
                    this.Datum.venue.coords = this.Datum.marker.coords;
                }
                this.Datum.venue.hidden = !attending;
            }
            _.defer(this.buildWindow.bind(this));
        }
    };
    EventSingleCtrl.prototype.uploadPhotoFlow = function () {
        var publicID = String(this.Datum.eID);
        if (CP.site.prefix !== 'www') {
            publicID = CP.site.prefix + "/" + publicID;
        }
        return this._photoUploadFlow.activate({
            uploadPreset: 'events',
            onSuccess: this._photoUploadSuccess.bind(this),
            publicID: publicID,
            modal: {
                deactivate: this._photoUploadFlow.deactivate
            }
        });
    };
    EventSingleCtrl.prototype._photoUploadSuccess = function () {
        this._photoUploadFlow.deactivate();
        return this._Events.update({
            eID: this.Datum.eID,
            type: this.Datum.indexType,
            updates: {
                hasPhoto: true
            }
        })
            .then(function () {
            // a hack to trigger a expression update if there already was a photo
            this.Datum.hasPhoto = Math.random();
            this._$scope.$root.safeDigest(this._$scope);
        }.bind(this));
    };
    EventSingleCtrl.prototype.removePhoto = function () {
        var context = this;
        return this._locale.ready('common')
            .then(function () {
            var values = {
                title: context._locale.getString('common.R_U_SURE'),
                confirmText: context._locale.getString('common.REMOVE'),
                cancelClass: 'btn-white',
                confirmClass: 'btn-red'
            };
            return context._modalConfirm.activate(values);
        })
            .then(function (close) {
            return context._Events.update({
                eID: context.Datum.eID,
                type: context.Datum.indexType,
                updates: { hasPhoto: false }
            })
                .then(function () {
                context.Datum.hasPhoto = false;
                context._$scope.$root.safeDigest(context._$scope);
                return true;
            })["finally"](close);
        });
    };
    /**
     * @descriptions opens a modal which upon entering a message and submitting
     * sends it to the backend to notify all event attendees
     *
     * @returns {IPromise<object>}
     */
    EventSingleCtrl.prototype.notifyAttendees = function () {
        var Modal = this._Modal, DataBus = this._DataBus;
        return Modal.activate({
            template: 'views/event/templates/NotifyAttendeesTemplate.html',
            container: '.map-pane',
        })
            .then(function (vars) {
            return this._Events.notifyUsers({
                eID: this.Datum.eID,
                type: this.Datum.indexType,
                emailKey: 'NOTIFY_ATTENDEES',
                excludeCreator: true,
                data: {
                    message: vars.message,
                }
            });
        }.bind(this))
            .catch(function (err) {
            throw DataBus.emit('yapServerResponse', err);
        })
            .then(function (response) {
            Modal.deactivate();
            DataBus.emit('yapServerResponse', response);
            return true;
        });
    };
    return EventSingleCtrl;
})();
/**
 * Builds the google maps address window for this event
 */
EventSingleCtrl.prototype.buildWindow = _.debounce(function () {
    if (this._$scope.$root.size.xs) {
        return;
    }
    var context = this, instance = this._Maps.getInstance();
    if (this._infoWindow && this._infoWindow.close) {
        this._infoWindow.close();
    }
    this._Activities.map.buildWindow(this._Events.map.formatWindow.call(this._Events, this.Datum, { showDirections: true }))
        .then(function (_infoWindow) {
        context._infoWindow = _infoWindow;
        context._infoWindow.open(instance.rootObject());
    });
}, 100);
EventSingleCtrl.$inject = [
    '$scope',
    '$stateParams',
    '$sce',
    'DataBus',
    'Util',
    'Events',
    'eventsMethods',
    'Activities',
    'Maps',
    'photoUploadFlow',
    'modalConfirm',
    'locale',
    'Meta',
    'User',
    'ModalService',
    'Datum',
];
require('../../app.js').controller('EventSingleCtrl', EventSingleCtrl);
//# sourceMappingURL=Ctrl.js.map