/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

interface IMisc {
    currentForm:ng.IFormController
    edit:boolean
    title:string
    hideContent:boolean
    showDelete:boolean
    showUpdate:boolean
    processing:boolean
    updateLocField:number
    classLearn:boolean
}

interface IEventCreateData extends cp.event.IModifyVariableVars {
    currentStep:number
    validSteps:number[]
    disableVenue:boolean
}

interface IEventCreateScope extends cp.IScope {
    misc:IMisc
    data:IEventCreateData
}

var _autosaveInterval:number = 33 * 1000,
    _baseState:string =  'dashboard.eventCreate';

class EventCreateCtrl {
    public newEvent:cp.event.IEventBuild;
    public data:IEventCreateData;
    public updateAction:() => any;
    public steps:any[];

    private _mapSet:any;
    private _autoSaveTimer:WindowTimers;
    private _eventSections:string[];

    constructor(
        private _$scope:IEventCreateScope,
        private _$state:ng.ui.IStateService,
        private _DataBus:NodeJS.EventEmitter,
        private _Config:any,
        private _Venues:cp.venue.IService,
        private _Util:any,
        private _Events:cp.event.IService,
        private _locStorage:any,
        private _$interval:ng.IIntervalService,
        private _$q:ng.IQService,
        private _Maps:cp.map.IService,
        private _EditEvent:any,
        private _User:cp.user.IService,
        ClassLearn:any
) {
        var stateChangeListener = _$scope.$root.$on('$stateChangeStart', this._onStateChange.bind(this)),
            context = this,
            variables = _Events.modify.initVars();

        // Fields that go right into the final event as is
        this.newEvent = variables.newEvent;
        this.newEvent.aboutHost = _$scope.$root.stripHtml(_User.data.personal);

        // Represents variables that should be stored and hold fields
        // that need to be formatted to an event object
        this.data = <IEventCreateData>_.extend(variables.vars, {
            currentStep: null,
            validSteps: [],
            disableVenue: false,
        });

        // Represents temporary variables that should not be saved
        // to local storage
        _$scope.misc = {
            currentForm: null,
            edit: !!_EditEvent,
            title: _EditEvent ? 'events.EDIT' : 'events.CREATE_EVENT',
            hideContent: false,
            showDelete: null,
            showUpdate: null,
            processing: null,
            updateLocField: 0,
            classLearn: null,
        };

        this._classLearnCheck(ClassLearn);

        this.steps = [{
            route: 'basicInfo',
            title: 'events.BASIC_INFO'
        }, {
            route: 'languages',
            title: 'events.LANGUAGES'
        }, {
            route: 'details',
            title: 'common.DETAILS'
        }];

        this._eventSections = _.clone(_Events.modify.defaultSections);

        this._mapSet = {
            disableCluster: true,
            markers: [],
            _markers: null,
        };

        // A new map markers layer
        _Maps.getInstance().selectSet('createEvent', this._mapSet);

        this._loadCurrent()["finally"](function () {
            context.currentState();
            context._autoSave();
        });

        _$scope.$on('$destroy', function () {
            stateChangeListener();

            var instance = context._Maps.getInstance();

            if (instance) {
                instance.deselectSet();
            }

            if (context._autoSaveTimer) {
                context._$interval.cancel(context._autoSaveTimer);
            }
        });
    }

    /**
     * Called an route change start - If leaving the create event flow without hitting cancel
     * it will delete any saved data
     *
     * @param  {object} event  not needed
     * @param  {object} to     to state
     */
    _onStateChange(event, to) {
        if (!/create\.[a-zA-Z]+$/.test(to.name)) {
            return this._clearEvent();
        }
    }

    /**
     * Loads any persistent event data to prefill the forms
     *
     * @return {Promise}
     */
    _loadCurrent() {
        var context = this,
            deferred = this._$q.defer(),
            event = this._locStorage.get('event.create'),
            promise;

        // If we have an edit event
        if (this._EditEvent) {
            if (event && event.newEvent.eID === this._EditEvent.eID) {

                // If we were in the middle of editing an event load local storage
                promise = this._$q.when(event);
            } else {

                // Otherwise parse the event into something we can use
                promise = this._Events.modify.parseEdit(this._EditEvent, this._eventSections)
                    .then(function (data) {
                        context.data = <IEventCreateData>data.vars;
                        context.data.validSteps = [1, 1, 1];
                        return data;
                    });
            }
        } else if (event) {
            promise = this._$q.when(event);
        } else {
            return this._$q.reject();
        }

        promise.then(function (saved) {
            context.newEvent = saved.newEvent;
            var vars = context.data = <IEventCreateData>_.merge(context.data, saved.vars),
                instance;

            if (vars.event.date) {
                vars.event.date = new Date(vars.event.date);
            }

            if (vars.event.endDate) {
                vars.event.endDate = new Date(vars.event.endDate);
            }

            if (vars.location && (instance = context._Maps.getInstance())) {
                instance.createMarker(vars.location.coords, null, true).center(17);
            }

            if (vars.rsvpOnlyRadius) {
                context.triggerRadius();
            }

            return deferred.resolve();
        });

        return deferred.promise;
    }

    /**
     * Gets the current state index based on url
     */
    currentState() {
        // Go to first slide if we landed on the parent
        var curState = this._$state.current.name,
            index, state, step;

        if (curState === _baseState) {
            curState = _baseState + ".basicInfo";
            this._$state.transitionTo(curState, null, {
                location: true,
                inherit: true,
                notify: true
            });
        }

        step = _.isArray(state = curState.match(/[^.]+$/)) ? state[0] : '';

        // Get the index by matching it against the available steps
        _.each(this.steps, function (_step, _index) {
            if (step === _step.route) {
                index = _index;
                return false;
            }
        });

        if (this.disableStepAccessible(index)) {
            index = this.getNextAvailStep();
            this._$state.go(_baseState + "." + this.steps[index].route);
        }

        this.data.currentStep = index;
        this.steps[index].active = this.steps[index].accessible = true;
        this._updateAccessibleSteps();
    }

    setStepForm(Form) {
        var context = this;
        this._$scope.misc.currentForm = Form;

        // unlisten to any previous watches
        if (this._$scope.misc.formListener) {
            this._$scope.misc.formListener();
            this._$scope.misc.formListener = null;
        }
        return this._$scope.misc.formListener = this._$scope.$watch(
            'misc.currentForm.$valid',
            function (post, prev) {
                if (post !== prev) {
                    return context._$scope.$root.safeApply(function () {
                        var nextStep:any = context.steps[context.data.currentStep + 1];

                        context.updateCurrentValidity();

                        // Make next step appear as accessible
                        if (nextStep) {
                            nextStep.accessible = post;
                        }
                    });
                }
            });
    }

    /**
     * Updates the classes of the steps that are accessible
     */
    private _updateAccessibleSteps() {
        var i = 0,
            len = this.steps.length;

        for (;i < len; i++) {
            this.steps[i].accessible = true;

            if (!this.data.validSteps[i]) {
                break;
            }
        }
    }

    /**
     * Returns the next available step
     *
     * @return {number}  index of the step
     */
    getNextAvailStep() {
        var i, j, ref;
        for (i = j = 0, ref = this.steps.length - 1; 0 <= ref ? j <= ref :
            j >= ref; i = 0 <= ref ? ++j : --j) {
            if (!this.data.validSteps[i]) {
                return i;
            }
        }
    }

    /**
     * Whether to restrict going forward to the next step or not
     *
     * If update is passed, will also function as a "Go To Slide" method
     *
     * @param  {number}  index   The index we're trying to get to
     * @param  {Boolean} update  Whether to go to the next state also
     * @return {Boolean}         A truthy value forbids progressing
     */
    disableStepAccessible(index, update) {

        // Update current step if Form
        var i, j, ref;
        this.updateCurrentValidity();

        // Disallow if any of the steps before our desired step are invalid
        if (index) {
            for (i = j = 0, ref = index - 1; 0 <= ref ? j <= ref : j >=
                ref; i = 0 <= ref ? ++j : --j) {
                if (!this.data.validSteps[i]) {
                    return true;
                }
            }
        }
        if (update) {
            this._autoSave();
            this.data.currentStep = index;
            this._storeEvent();
            this._updateAccessibleSteps();
        }
        return false;
    }

    /**
     * Inverts disableStep accessible
     *
     * @param  {number}  index   The index we're trying to get to
     * @return {Boolean}         If progressing
     */
    goTo(index) {
        if (!this.disableStepAccessible(index, true)) {
            this._$state.go('^.' + this.steps[index].route);
            return true;
        }
        return false;
    }

    updateCurrentValidity() {
        return this._$scope.misc.currentForm &&
            (this.data.validSteps[this.data.currentStep] = this._$scope.misc.currentForm.$valid);
    }

    /**
     * Proceed to next step in the create event process
     */
    "continue"() {
        if (!this.disableStepAccessible(this.data.currentStep + 1, true)) {
            return this._$state.go('^.' + this.steps[this.data.currentStep]
                .route);
        }
    }

    /**
     * GoogleLocation directive on select callback
     *
     * @param  {object} location - location data
     * @param  {string} service - only service now is 'google'
     * @param  {boolean=} ignoreMarker - whether to not update the marker
     */
    locationSelect(location, service, ignoreMarker) {
        this.data.updateLocation = true;
        this.data.location = this._Venues.scrapePlace(location, service);
        this.data.disableVenue = false;

        if (!ignoreMarker) {
            this.setMarker(this.data.location.coords);
        }
    }

    setMarker(coords) {
        var instance = this._Maps.getInstance();
        if (instance) {
            return this._$scope.safeApply(function () {
                return instance.createMarker(coords, 'createEvent', true).center(15);
            });
        }
    }

    public triggerRadius() {
        var coords, curCenter, defaultRadius, location,
            context = this;

        defaultRadius = 500;
        location = this.data.location;
        coords = location.coords;
        this.setMarker(coords, this.data.rsvpOnlyRadius);

        if (!this.data.rsvpOnlyRadius) {
            this._mapSet.circle = null;
            this._mapSet.markers = this._mapSet._markers;
            this._mapSet._markers = null;
            return;
        }

        curCenter = (coords.lat.toFixed(6)) + ',' + (coords.lon.toFixed(6));
        this.newEvent.venue.rsvpOnlyRadius = this.newEvent.venue.rsvpOnlyRadius || defaultRadius;

        this._mapSet.circle = _.extend({
            center: this._Maps.normalizeCoords(location.coords),
            radius: this.newEvent.venue.rsvpOnlyRadius,
            events: {
                radius_changed: _.debounce(function (vars) {
                    var latlng = vars.getCenter().toUrlValue();

                    if (latlng !== curCenter) {
                        context.data.disableVenue = true;
                        context._Util.geo.latlng2Address(latlng)
                            .then(function (places) {
                                if (!places.length) { return; }

                                ++context._$scope.misc.updateLocField;
                                context.locationSelect(
                                    places[0],
                                    'google',
                                    true
                                );

                                // Update the coords of the hidden marker
                                return context._mapSet._markers[0].coords =
                                    context._Maps.normalizeCoords(context.data.location.coords);
                            });
                    }
                    return context.newEvent.venue.rsvpOnlyRadius =
                        vars.getRadius();
                }, 100)
            }
        }, this._Maps.circle.editable);

        this._mapSet._markers = this._mapSet.markers;
        this._mapSet.markers = [];

        this._Maps.getInstance().updateScope();
    }

    /**
     * Watches the skill levels and updates the checkbox based on the expr
     */
    public onSkillChange() {
        var skill = this.data.skill;
        return this.data.allSkills = skill.from.level === 1 && skill.to.level === 6;
    }

    /**
     * Clicking on the all skills checkbox updates the skill level directives
     */
    public toggleAllSkills() {
        var skill;
        if (this.data.allSkills) {
            skill = this.data.skill;
            skill.from.level = 1;
            return skill.to.level = 6;
        }
    }

    public cancelEvent() {
        this._clearEvent();
        this._$state.go(this._Config.routes.myActivities, null, { inherit: false });
        return null;
    }

    /**
     * Stores the current event data in local storage
     */
    _storeEvent() {
        return this._locStorage.set('event.create', {
            newEvent: this.newEvent,
            vars: this.data
        });
    }
    _clearEvent() {
        return this._locStorage.remove('event.create');
    }

    /**
     * Initiates the autosave.
     * Subsequent calls reset the interval
     */
    _autoSave() {
        var context = this;
        if (this._autoSaveTimer) {
            this._$interval.cancel(this._autoSaveTimer);
        }

        // Set autosave
        this._autoSaveTimer = this._$interval(function () {
            return context._storeEvent();
        }, _autosaveInterval);
    }

    /**
     * Prepares the event data for the server
     */
    createEvent() {
        var context = this,
            newEvent;

        this._$scope.misc.processing = true;
        this._DataBus.emit('progressLoader', { start: true });
        return this._Events.modify.formatEdit(this.newEvent, this._eventSections, this.data)
            .then(function (_newEvent) {
                newEvent = _newEvent;
                newEvent.created = context._Config.format.date.toString('basic_date_time_no_millis');
                return context._Events.create(_newEvent, context.data.location);
            })
            .then(function (event) {
                return context._onFinish(event.eID);
            })
            .catch(function (err) {
                context._$scope.misc.processing = false;
                throw context._DataBus.emit('serverResponse', err);
            });
    }

    /**
     * Formats the event for updating. If there are changed fields
     * will follow up with a `Do you want to notify the attendees` screen
     */
    updateEvent() {
        var context = this,
            eID:number = this._EditEvent.eID,
            eventUpdates = null;

        return this._Events.modify.formatEdit(this.newEvent, this._eventSections, this.data)
            .then(function (newEvent) {
                eventUpdates = context._Util.objectDifferences(context._EditEvent, newEvent);

                // If there's nothing to update then same as cancel
                if (!_.keys(eventUpdates, newEvent).length) {
                    throw context.cancelEvent();
                }

                eventUpdates.updated = context._Config.format.date.toString('basic_date_time_no_millis');

                return context.updateMessage({
                    eID: eID,
                    updates: eventUpdates
                }, newEvent);
            });
    }

    /**
     * Screen that a user sees upon updating an event that gives him the
     * option to notify users with an optional message
     *
     * @param  {object} updateObj - object ready for insertion as is
     * @return {Promise}
     */
    updateMessage(updateObj, newEvent) {
        var context = this;

        this._$scope.misc.hideContent = this._$scope.misc.showNotify = true;

        return this.updateAction = function (notify) {
            context._$scope.misc.processing = true;
            context._DataBus.emit('progressLoader', { start: true });

            updateObj.type = context.data.eventAsListing ? 'listing' : 'event';

            if (notify) {
                updateObj.notify = true;
                if (context._$scope.misc.notifyMessage) {
                    updateObj.notifyMessage = context._$scope.misc.notifyMessage;
                }
            }

            return context._Events.update(updateObj)
                .then(function () {
                    return context._onFinish(updateObj.eID);
                }, function (err) {
                    if (err) {
                        return context._DataBus.emit('serverResponse', err);
                    }
                })["finally"](function () {
                    context.updateAction = $.noop;
                    context._$scope.misc.processing = false;
                });
        };
    }

    _onFinish(eID) {
        this._clearEvent();
        this.newEvent = {};
        this.data = {};
        this._$state.go(this._Config.routes.myActivities, {
            action: this._Util.encodeActionArgs({
                action: 'view',
                objID: eID,
                type: 'event'
            })
        });
    }

    /**
     * @description Trigger's a confirmation modal which upon confirming
     * will trigger the event to be deleted
     */
    noWaitDelete(notify) {
        var context = this,
            response:any = null,
            deleteObject:any = {
                eID: this.newEvent.eID,
                notify: true,
                type: this.newEvent.indexType,
                notifyMessage: null,
            };

        if (notify && this._$scope.misc.notifyMessage) {
            deleteObject.notifyMessage = this._$scope.misc.notifyMessage;
        }

        return this._Events["delete"](deleteObject)
            .then(function (_response) {
                return response = _response;
            })
            .catch(function (err) {
                if (err.status === 404) {
                    return response = {
                        type: "updates.ERROR-NO_RESULTS",
                        response: 404
                    }
                }

                return response = err;
            })
            .finally(function () {
                // show notification
                context._DataBus.emit('yapServerResponse', response);

                // clear the event from localstorage
                context._clearEvent();

                // go to activities
                context._$state.go(context._Config.routes.myActivities);
            });
    }

    /**
     * @description if the passed in argument is not empty means we need to show the learn more
     * @param {object} ClassLearn
     * @private
     */
    private _classLearnCheck(ClassLearn:{module:any; name:string;}):void {
        if (!ClassLearn) { return; }

        var DataBus = this._DataBus,
            $scope = this._$scope,
            $container = $('#classLearn').append(ClassLearn.module(this._$scope));

        $scope.misc.classLearn = true;

        DataBus.once(`/feature/mark/${ClassLearn.name}`, function () {
            $scope.misc.classLearn = false;
            $container.empty();
            $scope.$root.safeDigest($scope);
        }.bind(this));
    }
}

/**
 * @description Since we can't easily use promises with user button clicks we have to
 * store the `resolve` callback in a closure variable that's defined in #updateMessage
 *
 * @type {Function}
 */
EventCreateCtrl.prototype.updateAction = $.noop;

EventCreateCtrl.$inject = [
    '$scope',
    '$state',
    'DataBus',
    'Config',
    'Venues',
    'Util',
    'Events',
    'localStorageService',
    '$interval',
    '$q',
    'Maps',
    'EditEvent',
    'User',
    'ClassLearn',
];

require('../../app.js').controller('EventCreateCtrl', EventCreateCtrl);
