/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../../app').controller('EventListCtrl', [
    '$q',
    '$scope',
    'Activities',
    'DataBus',
    'eventsMethods',
    'Util',
    'Maps',
    'localStorageService',
    'AdSlot',
    'Events',
    require('root/event/directives/ListCtrl')
]);
