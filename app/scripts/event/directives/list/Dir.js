/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var MODULE_TYPE = 'event', MODULE_NAME = MODULE_TYPE + 'List', _defaultSearch = {
    venues: true,
    what: MODULE_TYPE,
};
var AbstractListDir_1 = require('root/search/AbstractListDir');
require('../../../app').directive(MODULE_NAME, [
    'DataBus', '$compile', '$analytics', '$q', 'Util', '$templateCache',
    function (DataBus, $compile, $analytics, $q, Util, $templateCache) {
        var listDirective = new AbstractListDir_1.AbstractDirective({
            moduleName: MODULE_NAME,
            moduleType: MODULE_TYPE,
            itemKeyID: 'eID',
        }, _defaultSearch, DataBus, $analytics, $compile, $q, Util);
        return {
            restrict: 'E',
            replace: true,
            scope: {
                id: '@',
                options: '=?',
                onRefresh: '=?',
                zeroDataDirective: '@',
                stack: '=?',
                group: '=?',
                subType: '@',
            },
            template: $templateCache.get('views/event/directives/list/Template.html'),
            controller: 'EventListCtrl',
            link: {
                pre: listDirective.pre.bind(listDirective),
                post: listDirective.postCallback(function (scope) {
                    // If a user logs out, reset events to defaults
                    var authReformatFn = function () {
                        _.defer(function () {
                            scope.methods.reformatAll();
                            scope.$root.safeDigest(scope);
                        });
                    };
                    DataBus.on('/site/authChange', authReformatFn);
                    scope.$on('$destroy', function () {
                        DataBus.removeListener('/site/authChange', authReformatFn);
                    });
                })
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map