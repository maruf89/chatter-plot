/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

/**
 * TODO: Share this logic across mobile
 *
 * @module goingStripDirective
 * @ngdoc directive
 * @restrict E
 * @requires User
 * @requires $templateCache
 * @requires locale
 * @requires DataBus
 * @requires PeopleDisplay
 */

'use strict';

/**
 * @description gets translated to display the text that shows when you
 * hover over your own image. In english it says 'Me'
 * @type {string}
 */
var _filterOut, _formatUsers, avatarHeight, meText;

meText = '';


/**
 * @description the size (height) of each avatar including padding/margin.
 * Used to determine how many avatars we can fit on the screen
 * @type {number}
 */

avatarHeight = 50;


/**
 * @description formats an array of users for the going strip directive
 *
 * @private
 * @type {function}
 * @param {User} User
 * @param {object} scope
 * @param {object} userObj - mapping of {usersID: user} of the fetched users
 * @param {array<number>=} getIDs - if passed will append the userID's from here instead of scope.source
 * @param {array<object>} users - array of users
 */

_formatUsers = function (scope, userObj, getIDs) {
    var vars = scope.vars;
    getIDs = getIDs || scope.source;
    vars.users = _.reduce(getIDs, function (userArr, userID) {
        var user = userObj[userID];
        if (user) {
            userArr.push({
                fullName: user.fullName,
                picture: user.picture,
                userID: userID
            });
        }
        return userArr;
    }, vars.users || []);
    return vars.goingLength = vars.users.length;
};

_filterOut = function (vars, newIDs) {
    vars.users = _.filter(vars.users, function (user) {
        return ~newIDs.indexOf(user.userID);
    });
    return vars.goingLength = newIDs.length;
};

require('../../../app.js').directive('goingStrip', [
    'User', '$templateCache', 'locale', 'DataBus', 'PeopleDisplay',
    function (User, $templateCache, locale, DataBus, PeopleDisplay) {

        var _loadMeText = function () {
            return locale.ready('activities').then(function () {
                return meText = locale.getString('activities.ME');
            });
        };

        return {
            restrict: 'E',
            replace: true,
            scope: {
                source: '=',
                maximum: '@',
                waiting: '=',
                heightOffset: '@'
            },
            template: $templateCache.get('views/event/directives/goingStrip/Template.html'),
            link: {
                pre: function (scope) {
                    var calculateMaxAvatars, filterOutFn, formatFn, get,
                        heightOffset, requiredUserFields, vars;

                    if (!_.isArray(scope.source)) {
                        scope.source = [];
                    }

                    scope.vars = vars = {
                        users: [],
                        goingLength: 0,
                        limitTo: 1
                    };

                    heightOffset = p(scope.heightOffset);
                    heightOffset = isNaN(heightOffset) ? 0 : heightOffset;
                    formatFn = _.partial(_formatUsers, scope);
                    filterOutFn = _.partial(_filterOut, vars);
                    requiredUserFields = PeopleDisplay.getRequiredFields().concat(['picture']);

                    get = {
                        userIDs: scope.source,
                        _source: [
                            'picture',
                            'firstName',
                            'lastName',
                            'userID',
                            'details.gender'
                        ]
                    };

                    _loadMeText();

                    scope.$watch('source', function (post, pre) {
                        var getNew,
                            oLen = pre.length,
                            pLen = post.length;

                        if (pLen !== oLen) {
                            if (pLen < oLen) {

                                // if we need to remove users
                                return filterOutFn(post);
                            } else {

                                // otherwise we're adding users
                                getNew = post.slice(oLen - pLen);

                                // if it's me then add from current self
                                if (getNew.length === 1 && getNew[0] === User.data.userID) {
                                    vars.users = vars.users.concat(_.pick(User.data, get._source, [
                                        'picture',
                                        'details'
                                    ]));
                                    return formatFn(vars.users);
                                }

                                User.cacheGet(getNew, requiredUserFields).then(function (userObj) {
                                    formatFn(userObj, getNew);
                                });
                            }
                        }
                    }, true);

                    /**
                     * Calculates how many avatar photos we can show
                     */
                    calculateMaxAvatars = function () {
                        var global = CP.Global.vars,
                            usableHeight = global.bodyHeight - global.navHeight - 150 - heightOffset;

                        vars.limitTo = Math.floor(usableHeight / avatarHeight);
                    };

                    calculateMaxAvatars();
                    if (scope.source.length) {
                        User.cacheGet(scope.source, requiredUserFields).then(formatFn);
                    }

                    vars.sections = [{
                        title: 'activities.PPL_ATTENDING',
                        userIDs: scope.source
                    }, {
                        title: 'activities.PPL_WAITLIST',
                        userIDs: scope.waiting,
                        hideEmpty: true
                    }];

                    vars.initModal = function () {
                        PeopleDisplay.activate({
                            sections: vars.sections,
                            category: 'Attending',
                        });
                    };
                }
            }
        };
    }
]);
