/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../../app').directive('teachClassLearn', [
    'User', '$templateCache', 'NewFeature',
    function (User, $templateCache, NewFeature) {
        var learnFeature = new NewFeature('teacherCampaign'), continueAction = function () {
            learnFeature.markFeature();
        };
        return {
            restrict: 'E',
            replace: true,
            scope: {
                disableButtons: '@',
            },
            template: $templateCache.get('views/event/directives/classLearn/Template.html'),
            link: {
                post: function (scope, iElem) {
                    iElem.on('click', '.continue-action', continueAction);
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map