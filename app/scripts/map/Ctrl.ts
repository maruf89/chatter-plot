/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

interface IScope extends cp.IScope {
    map:any
    clusterOptions:any
    clusterEvents:any
}

/**
 * Centers the map with the passed in coords.
 *
 * @private
 * @memberof MapCtrl
 * @param  {object} coords      Coordinates
 * coords @option {Float} lat   Latitude
 * coords @option {Float} lon   Longitude
 */
var MapCtrl, _markerSet, _markerSetBase, _setHistory,
    mapControlCtrl,

_centerMap = _.debounce(function (coords, zoom) {
    var lat = coords.lat || coords.latitude,
        lon = coords.lon || coords.lng || coords.longitude,
        self = this;

    if (!lat || !lon) {
        return false;
    }

    this._GoogleMapApi.then(function () {
        var root = self.rootObject(),
            LatLng;

        // Check if the passed in object is a google lat lng object already
        // Don't move this in the beginning in case the API hasn't loaded - will throw error
        if (coords instanceof google.maps.LatLng) {
            LatLng = coords;
            lat = coords.lat();
            lon = coords.lng();
        } else {
            LatLng = new google.maps.LatLng(lat, lon);
        }

        self._$scope.map.center.latitude = lat;
        self._$scope.map.center.longitude = lon;
        root.setCenter(LatLng);

        if (zoom && root.getZoom() !== zoom) {
            self.awaitZoomCallback();

            self._$scope.map.zoom = zoom;
            root.setZoom(zoom);
        }
    });

    this._defaultCurLocation = false;

    return this;
}, 350);


/**
 * @description a default stack object as described in the README under Maps -> Search Stacks
 * @private
 * @var {MarkerSetObject} base
 * @var {array<marker>} base.markers - stores visible map markers
 * @var {object} base.events - map events specific to the layer
 * @var {string} base.name - how the marker is identified by
 */
_markerSetBase = {
    markers: [],
    events: {},
    name: null
};


/**
 * @description An object containing references to each marker set like { <name>: MarkerSetObject }
 *
 * TODO: If/when we add support for multiple maps, this may have to change
 *
 * @private
 * @var {object}
 */
_markerSet = {
    "default": _.clone(_markerSetBase)
};


/**
 * @description Whenever a new markerSet is selected, it's value is pushed on here
 * so we can go back (and forward maybe?)
 *
 * TODO: If/when we add support for multiple maps, this WILL have to change (removed from global scope)
 *
 * @var {array<MarkerSetObject>}
 */
_setHistory = [];


/**
 * @class
 * @classdesc Manages everything with a single map instance including marker/event managing & styles
 */
MapCtrl = function (
    GoogleMapApi:any,
    $q:ng.IQService,
    Config:any,
    $scope:IScope,
    Maps:any,
    Util:any,
    DataBus:any
) {
    this._GoogleMapApi = GoogleMapApi;
    this._$q = $q;
    this._Config = Config;
    this._$scope = $scope;
    this._Maps = Maps;
    this._Util = Util;

    var onDestroy,
        context = this;

    /**
     * @name MapCtrl._$scope
     * @inner {object}
     */

    // Whether to update with the current geolocation when loads 
    this._defaultCurLocation = true;

    /**
     * Current location Google Maps marker control
     * @type {object}
     */
    this._currentLocation = {};
    this.mapEvents = {};

    /**
     * Will hold reference to the map object
     * @type {object}
     */
    this._control = {};
    this._markerControl = {};
    this._curLocation = null;
    this.map = $scope.map = _.extend(Maps.getDefaults('map'), {
        loaded: false,
        control: this._control,
        markerControl: this._markerControl,
        events: this.mapEvents,
        windowOptions: {
            show: false
        }
    });

    /**
     * @description styling the markers as defined {@link http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclustererplus/docs/reference.html#ClusterIconStyle}
     * @type {object}
     */
    this.clusterOptions = $scope.clusterOptions = this._Maps.getDefaults('clusterOpts');

    this.data = {
        events: {}
    };

    $scope.clusterEvents = {
        click: function (cluster, clusterModel) {
            if (context.currentSet.events.clusterClick) {
                return context.currentSet.events.clusterClick(cluster, clusterModel);
            }
        },
        clusteringbegin: DataBus.emit.bind(DataBus, 'progressLoader', {
            start: true,
            key: 'cluster'
        }),
        clusteringend: DataBus.emit.bind(DataBus, 'progressLoader', {
            key: 'cluster'
        })
    };

    // Start watching the maps loaded state from the get-go
    this.loaded();
    this._deferredLoading = [];

    /**
     * @description an array of functions to call when destroyed
     * @type {array<function>}
     * @private
     */
    this._onDestroyFn = [];

    // Set instance of this to be reachable by other sections of the site 
    onDestroy = Maps.setInstance(this);
    $scope.$on('$destroy', function () {
        context._onDestroy();
        onDestroy();
    });

    GoogleMapApi.then(function () {
        return context.rootObject.addCallback;
    });
};

MapCtrl.prototype = _.extend(MapCtrl.prototype, {
    /**
     * @description returns a promise that's fired when the map & controls have finished loading
     * @name MapCtrl#loaded
     * @returns {Promise<MapCtrl>} instance of self
     */
    loaded: function ():ng.IPromise<cp.map.IMapCtrl> {
        if (this.map.loaded) {
            return this._$q.when(this);
        }

        var self = this,
            deferred = this._$q.defer();

        if (this.map.loading) {
            this._deferredLoading.push(deferred);
        } else {
            this.map.loading = true;

            this._$q.all([
                this._GoogleMapApi,
                this.rootObjPromise()
            ]).then(function (args) {
                var map = args[0],
                    rootObj = args[1];

                map.event.addListenerOnce(rootObj, 'idle', function () {
                    // Defer until after the controls have loaded as well
                    _.defer(function () {
                        self.map.loaded = true;
                        self.ready = function () {
                            return self._$q.when(self);
                        };

                        _.each(self._deferredLoading, function (_deferred:ng.IDeferred<any>) {
                            _deferred.resolve(self);
                        });

                        self._deferredLoading = null;

                        deferred.resolve(self);
                    });
                });
            });
        }

        return deferred.promise;
    },

    /**
     * @description Centers the map.
     * If called with init, will grab the users current location and
     * use that unless another call to this method is made with actual coordinates
     * 
     * @param {object} coords - Coordinates
     * @param {float} coords.lat - Latitude
     * @param {float} coords.lon - Longitude
     * @param {number=} zoom - Map zoom level
     * @param {boolean=} init - Whether to initate with users current location
     */
    centerMap: function (coords:cp.map.ILocation, zoom?:number, init?:boolean):cp.map.IMapCtrl {
        if (init) {
            return this._defaultCurLocation && this.getLocation()
                .then(function (coords) {
                    if (!this._defaultCurLocation) {
                        return false;
                    }
                    return _centerMap.call(this, coords, zoom);
                }.bind(this));
        }

        _centerMap.call(this, coords, zoom);

        return this;
    },

    setCurrentLocation: function (coords, opts) {
        this._currentLocation = {
            latitude: coords.lat || coords.latitude,
            longitude: coords.lon || coords.longitude,
            icon: this._Config.map.icon.home
        };
        if (opts.replace) {
            this.createMarker(this._currentLocation, null, true);
        }
        if (opts) {
            return _.extend(this._currentLocation, opts);
        }
    },

    getCurrentLocation: function () {
        return this._currentLocation;
    },

    /**
     * @description synchronous request to get the google maps instance object
     * @returns {GMap}
     */
    rootObject: function () {
        return this._control.getGMap();
    },

    /**
     * @description asynchronous version of {@link MapCtrl#rootObject}
     * @returns {Promise<GMap>}
     */
    rootObjPromise: function () {
        var self = this,
            deferred = this._$q.defer(),
            tempCheck = function () {
                if (typeof self._control.getGMap !== 'function') {
                    return _.delay(tempCheck, 50);
                }
                return deferred.resolve(self.rootObject());
            };

        this._GoogleMapApi.then(tempCheck);
        return deferred.promise;
    },

    getLocation: function (force) {
        var self;
        self = this;
        if (!force && this.curLocation) {
            return this._$q.when(this.curLocation);
        }
        return this._Util.geo.getCurrentLocation()
            .then(function (coords) {
                return self._curLocation = coords;
            });
    },

    /**
     * Refreshes the map
     */
    refresh: function () {
        return this._control.refresh();
    },

    /**
     * @description Centers the map to show all of the current markers
     *
     * !IMPORTANT - this method triggers any `zoom` listeners bound to the map
     *
     * @name MapCtrl#center
     */
    center: function (zoom) {
        var self = this,
            length = this.currentSet.markers.length;

        this.data.noZoomCb = true;

        if (!length) {
            return this;
        }
        if (length === 1) {
            return this.centerMap(this.currentSet.markers[0].coords,
                zoom);
        }

        this._defaultCurLocation = false;
        this._GoogleMapApi.then(function () {
            var bounds = new google.maps.LatLngBounds(),
                markers = self.currentSet.markers,
                i = 0,
                times = Math.min(length, 100),
                marker;

            for (;i < times; i++) {
                marker = markers[i];
                bounds.extend(new google.maps.LatLng(marker.coords.latitude, marker.coords.longitude));
            }

            self.rootObject().fitBounds(bounds);

            _.defer(function () {
                _.defer(function () {
                    self.data.noZoomCb = false;
                });
            });
        });

        return this;
    },

    /**
     * @description Adds already formatted markers to the map
     * @param markers
     * @param identifier
     * @returns {MapCtrl}
     */
    pushMarkers: function (markers:cp.map.IPreMarker, identifier?:string):cp.map.IMapCtrl {
        var set = identifier ? _markerSet[identifier] : this.currentSet;
        set.markers = set.markers.concat(markers);
        this._$scope.$root.safeDigest(this._$scope);
        return this;
    },

    /**
     * @description Clears all of the markers & shapes (circles) of a set
     * @param  {string=} identifier  If not passed, will empty the current set
     */
    clearMarkers: function (identifier?:string):void {
        var set = identifier ? _markerSet[identifier] : this.currentSet;
        set.markers.length = 0;
        set.circle = null;

        if (set.hidden) {
            set.hidden.length = 0;
        }
    },

    /**
     * @description Accepts a set of unformatted marker data -> formats it -> adds to map
     * @param markers
     * @param identifier
     * @param replace
     * @returns {markers}
     */
    createMarkers: function (markers, identifier, replace) {
        if (replace) {
            // empty the markers 
            (identifier ? _markerSet[identifier] : this.currentSet).markers.length = 0;
        }

        _.each(markers, function (marker) {
            this.createMarker(marker, identifier);
        }, this);

        return markers;
    },

    /**
     * Adds any marker from an object that has at least latitude and longitude set
     * 
     * @param {object} marker       unformatted marker object
     * @param {string=} identifier  the set to add to
     * @param {boolean=} replace
     * @return {MapCtrl}             reference to self for chaining
     */
    createMarker: function (marker:cp.map.IMarker, identifier?:string, replace?:boolean):cp.map.IMapCtrl {
        var set = identifier ? _markerSet[identifier] : this.currentSet;
        if (replace) {
            set.markers.length = 0;
        }
        set.markers.push(this.formatMarker(marker));
        return this;
    },

    formatMarker: function (_marker) {
        var marker = {
            coords: {
                latitude: _marker.lat || _marker.latitude,
                longitude: _marker.lon || _marker.longitude
            },
            icon: _marker.icon || this._Config.map.icon.xs,
            data: _marker.data,
            id:null,
            model: _marker.model || {},
        };

        marker.id = _marker.vID || _marker.id || (marker.coords.latitude + ',' + marker.coords.longitude);

        return marker;
    },

    getWindow: function () {
        return this._$scope.map.windowOptions;
    },

    /**
     * Calls the map dragend callback with the map/self arguments
     *
     * @callback
     * @param  {function} cb - callback function
     */
    _onMapMove: _.debounce(function (cb) {
        var callback, checks, map;
        if (this.data.dragging) {
            return false;
        }
        map = this.rootObject();
        callback = _.partial(cb, map, this);

        // Check to see if the map move is listening 
        if (_.isArray(checks = this.data.mapMoveChecks) && checks.length) {

            // test the Control to see if autosearch is active, if not send the callback of the listener 
            if (!_.every(checks, function (check:(map:any, callback?:() => void) => boolean) {
                    return check(map, callback);
                })
            ) {
                return false;
            }
        }

        // If all green then call the passed in dragEnd callback 
        return callback();
    }, 1500),

    _onDragEnd: function (callback) {
        this.data.dragging = false;
        return this._onMapMove(callback);
    },

    _onDragStart: function () {
        return this.data.dragging = true;
    },

    _onZoomDragChange: function (callback) {
        if (!this.data.noZoomCb && callback) {
            return this._onMapMove(callback);
        }
    },

    awaitZoomCallback: function () {
        this.data.noZoomCb = true;
        this._Util.deferX(function () {
            this.data.noZoomCb = false;
        }.bind(this), 3);
    },

    /**
     * @description Updates the map instance initializing/removing map listeners depending
     * on the current set's properties
     * This is where the map move search is bound if the current set contains a `moveSearch` property that's a function
     * @private
     */
    _initSet: function () {
        var self = this,
            set = this.currentSet;

        return this.rootObjPromise()
            .then(function (map) {
                var _events = self.data.events;
                if (typeof set.moveSearch === 'function') {

                    // bind listeners to the map 
                    _events.dragstart =
                        google.maps.event.addListener(map, 'dragstart', self._onDragStart.bind(self));

                    _events.dragend =
                        google.maps.event.addListener(map, 'dragend', self._onDragEnd.bind(self, set.moveSearch));

                    _events.zoom_changed =
                        google.maps.event.addListener(map, 'zoom_changed', self._onZoomDragChange.bind(self, set.moveSearch));

                    // set some temporary data (this may not be the best place for this) 
                    self.data.doDrag = true;
                    self.data.mapMoveChecks = [];
                } else {
                    self._unbindDataEvents();
                }

                // Check if clustering is disabled 
                if (set.disableCluster) {
                    return self.clusterOptions.minimumClusterSize = self._Maps.defaults.clusterOpts.batchSize + 1;
                } else {
                    return self.clusterOptions.minimumClusterSize = self._Maps.defaults.clusterOpts.minimumClusterSize;
                }
            });
    },

    /**
     * @description removes all map move search events from the map
     * @private
     */
    _unbindDataEvents: function () {
        var _events = this.data.events;
        return _.each(['drag', 'dragstart', 'dragend', 'zoom_changed'],
            function (what) {
                if (_events[what]) {
                    google.maps.event.removeListener(_events[what]);
                    return delete _events[what];
                }
            });
    },

    /**
     * Either creates or loads a markers set/stack which has its own standalone
     * markers and events
     * 
     * @param  {string} identifier - name of the Set
     * @param  {object=} opts - contains events for the marker set
     * @param  {boolean=} forceOpts - whether to force load the new options
     * @return {number} history state length
     */
    selectSet: function (identifier?:string, opts?:any, forceOpts?:boolean):void {
        var historyLength = _setHistory.length,
            cur;

        if (this.currentSet && this.currentSet.name && this.currentSet.name !== identifier) {
            _setHistory.push(this.currentSet.name);
        }

        cur = this.currentSet = _markerSet[identifier];

        if (!cur) {
            cur = this.currentSet = (_markerSet[identifier] = _.cloneDeep(_markerSetBase));
        }
        if ((opts && !cur.opts) || forceOpts) {
            this.currentSet = _markerSet[identifier] = _.defaults(opts, cur);
            cur.opts = true;
        }
        this.currentSet.name = identifier;
        this._initSet();

        this._$scope.$root.safeDigest(this._$scope);
    },

    getSet: function (identifier:string):cp.map.IMarkerSet {
        return _markerSet[identifier] || this.currentSet;
    },

    setHistory: function () {
        return _.clone(_setHistory);
    },

    deselectSet: function (identifier?:string):void {

        if (identifier) {
            if (!this.currentSet || this.currentSet.name !== identifier) {
                return;
            }
        }

        if (this.currentSet = _markerSet[_setHistory.pop()]) {
            this._initSet();
        }

        this._$scope.$root.safeDigest(this._$scope);
    },

    centerHidden: function (_id, identifier, zoom) {
        var set;
        set = identifier ? _markerSet[identifier] : this.currentSet;
        return _.each(set.markers, function (marker:any) {
            if (String(marker._id) === String(_id)) {
                this.centerMap(marker.coords, zoom);
                return false;
            }
        }, this);
    },

    /**
     * @description Allows linking of otherwise non-reachable functions on the current instance object
     * @name MapCtrl#linkMethod
     * @see MapCtrl#callMethod
     * @see MapCtrl#unlinkMethod
     * @param {string} name - name of the top level object
     * @param {string} action - the action pertaining to the name
     * @param {function} fn - reference to the function
     */
    linkMethod: function (name, action, fn) {
        if (!this.controls) {
            this.controls = {};
        }

        this.controls[name] = this.controls[name] || {};
        return this.controls[name][action] = fn;
    },

    /**
     * @description calls a stored method
     * @name MapCtrl#callMethod
     * @see MapCtrl#linkMethod
     * @see MapCtrl#unlinkMethod
     * @param {string} name - name of the top level object
     * @param {string} action - the action pertaining to the name
     * @param {*} params - depends on the function
     * @returns {(function|null|undefined)}
     */
    callMethod: function (name, action, params):void {
        var fn;
        if (this.controls && this.controls[name] && (fn = this.controls[name][action])) {
            fn(params);
        }
    },

    /**
     * @description removes a method link
     * @name MapCtrl#unlinkMethod
     * @see MapCtrl#callMethod
     * @see MapCtrl#unlinkMethod
     * @param {string} name - name of the top level object
     * @param {string} action - the action pertaining to the name
     * @returns {null}
     */
    unlinkMethod: function (name, action):void {
        var controlName;
        if (this.controls && (controlName = this.controls[name])) {
            return controlName[action] = null;
        }
    },

    /**
     * @description runs safeDigest on the map scope
     */
    updateScope: function ():void {
        this._$scope.$root.safeDigest(this._$scope);
    },

    addOnDestroyFn: function (destroyCB) {
        this._onDestroyFn.push(destroyCB);
    },

    _onDestroy: function ():void {
        this._unbindDataEvents();
        _.each(this._onDestroyFn, function (destroyCB:Function):void {
            destroyCB();
        });

        this._onDestroyFn = null;
    }
});

MapCtrl.$inject = [
    'uiGmapGoogleMapApi',
    '$q',
    'Config',
    '$scope',
    'Maps',
    'Util',
    'DataBus',
];

import {MapControlCtrl} from 'root/map/MapControlCtrl';

require('../app')
    .controller('MapCtrl', MapCtrl)
    .controller('mapControlCtrl', [
        '$scope',
        '$analytics',
        'Maps',
        'DataBus',
        MapControlCtrl,
    ]);
