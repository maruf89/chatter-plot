/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var Maps,

    MapStyle = require('root/config/mapStyle.json'),

    _instance = null;


/**
 * @ngdoc service
 * @class
 * @classdesc Provides defaults for instantiating a google map instance as well as utility functions specific to
 * google maps and angular google maps
 */
Maps = function (uiGmapGoogleMapApi, $q, Config) {
    this._GoogleMapApi = uiGmapGoogleMapApi;
    this._$q = $q;

    // shortcut for defining the clusterOptions styles 
    var _icon = Config.map.icon;

    this.defaults = {
        mapMoveSearch: true,
        map: {
            zoom: 15,
            center: {
                latitude: 52.520645,
                longitude: 13.409779
            },
            controls: {},
            options: {
                streetViewControl: false,
                panControl: false,
                maxZoom: 19,
                minZoom: 3,
                styles: MapStyle,
                disableDefaultUI: true
            },
            dragging: true,
            bounds: {},
            fitMap: false
        },
        clusterOpts: {
            minimumClusterSize: 2,
            clusterClass: 'map-cluster',
            batchSize: 500,
            averageCenter: true,
            maxZoom: 15,

            /**
             * @description The different icons available for the cluster marker library to use
             * @property {Array<object>} styles
             */
            styles: [{
                url: _icon.xs,
                textColor: '#FFF',
                width: 45,
                height: 45
            }, {
                url: _icon.sm,
                textColor: '#FFF',
                width: 40,
                height: 40
            }, {
                url: _icon.md,
                textColor: '#FFF',
                width: 50,
                height: 50
            }, {
                url: _icon.lg,
                textColor: '#FFF',
                width: 60,
                height: 60
            }],

            /**
             * @description This method devises which icons to use from {@link MapCtrl.$scope.clusterOptions.styles}
             * @memberof MapCtrl.$scope.clusterOptions
             * @name calculator
             *
             * @param {PropMap} markers - object containing references to the markers and `length` property
             * @param {number} styles - how many icons are available
             * @return {object} containing {{string} text, {number} index, {string} title}
             */
            calculator: function (markers, styles) {
                var index, length = markers.length;
                if (length === 1) {
                    index = 1;
                } else if (length <= 10) {
                    index = 2;
                } else if (length <= 100) {
                    index = 3;
                } else {
                    index = 4;
                }
                return {
                    text: length,
                    index: index,
                    title: ''
                };
            }
        }
    };

    this.circle = {
        nonEditable: {
            fill: {
                color: '#15d994',
                opacity: .3
            },
            stroke: {
                color: '#006163',
                weight: 2,
                opacity: .85
            }
        },
        editable: null
    };

    this.circle.editable = _.extend({
        draggable: false,
        clickable: true,
        geodesic: true,
        editable: true
    }, this.circle.nonEditable);


    // Used to figure out cur location. Will be true if getting location from IP address 
    this.locationInaccurate = null;

    this._init();
};

Maps.prototype = _.extend(Maps.prototype, {
    _init: function () {
        var self = this;

        this.ready = [];
        this.onReady = function () {
            var deferred = self._$q.defer();
            self.ready.push(deferred);
            return deferred.promise;
        };

        // Load the infoBox 
        return this._GoogleMapApi.then(function () {
            return $.getScript('/third_party/google_maps_infobox.min.js',
                function () {
                    self.onReady = self._$q.when;
                    _.each(self.ready, function (promise) {
                        return promise.resolve();
                    });
                    return self.ready = null;
                });
        });
    },

    getDefaults: function (what) {
        return _.cloneDeep(this.defaults[what]);
    },

    setInstance: function (instance) {
        if (_instance) {
            throw new Error('Cannot set more than one instance of Map');
        }
        _instance = instance;
        return function () {
            return _instance = null;
        };
    },

    getInstance: function () {
        return _instance;
    },

    normalizeCoords: function (coords:any, elastic:boolean, c?:any):any {
        if (elastic) {
            c = {
                lat: coords.lat || coords.latitude,
                lon: coords.lon || coords.lng || coords.longitude
            };
        } else {
            c = {
                latitude: coords.lat || coords.latitude,
                longitude: coords.lon || coords.lng || coords.longitude
            };
        }

        if (coords.zoom) {
            c.zoom = coords.zoom;
        }

        return c;
    }
});

Maps.$inject = [
    'uiGmapGoogleMapApi',
    '$q',
    'Config',
];

require('../app').service('Maps', Maps);
