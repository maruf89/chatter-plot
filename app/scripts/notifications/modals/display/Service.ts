/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var self = null,

NotificationModal = function () {
    self = this;
    _.each(arguments, (function (val, index) {
        return this[NotificationModal.$inject[index]] = val;
    }), this);
    return self;
};

NotificationModal.prototype = _.extend(NotificationModal.prototype, {
    activate: function (options, force) {
        var deactivate, localScope, modal;
        if (!force && this.modal && this.modal.active()) {
            return this.deactivate();
        }
        this.modal = modal = this.btfModal({
            template: this.$templateCache.get(
                'views/notifications/modals/display/Template.html'
            )
        });
        if (modal.active()) {
            return false;
        }
        deactivate = this.deactivate.bind(this);
        localScope = {
            vars: {
                loadMore: this.loadMore.bind(this),
                deactivate: deactivate,
                notifications: {}
            }
        };

        // Remove the listener if we already created one 
        this.unregisterDestroy && this.unregisterDestroy();

        // and rebind 
        this.unregisterDestroy = this.$rootScope.$on(
            '$stateChangeSuccess', deactivate);
        this.Notifications.fetch({
                size: 10
            })
            .then(function (notifications) {
                return self.$rootScope.safeApply(function () {
                    return localScope.vars.notifications =
                        self.notifications = notifications;
                });
            });
        if (typeof options === 'object') {
            _.extend(localScope.vars, options);
        }
        return modal.activate(localScope);
    },
    deactivate: function () {
        this.modal && this.modal.deactivate();
        this.$rootScope.$on('$stateChangeSuccess', this.deactivate);
        this.unregisterDestroy && this.unregisterDestroy();
        return this.unregisterDestroy = null;
    },
    loadMore: function () {
        var notifs;
        notifs = this.notifications;
        return this.Notifications.fetch({
            from: notifs.loaded,
            size: 10
        });
    }
});

NotificationModal.$inject = ['btfModal', '$rootScope', '$templateCache',
    'Notifications'
];

require('../../../app').service('NotificationModal', NotificationModal);
