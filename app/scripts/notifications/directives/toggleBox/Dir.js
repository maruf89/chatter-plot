/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var defaultPostSize = 6;
require('../../../app').directive('notificationToggleBox', [
    'Notifications', 'DataBus', 'Util', '$templateCache', 'User',
    'NotificationModal',
    function (Notifications, DataBus, Util, $templateCache, User, NotificationModal) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                fetchSize: '@',
                pos: '@'
            },
            template: $templateCache.get('views/notifications/directives/toggleBox/Template.html'),
            link: {
                pre: function (scope, iAttrs) {
                    /**
                     * @name $scope.vars
                     * @property {object}
                     * @property {boolean} $scope.vars.visible - whether the notifications box is open
                     * @property {array} $scope.vars.sorted - array of notifs.sorted with length equal to fetchSize
                     * @property {boolean} $scope.vars.seeAll - whether to show the see all button
                     * @property {number} $scope.vars.unread - how many unread notifications exist in this set
                     * @property {object} $scope.vars.user - current user's data
                     * @property {number} $scope.vars.fetchSize - how many notifications to fetch (default: {@link defaultPostSize})
                     */
                    return scope.vars = {
                        visible: false,
                        sorted: null,
                        seeAll: null,
                        unread: null,
                        user: User.data,
                        fetchSize: iAttrs.fetchSize ? p(scope.fetchSize) : defaultPostSize
                    };
                },
                post: function (scope, iElem) {
                    var determineUnreadLength, onAuthChange, updateSet, updateUnread, vars;
                    vars = scope.vars;
                    updateSet = function (notifications) {
                        vars.sorted = notifications.sorted.slice(0, vars.fetchSize);
                        vars.seeAll = notifications.total > vars.fetchSize;
                        determineUnreadLength(notifications);
                        return scope.$root.safeDigest(scope);
                    };
                    /**
                     * @name determineUnreadLength
                     * @type {function}
                     * @description Calculate how many unread notifications exist in this set
                     *
                     * @callback
                     * @param {array<object>} notifications - if string, must be notification ID
                     * else if object, must contain 'nID' property
                     */
                    determineUnreadLength = function (notifications) {
                        vars.unread = _.filter(vars.sorted, function (notification) {
                            // return only the notifications that exist in the `unread` array
                            return notifications.unread.indexOf(notification.nID) !== -1;
                        });
                        return scope.$root.safeDigest(scope);
                    };
                    /**
                     * @name updateUnread
                     * @type {function}
                     * @description Calculates if any of the passed in read notifications match our current
                     * notifications, and marks them as unread
                     *
                     * @callback
                     * @param {array<object>} notifications - notifications
                     * else if object, must contain 'nID' property
                     */
                    updateUnread = function (nIDs) {
                        vars.unread = _.filter(vars.unread, function (notification) {
                            return nIDs.indexOf(notification
                                .nID) === -1;
                        });
                        return scope.$root.safeDigest(scope);
                    };
                    vars.hideBox = function () {
                        vars.visible = false;
                        return scope.$root.safeDigest(scope);
                    };
                    // Displays the notification box and pings the server that these have been read
                    vars.showNotifications = function () {
                        if (User.data.unreadNotification) {
                            Notifications.notificationsViewed();
                        }
                        vars.visible = !vars.visible;
                        return scope.$root.safeDigest(scope);
                    };
                    // Opens a notifications modal
                    vars.notificationsModal = NotificationModal.activate
                        .bind(NotificationModal);
                    Notifications.on('/updated', updateSet);
                    Notifications.on('/mark/read', updateUnread);
                    /**
                     * @ngdoc function
                     * @description on Login: populate the notifications
                     *              on Logout: hide the box
                     * @param {boolean} authenticated - whether the user is authenticated
                     */
                    onAuthChange = function (authenticated) {
                        if (authenticated) {
                            scope.vars.user = User.data;
                            return Notifications.fetch({
                                size: vars.fetchSize
                            })
                                .then(updateSet);
                        }
                        else {
                            vars.seeAll = vars.visible = false;
                            return vars.sorted = vars.user = null;
                        }
                    };
                    DataBus.on('/site/authChange', onAuthChange);
                    // upon opening/closing the notification button
                    iElem.on('click', '.btn-toggle', function (event) {
                        event.stopImmediatePropagation();
                        return vars.showNotifications();
                    });
                    // Hide tho notifications when one is clicked on
                    iElem.on('click', '.note-link', vars.hideBox);
                    // upon clicking off the button, hide the window if it's still open
                    CP.Cache.$document.on('click.userNotifications', function (event) {
                        if (vars.visible && (event.target !==
                            iElem[0] && !iElem[0].contains(event.target))) {
                            vars.showNotifications();
                            return scope.$root.safeDigest(scope);
                        }
                    });
                    return scope.$on('$destroy', function () {
                        DataBus.removeListener('/site/authChange', onAuthChange);
                        CP.Cache.$document.off('click.userNotifications');
                        Notifications.removeListener('/updated', updateSet);
                        return Notifications.removeListener('/mark/read', updateUnread);
                    });
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map