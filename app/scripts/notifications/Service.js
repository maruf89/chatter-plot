/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var Service_1 = require('root/notification/Service');
require('../app').service('Notifications', [
    'DataBus',
    '$q',
    'SocketIo',
    'locale',
    'User',
    'Config',
    '$state',
    Service_1.Notifications,
]);
//# sourceMappingURL=Service.js.map