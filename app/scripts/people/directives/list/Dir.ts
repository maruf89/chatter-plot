/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var MODULE_TYPE:string = 'people',
    MODULE_NAME:string = MODULE_TYPE + 'List', // peopleList Directive

    _defaultSearch:any = { size: 15 };

import {AbstractDirective} from 'root/search/AbstractListDir';

/**
 * @ngdoc directive
 * @module peopleListDirective
 *
 * @requires DataBus
 * @requires $compile
 * @requires $analytics
 * @requires $q
 * @requires Util
 * @requires $templateCache
 */
require('../../../app').directive(MODULE_NAME, [
    'DataBus', '$compile', '$analytics', '$q', 'Util', '$templateCache',
    function (
        DataBus:any,
        $compile:ng.ICompileService,
        $analytics:any,
        $q:ng.IQService,
        Util:any,
        $templateCache:ng.ITemplateCacheService
    ) {
        var listDirective:cp.activities.IListDirective = new AbstractDirective({
                moduleName: MODULE_NAME,
                moduleType: MODULE_TYPE,
                itemKeyID: 'userID',
            },
            _defaultSearch,
            DataBus,
            $analytics,
            $compile,
            $q,
            Util
        );

        return {
            restrict: 'E',
            replace: true,
            scope: {
                id:                 '@',
                options:            '=?',
                onRefresh:          '=?',
                zeroDataDirective:  '@',
                stack:              '=?',
                group:              '=?',
                subType:            '@',        // {string} - one of (teacher|tandem)
            },
            template: $templateCache.get('views/people/directives/list/Template.html'),
            controller: 'PeopleListCtrl',
            link: {
                pre: listDirective.pre.bind(listDirective),

                post: listDirective.postCallback(function (scope, iElem) {
                    iElem.on('click', '.message', function (event:JQueryEventObject) {
                        scope = <cp.activities.IListDirectiveActivityScope>angular.element(event.target).scope();
                        DataBus.emit('/message/user', {
                            to: [scope.Activity]
                        });
                    });
                })
            }
        };
    }
]);
