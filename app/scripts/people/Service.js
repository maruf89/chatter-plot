/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../app').service('People', [
    'SocketIo',
    'User',
    'PeopleHelper',
    'PeopleMap',
    require('root/people/Service'),
]);
//# sourceMappingURL=Service.js.map