/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />

"use strict";

require('../app').service('Action', [
    'FlowRequest',
    'FlowEvent',
    'People',
    'Util',
    'DataBus',
    require('root/activities/ActionService'),
]);
