/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var SortList_1 = require('root/activities/directives/SortList');
require('../../../app').directive('sortList', [
    '$templateCache',
    'Interaction',
    'Events',
    'People',
    'Venues',
    'DataBus',
    'Util',
    '$analytics',
    SortList_1.SortListDirective,
]);
//# sourceMappingURL=Dir.js.map