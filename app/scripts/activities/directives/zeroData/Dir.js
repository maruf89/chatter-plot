/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../../app').directive('zeroData', [
    '$state', '$templateCache', 'Util', 'Config',
    function ($state, $templateCache, Util, Config) {
        var tandemChoice = {
            illo: '/images/landing/tandem/_Intro.svg',
            button: {
                key: 'activities.FIND_TANDEMPARTNERS',
                className: 'btn-blue',
                link: "activity.search.default({type:'tandems'})"
            },
        }, eventChoice = {
            illo: '/images/AdSlot/addSelfSpeakerList/map.svg',
            button: {
                key: 'events.CREATE_EVENT',
                className: 'btn-white',
                link: 'dashboard.eventCreate'
            }
        }, types = {
            tandems: {
                title: {
                    key: 'profile.ZERO-FAVE_CTA'
                },
                choices: [tandemChoice]
            },
            myActivities: {
                title: { key: 'activities.ZERO-MY_TITLE' },
                copy: {
                    key: 'activities.ZERO-MY_BODY',
                    // There's 2 links in the translated text
                    // 1st - user search   &   2nd - event search
                    onTranslated: function (text) {
                        var $elem = $('<span>' + text + '</span>'), $links = $elem.find('a').addClass('styled-link');
                        $links[0].href = $state.href(Config.routes.search, { type: 'tandems' });
                        $links[1].href = $state.href(Config.routes.search, { type: 'events' });
                        return $elem.html();
                    }
                },
                choices: [tandemChoice, eventChoice]
            }
        };
        return {
            restrict: 'E',
            replace: true,
            scope: false,
            template: $templateCache.get('views/activities/directives/zeroData/Template.html'),
            link: {
                pre: function (scope, iElem, iAttrs) {
                    var data = scope.data = types[iAttrs.what], setupMap = {
                        title: '.cta-head',
                        copy: '.copy',
                    };
                    // Set the title/body copy
                    _.each(setupMap, function (selector, key) {
                        var $elem = iElem.find(selector);
                        if (!data[key]) {
                            $elem.addClass('hidden');
                            return;
                        }
                        Util.getI18nVars(data[key].key, data[key].variables)
                            .then(function (html) {
                            if (data[key].onTranslated) {
                                html = data[key].onTranslated(html);
                            }
                            return $elem[0].innerHTML = html;
                        });
                    });
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map