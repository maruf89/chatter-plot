/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
/**
 * @ngdoc directive
 * @module back2SearchDirective
 * @restrict E
 * @requires $templateCache
 */
'use strict';
require('../../../app').directive('back2Search', [
    '$state', '$history', '$rootScope', 'Config', '$templateCache', 'User',
    function ($state, $history, $rootScope, Config, $templateCache, User) {
        return {
            restrict: 'E',
            replace: true,
            scope: false,
            template: $templateCache.get('views/activities/directives/back2Search/Template.html'),
            link: {
                pre: function (scope) {
                    var last = $history.previous(), disable = false;
                    // If we're going to our own profil show the "To Search" button
                    if ($state.current.name === 'dashboard.profile.user') {
                        disable = User.userID && p($state.params.userID) === User.userID;
                    }
                    scope.vars = {
                        fromSearch: last.state.name === Config.routes.search && !disable,
                        checkState: function (event, toState, toParams, fromState) {
                            scope.vars.fromSearch = fromState.name === Config.routes.search;
                        }
                    };
                    scope.vars.unbindListener = $rootScope.$on('$stateChangeSuccess', scope.vars.checkState);
                    scope.$on('$destroy', scope.vars.unbindListener);
                },
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map