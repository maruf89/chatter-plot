/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

import {ActivitySearchCtrl} from 'front/sections/search/Ctrl';

require('../../app').controller('ActivitySearchCtrl', [
    '$scope',
    'Util',
    'DataBus',
    'Activities',
    'Maps',
    '$state',
    ActivitySearchCtrl
]);
