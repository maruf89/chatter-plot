/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../../../app').directive('searchZeroData', [
    '$templateCache',
    'Util',
    'User',
    '$q',
    'Activities',
    'Config',
    'iTalki',
    require('root/search/directives/searchZeroData'),
]);
//# sourceMappingURL=Dir.js.map