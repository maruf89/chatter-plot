/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var Ctrl_1 = require('front/sections/search/Ctrl');
require('../../app').controller('ActivitySearchCtrl', [
    '$scope',
    'Util',
    'DataBus',
    'Activities',
    'Maps',
    '$state',
    Ctrl_1.ActivitySearchCtrl
]);
//# sourceMappingURL=Ctrl.js.map