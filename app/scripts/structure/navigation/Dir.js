/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
/**
 * @ngdoc directive
 * @module navigationDirective
 *
 * @require Config
 * @require Auth
 * @require $state
 * @require eventHostModal
 * @require User
 * @require $q
 * @require locale
 */
'use strict';
require('../../app.js').directive('siteNav', [
    'Config', 'Auth', '$state', 'User', '$q', 'locale', 'Cloudinary', 'DataBus', '$templateCache',
    function (Config, Auth, $state, User, $q, locale, Cloudinary, DataBus, $templateCache) {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            template: $templateCache.get('views/structure/navigation/Template.html'),
            link: function (scope, iElem) {
                scope.routes = Config.routes;
                scope.vars = {
                    site: Config.display,
                    Auth: Auth,
                    currentMenu: null,
                    hasPhoto: null,
                    userData: User.data,
                    goHome: function () {
                        return $state.go(Config.routes.onLogout);
                    },
                    authWatchExpr: null
                };
                // On authentication update nav and set menu to user's first name
                var onAuthChange = function (authenticated) {
                    scope.vars.userData = User.data;
                    if (!(scope.vars.authWatchExpr = authenticated)) {
                        return;
                    }
                    var promise = User.data.firstName ? $q.when() : locale.ready('common');
                    promise.then(function () {
                        var value = User.data.firstName || locale.getString('common.NAV_DROPDOWN');
                        scope.$root.safeDigest(scope);
                        _.defer(function () {
                            $('#navMenuText').text(value);
                        });
                    });
                };
                // Needed by other parts of the site for sticky nav
                CP.Global.vars.navHeight = iElem.outerHeight();
                Auth.on('/authChange', onAuthChange);
                return scope.$on('$destroy', function () {
                    Auth.removeListener('/authChange', onAuthChange);
                });
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map