/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';


require('../../../../app').directive('cpPageSubheader', [
    '$compile',
    '$rootScope',
    '$templateCache',
    'DataBus',
    require('root/structure/navigation/directives/cpPageSubheader/Dir')
]);
