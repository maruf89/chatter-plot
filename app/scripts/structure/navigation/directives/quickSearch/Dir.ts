/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />

/**
 * @module quickSearchDirective
 *
 * @requires Util
 * @requires Venues
 * @requires locale
 * @requires $q
 * @requires $templateCache
 * @requires Activities
 */
'use strict';

require('../../../../app.js').directive('quickSearch', [
    'Util', 'Venues', 'locale', '$q', '$templateCache', 'Activities', '$analytics', 'DataBus',
    function (Util, Venues, locale, $q, $templateCache, Activities, $analytics, DataBus) {

        // preload
        locale.ready('activities');
        locale.ready('languages');
        return {
            restrict: 'E',
            replace: true,
            scope: {},
            template: $templateCache.get('views/structure/navigation/directives/quickSearch/Template.html'),
            link: {
                pre: function (scope) {
                    return scope.vars = {
                        focus: false,
                        showBox: false,
                        passiveLoc: false, // whether the location was set non-manually
                        filter: {
                            language: [],
                            type: 'tandems',
                            location: null,
                        },
                        langComplete: {},
                        location: {
                            "default": null,
                            retrievingDefault: null,
                            value: null,
                            force: true,
                        }
                    };
                },
                post: function (scope, iElem) {
                    var vars = scope.vars,
                        langFocusBound = false,
                        curLang = null,
                        $input = null,

                        updateLangPlaceholder = function () {
                            return vars.langComplete.placeholder = vars.langComplete
                                .placeholder_text = locale.getString('activities.SEARCHING_LANG', {
                                    language: curLang
                                });
                        },

                        buildDefaultLocation = function (location) {
                            return Util.geo.latlng2Address(location)
                                .then(function (places) {
                                    vars.location.retrievingDefault = false;
                                    if (!places[0]) {
                                        return;
                                    }

                                    var address = Venues.scrapePlace(places[0], 'google');
                                    vars.filter.location = address;

                                    if (address.city) {
                                        vars.location["default"] = address.city + ", " + address.country;
                                        vars.location.force = false;

                                        // 12 is the city/postal zoom level
                                        vars.filter.location.coords.zoom = 12;
                                    }
                                });
                        },

                        // Function that hides the popout modal if it's not selected
                        hideCheck = function (event, force) {
                            if (force !== true &&
                                (!vars.focus ||
                                    iElem[0] === event.target ||
                                    event.target === document.body ||
                                    iElem[0].contains(event.target) ||
                                    !document.body.contains(event.target)
                                )
                            ) {
                                return true;
                            }

                            // hide the window
                            vars.focus = false;

                            // Wait until the callback for blur.qs get's called to remove it
                            _.defer(function () {
                                $input && $input.off('blur.qs');
                                iElem.removeClass('has-lang');
                            });

                            return scope.$root.safeDigest(scope);
                        },

                        getLangObj = function (langIDs:number[]):ng.IPromise<any> {
                            if (!langIDs || !langIDs.length) {
                                langIDs = [0];
                            }

                            return Util.language.expand(Util.language.arrayToObject(langIDs), langIDs);
                        },

                        onSearchUpdate = function (options:cp.activities.ISearchOptionsOpts) {
                            getLangObj(options.languages).then(function (data) {
                                vars.getLanguage(data[0].languageID, data[0].name, true);
                                updateLangPlaceholder();
                                hideCheck(null, true);
                            });
                        };

                    CP.Cache.$document.on('click', hideCheck);

                    DataBus.on('/search/refresh', onSearchUpdate);

                    vars.onFocus = function () {
                        vars.focus = true;
                        if (!vars.location["default"] && !vars.location.retrievingDefault) {
                            vars.location.retrievingDefault = true;
                            Util.geo.getMyLocation().then(buildDefaultLocation);
                        }

                        if (langFocusBound) {
                            iElem.addClass('has-lang');
                        }

                        return scope.$root.safeDigest(scope);
                    };

                    vars.getLanguage = function (langID, name, notManual) {
                        this.showBox = true;
                        curLang = name;
                        this.filter.language = [langID];
                        $input = iElem.find('.lang-search input').blur();

                        if (!langFocusBound) {
                            $input.on('focus.qs', function () {
                                $input.val(curLang);

                                // We don't want to show the autocomplete if just 1 option
                                if (vars.filter.language) {
                                    $input.typeahead('close');
                                }
                                _.defer(function () {
                                    return $input[0].select();
                                });

                                // This needs to get rebound every time, otherwise we get into a
                                // vicious loop where you can't get out of the language select
                                $input.one('blur.qs', function () {
                                    _.defer(function () {
                                        $input.val('');
                                        updateLangPlaceholder();
                                    });
                                });
                            });
                            langFocusBound = true;
                        }
                        updateLangPlaceholder();

                        if (!notManual) {
                            $analytics.eventTrack('click', {
                                category: 'search',
                                label: 'quickSearch: language selected'
                            });
                        }

                        // Important that we return false here to stop autocomplete
                        return false;
                    };

                    vars.invalidateSearch = function (field) {
                        return this.filter[field] = null;
                    };

                    vars.addLoc = function (place, passive) {
                        this.filter.location = place;
                        this.passiveLoc = passive;
                        scope.$root.safeDigest(scope);
                    };

                    vars.toggleLocation = function () {
                        this.location.force = true;

                        _.defer(function () {
                            var _input = iElem.find('.search-loc input').focus();
                            _.defer(function () {
                                _input.trigger('focus');
                            });
                        });

                        $analytics.eventTrack('click', {
                            category: 'search',
                            label: 'quickSearch: manually enter location'
                        });
                    };

                    vars.submitSearch = function () {
                        var coords, search,
                            filter = this.filter,
                            specificity = this.passiveLoc ? 12 : Venues.scrapeSpecificity(filter.location, 'google');

                        vars.focus = false;

                        if (!(coords = filter.location.coords)) {
                            coords = Venues.getCoords(filter.location, 'google');
                        }

                        // Don't zoom in too far
                        coords.zoom = specificity.zoom;

                        search = {
                            location: {
                                coords: coords,
                            },
                            languages: filter.language
                        };

                        $analytics.eventTrack('submit', {
                            category: 'search',
                            label: 'quickSearch'
                        });

                        return Activities.search(search, filter.type);
                    };

                    return scope.$on('$destroy', function () {
                        return CP.Cache.$document.off('click', hideCheck);
                    });
                }
            }
        };
    }
]);
