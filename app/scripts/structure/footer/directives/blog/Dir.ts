/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../../../app.js').directive('footerBlog', [
    'Config', '$templateCache',
    function(Config:any, $templateCache:ng.ITemplateCacheService) {
        return {
            restrict: 'E',
            replace: true,
            scope: {},
            template: $templateCache.get('views/structure/footer/directives/blog/Template.html'),
            link: {
                pre: function(scope) {
                    // Needed for jFeed to work
                    jQuery.browser = {};

                    scope.vars = {
                        feeds: [],
                        rssLink: Config.socialMedia.blog.rss,
                    };

                    var screen:any = scope.$root.size,
                        maxItems:number = 2,
                        lgMaxItems:number = 3,
                        maxDescriptionLength:number = 140,

                    loadFeed = function():void {
                        jQuery.getFeed({
                            url: Config.socialMedia.blog.rss,
                            success: processFeed,
                            error: function () {
                                console.log('error loading rss in footerBlog');
                            }
                        })
                    },

                    processFeed = function (feed:any) {
                        // check if we're greater than or larger than tablet
                        // screen.current will only have length === 1 if mobile or tablet
                        var itemLength:number = screen.current.length > 1 ? lgMaxItems : maxItems;

                        scope.vars.feeds = feed.items.slice(0, itemLength);
                        formatIndividual(scope.vars.feeds);
                        scope.$root.safeDigest(scope);
                    },

                    formatIndividual = function (items:any[]) {
                        var hasPhoto:boolean,
                            photo:string,

                            // pull the full image markup, minus the closing >
                            imgPattern:RegExp = /^<img[^>]*/,

                            // pulls the src from the image
                            srcPattern:RegExp = /src="([^"]*)/;

                        _.each(items, function (item:any) {
                            photo = item.description.match(imgPattern);

                            // will be null if no matches found
                            if (_.isArray(photo)) {
                                item.image = photo[0].match(srcPattern)[1];

                                // crop out the image and factor in that the closing bracket > needs to be accounted
                                item.description = item.description.substr(photo[0].length + 1);
                            }

                            item.description = scope.$root.stripHtml(item.description)
                                                          .substr(0, maxDescriptionLength);
                        });
                    };

                    jQuery.getScript('/bower_components/jfeed/build/dist/jquery.jfeed.min.js', loadFeed);
                }
            }
        }
    }
]);




