/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var seoData = require('root/config/seoDefaults.json');
require('../../../../app').directive('footerSeoLinks', [
    '$state', '$q', '$templateCache', '$rootScope', 'Util', 'locale', 'FB',
    function ($state, $q, $templateCache, $root, Util, locale, FB) {
        var showThis = !$root.size.xs;
        return {
            restrict: 'E',
            replace: true,
            scope: {
                activityType: '@',
            },
            template: showThis ? $templateCache.get('views/structure/footer/directives/seo-links/Template.html') : '',
            link: {
                pre: function (scope, iElem) {
                    var type = scope.activityType || 'tandem', activityType = type === 'tandem' ? 'tandem-partners' : 'language-events', activityText, maxLength = 10, initLinks = function () {
                        activityText = locale.getString('activities.' + (type === 'tandem' ? 'TANDEM_PARTNERS' : 'EVENTS'));
                        _.each(scope.vars.sections, function (section) {
                            scope.vars[section] = _.map(seoData[section].slice(0, maxLength), format[section]);
                        });
                        scope.$root.safeDigest(scope);
                    }, format = {
                        locations: function (city) {
                            return {
                                link: $state.href('activity.search.type', {
                                    type: activityType,
                                    city: city.value,
                                }),
                                text: locale.getString('meta.ACTIVITIES_IN_CITY', {
                                    activities: activityText,
                                    in_city: locale.getString(city.key),
                                }),
                            };
                        },
                        languages: function (lang) {
                            return {
                                link: $state.href('activity.search.language', {
                                    type: activityType,
                                    language: lang.value,
                                }),
                                text: locale.getString('meta.PRACTICE_LANGUAGE', {
                                    language: locale.getString(lang.key),
                                }),
                            };
                        },
                        resources: function (resource) {
                            return {
                                link: $state.href(resource.link),
                                text: locale.getString(resource.key),
                                label: resource.label,
                            };
                        }
                    }, parseFBWidget = function () {
                        window.FB.XFBML.parse(iElem.find('.fb-widget')[0]);
                    };
                    scope.vars = {
                        sections: ['locations', 'languages', 'resources'],
                        locations: [],
                        languages: [],
                        resources: [],
                        getHeadKey: function (sec) {
                            return 'misc.' + sec.toUpperCase();
                        },
                        linkToLabel: function (link) {
                            return link.substr(1).replace(/-/g, ' ');
                        },
                    };
                    $q.all([
                        locale.ready('languages2'),
                        locale.ready('cities'),
                        locale.ready('activities'),
                        locale.ready('meta'),
                        locale.ready('standalone'),
                        locale.ready('teacher'),
                        locale.ready(locale.getPath(seoData.resources[0].key)),
                    ]).then(initLinks);
                    FB.onLoaded().then(parseFBWidget);
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map