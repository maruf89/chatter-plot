/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var locStorageKey = 'siteMessageHide';
require('../../app').directive('siteMessage', ['$templateCache', 'localStorageService',
    function ($templateCache, locStorage) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: $templateCache.get('views/structure/siteMessage/Template.html'),
            link: {
                pre: function (scope) {
                    var message = CP.Settings.ds_siteMessage;
                    message = message && message.text;
                    scope.vars = {
                        show: message && !locStorage.get(locStorageKey),
                        message: message
                    };
                    // If there is no message then remove the key in case of new future messages
                    if (!message) {
                        _.defer(function () {
                            locStorage.remove(locStorageKey);
                        });
                    }
                },
                post: function (scope, iElem) {
                    if (!scope.vars.show) {
                        iElem.remove();
                        iElem = null;
                    }
                    else {
                        iElem.on('click', '.close-btn', function () {
                            locStorage.set(locStorageKey, true);
                            iElem.remove();
                            iElem = null;
                        });
                    }
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map