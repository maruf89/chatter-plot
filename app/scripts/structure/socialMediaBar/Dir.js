/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../app.js').directive('socialMediaBar', [
    'Config',
    function (Config) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            templateUrl: 'views/structure/socialMediaBar/Template.html',
            link: {
                pre: function (scope) {
                    var order = ['facebook', 'gplus', 'twitter', 'blog'];
                    return scope.vars = {
                        sources: _.map(order, function (media) {
                            return {
                                name: media,
                                data: Config.socialMedia[media]
                            };
                        })
                    };
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map