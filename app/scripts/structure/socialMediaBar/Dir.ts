/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../app.js').directive('socialMediaBar', [
    'Config',
    function (Config:any) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            templateUrl: 'views/structure/socialMediaBar/Template.html',
            link: {
                pre: function (scope) {
                    var order:string[] = ['facebook', 'gplus', 'twitter', 'blog'];

                    return scope.vars = {
                        sources: _.map(order, function (media:string) {
                            return {
                                name: media,
                                data: Config.socialMedia[media]
                            };
                        })
                    };
                }
            }
        };
    }
]);
