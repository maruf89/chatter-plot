/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var landingHostTrnsl8 = function () {
    var self, vars;
    self = this;
    _.each(arguments, function (val, index) {
        self[landingHostTrnsl8.$inject[index]] = val;
    });
    self.$element.addClass('styled-link');
    vars = {
        modal: {
            deactivate: self.eventHostModal.deactivate,
            callToAction: function () {
                if (self.eventHostModal.active()) {
                    self.eventHostModal.deactivate();
                }
                return Auth.upgradeUser('HOST', {
                    onUpgrade: 'dashboard.eventCreate'
                });
            }
        }
    };
    self.$element.on('click', function () {
        return self.eventHostModal.activate(vars);
    });
    return self;
};
landingHostTrnsl8.$inject = ['$scope', '$element', 'eventHostModal', 'Auth'];
require('../../app').controller('landingHostTrnsl8', landingHostTrnsl8);
//# sourceMappingURL=host.js.map