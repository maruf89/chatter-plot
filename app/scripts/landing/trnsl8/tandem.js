/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var landingTandemTrnsl8 = function () {
    var self, vars;
    self = this;
    _.each(arguments, function (val, index) {
        self[landingTandemTrnsl8.$inject[index]] = val;
    });
    self.$element.addClass('inherit-link');
    vars = {
        modal: {
            deactivate: self.tandemModal.deactivate
        }
    };
    self.$element.on('click', function () {
        return self.tandemModal.activate(vars);
    });
    return self;
};
landingTandemTrnsl8.$inject = ['$scope', '$element', 'tandemModal'];
require('../../app').controller('landingTandemTrnsl8', landingTandemTrnsl8);
//# sourceMappingURL=tandem.js.map