/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var landingTeacherTrnsl8 = function () {
    var self, vars;
    self = this;
    _.each(arguments, function (val, index) {
        self[landingTeacherTrnsl8.$inject[index]] = val;
    });
    self.$element.addClass('styled-link');
    vars = {
        modal: {
            deactivate: self.teacherModal.deactivate

            // callToAction: -> 

            //     if self.teacherModal.active() then self.teacherModal.deactivate() 

            //     self.Auth.upgradeUser('HOST') 
        }
    };
    self.$element.on('click', function () {
        return self.teacherModal.activate(vars);
    });
    return self;
};

landingTeacherTrnsl8.$inject = ['$scope', '$element', 'teacherModal'];

require('../../app').controller('landingTeacherTrnsl8', landingTeacherTrnsl8);
