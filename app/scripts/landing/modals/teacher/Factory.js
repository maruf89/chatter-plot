/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../../app').factory('teacherModal', [
    'btfModal',
    function (Modal) {
        return Modal({
            templateUrl: 'views/landing/modals/teacher/Template'
        });
    }
]);
//# sourceMappingURL=Factory.js.map