/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../../app').factory('eventHostModal', [
    'btfModal', '$templateCache',
    function (Modal, $templateCache) {
        return Modal({
            template: $templateCache.get('views/landing/modals/eventHost/Template.html')
        });
    }
]);
//# sourceMappingURL=Factory.js.map