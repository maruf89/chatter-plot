/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../../app').factory('tandemModal', [
    'btfModal',
    function (Modal) {
        return Modal({
            templateUrl: 'views/landing/modals/tandem/Template'
        });
    }
]);
//# sourceMappingURL=Factory.js.map