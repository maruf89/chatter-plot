/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../../app').directive('landingSectionSplash', [
    'Activities', '$templateCache',
    function (Activities, $templateCache) {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            template: $templateCache.get('views/landing/sections/splash/Template.html'),
            link: {
                pre: function (scope, iElem, iAttrs) {
                    scope.vars = {
                        hideSearch: iAttrs.hideSearch === 'true',
                        modalActive: false,
                        onSearch: Activities.search.bind(Activities),
                        searchOptions: {
                            location: {
                                coords: null,
                                distance: 200
                            },
                            languages: [0],
                            pagination: 0,
                            incrementBy: 15,
                            all: false,
                        },
                        type: 'tandems',
                    };
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map