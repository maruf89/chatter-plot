/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var regionsSource = require('front/sections/landing/config/regions.json');
/**
 * TEMPORARY - For Tristant
 *
 * var screen = will be an object that looks like:
 *      {
 *         "current":[
 *            "sm",
 *            "md"
 *         ],
 *         "xs":false,
 *         "grey-zone":false, // represents the size in between mobile & tablet
 *         "sm":true,
 *         "md":true,
 *         "lg":false
 *      }
 *
 *  if current size is mobile, grey-zone or tablet, then `current` will only have 1 value in the array
 */
require('../../../app').directive('lsRegions', [
    'Config', '$state', '$templateCache',
    function (Config, $state, $templateCache) {
        return {
            restrict: 'E',
            scope: false,
            replace: true,
            template: $templateCache.get('views/landing/sections/regions/Template.html'),
            link: {
                pre: function (scope) {
                    // get the current browser size
                    var screen = scope.$root.size, regions = regionsSource.blocks, curColHeight = 0, rowHeight = 4, colArray = [], curColumn = [];
                    // Only match if mobile, or tablet
                    if (screen.current.length === 1) {
                        // limit to 4 regions
                        regions = regions.slice(0, 3);
                    }
                    // Iterates through regions putting images into columns
                    _.each(regions, function (region) {
                        // Define a link for each image
                        region.href = $state.href('activity.search.language', {
                            type: 'tandem-partners',
                            language: region.lang.trim().toLocaleLowerCase(),
                        });
                        curColumn.push(region);
                        curColHeight += region.size;
                        if (curColHeight >= rowHeight) {
                            colArray.push(curColumn);
                            curColumn = [];
                            curColHeight = 0;
                        }
                    });
                    // make the json data accessible to the scope template
                    scope.regionData = regionsSource;
                    scope.regions = regions;
                    scope.colArray = colArray;
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map