/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var defaultText = require('root/landing/ABText/default'),

LandingCtrl = function ($scope, $analytics, $state, Config, Auth) {
    this._$scope = $scope;
    this._$analytics = $analytics;
    this._$state = $state;
    this._Config = Config;

    $scope.AB = {
        text: defaultText,
    };

    $scope.$root.addPageClass('body-landing');

    Auth.on('/authChange', this.parseFB);
    _.defer(function () {
        this.parseFB(Auth.isAuthenticated());
    }.bind(this));
};

LandingCtrl.prototype = _.extend(LandingCtrl.prototype, {
    parseFB: function (parse) {
        if (parse && window.FB) {
            _.defer(function () {
                window.FB.XFBML.parse($('.floating.fb-like-container')[0]);
            });
        }
    },
});

LandingCtrl.$inject = [
    '$scope',
    '$analytics',
    '$state',
    'Config',
    'Auth',
];

require('../app').controller('LandingCtrl', LandingCtrl);
