/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../../app').directive('fullShareBar', [
    'Config', '$templateCache',
    function (Config, $templateCache) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: $templateCache.get('views/landing/directives/fullShareBar/Template.html'),
            link: {
                pre: function (scope) {
                    var order = ['facebook', 'gplus', 'twitter', 'blog'];
                    scope.vars = {
                        sources: _.map(order, function (media) {
                            return {
                                name: media,
                                data: Config.socialMedia[media]
                            };
                        })
                    };
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map