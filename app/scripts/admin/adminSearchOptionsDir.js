/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../app')
    .directive('adminSearchOptions', [
    function () {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                source: '='
            },
            templateUrl: 'views/admin/adminSearchOptionsTemplate',
        };
    }
]);
//# sourceMappingURL=adminSearchOptionsDir.js.map