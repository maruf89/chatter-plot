/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var adminEventBulkAddCtrl = function () {
    var self;
    self = this;
    _.each(arguments, function (val, index) {
        self[adminEventBulkAddCtrl.$inject[index]] = val;
    });
    self.file = null;
    self.flow = null;
    return self;
};
adminEventBulkAddCtrl.prototype = _.extend(adminEventBulkAddCtrl.prototype, {
    uploadError: function (err) {
        return console.log(err);
    },
    uploadSuccess: function (msg) {
        return msg = JSON.parse(msg);
    },
    oneTimeButton: function () {
        return this.SocketIo.onEmitSock('/event/onetimeSitemapAdd')
            .then(function () {
            return console.log('success!');
        });
    }
});
adminEventBulkAddCtrl.$inject = ['$q', '$scope', 'SocketIo', 'DataBus'];
require('../../../app').controller('adminEventBulkAddCtrl', adminEventBulkAddCtrl);
//# sourceMappingURL=Ctrl.js.map