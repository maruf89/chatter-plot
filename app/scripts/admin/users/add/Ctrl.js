/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var adminUsersAddCtrl = function () {
    var self;
    self = this;
    _.each(arguments, function (val, index) {
        self[adminUsersAddCtrl.$inject[index]] = val;
    });
    return self;
};
adminUsersAddCtrl.prototype = angular.extend(adminUsersAddCtrl.prototype, {
    addUser: function (form) {
        var self;
        self = this;
        return self.SocketIo.onEmitSock('/admin/users/addFrom', {
            sql: 'sqlData',
            es: 'esData'
        })
            .then(function (response) {
            self.DataBus.emit('yapServerResponse', response);
            return console.log(response);
        }, function (err) {
            return self.DataBus.emit('yapServerResponse', err);
        });
    },
    deleteUsers: function () {
        var self;
        self = this;
        return self.SocketIo.onEmitSock('/admin/users/deleteFakies')
            .then(function (response) {
            self.DataBus.emit('yapServerResponse', response);
            return console.log(response);
        }, function (err) {
            return self.DataBus.emit('yapServerResponse', err);
        });
    }
});
adminUsersAddCtrl.$inject = ['$scope', 'SocketIo', 'DataBus'];
require('../../../app.js')
    .controller('adminUsersAddCtrl', adminUsersAddCtrl);
//# sourceMappingURL=Ctrl.js.map