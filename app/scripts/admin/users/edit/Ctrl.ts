/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var Form = require('../../formClass'),

adminUsersEditCtrl = function () {
    var self = this;

    _.each(arguments, function (val, index) {
        self[adminUsersEditCtrl.$inject[index]] = val;
    });

    Form.prototype.initiate.apply(self, [self.users.data, 'user']);
};

adminUsersEditCtrl.prototype = angular.extend(adminUsersEditCtrl.prototype, Form.prototype, {

    /**
     * After edits have been made and user confirms to save changes.
     * Will check for differences between the original, and if they exist
     * will send a socket update to the server
     *
     * @param  {object} user
     */
    confirmEdit: function (user) {
        var difference, self;
        self = this;
        difference = _.omit(user._source, function (val, key) {
            return user.original[key] === val;
        });

        // Return if there are no differences 
        if (!_.keys(difference)
            .length) {
            return;
        }
        return self.SocketIo.onEmitSock('/user/edit', {
                action: 'update',
                data: [difference],
                userID: user._id
            })
            .then(function (n, response) {
                return self.DataBus.emit('yap', {
                    type: 'success',
                    body: 'Changes Updated!',
                    title: 'Updated'
                });
            });
    },

    /**
     * Called on 'Delete School' button
     *
     * @param  {object} user
     */
    "delete": function (user) {
        var self = this,
            index = 0;

        _.every(self.users.data, function (_user) {
            return _user !== user && ++index;
        });

        return self.locale.ready('admin')
            .then(function () {
                var confirmText, message, title;
                title = self.locale.getString('admin.DELETE_USER');
                message = self.locale.getString(
                    'admin.DELETE_USER_MESSAGE', {
                        firstName: user._source.firstName,
                        lastName: user._source.lastName
                    });
                confirmText = self.locale.getString('admin.DELETE');
                return self.modalConfirm.activate(title, message, confirmText);
            })
            .then(function (close) {
                return self.SocketIo.onEmitSock('/user/delete', {
                    userID: user._id
                })
                .then(function () {

                    // Remove the user from the scope
                    var elem, scope;
                    elem = $(document.getElementById(user._id));
                    scope = elem.scope();
                    self.DataBus.emit('yap', {
                        type: 'success',
                        body: 'Row Successfully Deleted!',
                        title: 'Success'
                    });
                    self.users.data.splice(index, 1);
                    if (scope) {
                        return scope.$destroy();
                    }
                }, function (error) {
                    return self.DataBus.emit('yapServerResponse', error);
                })["finally"](close);
            });
    },

    /**
     * Called on the 'All Fields' button, will load a modal
     * with all of the fields data displayed
     *
     * @param  {object} user
     */
    fullDescription: function (user) {
        return this.modalDataView.activate(user._source.companyName, user._source);
    },

    /**
     * Handles updating the query params and loading the next set of users
     *
     * @param  {Integer} direction 0 for 'previous' - 1 for 'next'
     */
    go: function (direction) {
        var params = this.$location.search();
        params.from = +(this.current.from || 0) + +(direction ? this.current.size : -this.current.size);
        params.size = this.current.size;
        return this.$location.search(params);
    }
});

adminUsersEditCtrl.$inject = [
    '$q',
    '$scope',
    'SocketIo',
    'users',
    'modalConfirm',
    'DataBus',
    '$location',
    'locale',
];

require('../../../app').controller('adminUsersEditCtrl', adminUsersEditCtrl);
