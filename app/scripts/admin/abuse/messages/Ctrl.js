/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var adminAbuseMessagesCtrl = function () {
    var self = this;
    _.each(arguments, function (val, index) {
        self[adminAbuseMessagesCtrl.$inject[index]] = val;
    });
};
adminAbuseMessagesCtrl.prototype = _.extend(adminAbuseMessagesCtrl.prototype, {});
adminAbuseMessagesCtrl.$inject = [
    '$q',
    '$scope',
    'SocketIo',
    'modalConfirm',
    'DataBus'
];
require('../../../app').controller('adminAbuseMessagesCtrl', adminAbuseMessagesCtrl);
//# sourceMappingURL=Ctrl.js.map