/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var adminMiscSiteMsgCtrl = function ($scope, SocketIo, DataBus) {
    this._$scope = $scope;
    this._SocketIo = SocketIo;
    this._DataBus = DataBus;
    this.messageKey = 'siteMessage';
    this.message = CP.Settings['ds_' + this.messageKey];
    this.has = !!this.message;
    this.message = this.message || {};
};
/**
 * @description sets a message for all users to see
 * @param set
 * @returns {*}
 */
adminMiscSiteMsgCtrl.prototype.toggleSetMsg = function (set) {
    if (!this.message.text) {
        return false;
    }
    var payload = {
        key: this.messageKey,
        data: set && this.message
    };
    return this._SocketIo.onEmitSock('/admin/misc/siteMessage', payload)
        .then(function () {
        if (!(this.has = !this.has)) {
            this.message = {};
        }
        this.$scope.$root.safeDigest(this.$scope);
    }.bind(this));
};
adminMiscSiteMsgCtrl.$inject = [
    '$scope',
    'SocketIo',
    'DataBus',
];
require('../../../app.js').controller('adminMiscSiteMsgCtrl', adminMiscSiteMsgCtrl);
//# sourceMappingURL=Ctrl.js.map