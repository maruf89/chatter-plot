/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var adminMiscFeatureCtrl = function ($scope, SocketIo, DataBus) {
    this._$scope = $scope;
    this._SocketIo = SocketIo;
    this._DataBus = DataBus;
};

adminMiscFeatureCtrl.prototype.doIt = function () {
    var res;

    this._SocketIo.onEmitSock('/admin/feature/desired').then(function (response) {
        res = response;
    }).catch(function (err) {
        res = err;
    }).finally(function () {
        this._DataBus.emit('yapServerResponse', res);
    }.bind(this));
};

adminMiscFeatureCtrl.$inject = [
    '$scope',
    'SocketIo',
    'DataBus',
];

require('../../../app.js').controller('adminMiscFeatureCtrl', adminMiscFeatureCtrl);
