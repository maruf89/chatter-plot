/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var Form;

module.exports = Form = (function () {
    function Form() {}

    Form.prototype.initiate = function (data, field1) {
        var self;
        this.data = data;
        this.field = field1;
        self = this;

        /**
         * Wether all of the rows are selected || not button
         * @type {Boolean}
         */
        this.allSelected = false;
        this.current = {
            size: +(self.$location.$$search.size || 25),
            from: +(self.$location.$$search.from || 0),
            filterEmpty: self.$location.$$search.empty || 0
        };
        self.current.to = self.current.from + self.current.size;

        /**
         * Will store the currently selected entries
         * @type {array}
         */
        return this._bulkStore = [];
    };


    /**
     * Selects/Deselects all of the users checkboxex
     */

    Form.prototype.toggleAll = function () {
        var self;
        self = this;
        self.allSelected = !self.allSelected;
        return _.each(this.data, function (field) {
            field.selected = self.allSelected;
            self._bulkArrayUpdate(self.allSelected, field._id);
            return true;
        });
    };


    /**
     * Selects/Deselects the selected user checkbox
     *
     * @param  {object} user
     */

    Form.prototype.toggleSingle = function (field) {
        field.selected = !field.selected;
        return bulkArrayUpdate(field.selected, field);
    };


    /**
     * When edit is clicked, if edit is turned on, will
     * clone the original field source to check for differences on save
     *
     * @param  {object} field
     */

    Form.prototype.toggleEdit = function (field) {
        field.edit = !field.edit;

        // clone the source if we're starting edit 
        return field.original = field.edit ? _.clone(field._source) :
            null;
    };


    /**
     * Handles updating the query params and loading the next set of fields
     *
     * @param  {Integer} direction 0 for 'previous' - 1 for 'next'
     */

    Form.prototype.go = function (direction) {
        var params = this.$location.search();
        params.from = +(this.current.from || 0) + +(direction ? this.current
            .size : -this.current.size);
        params.size = this.current.size;
        return this.$location.search(params);
    };


    /**
     * Will update the page with results based on the passed in column
     *
     * @param  {string} field  equivalent to a field value
     */

    Form.prototype.sort = function (field) {
        var sort = 'asc';

        // only update the sort if the field is the same 
        if (this.$location.$$search.sortBy !== field) {
            sort = this.$location.$$search.sort === 'asc' ? 'desc' :
                'asc';
        }
        return this.$location.search({
            'sortBy': field,
            'sort': sort
        });
    };

    Form.prototype.filterEmpty = function () {
        var params;
        params = this._pluckParams();
        params.empty = (this.current.filterEmpty = !this.current.filterEmpty) ?
            'true' : 'false';
        return this.$location.search(params);
    };

    Form.prototype._pluckParams = function () {
        return _.pick(this.$location.search(), ['from', 'size', 'sortBy',
            'sort', 'empty'
        ]);
    };


    /**
     * Accepts a boolean wether the key should exist in the array,
     * if true and index doesn't already exist, then it's added
     * if false and index does exist, remove it
     *
     * @param  {Boolean} bool Wether it should exist in the array || not
     * @param  {string} key   Array index identifier
     */

    Form.prototype._bulkArrayUpdate = function (bool, key) {
        var index;
        index = this._bulkStore.indexOf(key);

        // Store/Splice index from bulk array 
        if (bool) {
            if (!index) {
                this._bulkStore.push(key);
            }
        } else {
            if (index) {
                this._bulkStore.splice(key, 1);
            }
        }
    };

    return Form;

})();
