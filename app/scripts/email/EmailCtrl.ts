/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var EmailCtrl = function (Config, Auth, $state, $q, $location, User, DataBus) {
    this._Config = Config;
    this._Auth = Auth;
    this._$state = $state;
    this._$q = $q;
    this._$location = $location;
    this._User = User;
    this._DataBus = DataBus;
    this.vars = {
        passwordUpdated: null,
        passwordMethod: 'passwordReset'
    };
    return this;
};

EmailCtrl.prototype = _.extend(EmailCtrl.prototype, {

    /**
     * Attempts to authenticate the user using the token and redirects upon login
     */
    initVerifiedRedirect: function () {
        this._Auth.setOnAuthHook(function (user) {
            return this._$state.go(this._Config.routes.onAccntVerify, user);
        }.bind(this), true);

        if (CP.session.doActivate) {
            this._Auth.activateUser(this._$location.search());
        }
        return setTimeout(this.upgradeSocket.bind(this), 2000);
    },

    upgradeSocket: function () {
        return this._Auth.authenticateWithToken(this._$location.search());
    },

    /**
     * Only called if user is already activated and optionally wants to
     * add a password via the confirmation screen
     * 
     * @param  {string} password
     */
    passwordUpdate: function (password) {
        var self, updates;
        self = this;
        updates = {
            password: password
        };
        return this._User.update(updates, true)
            .then(function () {
                self.vars.showPasswordForm = false;
                return self.vars.passwordUpdated = true;
            });
    },

    /**
     * Called on 'Forgot Password Reset' AND 'User Confirmation' via Email/Password Signup
     * Does NOT apply to users who signed up by social media and created a password
     * 
     * @param  {string} password
     * @return {Promise}
     */
    passwordReset: function (password) {
        var data, method, self;
        self = this;
        data = {
            password: password,
            token: this._$location.search()
                .token
        };

        // If password reset, will grab the default method 
        method = this.vars.passwordMethod;
        return this._Auth[method](data)
            .then(function () {
                self.vars.passwordUpdated = true;
                if (self.vars.passwordCallback) {
                    return self.vars.passwordCallback.call(self);
                }
            });
    },
    invalid: function () {
        var $state, source, sources, types, vars;
        vars = this.vars;
        types = {
            invalid_link: {
                title: 'updates.ERROR-INVALID_LINK_TITLE',
                copy: 'updates.ERROR-INVALID_LINK_FULL'
            },
            expired_token: {
                title: 'updates.ERROR-LINK_EXPIRED_TITLE',
                copy: 'updates.ERROR-LINK_EXPIRED_FULL'
            }
        };
        sources = {
            password_reset: {
                copy: 'updates.EXPIRED_PASS_RESET',
                link: 'account.auth',
                linkParams: {
                    page: "password-reset"
                }
            },
            confirm: {
                copy: 'updates.EXPIRED_CONFIRMATION',
                onClick: this.resendConfirmation.bind(this)
            }
        };
        $state = this._$state;
        vars.type = types[$state.params.type];
        source = vars.source = sources[$state.params.source];
        return vars.goLink = function () {
            vars.btnClicked = true;
            if (source.link) {
                return $state.go(source.link, source.linkParams);
            } else if (source.onClick) {
                return source.onClick();
            } else {
                return $state.go('landing.default');
            }
        };
    },
    resendConfirmation: function () {
        var self = this,
            activated = false;

        return this._Auth.isLoggedIn()
            .then(function () {
                if (!self._Auth.notActivated) {
                    return true;
                }
                activated = true;
                return self._Auth.resendActivationEmail();
            })
            .then(function () {
                var data;
                if (activated) {
                    data = {
                        type: 'success',
                        title: 'updates.SENT',
                        body: 'auth.ACTIVATION_RESENT'
                    };
                } else {
                    data = {
                        type: 'info',
                        title: 'updates.INFO',
                        body: 'auth.WARNING-ALRDY_VERIFIED'
                    };
                }
                data.duration = 3500;
                self._DataBus.emit('yap', data);
                return self._$state.go(self._Config.routes.onSignup);
            });
    }
});

EmailCtrl.$inject = ['Config', 'Auth', '$state', '$q', '$location', 'User',
    'DataBus'
];

require('../app').controller('EmailCtrl', EmailCtrl);
