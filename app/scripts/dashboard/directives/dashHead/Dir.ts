/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var _heads = {};

require('../../../app').directive('dashHead', [
    '$animate', '$compile',
    function ($animate, $compile) {
        return {
            restrict: 'A',
            scope: false,
            compile: function (tElem, tAttrs) {
                var $compiled, _scope, dashHead, headID;
                headID = tAttrs.dashHead + "DashHead";
                dashHead = _heads[headID];
                $compiled = null;
                if (dashHead) {
                    _scope = tElem.scope() || dashHead.$elem.scope();

                    // Must remove references to this directive before $compile'ing
                    tElem[0].removeAttribute('data-dash-head');
                    tElem[0].removeAttribute('dash-head');
                    $compiled = $compile(tElem.clone())(_scope);
                    if (dashHead.history.length) {

                        // Set a note not to remove this from the stack
                        dashHead.$current.tempHide = true;
                        $animate.leave(dashHead.$current);
                    }
                    $animate.enter($compiled, dashHead.$elem);
                    dashHead.history.push(dashHead.$current = $compiled);
                    tElem.empty()
                        .addClass('hidden');
                } else {
                    tElem[0].id = headID;
                    _heads[headID] = {
                        $elem: tElem,
                        history: [],
                        $current: null
                    };
                }
                return {
                    pre: function (scope) {
                        return scope.$on('$destroy', function () {
                            var $last;
                            if (dashHead) {

                                // If we're hiding a parent element, don't remove from history, don't do anything
                                if ($compiled.tempHide) {
                                    return true;
                                }
                                $animate.leave(dashHead.history
                                    .pop());
                                $last = dashHead.history[
                                    dashHead.history.length -
                                    1];
                                if ($last) {
                                    $last.tempHide = null;
                                    return $animate.enter(
                                        dashHead.$current =
                                        $last, dashHead.$elem
                                    );
                                }
                            } else {

                                // If original
                                return _heads[headID] = null;
                            }
                        });
                    }
                };
            }
        };
    }
]);
