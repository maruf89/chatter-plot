/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../../app').directive('hostingZeroData', [
    '$templateCache',
    function ($templateCache) {
        return {
            restrict: 'EA',
            replace: true,
            template: $templateCache.get(
                'views/dashboard/directives/hostingZeroData/Template.html'
            )
        };
    }
]);
