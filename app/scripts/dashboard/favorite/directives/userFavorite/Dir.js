/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
/**
 * @description A button that adds/removes a user to the current user's favorites
 * @ngdoc directive
 * @module userFavoriteDirective
 * @restrict E
 */
'use strict';
var listeners = 0, 
/**
 * @description A collection of scopes to be iterated over when authentication changes
 */
scopeArr = [], _checkFn = null, 
/**
 * @description iterates over the array of scopes that we have and adjusts the favorite button depending
 * @
 */
_onAuthSwap = function (authenticated) {
    return _.each(scopeArr, function (scope) {
        return _enableButton.call(scope, authenticated);
    });
}, _enableButton = function (authenticated) {
    if (!authenticated) {
        return this.vars.isFavorite = false;
    }
    this.vars.isFavorite = _checkFn(this.vars.targetID, 'all');
    return this.$root.safeDigest(this);
};
require('../../../../app').directive('userFavorite', [
    'User', 'DataBus', 'Favorite', '$templateCache',
    function (User, DataBus, Favorite, $templateCache) {
        return {
            restrict: 'E',
            replace: true,
            template: $templateCache.get('views/dashboard/favorite/directives/userFavorite/Template.html'),
            scope: {
                target: '@',
                category: '@',
                btnClass: '@',
            },
            link: {
                pre: function (scope) {
                    scope.target = p(scope.target);
                    scope.vars = {
                        isFavorite: null,
                        targetID: scope.target
                    };
                    _checkFn = Favorite.isFave.bind(Favorite);
                    _enableButton.call(scope, User.userID);
                },
                post: function (scope) {
                    scope.vars.toggleFavorite = function () {
                        var action = (scope.vars.isFavorite ? 'un' : '') + 'favorite', previous = scope.vars.isFavorite;
                        DataBus.emit('/user/favorite', { action: action, targetID: scope.vars.targetID, folder: 'all' });
                        scope.vars.isFavorite = !previous;
                        scope.$root.safeDigest(scope);
                        Favorite.toggle(scope.vars.targetID)
                            .then(function () {
                            // update this again in case user had to log in
                            // and it was reset because of that
                            scope.vars.isFavorite = !previous;
                        })
                            .catch(function () {
                            DataBus.emit('/error/user/favorite', { targetID: scope.vars.targetID });
                            scope.vars.isFavorite = previous;
                        }).finally(function () {
                            scope.$root.safeDigest(scope);
                        });
                    };
                    scopeArr.push(scope);
                    if (++listeners === 1) {
                        DataBus.on('/site/authChange', _onAuthSwap);
                    }
                    scope.$on('$destroy', function () {
                        var index;
                        if (!--listeners) {
                            // If last listener, remove listener and empty scope array
                            DataBus.removeListener('/site/authChange', _onAuthSwap);
                            scopeArr = [];
                            return _checkFn = null;
                        }
                        else {
                            // Otherwise just remove the scope
                            if ((index = scopeArr.indexOf(scope)) === -1) {
                                return scopeArr.splice(index, 1);
                            }
                        }
                    });
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map