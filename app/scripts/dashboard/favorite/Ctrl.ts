/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var FavoriteCtrl,

    _linkClass = 'fave-tab',

    /**
     * @description holds the id of the map stack layer
     * @var {string}
     */
    _mapStackID = 'favorite';

/**
 * @class
 * @classdesc handles all events that said user is taking part in or hosting
 */
FavoriteCtrl = function(
    $scope,
    Favorite,
    Maps,
    Util,
    Config,
    DataBus
) {
    this._$scope = $scope;
    this._Favorite = Favorite;
    this._Maps = Maps;
    this._Util = Util;
    this._Config = Config;

    this.get();
    this.sections = null;
    this.onUpdate = 0;
    this.onBuild = this.onBuild.bind(this);
    this.linkClass = _linkClass;

    // Define the map layer 
    this.mapSet = {
        disableCluster: true,
        events: {
            click: this.markerClick.bind(this)
        }
    };

    this.mapInstance = this._Maps.getInstance();
    this.mapInstance.selectSet(_mapStackID, this.mapSet);

    var faveChangeCallback = this._onFaveChange.bind(this);
    DataBus.on('/user/favorite', faveChangeCallback);

    this._$scope.$on('$destroy', function() {

        DataBus.removeListener('/user/favorite', faveChangeCallback);
        if (this.mapInstance) {
            this.mapInstance.deselectSet();
        }
    }.bind(this));
};

FavoriteCtrl.prototype = _.extend(FavoriteCtrl.prototype, {

    /**
     * @description fetches the users favorites from the server
     * @name FavoriteCtrl#get
     */
    get: function () {
        var self = this;
        return this._Favorite.fetch().then(function(folders) {
            self.sections = _.map(folders, function(folder, name) {
                return {
                    userIDs: folder,
                    hideEmpty: true,
                    translatedTitle: name !== 'all' ? name : null,
                    folder: name
                };
            });
            self.onUpdate++;
            self._$scope.$root.safeDigest(self._$scope);
        });
    },

    /**
     * @description called from the peopleDisplay directive when the users have been assembled/parsed
     * @name FavoriteCtrl#onBuild
     * @callback
     * @param {array<object>} sections
     */
    onBuild: function (sections) {
        if (!sections.length) {
            return true;
        }

        // {array<object>} assembles an array of users 
        var usersObj = _.reduce(sections, function (next, section) {
            // Delete any deleted users from our list in the DB 
            if (section.deleted) {
                this._Favorite.bulkRemove(section.deleted, section.folder);
            }
            if (!next) {
                next = section.users;
            } else {
                next = _.extend(next, section.users);
            }
            return next;
        }.bind(this), null);

        // Adds each marker to the map 
        return _.delay(function () {
            var instance = this._Maps.getInstance();

            _.each(usersObj, function (_user) {
                var user = _.clone(_user);
                user.$elem = $.bind($, '.' + _linkClass + '-' + user.userID);

                return instance.createMarker({
                    id: _linkClass + '-' + user.userID,
                    lat: user.locations[0].coords.lat,
                    lon: user.locations[0].coords.lon,
                    data: user,
                    icon: this._Config.map.icon.xs
                }, _mapStackID);
            }, this);
            return _.defer(function () {
                return instance.center();
            });
        }.bind(this), 500);
    },

    /**
     * @description called map marker click
     * @name FavoriteCtrl#markerClick
     */
    markerClick: function (marker) {
        var data = marker.model.data,
            instance = this._Maps.getInstance();

        this._Util.$hilite(data.$elem(), 2000);
        instance.centerMap(data.coords, marker.map.getZoom() + 1);
    },

    _getSection: function (folderName) {
        var i = this.sections.length - 1;

        for (;i >= 0; i--) {
            if (this.sections[i].folder === folderName) {
                return this.sections[i];
            }
        }

        return false;
    },

    _onFaveChange: function (data) {
        var folder = this._getSection(data.folder),
            userIDs = folder && folder.userIDs,
            length,
            update;

        if (!folder) {
            return;
        }

        if (data.action === 'unfavorite') {
            length = userIDs.length;
            this._Util.arrRemove(userIDs, data.targetID);
            update = length !== userIDs.length;
        } else if (data.action === 'favorite') {
            update = userIDs.indexOf(data.targetID) === -1;
            update && userIDs.push(data.targetID);
        }

        if (update) {
            this.onUpdate++;
            this._$scope.safeDigest(this._$scope);
        }
    },
});

FavoriteCtrl.$inject = [
    '$scope',
    'Favorite',
    'Maps',
    'Util',
    'Config',
    'DataBus',
];

require('../../app.js').controller('FavoriteCtrl', FavoriteCtrl);
