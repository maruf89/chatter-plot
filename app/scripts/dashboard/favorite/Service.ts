/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

/**
 * @ngdoc service
 * @class
 * @classdesc handles all updating & fetching the user's favorites
 */
var Favorite = function(User, SocketIo, Util, Auth) {
    this._User = User;
    this._SocketIo = SocketIo;
    this._Util = Util;
    this._Auth = Auth;
    return this;
};

Favorite.prototype = _.extend(Favorite.prototype, {
    /**
     * @description grabs all of the favorites for the current user
     * @name Favorite#fetch
     * @param {array<string>=} folders - if passed will only fetch the selected folders
     * @returns {Promise<object>}
     */
    fetch: function(folders) {
        return this._SocketIo.onEmitSock('/favorite/fetch', folders).then(function(result) {
            return result.favorite;
        });
    },

    /**
     * @description either adds or removes a user to a saved list depending on whether they're already favorited
     * @name Favorite#toggle
     * @param {number} userID - the userID of the user to add to the current user's list
     * @param {string} [folder='all'] - the folder to add the user to
     * @returns {Promise<boolean>} returns true if the user was added, and false if they were removed
     */
    toggle: function (userID, folder) {
        var Util, data, folderArr, myID;
        if (folder == null) {
            folder = 'all';
        }

        Util = this._Util;
        myID = this._User.userID;

        if (!this._Auth.isAuthenticated()) {
            return this._Auth.isLoggedIn({
                userID: userID,
                action: 'favorite'
            })
            .then(this.toggle.bind(this, userID, folder));
        }

        if (typeof userID !== 'number' ||
            !myID ||
            myID === userID
        ) {
            return false;
        }

        folderArr = this._User.data.favorite[folder];
        data = {
            method: folderArr.indexOf(userID) === -1 ? 'add' : 'remove',
            targetID: userID,
            folder: folder
        };

        return this._SocketIo.onEmitSock('/favorite/toggle', data).then(function() {
            if (data.method === 'add') {
                folderArr.push(userID);
            } else {
                Util.arrRemove(folderArr, userID);
            }
            return data.method === 'add';
        });
    },

    /**
     * Used primarily to clean up deleted users, this wipes them from a users favorites list
     */
    bulkRemove: function(userIDs, folder) {
        folder = folder || 'all';

        if (!_.isArray(userIDs)) {
            throw new Error('expecting userIDs to be an array');
        }

        return this._SocketIo.onEmitSock('/favorite/bulkRemove', {
            targetIDs: userIDs,
            folder: folder
        }).then(function() {
            this._User.favorite[folder] = _.difference(userIDs, this._User.favorite[folder]);

            return userIDs;
        }.bind(this));
    },

    isFave: function (userID, folder) {
        if (folder == null) {
            folder = 'all';
        }
        return this._User.data && (this._User.data.favorite[folder].indexOf(
            userID) !== -1);
    }
});

Favorite.$inject = ['User', 'SocketIo', 'Util', 'Auth'];

require('../../app.js').service('Favorite', Favorite);
