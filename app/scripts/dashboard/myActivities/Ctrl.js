/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var MyActivitiesCtrl_1 = require('root/activities/MyActivitiesCtrl');
require('../../app').controller('MyActivitiesCtrl', [
    '$scope',
    '$q',
    '$state',
    'Util',
    'Events',
    'Interaction',
    'User',
    'Maps',
    'Activities',
    'DataBus',
    'Action',
    'Venues',
    'localStorageService',
    MyActivitiesCtrl_1.MyActivitiesCtrl,
]);
//# sourceMappingURL=Ctrl.js.map