/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

import {MyActivitiesCtrl} from 'root/activities/MyActivitiesCtrl';

require('../../app').controller('MyActivitiesCtrl', [
    '$scope',
    '$q',
    '$state',
    'Util',
    'Events',
    'Interaction',
    'User',
    'Maps',
    'Activities',
    'DataBus',
    'Action',
    'Venues',
    'localStorageService',
    MyActivitiesCtrl,
]);
