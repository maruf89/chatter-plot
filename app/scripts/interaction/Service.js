/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var App = require('../app');
var Service_1 = require('root/interaction/Service');
App.service('Interaction', [
    '$q',
    'SocketIo',
    'User',
    'Config',
    'DataBus',
    '$sce',
    'InteractionHelper',
    'InteractionMap',
    Service_1.Interaction,
]);
//# sourceMappingURL=Service.js.map