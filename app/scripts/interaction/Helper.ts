/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var Helper = require('root/interaction/Helper'),
    app = require('../app');

app.factory('InteractionHelper', [
    '$state',
    'User',
    'Venues',
    'locale',
    'Util',
    'Config',
    Helper.Map
]);

app.factory('InteractionMap', [
    'Venues',
    '$q',
    Helper.Helper,
]);
