/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../../app').directive('chatWindows', [
    'Interaction', 'User', 'DataBus', 'Auth', '$q', '$templateCache',
    function (Messaging, User, DataBus, Auth, $q, $templateCache) {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            template: $templateCache.get('views/interaction/directives/chatWindows/Template.html'),
            link: {
                post: function (scope) {
                    scope.chat = {
                        talks: {},

                        openMessage: function (message) {
                            Auth.isLoggedIn().then(function () {
                                if (message.to[0].userID === User.userID) {
                                    return $q.reject();
                                }

                                var messageID = _.pluck(message.to, 'userID').sort().join('|');

                                if (!scope.chat.talks[messageID]) {
                                    scope.chat.talks[messageID] = message;
                                    scope.$root.safeDigest(scope);
                                }
                            });
                        },

                        toggleMinimize: function (event, key) {
                            event.preventDefault();
                            $("#chat-" + key).toggleClass('minimize');
                            return false;
                        },

                        closeWindow: function (key) {
                            delete scope.chat.talks[key];
                        },

                        onAuthChange: function () {
                            scope.chat.talks = {};
                        },

                        send: function (talk) {
                            return Messaging.send({
                                to: _.pluck(talk.to, 'userID'),
                                message: talk.message
                            })
                            .then(function (msgID) {
                                talk.message = '';
                                talk.msgID = msgID;
                                scope.$root.safeDigest(scope);
                            });
                        }
                    };

                    DataBus.on('/message/user', scope.chat.openMessage);
                    DataBus.on('/site/authChange', scope.chat.onAuthChange);

                    scope.$on('$destroy', function () {
                        DataBus.removeListener('/message/user', scope.chat.openMessage);
                        DataBus.removeListener('/site/authChange', scope.chat.onAuthChange);
                    });
                }
            }
        };
    }
]);
