/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../../app').directive('requestView', [
    'Interaction', 'Venues', '$templateCache',
    function (Interaction, Venues, $templateCache) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                activity: '=',
            },
            template: $templateCache.get('views/interaction/directives/listing/Template.html'),
            link: {
                pre: function (scope) {
                    scope.Activity = scope.activity;
                    scope.Interaction = Interaction.helper;
                    scope.cache = {
                        venues: Venues.venues,
                    };
                },
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map