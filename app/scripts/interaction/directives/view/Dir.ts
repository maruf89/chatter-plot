/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../../app').directive('requestView', [
    'Interaction', 'Venues', '$templateCache',
    function (
        Interaction:cp.interaction.IService,
        Venues:cp.venue.IService,
        $templateCache:ng.ITemplateCacheService
    ) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                activity:   '=',        // {object} the requset object
            },
            template: $templateCache.get('views/interaction/directives/listing/Template.html'),
            link: {
                pre: function (scope) {
                    scope.Activity = scope.activity;
                    scope.Interaction = Interaction.helper;

                    scope.cache = {
                        venues: Venues.venues,
                    };
                },
            }
        };
    }
]);
