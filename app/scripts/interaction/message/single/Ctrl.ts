/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

/**
 * @module MessageSingleCtrl
 */
'use strict';

var _msgActions = {
    abuse: {
        key: 'standalone.REPORT_ABUSE',
        fn: function (self) {
            return self._abuseModal.activate({
                content: {
                    DISPLAY_TYPE: 'Message/Conversation',
                    SUBTYPE: 'messages',
                    CONTENT_ID: self._$scope.convo.msgID,
                    TIMESTAMP: self._Config.format.date.toString('basic_date_time_no_millis')
                },
                modal: {
                    deactivate: self._abuseModal.deactivate
                }
            });
        }
    }
};

class MessageSingleCtrl {
    public whenFrom:any;

    public scrollTrigger:number;
    public data:any;
    /**
     * @deprecated
     */
    public vars:any;

    private _messageHandle:() => void;

    constructor(
        private _$scope:cp.IScope,
        private _Conversation:any,
        private _User:cp.user.IService,
        private _Messaging:any,
        private _SocketIo:any,
        private _abuseModal:any,
        private _Config:any,
        private _keyboardFilter:any,
        private _locStorage:any
    ) {
        var appendFn, eventKey,
            self = this,
            autoEnter = _locStorage.get('messaging.autoEnter'),
            people = _Conversation.people;

        // If in the route resolve, people were requested, then we need to add them to the service
        if (people) {
            _User.addPeople(people, null, true);
        }

        _$scope.convo = _Conversation.conversation;
        _$scope.messages = _.map(_Conversation.messages.reverse(), function (msg) {
            msg.parsed = _Messaging.parseMessage(msg.message);
            return msg;
        });

        _$scope.people = _Messaging.people;
        this.conversationRead();

        // Part of the auto-scroll functionality to scroll to bottom of the conversation

        // upon each new message - This is just the setup
        this.scrollTrigger = 0;

        _.defer(function () {
            ++self.scrollTrigger;
            _$scope.safeDigest(_$scope);
        });

        this.data = this.vars = {
            message: '',
            ddVisible: false,
            ddOptions: _msgActions,
            autoEnter: false
        };

        if (typeof autoEnter === 'string') {
            this.data.autoEnter = autoEnter === 'true';
        }

        if (!MessageSingleCtrl.prototype.whenFrom) {
            MessageSingleCtrl.prototype.whenFrom = _Messaging.whenFrom.bind(_Messaging);
        }

        appendFn = this.newMsg.bind(this);
        eventKey = _Messaging.generateKey(_Conversation.conversation.msgID);

        // Join the socket room
        this._messageHandle = this._SocketIo.join(eventKey, appendFn);
        _$scope.$on('$destroy', function () {
            // remove the handle on destroy so we don't get duplicate calls if we rejoin the same room
            self._messageHandle.remove();
        });
    }

    isMe(userID) {
        return userID === this._User.userID ? 'me' : '';
    }

    sendMsg(event) {
        // prevent a page refresh
        event.preventDefault();

        var self = this,
            message = this.data.message;

        self.data.message = '';

        if (!message) {
            return false;
        }

        return this._Messaging.send({
            to: this._Conversation.conversation.participants,
            message: message,
            channel: this._messageHandle.channel
        })
            .then(function () {
                $('.write')[0].focus();
            }).catch(function () {
                self.data.message = message;
            }).finally(function () {
                self._$scope.$root.safeDigest(self._$scope);
            });
    }

    enterCheck(event) {
        if (this.data.autoEnter && this._keyboardFilter(event.keyCode) === 'enter') {
            return this.sendMsg();
        }
    }

    autoEnterChange() {
        return this._locStorage.set('messaging.autoEnter', this.data.autoEnter);
    }

    /**
     * When a new message comes in will append the message to the body and notify the notifications
     *
     * @param {object} msg  the message object received from the server
     */
    newMsg(msg) {
        var self = this;

        // Append the message
        this.appendMsg(msg);

        // Notify the notification that this has been viewed
        _.delay(function () {
            self._Messaging.markRead(self._$scope.convo.msgID);
        }, 25);
    }

    appendMsg(msg) {
        this.scrollTrigger++;

        msg.parsed = this._Messaging.parseMessage(msg.message);
        this._$scope.messages.push(msg);
        this._$scope.$root.safeDigest(this._$scope);
    }

    conversationRead() {
        var conversation = this._Conversation.conversation;

        // Update notifications
        return this._Messaging.markRead(conversation.msgID);
    }
}

MessageSingleCtrl.$inject = [
    '$scope',
    'Conversation',
    'User',
    'Interaction',
    'SocketIo',
    'abuseModal',
    'Config',
    'keyboardFilter',
    'localStorageService',
];

require('../../../app').controller('MessageSingleCtrl', MessageSingleCtrl);
