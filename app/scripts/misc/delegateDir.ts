/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';
require('../app').directive('delegate', [
    '$parse',
    function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, elem, attributes) {
                var bind;
                bind = function (chunk) {
                    var action, expression, expressionHandler, ref,
                        selector;
                    ref = chunk.trim()
                        .split('|'), selector = ref[0], expression =
                        ref[1], action = ref[2];
                    expressionHandler = $parse(expression);
                    action = action || 'click';
                    action += '.delegate';
                    elem.on(action, selector, function (ev) {
                        var localScope;
                        localScope = angular.element(ev.target)
                            .scope();
                        return localScope.$apply(function () {
                            return expressionHandler(
                                localScope, {
                                    $event: ev
                                });
                        });
                    });
                    return scope.$on('$destroy', function (ev) {
                        return elem.off('click.delegate');
                    });
                };

                // Allow for multiple delegations
                return angular.forEach(attributes.delegate.trim()
                    .split(';'), bind);
            }
        };
    }
]);
