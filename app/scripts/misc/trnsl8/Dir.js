/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
/**
 * This directive is used to add functionality to translated text
 *
 * A sample translation looks like:
 *
 * {
 *     "SOME_KEY": "No events in your area? <a trnsl8='landingHostTrnsl8'>Take initiative by becoming a host!</a>"
 * }
 *
 * this will load the landingHostTrnsl8 controller
 */
require('../../app').directive('trnsl8', [
    function () {
        return {
            restrict: 'A',
            scope: {},
            controller: '@',
            name: 'trnsl8'
        };
    }
]);
//# sourceMappingURL=Dir.js.map