/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
/**
 * ngdoc directive
 * @namespace GoogleLocationDirective
 */
'use strict';
var googleLoc = require('front/util/googleLocation'), 
/**
 * @description Google autocomplete input keydown listener - auto selects first result if enter click
 * when no selection selected - http://stackoverflow.com/a/11703018/3808105
 * @param {HTMLElement} input (text)
 */
enterFirstResultWrapper = function (input) {
    // store the original event binding function
    var _addEventListener = (input.addEventListener) ? input.addEventListener : input.attachEvent, $input = $(input);
    function addEventListenerWrapper(type, listener) {
        // Simulate a 'down arrow' keypress on hitting 'return' when no pac suggestion is selected,
        // and then trigger the original listener.
        if (type == "keydown") {
            var orig_listener = listener;
            listener = function (event) {
                var suggestion_selected = $(".pac-item-selected").length > 0, simulated_downarrow;
                if (event.which == 13 && !suggestion_selected) {
                    simulated_downarrow = $.Event("keydown", {
                        keyCode: 40,
                        which: 40
                    });
                    orig_listener.apply(input, [simulated_downarrow]);
                    if (!$input.data('selected')) {
                        event.preventDefault();
                    }
                }
                orig_listener.apply(input, [event]);
            };
        }
        _addEventListener.apply(input, [type, listener]);
    }
    input.addEventListener = addEventListenerWrapper;
    input.attachEvent = addEventListenerWrapper;
};
require('../../../app.js').directive('googleLocation', [
    'uiGmapGoogleMapApi', 'locale', 'Util', 'DataBus', 'keyboardFilter', '$templateCache',
    function (GoogleMapApi, locale, Util, DataBus, keyboardFilter, $templateCache) {
        return {
            restrict: 'E',
            replace: true,
            require: '^form',
            scope: {
                source: '=?',
                displaySrc: '=?',
                onSelect: '&',
                onEmpty: '&',
                doEmpty: '=?',
                types: '@',
                required: '=?',
                update: '=?',
                curLocation: '@',
                invalidate: '=?',
                invalidError: '@',
                showIcon: '@',
                placeholder: '@',
                noGeneral: '@',
                getCurLoc: '@',
                zoomLevel: '=?',
                syncKey: '@',
            },
            template: $templateCache.get('views/misc/form/googleLocation/Template.html'),
            link: {
                pre: function (scope, iElem, iAttrs, Form) {
                    var placeholder, syncKey = iAttrs.syncKey;
                    scope.vars = {
                        inputID: "googleLocation-" + scope.$id,
                        placeholder: null,
                        defaultValue: '',
                        curLocation: iAttrs.curLocation === 'true',
                        syncObj: null,
                    };
                    if (iAttrs.update) {
                        scope.$watch('update', function (post, prev) {
                            if (post !== prev) {
                                googleLoc.setDefault(scope, Util);
                            }
                        });
                    }
                    googleLoc.setDefault(scope, Util);
                    placeholder = iAttrs.placeholder || 'activities.PLACEHOLDER-FAKE_DESIRED_LOC';
                    locale.ready(locale.getPath(placeholder)).then(function () {
                        scope.vars.placeholder = locale.getString(placeholder);
                    });
                    googleLoc.initSync(syncKey, scope.vars, scope.$root);
                    scope.Form = Form;
                },
                post: function (scope, iElem, iAttrs) {
                    var vars = scope.vars, selected = false, invalidationUpdate, input;
                    vars.getCurrentLocation = function () {
                        DataBus.emit('progressLoader', { start: true });
                        locale.ready('common').then(function () {
                            vars.defaultValue = locale.getString('common.SEARCHING');
                        });
                        return Util.geo.getCurrentLocation().then(function (coords) {
                            if (scope.onSelect) {
                                scope.onSelect({
                                    location: coords,
                                    type: 'CURRENT',
                                    passive: false,
                                });
                            }
                            return locale.ready('common')
                                .then(function () {
                                vars.defaultValue = locale.getString('common.MY_LOCATION');
                                DataBus.emit('progressLoader');
                                scope.$root.safeDigest(scope);
                            });
                        });
                    };
                    /**
                     * @context googleMaps
                     */
                    vars.locationSelected = function (isSyncOrLoc) {
                        selected = false;
                        var isSync = typeof isSyncOrLoc === 'boolean' && isSyncOrLoc, location = typeof this.getPlace === 'function' ? this.getPlace() : this;
                        googleLoc.locationSelected(location, vars, isSync);
                        if (!location.geometry) {
                            invalidationUpdate(false);
                            return DataBus.emit('yap', {
                                type: 'warning',
                                title: 'updates.WARNING',
                                body: vars.googleError = 'updates.WARNING-INVALID_GOOG_LOC',
                            });
                        }
                        else if (scope.noGeneral && googleLoc.isLocationVague(location)) {
                            invalidationUpdate(false);
                            return DataBus.emit('yap', {
                                type: 'warning',
                                title: 'updates.WARNING',
                                body: vars.googleError = 'updates.WARNING-MORE_SPECIFIC_GOOG_LOC',
                                duration: 4250,
                            });
                        }
                        invalidationUpdate(true);
                        if (iAttrs.onSelect) {
                            scope.onSelect({
                                location: location,
                                type: 'GOOGLE',
                                passive: isSync,
                            });
                        }
                        $(input).data('selected', true);
                        if (scope.required || iAttrs.source) {
                            scope.source = location;
                        }
                        scope.$root.safeDigest(scope);
                    };
                    /**
                     * @description Updates the location field to be valid/invalid
                     * @param {boolean} val - pass true to validate
                     */
                    invalidationUpdate = function (val) {
                        (scope.Form.loc || scope.Form).$setValidity('invalidate', val);
                    };
                    if (iAttrs.invalidate && typeof scope.invalidate === 'boolean') {
                        scope.$watch('invalidate', function (post, prev) {
                            if (post !== prev) {
                                invalidationUpdate(post);
                            }
                        });
                        invalidationUpdate(scope.invalidate);
                    }
                    if (iAttrs.onEmpty) {
                        iElem.on('keyup', '.input-location', function (event) {
                            if (selected && !event.currentTarget.value) {
                                scope.onEmpty();
                                selected = false;
                            }
                        });
                    }
                    if (iAttrs.doEmpty) {
                        scope.$watch('doEmpty', function (post, prev) {
                            if (post !== prev) {
                                vars.defaultValue = '';
                            }
                        });
                    }
                    if (iAttrs.getCurLoc === 'true') {
                        googleLoc.getCurrentLocation(Util).then(function (location) {
                            if (iAttrs.onSelect) {
                                scope.onSelect({
                                    location: location,
                                    type: 'GOOGLE',
                                    passive: true // since not user triggered
                                });
                            }
                            $(input).data('selected', true);
                            if (scope.required || iAttrs.source) {
                                scope.source = location;
                            }
                            scope.$root.safeDigest(scope);
                        });
                    }
                    _.defer(function () {
                        var options = {}, ref;
                        input = iElem[0].querySelector('#' + vars.inputID);
                        enterFirstResultWrapper(input);
                        if (iAttrs.types) {
                            options.types = scope.types.split(',');
                        }
                        if ((ref = scope.source) != null ? ref.coords : void 0) {
                            options.radius = 50 * 1000;
                        }
                        GoogleMapApi.then(function (maps) {
                            var autocomplete = new maps.places.Autocomplete(input, options);
                            maps.event.addListener(autocomplete, 'place_changed', vars.locationSelected);
                        });
                        $(input)
                            .on('focus', function (event) {
                            _.defer(function () {
                                selected = true;
                                event.target.select();
                            });
                        })
                            .on('keydown', function (event) {
                            if (keyboardFilter(event.keyCode) === 'enter' && selected) {
                                selected = false;
                                event.preventDefault();
                            }
                        });
                    });
                    scope.$on('$destroy', function () {
                        if (vars.syncObj) {
                            googleLoc.removeSync(Util, vars);
                        }
                    });
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map