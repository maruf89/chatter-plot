/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var skillLevels = require('root/config/languageLevels'), skillLevelLength = _.keys(skillLevels).length, translated = null, translating = false;
require('../../../app.js').directive('skillSelect', [
    'locale',
    function (locale) {
        locale.ready('profile');
        return {
            restrict: 'E',
            replace: true,
            scope: {
                source: '=',
                name: '@',
                min: '=?',
                max: '=?',
                onChange: '&',
                showDefault: '@',
                showNative: '@'
            },
            templateUrl: 'views/misc/form/skillSelect/Template',
            link: {
                pre: function (scope, iElem, iAttrs) {
                    var levelsLength = skillLevelLength, skillz = _.clone(skillLevels);
                    if (!scope.showNative) {
                        levelsLength -= 1;
                        delete skillz[7];
                    }
                    scope.vars = {
                        levels: skillz,
                        current: null,
                    };
                    _.each(skillz, function (skill) {
                        if (scope.source.level === skill.id) {
                            scope.vars.current = skill;
                        }
                    });
                    if (scope.showDefault) {
                        locale.ready('profile').then(function () {
                            var option = document.createElement('option');
                            option.value = '';
                            option.text = locale.getString('profile.SELECT_LEVEL');
                            iElem.find('select').prepend(option);
                        });
                    }
                    if (!(translated || translating)) {
                        // to prevent duplicate processing
                        translating = true;
                        locale.ready('profile')
                            .then(function () {
                            _.each(skillLevels, function (skill, key) {
                                skillLevels[key].display = locale.getString("profile." + skill.key);
                                if (skill.cefr) {
                                    return skillLevels[key].display += " - " + skill.cefr;
                                }
                            });
                            scope.$root.safeDigest(scope);
                            translated = true;
                            translating = false;
                        });
                    }
                    // if watching the minimum, than it must be the maximum itself
                    if (iAttrs.min) {
                        scope.$watch('min.level', function (post, prev) {
                            var num;
                            if (post !== prev) {
                                num = p(post);
                                var i, j, ref;
                                for (i = j = 1, ref =
                                    levelsLength; 1 <=
                                    ref ? j <= ref :
                                    j >= ref; i = 1 <=
                                    ref ? ++j : --j) {
                                    if (i < num) {
                                        delete scope.vars.levels[i];
                                    }
                                    else {
                                        scope.vars.levels[i] = scope.vars.levels[i] || skillLevels[i];
                                    }
                                }
                                scope.$root.safeDigest(scope);
                            }
                        });
                    }
                    // same with max
                    if (iAttrs.max) {
                        return scope.$watch('max.level', function (post, prev) {
                            var num;
                            if (post !== prev) {
                                num = p(post);
                                var i, j, ref;
                                for (i = j = 1, ref =
                                    levelsLength; 1 <=
                                    ref ? j <= ref :
                                    j >= ref; i = 1 <=
                                    ref ? ++j : --j) {
                                    if (i > num) {
                                        delete scope.vars.levels[i];
                                    }
                                    else {
                                        scope.vars.levels[i] = scope.vars.levels[i] || skillLevels[i];
                                    }
                                }
                                scope.$root.safeDigest(scope);
                            }
                        });
                    }
                },
                post: function (scope, iElem, iAttrs) {
                    if (iAttrs.onChange) {
                        iElem.on('change', 'select', function (event) {
                            scope.onChange({ event: event });
                            scope.$root.safeDigest(scope);
                        });
                    }
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map