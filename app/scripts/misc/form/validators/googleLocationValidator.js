/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
/**
 * This validator is for the `googleLocation` directive. It ensures that a
 * valid location is set if the input is a required field
 */
require('../../../app').directive('googleLocationValidator', [
    '$q',
    function ($q) {
        return {
            require: 'ngModel',
            scope: {
                source: '=googleLocationValidator'
            },
            link: function (scope, iElem, iAttrs, ngModel) {
                var curPromise, location;
                curPromise = null;
                location = null;
                scope.$watch('source', function (post, prev) {
                    if (post && _.isPlainObject(post) && ((post.scope &&
                        post.scope === 'GOOGLE') || (post.source && post.source.name ===
                        'GOOGLE') || post.coords)) {
                        ngModel.$setValidity('googleLocation', true);
                        return location = true;
                    }
                });
                return ngModel.$validators.googleLocation = function (val) {
                    if (!val) {
                        location = false;
                    }
                    return location || !iAttrs.required;
                };
            }
        };
    }
]);
//# sourceMappingURL=googleLocationValidator.js.map