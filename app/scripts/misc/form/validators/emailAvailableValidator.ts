/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../../app').directive('emailAvailableValidator', [
    'SocketIo', 'DataBus', '$q',
    function (SocketIo, DataBus, $q) {
        return {
            require: 'ngModel',
            link: function (scope, elem, attrs, ngModel) {
                return ngModel.$asyncValidators.emailAvailable =
                    function (value) {
                        var deferred;
                        if (!value) {
                            return $q.reject(false);
                        }
                        deferred = $q.defer();
                        SocketIo.onEmitSock('/email/exists', value)
                            .then(function (exists) {
                                if (!exists) {
                                    return deferred.resolve();
                                } else {
                                    return deferred.reject();
                                }
                            });
                        return deferred.promise;
                    };
            }
        };
    }
]);
