/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../../app').directive('timePicker', [
    'DataBus',
    'Config',
    '$templateCache',
    require('root/directives/TimePicker').directive,
]);
