/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var moment = require('moment');

require('../../../app').directive('datePicker', [
    '$timeout', 'locale', 'Config',
    function ($timeout, locale, Config) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                source: '=',
                onChange: '&',
                required: '=',
                minDate: '=?',
                format: '@'
                // placeholder:    '@'
                // disablePast:    '@'
            },
            templateUrl: 'views/misc/form/datePicker/Template.html',
            link: {
                pre: function (scope, iElem, iAttrs) {
                    var dateBox, placeholder,
                        safetyTimer = null,
                        safetyFn = function () {
                            return safetyTimer = false;
                        },

                        localeData = moment.localeData(moment.locale());

                    scope.vars = {
                        dateShow: null,
                        placeholder: null,
                        display: scope.source instanceof Date ?
                            Config.format.date.display(scope.source,
                                scope.format) : '',
                        showPicker: function () {
                            scope.vars.dateShow = true;
                            safetyTimer = true;
                            return $timeout(safetyFn, 200);
                        }
                    };
                    placeholder = iAttrs.placeholder || 'events.PLACEHOLDER-SELECT_DATE';

                    locale.ready(locale.getPath(placeholder))
                        .then(function () {
                            return scope.vars.placeholder = locale.getString(placeholder);
                        });

                    scope.options = {
                        disablePast: !!iAttrs.disablePast,
                        onChange: function (selectedDate) {
                            scope.vars.display = Config.format.date.display(scope.source, scope.format);
                            scope.vars.dateShow = false;

                            if (iAttrs.onChange) {
                                return scope.onChange(selectedDate);
                            }
                        }
                    };

                    if (localeData._months) {
                        scope.options.monthNames = localeData._months;
                    }

                    if (localeData._weekdaysMin) {
                        scope.options.dayNames = _.map(localeData._weekdaysMin, function (day) {
                            return day[0] + day[1];
                        });
                    }

                    dateBox = iElem.find('.date-box');
                    scope.vars.onClose = function (event) {
                        if (safetyTimer || !scope.vars.dateShow) {
                            return true;
                        }
                        if (dateBox[0].tagName !== event.target.tagName &&
                            !dateBox.has(event.target)
                            .length) {
                            return scope.$root.safeApply(function () {
                                return scope.vars.dateShow = false;
                            });
                        }
                    };
                    return CP.Cache.$document.on('click', scope.vars.onClose);
                }
            }
        };
    }
]);
