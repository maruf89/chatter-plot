/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../app').directive('viewTitle', ['$rootScope', 'locale', function ($rootScope, locale) {
    return {
        restrict: 'EA',
        link: function (scope, iElem) {
            // If we've been inserted as an element then we detach from the DOM because the caller
            // doesn't want us to have any visual impact in the document.
            // Otherwise, we're piggy-backing on an existing element so we'll just leave it alone.
            var tagName = iElem[0].tagName.toLowerCase();
            if (tagName === 'view-title' || tagName === 'viewtitle') {
                iElem.remove();
            }

            scope.$watch(
                function () {
                    return iElem[0].innerText;
                },
                function (newTitle) {
                    var path = newTitle && locale.getPath(newTitle);

                    if (path) {
                        locale.ready(path).then(function () {
                            $rootScope.viewTitle = locale.getString(newTitle);
                        });
                    }
                }
            );

            scope.$on('$destroy', function () {
                delete $rootScope.viewTitle;
            });
        }
    };
}])

.directive('viewHead', ['locale', function (locale) {
    var head = window.angular.element(document.head);
    return {
        restrict: 'A',
        link: function (scope, iElem, iAttrs) {
            var i18n,
                newMeta = document.createElement('meta');


            // Move the element into the head of the document.
            // Although the physical location of the document changes, the element remains
            // bound to the scope in which it was declared, so it can refer to variables from
            // the view scope if necessary.
            _.defer(function () {
                _.defer(function () {
                    iElem.remove();
                    newMeta.content = iAttrs.content;
                });
            });

            if (iAttrs.property) {
                newMeta.setAttribute('property', iAttrs.property);
            }

            if (iAttrs.name) {
                newMeta.name = iAttrs.name;
            }

            if (iAttrs.i18n) {
                i18n = iAttrs.content;
                locale.ready(locale.getPath(i18n)).then(function () {
                    newMeta.content = locale.getString(i18n);
                });
            }

            head.append(newMeta);

            // When the scope is destroyed, remove the element.
            // This is on the assumption that we're being used in some sort of view scope.
            // It doesn't make sense to use this directive outside of the view, and nor does it
            // make sense to use it inside other scope-creating directives like ng-repeat.
            scope.$on('$destroy', function () {
                $(newMeta).remove();
            });
        }
    };
}]);
