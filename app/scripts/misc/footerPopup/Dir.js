/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../app').directive('footerPopup', [
    '$analytics', '$templateCache',
    function ($analytics, $templateCache) {
        return {
            restrict: 'EA',
            scope: false,
            replace: true,
            transclude: true,
            template: $templateCache.get('views/misc/footerPopup/Template.html'),
            link: {
                post: function (scope, iElem) {
                    var _trackingCounter, toggleFooter;
                    _trackingCounter = 0;
                    toggleFooter = function () {
                        iElem.toggleClass('active');
                        // Don't track closes, only opens
                        if (++_trackingCounter % 2) {
                            return $analytics.eventTrack('click', {
                                category: 'maps',
                                label: 'select language (show footer)'
                            });
                        }
                    };
                    return iElem.on('click', '.trigger', toggleFooter);
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map