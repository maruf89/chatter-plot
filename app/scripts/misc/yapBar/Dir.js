/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
/**
 * @ngdoc directive
 * @module yapBarDirective
 * @description Yap Bar - Utility module to show notifications to the user for cases of
 *           Success, Info, Warning or Error
 *
 * @requires DataBus
 *
 * Methods & Usage:
 *     yap: Instantly shows a popup message with the given Message Object
 *          if passed. If no message passed, will show the next queued message
 *
 * @example
 *      DataBus.emit('yap', messageObject) // show messageObject
 *      DataBus.emit('yap') // show queued message
 *
 *      // yapSoon - Queues a Message Object
 *      DataBus.emit('yapSoon', messageObject)
 *
 *      // yapNextPage - Queues a message to be show on the next route change
 *      DataBus.emit('yapNextPage', messageObject)
 *
 *      // yapServerResponse - Displays result from server
 *      DataBus.emit('yapServerResponse', responseObject)
 *
 * messageObject
 * @param {object} msg - Object with message data
 * @param {number} [msg.duration=2250] - How long in milliseconds the message should be visible before dissappearing
 * @param {string} msg.type - One of (success|warning|info|error)
 * @param {string} msg.title - Title for the popup
 * @param {string} msg.body - Body content of the popup
 */
var set = function (queue, message) {
    return queue.push(message);
}, yap = function (queue, index, message) {
    var duration, self = this;
    if (!message) {
        if (queue.length) {
            message = queue.shift();
        }
        else {
            return false;
        }
    }
    self.messages[index] = message;
    duration = message.duration || 2250;
    duration += 500;
    return setTimeout(function () {
        if (self.messages[index]) {
            return self.$apply(function () {
                self.messages[index] = null;
                try {
                    return delete self.messages[index];
                }
                catch (_error) {
                }
            });
        }
    }, duration);
}, 
/**
 * Server Response types
 * @type {object}
 */
_responses = {
    '1': 'info',
    '2': 'success',
    '3': 'warning',
    '4': 'error',
    '5': 'error',
    '6': 'warning'
};
require('../../app').directive('yapBar', [
    '$rootScope', 'DataBus', 'locale',
    function ($rootScope, Bus, locale) {
        var routeChangeMsg = [], queue = [], counter = 1;
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'views/misc/yapBar/Template.html',
            scope: false,
            link: function (scope) {
                scope.messages = {};
                scope.doeth = {
                    close: function (index, ignore) {
                        if (!ignore && scope.messages[index]) {
                            scope.messages[index] = null;
                            try {
                                return delete scope.messages[index];
                            }
                            catch (e) { }
                        }
                    }
                };
                var _yap = yap.bind(scope, queue);
                Bus.on('yap', function (msg) {
                    locale.ready(locale.getPath(msg.title))
                        .then(function () {
                        msg.title = locale.getString(msg.title);
                        return locale.ready(locale.getPath(msg.body));
                    })
                        .then(function () {
                        msg.body = locale.getString(msg.body);
                        $rootScope.safeApply(function () {
                            _yap(counter++, msg);
                        });
                    });
                });
                Bus.on('yapLog', function (message) {
                    $rootScope.safeApply(function () {
                        _yap(counter++, {
                            body: message,
                            type: 'info',
                            title: 'Log',
                        });
                    });
                });
                Bus.on('yapSoon', queue.push);
                Bus.on('yapNextPage', routeChangeMsg.push);
                Bus.on('yapServerResponse', function (res) {
                    if (!res || !(res.type || res.customMsg)) {
                        throw new Error('Missing either notification type or title props.');
                    }
                    var msg = {
                        type: String(_responses[String(res.response || res.RESPONSE)[0]]),
                        title: null,
                        body: res.text,
                        duration: res.duration
                    };
                    locale.ready('updates')
                        .then(function () {
                        var type = 'updates.' + msg.type.toUpperCase();
                        msg.title = locale.getString(type);
                        return locale.ready(locale.getPath(res.type))
                            .then(function () {
                            msg.body = msg.body || res.customMsg || locale.getString(res.type, res.vars);
                            $rootScope.safeApply(function () {
                                _yap(counter++, msg);
                            });
                        });
                    });
                });
                $rootScope.$on('$stateChangeSuccess', function () {
                    if (routeChangeMsg.length) {
                        return _yap(counter++, routeChangeMsg.shift());
                    }
                });
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map