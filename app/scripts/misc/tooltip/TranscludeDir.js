/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var timeoutMS = 350, teardownMS = 450, disappearMS = 125;
require('../../app').directive('tooltipTransclude', [
    'Config', 'locale', '$timeout',
    function (Config, locale, $timeout) {
        return {
            restrict: 'E',
            replace: true,
            template: '<div class="Tooltip transclude">' +
                '<a ng-if="::!attach" class="icon-help" title=""></a>' +
                '<div class="box ng-hide {{::pos}}" ng-show="vars.active">' +
                '<div class="inner">' +
                '<a class="carrot"></a><div ng-transclude></div>' + '</div>' +
                '</div>' + '</div>',
            transclude: true,
            scope: {
                pos: '@',
                attach: '@'
            },
            link: {
                pre: function (scope) {
                    scope.vars = {
                        active: false
                    };
                },
                post: function (scope, iElem) {
                    var timer = null, $elem = scope.attach ? iElem.parent() : iElem, teardownTimer = null, teardownComplete = true, disappearStart = false, appear = function () {
                        timer = null;
                        scope.vars.active = true;
                        scope.$root.safeDigest(scope);
                    }, disappear = function () {
                        if (!scope.vars.active || !disappearStart) {
                            return disappearStart = false;
                        }
                        disappearStart = false;
                        teardownComplete = false;
                        if (teardownTimer) {
                            $timeout.cancel(teardownTimer);
                            teardownTimer = $timeout(function () {
                                teardownComplete = true;
                            }, teardownMS);
                        }
                        scope.vars.active = false;
                        scope.$root.safeDigest(scope);
                    }, onEnter = function () {
                        disappearStart = false;
                        if (!teardownComplete) {
                            $timeout.cancel(teardownTimer);
                            return appear();
                        }
                        timer = $timeout(appear, timeoutMS);
                    }, onLeave = function () {
                        if (timer) {
                            $timeout.cancel(timer);
                            timer = null;
                        }
                        else {
                            disappearStart = true;
                            $timeout(disappear, disappearMS);
                        }
                    };
                    $elem.on('mouseenter', onEnter).on('mouseleave', onLeave);
                }
            }
        };
    }
]);
//# sourceMappingURL=TranscludeDir.js.map