/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
/**
 * @ngdoc directive
 * @module tooltipDirective
 * @description Creates a hoverable tooltip either as a standalone element, or attaches
 * itself to a parent element
 *
 * @restrict E
 */
'use strict';
var timeoutMS = 350;
require('../../app').directive('tooltip', [
    'Config', 'locale', '$timeout', 'Util',
    function (Config, locale, $timeout, Util) {
        return {
            restrict: 'E',
            replace: true,
            template: '<div class="Tooltip">' +
                '<a ng-if="::!attach" class="icon-help" title=""></a>' +
                '<div class="box ng-hide {{::pos}}" ng-show="vars.active">' +
                '<div class="inner">' +
                '<p class="text"></p><a class="carrot"></a>' +
                '</div>' +
                '</div>' +
                '</div>',
            scope: {
                text: '@',
                translateArr: '@',
                watchVar: '=?',
                pos: '@',
                attach: '@' // {boolean} whether to hide the icon and attach the functionaliy to the host elem
            },
            link: {
                pre: function (scope, iElem) {
                    if (!(scope.text || scope.translateArr || scope.watchVar)) {
                        console.warn('Tooltip directive requires a text property corresponding to a translation key.');
                    }
                    scope.vars = {
                        active: false,
                        content: null
                    };
                    if (!scope.pos) {
                        scope.pos = 'top';
                    }
                    var setVal = function (text) {
                        iElem.find('.text').text(text);
                    }, translateSingle = function (key) {
                        locale.ready(locale.getPath(key)).then(function () {
                            setVal(locale.getString(key));
                        });
                    };
                    if (scope.attach) {
                        iElem.addClass('attach');
                    }
                    if (scope.translateArr) {
                        return Util.concatTranslations(scope.translateArr.split(',')).then(function (vals) {
                            return setVal(vals.join(' '));
                        });
                    }
                    else if (scope.text) {
                        translateSingle(scope.text);
                    }
                    else {
                        scope.$watch('watchVar', function (post, prev) {
                            if (post && post !== prev) {
                                return translateSingle(post);
                            }
                        });
                        translateSingle(scope.watchVar);
                    }
                },
                post: function (scope, iElem) {
                    var timer = null, $elem = scope.attach ? iElem.parent() : iElem, appear = function () {
                        timer = null;
                        scope.vars.active = true;
                        scope.$root.safeDigest(scope);
                    }, disappear = function () {
                        if (!scope.vars.active) {
                            return false;
                        }
                        scope.vars.active = false;
                        scope.$root.safeDigest(scope);
                    }, onEnter = function () {
                        timer = $timeout(appear, timeoutMS);
                    }, onLeave = function () {
                        if (timer) {
                            $timeout.cancel(timer);
                            timer = null;
                        }
                        else {
                            disappear();
                        }
                    };
                    $elem.on('mouseenter', onEnter).on('mouseleave', onLeave);
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map