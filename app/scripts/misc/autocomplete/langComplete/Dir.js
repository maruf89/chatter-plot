/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var updatePlaceholder = function (Autocomplete, hide) {
    return Autocomplete.placeholder = this.langs.length || hide ? '' :
        Autocomplete.placeholder_text;
}, miscFns = require('common/front/modules/misc');
/**
 * @ngdoc directive
 * @description Updates the Autocompletes placeholder value
 * depending on whether the field has any terms
 *
 * @param  {Scope}            scope         Scope object with a placeholder value
 * @param  {AutocompleteCtrl} Autocomplete  Autocomplete controller
 */
require('../../../app').directive('langComplete', [
    'keyboardFilter', 'locale', 'Util', '$templateCache',
    function (keyboardFilter, locale, Util, $templateCache) {
        // start loading the translated languages immediately
        Util.language.getTranslated(true);
        /**
         * @ngdoc object
         * @description If a term option is hovered, or selected via keyboard
         * event, we want to keep it's reference
         *
         * @type {object}
         */
        var activeOption = {
            index: null,
            $elem: null
        }, onArrow = function (scope, Autocomplete, upArrow) {
            var $nextElem, method;
            if (!Autocomplete.suggestions.length) {
                return true;
            }
            if (!activeOption.$elem) {
                method = upArrow ? 'last' : 'first';
                $nextElem = $(this).find('.term-option')[method]();
                return $nextElem.trigger('mouseover');
            }
            method = upArrow ? 'prev' : 'next';
            $nextElem = activeOption.$elem[method]();
            activeOption.$elem.trigger('mouseleave');
            if ($nextElem.length) {
                return $nextElem.trigger('mouseover');
            }
        };
        return {
            restrict: 'E',
            replace: true,
            template: $templateCache.get('views/misc/autocomplete/langComplete/Template.html'),
            scope: {
                icon: '@',
                langs: '=?',
                exclude: '=?',
                placeholder: '@',
                disabled: '=?',
                onAdd: '&',
                onRemove: '&',
                onFocus: '&',
                onBlur: '&',
                required: '=?',
                Autocomplete: '=?ref',
                allowAll: '@',
                showDefaults: '@',
            },
            link: {
                pre: function (scope, iElem, iAttrs) {
                    scope.vars = {
                        disabled: scope.disabled,
                        $touched: false,
                        isEmpty: !(scope.langs && scope.langs.length && scope.langs[0].languageID),
                        fieldName: iAttrs.name || 'langComplete'
                    };
                    if (scope.vars.isEmpty) {
                        scope.langs = scope.langs || [];
                    }
                    else {
                        Util.language.expandPossible(scope.langs)
                            .then(function (complete) {
                            return scope.langs = complete;
                        });
                    }
                    if (iAttrs.disabled) {
                        return scope.$watch('disabled', function (newVal) {
                            scope.vars.disabled = newVal;
                        });
                    }
                },
                post: function (scope, iElem, iAttrs) {
                    // Store the namespace of this instance for managing events
                    var $input, AC, _updatePlaceholder, addTerm, placeholder, removeTerm, toggleDefaultsBox, updateRequired, vars, NS = ".tt" + scope.$id, languages = null, isIE = miscFns.getInternetExplorerVersion() !== -1;
                    // support referencing from outside
                    scope.Autocomplete = AC = scope.Autocomplete || {};
                    _.extend(AC, {
                        autocomplete: iAttrs.autocomplete,
                        onAdd: iAttrs.onAdd ? scope.onAdd : false,
                        onRemove: iAttrs.onRemove ? scope.onRemove : false,
                        suggestions: []
                    });
                    // Create our own bound version to not have to add the scope & reference every time
                    _updatePlaceholder = updatePlaceholder.bind(scope, AC);
                    AC.exclude = scope.exclude || [];
                    if (!iAttrs.exclude && scope.langs.length) {
                        AC.exclude = _.pluck(scope.langs, 'term');
                    }
                    // Local template variables
                    vars = scope.vars;
                    // What our template will listen to
                    updateRequired = function () {
                        return vars.required = scope.required ? vars
                            .isEmpty : false;
                    };
                    if (iAttrs.required) {
                        scope.$watch('required', updateRequired);
                    }
                    if (iAttrs.onFocus) {
                        iElem.find('.active-term')
                            .on('focus', _.debounce(function (event) {
                            return scope.onFocus({
                                event: event,
                                languages: scope.langs
                            });
                        }, 250, { leading: true, trailing: false }));
                    }
                    placeholder = iAttrs.placeholder || 'profile.PLACEHOLDER-ADD_LANGUAGE';
                    locale.ready(locale.getPath(placeholder))
                        .then(function () {
                        AC.placeholder_text = locale.getString(placeholder);
                        _updatePlaceholder();
                        scope.$root.safeDigest(scope);
                    });
                    $input = $(iElem).find('.active-term');
                    // IE9 does not support placeholders
                    if ($.fn.placeholder) {
                        $input.attr('placeholder', AC.placeholder_text).placeholder();
                    }
                    vars.getLangKey = function (language) {
                        var languageID = typeof language === 'object' ? language.languageID : language;
                        return Util.language.getI18nKey(languageID);
                    };
                    /**
                     * @description get's called when a language is selected
                     * Also has the option to set hooks which halt further execution depending on the response
                     *      Hook Responses: true  - halts execution after calling $digest
                     *                      1     - same as true except also updates the placeholder
                     *                      false - continue add the language to our list and $digest
                     * @param term
                     * @param id
                     * @returns {*}
                     */
                    addTerm = function (term, id) {
                        var hookRes;
                        // hide the placeholder text
                        _updatePlaceholder(true);
                        // reset the active option
                        activeOption = {};
                        // remove the options
                        $input.typeahead('val', '');
                        // Call hook
                        if (AC.onAdd && (hookRes = AC.onAdd({
                            languageID: id,
                            name: term
                        })) === false) {
                            scope.$root.safeDigest(scope);
                            return false;
                        }
                        // disable required flag because it's not empty
                        vars.isEmpty = vars.required = false;
                        // focus on the input
                        $input.focus();
                        // set the input to empty
                        $input[0].value = '';
                        if (hookRes === 1) {
                            return _updatePlaceholder();
                        }
                        scope.langs.push({
                            languageID: id,
                            name: term
                        });
                        AC.exclude.push(term);
                        scope.$root.safeDigest(scope);
                    };
                    removeTerm = function (index) {
                        if (index == null) {
                            index = scope.langs.length - 1;
                        }
                        if (index < 0) {
                            return;
                        }
                        // Call hook
                        if (AC.onRemove && (AC.onRemove(index) === false)) {
                            return false;
                        }
                        scope.langs.splice(index, 1);
                        // Enable ng-required if necessary
                        if (scope.langs.length) {
                            AC.exclude.splice(index, 1);
                        }
                        else {
                            AC.exclude.length = 0;
                            vars.isEmpty = true;
                            vars.required = scope.required;
                        }
                        _updatePlaceholder();
                        $input.focus();
                        scope.$root.safeDigest(scope);
                    };
                    var triggerInput = (function () {
                        // check for IE since typeahead does
                        var trigger = isIE ? 'paste' : 'input';
                        return function (val) {
                            $input[0].value = val;
                            $input.trigger(trigger);
                        };
                    })();
                    toggleDefaultsBox = _.debounce(function () {
                        if ($input[0].value === '') {
                            triggerInput('…');
                            if (isIE) {
                                return _.defer(function () {
                                    triggerInput('');
                                    _updatePlaceholder(true);
                                });
                            }
                            triggerInput('');
                            _updatePlaceholder(true);
                        }
                    }, { leading: true, trailing: false });
                    // Adds a box term
                    iElem.on('click', '.tt-suggestion', function () {
                        // get the autocompleted text
                        var id, term = $input.val();
                        // If index not found return poop
                        if ((id = _.indexOf(languages, term)) === -1) {
                            return false;
                        }
                        // if our languages array doesn't have an 'All Languages' value at
                        // index 0, then increment the index to use the selected languages correct ID
                        !scope.allowAll && id++;
                        addTerm(term, id);
                    })
                        .on('click', '.term-remove', function (event) {
                        // Removes a term by clicking the `X`
                        var index = $(event.target).parent().index();
                        removeTerm(index);
                    })
                        .on('click', '.term-editor', function () {
                        // Focus on the input after selecting a term from the list
                        $input.focus();
                    })
                        .one('blur', '.active-term', function () {
                        // Updates the `vars.touched` variable on blur
                        vars.$touched = true;
                        scope.$root.safeDigest(scope);
                    })
                        .on('mouseover', '.tt-suggestion', function (event) {
                        // Adds a highlight class to the term
                        activeOption = {
                            $elem: $(event.target),
                            index: $(event.target).index()
                        };
                        activeOption.$elem.addClass('highlight');
                    })
                        .on('mouseleave', '.tt-suggestion', function (event) {
                        // Remove hover state from options
                        if (event.target === activeOption.$elem[0]) {
                            activeOption = {};
                        }
                        angular.element(event.target).removeClass('highlight');
                    })
                        .on('click', '.term-option', function (event) {
                        // Adds a term from the autocomplete list
                        addTerm(event.target.innerText);
                    })
                        .on('keydown', '.active-term', function (event) {
                        // Detect certain keyboard events from the input
                        AC.isFocused = true;
                        var args = [scope, AC], val;
                        switch (keyboardFilter(event.keyCode)) {
                            case 'down arrow':
                            case 'tab':
                                onArrow.apply(iElem, args);
                                break;
                            case 'up arrow':
                                onArrow.apply(iElem, args.concat([true]));
                                break;
                            case 'enter':
                                if (activeOption.$elem) {
                                    return activeOption.$elem.trigger('click');
                                }
                                val = this.value.toLowerCase();
                                _.each(AC.suggestions, function (language) {
                                    if (language.value.toLowerCase() === val) {
                                        addTerm(language.value, language.id);
                                        event.preventDefault();
                                    }
                                });
                                break;
                            case 'escape':
                                if (AC.suggestions.length) {
                                    AC.isFocused = false;
                                    scope.$root.safeDigest(scope);
                                }
                                break;
                            case 'backspace':
                                _.defer((function () {
                                    // Return true if backspacing text
                                    if (this.value) {
                                        return true;
                                    }
                                    // If no text then it will show the defaults drop down
                                    if (scope.showDefaults) {
                                        toggleDefaultsBox();
                                    }
                                    if (scope.langs.length) {
                                        return removeTerm();
                                    }
                                })
                                    .bind(this));
                                break;
                        }
                    })
                        .on('keyup', '.active-term', function (event) {
                        // Make sure key is a single letter so s + alt wouldn't match 'salt' but sal + t would
                        var suggestion;
                        if (AC.suggestions && AC.suggestions.length === 1) {
                            suggestion = AC.suggestions[0];
                            // make sure that the current input === to the only suggestion & that the user's not backspacing
                            if (this.value.toLowerCase() ===
                                suggestion.value.toLowerCase() &&
                                keyboardFilter(event.keyCode) !== 'backspace') {
                                return addTerm(suggestion.value, suggestion.id);
                            }
                        }
                    });
                    Util.language.getTranslated(!!scope.allowAll)
                        .then(function (_languages) {
                        languages = _languages;
                        var typeAheadMinLength = 1, 
                        // If we're not loading the 'All Languages' key, then offset the index of
                        // the autocomplete results by + 1, otherwise we get the wrong index language
                        indexOffset = !scope.allowAll ? 1 : 0;
                        // *Hack Alert*
                        // Unfortunately this is the only way to trigger the default search results
                        if (scope.showDefaults) {
                            typeAheadMinLength = 0;
                            $input.on('focus', toggleDefaultsBox);
                        }
                        $input.on('blur', function (event) {
                            if (iAttrs.onBlur) {
                                scope.onBlur({ event: event });
                            }
                            if (scope.showDefaults) {
                                // Allow any possible functions from higher up to manipulate
                                // the language variables before resetting the placeholder
                                _.defer(function () {
                                    // And this is how we add the placeholder text back
                                    _updatePlaceholder();
                                    scope.$root.safeDigest(scope);
                                });
                            }
                        });
                        $input.typeahead({
                            hint: false,
                            highlight: true,
                            minLength: typeAheadMinLength
                        }, {
                            name: 'languages',
                            display: 'value',
                            source: Util.language.ttSubstringMatcher(languages, AC.suggestions, AC.exclude, 25, indexOffset)
                        });
                    });
                    scope.$on('$destroy', function () {
                        return CP.Cache.$document.off(NS);
                    });
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map