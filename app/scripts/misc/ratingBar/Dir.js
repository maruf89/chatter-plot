/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var starLength = 5;
require('../../app').directive('ratingBar', [function () {
        return {
            restrict: 'A',
            template: '<span class="rating-bar"><strong ng-bind="rating" class="rating"></strong><span class="stars"></span></span>',
            scope: {
                rating: '=ratingBar',
            },
            link: {
                pre: function (scope, iElem) {
                    // How many stars we're showing total
                    var stars = 5, $stars = iElem.find('.stars'), 
                    // the dynamic rating we're showing
                    rating = parseFloat(scope.rating), 
                    // loop variables
                    elem, index, className, 
                    // Used to determine if we show a half or full star
                    // if the remainder is a float like 4.3 then show 4 1/2 stars
                    curRemainder;
                    for (index = 1; index < (stars + 1); index++) {
                        curRemainder = rating - (index - 1);
                        elem = document.createElement('span');
                        className = 'star-cont icon-star';
                        if (index <= rating || curRemainder >= .75) {
                            // rounding up do we show a full star?
                            className += ' filled';
                        }
                        else if (curRemainder >= .25) {
                            // rounding up do we show a half star?
                            className += '-half filled';
                        }
                        elem.className = className;
                        $stars.append(elem);
                    }
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map