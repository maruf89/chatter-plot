/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
/**
 * @ngdoc languages
 * @module languagesDirective
 *
 * @requires Util
 */
'use strict';
var levels = require('root/config/languageLevels'), _openTabs = null, sortLevel = function (a, b) {
    return (b.level || 0) - (a.level || 0);
};
require('../../app').directive('languages', [
    'Util',
    function (Util) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'views/misc/languages/Template.html',
            scope: {
                which: '@',
                source: '=',
                limit: '@overflowLimit',
                tooltipPos: '@',
                highlight: '@' //  passed, it will move it to not be hidden TODO: build this
            },
            transclude: true,
            link: {
                pre: function (scope, iElem) {
                    var checkLimit, updateLanguages, vars, hideListener = null;
                    // set a default tooltip position if none set
                    scope.tooltipPos = scope.tooltipPos || 'top';
                    scope.vars = vars = {
                        showHidden: false,
                        showMarker: !scope.which,
                        ourSource: null,
                        iteration: [],
                        cacheKey: 'main',
                        levels: levels,
                        getIteration: _.memoize(function (ignore, iteration) {
                            if (!iteration.length) {
                                return iteration;
                            }
                            return iteration.sort(sortLevel);
                        }, function (cacheKey) {
                            return cacheKey;
                        }),
                        hideWindow: function () {
                            if (hideListener) {
                                CP.Cache.$document.off('click', vars.clickCheck);
                            }
                            vars.showHidden = false;
                            scope.$root.safeDigest(scope);
                        },
                        toggleHidden: function () {
                            if (vars.showHidden) {
                                return vars.hideWindow();
                            }
                            // Make sure there is never more than 1 window open
                            if (_openTabs) {
                                _openTabs.hideWindow();
                            }
                            _openTabs = vars;
                            vars.showHidden = hideListener = true;
                            CP.Cache.$document.on('click', vars.clickCheck);
                            return true;
                        },
                        clickCheck: function (event) {
                            if (event.target === iElem[0] || iElem[0].contains(event.target)) {
                                return true;
                            }
                            return vars.hideWindow();
                        }
                    };
                    updateLanguages = function () {
                        vars.getIteration.cache = {};
                        if (_.isArray(scope.source) && typeof scope.source[0] === 'number') {
                            vars.ourSource = Util.language.arrayToObject(scope.source);
                        }
                        else {
                            vars.ourSource = _.clone(scope.source);
                        }
                        return Util.language.expandPossible(vars.ourSource)
                            .then(function (complete) {
                            var dupes = [];
                            // If `which` is passed, it will either contain 'learning' || 'teaching'
                            // If so then filter only for the objects that have
                            vars.iteration = scope.which ? _
                                .filter(complete, function (l) {
                                return l && l[scope.which] && (dupes.indexOf(l) === -1) && (dupes.push(l) + 1);
                            }) : _.filter(complete, function (l) {
                                return l && (dupes.indexOf(l) === -1) && (dupes.push(l) + 1);
                            });
                            return checkLimit();
                        });
                    };
                    checkLimit = function () {
                        var langLength = vars.iteration.length, limit = isNaN(p(scope.limit)) ? 0 : p(scope.limit), newIterationLength;
                        if (limit && langLength > limit) {
                            vars.allIteration = vars.iteration;
                            // Hide 1 language from the limit to show the `show more` link
                            newIterationLength = scope.limit - 1;
                            vars.iteration = vars.iteration.sort(sortLevel).slice(0, newIterationLength);
                            // How many
                            vars.hiddenLength = langLength - newIterationLength;
                        }
                        else {
                            vars.allIteration = vars.hiddenLength = null;
                        }
                        return scope.$root.safeDigest(scope);
                    };
                    scope.$watch('source', _.debounce(updateLanguages, 300));
                    updateLanguages();
                    return scope.$on('$destroy', function () {
                        return CP.Cache.$document.off('click', vars.clickCheck);
                    });
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map