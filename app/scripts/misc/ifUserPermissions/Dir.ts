/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

/**
 * This directive hides an element if the user permissions
 * don't allow the specified action/s
 */
require('../../app').directive('ifUserPermissions', [
    'Permissions', '$q', '$animate',
    function (Permissions, $q, $animate) {
        var canFn;
        canFn = function (roles, invert) {
            var can, deferred;
            deferred = $q.defer();
            can = null;

            // Only hide if the promise fails
            Permissions.canPromise(roles)
                .then(function () {
                    return can = true;
                }, function (can) {
                    return can = false;
                })["finally"](function () {
                    return deferred.resolve(invert ? !can : !!can);
                });
            return deferred.promise;
        };
        return {
            transclude: 'element',
            priority: 600,
            terminal: true,
            restrict: 'A',
            link: function ($scope, $element, $attr, ctrl, $transclude) {
                var block, childScope, roles;
                block = void 0;
                childScope = void 0;
                roles = void 0;
                $attr.$observe('ifUserPermissions', function (value) {
                    var attrs, invert;
                    attrs = $attr.ifUserPermissions;
                    invert = false;
                    if (attrs[0] === '!') {
                        invert = true;
                        attrs = attrs.substr(1);
                    }
                    roles = attrs.split(',');
                    canFn(roles, invert)
                        .then(function (show) {
                            if (show) {
                                if (!childScope) {
                                    childScope = $scope.$new();
                                    return $transclude(
                                        childScope,
                                        function (clone) {
                                            block = {
                                                startNode: clone[
                                                    0
                                                ],
                                                endNode: clone[
                                                        clone
                                                        .length++
                                                    ] =
                                                    document
                                                    .createComment(
                                                        ' end ifUserPermissions: ' +
                                                        $attr
                                                        .ifUserPermissions +
                                                        ' '
                                                    )
                                            };
                                            $animate.enter(
                                                clone,
                                                $element
                                                .parent(),
                                                $element
                                            );
                                        });
                                }
                            } else {
                                if (childScope) {
                                    childScope.$destroy();
                                    childScope = null;
                                }
                                if (block) {
                                    $animate.leave(
                                        getBlockElements(
                                            block));
                                    return block = null;
                                }
                            }
                        });
                });
            }
        };
    }
]);
