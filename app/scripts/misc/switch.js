/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
/**
 * Serves as a toggle switcher between all child elements
 *
 * @example
 *     switch
 *         box-1
 *             I show first
 *         box-2
 *             I show on click
 */
require('../app').directive('switch', [
    function () {
        return {
            restrict: 'E',
            scope: false,
            link: function (scope, elem, attributes) {
                var children, current, state, total;
                children = elem.children()
                    .addClass('hidden');
                state = 0;
                total = children.length;
                current = angular.element(children[state])
                    .removeClass('hidden');
                elem.on('click.switch', function (ev) {
                    current.addClass('hidden');
                    state = ++state % total;
                    return current = angular.element(children[state])
                        .removeClass('hidden');
                });
                return scope.$on('$destroy', function (ev) {
                    return elem.off('click.switch');
                });
            }
        };
    }
]);
//# sourceMappingURL=switch.js.map