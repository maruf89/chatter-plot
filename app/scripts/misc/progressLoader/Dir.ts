/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

/**
 * @ngdoc directive
 * @name progressLoader
 * Options:
 *     {Boolean} start  if empty will turn off
 *     {string}  key    if passed will only turn off when passed with the closing key (and `start` false)
 * 
 * @requires {DataBus}
 */
require('../../app').directive('progressLoader', ['DataBus', function (DataBus) {
    return {
        restrict: 'A',
        scope: {
            progressLoader: '@',
            listener: '@',
            template: '@',
            autostart: '@'
        },
        link: {
            post: function (scope) {
                var listener = scope.listener || 'progressLoader',
                    intObject = {
                        template: scope.template || 3,
                        parent: scope.progressLoader,
                        start: scope.autostart === 'true'
                    },

                    Loader = new Mprogress(intObject),
                    listenKey = null,

                    run = function (opts) {
                        if (opts == null) {
                            opts = {};
                        }

                        var method = opts.start ? 'start' : 'end';

                        if (opts.key) {
                            if (opts.start) {
                                Loader.start();
                                return listenKey = opts.key;
                            } else if (opts.key === listenKey) {
                                Loader.end();
                                return listenKey = null;
                            }
                        } else if (opts.destroy) {
                            return Loader.end(null, true);
                        } else if (!listenKey) {
                            return Loader[method]();
                        }
                    };

                DataBus.on(listener, run);
                return scope.$on('$destroy', function () {
                    run({ destroy: true });
                    return DataBus.removeListener(listener, run);
                });
            }
        }
    };
}]);
