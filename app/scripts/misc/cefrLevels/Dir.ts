/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

/**
 * @ngdoc directive
 * @module cefrLevelsDirective
 * @description Converts an array of language skill (from 1-6) levels into a color
 * coded icon representing the a skill range, along with a tooltip explanation
 */
'use strict';

var levels = require('root/config/languageLevels');

require('../../app').directive('cefrLevels', [
    function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'views/misc/cefrLevels/Template.html',
            scope: {
                source: '=',
                showText: '@',
                tooltipPos: '@'
            },
            link: {
                pre: function (scope, iElem, iAttrs) {
                    var isSame, maxLevel, minLevel, vars;
                    minLevel = scope.source[0];
                    maxLevel = scope.source[scope.source.length - 1];
                    isSame = minLevel === maxLevel;
                    scope.showText = !!iAttrs.showText;
                    scope.tooltipPos = scope.tooltipPos || 'top';
                    scope.vars = vars = {
                        iconSkill: isSame ? minLevel : minLevel +
                            "-" + maxLevel,
                        fromText: 'profile.' + levels[minLevel].key,
                        toText: isSame ? null : 'profile.' + levels[
                            maxLevel].key,
                        text: null
                    };
                    vars.text = vars.fromText;
                    if (vars.toText) {
                        return vars.text += ',common.A_TO_B,' + vars.toText;
                    }
                }
            }
        };
    }
]);
