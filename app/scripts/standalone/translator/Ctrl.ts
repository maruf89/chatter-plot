/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var translators = _.shuffle(require('./translators.json')),

TranslatorThankyouCtrl = function (translatorModal) {
    this._translatorModal = translatorModal;

    this.translators = translators;

    // only swap when even number 
    if (translators.length % 2) {
        _.delay(this.swapElems, 500);
    } else {
        this.isEven = true;
    }
};

TranslatorThankyouCtrl.prototype = _.extend(TranslatorThankyouCtrl.prototype, {
    /**
     * Swaps the cta box with the last translator box element to fix the sizing offset
     */
    swapElems: function () {
        var iElem = $('.Translator'),
            $cta = iElem.find('.cta'),
            $translator = iElem.find('.translator').last();

        $cta.after($translator);
    },
    openModal: function () {
        var self = this,
            vars = {
                modal: {
                    deactivate: this._translatorModal.deactivate,
                    callToAction: function () {
                        if (self._translatorModal.active()) {
                            return self._translatorModal.deactivate();
                        }
                    }
                }
            };
        return this._translatorModal.activate(vars);
    }
});

TranslatorThankyouCtrl.$inject = ['translatorModal'];

require('../../app').controller('TranslatorThankyouCtrl', TranslatorThankyouCtrl);
