/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var EventIdeasCtrl = function ($scope, Auth, Permissions, $state) {
    var check;
    $scope.vars = {
        cta: null
    };
    check = function () {
        if (Auth.isAuthenticated() && Permissions.can(['CREATE_EVENT'])) {
            return $scope.vars.cta = function () {
                return $state.go('dashboard.eventCreate');
            };
        }
        else {
            return $scope.vars.cta = function () {
                return Auth.upgradeUser('HOST', {
                    onUpgrade: 'dashboard.eventCreate'
                });
            };
        }
    };
    Auth.on('/authChange', check);
    check();
    return self;
};
EventIdeasCtrl.$inject = ['$scope', 'Auth', 'Permissions', '$state'];
require('../../app').controller('EventIdeasCtrl', EventIdeasCtrl);
//# sourceMappingURL=Ctrl.js.map