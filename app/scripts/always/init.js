/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var CP = window.CP, _deleteStorage = function (locStorage) {
    locStorage.clearAll();
};
_.extend(CP.Settings, {
    isDev: /^dev/.test(location.hostname)
});
/**
 * Shortcut p('24) === parseInt('24', 10)
 * @type {Function}
 */
window.p = _.partialRight(parseInt, 10);
CP.ready = false;
CP.Cache = {
    $document: $(document),
    $html: $('html'),
    $body: $(document.body),
    $window: $(window),
};
CP.Global = {
    regex: {
        email: /^[_a-z0-9-+]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/i,
        url: /(http|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/gi
    },
    vars: {
        bodyHeight: CP.Cache.$body.height()
    }
};
var Init = function (locale, User, Auth, SocketIo, DataBus, locStorage, $rootScope, Config, Util, $state, BrowserSize) {
    $rootScope.CP = CP;
    $rootScope.global = CP.Global;
    $rootScope._data = {};
    _deleteStorage = _.partial(_deleteStorage, locStorage);
    Modernizr.addTest('windows-os', function () {
        return !!~navigator.appVersion.indexOf('Win');
    });
    /**
     * On initial socket connection, set a variable that the site has loaded
     */
    var _siteReady = function () {
        SocketIo.removeListener('connected', _siteReady);
        // SocketIo.removeListener('upgradeSocket/connected', _siteReady) 
        Auth.removeListener('/authenticated/ready', _siteReady);
        CP.ready = true;
        return DataBus.emit('/site/ready');
    }, 
    /**
     * Listen to socket io connection and call site ready when connected
     */
    _socketListen = function () {
        SocketIo.once('connected', _siteReady);
        // SocketIo.once('upgradeSocket/connected', _siteReady) 
        return Auth.once('/authenticated/ready', _siteReady);
    };
    /**
     * The token the user has saved in local storage has expired
     */
    SocketIo.on('/error/expiredToken', _deleteStorage);
    SocketIo.on('/error/connect', function (err) {
        if (err.response && err.type) {
            return DataBus.emit('yapServerResponse', err);
        }
    });
    /**
     * Called from the server to disconnect the current user
     */
    SocketIo.on('/disconnect/user', function (opts) {
        if (opts == null) {
            opts = {};
        }
        CP.ready = false;
        DataBus.emit('/site/deauth', 'server');
        DataBus.emit('yap', {
            type: 'error',
            title: opts.title || 'updates.ERROR_EXCL',
            body: opts.body || 'updates.ERROR-UNKOWN_ERROR',
            duration: 60 * 1000,
            disableClick: true
        });
        return _socketListen();
    });
    SocketIo.on('/error/reconnect', function () {
        DataBus.emit('yapServerResponse', {
            response: 400,
            type: 'updates.ERROR-LOGIN_EXPIRED'
        });
        CP.ready = false;
        DataBus.emit('/site/deauth', 'server');
        return _socketListen();
    });
    _socketListen();
    Auth.onInit();
    // Generates a number which should be unique from session to session
    // (each page reload is a new session)
    $rootScope.sessionID = Math.round((new Date()).getTime() / 1000).toString().substr(4);
    // Will store certain media query information
    $rootScope.size = BrowserSize.getSizes();
    // Add a safe $apply method to the $rootScope 
    $rootScope.safeApply = function (fn, method) {
        var phase;
        phase = this.$$phase || this.$root.$$phase;
        if (phase === '$apply' || phase === '$digest') {
            if (fn && (typeof fn === 'function')) {
                return fn();
            }
        }
        else {
            return this[method || '$apply'](fn);
        }
    };
    $rootScope.safeDigest = function (scope) {
        return this.safeApply.call(scope, '$digest');
    };
    $rootScope.goBack = function () {
        return window.history.back();
    };
    /**
     * @description Utility method to strip any text of HTML
     */
    $rootScope.stripHtml = function (text) {
        var div;
        if (!text) {
            return '';
        }
        div = document.createElement('div');
        div.innerHTML = text || '';
        return div.textContent || div.innerText || '';
    };
    /**
     * A function that runs when we know for a fact that the page is loaded
     * Currently only needed to update the headless SEO browser Prerender to know when to cache
     */
    $rootScope.mustBeLoadedIn = function (ms) {
        if (!window.prerenderReady) {
            return _.delay(function () {
                window.prerenderReady = true;
                return $rootScope.mustBeLoadedIn = $.noop;
            }, ms || 3000);
        }
    };
    // Add a time out of 3 seconds 
    $rootScope.mustBeLoadedIn(3000);
    if ($rootScope.isHeadlessBrowser = /\sPrerender\s/.test(window.navigator.userAgent)) {
        CP.ready = true;
    }
    $rootScope.$state = $state;
    $rootScope.trim = function (text, length, ellipsify) {
        if (text && text.length > length) {
            return text.substr(0, length) + (ellipsify ? '…' : '');
        }
        return text;
    };
    /**
     * @ngdoc property
     * @description the DOM body listens to this and adds any classes set to this variable
     * @name $rootScope#pageClasses
     * @propertyOf $rootScope
     * @returns {}
     */
    $rootScope.pageClasses = '';
    $rootScope.addPageClass = function (className) {
        var varName = 'pageClasses';
        if (!$rootScope[varName]) {
            $rootScope[varName] = className;
        }
        else {
            $rootScope[varName] += ' ' + className;
        }
        $rootScope.safeApply();
    };
    $rootScope.removePageClass = function (className) {
        var index = $rootScope.pageClasses.search(className), str;
        if (index === -1) {
            return;
        }
        str = $rootScope.pageClasses.split('');
        str.splice(index, className.length);
        $rootScope.pageClasses = str.join('');
        $rootScope.safeApply();
    };
    // Required ckeditor parameters 
    $rootScope.ckEditorOptions = {
        language: 'en',
        uiColor: '#FFFFFF'
    };
    CKEDITOR.basePath = '/bower_components/ng-ckeditor/libs/ckeditor/';
    var editorPlugins = CKEDITOR.plugins;
    editorPlugins.basePath = CKEDITOR.basePath + 'plugins/';
    // On locale set/update update the ckeditor 
    DataBus.on('/settings/processed', function (settings) {
        if (settings.locale) {
            return $rootScope.ckEditorOptions.languages = settings.locale.substr(0, 2);
        }
    });
    // Try to get the user's current location ASAP 
    return _.defer(function () {
        return Util.geo.initGetCurrentCoords(User, 'data');
    });
};
Init.$inject = [
    'locale',
    'User',
    'Auth',
    'SocketIo',
    'DataBus',
    'localStorageService',
    '$rootScope',
    'Config',
    'Util',
    '$state',
    'BrowserSize',
    'FB',
];
require('../app.js').run(Init);
//# sourceMappingURL=init.js.map