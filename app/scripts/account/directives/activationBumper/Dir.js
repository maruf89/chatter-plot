/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var ActivationBumper = function (Auth, locale, SocketIo, DataBus, $templateCache) {
    return {
        restrict: 'E',
        replace: false,
        template: $templateCache.get('views/account/directives/activationBumper/Template.html'),
        scope: {},
        compile: function () {
            return {
                pre: function (scope) {
                    var $destroy = DataBus.emit.bind(DataBus, '/nav/subheader/change'), successFn = function () {
                        Auth.accountActivated();
                        $destroy();
                    };
                    scope.vars = {
                        resendEmail: function () {
                            resend(Auth, DataBus)
                                .then(_.partial(resendCallback, DataBus, scope, locale));
                        },
                        message: '',
                    };
                    locale.ready('auth').then(function () {
                        scope.vars.message = locale.getString('auth.BUMPER-CONFIRM_MESSAGE');
                    });
                    DataBus.on('/site/deauth', $destroy);
                    SocketIo.onSock('/email/activation/activated<', successFn);
                    scope.$on('$destroy', function () {
                        this._SocketIo._removeListener('/email/activation/activated<', successFn);
                        DataBus.removeListener('/site/deauth', $destroy);
                    });
                }
            };
        }
    };
}, resend = function (Auth, DataBus) {
    DataBus.emit('progressLoader', { start: true });
    return Auth.resendActivationEmail();
}, resendCallback = function (DataBus, scope, locale, response) {
    DataBus.emit('progressLoader');
    if (response.notify) {
        DataBus.emit('yap', response.notify);
    }
    if (response.remove) {
        return scope.$destroy();
    }
    if (response.type) {
        return locale.ready(locale.getPath(response.type))
            .then(function () {
            return scope.vars.message = locale.getString(response.type);
        }.bind(this));
    }
};
require('../../../app.js').directive('activationBumper', [
    'Auth',
    'locale',
    'SocketIo',
    'DataBus',
    '$templateCache',
    ActivationBumper
]);
//# sourceMappingURL=Dir.js.map