/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var ready = false,

    _locales = null,

    _updateFn = _.debounce(function (Config, updates) {
        return Config.updateSettings(updates);
    }, 100);

require('../../../../app').directive('settingsDisplayLanguages', [
    'API', 'Config', 'DataBus',
    function (API, Config, DataBus) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: CP.site.views + 'account/settings/directives/displayLanguages/Template.html',
            scope: {
                current: '=?'
            },
            compile: function () {
                var vars = { locales: _locales },
                    keys,
                    locales;

                if (!_locales) {
                    locales = require('common/config/locales').supported;

                    // Get all the keys
                    keys = _.keys(locales);

                    // Filter them to only get the ones with language-location as well (en-US)
                    keys = _.filter(keys, function (key) {
                        return key[2] === '-';
                    });

                    vars.locales = _locales = _.pick(locales, keys);
                }

                return {
                    pre: function (scope) {
                        scope.vars = vars;
                        scope._watch = Config.settings;
                        vars.settings = scope.current || Config.settings;
                    },
                    post: function (scope) {
                        scope.$watch('_watch.locale', function (post, prev) {
                            if (prev !== post && post && post !== CP.locale) {
                                if (scope.$root.userID) {
                                    DataBus.emit('progressLoader', { start: true });
                                    _updateFn(Config, { locale: post });
                                } else {
                                    Config._localeChange(post);
                                }
                            }
                        });
                    }
                };
            }
        };
    }
]);
