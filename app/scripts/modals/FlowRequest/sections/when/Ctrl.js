/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var NAME = 'when';
/**
 * This service is used to open the Signup / Login modals
 * as well as any other related modals including 'password-reset' & 'email-signup'
 */
var FlowReqWhenCtrl = (function () {
    function FlowReqWhenCtrl($scope, _$q, _Config, _Util) {
        this._$q = _$q;
        this._Config = _Config;
        this._Util = _Util;
        var shared = this.shared = $scope.shared;
        shared.when = shared.when || {};
        this._prefill();
        $scope.$on('whenPrepare', this._prepare.bind(this));
        $scope.$on('update', function () {
            this._prefill();
            $scope.$root.safeDigest($scope);
        }.bind(this));
    }
    /**
     * @description if we are editing a request we will have a location selected already
     * @private
     */
    FlowReqWhenCtrl.prototype._prefill = function () {
        var dateString = this.shared._when, whenObj = this.shared.when, date;
        if (!dateString) {
            return;
        }
        // TODO: get the timezone of the event and update this when ready
        date = this._Config.format.date.toObj('basic_date_time_no_millis', dateString).toDate();
        whenObj.time = date;
        whenObj.date = _.clone(whenObj.time);
    };
    FlowReqWhenCtrl.prototype._prepare = function (event, deferred) {
        event.preventDefault();
        var source = this.shared.when, time = source.time.split('|'), promise, formattedArgs = [source.date, time];
        // If we have the timezone attached to a location, add it
        if (this.shared.venue) {
            promise = this._Util.geo.getTimezone(this.shared.venue.coords, source.date, true)
                .then(function (tz) {
                return formattedArgs.push(tz);
            });
        }
        else {
            promise = this._$q.when();
        }
        return promise.then(function () {
            this.shared['_' + NAME] = this._Config.format.date.toString('basic_date_time_no_millis', formattedArgs);
            deferred.resolve();
        }.bind(this));
    };
    return FlowReqWhenCtrl;
})();
FlowReqWhenCtrl.$inject = [
    '$scope',
    '$q',
    'Config',
    'Util',
];
require('../../../../app.js').controller('FlowReqWhenCtrl', FlowReqWhenCtrl);
//# sourceMappingURL=Ctrl.js.map