/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var commonInteraction = require('common/type/interaction'), commonUser = require('common/type/user');
/**
 * This service is used to open the Signup / Login modals
 * as well as any other related modals including 'password-reset' & 'email-signup'
 */
var FlowRequest = (function () {
    function FlowRequest(_User, _DataBus, _Interaction, _$q, _$state, _$analytics, _btfModal, _$templateCache, _Venues, _Activities, _Maps, _Util, _Config, $rootScope) {
        this._User = _User;
        this._DataBus = _DataBus;
        this._Interaction = _Interaction;
        this._$q = _$q;
        this._$state = _$state;
        this._$analytics = _$analytics;
        this._btfModal = _btfModal;
        this._$templateCache = _$templateCache;
        this._Venues = _Venues;
        this._Activities = _Activities;
        this._Maps = _Maps;
        this._Util = _Util;
        this._Config = _Config;
        this._templates = {
            create: 'views/modals/FlowRequest/Template.html',
            deny: 'views/modals/FlowRequest/DenyTemplate.html',
            view: 'views/modals/FlowRequest/ViewTemplate.html',
        };
        /**
         * @description the default sections to include for each requset type
         * @type {{tandem: string[]}}
         * @private
         */
        this._sectionTypes = {
            tandem: [
                'venue',
                'when',
                'message',
            ],
        };
        /**
         * @description fields to ignore when duplicating a request
         * @type {string[]}
         * @private
         */
        this._duplicateIgnore = [
            'when',
            'message',
        ];
        // close modal on state change
        $rootScope.$on('$stateChangeSuccess', this.deactivate.bind(this));
        this._onSucessRedirect = _Config.routes.myActivities;
        this.respondRequest = _Interaction.respondRequest.bind(_Interaction);
    }
    FlowRequest.prototype._initModal = function (modalName) {
        if (this._modal) {
            if (this._modal.name === modalName) {
                return this._$q.reject();
            }
            this.deactivate();
        }
        this._modal = this._btfModal({
            template: this._$templateCache.get(this._templates[modalName]),
            container: '.map-pane',
        });
        this._modal.name = modalName;
        return this._$q.when(this._modal);
    };
    /**
     * @description Initiates the modal
     *
     * @param {object} opts - will be one of `signup|login`
     */
    FlowRequest.prototype.activate = function (opts) {
        this._deferred = this._$q.defer();
        var deactivate = this.deactivate.bind(this);
        this._initModal('create')
            .then(function (modal) {
            var $analytics = this._$analytics, actionLabel = opts.editRequest ? 'edited' : 'submitted';
            this._shared = opts.data;
            this._sections = opts.sections || _.clone(this._sectionTypes[opts.type]);
            this._type = opts.type;
            this._reqUserID = opts.reqUserID;
            this._shared.reqUser = this._User.users[opts.reqUserID];
            modal.activate({
                modal: {
                    deactivate: deactivate,
                    sections: this._sections,
                    shared: this._shared,
                    ngIncludify: this._ngIncludify,
                    onSubmit: this._submit.bind(this),
                    title: opts.title || 'activities.REQ_' + opts.type.toUpperCase() + '_TITLE',
                    processing: false,
                },
            }).then(function (args) {
                $analytics.eventTrack('submit', {
                    category: opts.type + " request",
                    label: actionLabel
                });
                return args;
            });
        }.bind(this))
            .catch(deactivate);
        return this._deferred.promise;
    };
    /**
     * @description /views/modals/FlowRequest/sections/{{::section}}/Template.html'
     * @param path
     * @param section
     * @returns {string}
     */
    FlowRequest.prototype._ngIncludify = function (path, section) {
        return path + section + '/Template.html';
    };
    FlowRequest.prototype._getFullRequest = function (reqID) {
        var requestPromise = this._Interaction.requests[reqID] ?
            this._$q.when(this._Interaction.requests[reqID]) :
            this._Interaction.getType({ objID: reqID, type: 'request' }), otherUser = commonInteraction.getParticipants(reqID, this._User.userID), getFields = ['firstName', 'lastName', 'locations', 'userID', 'picture'], personPromise = this._User.cacheGet(otherUser, getFields);
        return this._$q.all([
            requestPromise,
            personPromise,
        ]).then(function (response) {
            return {
                request: response[0],
                otherID: otherUser[0],
                users: response[1],
            };
        });
    };
    /**
     * @description Edit or Duplicate a request
     * Editing a request creates a new request while cancelling the old one, the functionality
     * is nearly identical for duplicating requests which doesn't affect the old request just copies it
     * @param reqID
     * @returns {IPromise<TResult>}
     */
    FlowRequest.prototype.edit = function (reqID) {
        return this._getFullRequest(reqID).then(function (response) {
            var request = response.request, isDuplicate = request.expired, 
            // the persons locations
            personLocs = response.users[response.otherID].locations, sections = _.clone(this._sectionTypes[request.type]), sharedData = this._deduceSections(request, sections, isDuplicate);
            // Only edit if it's editable
            if (request.status !== 'pending' &&
                request.status !== 'accept') {
                throw false;
            }
            this._editRequest = !isDuplicate && request; // if we're duplicating, don't edit the old request
            sharedData._isModified = reqID;
            sharedData.suggestedVenues = commonUser.combineUserLocations(personLocs, this._User.data.locations);
            return this.activate({
                type: request.type,
                editRequest: this._editRequest,
                reqUserID: response.otherID,
                sections: sections,
                title: !isDuplicate && 'activities.REQ_EDIT_TITLE',
                data: sharedData,
            });
        }.bind(this));
    };
    FlowRequest.prototype.duplicate = function (reqID) {
        return this.edit(reqID)
            .then(function (args) {
            _.defer(function () {
                this._$state.go(this._Config.routes.dashboard, {
                    state: 'current',
                    action: this._Util.encodeActionArgs({
                        action: 'view',
                        objID: encodeURIComponent(args.request.reqID),
                        type: 'request',
                    })
                });
            }.bind(this));
            return args;
        }.bind(this));
    };
    /**
     * @description accepts a request and grabs the usable form data from it and converts
     * it to an object
     * @param {object} request
     * @param {array<string>} sections - sections to deduce from the request
     * @param {boolean} isCopy - if we're copying the request we don't want to duplicate certain data
     * @returns {{sections: string[], shared: {}}}
     * @private
     */
    FlowRequest.prototype._deduceSections = function (request, sections, isCopy) {
        if (isCopy) {
            sections = _.difference(sections, this._duplicateIgnore);
        }
        return _.reduce(sections, function (shared, section) {
            shared['_' + section] = request[section];
            return shared;
        }, {});
    };
    /**
     * @description packages up the request and sends it to the server
     * @param $scope
     */
    FlowRequest.prototype._submit = function ($scope) {
        var DataBus = this._DataBus, deferred = this._deferred;
        $scope.modal.processing = true;
        $scope.$root.safeDigest($scope);
        // Start the progress loader animation
        DataBus.emit('progressLoader', { start: true });
        return this._triggerRequest($scope)
            .then(this._prepareRequest.bind(this))
            .then(this._Interaction.sendRequest.bind(this._Interaction))
            .then(function (request) {
            this.deactivate(true);
            DataBus.emit('yapServerResponse', {
                response: 200,
                type: 'updates.SENT',
            });
            // Send the users to the activities so they know where it lives
            if (this._$state.current.name !== this._onSucessRedirect) {
                _.defer(function () {
                    this._$state.go(this._onSucessRedirect, {
                        action: this._Util.encodeActionArgs({
                            action: 'view',
                            objID: encodeURIComponent(request.reqID),
                            type: 'request',
                        })
                    });
                }.bind(this));
            }
            return deferred.resolve({ completed: true, request: request });
        }.bind(this))
            .catch(function (serverResponse) {
            $scope.modal.processing = false;
            DataBus.emit('yapServerResponse', serverResponse);
            throw serverResponse;
        })
            .finally(function () {
            $scope.$root.safeDigest($scope);
            DataBus.emit('progressLoader');
        });
    };
    /**
     * @description calls all of the children preparers
     *
     * @param {object} $scope
     * @returns {Promise}
     * @private
     */
    FlowRequest.prototype._triggerRequest = function ($scope) {
        return this._$q.all(_.map(this._sections, function (section) {
            var deferred = this._$q.defer(), event = $scope.$broadcast(section + 'Prepare', deferred);
            return event.defaultPrevented ? deferred.promise : this._$q.when();
        }, this));
    };
    /**
     * @description Broadcasts a trigger to all child scopes that need to process any data after which it will
     * be available on the shared object under either underscore + name (_field) or just the field name
     *
     * @returns {object}
     * @private
     */
    FlowRequest.prototype._prepareRequest = function () {
        var requestObj = {
            type: this._type,
            creator: this._User.userID,
            participants: [this._User.userID, this._reqUserID],
            status: 'pending',
        }, shared = this._shared;
        _.each(this._sections, function (section) {
            var curData;
            curData = shared['_' + section] || shared[section];
            if (curData !== void 0) {
                requestObj[section] = curData;
            }
        }, this);
        // Add additional data if this was an edit
        if (this._editRequest) {
            // First check if anything actually changed
            if (!commonInteraction.requestsDiffer(this._editRequest, requestObj)) {
                // if nothing changed -> do nothing!
                return this._$q.reject({
                    response: 400,
                    type: 'activities.REQUEST_NO_CHANGE_MADE',
                });
            }
            requestObj.oldReqID = this._editRequest.reqID;
            requestObj.oldRequest = this._editRequest;
        }
        return this._$q.when(requestObj);
    };
    FlowRequest.prototype.deny = function (datum) {
        this._deferred = this._$q.defer();
        this._initModal('deny')
            .then(function (modal) {
            var STATUS = datum.status.toUpperCase();
            modal.activate({
                Service: this,
                modal: {
                    deactivate: this.deactivate.bind(this),
                    title: 'activities.REQ_' + STATUS + '_TITLE',
                    sendText: 'common.DELETE',
                    onSubmit: this._submitDecline.bind(this),
                    processing: false,
                    reqObj: {
                        reqID: datum.reqID,
                        status: datum.status,
                        message: null,
                    },
                },
            });
        }.bind(this));
        return this._deferred.promise;
    };
    FlowRequest.prototype._submitDecline = function ($scope) {
        $scope.modal.processing = true;
        var DataBus = this._DataBus, deferred = this._deferred;
        $scope.$root.safeDigest($scope);
        // Start the progress loader animation
        DataBus.emit('progressLoader', { start: true });
        return this._Interaction.respondRequest($scope.modal.reqObj)
            .then(function (response) {
            this.deactivate(true);
            DataBus.emit('yapServerResponse', {
                response: 200,
                type: 'updates.SENT',
            });
            this._$analytics.eventTrack('submit', {
                category: 'tandem/teacher request',
                label: 'declined/canceled'
            });
            return deferred.resolve(response);
        }.bind(this))
            .catch(function (serverResponse) {
            $scope.modal.processing = false;
            DataBus.emit('yapServerResponse', serverResponse);
            throw serverResponse;
        })
            .finally(function () {
            $scope.$root.safeDigest($scope);
            // Start the progress loader animation
            DataBus.emit('progressLoader');
        });
    };
    FlowRequest.prototype.view = function (reqID) {
        this._deferred = this._$q.defer();
        var modal, request, deactivate = this.deactivate.bind(this);
        this._initModal('view')
            .then(function (_modal) {
            modal = _modal;
            return this._getFullRequest(reqID);
        }.bind(this))
            .catch(deactivate)
            .then(function (response) {
            request = response.request;
            this._User.addPeople(response.users, null, true);
            this._Interaction.formatRequest(request);
            this._focusMarker(request);
            this._$analytics.eventTrack('click', {
                category: request.type + " request",
                label: 'view request'
            });
            return this._Venues.getCacheFormat(request.venue);
        }.bind(this))
            .then(function () {
            modal.activate({
                modal: {
                    deactivate: deactivate,
                    Activity: request,
                },
            });
        });
        return this._deferred.promise;
    };
    FlowRequest.prototype._focusMarker = function (request) {
        var markers = this._Activities.map.formatMarker('request', [], null, [], request), setName = 'focusRequest', instance = this._Maps.getInstance();
        instance.selectSet(setName);
        instance.clearMarkers(setName);
        instance.pushMarkers(markers, setName);
        instance.center();
        this._onDeactivate = function () {
            instance.deselectSet(setName);
            instance.center();
        };
    };
    FlowRequest.prototype.deactivate = function (noReject) {
        if (!this._modal || !this._modal.active()) {
            return;
        }
        this._modal.deactivate();
        this._modal = null;
        !noReject && this._deferred.reject();
        if (this._onDeactivate) {
            this._onDeactivate();
            this._onDeactivate = null;
        }
    };
    return FlowRequest;
})();
FlowRequest.$inject = [
    'User',
    'DataBus',
    'Interaction',
    '$q',
    '$state',
    '$analytics',
    'btfModal',
    '$templateCache',
    'Venues',
    'Activities',
    'Maps',
    'Util',
    'Config',
    '$rootScope',
];
require('../../app.js').service('FlowRequest', FlowRequest);
//# sourceMappingURL=Service.js.map