/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var ModalFactory = function ($rootScope, $window, $injector, $state, Config) {
    return ModalFactory = (function () {
        function ModalFactory() {
            this.current = {
                instance: null,
                modals: {},
                history: []
            };
        }
        ModalFactory.prototype.load = function (module, locals, scope) {
            var impl, modals;
            if (!this.current.modals[module]) {
                impl = $injector.get(module);
                modals = {};
                modals[module] = impl;
                this.addModals(modals);
            }
            return this.open(module, locals, scope);
        };
        /**
         * Registers a modal to this Instance
         *
         * @param {object} modals  { <modalKey>: <modalReference> }
         */
        ModalFactory.prototype.addModals = function (modals) {
            return _.each(modals, (function (_this) {
                return function (modal, key) {
                    return _this.current.modals[key] = modal;
                };
            })(this));
        };
        /**
         * Opens a new Modal
         *
         * @param  {string} which   The modal key
         * @param  {object} locals  Variables to extend the scope with (if scope argument == null)
         * @param  {$scope} scope   angular scope object to use for modal
         */
        ModalFactory.prototype.open = function (which, locals, scope) {
            var deactivate, modal;
            if (!(modal = this.current.modals[which])) {
                return false;
            }
            // deactivate current instance 
            this.deactivate();
            deactivate = function () {
                // if land
                if ($window.history.length === 1) {
                    _.defer(function () {
                        $state.go(Config.routes.home);
                    });
                    return modal.deactivate();
                }
                return modal.deactivate() && $rootScope.goBack();
            };
            if (_.isPlainObject(locals)) {
                locals.modal = locals.modal || {};
                locals.modal.deactivate = locals.modal.deactivate ||
                    deactivate;
            }
            else {
                locals = {
                    modal: {
                        deactivate: deactivate
                    }
                };
            }
            this.current.history.push([which, locals, scope]);
            this.current.instance = modal;
            return modal.activate(locals, scope);
        };
        /**
         * Replaces the current modal and if a previous modal was open
         * will open it
         *
         * @param  {string} which   The modal key
         * @param  {object} locals  Variables to extend the scope with (if scope argument == null)
         * @param  {$scope} scope   angular scope object to use for modal
         */
        ModalFactory.prototype.close = function () {
            this.current.history.pop();
            this.deactivate();
            if (this.current.history.length) {
                return this.open.apply(this, this.current.history.pop());
            }
        };
        /**
         * Closes the current modal and triggers `window.history.back()` if the
         * modal has a true `backOnClose` property
         *
         * @param  {Boolean} noBack  whether to disable the history check
         */
        ModalFactory.prototype.deactivate = function (noBack) {
            var cur;
            if (cur = this.current.instance) {
                this.current.instance = null;
                return $rootScope.safeApply(function () {
                    if (!noBack && cur.backOnClose) {
                        $window.history.back();
                    }
                    return cur.deactivate();
                });
            }
        };
        /**
         * Replaces the current modal with the new one
         *
         * @param  {string} which   The modal key
         * @param  {object} locals  Variables to extend the scope with (if scope argument == null)
         * @param  {$scope} scope   angular scope object to use for modal
         */
        ModalFactory.prototype.replace = function (which, locals, scope) {
            // remove current history state 
            this.current.history.pop();
            // close it without triggering 'back' 
            this.deactivate(true);
            // replace with new one 
            return this.open(which, locals, scope);
        };
        /**
         * Clears all active modals and replaces with the new one
         *
         * @param  {string} which   The modal key
         * @param  {object} locals  Variables to extend the scope with (if scope argument == null)
         * @param  {$scope} scope   angular scope object to use for modal
         */
        ModalFactory.prototype.replaceAll = function (which, locals, scope) {
            this.closeAll();
            return this.open(which, locals, scope);
        };
        /**
         * Closes all modals
         */
        ModalFactory.prototype.closeAll = function () {
            this.deactivate();
            this.current.history.length = 0;
            return true;
        };
        return ModalFactory;
    })();
};
ModalFactory.$inject = [
    '$rootScope',
    '$window',
    '$injector',
    '$state',
    'Config'
];
require('../app').factory('ModalFactory', ModalFactory);
//# sourceMappingURL=ModalFactory.js.map