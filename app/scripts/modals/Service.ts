/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

//var commonInteraction = require('../../../../common/type/interaction'),
//    commonUser = require('../../../../common/type/user');

/**
 * This service is used to open the Signup / Login modals
 * as well as any other related modals including 'password-reset' & 'email-signup'
 */
class ModalService implements cp.modal.IService {
    private _deferred:ng.IDeferred<string>;
    private _modal:any;
    private _onDeactivate:() => void;
    private _template:string;

    constructor(
        private _btfModal:any,
        private _$q:ng.IQService,
        private _$templateCache:ng.ITemplateCacheService,
        $rootScope:cp.IRootScopeService
    ) {
        // close modal on state change
        $rootScope.$on('$stateChangeSuccess', this.deactivate.bind(this));

        this._template = _$templateCache.get('views/modals/Template.html');
    }

    private _initModal(template:string, opts?:{container?:string; controller?:Function}):any {
        var options:any = {
            template: template,
        };

        if (opts.container) { options.container = opts.container; }
        if (opts.controller) { options.controller = opts.controller; }

        return this._modal = this._btfModal(options);
    }

    /**
     * @description Initiates the modal
     *
     * @param {object} opts
     * @param {string} opts.template - class to apply to modal box
     * @param {string=} opts.class - class to apply to root template
     * @param {object=} opts.vars
     */
    public activate(opts:any):ng.IPromise<string> {
        this._deferred = this._$q.defer();

        this._modal && this._modal.active() && this.deactivate();

        var template:string = this._wrapTpl(this._$templateCache.get(opts.template)),
            modal = this._initModal(template, opts),
            deactivate = this.deactivate.bind(this),
            deferred = this._deferred,

            vars = {},

            onSubmit = this._onSubmit.bind(this, vars),

            localScope = {
                deactivate: deactivate,
                onSubmit: onSubmit,
                'class': opts.class || '',
                vars: vars,
            };

        if (opts.vars) {
            _.extend(vars, opts.vars);
        }

        modal.activate({
            modal: localScope,
        });

        return deferred.promise;
    }

    private _wrapTpl(template:string):string {
        return this._template.replace('TRANSCLUDE', template);
    }

    private _onSubmit(vars:any, what?:any):ng.IDeferred<any> {
        this._deferred.resolve(what || vars);
        this._deferred = null;
    }

    public deactivate(noReject?:boolean):void {
        if (!this._modal || !this._modal.active()) {
            return;
        }

        this._modal.deactivate();
        this._modal = null;

        !noReject && this._deferred && this._deferred.reject();
        this._deferred = null;

        if (this._onDeactivate) {
            this._onDeactivate();
            this._onDeactivate = null;
        }
    }
}

ModalService.$inject = [
    'btfModal',
    '$q',
    '$templateCache',
    '$rootScope',
];

require('../app').service('ModalService', ModalService);
