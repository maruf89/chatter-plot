'use strict';

export var preset = {
    profile: {
        params: {
            uploadPreset: 'profile',
            transformation: [
                [{
                    limit: true,
                    width: 500,
                    height: 500
                }]
            ]
        },
        options: {
            crop: {
                minWidth: 300
            }
        }
    }
};

preset.events = {
    params: {
        uploadPreset: 'events',
        transformation: preset.profile.params.transformation
    },
    options: preset.profile.options
};
