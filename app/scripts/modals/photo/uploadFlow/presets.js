'use strict';
exports.preset = {
    profile: {
        params: {
            uploadPreset: 'profile',
            transformation: [
                [{
                        limit: true,
                        width: 500,
                        height: 500
                    }]
            ]
        },
        options: {
            crop: {
                minWidth: 300
            }
        }
    }
};
exports.preset.events = {
    params: {
        uploadPreset: 'events',
        transformation: exports.preset.profile.params.transformation
    },
    options: exports.preset.profile.options
};
//# sourceMappingURL=presets.js.map