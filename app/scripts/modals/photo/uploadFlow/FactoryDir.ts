/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var app = require('../../../app'),

    maxSize = 2,
    maxDimen = 1600;

import preset = require('./presets');

app.factory('photoUploadFlow', [
    'btfModal',
    function (Modal) {
        return Modal({
            templateUrl: 'views/modals/photo/uploadFlow/Template.html',
            controller: 'photoUploadFlowCtrl as Photo'
        });
    }
]);

var photoUploadFlowCtrl = function ($scope, Cloudinary) {
    this._$scope = $scope;
    this._Cloudinary = Cloudinary;

    this.uploadConfig = Cloudinary.flowConfig();
    this.flow = null;
    this.isUploading = false;
    this.obj = {
        src: null,
        coords: [100, 100, 200, 200, 100, 100],
        thumbnail: false
    };

    $scope.vars = {
        error: {
            fileType: false,
            fileSize: false,
        },
        max: {
            fileSize: maxSize
        }
    };

    var types = $scope.fileTypes || 'photo',
        onUploaded, unbindListener;

    if (_.isArray(types)) {
        this.fileTypes = types;
    } else {
        switch (types) {
            case 'photo':
                this.fileTypes = ['jpg', 'png', 'gif'];
        }
    }

    this.preset = preset.preset[$scope.uploadPreset];
    onUploaded = this.uploaded.bind(this);
    unbindListener = $scope.$watch('Photo.flow', function (post) {
        if (post) {
            this.flow.on('fileSuccess', onUploaded);
            return unbindListener();
        }
    }.bind(this));

    $scope.$on('$destroy', function () {
        if (this.flow) {
            this.flow.off('fileSuccess', onUploaded);
        }
        if (this.flow.isUploading()) {
            return this.flow.cancel();
        }
    }.bind(this));
};

photoUploadFlowCtrl.prototype = _.extend(photoUploadFlowCtrl.prototype, {
    photoAdded: function ($file) {
        var fileReader,
            vars = this._$scope.vars,
            hasError = null;

        if (this.fileTypes) {
            if (this.fileTypes.indexOf($file.getExtension()) === -1) {
                hasError = vars.error.fileType = true;
            }
        }

        if (($file.size / 1024 / 1024) > maxSize) {
            hasError = vars.error.fileSize = true;
        }

        if (hasError) {
            return this._$scope.$root.safeDigest(this._$scope);
        }

        fileReader = new FileReader();
        fileReader.onload = function (event) {
            this.obj.src = event.target.result;
            this._$scope.$root.safeDigest(this._$scope);
        }.bind(this);

        fileReader.readAsDataURL($file.file);
    },

    hideModal: function () {
        return this.modalHidden = !this.modalHidden;
    },

    cancel: function () {
        this.flow.cancel();
        return this.obj.src = null;
    },

    initUpload: function () {
        this.isUploading = true;

        var params = _.cloneDeep(this.preset.params),
            toAdd = params.transformation,
            flow = this.flow,
            crop;

        if (toAdd) {
            if (_.isArray(toAdd[0])) {
                toAdd = toAdd[0];
            }

            crop = this.obj.coords;
            toAdd.unshift({
                crop: true,
                x: crop[0],
                y: crop[1],
                width: crop[2] - crop[0],
                height: crop[3] - crop[1]
            });
        }

        params.publicID = this._$scope.publicID || this._$scope.userID;

        return this._Cloudinary.extendFlowConfig(this.flow, params)
            .then(function () {
                return flow.upload();
            });
    },

    uploaded: function ($file, $message) {
        var data = null;

        try {
            data = JSON.parse($message);
        } catch (_error) {
            console.log('error uploading:', err);
        }

        _.delay(function () {
            this.isUploading = false;
            this._$scope.safeDigest(this._$scope);

            if (this._$scope.onSuccess) {
                this._$scope.onSuccess(data);
            }
        }.bind(this), 500);
    }
});

photoUploadFlowCtrl.$inject = [
    '$scope',
    'Cloudinary',
];

app.controller('photoUploadFlowCtrl', photoUploadFlowCtrl);
