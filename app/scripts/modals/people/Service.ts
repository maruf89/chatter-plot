/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

import Abstract = require('../Abstract');

/**
 * @description What we need from each user
 *
 * TODO: since this got moved to a peopleDisplay directive, this doesn't belong here
 *
 * @type {array<string>}
 * @memberof PeopleDisplay
 */
var _requiredUserFields = [
    'userID',
    'firstName',
    'lastName',
    'languages',
    'details',
    'city',
    'state',
    'country',
    'picture.default'
];

/**
 * @classdesc A modal that loads a people-display directive which showcases users
 * @class
 * @requires btfModal
 * @requires $rootScope
 * @requires $state
 */
class PeopleDisplay extends Abstract.AbstractModal {
    constructor(
        private _btfModal,
        private _$rootScope,
        private _$state
    ) {
        super();
    }

    activate(opts, force) {
        var modal,
            context = this;

        if (this.modal) {
            if (force) {
                return this.deactivate()
                    .then(function () {
                        return context.activate(opts.sections);
                    });
            }
            return false;
        }

        this.modal = modal = this._btfModal({ templateUrl: 'views/modals/people/Template.html' });

        modal.activate({
            modal: {
                sections: opts.sections,
                category: opts.category,
                deactivate: this.deactivate.bind(this)
            }
        });

        return super.activate();
    }

    getRequiredFields() {
        return _.clone(_requiredUserFields);
    }
}

PeopleDisplay.$inject = [
    'btfModal',
    '$rootScope',
    '$state',
];

require('../../app').service('PeopleDisplay', PeopleDisplay);
