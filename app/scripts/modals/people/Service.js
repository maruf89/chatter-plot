/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Abstract = require('../Abstract');
/**
 * @description What we need from each user
 *
 * TODO: since this got moved to a peopleDisplay directive, this doesn't belong here
 *
 * @type {array<string>}
 * @memberof PeopleDisplay
 */
var _requiredUserFields = [
    'userID',
    'firstName',
    'lastName',
    'languages',
    'details',
    'city',
    'state',
    'country',
    'picture.default'
];
/**
 * @classdesc A modal that loads a people-display directive which showcases users
 * @class
 * @requires btfModal
 * @requires $rootScope
 * @requires $state
 */
var PeopleDisplay = (function (_super) {
    __extends(PeopleDisplay, _super);
    function PeopleDisplay(_btfModal, _$rootScope, _$state) {
        _super.call(this);
        this._btfModal = _btfModal;
        this._$rootScope = _$rootScope;
        this._$state = _$state;
    }
    PeopleDisplay.prototype.activate = function (opts, force) {
        var modal, context = this;
        if (this.modal) {
            if (force) {
                return this.deactivate()
                    .then(function () {
                    return context.activate(opts.sections);
                });
            }
            return false;
        }
        this.modal = modal = this._btfModal({ templateUrl: 'views/modals/people/Template.html' });
        modal.activate({
            modal: {
                sections: opts.sections,
                category: opts.category,
                deactivate: this.deactivate.bind(this)
            }
        });
        return _super.prototype.activate.call(this);
    };
    PeopleDisplay.prototype.getRequiredFields = function () {
        return _.clone(_requiredUserFields);
    };
    return PeopleDisplay;
})(Abstract.AbstractModal);
PeopleDisplay.$inject = [
    'btfModal',
    '$rootScope',
    '$state',
];
require('../../app').service('PeopleDisplay', PeopleDisplay);
//# sourceMappingURL=Service.js.map