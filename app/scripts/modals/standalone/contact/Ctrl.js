/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var ContactCtrl, app, categories, extend = function (child, parent) {
    for (var key in parent) {
        if (hasProp.call(parent, key))
            child[key] = parent[key];
    }
    function ctor() {
        this.constructor = child;
    }
    ctor.prototype = parent.prototype;
    child.prototype = new ctor();
    child.__super__ = parent.prototype;
    return child;
}, hasProp = {}.hasOwnProperty;
(app = require('../../../app'))
    .factory('contactModal', [
    'btfModal', '$templateCache',
    function (btfModal, $templateCache) {
        return btfModal({
            template: $templateCache.get('views/modals/standalone/contact/Template.html')
        });
    }
]);
categories = require('./categories.json');
ContactCtrl = (function (superClass) {
    extend(ContactCtrl, superClass);
    function ContactCtrl($scope, SocketIo, User, DataBus, $state, Util) {
        this.$scope = $scope;
        this.SocketIo = SocketIo;
        this.User = User;
        this.DataBus = DataBus;
        this._$state = $state;
        this._Util = Util;
        this.$scope.categoryOptions = categories;
        ContactCtrl.__super__.constructor.apply(this, arguments);
        this.$scope.message.type = 'contact';
        return this;
    }
    return ContactCtrl;
})(require('../standaloneModal'));
ContactCtrl.$inject = ['$scope', 'SocketIo', 'User', 'DataBus', '$state', 'Util'];
app.controller('ContactCtrl', ContactCtrl);
//# sourceMappingURL=Ctrl.js.map