/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var StandaloneModal;
module.exports = StandaloneModal = (function () {
    function StandaloneModal() {
        var $scope = this.$scope, User = this.User, isSet = $scope.userSet = User.userID;
        $scope.vars = {
            sent: false
        };
        $scope.message = {
            subject: null,
            details: null,
            noResponder: true,
            type: null,
            userAgent: navigator.userAgent,
            locale: navigator.language,
            siteLocale: CP.site.prefix,
        };
        if (isSet) {
            $scope.message.email = User.data.email;
            $scope.message.name = User.format.name(null, null);
        }
        $scope.send = this.send.bind(this);
    }
    StandaloneModal.prototype.send = function () {
        var self = this;
        this.$scope.vars.sent = true;
        this.DataBus.emit('progressLoader', { start: true });
        return this.SocketIo.onEmitSock('/email/message-form', this.$scope.message)
            .then(function (response) {
            self.DataBus.emit('yapServerResponse', response);
            self.DataBus.emit('progressLoader');
            self.close();
        }).catch(function (err) {
            self.$scope.vars.sent = false;
            self.$scope.$root.safeDigest(self.$scope);
            self.DataBus.emit('yapServerResponse', err);
        });
    };
    StandaloneModal.prototype.close = function () {
        var $scope = this.$scope, results = [], $state = this._$state, args;
        while ($scope) {
            if ($scope.deactivate) {
                $scope.deactivate();
                $scope = null;
                break;
            }
            results.push($scope = $scope.$parent);
        }
        if (!$scope && $state) {
            if ($state.params.redirect) {
                args = $state.params.action && this._Util.parseActionArgs($state.params.action);
                $state.go(decodeURIComponent($state.params.redirect), args);
            }
            else {
                this._$state.go('landing.default');
            }
            $('.sa-modal').remove();
        }
        return results;
    };
    StandaloneModal.prototype.setVariables = function (variables) {
        return this.$scope.message.globalBase = variables;
    };
    return StandaloneModal;
})();
//# sourceMappingURL=standaloneModal.js.map