/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
//var commonInteraction = require('../../../../common/type/interaction'),
//    commonUser = require('../../../../common/type/user');
/**
 * This service is used to open the Signup / Login modals
 * as well as any other related modals including 'password-reset' & 'email-signup'
 */
var ModalService = (function () {
    function ModalService(_btfModal, _$q, _$templateCache, $rootScope) {
        this._btfModal = _btfModal;
        this._$q = _$q;
        this._$templateCache = _$templateCache;
        // close modal on state change
        $rootScope.$on('$stateChangeSuccess', this.deactivate.bind(this));
        this._template = _$templateCache.get('views/modals/Template.html');
    }
    ModalService.prototype._initModal = function (template, opts) {
        var options = {
            template: template,
        };
        if (opts.container) {
            options.container = opts.container;
        }
        if (opts.controller) {
            options.controller = opts.controller;
        }
        return this._modal = this._btfModal(options);
    };
    /**
     * @description Initiates the modal
     *
     * @param {object} opts
     * @param {string} opts.template - class to apply to modal box
     * @param {string=} opts.class - class to apply to root template
     * @param {object=} opts.vars
     */
    ModalService.prototype.activate = function (opts) {
        this._deferred = this._$q.defer();
        this._modal && this._modal.active() && this.deactivate();
        var template = this._wrapTpl(this._$templateCache.get(opts.template)), modal = this._initModal(template, opts), deactivate = this.deactivate.bind(this), deferred = this._deferred, vars = {}, onSubmit = this._onSubmit.bind(this, vars), localScope = {
            deactivate: deactivate,
            onSubmit: onSubmit,
            'class': opts.class || '',
            vars: vars,
        };
        if (opts.vars) {
            _.extend(vars, opts.vars);
        }
        modal.activate({
            modal: localScope,
        });
        return deferred.promise;
    };
    ModalService.prototype._wrapTpl = function (template) {
        return this._template.replace('TRANSCLUDE', template);
    };
    ModalService.prototype._onSubmit = function (vars, what) {
        this._deferred.resolve(what || vars);
        this._deferred = null;
    };
    ModalService.prototype.deactivate = function (noReject) {
        if (!this._modal || !this._modal.active()) {
            return;
        }
        this._modal.deactivate();
        this._modal = null;
        !noReject && this._deferred && this._deferred.reject();
        this._deferred = null;
        if (this._onDeactivate) {
            this._onDeactivate();
            this._onDeactivate = null;
        }
    };
    return ModalService;
})();
ModalService.$inject = [
    'btfModal',
    '$q',
    '$templateCache',
    '$rootScope',
];
require('../app').service('ModalService', ModalService);
//# sourceMappingURL=Service.js.map