/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

export class AbstractModal {
    constructor() {}

    activate() {
        // bind a method to close the modal on state change
        if (this._$state && this._$rootScope) {
            return this.deactivateBound = this._$rootScope.$on('$stateChangeStart', this.deactivate.bind(this));
        }
    }

    deactivate() {
        var modal;

        typeof this.deactivateBound === 'function' && this.deactivateBound();

        if ((modal = this.modal)) {
            this.modal = null;
            return modal.deactivate();
        }
    }
}
