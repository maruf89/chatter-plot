/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

import {AuthenticateModalCtrl} from 'root/modals/AuthenticateCtrl';

require('../../app').controller('AuthenticateModalCtrl', [
    'FB',
    'LinkedIn',
    'GPlus',
    '$scope',
    'Auth',
    '$state',
    'DataBus',
    'Config',
    '$location',
    '$analytics',
    'locale',
    AuthenticateModalCtrl,
]);
