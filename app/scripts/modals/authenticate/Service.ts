/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

/**
 * This service is used to open the Signup / Login modals
 * as well as any other related modals including 'password-reset' & 'email-signup'
 */
class AuthenticateModal {
    private _isHidden:boolean;
    private _modal:any;

    /**
     * @description if we're deactivating the modal but it still hasn't fully deactivated
     * @type {boolean}
     * @private
     */
    private _deactivating:boolean = false;

    constructor(
        btfModal:any,
        $templateCache:ng.ITemplateCacheService,
        private _$rootScope:cp.IRootScopeService,
        private _$state:ng.ui.IStateService,
        private _Util:any,
        private _Config:any
    ) {
        this._modal = btfModal({
            template: $templateCache.get('views/modals/authenticate/Template.html'),
            controller: 'AuthenticateModalCtrl as Authenticate'
        });
    }

    /**
     * @description Shows a signup/login modal
     *
     * @param  {string}  which - will be one of signup|login
     * @param  {object=} opts - optional parameters to pass to the controller
     */
    public activate(which:string, opts?:any) {
        var modal = this._modal,
            context = this,
            vars = {
                view: which,
                message: null,
            };

        if (typeof opts === 'object') {
            _.extend(vars, opts);
        }

        if (!this._deactivating && modal.active()) {
            if (this._isHidden) {
                this.show();
            }

            if (modal.data.onReactivate) {
                modal.data.onReactivate(vars);
            }

            return true;
        }

        modal.data = {
            deactivate: function () {
                return modal.active() && context.deactivate(opts.reject);
            },
            hide: this.hide.bind(this),
            onReactivate: null,
        };

        if (vars.message) {
            this._buildAuthMessage(vars);
        }

        return modal.activate({
            Service: this,
            modal: modal.data,
            vars: vars
        }, null, true);
    }

    private _toggleHide(show) {
        var toggleVal:boolean = !show;

        $('#authModal').toggleClass('hidden', toggleVal);
        this._isHidden = toggleVal;
    }

    public show() {
        this._toggleHide(true);
    }

    public hide() {
        this._toggleHide(false);
    }

    private _buildAuthMessage(vars:any):void {
        var $state = this._$state;

        this._Util.getI18nVars('auth.MODAL_BAR_BASE', [{
            key: 'to_do_smth',
            value: vars.message,
        }])
            .then(function (translation) {
                var $elem = $(`<div>${translation}</div>`),
                    link = $elem.find('a')[0];

                link.href = $state.href('standalone.addYourself');
                link.setAttribute('class', 'auth-msg-link');

                vars.authMessage = $elem.html();
            })
    }

    public deactivate(callback?:Function) {
        if (!this._deactivating) {
            this._modal.deactivate().then(function () {
                this._deactivating = false;
            }.bind(this));

            this._deactivating = true;
        }

        if (this.onDeactivate) {
            return this.onDeactivate();
        }

        callback && callback();
    }
}

AuthenticateModal.$inject = [
    'btfModal',
    '$templateCache',
    '$rootScope',
    '$state',
    'Util',
    'Config',
];

require('../../app.js').service('AuthenticateModal', AuthenticateModal);
