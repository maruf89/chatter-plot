/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

exports.prereq = function (tAttrs, content, container) {
    var contClasses = [];

    if (tAttrs.padding === 'true') {
        content.addClass('modal-padding');
    }
    if (tAttrs.absCenter) {
        contClasses.push('abs-center');
    }
    if (tAttrs.boxClass) {
        contClasses.push(tAttrs.boxClass);
    }

    // Add all potential classes 
    if (contClasses.length) {
        return container.addClass(contClasses.join(' '));
    }
};

exports.bindClose = function (scope, iElem, iAttrs, Bus) {
    scope.modal = scope.modal || {};

    // Closes the modal 
    scope.modal.deactivate = function () {
        if (iAttrs.deactivate) {
            iElem.off('click');

            // Check that angular is aware that a click just happened before deactivating 
            return scope.$root.safeApply(scope.deactivate);
        }
    };
    Bus.on('/site/deauthenticated', scope.modal.deactivate);
    iElem.on('click', function (ev) {
        if ((ev.target === ev.currentTarget && !iAttrs.disableBgClose) ||
            $(ev.target)
                .hasClass('close-modal')) {
            return scope.modal.deactivate();
        }
    });
    return scope.$on('$destroy', function () {
        return Bus.removeListener('/site/deauthenticated', scope.modal.deactivate);
    });
};
