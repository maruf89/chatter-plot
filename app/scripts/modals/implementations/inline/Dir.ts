/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../../app')
.directive('inlineModal', [
    'locale',
    function (locale) {
        return {
            restrict: 'E',
            scope: false,
            replace: true,
            transclude: true,
            templateUrl: 'views/modals/implementations/inline/Transclude.html',
            compile: function (tElement, tAttrs) {
                var $h3, $header, contClasses, container, content;
                container = tElement.find('.inline-modal');
                contClasses = [];
                content = tElement.find('.content');
                if (tAttrs.padding === 'true') {
                    content.addClass('modal-padding');
                }
                if (tAttrs.absCenter) {
                    contClasses.push('abs-center');
                }
                if (tAttrs.boxClass) {
                    contClasses.push(tAttrs.boxClass);
                }

                // Add all potential classes
                if (contClasses.length) {
                    container.addClass(contClasses.join(' '));
                }
                if (tAttrs.title) {
                    $header = $('<header class="title" />');
                    $h3 = $('<h3 />')
                        .appendTo($header);
                    locale.ready(locale.getPath(tAttrs.title))
                        .then(function () {
                            return $h3.text(locale.getString(tAttrs.title));
                        });
                    $header.prependTo(tElement);
                }
                if (tAttrs.padding === 'true') {
                    content.addClass('modal-padding');
                }
                return function () {};
            }
        };
    }
]);
