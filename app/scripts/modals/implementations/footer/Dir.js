/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var compileFn = require('../compileFn');
require('../../../app').directive('footerModal', [
    '$window', 'DataBus',
    function ($window, Bus) {
        return {
            restrict: 'E',
            scope: {
                deactivate: '&'
            },
            replace: true,
            transclude: true,
            templateUrl: 'views/modals/implementations/footer/Transclude.html',
            compile: function (tElement, tAttrs) {
                var container, content;
                container = tElement.find('.content-modal');
                content = tElement.find('.content-modal-inner');
                compileFn.prereq(tAttrs, content, container);
                return {
                    post: function (scope, iElem, iAttrs) {
                        return compileFn.bindClose(scope, iElem, iAttrs, Bus);
                    }
                };
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map