/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var compileFn = require('../compileFn');

require('../../../app').directive('footerModal', [
    '$window', 'DataBus',
    function ($window, Bus) {
        return {
            restrict: 'E',
            scope: {
                deactivate: '&'
                // disableBgClose: '@' - if set, clicking on the bg screen will not close
                // title:      '@' - title of the modal
                // padding:    '@' - set to `true` to add default padding to content
                // absCenter:  '@' - set to `true` to position absolute & center
                // boxClass:   '@' - Class to add to .title-modal
            },
            replace: true,
            transclude: true,
            templateUrl: 'views/modals/implementations/footer/Transclude.html',
            compile: function (tElement, tAttrs) {
                var container, content;
                container = tElement.find('.content-modal');
                content = tElement.find('.content-modal-inner');
                compileFn.prereq(tAttrs, content, container);
                return {
                    post: function (scope, iElem, iAttrs) {
                        return compileFn.bindClose(scope, iElem,
                            iAttrs, Bus);
                    }
                };
            }
        };
    }
]);
