/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var compileFn = require('../compileFn');

require('../../../app')
.directive('titleModal', [
    'locale', 'DataBus',
    function (locale, Bus) {
        return {
            restrict: 'E',
            scope: {
                deactivate: '&'
                // disableBgClose: '@' - if set, clicking on the bg screen will not close
                // title:      '@' - title of the modal
                // padding:    '@' - set to `true` to add default padding to content
                // absCenter:  '@' - set to `true` to position absolute & center
                // boxClass:   '@' - Class to add to .title-modal
            },
            replace: true,
            transclude: true,
            templateUrl: 'views/modals/implementations/title/Transclude.html',
            compile: function (tElement, tAttrs) {
                var container, content;
                container = tElement.find('.title-modal');
                content = tElement.find('.modal-inner');
                compileFn.prereq(tAttrs, content, container);
                return {
                    pre: function (scope, iElem, iAttrs) {

                        // template variables
                        scope.modal = {
                            title: null
                        };
                        return locale.ready(locale.getPath(iAttrs.title))
                            .then(function () {
                                return scope.modal.title =
                                    locale.getString(iAttrs.title);
                            });
                    },
                    post: function (scope, iElem, iAttrs) {
                        return compileFn.bindClose(scope, iElem,
                            iAttrs, Bus);
                    }
                };
            }
        };
    }
]);
