/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var ModalConfirm, myApp = require('../../../app');
/**
 * !Factory
 */
myApp.factory('modalConfirmFactory', [
    'btfModal',
    function (btfModal) {
        return btfModal({
            controller: [
                '$scope',
                function ($scope) {
                    // Bind deactivation when background screen is clicked 
                    return $scope.deactivate = $scope.modal.onCancel;
                }
            ],
            templateUrl: 'views/modals/implementations/confirm/Template.html'
        });
    }
]);
/**
 * !Service
 */
ModalConfirm = function ($q, Confirm, locale) {
    var self = this;
    _.each(arguments, function (val, index) {
        self[ModalConfirm.$inject[index]] = val;
    });
    return self;
};
ModalConfirm.prototype = angular.extend({
    activate: function (values) {
        var deferred, self;
        self = this;
        deferred = this.$q.defer();
        self.locale.ready('common')
            .then(function () {
            var data;
            data = {
                modal: {
                    confirm: values.confirmText || self.locale
                        .getString('common.CONFIRM'),
                    cancel: values.cancelText || self.locale
                        .getString('common.CANCEL'),
                    title: values.title,
                    message: values.message,
                    onCancel: self.closeModal.bind(self, deferred.reject),
                    onSuccess: deferred.resolve.bind(deferred, self.modalConfirmFactory
                        .deactivate),
                    className: values.className || '',
                    confirmClass: values.confirmClass ||
                        'btn-blue',
                    cancelClass: values.cancelClass ||
                        'btn-red'
                }
            };
            return self.modalConfirmFactory.activate(data);
        });
        return deferred.promise;
    },
    closeModal: function (rejectResolve) {
        this.modalConfirmFactory.deactivate();
        if (rejectResolve) {
            return rejectResolve();
        }
    }
});
ModalConfirm.$inject = ['$q', 'modalConfirmFactory', 'locale'];
myApp.service('modalConfirm', ModalConfirm);
//# sourceMappingURL=confirm.js.map