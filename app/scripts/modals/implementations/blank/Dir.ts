/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var compileFn = require('../compileFn');

require('../../../app').directive('blankModal', [
    'locale', 'DataBus',
    function (locale, Bus) {
        return {
            restrict: 'E',
            scope: {
                deactivate: '&'
                // disableBgClose: '@' - if set, clicking on the bg screen will not close
                // padding:    '@' - set to `true` to add default padding to content
                // absCenter:  '@' - set to `true` to position absolute & center
                // boxClass:   '@' - Class to add to .title-modal
            },
            replace: true,
            transclude: true,
            templateUrl: 'views/modals/implementations/blank/Transclude.html',
            compile: function (tElement, tAttrs) {
                var container = tElement.find('.blank-modal'),
                    content = tElement.find('.modal-inner');

                compileFn.prereq(tAttrs, content, container);

                return {
                    post: function (scope, iElem, iAttrs) {
                        compileFn.bindClose(scope, iElem, iAttrs, Bus);
                    }
                };
            }
        };
    }
]);
