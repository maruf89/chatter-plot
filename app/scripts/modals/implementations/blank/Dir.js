/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var compileFn = require('../compileFn');
require('../../../app').directive('blankModal', [
    'locale', 'DataBus',
    function (locale, Bus) {
        return {
            restrict: 'E',
            scope: {
                deactivate: '&'
            },
            replace: true,
            transclude: true,
            templateUrl: 'views/modals/implementations/blank/Transclude.html',
            compile: function (tElement, tAttrs) {
                var container = tElement.find('.blank-modal'), content = tElement.find('.modal-inner');
                compileFn.prereq(tAttrs, content, container);
                return {
                    post: function (scope, iElem, iAttrs) {
                        compileFn.bindClose(scope, iElem, iAttrs, Bus);
                    }
                };
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map