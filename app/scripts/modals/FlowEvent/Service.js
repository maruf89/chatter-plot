/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
var FlowEvent = (function () {
    function FlowEvent(_$q, _$state, _Events, _ModalService, _Util, _Config) {
        this._$q = _$q;
        this._$state = _$state;
        this._Events = _Events;
        this._ModalService = _ModalService;
        this._Util = _Util;
        this._Config = _Config;
        this._duplicateTemplate = 'views/modals/FlowEvent/DuplicateTemplate.html';
    }
    FlowEvent.prototype.view = function (eID, subType) {
        console.log('view event…');
        return this._$q.when({ completed: true });
    };
    FlowEvent.prototype.duplicate = function (eID, subType) {
        var template = this._duplicateTemplate, modal = this._ModalService, Events = this._Events, context = this, modify = Events.modify, doPublish, venueDetails;
        return this._Events.get({
            evTypes: [{
                    eID: eID,
                    type: subType,
                }],
            venues: true,
            description: true
        })
            .then(function (eventArr) {
            venueDetails = eventArr[0].venueDetails;
            return modify.parseEdit(eventArr[0], ['duplicate']);
        })
            .then(function (vars) {
            return modal.activate({
                'class': 'Flow-Ev Event Create duplicate',
                template: template,
                container: '.map-pane',
                vars: vars,
            });
        })
            .then(function (results) {
            var event = results[0].newEvent, vars = results[0].vars;
            doPublish = results[1];
            return modify.formatEdit(event, ['date'], vars);
        }).catch(function (err) {
            console.log(err);
            throw err;
        })
            .then(function (formattedEvent) {
            return Events.create(formattedEvent, venueDetails);
        })
            .then(function (Event) {
            // Should we publish?
            var promise = doPublish ? Events.publish(Event) : context._$q.when();
            return promise.then(function () {
                Event.published = doPublish;
                _.defer(function () {
                    var args = context._Util.encodeActionArgs({
                        action: 'view',
                        objID: Event.eID,
                        type: 'event',
                    });
                    context._$state.go(context._Config.routes.dashboard, { action: args, state: 'current' });
                });
                return { completed: true, request: Event };
            });
        })
            .finally(function () {
            modal.deactivate();
        });
    };
    return FlowEvent;
})();
require('../../app').service('FlowEvent', [
    '$q',
    '$state',
    'Events',
    'ModalService',
    'Util',
    'Config',
    FlowEvent,
]);
//# sourceMappingURL=Service.js.map