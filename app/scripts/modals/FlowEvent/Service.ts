/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

"use strict";

class FlowEvent implements cp.modal.flow.IEventService {
    private _duplicateTemplate:string = 'views/modals/FlowEvent/DuplicateTemplate.html';

    constructor(
        private _$q:ng.IQService,
        private _$state:ng.ui.IStateService,
        private _Events:cp.event.IService,
        private _ModalService:cp.modal.IService,
        private _Util:any,
        private _Config:any
    ) {}

    public view(eID:number, subType:string):ng.IPromise<any> {
        console.log('view event…');
        return this._$q.when({ completed: true });
    }

    public duplicate(eID:number, subType:string):ng.IPromise<any> {
        var template = this._duplicateTemplate,
            modal = this._ModalService,
            Events = this._Events,
            context = this,
            modify = Events.modify,
            doPublish:boolean,
            venueDetails:cp.venue.IVenueFull;

        return this._Events.get({
            evTypes: [{
                eID: eID,
                type: subType,
            }],
            venues: true,
            description: true
        })
            .then(function (eventArr:cp.event.IEventFormatted[]) {
                venueDetails = eventArr[0].venueDetails;
                return modify.parseEdit(eventArr[0], ['duplicate']);
            })

            // Open the modal
            .then(function (vars) {
                return modal.activate({
                    'class': 'Flow-Ev Event Create duplicate',
                    template: template,
                    container: '.map-pane',
                    vars: vars,
                });
            })

            // Format the event
            .then(function (results:any[]) {
                var event = results[0].newEvent,
                    vars = results[0].vars;

                doPublish = results[1];

                return modify.formatEdit(event, ['date'], vars)
            }).catch(function (err) {
                console.log(err);
                throw err;
            })

            // Create the event
            .then(function (formattedEvent:cp.event.IEventFormatted) {
                return Events.create(formattedEvent, venueDetails);
            })

            // Event Created
            .then(function (Event:cp.event.IEventFormatted) {
                // Should we publish?
                var promise:ng.IPromise<any> = doPublish ? Events.publish(Event) : context._$q.when();

                return promise.then(function () {
                    Event.published = doPublish;

                    _.defer(function () {
                        var args = context._Util.encodeActionArgs({
                            action: 'view',
                            objID: Event.eID,
                            type: 'event',
                        });
                        context._$state.go(context._Config.routes.dashboard, { action: args, state: 'current' });
                    });

                    return { completed: true, request: Event };
                })
            })

            .finally(function () {
                modal.deactivate();
            });
    }
}

require('../../app').service('FlowEvent', [
    '$q',
    '$state',
    'Events',
    'ModalService',
    'Util',
    'Config',
    FlowEvent,
]);
