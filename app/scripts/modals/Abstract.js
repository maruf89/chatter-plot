/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var AbstractModal = (function () {
    function AbstractModal() {
    }
    AbstractModal.prototype.activate = function () {
        // bind a method to close the modal on state change
        if (this._$state && this._$rootScope) {
            return this.deactivateBound = this._$rootScope.$on('$stateChangeStart', this.deactivate.bind(this));
        }
    };
    AbstractModal.prototype.deactivate = function () {
        var modal;
        typeof this.deactivateBound === 'function' && this.deactivateBound();
        if ((modal = this.modal)) {
            this.modal = null;
            return modal.deactivate();
        }
    };
    return AbstractModal;
})();
exports.AbstractModal = AbstractModal;
//# sourceMappingURL=Abstract.js.map