/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

import {Util} from 'root/services/util';
import {Language} from 'root/services/util/language';

/**
 * @description A light weight scroll to utility which has LOTS of room for improvement
 * @name Util#$scrollTo
 * @param {object} $elem - The element we're scrolling to
 * @param {object} $parent - The closest parent with scroll enabled (default:$('.pane-wrapper-scroll'))
 * @param {object=} opts
 * @param {number} opts.padding
 */
Util.prototype.$scrollTo = function ($elem, $parent, opts) {
    opts = opts || {};

    $elem = $elem instanceof jQuery ? $elem : $($elem);

    var bounding = $elem[0].getBoundingClientRect(),
        elHeight = $elem.height(),
        winHeight = CP.Cache.$window.height(),
        scroll;

    if (!$parent) {
        $parent = $elem.closest('.pane-wrapper-scroll');

        if (!$parent.length) {
            $parent = $.merge(CP.Cache.$body, CP.Cache.$html);
        }
    }

    if (bounding.top <= 0) {
        scroll = Math.max($parent.scrollTop() + bounding.top - (opts.padding || 0), 0);
    } else if (bounding.top <= elHeight) {
        scroll = Math.max($parent.scrollTop() - (elHeight - bounding.top) - (opts.padding || 0), 0);
    } else if (bounding.top + elHeight >= winHeight) {
        scroll = Math.max($parent.scrollTop() - (winHeight - (
            bounding.top + elHeight) - (opts.padding || 0)), 0);
    } else if (bounding.bottom - winHeight > 0) {
        scroll = $parent.scrollTop() + (bounding.bottom - winHeight);
    } else {
        return;
    }

    $parent.animate({
        scrollTop: scroll
    }, opts.duration || 350);
};

Language.ttSubstringMatcher = function (
    strs,
    matches,
    exclude,
    langOptionLimit,
    indexOffset
) {
    if (langOptionLimit == null) {
        langOptionLimit = matches.length;
    }
    if (indexOffset == null) {
        indexOffset = 0;
    }

    var allCache;

    /**
     * @param {string} q - the value of the inputted text
     * @param {function} sync - inner callback from the typeahead module that MUST be called with the matches
     * to finish processing
     */
    return function (q, sync) {

        // A optimization to trigger default search
        var substrRegex;
        if (q === '…') {
            return sync([]);
        }

        if (q === '' && allCache) {
            return sync(allCache);
        }

        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, "i");

        // empty the array
        matches.length = 0;

        // iterate through the pool of strings and for any string that

        // contains the substring `q`, add it to the `matches` array
        _.each(strs, function (str, index) {

            // the typeahead jQuery plugin expects suggestions to a

            // JavaScript object, refer to typeahead docs for more info
            if (substrRegex.test(str)) {
                if (~_.indexOf(exclude, str)) {
                    return true;
                }

                // The languages are already sorted by their id's in ascending order

                // starting from 1, so we need to make up for array's 0-base indexing
                if (matches.push({
                        value: str,
                        id: index + indexOffset
                    }) === langOptionLimit) {

                    // if the number of items in the array is our limit, then stop looping
                    return false;
                }
            }
            return true;
        });

        if (q === '') {
            allCache = _.clone(matches);
        }

        return sync(matches);
    };
}

require('../app').service('Util', [
    'localStorageService',
    '$q',
    '$rootScope',
    'locale',
    '$location',
    '$state',
    '$stateParams',
    '$http',
    Util,
]);
