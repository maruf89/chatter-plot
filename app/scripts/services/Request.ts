/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

import App = require('../app');

var Request, self = null;

Request = function () {
    self = this;
    _.each(arguments, function (val, index) {
        self[Request.$inject[index]] = val;
    });
    if (/\sPrerender\s/.test(window.navigator.userAgent)) {
        Request.prototype.onEmitSock = Request.prototype.httpRequest;
    } else {
        self.onEmitSock = self.SocketIo.onEmitSock.bind(self.SocketIo);
    }
    return self;
};

Request.prototype = _.extend(Request.prototype, {
    httpRequest: function (path, data, rID) {
        self = this;
        return self.$http.post(path, data)
            .then(function (response) {
                return response.data;
            });
    }
});

Request.$inject = ['$rootScope', 'SocketIo', '$http'];

App.service('Request', Request);
