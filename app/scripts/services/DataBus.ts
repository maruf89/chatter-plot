/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

import App = require('../app');
import {DataBus} from 'root/services/DataBus';

App.service('DataBus', DataBus);
