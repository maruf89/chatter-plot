/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

import App = require('../app');

var API, self = null;

API = function () {
    var loc;
    self = this;
    _.each(arguments, function (val, index) {
        return self[API.$inject[index]] = val;
    });
    if (String((loc = self.localStorageService).get('_version')) !== String(CP.deployVersion)) {
        loc.remove('cash.API:settings:tz');
        loc.remove('cash.API:settings:locale');
    }
    return this;
};

API.prototype = angular.extend(API.prototype, {
    convert: {

        /**
         * Converts the passed in language id's to object form with the name
         *
         * @param {array} ids  Array of language ID's
         */
        ID2Language: function (ids) {
            return self.convert._helper(ids, 'ID2L', 'ID2Language');
        },

        /**
         * Does the opposite of the previous function, retrieving the 
         *
         * @param {array} languages [description]
         */
        Language2ID: function (languages) {
            return self.convert._helper(ids, 'L2ID', 'Language2ID');
        },
        _helper: function (values, cache, method) {
            var cacheKey, cached;
            if (!_.isArray(values)) {
                throw new Error(("Expecting the first parameter for " +
                        method + " to be an Array. Instead got ") +
                    typeof values);
            }
            values.sort();
            cacheKey = values.join('|');
            cacheKey = "cash.API:conv:" + cache + ":" + cacheKey;
            if (cached = self.localStorageService.get(cacheKey)) {
                return self.$q.when(cached);
            }
            return self.SocketIo.onEmitSock("api/convert/" + method,
                    values)
                .then(function (cached) {
                    self.localStorageService.set(cacheKey, cached);
                    return cached;
                });
        }
    },
    venue: {

        /**
         * Given an array of venue ids (vID) will return the corresponding documents for them
         * 
         * @param  {array<number>} ids - venue vID's
         * @param  {boolean} obj - Whether the promise should return in object format
         * @return {Promise<array|object>} Returns in array format by default
         */
        get: function (ids, obj) {
            var cacheKey, cached;
            if (!_.isArray(ids)) {
                throw new Error("Expecting the first parameter for get to be an Array. Instead got " + typeof values);
            }

            // If we want a response, recurse this function to get array response and 

            // then convert it to an object format 
            if (obj) {
                return self.venue.get(ids).then(function (venues) {
                    var venueObj;
                    venueObj = {};
                    _.each(venues, function (venue) {
                        return venueObj[venue.vID] =
                            venue;
                    });
                    return venueObj;
                });
            }
            ids.sort();
            cacheKey = ids.join('|');
            cacheKey = "cash.API:venue:get:" + cacheKey;
            if (cached = self.localStorageService.get(cacheKey)) {
                return self.$q.when(cached);
            }
            return self.SocketIo.onEmitSock('/venue/get', ids)
                .then(function (cached) {
                    self.localStorageService.set(cacheKey, cached);
                    return cached;
                });
        }
    },
    settings: {
        timeZone: function () {
            return self._helper('cash.API:settings:tz', 'api/settings/tz');
        }
    },

    _helper: function (cacheKey, call) {
        var cached;
        if (cached = self.localStorageService.get(cacheKey)) {
            return self.$q.when(cached);
        }
        return self.SocketIo.onEmitSock(call)
            .then(function (cached) {
                self.localStorageService.set(cacheKey, cached);
                return cached;
            });
    }
});

API.$inject = ['SocketIo', 'localStorageService', '$q'];

App.service('API', API);
