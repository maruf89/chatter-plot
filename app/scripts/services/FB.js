/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var oauthMethods = {
    connect: 'connectURL',
    verify: 'verifyURL'
}, SERVICE_NAME = 'facebook';
var FB = (function () {
    function FB(_Facebook, _$http, _$window, _$q, _locale, _DataBus, _SocketIo, _Auth, _ipCookie) {
        this._Facebook = _Facebook;
        this._$http = _$http;
        this._$window = _$window;
        this._$q = _$q;
        this._locale = _locale;
        this._DataBus = _DataBus;
        this._SocketIo = _SocketIo;
        this._Auth = _Auth;
        this._ipCookie = _ipCookie;
        this._statusWaiting = [];
        this._loadDefer = [];
        var onLoaded = this._onLoaded.bind(this), context = this;
        this._DataBus.on('/site/authChange', this._listenFBAuthChange.bind(this));
        this._listenFBAuthChange(false);
        window.fbAsyncInit = function () {
            if (!window.FB || !window.FB.init) {
                return setTimeout(window.fbAsyncInit, 50);
            }
            window.FB.init({
                appId: CP.Settings.facebook.appId,
                version: 'v2.4',
                status: true,
                cookie: true,
                xfbml: true
            });
            window.FB.Event.subscribe('auth.statusChange', function (response) {
                var fn, results = [];
                context._Facebook.setState(response);
                while (context._statusWaiting[0]) {
                    fn = context._statusWaiting.shift();
                    results.push(fn());
                }
                return results;
            });
            _.defer(onLoaded);
        };
        // Facebook init script
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            // load the corresponding locale
            js.src = "//connect.facebook.net/" + (context._locale.getLocale()
                .replace('-', '_')) + "/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        })(document, 'script', 'facebook-jssdk');
        _DataBus.on('/site/unauthenticated/init', this.checkIfLoggedIn.bind(this));
    }
    FB.prototype._onLoaded = function () {
        if (this._loadDefer.length) {
            _.each(this._loadDefer, function (deferred) {
                deferred.resolve();
            });
        }
        this._loadDefer = null;
        this.onLoaded = this._$q.when;
    };
    FB.prototype.onLoaded = function () {
        var deferred = this._$q.defer();
        this._loadDefer.push(deferred);
        return deferred.promise;
    };
    /**
     * @description if not logged in will join a channel to listen to whenever the facebook auth cache buster
     * key changes and update the settings to that
     * @param removeListener
     * @private
     */
    FB.prototype._listenFBAuthChange = function (removeListener) {
        _.defer(function () {
            var eventKey = 'settings/change', cbListener = this._onFBKeyChange;
            if (removeListener) {
                if (cbListener) {
                    cbListener.remove();
                    this._onFBKeyChange = null;
                }
            }
            else if (!cbListener) {
                this._SocketIo.onReady().then(function () {
                    // make sure there's no race condition while waiting for SocketIo to boot up
                    if (!this._Auth.isAuthenticated()) {
                        this._onFBKeyChange = this._SocketIo.join(eventKey, function (data) {
                            // since the key we're listening on isn't FB specific
                            if ('facebook' in data) {
                                CP.Settings.facebook = data.facebook;
                            }
                        });
                    }
                }.bind(this));
            }
        }.bind(this));
    };
    FB.prototype.checkIfLoggedIn = function () {
        if (this._ipCookie('disableSocialConnect')) {
            return false;
        }
        // If we logged out then we do not want to force login again
        if (!window.FB || !window.FB.init) {
            return setTimeout(this.checkIfLoggedIn.bind(this), 50);
        }
        var state = this._Facebook.state, context = this;
        if (!state) {
            return this._statusWaiting.push(this.checkIfLoggedIn.bind(this));
        }
        if (state.status !== 'connected') {
            return false;
        }
        return this._SocketIo.onEmitSock('/auth/serviceToken', {
            service: SERVICE_NAME,
            data: state.authResponse
        })
            .then(function (response) {
            // Lastly try to authenticate with the token if we got one 
            if (response.token) {
                context._Auth.setOnAuthHook($.noop, true);
                return context._Auth.authenticateWithToken(response);
            }
        });
    };
    FB.prototype.share = function (vars) {
        var width = 550, height = 500, leftPosition = (this._$window.innerWidth / 2) - ((width / 2) + 10), topPosition = (this._$window.innerHeight / 2) - ((height / 2) + 50), windowFeatures = 'status=no,height=' + height +
            ',width=' + width +
            ',resizable=yes,left=' + leftPosition +
            ',top=' + topPosition +
            ',screenX=' + leftPosition +
            ',screenY=' + topPosition +
            ',toolbar=no,menubar=no,scrollbars=no,location=no,directories=no';
        return window.open('https://www.facebook.com/sharer.php?u=' +
            encodeURIComponent(vars.url) + '&t=' +
            encodeURIComponent(vars.text), 'sharer', windowFeatures);
    };
    FB.prototype.getLoginStatus = function () {
        return this._Facebook.getLoginStatus();
    };
    FB.prototype.login = function () {
        return this._Facebook.login();
    };
    FB.prototype.logout = function () {
        this._Facebook.logout();
        return this._$http.post('/logout/facebook');
    };
    FB.prototype.unsubscribe = function () {
        return this._Facebook.unsubscribe();
    };
    FB.prototype.getInfo = function (callback) {
        return window.FB.api("/" + this.id, callback);
    };
    FB.prototype.query = function (method, queryObj, callback) {
        return window.FB.api({
            method: method,
            queries: queryObj
        }, callback);
    };
    FB.prototype.connect = function () {
        return this._oauthCall('connect');
    };
    FB.prototype.verify = function () {
        return this._oauthCall('verify');
    };
    FB.prototype._oauthCall = function (method) {
        var deferred = this._$q.defer(), callback = function (err, service) {
            if (err) {
                return deferred.reject(service);
            }
            return deferred.resolve(service);
        }, oauthMethod = oauthMethods[method], oauthURL = CP.Settings.facebook[oauthMethod]
            .replace('redirect_uri=.', 'state=' + CP.site.prefix + '&redirect_uri=https://' + CP.site.prefix + '.');
        window.onFacebookAuth = callback;
        this._$window.open(oauthURL, 'AuthenticateFacebook', 'width=600, height=600');
        return deferred.promise;
    };
    return FB;
})();
FB.$inject = [
    'Facebook',
    '$http',
    '$window',
    '$q',
    'locale',
    'DataBus',
    'SocketIo',
    'Auth',
    'ipCookie'
];
require('../app').service('FB', FB);
//# sourceMappingURL=FB.js.map