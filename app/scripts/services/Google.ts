/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../app').service('Google', [
    '$q',
    'uiGmapGoogleMapApi',
    'Maps',
    require('root/services/Google')
]);
