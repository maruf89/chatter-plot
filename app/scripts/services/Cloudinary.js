/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
/**
 * @class
 * @classdesc handles everything on the front end with Cloudinary and image generation stuff
 */
var Cloudinary = require('root/services/Cloudinary');
require('../app')
    .service('Cloudinary', [
    'SocketIo',
    Cloudinary.service,
])
    .directive('cloudImg', [
    'Cloudinary',
    Cloudinary.directive,
]);
//# sourceMappingURL=Cloudinary.js.map