/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var features = require('./features.json');
require('../../app').factory('NewFeature', [
    'User', 'SocketIo', 'btfModal', 'DataBus', '$templateCache', '$compile',
    function (User, SocketIo, btfModal, DataBus, $templateCache, $compile) {
        /**
         * @ngdoc factory
         * @class
         * @classdesc Facilitates showing feature updates to the user
         */
        var NewFeature = function (featureName, data) {
            this.featureName = featureName;
            this.options = features[featureName];
            this.data = data || {};
        };
        NewFeature.prototype = _.extend(NewFeature.prototype, {
            /**
             * Checks whether a feature is applicable to a user
             * @param {boolean} openIfFound - if true and applicable, will activate the feature
             * @param {string=} featureName
             * @returns {boolean}
             */
            check: function (openIfFound, featureName) {
                featureName = featureName || this.featureName;
                // If not logged in, exit
                if (!User.userID) {
                    return false;
                }
                var features = User.data.features;
                // If not applicable -> quit
                if (!_.isArray(features) || features.indexOf(featureName) === -1) {
                    return false;
                }
                // applicable
                if (openIfFound) {
                    this.activate();
                }
                return true;
            },
            getModule: function (featureName) {
                featureName = featureName || this.featureName;
                var module = features[featureName].module;
                if (module) {
                    return $compile("<" + module + "></" + module + ">");
                }
                return false;
            },
            /**
             * @description Builds the modal if it doesn't exist yet
             * @returns {btfModal}
             * @private
             */
            _getModal: function () {
                if (this.modal) {
                    return this.modal;
                }
                var options = {
                    controller: this.options.controller
                };
                if (this.options.templateUrl) {
                    options.templateUrl = this.options.templateUrl;
                }
                else if (this.options.templateCache) {
                    options.template = $templateCache.get(this.options.templateCache);
                }
                else {
                    options.template = this.options.template;
                }
                return this.modal = btfModal(options);
            },
            /**
             * @description Activates the feature
             */
            modalActivate: function () {
                var modal = this._getModal();
                if (modal.active()) {
                    return false;
                }
                modal.activate({
                    modal: {
                        deactivate: this.deactivate.bind(this, this.data.onCancel, true)
                    },
                    vars: this.data.vars,
                    onSuccess: this.deactivate.bind(this, this.data.onSuccess, true)
                });
            },
            /**
             * @description deactivates the featured modal & removes the feature
             * @param {function=} callback - if passed will be called
             */
            deactivateModal: function (callback, remove) {
                if (typeof callback === 'function') {
                    callback();
                }
                this.modal.active() && this.modal.deactivate();
            },
            /**
             * @description opposite of removeFeature
             * @private
             */
            markFeature: function () {
                if (!User.userID) {
                    return;
                }
                DataBus.emit("/feature/mark/" + this.featureName);
                SocketIo.onEmitSock('/user/feature/unseen', this.featureName).catch(function (err) {
                    console.log(err);
                    throw new Error('Error updating a feature as unseen');
                })
                    .then(function () {
                    // on success remove the feature locally
                    var index = User.data.features.indexOf(this.featureName);
                    if (index === -1) {
                        User.data.features.push(this.featureName);
                    }
                }.bind(this));
            }
        });
        return NewFeature;
    }
]);
//# sourceMappingURL=Service.js.map