/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../../../app.js').directive('venueInfo', [
    'Venues', '$templateCache',
    function (Venues, $templateCache) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                venue: '=?',
            },
            template: $templateCache.get('views/services/Venues/directives/info/Template.html'),
            link: {
                pre: function (scope) {
                    var vars, openingHours = scope.venue.opening_hours, text, 
                    // get the current day of the week as a number
                    today = (new Date()).getDay();
                    vars = scope.vars = {
                        openClose: function (isOpen) {
                            return 'misc.' + (isOpen ? 'STORE_OPEN_NOW' : 'STORE_CLOSED_NOW');
                        },
                        weekdayText: null,
                        gmapUrl: Venues.addressStringToUrl('google', (scope.venue.name || ''), scope.venue.formatted_address)
                    };
                    if (openingHours && (text = openingHours.weekday_text)) {
                        vars.weekdayText = _.map(text, function (text, index) {
                            // convert text like 'Montag: 12:00–22:30' to 'Montag' & '12:00-22-30'
                            var parts = text.split(': '), day = {
                                name: text,
                                hours: null,
                                today: (today === index + 1)
                            };
                            if (_.isArray(parts)) {
                                day.name = parts[0];
                                day.hours = parts[1];
                            }
                            return day;
                        });
                    }
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map