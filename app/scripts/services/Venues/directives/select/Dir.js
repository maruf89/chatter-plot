/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var photoSize = 65, photoReqSize = Math.round(photoSize * 1.5 * window.devicePixelRatio);
require('../../../../app.js').directive('venuesSelect', [
    'Venues', '$templateCache',
    function (Venues, $templateCache) {
        var formatSelect = function (venueObjs) {
            return _.map(venueObjs, function (venueObj) {
                venueObj.venue.picture = venueObj.venue.picture || Venues.getPhotoSrc(venueObj.service, photoReqSize);
                return venueObj;
            });
        };
        return {
            restrict: 'E',
            replace: true,
            require: '^form',
            scope: {
                suggested: '=?',
                venueData: '=?',
                selectedObj: '=',
            },
            template: $templateCache.get('views/services/Venues/directives/select/Template.html'),
            link: {
                pre: function (scope, iElem, iAttrs, Form) {
                    var vars, suggestedLength = scope.suggested && scope.suggested.length || 0;
                    scope.vars = vars = {
                        loaded: true,
                        venueData: scope.venueData || [],
                        unique: 'ven-sel' + scope.$id,
                        selected: scope.selectedObj,
                        emptyInput: 0,
                    };
                    Form.$setValidity('hiddenSelect', false);
                    scope.$watch('vars.selected.index', function (post, prev) {
                        if (post !== prev) {
                            Form.$setValidity('hiddenSelect', !!post);
                        }
                    });
                    // if we have locations to show by default
                    if (suggestedLength) {
                        if (scope.venueData && scope.venueData.length === suggestedLength) {
                            // check if we already have all of the data
                            vars.venueData = formatSelect(scope.venueData);
                        }
                        else {
                            // If not then mark self as loading
                            vars.loaded = false;
                            Venues.getPlaceBulk(scope.suggested)
                                .then(_.partial(Venues.formatBulk, scope.suggested, 1))
                                .then(function (venues) {
                                // add to the existing array without breaking reference
                                [].push.apply(vars.venueData, formatSelect(venues));
                                vars.loaded = true;
                                scope.$root.safeDigest(scope.$parent);
                            });
                        }
                    }
                },
                post: function (scope, iElem, iAttrs, Form) {
                    var vars = scope.vars, existsInList = function (place) {
                        _.every(vars.venueData, function (suggested) {
                            return suggested.service.id !== place.id;
                        });
                    };
                    vars.onLocSelect = function (place) {
                        if (existsInList(place)) {
                            return false;
                        }
                        var venueObj = [Venues.place2VenueObj(place, 'google')];
                        vars.venueData.push(formatSelect(venueObj)[0]);
                        vars.emptyInput++;
                        scope.$root.safeDigest(scope);
                    };
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map