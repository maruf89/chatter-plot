/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../../../app.js').directive('venuesListingDummy', ['$templateCache', 'locale',
    function ($templateCache, locale) {
        return {
            restrict: 'E',
            replace: true,
            template: $templateCache.get('views/services/Venues/directives/listingDummy/Template.html'),
            link: {
                pre: function (scope, iElem) {
                    scope.vars = {
                        rating: 4.2
                    };
                    locale.ready('activities').then(function () {
                        var faveString = locale.getString('activities.PLACEHOLDER-FAKE_DESIRED_LOC'), matches = faveString.match(/(^[^,]+),\s?(.+)/), isMatch = _.isArray(matches);
                        if (isMatch) {
                            iElem.find('.v-name')[0].innerText = matches[1];
                            iElem.find('.v-address')[0].innerText = matches[2];
                        }
                    });
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map