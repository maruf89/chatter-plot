/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var App = require('../app');
var Meta = function () {
    _.each(arguments, function (val, index) {
        this[Meta.$inject[index]] = val;
    }, this);
    return this;
};
Meta.prototype = _.extend(Meta.prototype, {
    stripHTML: function (html) {
        // Finish all paragraphs with a line break & replace spaces with spaces 
        var tmp;
        html = html.replace(/<\/p>/g, '\n')
            .replace(/\&nbsp;/g, ' ');
        tmp = document.createElement("DIV");
        tmp.innerHTML = html;
        return tmp.textContent || tmp.innerText || "";
    },
    canonical: function () {
        return this.$state.href(this.$state.current.name, null, {
            absolute: true
        });
    },
    image: function (cloudinaryID, type) {
        return this.Cloudinary.generateSrc(cloudinaryID, null, type, {
            width: 600,
            height: 400,
            fill: true
        });
    }
});
Meta.$inject = ['Cloudinary', '$state'];
App.service('Meta', Meta);
//# sourceMappingURL=Meta.js.map