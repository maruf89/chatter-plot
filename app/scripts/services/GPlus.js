/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var App = require('../app');
var oauthMethods = {
    connect: 'connectURL',
    verify: 'verifyURL'
}, GPlus = function ($q, $window) {
    this._$q = $q;
    this._$window = $window;
};
GPlus.prototype = _.extend(GPlus.prototype, {
    share: function (vars) {
        var width = 550, height = 500, leftPosition = (this._$window.innerWidth / 2) - ((width / 2) + 10), topPosition = (this._$window.innerHeight / 2) - ((height / 2) + 50), windowFeatures = 'status=no,height=' + height + ',width=' +
            width + ',resizable=yes,left=' + leftPosition + ',top=' +
            topPosition + ',screenX=' + leftPosition + ',screenY=' +
            topPosition +
            ',toolbar=no,menubar=no,scrollbars=no,location=no,directories=no';
        this._$window.open('https://plus.google.com/share?url=' +
            encodeURIComponent(vars.url), 'sharer', windowFeatures);
    },
    connect: function () {
        return this._oauthCall('connect');
    },
    verify: function () {
        return this._oauthCall('verify');
    },
    _oauthCall: function (method) {
        var deferred = this._$q.defer(), callback = function (err, service) {
            if (err) {
                return deferred.reject(service);
            }
            return deferred.resolve(service);
        }, oauthMethod = oauthMethods[method], oauthURL = CP.Settings.google[oauthMethod]
            .replace('redirect_uri=.', 'state=' + CP.site.prefix + '&redirect_uri=https://' + CP.site.prefix + '.');
        window.onGoogleAuth = callback;
        this._$window.open(oauthURL, 'AuthenticateGoogle', 'width=600, height=600');
        return deferred.promise;
    }
});
GPlus.$inject = [
    '$q',
    '$window',
];
App.service('GPlus', GPlus);
//# sourceMappingURL=GPlus.js.map