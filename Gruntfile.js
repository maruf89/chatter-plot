'use strict';

const _ = require('lodash-node/modern');

const defaultEnvironments = {
    mbuild: 'production',
    build: 'production',
    dev: 'development',
    staging: 'staging',
};

const supportedLocales = require('./common/config/locales.json');
const devLocales = {
    'en': null,
};

module.exports = function (grunt) {
    var jsonConfig = grunt.file.readJSON('wwwconfig.json'),
        defEnv = grunt.cli.tasks.length ? grunt.cli.tasks[0] : 'development',
        targetenv = grunt.option('targetenv') || defaultEnvironments[defEnv] || 'development',
        envConfig = jsonConfig[targetenv],

        jsHashVar = 'jsHashres',
        iconHashVar = 'iconHashres';

    grunt.option('targetenv', targetenv);

    var nodemonIgnoredFiles = [
        'tmp/**',
        'app/**',
        'dist/**',
        'node_modules/**',
        'test/**',
        'doc/**',
        '.bowerrc',
        '.editorconfig',
        '.gitattributes',
        '.gitignore',
        '.jshintrc',
        '*.json',
        'README.md',
        'Gruntfile.js',
        'karma.conf.js',
        'common/front',
        'common/assets',
    ];

    grunt.loadNpmTasks('grunt-angular-localization');

    require('jit-grunt')(grunt, {
        ngtemplates: 'grunt-angular-templates',
    });

    // Load grunt tasks automatically
    //require('load-grunt-tasks')(grunt);

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    // Define the configuration for all the tasks
    grunt.initConfig({
        config: {
            app:            'app',
            scripts:        'app/scripts',
            styles:         'app/styles',
            lib:            'app/lib',
            templates:      'app/views',
            common:         'common',
            distCommon:     'dist/common',
            assets:         'common/assets',
            commonFront:    'common/front',
            distCommonFront:'dist/common/front',
            components:     'common/front/bower_components',
            modules:        'common/front/modules',
            libraries:      'common/front/libraries',
            commonSections: 'common/front/sections',
            viewsDev:       './views',
            publicDev:      'public/',
            distTemp:       'dist/.tmp',
            nodeModules:    './node_modules',
            dist:           'dist',
            publicDist:     'dist/public',
        },

        browserify: {
            dev: {
                options: {
                    watch: false // doesn't always work
                },
                files: _.reduce(devLocales, function (obj, val, prefix) {
                    obj['<%= config.publicDev %>/scripts/bundle.' + prefix + '.js'] = [
                        '<%= config.commonSections %>/config/locales/' + prefix + '.app.js',
                        '<%= config.app %>/scripts/*/**/*.js',
                    ];

                    return obj;
                }, {})
            },
            dist: {
                files: _.reduce(supportedLocales.defaultMap, function (obj, val, prefix) {
                    obj['<%= config.distTemp %>/scripts/' + prefix + '.app.js'] =
                        '<%= config.commonSections %>/config/locales/' + prefix + '.app.js';

                    return obj;
                }, {
                    '<%= config.distTemp %>/scripts/main.js': '<%= config.app %>/scripts/*/**/*.js'
                }),
            }
        },

        // Empties folders to start fresh
        clean: {
            dev: {
                files: [{
                    dot: true,
                    src: [
                        'tmp',
                        '<%= config.viewsDev %>/**',
                        '!<%= config.viewsDev %>/m',
                        '<%= config.publicDev %>/**',
                        '!<%= config.publicDev %>/m',
                        '<%= config.templates %>',
                        '<%= config.scripts %>/bundle*',
                        '<%= config.nodeModules %>/front',
                        '<%= config.nodeModules %>/root',
                        '<%= config.nodeModules %>/common',
                    ]
                }]
            },
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '<%= config.dist %>',
                        '<%= config.scripts %>/bundle*',
                        '<%= config.nodeModules %>/front',
                        '<%= config.nodeModules %>/root',
                        '<%= config.nodeModules %>/common',
                    ]
                }]
            },
            server: '<%= config.publicDev %>',
            styles: '<%= config.publicDev %>/styles',
            scripts: '<%= config.publicDev %>/scripts',
            devStylusMain: '<%= config.styles %>/main.css',
            document: './doc',
            docBundle: './doc/source/front/bundle.js',
        },

        // Run some tasks in parallel to speed up the build process
        concurrent: {
            dev1: [
                'localize',
                'copy:stylusScripts',
                'copy:jadeDev',
                'mkdir:dev',
            ],
            dev2: [
                'replace:dev',
                'jade:compile',
            ],
            dev3: [
                'clean:devStylusMain',
                'browserify:dev',
                'jade:all',
            ],
            nodemon: {
                options: {
                    logConcurrentOutput: true
                },
                tasks: [
                    'nodemon:dev',
                    'node-inspector:custom',
                    'watch',
                ]
            },
            mobile: {
                options: {
                    logConcurrentOutput: true
                },
                tasks: [
                    'nodemon:dev',
                    'node-inspector:custom',
                    'ionic',
                ]
            },
            // clone of 'mobile' but without building initiall
            md: {
                options: {
                    logConcurrentOutput: true
                },
                tasks: [
                    'nodemon:dev',
                    'node-inspector:custom',
                    'ionicWatch',
                ]
            },
            preDist: [
                'clean:dist',
                'copy:stylusScripts',
                'copy:jadeDev',
                'localize',
                'uglify:bowerDist',
            ],
            dist: [
                'ngtemplates:dist',
                'imagemin',
                'copy:dist',
                'jade:all',
            ],
            dist2: [
                'browserify:dist',
                'replace:dist',
                'symlink:commonFront',
            ],
            dist3: [
                'stylus:dist',
                'jadeUsemin:dist',
                'symlink:common',
            ],
            uglify: [
                'uglify:distNoMangle',
            ],
            dist5: [
                'replace:dist2',
            ],
        },

        concat: {
            dist: {
                files: _.reduce(supportedLocales.defaultMap, function (obj, val, prefix) {
                    obj['<%= config.publicDist %>/scripts/main.' + prefix + '.min.js'] =[
                        '<%= config.distTemp %>/scripts/scripts.min.js',
                        '<%= config.distTemp %>/scripts/' + prefix + '.app.min.js',
                        '<%= config.distTemp %>/scripts/main.min.js'
                    ];

                    return obj;
                }, {})
            }
        },

        // Copies remaining files to places other tasks can use
        copy: {
            dist: {
                files: [{
                    expand: true,                       // Favicon
                    dot: true,
                    cwd: '<%= config.app %>',
                    dest: '<%= config.publicDist %>',
                    src: [
                        'favicon.ico',
                        'robots.txt',
                    ]
                }, {
                    expand: true,                       // Bower
                    dot: true,
                    cwd: '<%= config.components %>',
                    dest: '<%= config.publicDist %>/bower_components',
                    src: [
                        '**/*'
                    ]
                }, {
                    expand: true,                       // Fonts + assets
                    dot: true,
                    cwd: '<%= config.assets %>',
                    dest: '<%= config.publicDist %>',
                    src: [
                        'fonts/**/*',
                    ]
                }, {
                    expand: true,                       // Jade Templates
                    dot: true,
                    cwd: '<%= config.app %>/views',
                    dest: '<%= config.dist %>/views',
                    src: [
                        '**/*.jade',
                    ]
                }, {
                    expand: true,                       // CSS Stylus files
                    dot: true,
                    cwd: '<%= config.app %>/styles',
                    dest: '<%= config.publicDist %>/styles',
                    src: [
                        '{,*/}*.{styl,css}',
                        '**/{,*/}*.{styl,css}',
                    ]
                }, {
                    expand: true,                       // Common (shared) code
                    dest: '<%= config.dist %>',
                    src: [
                        'common/**/*',
                        '!common/front',
                    ]
                }, {
                    expand: true,                       // Backend Node files + package.json
                    dest: '<%= config.dist %>',
                    src: [
                        'package.json',
                        'app.js',
                        'lib/**/*',
                    ]
                }, {
                    expand: true,                       // scripts + modules
                    dot: true,
                    cwd: '<%= config.app %>',
                    dest: '<%= config.distTemp %>',
                    src: [
                        'modules/*.js',
                        'scripts/app.*',
                        'scripts/*/**/*.{js,json}',
                    ]
                }]
            },

            distLib: {
                files: [{
                    expand: true,
                    dest: '<%= config.dist %>',
                    src: [
                        'app.js',
                        'lib/**/*',
                    ]
                }]
            },

            // For dev copying .jade files in /script to /views
            jadeDev: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.scripts %>',
                    dest: (targetenv == 'production' ? '<%= config.templates %>' : '<%= config.viewsDev %>'),
                    src: [
                        '**/*.jade',
                        '!index.jade',
                    ]
                }]
            },

            stylus: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.styles %>',
                    dest: '<%= config.publicDev %>/styles/',
                    src: [
                        '**/*.{styl,css}',
                    ]
                }, {
                    src: '<%= config.styles %>/main.styl',
                    dest: '<%= config.publicDev %>/styles/main.styl',
                }]
            },

            stylusScripts: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.scripts %>',
                    dest: '<%= config.styles %>',
                    src: [
                        '**/*.{styl,css}',
                    ]
                }, {
                    expand: true,
                    dot: true,
                    cwd: '<%= config.components %>/MProgress/src/css',
                    dest: '<%= config.styles %>/_plugins',
                    src: '*.styl',
                }]
            },

            distStylus: {
                files: {
                    '<%= config.publicDist %>/styles/main.styl': '<%= config.app %>/styles/main.styl'
                }
            },

            document: {
                files: [{
                    expand: true,                       // CSS Stylus files
                    dot: true,
                    cwd: '<%= config.scripts %>',
                    dest: 'doc/source/front',
                    src: [
                        '**/*.js',
                    ]
                },{
                    expand: true,                       // CSS Stylus files
                    dot: true,
                    cwd: './',
                    dest: 'doc/source/back',
                    src: [
                        'app.js',
                        'lib/**/*.js',
                    ]
                }]
            }
        },

        hashres: {
            options: {
                fileNameFormat: '${name}.${hash}.${ext}',
                renameFiles: true
            },
            dist: {
                dest: ['<%= config.dist %>/views/index.jade'],
                src: [
                    '<%= config.publicDist %>/styles/main.css',
                    '<%= config.publicDist %>/scripts/preload.min.js',

                ]
                //.concat(_.map(supportedLocales.defaultMap, function (val, prefix) {
                //    return '<%= config.publicDist %>/scripts/main.min.js',
                //
                //    return {
                //        src: '<%= config.publicDist %>/scripts/main.' + prefix + '.min.js',
                //        dest: '<%= config.publicDist %>/scripts/main.' + prefix + '.min<%= config.cacheBuster %>.js',
                //    }
                //}))
            },
        },

        // The following *-min tasks produce minified files in the dist folder
        imagemin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= config.app %>/assets/images',
                    src: '{,*/}*.{png,jpg,jpeg,gif}',
                    dest: '<%= config.publicDist %>/images'
                }]
            }
        },

        jade: {
            compile: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: (targetenv == 'production' ? '<%= config.templates %>' : '<%= config.viewsDev %>'),
                    dest: (targetenv == 'production' ? '<%= config.distTemp %>/views' : '<%= config.viewsDev %>'),
                    ext: '.html',
                    src: [
                        'activities/**/*.jade',
                        'account/auth/*.jade',
                        'account/settings/*.jade',
                        'account/directives/**/*.jade',
                        'dashboard/**/*.jade',
                        'event/**/*.jade',
                        'people/**/*.jade',
                        'landing/**/*.jade',
                        'map/**/*.jade',
                        'notifications/**/*.jade',
                        'structure/**/*.jade',
                        'profile/**/*.jade',
                        'misc/footerPopup/*.jade',
                        'misc/loader/*.jade',
                        'misc/autocomplete/**/*.jade',
                        'misc/form/googleLocation/*.jade',
                        'misc/form/timePicker/*.jade',
                        'structure/layouts/ddLayout1/*.jade',
                        'modals/**/*.jade',
                        'standalone/**/*.jade',
                        'interaction/**/*.jade',
                        'services/Venues/**/*.jade',
                        '**/View.jade',

                        '!map/MapLayout.jade',
                        '!standalone/email-preferences.jade',
                        '!standalone/oauthPopupCallback.jade',
                        '!standalone/popupClose.jade',
                        '!standalone/underConstruction.jade',
                        '!standalone/pages/privacyPolicyTemplate.jade',
                        '!standalone/pages/termsServiceTemplate.jade',
                        '!standalone/pages/aboutTemplate.jade',
                        '!standalone/eventIdeas/Template.jade',
                        '!standalone/404/Template.jade',
                    ]
                }]
            },
            all: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: (targetenv == 'production' ? '<%= config.templates %>' : '<%= config.viewsDev %>'),
                    dest: (targetenv == 'production' ? '<%= config.distTemp %>/views' : '<%= config.viewsDev %>'),
                    ext: '.html',
                    src: [
                        '**/*.jade',
                        '!index.jade',
                        '!standalone/email-preferences.jade',
                        '!standalone/oauthPopupCallback.jade',
                    ]
                }]
            }
        },

        // Performs rewrites based on rev and the useminPrepare configuration
        jadeUsemin: {
            dist: {
                options: {
                    prefix: 'dist/public/',
                    tasks: {
                        js: ['concat']
                    }
                },
                files: {
                    '<%= config.dist %>/views/index.jade': '<%= config.dist %>/views/index.jade'
                }
            }
        },

        jsdoc: {
            dev: {
                src: [
                    'doc/source/**/*.js',
                ],
                jsdoc: '/usr/local/bin/jsdoc',
                options: {
                    destination: 'doc',
                    template : "node_modules/ink-docstrap/template",
                    configure : "jsdoc.conf.json",
                },
            },
        },

        localize: {
            options: {
                defaultLocale: supportedLocales.list[0],
                basePath: '<%= config.assets %>/languages',
                fileExtension: '.lang.json',
                locales: supportedLocales.list.slice(1),
            },
        },

        mkdir: {
            dev: {
                options: {
                    create: ['<%= config.publicDev %>', 'tmp']
                }
            },
            dist: {
                options: {
                    create: ['<%= config.dist %>/tmp']
                }
            }
        },

        // Allow the use of non-minsafe AngularJS files. Automatically makes it
        // minsafe compatible so Uglify does not destroy the ng references
        ngAnnotate: {
            options: {
                singleQuotes: true
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= config.publicDist %>/scripts',
                    src: ['{,*/}*.js',],
                    dest: '<%= config.publicDist %>/scripts',
                    replace: true
                }]
            }
        },

        ngtemplates: {
            dev: {
                cwd: '<%= config.viewsDev %>',
                src: ['**/*.html'],
                dest: '<%= config.scripts %>/chatterplot.templates.js',
                options: {
                    module: 'chatterplot.templates',
                    prefix: 'views/',
                    standalone: true,
                    htmlmin: {
                        removeRedundantAttributes:      true,
                    }
                }
            },
            dist: {
                cwd: '<%= config.distTemp %>/views',
                src: ['**/*.html'],
                dest: '<%= config.scripts %>/chatterplot.templates.js',
                options: {
                    module: 'chatterplot.templates',
                    prefix: 'views/',
                    standalone: true,
                    htmlmin: {
                        collapseBooleanAttributes:      true,
                        collapseWhitespace:             true,
                        removeAttributeQuotes:          true,
                        removeComments:                 true, // Only if you don't use comment directives!
                        removeEmptyAttributes:          true,
                        removeScriptTypeAttributes:     true,
                        removeRedundantAttributes:      true,
                        removeStyleLinkTypeAttributes:  true,
                    }
                }
            }
        },

        nodemon: {
            dev: {
                script: 'app.js',
                options: {
                    args: ['development'],
                    nodeArgs: ['--debug'],
                    env: {
                        PORT: 9000,
                        NODE_ENV: 'development',
                        PREFIX: 'dev',
                    },
                    // omit this property if you aren't serving HTML files and
                    // don't want to open a browser tab on start
                    callback: function (nodemon) {
                        nodemon.on('log', function (event) {
                            console.log(event.colour);
                        });

                        // refreshes browser when server reboots
                        nodemon.on('restart', function () {
                            // Delay before server listens on port
                            setTimeout(function() {
                                require('fs').writeFileSync('.rebooted', 'rebooted');
                            }, 5000);
                        });
                    },
                    cwd: __dirname,
                    ext: 'js, styl',
                    watch: ['./app.js', 'lib', 'common/type', 'common/util'],
                    delayTime: 1,
                    ignore: nodemonIgnoredFiles
                },
                exec: {
                    options: {
                        exec: 'less'
                    }
                }
            },

            staging: {
                script: 'lib/worker.js',
                options: {
                    args: ['staging'],
                    nodeArgs: ['--debug'],
                    env: {
                        HTTP_PORT: 9000,
                        HTTPS_PORT: 9443,
                        NODE_ENV: 'staging',
                        PROTOCOL: 'https',
                        PREFIX: 'staging',
                        SITENAME: 'chatterplot',
                        ENV_TYPE: 'non_cluster',
                    },
                    cwd: __dirname,
                    ext: 'js, styl',
                    watch: ['./app.js', 'lib'],
                    delayTime: 1,
                    ignore: nodemonIgnoredFiles
                },
                exec: {
                    options: {
                        exec: 'less'
                    }
                }
            },

            distTest: {
                script: 'app.js',
                options: {
                    args: ['production'],
                    nodeArgs: [],
                    env: {
                        HTTP_PORT: 9000,
                        HTTPS_PORT: 9443,
                        NODE_ENV: 'production',
                        PREFIX: 'dev',
                        SITENAME: 'chatterplot',
                        ENV_TYPE: 'cluster',
                    },
                    cwd: __dirname + '/dist',
                    ext: 'js, styl',
                    watch: ['dist/app.js', 'dist'],
                    delayTime: 1,
                    ignore: nodemonIgnoredFiles
                },
                exec: {
                    options: {
                        exec: 'less'
                    }
                }
            },
        },

        'node-inspector': {
            custom: {
                'web-port': 8080,
                'web-host': '127.0.0.1',
                'debug-port': 5859,
                'save-live-edit': false,
                'no-preload': true,
                'stack-trace-limit': 2,
                'hidden': [
                    'node_modules',
                    'public',
                    'dist',
                    'app',
                    'common/front',
                    'ionic',
                    'test',
                    'tmp',
                ]
            },
        },

        replace: {
            options: {
                patterns: [{
                    json: envConfig
                }]
            },
            dev: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: ['<%= config.scripts %>/index.jade'],
                        dest: '<%= config.viewsDev %>'
                    }
                ]
            },
            dist: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: ['<%= config.scripts %>/index.jade'],
                        dest: '<%= config.dist %>/views'
                    }
                ]
            },
            dist2: {
                options: {
                    patterns: [{
                        json: {
                            "jsHashVar": grunt.option(jsHashVar),
                        }
                    }]
                },
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: '<%= config.dist %>/views/index.jade',
                        dest: '<%= config.dist %>/views'
                    }
                ]
            },
        },

        setHashVar: {
            dist: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.publicDist %>/fonts/icomoon',
                    dest: '<%= config.publicDist %>/fonts/icomoon',
                    hashVar: iconHashVar,                     // <!--- Important: needed
                    src: [
                        '*.{svg,eot,ttf,woff}'
                    ]
                }]
            },
            dist2: {
                expand: true,
                dot: true,
                cwd: '<%= config.publicDist %>/scripts',
                dest: '<%= config.publicDist %>/scripts',
                hashVar: jsHashVar,                     // <!--- Important: needed
                src: [
                    'main.*.min.js',
                ]
            }
        },

        stylus: {
            options: {
                paths: [
                    '<%= config.assets %>/styles',
                    '<%= config.assets %>/images',
                    '<%= config.components %>',
                ],
                urlfunc: 'embedurl', // use embedurl('test.png') in our code to trigger Data URI embedding
                use: [
                    require('nib'), // use stylus plugin at compile time
                    require('rupture'),
                ],
                define: {
                    "iconHashres": grunt.option(iconHashVar) || '', // corresponds to `setHashVars`
                    "cdnBase": envConfig.cdnUrl,
                    "imageSizes": require('./app/scripts/config/imageSizes.json'),
                },
                compress: true,
                rawDefine: true,
                "include css": true,
            },
            dev: {
                files: {
                    '<%= config.publicDev %>/styles/main.css': '<%= config.publicDev %>/styles/main.styl',
                }
            },
            dist: {
                files: {
                    '<%= config.publicDist %>/styles/main.css': '<%= config.styles %>/main.styl',
                }
            },
        },

        svgmin: {                       // Task
            options: {                  // Configuration that will be passed directly to SVGO
                plugins: [{
                    removeViewBox: false
                }, {
                    removeUselessStrokeAndFill: false
                }, {
                    convertPathData: {
                        straightCurves: false // advanced SVGO plugin option
                    }
                }]
            },
            dist: {                     // Target
                files: [{               // Dictionary of files
                    expand: true,       // Enable dynamic expansion.
                    cwd: '<%= config.assets %>/images',     // Src matches are relative to this path.
                    src: ['{,*/}*.svg'],  // Actual pattern(s) to match.
                    dest: '<%= config.publicDist %>/images',       // Destination path prefix.
                    ext: '.svg'     // Dest filepaths will have this extension.
                }]
            }
        },

        symlink: {
            dev: {
                files: [{
                    src: '<%= config.styles %>',
                    dest: '<%= config.publicDev %>/styles'
                }, {
                    src: '<%= config.scripts %>',
                    dest: '<%= config.publicDev %>/scripts'
                }, {
                    src: '<%= config.components %>',
                    dest: '<%= config.publicDev %>/bower_components'
                }, {
                    src: '<%= config.assets %>/fonts',
                    dest: '<%= config.publicDev %>/fonts'
                }, {
                    src: '<%= config.app %>/favicon.ico',
                    dest: '<%= config.publicDev %>/favicon.ico'
                }]
            },
            common: { // both dev/dist depending on --targetenv
                files: [{
                    src: '<%= config.assets %>/images',
                    dest: '/images'
                }, {
                    src: '<%= config.assets %>/languages',
                    dest: '/languages'
                }, {
                    src: '<%= config.assets %>/map',
                    dest: '/map'
                }, {
                    src: '<%= config.assets %>/misc',
                    dest: '/misc'
                }, {
                    src: '<%= config.modules %>',
                    dest: '/modules'
                }, {
                    src: '<%= config.libraries %>',
                    dest: '/third_party'
                }].map(function (obj) {
                    // prefix each 'dest' with the dev/production folder
                    var folder = targetenv == 'production' ? '<%= config.publicDist %>' : '<%= config.publicDev %>';

                    obj.dest = folder + obj.dest;
                    return obj;
                })
            },
            commonFront: {
                files: [{
                    src: '<%= config.' + (targetenv == 'production' ? 'distCommonFront' : 'commonFront') + ' %>',
                    dest: '<%= config.nodeModules %>/front',
                }, {
                    src: '<%= config.' + (targetenv == 'production' ? 'distCommonFront' : 'commonFront') + ' %>/sections',
                    dest: '<%= config.nodeModules %>/root',
                }, {
                    src: '<%= config.' + (targetenv == 'production' ? 'distCommon' : 'common') + ' %>',
                    dest: '<%= config.nodeModules %>/common',
                }]
            }
        },

        touch: {
            dev: {
                src: ['tmp/restart.txt']
            },
            dist: {
                src: ['<%= config.dist %>/tmp/restart.txt']
            }
        },

        uglify: {
            bowerDist: {
                files: {
                    '<%= config.components %>/angular-modal-modified/modal.min.js': '<%= config.components %>/angular-modal-modified/modal.js',
                    '<%= config.components %>/ng-jcrop/ng-jcrop.min.js': '<%= config.components %>/ng-jcrop/ng-jcrop.js',
                }
            },
            distNoMangle: {
                options: {
                    mangle: false,
                },
                files: _.reduce(supportedLocales.defaultMap, function (obj, val, prefix) {
                    obj['<%= config.distTemp %>/scripts/' + prefix + '.app.min.js'] =
                        '<%= config.distTemp %>/scripts/' + prefix + '.app.js';

                    return obj;
                }, {
                    '<%= config.distTemp %>/scripts/main.min.js': '<%= config.distTemp %>/scripts/main.js'
                })
            }
        },

        watch: {
            options: {
                spawn: false,
            },
            i18n: {
                files: [
                    '<%= config.assets %>/languages/en-US/*.lang.json'
                ],
                tasks: [
                    'localize'
                ],
                options: {
                    spawn: false,
                }
            },
            js: {
                files: [
                    '<%= config.scripts %>/app.js',
                    '<%= config.scripts %>/*/**/*.js',
                    '<%= config.modules %>/*.js',
                    '<%= config.commonFront %>/modules/*.js',
                    '<%= config.commonFront %>/util/*.js',
                    '<%= config.commonFront %>/sections/**/*.js',
                    '<%= config.commonFront %>/bower_components/ion-autocomplete-modified/dist/ion-autocomplete.js',
                ],
                tasks: [
                    'browserify:dev'
                ],
                options: {
                    spawn: false
                }
            },
            server: {
                files: ['.rebooted'],
                options: {
                    spawn: false,
                }

            },
            scriptsJade: {
                files: [
                    '<%= config.scripts %>/**/*.jade',
                    '!<%= config.scripts %>/index.jade',
                ],
                tasks: [
                    'newer:copy:jadeDev',
                    'jade:compile',
                    'ngtemplates:dev',
                    'browserify:dev',
                ],
            },
            indexJade: {
                files: '<%= config.scripts %>/index.jade',
                tasks: ['replace:dev'],
                options: {
                    spawn: false,
                }
            },
            stylusScripts: {
                files: [
                    '<%= config.scripts %>/**/*.styl',
                ],
                tasks: [
                    'newer:copy:stylusScripts',
                    'clean:devStylusMain',
                    'stylus:dev',
                ],
                options: {
                    spawn: false,
                }
            },
            stylus: {
                files: [
                    '<%= config.assets %>/styles/*.styl',
                    '<%= config.styles %>/*.styl',
                    '<%= config.styles %>/elements/*.styl',
                    '<%= config.styles %>/modules/**/*.styl',
                    '<%= config.styles %>/_plugins/*.styl',
                ],
                tasks: [
                    'clean:devStylusMain',
                    'stylus:dev',
                ],
                options: {
                    spawn: false,
                }
            }
        },
    });

    require('./tasks/setHashVar.js')(grunt);

    grunt.registerTask('devBase', [
        'clean:dev',
        'concurrent:dev1',
        'symlink:commonFront',
        'symlink:common',
        'symlink:dev',
        'concurrent:dev2',
        'ngtemplates:dev',
        'concurrent:dev3',
        'stylus:dev',
    ]);

    grunt.registerTask('dev', [
        'devBase',
        'concurrent:nodemon',
    ]);

    grunt.registerTask('d', [
        'concurrent:nodemon',
    ]);

    grunt.registerTask('staging', [
        'devBase',
        'ionicStaging',
        'touch:dev',
    ]);

    grunt.registerTask('buildBase', [
        'concurrent:preDist',
        'jade:compile',
        'concurrent:dist',
        'concurrent:dist2',
        'setHashVar:dist',
        'concurrent:dist3',
        'concurrent:uglify',
        'concat:dist',
        'setHashVar:dist2',
        'concurrent:dist5',
        'hashres',
        'copy:distStylus',
    ]);

    grunt.registerTask('build', [
        'buildBase',
        'mkdir:dist',
        'touch:dist',
    ]);

    grunt.registerTask('mbuild', [
        'buildBase',
        'ionicDist',
        'mkdir:dist',
        'touch:dist',
    ]);

    grunt.registerTask('document', [
        'clean:document',
        'copy:document',
        'clean:docBundle',
        'jsdoc',
    ]);

    grunt.registerTask('mdev', [
        'devBase',
        'concurrent:mobile',
    ]);

    grunt.registerTask('md', [
        'concurrent:md',
    ]);

    grunt.registerTask('ionic', function (target) {
        target = target || 'dev';

        var targetBuild = 'development';

        if (target === 'build') {
            targetBuild = 'production';
        } else if (target === 'staging') {
            targetBuild = 'staging'
        }

        console.log('ionic calling:', target);

        var child = grunt.util.spawn({
            grunt: true,
            args: [target, '--targetenv=' + targetBuild],
            opts: {
                cwd: 'ionic',
            }
        }, this.async());

        child.stdout.pipe(process.stdout);
        child.stderr.pipe(process.stderr);
    });

    grunt.registerTask('ionicWatch', function () {
        grunt.task.run('ionic:watch');
    });

    grunt.registerTask('ionicStaging', function () {
        grunt.task.run('ionic:staging');
    });

    grunt.registerTask('ionicDist', function () {
        grunt.task.run('ionic:build');
    });
};
