'use strict';

var env                 = process.env.NODE_ENV || 'development'
    , path              = require('path')
    , libDir            = path.resolve(process.cwd(), 'lib')
    , cluster           = require('cluster')
    , Fetch             = require(path.join(libDir, 'Fetcher')) // Define the global Fetcher
    , logger            = require('bragi')
    ;

if (cluster.isMaster) {
    logger.log('init:server:master', 'init all')
    cluster.setupMaster({
        exec: path.join(libDir, 'worker.js'),
        //args: ['--use', 'https']
    });

    // set it just in case it's empty
    process.env.NODE_ENV = env;

    var numProcesses    = require('os').cpus().length,

        // This stores our workers. We need to keep them to be able to reference
        // them based on source IP address. It's also useful for auto-restart,
        // for example.
        workers         = [],

        // Application Config
        config          = Fetch.config('config', true, 'js'),

        // Helper function for spawning worker at index 'i'.
        spawn = function(i) {
            workers[i] = cluster.fork();

            // Optional: Restart worker on exit
            workers[i].on('exit', function(worker, code, signal) {
                console.log('respawning worker', i);
                spawn(i);
            });
        };

    // Spawn workers.
    let i = 0;
    for (; i < numProcesses; i++) {
        spawn(i);
    }

    // Helper function for getting a worker index based on IP address.
    // This is a hot path so it should be really fast. The way it works
    // is by converting the IP address to a number by removing the dots,
    // then compressing it to the number of slots we have.
    //
    // Compared against "real" hashing (from the sticky-session code) and
    // "real" IP number conversion, this function is on par in terms of
    // worker index distribution only much faster.
    var worker_index = function(ip, len) {
            var s = '';
            for (var i = 0, _len = ip.length; i < _len; i++) {
                if (ip[i] !== '.' && ip[i] !== ':' && ip[i] !== 'f') {
                    s += ip[i];
                }
            }
            return Number(s) % len;
        },

        net     = require('net');
    //    fs      = require('fs'),
    //    HOME    = process.env.HOME,
    //    files   = [
    //        'chatterplot-DomainValidationSecureServerCA.crt',
    //        'chatterplot-AddTrustCA.crt',
    //        'chatterplot-AddTrustExternalCARoot.crt'
    //    ],
    //
    //    sslOptions = {
    //        key:  fs.readFileSync(path.join(HOME, 'ssl', 'chatterplot.key')),
    //        cert: fs.readFileSync(path.join(HOME, 'ssl', 'chatterplot.crt')),
    //        ciphers: [
    //            'ECDHE-RSA-AES128-GCM-SHA256',
    //            'ECDHE-ECDSA-AES128-GCM-SHA256',
    //            'ECDHE-RSA-AES256-GCM-SHA384',
    //            'ECDHE-ECDSA-AES256-GCM-SHA384',
    //            'DHE-RSA-AES128-GCM-SHA256',
    //            'ECDHE-RSA-AES128-SHA256',
    //            'DHE-RSA-AES128-SHA256',
    //            'ECDHE-RSA-AES256-SHA384',
    //            'DHE-RSA-AES256-SHA384',
    //            'ECDHE-RSA-AES256-SHA256',
    //            'DHE-RSA-AES256-SHA256',
    //            'HIGH',
    //            '!aNULL',
    //            '!eNULL',
    //            '!EXPORT',
    //            '!DES',
    //            '!RC4',
    //            '!MD5',
    //            '!PSK',
    //            '!SRP',
    //            '!CAMELLIA'
    //        ].join(':'),
    //    };
    //
    //// If production attach additional trust .crt's
    //if (env === 'production') {
    //    sslOptions.ca = (function() {
    //        var _i, _len, _results;
    //
    //        _results = [];
    //        for (_i = 0, _len = files.length; _i < _len; _i++) {
    //            var file = files[_i];
    //            _results.push(fs.readFileSync(path.join(HOME, 'ssl',  file)));
    //        }
    //        return _results;
    //    })();
    //}

    // Finally create the server that listens
    logger.log('server:master', 'Creating server to accept calls');
    net.createServer({}, function(connection) {
        // We received a connection and need to pass it to the appropriate
        // worker. Get the worker for this connection's source IP and pass
        // it the connection.
        var worker = workers[worker_index(connection.remoteAddress, numProcesses)];

        worker.send('sticky-session:connection', connection);
    }).listen(config.httpsPort, function () {
        console.log('Secure Express server listening on port %d', config.httpsPort)
    });

    // Load Cron
    setTimeout(function () {
        Fetch.controller('cron');
    }, 0);
} else {
    // Only needed if called by `slc run --cluster=cpus`
    require(path.join(libDir, 'worker'))
}
