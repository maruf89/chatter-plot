# ************************************************************
# Sequel Pro SQL dump
# Version 4135
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 10.0.12-MariaDB)
# Database: users
# Generation Time: 2015-01-26 14:01:53 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table facebook
# ------------------------------------------------------------

DROP TABLE IF EXISTS `facebook`;

CREATE TABLE `facebook` (
  `fb_id` bigint(20) NOT NULL,
  `access_token` varchar(255) NOT NULL,
  UNIQUE KEY `fbId` (`fb_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table google
# ------------------------------------------------------------

DROP TABLE IF EXISTS `google`;

CREATE TABLE `google` (
  `gplus_id` char(21) NOT NULL,
  `access_token` varchar(350) NOT NULL,
  `refresh_token` varchar(100) NOT NULL,
  PRIMARY KEY (`gplus_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table linkedin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `linkedin`;

CREATE TABLE `linkedin` (
  `linkedin_id` varchar(15) NOT NULL,
  `access_token` varchar(350) NOT NULL,
  PRIMARY KEY (`linkedin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table user_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_groups`;

CREATE TABLE `user_groups` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(15) NOT NULL DEFAULT '',
  `roles` varchar(300) DEFAULT NULL,
  `on_activate` varchar(15) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user_groups` WRITE;
/*!40000 ALTER TABLE `user_groups` DISABLE KEYS */;

INSERT INTO `user_groups` (`id`, `name`, `roles`, `on_activate`)
VALUES
	(1,'admin','USER_EDIT,USER_DELETE,USER_ADD,USER_VIEW,EDIT_PERMISSIONS,SELF_VIEW,SELF_EDIT',NULL),
	(2,'basic','SELF_VIEW,SELF_EDIT',NULL),
	(3,'non_activated','SELF_VIEW,SELF_EDIT','basic'),
	(4,'pre_admin','SELF_VIEW,SELF_EDIT','admin'),
	(5,'host','CREATE_EVENT,PUBLISH_EVENT','');

/*!40000 ALTER TABLE `user_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_permissions`;

CREATE TABLE `user_permissions` (
  `bit` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `user_permissions` WRITE;
/*!40000 ALTER TABLE `user_permissions` DISABLE KEYS */;

INSERT INTO `user_permissions` (`bit`, `name`)
VALUES
	(1,'USER_EDIT'),
	(2,'USER_DELETE'),
	(4,'USER_ADD'),
	(8,'USER_VIEW'),
	(16,'EDIT_PERMISSIONS'),
	(32,'SELF_VIEW'),
	(64,'SELF_EDIT'),
	(128,'CREATE_EVENT'),
	(256,'PUBLISH_EVENT');

/*!40000 ALTER TABLE `user_permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(38) NOT NULL,
  `password` char(32) NOT NULL DEFAULT '',
  `role` int(11) NOT NULL DEFAULT '0',
  `active` bigint(20) DEFAULT '1',
  `fb_id` bigint(20) DEFAULT NULL,
  `gplus_id` char(21) NOT NULL DEFAULT '',
  `linkedin_id` varchar(15) NOT NULL DEFAULT '',
  `mc_euid` varchar(10) DEFAULT NULL COMMENT 'mailchimp: the unique id for an email address (not list related) - the email "id" returned from listMemberInfo, Webhooks, Campaigns, etc.',
  `mc_leid` varchar(10) DEFAULT NULL COMMENT 'mailchimp: the list email id (previously called web_id) for a list-member-info type call. this doesn''t change when the email address changes',
  `only_mail` tinyint(1) NOT NULL DEFAULT '0',
  `activated` tinyint(4) DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `fb_id` (`fb_id`,`gplus_id`,`linkedin_id`),
  KEY `active` (`active`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
