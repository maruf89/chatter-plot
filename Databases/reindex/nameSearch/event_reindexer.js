'use strict';

module.exports = {
    query: {
        fields: ['_source', '_parent']
    },
    index: function (item, opts) {
        var object = {
                index: {
                    _index: opts.index,
                    _id: item._id
                }
            },
            source = item._source;

        object.index._type = source.hasOwnProperty('aboutHost') ? 'event' : 'listing';

        return [
            object,
            source
        ];
    }
};
