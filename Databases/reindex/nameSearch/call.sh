echo "Updating users"

curl -XPUT localhost:9200/users_v10 -d '{
    "aliases": {
        "users": {}
    },

    "mappings": {
        "user": {
            "_routing": {
                "path": "userID"
            },
            "_id": {
                "path": "userID"
            },
            "properties": {
                "userID": {
                    "type": "integer"
                },
                "unreadNotification": {
                    "type": "boolean"
                },
                "userGroup": {
                    "type": "string"
                },
                "lastName" : {
                    "fields" : {
                        "metaphone" : {
                            "type" : "string",
                            "analyzer" : "name_metaphone"
                        },
                        "partial" : {
                            "search_analyzer" : "full_name",
                            "index_analyzer" : "partial_name",
                            "type" : "string"
                        },
                        "lastName" : {
                            "type" : "string",
                            "analyzer" : "full_name"
                        }
                    },
                    "type" : "multi_field"
                },
                "firstName" : {
                    "fields" : {
                        "metaphone" : {
                            "type" : "string",
                            "analyzer" : "name_metaphone"
                        },
                        "partial" : {
                            "search_analyzer" : "full_name",
                            "index_analyzer" : "partial_name",
                            "type" : "string"
                        },
                        "firstName" : {
                            "type" : "string",
                            "analyzer" : "full_name"
                        }
                    },
                    "type" : "multi_field"
                },
                "birthday": {
                    "type": "date",
                    "index": "not_analyzed",
                    "format": "date"
                },
                "joinDate": {
                    "type": "date",
                    "format": "basic_date_time"
                },
                "availabilityText": {
                    "type": "string",
                    "index": "no"
                },
                "picture": {
                    "type": "object",
                    "properties": {
                        "default": {
                            "type": "string",
                            "index": "no"
                        },
                        "thumb": {
                            "type": "string",
                            "index": "no"
                        },
                        "hasPhoto": {
                            "type": "boolean"
                        },
                        "service": {
                            "type": "string"
                        },
                        "id": {
                            "type": "string"
                        }
                    }
                },
                "email": {
                    "type": "string",
                    "index": "not_analyzed"
                },
                "favorite": {
                    "type": "nested"
                },
                "languages": {
                    "type": "nested",
                    "properties": {
                        "languageID": {
                            "type": "integer"
                        },
                        "level": {
                            "type": "integer"
                        },
                        "learning": {
                            "type": "boolean"
                        },
                        "teaching": {
                            "type": "boolean"
                        },
                        "native": {
                            "type": "boolean"
                        }
                    }
                },
                "aboutContact": {
                    "type": "string"
                },
                "coords": {
                    "type": "geo_point"
                },
                "address": {
                    "type": "string",
                    "index": "no"
                },
                "postalCode": {
                    "type": "string",
                    "index": "no"
                },
                "sublocality": {
                    "type": "string"
                },
                "city": {
                    "type": "string"
                },
                "state": {
                    "type": "string"
                },
                "country": {
                    "type": "string"
                },
                "locations": {
                    "type": "integer",
                    "index": "no"
                },
                "personal": {
                    "type": "string",
                    "index": "no"
                },
                "interests": {
                    "type": "object",
                    "properties": {
                        "question": {
                            "type": "string"
                        },
                        "answer": {
                            "type": "string",
                            "analyzer": "keyword"
                        }
                    }
                },
                "details": {
                    "type": "object",
                    "properties": {
                        "gender": {
                            "type": "string",
                            "analyzer": "keyword"
                        },
                        "hometown": {
                            "type": "string",
                            "index": "no"
                        },
                        "occupation": {
                            "type": "string",
                            "analyzer": "keyword"
                        }
                    }
                },
                "accounts": {
                    "type": "nested",
                    "properties": {
                        "type": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "id": {
                            "type": "string",
                            "index": "not_analyzed"
                        }
                    }
                },
                "attending": {
                    "type": "nested",
                    "properties": {
                        "events": {
                            "type": "integer"
                        },
                        "eventsWaitList": {
                            "type": "integer"
                        }
                    }
                },
                "settings": {
                    "type": "object",
                    "properties": {
                        "unit": {
                            "type": "integer"
                        },
                        "formats": {
                            "type": "object",
                            "properties": {
                                "date": {
                                    "type": "integer"
                                },
                                "time": {
                                    "type": "integer"
                                }
                            }
                        },
                        "locale": {
                            "type": "string"
                        },
                        "notifs": {
                            "type": "object",
                            "properties": {
                                "email": {
                                    "type": "object",
                                    "properties": {
                                        "eventsILearn": {
                                            "type": "boolean"
                                        },
                                        "eventsISpeak": {
                                            "type": "boolean"
                                        },
                                        "eventsAllLang": {
                                            "type": "boolean"
                                        },
                                        "updates": {
                                            "type": "boolean"
                                        },
                                        "newMsg": {
                                            "type": "boolean"
                                        }
                                    }
                                },
                                "messaging": {
                                    "type": "object",
                                    "properties": {
                                        "acceptAll": {
                                            "type": "boolean"
                                        },
                                        "acceptFollowing": {
                                            "type": "boolean"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },

        "notifications": {
            "_parent": {
                "type": "user"
            },
            "_id": {
                "path": "nID"
            },
            "properties": {
                "nID": {
                    "type": "string"
                },
                "source": {
                    "type": "integer"
                },
                "sourceType": {
                    "type": "integer"
                },
                "sourceID": {
                    "type": "string"
                },
                "from": {
                    "type": "integer"
                },
                "when": {
                    "type": "date",
                    "format": "basic_date_time_no_millis"
                },
                "read": {
                    "type": "date",
                    "format": "basic_date_time_no_millis"
                },
                "snippet": {
                    "type": "string",
                    "index": "not_analyzed"
                }
            }
        }
    },
    "settings" : {
        "number_of_shards": 1,
        "number_of_replicas": 1,
        "analysis" : {
            "filter" : {
                "name_ngrams" : {
                    "side" : "front",
                    "max_gram" : 10,
                    "min_gram" : 1,
                    "type" : "edgeNGram"
                },
                "name_metaphone" : {
                    "replace" : false,
                    "encoder" : "metaphone",
                    "type" : "phonetic"
                }
            },
            "analyzer" : {
                "full_name" : {
                    "filter" : [
                        "standard",
                        "lowercase",
                        "asciifolding"
                    ],
                    "type" : "custom",
                    "tokenizer" : "standard"
                },
                "name_metaphone" : {
                    "filter" : [
                        "name_metaphone"
                    ],
                    "type" : "custom",
                    "tokenizer" : "standard"
                },
                "partial_name" : {
                    "filter" : [
                        "standard",
                        "lowercase",
                        "asciifolding",
                        "name_ngrams"
                    ],
                    "type" : "custom",
                    "tokenizer" : "standard"
                }
            }
        }
    }
}'

echo "Updating events"
curl -XPUT localhost:9200/events_v3 -d '{
    "aliases": {
        "events": {}
    },

    "mappings": {
        "event": {
            "_routing": {
                "path": "eID"
            },
            "_id": {
                "path": "eID"
            },
            "properties": {
                "eID": {
                    "type": "integer"
                },
                "languages": {
                    "type": "integer"
                },
                "when": {
                    "type": "date",
                    "format": "basic_date_time_no_millis"
                },
                "until": {
                    "type": "date",
                    "format": "basic_date_time_no_millis"
                },
                "created": {
                    "type": "date",
                    "format": "basic_date_time_no_millis"
                },
                "lastUpdated": {
                    "type": "date",
                    "format": "basic_date_time_no_millis"
                },
                "creator": {
                    "type": "integer"
                },
                "organizers": {
                    "type": "integer"
                },
                "levels": {
                    "type": "integer"
                },
                "name": {
                    "fields" : {
                        "metaphone" : {
                            "type" : "string",
                            "analyzer" : "name_metaphone"
                        },
                        "partial" : {
                            "search_analyzer" : "full_name",
                            "index_analyzer" : "partial_name",
                            "type" : "string"
                        },
                        "name" : {
                            "type" : "string",
                            "analyzer" : "full_name"
                        }
                    },
                    "type" : "multi_field"
                },
                "hasPhoto": {
                    "type": "boolean"
                },
                "description": {
                    "type": "string",
                    "index": "no"
                },
                "venue": {
                    "type": "nested",
                    "properties": {
                        "vID": {
                            "type": "integer"
                        },
                        "coords": {
                            "type": "geo_point"
                        },
                        "rsvpOnlyAddress": {
                            "type": "integer"
                        }
                    }
                },
                "locationDisplayName": {
                    "type": "string"
                },
                "published": {
                    "type": "boolean"
                },
                "goingList": {
                    "type": "integer"
                },
                "waitList": {
                    "type": "integer"
                },
                "maxGoing": {
                    "type": "integer"
                },
                "aboutHost": {
                    "type": "string",
                    "index": "no"
                },
                "paymentInfo": {
                    "type": "string",
                    "index": "no"
                },
                "priceString": {
                    "type": "string"
                }
            }
        },

        "listing": {
            "_routing": {
                "path": "eID"
            },
            "_id": {
                "path": "eID"
            },
            "properties": {
                "eID": {
                    "type": "integer"
                },
                "languages": {
                    "type": "integer"
                },
                "when": {
                    "type": "date",
                    "format": "basic_date_time_no_millis"
                },
                "until": {
                    "type": "date",
                    "format": "basic_date_time_no_millis"
                },
                "created": {
                    "type": "date",
                    "format": "basic_date_time_no_millis"
                },
                "lastUpdated": {
                    "type": "date",
                    "format": "basic_date_time_no_millis"
                },
                "creator": {
                    "type": "integer"
                },
                "organizers": {
                    "type": "integer"
                },
                "levels": {
                    "type": "integer"
                },
                "name": {
                    "fields" : {
                        "partial" : {
                            "search_analyzer" : "full_name",
                            "index_analyzer" : "partial_name",
                            "type" : "string"
                        },
                        "name" : {
                            "type" : "string",
                            "analyzer" : "full_name"
                        }
                    },
                    "type" : "multi_field"
                },
                "description": {
                    "type": "string",
                    "index": "no"
                },
                "venue": {
                    "type": "nested",
                    "properties": {
                        "vID": {
                            "type": "integer"
                        },
                        "coords": {
                            "type": "geo_point"
                        },
                        "rsvpOnlyRadius": {
                            "type": "integer"
                        }
                    }
                },
                "locationDisplayName": {
                    "type": "string"
                },
                "published": {
                    "type": "boolean"
                },
                "goingList": {
                    "type": "integer"
                },
                "registrationURL": {
                    "type": "string"
                },
                "paymentInfo": {
                    "type": "string",
                    "index": "no"
                },
                "priceString": {
                    "type": "string"
                }
            }
        }
    },

    "settings" : {
        "number_of_shards": 1,
        "number_of_replicas": 1,
        "analysis" : {
            "filter" : {
                "name_ngrams" : {
                    "side" : "front",
                    "max_gram" : 10,
                    "min_gram" : 1,
                    "type" : "edgeNGram"
                },
                "name_metaphone" : {
                    "replace" : false,
                    "encoder" : "metaphone",
                    "type" : "phonetic"
                }
            },
            "analyzer" : {
                "full_name" : {
                    "filter" : [
                        "standard",
                        "lowercase",
                        "asciifolding"
                    ],
                    "type" : "custom",
                    "tokenizer" : "standard"
                },
                "name_metaphone" : {
                    "filter" : [
                        "name_metaphone"
                    ],
                    "type" : "custom",
                    "tokenizer" : "standard"
                },
                "partial_name" : {
                    "filter" : [
                        "standard",
                        "lowercase",
                        "asciifolding",
                        "name_ngrams"
                    ],
                    "type" : "custom",
                    "tokenizer" : "standard"
                }
            }
        }
    }
}'

echo "Reindexing users"
elasticsearch-reindex -f localhost:9200/users -t localhost:9200/users_v10 user_reindexer.js

echo "Reindexing events"
elasticsearch-reindex -f localhost:9200/events_v2 -t localhost:9200/events_v3 event_reindexer.js

curl -XPOST localhost:9200/_aliases -d '{
    "actions": [
        { "remove": {
            "alias": "users",
            "index": "users_v9"
        }},
        { "add": {
            "alias": "users",
            "index": "users_v10"
        }},
        { "remove": {
            "alias": "events",
            "index": "events_v2"
        }},
        { "add": {
            "alias": "events",
            "index": "events_v3"
        }}
    ]
}'

echo Index has been successfully indexed and transformed, double check before running the next command to delete the old index
echo ' '
echo to delete the old index run:
echo curl -XDELETE localhost:9200/users_v9
