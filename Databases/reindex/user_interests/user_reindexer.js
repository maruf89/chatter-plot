'use strict';

module.exports = {
    query: {
        fields: ['_source', '_parent']
    },
    index: function (item) {
        var object = {
            index: {
                _index: item._index,
                _type: item._type,
                _id: item._id
            }
        };

        if (item._type === 'notifications') {
            object.index._parent = item.fields._parent;
        }

        return [
            object,
            item._source
        ];
    }
};
