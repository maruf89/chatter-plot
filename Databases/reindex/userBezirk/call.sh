echo "Updating users"

curl -XPUT localhost:9200/users_v12 -d '{
    "mappings": {
        "user": {
            "_routing": {
                "path": "userID"
            },
            "_id": {
                "path": "userID"
            },
            "properties": {
                "userID": {
                    "type": "integer"
                },
                "unreadNotification": {
                    "type": "boolean"
                },
                "features": {
                    "type": "string",
                    "index": "not_analyzed"
                },
                "userGroup": {
                    "type": "string"
                },
                "lastName" : {
                    "fields" : {
                        "metaphone" : {
                            "type" : "string",
                            "analyzer" : "name_metaphone"
                        },
                        "partial" : {
                            "search_analyzer" : "full_name",
                            "index_analyzer" : "partial_name",
                            "type" : "string"
                        },
                        "lastName" : {
                            "type" : "string",
                            "analyzer" : "full_name"
                        }
                    },
                    "type" : "multi_field"
                },
                "firstName" : {
                    "fields" : {
                        "metaphone" : {
                            "type" : "string",
                            "analyzer" : "name_metaphone"
                        },
                        "partial" : {
                            "search_analyzer" : "full_name",
                            "index_analyzer" : "partial_name",
                            "type" : "string"
                        },
                        "firstName" : {
                            "type" : "string",
                            "analyzer" : "full_name"
                        }
                    },
                    "type" : "multi_field"
                },
                "birthday": {
                    "type": "date",
                    "index": "not_analyzed",
                    "format": "date"
                },
                "joinDate": {
                    "type": "date",
                    "format": "basic_date_time"
                },
                "availabilityText": {
                    "type": "string",
                    "index": "no"
                },
                "picture": {
                    "type": "object",
                    "properties": {
                        "default": {
                            "type": "string",
                            "index": "no"
                        },
                        "thumb": {
                            "type": "string",
                            "index": "no"
                        },
                        "hasPhoto": {
                            "type": "boolean"
                        },
                        "service": {
                            "type": "string"
                        },
                        "id": {
                            "type": "string"
                        }
                    }
                },
                "email": {
                    "type": "string",
                    "index": "not_analyzed"
                },
                "favorite": {
                    "type": "nested"
                },
                "languages": {
                    "type": "nested",
                    "properties": {
                        "languageID": {
                            "type": "integer"
                        },
                        "level": {
                            "type": "integer"
                        },
                        "learning": {
                            "type": "boolean"
                        },
                        "teaching": {
                            "type": "boolean"
                        },
                        "native": {
                            "type": "boolean"
                        }
                    }
                },
                "aboutContact": {
                    "type": "string"
                },
                "locations": {
                    "type": "nested",
                    "properties": {
                        "vID": {
                            "type": "integer"
                        },
                        "type": {
                            "type": "string"
                        },
                        "google": {
                            "type": "string",
                            "index": "no"
                        },
                        "yelp": {
                            "type": "string",
                            "index": "no"
                        },
                        "primary": {
                            "type": "boolean"
                        },
                        "coords": {
                            "type": "geo_point"
                        },
                        "sublocality": {
                            "type": "string"
                        },
                        "city": {
                            "type": "string"
                        },
                        "state": {
                            "type": "string"
                        }
                    }
                },
                "personal": {
                    "type": "string",
                    "index": "no"
                },
                "interests": {
                    "type": "object",
                    "properties": {
                        "question": {
                            "type": "string"
                        },
                        "answer": {
                            "type": "string",
                            "analyzer": "keyword"
                        }
                    }
                },
                "details": {
                    "type": "object",
                    "properties": {
                        "gender": {
                            "type": "string",
                            "analyzer": "keyword"
                        },
                        "hometown": {
                            "type": "string",
                            "index": "no"
                        },
                        "occupation": {
                            "type": "string",
                            "analyzer": "keyword"
                        }
                    }
                },
                "accounts": {
                    "type": "nested",
                    "properties": {
                        "type": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "id": {
                            "type": "string",
                            "index": "not_analyzed"
                        }
                    }
                },
                "attending": {
                    "type": "nested",
                    "properties": {
                        "events": {
                            "type": "integer"
                        },
                        "eventsWaitList": {
                            "type": "integer"
                        },
                        "attended": {
                            "type": "integer"
                        }
                    }
                },
                "settings": {
                    "type": "object",
                    "properties": {
                        "unit": {
                            "type": "integer"
                        },
                        "formats": {
                            "type": "object",
                            "properties": {
                                "date": {
                                    "type": "integer"
                                },
                                "time": {
                                    "type": "integer"
                                }
                            }
                        },
                        "locale": {
                            "type": "string"
                        },
                        "notifs": {
                            "type": "object",
                            "properties": {
                                "email": {
                                    "type": "object",
                                    "properties": {
                                        "eventsILearn": {
                                            "type": "boolean"
                                        },
                                        "eventsISpeak": {
                                            "type": "boolean"
                                        },
                                        "eventsAllLang": {
                                            "type": "boolean"
                                        },
                                        "updates": {
                                            "type": "boolean"
                                        },
                                        "newMsg": {
                                            "type": "boolean"
                                        }
                                    }
                                },
                                "messaging": {
                                    "type": "object",
                                    "properties": {
                                        "acceptAll": {
                                            "type": "boolean"
                                        },
                                        "acceptFollowing": {
                                            "type": "boolean"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },

        "notifications": {
            "_parent": {
                "type": "user"
            },
            "_id": {
                "path": "nID"
            },
            "properties": {
                "nID": {
                    "type": "string"
                },
                "source": {
                    "type": "integer"
                },
                "sourceType": {
                    "type": "integer"
                },
                "sourceID": {
                    "type": "string"
                },
                "from": {
                    "type": "integer"
                },
                "when": {
                    "type": "date",
                    "format": "basic_date_time_no_millis"
                },
                "read": {
                    "type": "date",
                    "format": "basic_date_time_no_millis"
                },
                "snippet": {
                    "type": "string",
                    "index": "not_analyzed"
                }
            }
        }
    },
    "settings" : {
        "number_of_shards": 1,
        "number_of_replicas": 1,
        "analysis" : {
            "filter" : {
                "name_ngrams" : {
                    "side" : "front",
                    "max_gram" : 10,
                    "min_gram" : 1,
                    "type" : "edgeNGram"
                },
                "name_metaphone" : {
                    "replace" : false,
                    "encoder" : "metaphone",
                    "type" : "phonetic"
                }
            },
            "analyzer" : {
                "full_name" : {
                    "filter" : [
                        "standard",
                        "lowercase",
                        "asciifolding"
                    ],
                    "type" : "custom",
                    "tokenizer" : "standard"
                },
                "name_metaphone" : {
                    "filter" : [
                        "name_metaphone"
                    ],
                    "type" : "custom",
                    "tokenizer" : "standard"
                },
                "partial_name" : {
                    "filter" : [
                        "standard",
                        "lowercase",
                        "asciifolding",
                        "name_ngrams"
                    ],
                    "type" : "custom",
                    "tokenizer" : "standard"
                }
            }
        }
    }
}'

echo "Reindexing users"
elasticsearch-reindex -f localhost:9200/users_v11 -t localhost:9200/users_v12 -m true -s 300000 user_reindexer.js

curl -XPOST localhost:9200/_aliases -d '{
    "actions": [
        { "remove": {
            "alias": "users",
            "index": "users_v11"
        }},
        { "add": {
            "alias": "users",
            "index": "users_v12"
        }}
    ]
}'

echo Index has been successfully indexed and transformed, double check before running the next command to delete the old index
echo ' '
echo to delete the old index run:
echo curl -XDELETE localhost:9200/users_v10
