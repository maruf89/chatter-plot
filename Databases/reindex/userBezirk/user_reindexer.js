'use strict';

var Q       = require('q'),
    geocoder = require('geocoder'),
    locUtil = require('../../../lib/util/geocoderPromise.js'),

    geocodeFn = Q.denodeify(geocoder.reverseGeocode.bind(geocoder));

module.exports = {
    query: {
        fields: ['_source', '_parent']
    },
    index: function (item, opts, client) {
        var object = {
                index: {
                    _index: opts.index,
                    _type: item._type,
                    _id: item._id
                }
            },
            source = item._source,
            returnObj = [
                object,
                source
            ],

            returnFn = function () {
                return returnObj;
            },

            locationsKey = 'locations';

        if (item._type === 'notifications') {
            object.index._parent = item.fields._parent;
            source.sourceID = String(source.sourceID);
            source.read = source.read || null;
            return returnObj;
        }

        //// User object

        // Checking if we don't have locations or we already have the desired locations set
        if ((!Array.isArray(source[locationsKey]) || !source[locationsKey].length) ||
            (source[locationsKey][0].sublocality || source[locationsKey][0].city || source[locationsKey][0].state)
        ) {
            return returnFn();
        }

        let primary = source[locationsKey][0];

        return promiseDelay().then(function () {
            console.log('fetching user:', source.userID);
            return geocodeFn(primary.coords.lat, primary.coords.lon).then(function (response) {
                if (response.status === 'OVER_QUERY_LIMIT') {
                    console.log('query limit reached');
                }

                if (!Array.isArray(response.results) || !response.results.length) {
                    return returnFn();
                }

                let location = locUtil.scrapePlace(response.results[0], 'google');

                if (location.sublocality) {
                    primary.sublocality = location.sublocality;
                }

                if (location.city) {
                    primary.city = location.city;
                }

                if (location.state) {
                    primary.state = location.state;
                }

                return returnFn();
            });
        });
    }
};

var counter = 0,
    interval = 1000,
    promiseDelay = function () {
        if (!counter++) {
            return Q.resolve();
        }

        let deferred = Q.defer();

        setTimeout(deferred.resolve, (counter * interval));

        return deferred.promise;
    };
