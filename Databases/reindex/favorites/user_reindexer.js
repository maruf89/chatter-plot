'use strict';

module.exports = {
    query: {
        fields: ['_source', '_parent']
    },
    index: function (item) {
        var object = {
                index: {
                    _index: 'users_v9',
                    _type: item._type,
                    _id: item._id
                }
            },
            source = item._source,
            favorite,
            pictureTest = /^https?:\/\/res\.cloudinary\.com/,
            fbTest = /\/facebook\//,
            picture;

        if (item._type === 'notifications') {
            object.index._parent = item.fields._parent;
            source.sourceID = String(source.sourceID);
            source.read = source.read || null;
        } else {
            favorite = source.favorite = source.favorite || {};
            favorite.all = favorite.all || [];

            if ((picture = source.picture) && picture.default && pictureTest.test(picture.default)) {
                if (fbTest.test(picture.default)) {
                    picture.service = 'facebook';

                    source.accounts.forEach(function (accnt) {
                        if (accnt.type === picture.service) {
                            picture.id = accnt.id;
                        }
                    });
                }

                try {
                    delete picture.default;
                    delete picture.thumb;
                } catch (e) {}

                picture.hasPhoto = true;
            }
        }

        return [
            object,
            source
        ];
    }
};
