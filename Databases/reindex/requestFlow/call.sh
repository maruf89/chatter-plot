echo "Updating users"
curl -XPUT localhost:9200/users_v14 -d '{
    "aliases": {
        "users": {}
    },

    "mappings": {
        "user": {
            "_routing": {
                "path": "userID"
            },
            "_id": {
                "path": "userID"
            },
            "properties": {
                "userID": {
                    "type": "integer"
                },
                "unreadNotification": {
                    "type": "boolean"
                },
                "features": {
                    "type": "string",
                    "index": "not_analyzed"
                },
                "userGroup": {
                    "type": "string"
                },
                "lastName" : {
                    "fields" : {
                        "metaphone" : {
                            "type" : "string",
                            "analyzer" : "name_metaphone"
                        },
                        "partial" : {
                            "search_analyzer" : "full_name",
                            "index_analyzer" : "partial_name",
                            "type" : "string"
                        },
                        "lastName" : {
                            "type" : "string",
                            "analyzer" : "full_name"
                        }
                    },
                    "type" : "multi_field"
                },
                "firstName" : {
                    "fields" : {
                        "metaphone" : {
                            "type" : "string",
                            "analyzer" : "name_metaphone"
                        },
                        "partial" : {
                            "search_analyzer" : "full_name",
                            "index_analyzer" : "partial_name",
                            "type" : "string"
                        },
                        "firstName" : {
                            "type" : "string",
                            "analyzer" : "full_name"
                        }
                    },
                    "type" : "multi_field"
                },
                "birthday": {
                    "type": "date",
                    "index": "not_analyzed",
                    "format": "date"
                },
                "joinDate": {
                    "type": "date",
                    "format": "basic_date_time"
                },
                "availabilityText": {
                    "type": "string",
                    "index": "no"
                },
                "picture": {
                    "type": "object",
                    "properties": {
                        "default": {
                            "type": "string",
                            "index": "no"
                        },
                        "thumb": {
                            "type": "string",
                            "index": "no"
                        },
                        "hasPhoto": {
                            "type": "boolean"
                        },
                        "service": {
                            "type": "string"
                        },
                        "id": {
                            "type": "string"
                        }
                    }
                },
                "email": {
                    "type": "string",
                    "index": "not_analyzed"
                },
                "favorite": {
                    "type": "nested"
                },
                "languages": {
                    "type": "nested",
                    "properties": {
                        "languageID": {
                            "type": "integer"
                        },
                        "level": {
                            "type": "integer"
                        },
                        "learning": {
                            "type": "boolean"
                        },
                        "teaching": {
                            "type": "boolean"
                        },
                        "native": {
                            "type": "boolean"
                        }
                    }
                },
                "aboutContact": {
                    "type": "string"
                },
                "locations": {
                    "type": "nested",
                    "properties": {
                        "vID": {
                            "type": "integer"
                        },
                        "type": {
                            "type": "string"
                        },
                        "google": {
                            "type": "string",
                            "index": "no"
                        },
                        "yelp": {
                            "type": "string",
                            "index": "no"
                        },
                        "primary": {
                            "type": "boolean"
                        },
                        "coords": {
                            "type": "geo_point"
                        },
                        "sublocality": {
                            "type": "string"
                        },
                        "city": {
                            "type": "string"
                        },
                        "state": {
                            "type": "string"
                        },
                         "country": {
                             "type": "string"
                         }
                    }
                },
                "personal": {
                    "type": "string",
                    "index": "no"
                },
                "interests": {
                    "type": "object",
                    "properties": {
                        "question": {
                            "type": "string"
                        },
                        "answer": {
                            "type": "string",
                            "analyzer": "keyword"
                        }
                    }
                },
                "details": {
                    "type": "object",
                    "properties": {
                        "gender": {
                            "type": "string",
                            "analyzer": "keyword"
                        },
                        "hometown": {
                            "type": "string",
                            "index": "no"
                        },
                        "occupation": {
                            "type": "string",
                            "analyzer": "keyword"
                        }
                    }
                },
                "accounts": {
                    "type": "nested",
                    "properties": {
                        "type": {
                            "type": "string",
                            "index": "not_analyzed"
                        },
                        "id": {
                            "type": "string",
                            "index": "not_analyzed"
                        }
                    }
                },
                "attending": {
                    "type": "nested",
                    "properties": {
                        "events": {
                            "type": "integer"
                        },
                        "eventsWaitList": {
                            "type": "integer"
                        },
                        "attended": {
                            "type": "integer"
                        }
                    }
                },
                "settings": {
                    "type": "object",
                    "properties": {
                        "unit": {
                            "type": "integer"
                        },
                        "formats": {
                            "type": "object",
                            "properties": {
                                "date": {
                                    "type": "integer"
                                },
                                "time": {
                                    "type": "integer"
                                }
                            }
                        },
                        "locale": {
                            "type": "string"
                        },
                        "notifs": {
                            "type": "object",
                            "properties": {
                                "email": {
                                    "type": "object",
                                    "properties": {
                                        "eventsILearn": {
                                            "type": "boolean"
                                        },
                                        "eventsISpeak": {
                                            "type": "boolean"
                                        },
                                        "eventsAllLang": {
                                            "type": "boolean"
                                        },
                                        "updates": {
                                            "type": "boolean"
                                        },
                                        "newMsg": {
                                            "type": "boolean"
                                        },
                                        "requestTandem": {
                                            "type": "boolean"
                                        },
                                        "requestTeacher": {
                                            "type": "boolean"
                                        }
                                    }
                                },
                                "messaging": {
                                    "type": "object",
                                    "properties": {
                                        "acceptAll": {
                                            "type": "boolean"
                                        },
                                        "acceptFollowing": {
                                            "type": "boolean"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },

        "notifications": {
            "_parent": {
                "type": "user"
            },
            "_id": {
                "path": "nID"
            },
            "properties": {
                "nID": {
                    "type": "string"
                },
                "source": {
                    "type": "integer"
                },
                "sourceType": {
                    "type": "integer"
                },
                "sourceID": {
                    "type": "string"
                },
                "from": {
                    "type": "integer"
                },
                "when": {
                    "type": "date",
                    "format": "basic_date_time_no_millis"
                },
                "read": {
                    "type": "date",
                    "format": "basic_date_time_no_millis"
                },
                "snippet": {
                    "type": "string",
                    "index": "not_analyzed"
                }
            }
        }
    },
    "settings" : {
        "number_of_shards": 1,
        "number_of_replicas": 1,
        "analysis" : {
            "filter" : {
                "name_ngrams" : {
                    "side" : "front",
                    "max_gram" : 10,
                    "min_gram" : 1,
                    "type" : "edgeNGram"
                },
                "name_metaphone" : {
                    "replace" : false,
                    "encoder" : "metaphone",
                    "type" : "phonetic"
                }
            },
            "analyzer" : {
                "full_name" : {
                    "filter" : [
                        "standard",
                        "lowercase",
                        "asciifolding"
                    ],
                    "type" : "custom",
                    "tokenizer" : "standard"
                },
                "name_metaphone" : {
                    "filter" : [
                        "name_metaphone"
                    ],
                    "type" : "custom",
                    "tokenizer" : "standard"
                },
                "partial_name" : {
                    "filter" : [
                        "standard",
                        "lowercase",
                        "asciifolding",
                        "name_ngrams"
                    ],
                    "type" : "custom",
                    "tokenizer" : "standard"
                }
            }
        }
    }
}'

echo "Reindexing users with 'requestTandem' enabled & updating missing locations"
elasticsearch-reindex -f localhost:9200/users_v12 -t localhost:9200/users_v14 -m true user_reindexer.js

echo "Updating interactions"
curl -XPUT localhost:9200/interactions_v4 -d '{
      "settings": {
          "number_of_shards": 1,
          "number_of_replicas": 0
      },

      "aliases": {
          "interactions": {}
      },

      "mappings": {
          "message": {
              "_id": {
                  "path": "msgID"
              },
              "properties": {
                  "msgID": {
                      "type": "string",
                      "index" : "not_analyzed"
                  },
                  "participants": {
                      "type": "integer"
                  },
                  "excluded": {
                      "type": "integer"
                  },
                  "updated": {
                      "type": "date",
                      "format": "basic_date_time"
                  },
                  "snippet": {
                      "type": "string",
                      "index": "not_analyzed"
                  },
                  "read": {
                      "type": "integer"
                  }
              }
          },

          "log": {
              "_parent": {
                  "type": "message"
              },
              "properties": {
                  "timestamp": {
                      "type": "date",
                      "format": "basic_date_time"
                  },
                  "message": {
                      "type": "string",
                      "index": "not_analyzed"
                  },
                  "from": {
                      "type": "integer"
                  }
              }
          },

          "request": {
              "_id": {
                  "path": "reqID"
              },
              "properties": {
                  "reqID": {
                      "type": "string",
                      "index" : "not_analyzed"
                  },
                  "oldReqID": {
                      "type": "string",
                      "index" : "not_analyzed"
                  },
                  "type": {
                      "type": "string",
                      "index" : "not_analyzed"
                  },
                  "venue": {
                      "type": "object",
                      "properties": {
                          "vID": {
                              "type": "integer"
                          },
                          "google": {
                              "type": "string",
                              "index": "not_analyzed"
                          },
                          "yelp": {
                              "type": "string",
                              "index": "not_analyzed"
                          },
                          "coords": {
                              "type": "geo_point"
                          }
                      }
                  },
                  "when": {
                      "type": "date",
                      "format": "basic_date_time_no_millis"
                  },
                  "until": {
                      "type": "date",
                      "format": "basic_date_time_no_millis"
                  },
                  "created": {
                      "type": "date",
                      "format": "basic_date_time_no_millis"
                  },
                  "creator": {
                      "type": "integer"
                  },
                  "participants": {
                      "type": "integer"
                  },
                  "status": {
                      "type": "string",
                      "index" : "not_analyzed"
                  }
              }
          }
      }
}'

echo "Reindexing interactions"
elasticsearch-reindex -f localhost:9200/interactions_v2 -t localhost:9200/interactions_v4 interaction_reindexer.js

curl -XPOST localhost:9200/_aliases -d '{
    "actions": [
        { "remove": {
            "alias": "interactions",
            "index": "interactions_v2"
        }},
        { "add": {
            "alias": "interactions",
            "index": "interactions_v4"
        }},
        { "remove": {
            "alias": "users",
            "index": "users_v12"
        }},
        { "add": {
            "alias": "users",
            "index": "users_v14"
        }}
    ]
}'

echo Index has been successfully indexed and transformed, double check before running the next command to delete the old index
echo ' '
echo to delete the previous old indexes run:
echo curl -XDELETE localhost:9200/interactions_v1
echo curl -XDELETE localhost:9200/users_v11
