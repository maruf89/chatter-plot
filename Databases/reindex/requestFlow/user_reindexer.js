'use strict';

const Q = require('q');
const gmAPI = require('googlemaps');
const gmClient = new gmAPI({
    key: 'AIzaSyCd46hEhVV0TRICY9zdxqUpoFhB5C8Sk-A',
    secure: true
});

const getPlace = function (sourceID) {
    const deferred = Q.defer();

    gmClient.placeDetails({
        placeid: sourceID
    }, function (err, res) {
        if (err || res.status !== 'OK')
            return deferred.reject(err || res);

        return deferred.resolve(res.result);
    });

    return deferred.promise;
};

const commonVenue = require('../../../common/util/venue');

commonVenue.serviceConfig('google', {
    getPlace: getPlace,
    Q: Q,
});

module.exports = {
    query: {
        fields: ['_source', '_parent']
    },
    index: function (item, opts, client) {
        const object = {
            index: {
                _index: opts.index,
                _type: item._type,
                _id: item._id
            }
        };

        const source = item._source;

        const returnFn = function () {
            return [
                object,
                source
            ];
        };

        if (item._type === 'notifications') {
            object.index._parent = item.fields._parent;
            return returnFn();
        } else if (item._type === 'user' && source.settings) {
            let settings = source.settings.notifs.email;

            settings.requestTandem = true;
            settings.requestTeacher = true;
        }

        let primaryLocation;

        // fill in location
        if (item._type === 'user' &&
            source.locations &&
            source.locations.length &&
            (primaryLocation = source.locations[0]) &&
            primaryLocation.primary &&
            primaryLocation.google
                //&& !(primaryLocation.sublocality ||
                //  primaryLocation.city ||
                //  primaryLocation.state ||
                //  primaryLocation.country
                //)
        ) {
            console.log('getting location for user:', source.userID);
            console.log(primaryLocation);
            return commonVenue.getPlace(primaryLocation)
                .then(commonVenue.scrapePlace)
                .then(function (formattedPlace) {
                    console.log('got formatted location for user:', source.userID);
                    if (formattedPlace.sublocality) primaryLocation.sublocality = formattedPlace.sublocality;
                    if (formattedPlace.city) primaryLocation.city = formattedPlace.city;
                    if (formattedPlace.state) primaryLocation.state = formattedPlace.state;
                    if (formattedPlace.country) primaryLocation.country = formattedPlace.country;
                    return returnFn();
                }).catch(function (err) {
                    console.log('error getting location for user:', source.userID);
                    console.log(source);
                    console.log('error:');
                    console.log(err);
                    throw err;
                });
        }

        return returnFn();
    }
};