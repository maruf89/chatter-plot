'use strict';

module.exports = {
    query: {
        fields: ['_source', '_parent']
    },
    index: function (item, opts) {
        var object = {
                index: {
                    _index: opts.index,
                    _type: item._type,
                    _id: item._id
                }
            },
            source = item._source,
            returnObj = [
                object,
                source
            ];

        if (item.fields && item.fields._parent) {
            object.index._parent = item.fields._parent;
        }

        return returnObj;
    }
};
