'use strict';

const extractGPlusID = function (user) {
    if (!Array.isArray(user.accounts)) {
        return false;
    }

    var i = 0,
        len = user.accounts.length;

    for (;i < len;i++) {
        if (user.accounts[i].type === 'google') {
            return user.accounts[i].id;
        }
    }

    return false;
};

module.exports = {
    query: {
        fields: ['_source', '_parent']
    },
    index: function (item, opts) {
        var data = item._source,

            object = {
                index: {
                    _index: opts.index,
                    _type: item._type,
                    _id: item._id
                }
            },

            gplusID;

        if (item._type === 'user') {
            if (data.picture &&
                data.picture.default &&
                data.picture.default.indexOf('googleusercontent') !== -1 &&
                (gplusID = extractGPlusID(data))
            ) {
                data.picture = {
                    hasPhoto: true,
                    service: 'gplus',
                    id: gplusID
                };
            }
        } else if (item._type === 'notifications') {
            object.index._parent = item.fields._parent;
        }

        return [
            object,
            item._source
        ];
    }
};