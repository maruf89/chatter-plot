echo "Updating events/listings"

curl -XPUT localhost:9200/events_v4 -d '{
    "aliases": {
        "events": {}
    },

    "mappings": {
        "event": {
            "_routing": {
                "path": "eID"
            },
            "_id": {
                "path": "eID"
            },
            "properties": {
                "eID": {
                    "type": "integer"
                },
                "languages": {
                    "type": "integer"
                },
                "when": {
                    "type": "date",
                    "format": "basic_date_time_no_millis"
                },
                "until": {
                    "type": "date",
                    "format": "basic_date_time_no_millis"
                },
                "created": {
                    "type": "date",
                    "format": "basic_date_time_no_millis"
                },
                "lastUpdated": {
                    "type": "date",
                    "format": "basic_date_time_no_millis"
                },
                "creator": {
                    "type": "integer"
                },
                "organizers": {
                    "type": "integer"
                },
                "levels": {
                    "type": "integer"
                },
                "name": {
                    "fields" : {
                        "metaphone" : {
                            "type" : "string",
                            "analyzer" : "name_metaphone"
                        },
                        "partial" : {
                            "search_analyzer" : "full_name",
                            "index_analyzer" : "partial_name",
                            "type" : "string"
                        },
                        "name" : {
                            "type" : "string",
                            "analyzer" : "full_name"
                        }
                    },
                    "type" : "multi_field"
                },
                "hasPhoto": {
                    "type": "boolean"
                },
                "description": {
                    "type": "string",
                    "index": "no"
                },
                "venue": {
                    "type": "nested",
                    "properties": {
                        "vID": {
                            "type": "integer"
                        },
                        "google": {
                            "type": "string"
                        },
                        "yelp": {
                            "type": "string"
                        },
                        "coords": {
                            "type": "geo_point"
                        },
                        "rsvpOnlyAddress": {
                            "type": "integer"
                        }
                    }
                },
                "locationDisplayName": {
                    "type": "string",
                    "index": "no"
                },
                "published": {
                    "type": "boolean"
                },
                "goingList": {
                    "type": "integer"
                },
                "waitList": {
                    "type": "integer"
                },
                "maxGoing": {
                    "type": "integer"
                },
                "aboutHost": {
                    "type": "string",
                    "index": "no"
                },
                "paymentInfo": {
                    "type": "string",
                    "index": "no"
                },
                "priceString": {
                    "type": "string"
                }
            }
        },

        "listing": {
            "_routing": {
                "path": "eID"
            },
            "_id": {
                "path": "eID"
            },
            "properties": {
                "eID": {
                    "type": "integer"
                },
                "languages": {
                    "type": "integer"
                },
                "when": {
                    "type": "date",
                    "format": "basic_date_time_no_millis"
                },
                "until": {
                    "type": "date",
                    "format": "basic_date_time_no_millis"
                },
                "created": {
                    "type": "date",
                    "format": "basic_date_time_no_millis"
                },
                "lastUpdated": {
                    "type": "date",
                    "format": "basic_date_time_no_millis"
                },
                "creator": {
                    "type": "integer"
                },
                "organizers": {
                    "type": "integer"
                },
                "levels": {
                    "type": "integer"
                },
                "name": {
                    "fields" : {
                        "partial" : {
                            "search_analyzer" : "full_name",
                            "index_analyzer" : "partial_name",
                            "type" : "string"
                        },
                        "name" : {
                            "type" : "string",
                            "analyzer" : "full_name"
                        }
                    },
                    "type" : "multi_field"
                },
                "description": {
                    "type": "string",
                    "index": "no"
                },
                "venue": {
                    "type": "nested",
                    "properties": {
                        "vID": {
                            "type": "integer"
                        },
                        "google": {
                            "type": "string"
                        },
                        "yelp": {
                            "type": "string"
                        },
                        "coords": {
                            "type": "geo_point"
                        },
                        "rsvpOnlyAddress": {
                            "type": "integer"
                        }
                    }
                },
                "locationDisplayName": {
                    "type": "string"
                },
                "published": {
                    "type": "boolean"
                },
                "goingList": {
                    "type": "integer"
                },
                "registrationURL": {
                    "type": "string"
                },
                "paymentInfo": {
                    "type": "string",
                    "index": "no"
                },
                "priceString": {
                    "type": "string"
                }
            }
        }
    },

    "settings" : {
        "number_of_shards": 1,
        "number_of_replicas": 1,
        "analysis" : {
            "filter" : {
                "name_ngrams" : {
                    "side" : "front",
                    "max_gram" : 10,
                    "min_gram" : 1,
                    "type" : "edgeNGram"
                },
                "name_metaphone" : {
                    "replace" : false,
                    "encoder" : "metaphone",
                    "type" : "phonetic"
                }
            },
            "analyzer" : {
                "full_name" : {
                    "filter" : [
                        "standard",
                        "lowercase",
                        "asciifolding"
                    ],
                    "type" : "custom",
                    "tokenizer" : "standard"
                },
                "name_metaphone" : {
                    "filter" : [
                        "name_metaphone"
                    ],
                    "type" : "custom",
                    "tokenizer" : "standard"
                },
                "partial_name" : {
                    "filter" : [
                        "standard",
                        "lowercase",
                        "asciifolding",
                        "name_ngrams"
                    ],
                    "type" : "custom",
                    "tokenizer" : "standard"
                }
            }
        }
    }
}'

echo "Reindexing events"
elasticsearch-reindex -f localhost:9200/events_v3 -t localhost:9200/events_v4 -m true -s 300000 event_reindexer.js

curl -XPOST localhost:9200/_aliases -d '{
    "actions": [
        { "remove": {
            "alias": "events",
            "index": "events_v3"
        }},
        { "add": {
            "alias": "events",
            "index": "events_v4"
        }}
    ]
}'

echo Index has been successfully indexed and transformed, double check before running the next command to delete the old index
echo ' '
echo to delete the old index run:
echo curl -XDELETE localhost:9200/events_v2
