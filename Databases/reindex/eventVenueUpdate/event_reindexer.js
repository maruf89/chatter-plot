'use strict';

module.exports = {
    query: {
        fields: ['_source', '_parent']
    },
    index: function (item, opts, client) {
        var object = {
                index: {
                    _index: opts.index,
                    _type: item._type,
                    _id: item._id
                }
            },
            source = item._source,
            returnObj = [
                object,
                source
            ],

            returnFn = function () {
                return returnObj;
            };

        if (!source.venue) {
            console.log('invalid venue:', item._id);
            return returnFn();
        }

        return client.get({
            index: 'venues',
            type: 'venue',
            id: source.venue.vID,
        }).then(function (venue) {
            //console.log(venue);
            let venueSource = venue._source;
            if (venueSource) {
                if (venueSource.google) {
                    source.venue.google = venueSource.google;
                }
                if (venueSource.yelp) {
                    source.venue.yelp = venueSource.yelp;
                }
            }

            return returnFn();
        });
    }
};
