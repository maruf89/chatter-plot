'use strict';

const _ = require('lodash-node/modern');

const notifs = {
    "email": {
        "eventsILearn": true,
        "eventsISpeak": true,
        "eventsAllLang": true,
        "updates": true,
        "newMsg": true,
        "requestTandem": true,
        "requestTeacher": true
    },
    "messaging": {
        "acceptAll": true,
        "acceptFollowing": true
    }
};

module.exports = {
    query: {
        fields: ['_source', '_parent']
    },
    index: function (item, opts) {
        var data = item._source,

            object = {
                index: {
                    _index: opts.index,
                    _type: item._type,
                    _id: item._id
                }
            },

            settings;

        if (item._type === 'user') {
            if (data.settings) {
                settings = data.settings.notifs;

                _.each(notifs, function (fields, group) {
                    var subSettings = settings[group];

                    if (!subSettings) {
                        settings[group] = fields;
                        return false;
                    }

                    _.each(fields, function (def, key) {
                        if (!subSettings.hasOwnProperty(key)) {
                            subSettings[key] = def;
                        }
                    });
                });
            }
        } else if (item._type === 'notifications') {
            object.index._parent = item.fields._parent;
        }

        return [
            object,
            item._source
        ];
    }
};