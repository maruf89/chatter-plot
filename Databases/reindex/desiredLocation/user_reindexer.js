'use strict';

var Q       = require('q'),
    geocoder = require('geocoder'),

    geocodeFn = Q.denodeify(geocoder.geocode.bind(geocoder));

module.exports = {
    query: {
        fields: ['_source', '_parent']
    },
    index: function (item, opts, client) {
        var object = {
                index: {
                    _index: opts.index,
                    _type: item._type,
                    _id: item._id
                }
            },
            source = item._source,
            returnObj = [
                object,
                source
            ],

            returnFn = function () {
                deleteFields(source, addressFields);
                return returnObj;
            },

            addressFields = [
                'sublocality',
                'city',
                'state',
                'postal',
                'country'
            ],

            hasLocations = false,
            promise,
            locationsKey = 'locations';

        if (item._type === 'notifications') {
            object.index._parent = item.fields._parent;
            source.sourceID = String(source.sourceID);
            source.read = source.read || null;
            return returnObj;
        }

        //// User object

        // Create a features array
        if (!Array.isArray(source.features)) {
            source.features = [];
        }

        // push the new notifications
        if (source.features.indexOf('locs01') === -1) {
            source.features .push('locs01');
        }

        // Checking if we have locations
        if (!Array.isArray(source[locationsKey]) || !source[locationsKey].length) {
            let alreadyParsed;

            if (alreadyParsed = source[locationsKey[0]]) {
                if (alreadyParsed.google || alreadyParsed.yelp || alreadyParsed.type === 'temp') {
                    // We have what we need, eject
                    return returnFn();
                }
            }

            // If they have both street address AND coordinates we cannot use that address
            // because it's sharing their exact home location (maybe)
            if (source.coords && !source.adress) {
                // if not, just use the existing coordinates
                source[locationsKey] = [{
                    primary: true,
                    coords: source.coords,
                    type: 'temp'
                }];

                // We have what we need, eject
                return returnFn();
            } else if ((source.sublocality || source.city) && (source.state || source.country || source.postal)) {
                // Else if we have enough locations fields to get coordinates

                // Build an address string
                let addressString = addressFields.reduce(function (arr, field) {
                    if (source[field]) {
                        arr.push(source[field]);
                    }

                    return arr;
                }, []).join(' ');

                // Make a coordintes request using those fields
                promise = geocodeFn(addressString)
            } else {
                source[locationsKey] = null;
                return returnFn();
            }
        } else {
            hasLocations = true;
            promise = client.mget({
                index: 'venues_v1',
                type: 'venue',
                body: {
                    ids: source[locationsKey].map(function (id) { return String(id); })
                }
            });
        }

        return promise.then(function (response) {
            if (hasLocations) {
                // check if we were querying existing venues
                if (Array.isArray(response.docs) && response.docs[0]) {
                    // Iterate over the venue responses and build the new locations object
                    source[locationsKey] = response.docs.reduce(function (arr, venue) {

                        var data = venue._source,
                            returnVenue = {
                                vID: parseInt(venue._id)
                            };

                        if (!data) {
                            return arr;
                        }

                        // only push if we have a source
                        if (typeof data.google === 'string') {
                            returnVenue.google = data.google;
                            arr.push(returnVenue);
                        } else if (typeof data.yelp === 'string') {
                            returnVenue.yelp = data.yelp;
                            arr.push(returnVenue);
                        } else if (typeof data.source === 'object' && data.source.name === 'GOOGLE') {
                            returnVenue.google = data.source.ID;
                            arr.push(returnVenue);
                        } else if (typeof data.source === 'object' && data.source.name === 'YELP') {
                            returnVenue.yelp = data.source.ID;
                            arr.push(returnVenue);
                        }

                        if (arr.length === 1) {
                            returnVenue.primary = true;
                            returnVenue.coords = data.coords;
                        }

                        return arr;
                    }, []);
                }
            } else {
                // Otherwise we were creating a temp location & fetching coordinates for it
                let coords = response;

                // If we have coordinates we can use
                coords = (Array.isArray(coords.results) && coords.results[0]) ? coords.results[0].geometry.location : null;
                if (coords && coords.lat && coords.lng) {
                    // Set a temporary locations options

                    source[locationsKey] = [{
                        primary: true,
                        coords: {
                            lat: coords.lat,
                            lon: coords.lng
                        },
                        type: 'temp'
                    }];
                }
            }

            // Return either everything
            return returnFn();
        });
    }
};

var deleteFields = function (source, fields) {
    // Delete all of the obselete fields
    try {
        delete source.name;
        delete source.address;
        delete source.coords;

        fields.forEach(function (field) {
            delete source[field];
        });
    } catch (e) {}
};
