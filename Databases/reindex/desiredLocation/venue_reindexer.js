'use strict';

module.exports = {
    query: {
        fields: ['_source']
    },
    index: function (item, opts) {
        var object = {
                index: {
                    _index: opts.index,
                    _type: item._type,
                    _id: item._id
                }
            },
            source = item._source,
            newSource = {
                vID: parseInt(source.vID, 10),
                coords: source.coords,
            };

        if (typeof source.source === 'object' && source.source.ID) {
            newSource.google = source.source.ID;
        }

        if (source.name) {
            newSource.name = source.name;
        }

        if (source.tz) {
            newSource.tz = source.tz;
        }

        return [
            object,
            newSource
        ];
    }
};
