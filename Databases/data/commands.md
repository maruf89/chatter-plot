# events.json
curl -XPUT localhost:9200/events_v2 -d '

# languages.json
curl -XPUT localhost:9200/sources_v1 -d '

# data/languages.bulk
curl -s -XPOST localhost:9200/sources/_bulk --data-binary @languages.bulk; echo

# user.json
curl -XPUT localhost:9200/users_v2 -d '

# venues.json
curl -XPUT localhost:9200/venues_v1 -d '

# interactions.json
curl -XPUT localhost:9200/interactions_v1 -d '

# notifications.json
curl -XPUT localhost:9200/notifications_v1 -d '