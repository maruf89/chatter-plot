### Very disorganized instructions for building the Chatterplot dev environment

1. Install XCode `$ xcode-select --install`
1. Install [git](http://mac.github.com/)
2. Install iojs (node) `$ echo "Current io.js version is $(curl -fsSL https://iojs.org/dist/index.tab | head -2 | tail -1 | cut -f 1)"`<br>
`$ curl -fsSL bit.ly/iojs-dev -o /tmp/iojs-dev.sh; bash /tmp/iojs-dev.sh`
3. Install [iTerm2](https://www.iterm2.com/downloads.html)
4. Create an `opts` directory where we'll hold 2 of our databases `sudo mkdir /opts && sudo chmod 777 /opts`
4. Install *Brew* `$ ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`
5. Check brew `$ brew doctor`
6. Update brew `$ brew update`
7. Install the *MariaDB* database `$ brew install mariadb`
8. Continue setting up MariaDB - Continue from **Step 7** at this [**link**](https://mariadb.com/blog/installing-mariadb-10010-mac-os-x-homebrew)
9. Once you get mariadb started and logged in as root run these commands:
10. Create a chatterplot user `$ CREATE USER 'chatterplot'@'localhost' IDENTIFIED BY 'Bcp1tbfSite 4vA';`
11. Create the users database `$ CREATE DATABASE users;`
12. Grant all rights of the users db to the new cp user `$ GRANT ALL PRIVILEGES ON users.* TO 'chatterplot'@'%' WITH GRANT OPTION;`
9. Install [latest java](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) (1.7 or 1.8 is required for elasticsearch)
5. Install [Elasticsearch](https://download.elastic.co/elasticsearch/elasticsearch/elasticsearch-1.5.0.zip) *(Download the zip)*
6. Make sure you have `/opt` created `$ sudo mkdir /opt` if you don't get an error run: `$ sudo chown -R `whoami` /opt`
6. Find elasticsearch in the Downloads directory, unzip it and in the command line run `$ mv ~/Downloads/elasticsearch-1.5.0 /opt/elastic`
6. Install needed plugin `cd /opt/elastic && bin/plugin install elasticsearch/elasticsearch-analysis-phonetic/2.5.0`
6. Install the Redis database `$ cd ~/Downloads && wget https://github.com/antirez/redis/archive/2.8.21.tar.gz`
7. unzip the redis file `$ tar zxvf redis.x.x.x && mv redis.x.x.x /opt/redis` *You will have to replace `redis.x.x.` with the exact file name*
8. Next build the redis database `$ cd /opt/redis && make`
3. Create an account on [Bitbucket](https://bitbucket.org) and send me your username or email
3. Once I’ve added you to the team you can download the codebase
3. Go to a folder where you want to install `$ cd ~ && mkdir -p git && cd git && git clone git@bitbucket.org:maruf89/chatter-plot.git` *(This will create a git folder in your home directory and clone the repository there)*
3. Install the required node modules `$ npm install -g stylus grunt grunt-cli node-inspector nodemon coffee-script n nib elasticsearch-reindex mocha`
3. Update your iojs to the current version we’re using `2.2.1`
3. Once \#6 is done downloading run: `$ cd chatter-plot`
4. Install all of the node dependencies: `$ npm install`
5. next go to your home directory because we'll need to create ssl certs `$ mkdir -p ~/ssl && cd ~/ssl`
6. `$ sudo ssh-keygen -f chatterplot.key`
7. `$ sudo openssl req -new -key chatterplot.key -out chatterplot.csr`
8. `$ sudo openssl x509 -req -days 365 -in chatterplot.csr -signkey chatterplot.key -out chatterplot.crt`
5. Install Oh my ZSH `$ curl -L https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh | sh`
6. Edit your startup config `$ vim ~/.zshrc`

Inside the file you will want to locate `plugins=` and replace it with `plugins=(git)`

Next go to the very bottom (Not in insert mode hit `Shift + g` it will take to the end of the file).
Somewhere beneath `# Example aliases` add these lines:


```bash
alias redis="nohup redis-server /usr/local/etc/redis.conf &"
alias elastic="sudo sh /opt/elasticsearch/startfile.sh"
alias mariadb="cd /usr/local/Cellar/mariadb/10.0.17; nohup /usr/local/Cellar/mariadb/10.0.17/bin/mysqld_safe --datadir=/usr/local/var/mysql &"
alias cpSetup="elastic && redis && mariadb && cd ~/cp"

alias sup="git status"
alias garble="git add -A :/"
alias scribble="git commit -m "

ulimit -n 2560
```

**Note:**
in vim you type `i` to insert<br>
`esc` to get out of insert mode (required to save or exit)<br>
`:w (enter)` to write or<br>
`:x (enter)` to write and exit<br>

Lastly startup the environment up by running:<br>
`$ cpSetup`<br>
*(wait like 10 seconds for all of the databases to start up)*<br>
`$ grunt dev`