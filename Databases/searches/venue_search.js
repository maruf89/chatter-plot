// Single search
curl -XGET localhost:9200/venues/venue/_search -d '{
    "query": {
        "filtered": {
            "filter": {
                "nested": {
                    "path": "source",
                    "query": {
                        "filtered": {
                            "filter": {
                                "bool": {
                                    "must": [{
                                        "term": {
                                            "source.name": ["GOOGLE"]
                                        }
                                    }, {
                                        "term": {
                                            "source.ID": ["ChIJZRZQJhCU3UYRRMKcU2izpVw"]
                                        }
                                    }]
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}'

// Multi search
curl -XGET localhost:9200/venues/venue/_search -d '{
    "fields": ["vID", "source.name", "source.ID"],
    "query": {
        "filtered": {
            "filter": {
                "nested": {
                    "path": "source",
                    "query": {
                        "filtered": {
                            "filter": {
                                "bool": {
                                    "should": [{
                                        "bool": {
                                            "must": [{
                                                "term": {
                                                    "source.name": ["GOOGLE"]
                                                }
                                            }, {
                                                "term": {
                                                    "source.ID": ["ChIJZRZQJhCU3UYRRMKcU2izpVw"]
                                                }
                                            }]
                                        }
                                    }, {
                                        "bool": {
                                            "must": [{
                                                "term": {
                                                    "source.name": ["GOOGLE"]
                                                }
                                            }, {
                                                "term": {
                                                    "source.ID": ["ChIJhS_tjQmU3UYRbLke3ZZm55g"]
                                                }
                                            }]
                                        }
                                    }]
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}'