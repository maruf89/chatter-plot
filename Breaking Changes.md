###Elasticsearch

- Created a 'lib/config/misc/constants.json' to hold the Elasticsearch index names/types

###Grunt updates

- npm install grunt-ng-annotate --save
- npm install ng-annotate --save
------
- **Build** for cachebusting - after renaming to cached version, *copy* copies *main.styl* from app -> dist (TODO: use copy to make a copy of the timestamp, not rename)

- **Important** Updating grunt jadeUsemin from 0.5.2 to 0.11.0 minifies the code now apparently which may fuck up some bower_components which aren't ng-minified

# Scaling

- When scaling will it be a problem that some scripts use redis without pub/sub?


###Could Break - Changed

`esUser.mget` to not return docs, added `esUser.mgetExtractSource` to handle that

- Test notifying users
- Test exportWithVenues (basically anything with events)
- Test `socket.on '/user/get>'` with multiple users

###Moves

- Moved `venueHelper.coffee` from `util/elasticsearch/venueHelper` to `services/helpers/venueHelper.coffee`
- Moved the 400/500 errors into `config/misc/constants.json`


###Refactor

- Added comments to **Event Service** and **Venue Service**

### 2015-05-19

- Added Cantonese to the languages

```
curl -XPOST localhost:9200/sources/languages -d '{ "languageID":229, "language":"Cantonese", "native":"广东话", "language_suggest": { "input": ["Cantonese", "广东话"], "output": "Cantonese|229", "weight": 5 }}'
```
### 2015-05-15

Update interest mapping in users

`cd Databases/reindex/user_interests`
`sh ./call.sh`

### 2015-05-12

Update all fake accounts to disable messaging
`for i in {362..460}; do curl -XPOST "http://localhost:9200/users/user/$i/_update" -d '{"doc": {"settings": {"notifs": {"messaging": {"acceptAll": false}}}}}'; done`

### 2015-04-12

- Added Timezone to venues
```json`
"timezone": {
    "type": "integer",
    "index": "not_analyzed"
}
```

### 2015-03-24

- Elasticsearch update: Added `sublocality` to `users` index

### 2015-03-10

Swapped the timezones in the settings of all users and updated moment

```bash
npm install

cd ~/public_html/Databases/reindex/users_timezone_swap
sh call.sh

```


### 2015-03-02

Moved socket/event - '/user/get' from secure to unsecure

### 2-26-2015

Changed events mappings: removed `going` and replaced with an integer array `goingList`
to run:
```bash
cd
npm install -g elasticsearch-reindex
cd ~/public_html/Databases/reindex/events_v2
sh call.sh
```

### 12-8-2014

Changed language autocomplete to read from the front end. Any language changes will need to be updated on the front end at `config/languages.json` as well as in Elasticsearch

### 11-30-2014

Shouldn't break anything but I added a `staging` run mode alongside `development` and `production` and it basically clones dev so I added it everywhere there was dev

- Updated index.jade to listen for `staging` as well and added some els statments instead of explicit `if`'s

### 11-26-2014

Moving settings into users as a nested doc since joins aren't very convenient and parent/child are only really good if there's a one-to-many relationship for parents to children and these are just 1-to-1

Back up any existing users and recreate the users index

### 12-28-2014

Updated the angular google maps API (because bower keeps updating it and they keep changing their code). Existing maps *might* break **98b3a14**

### 1-24-2015

Renamed/modified share module from `activities/shareActivity` to `misc/sharePage` and changed from `share-activity` to `share-page`

### 2-5-2015

Added grunt-contrib-jade, grunt-angular-templates and heavily modified the flow to be able to preload jade templates 
