# Client Side

Chatterplot uses the AngularJS framework along with a variety of other libraries including:

- Google Maps
- lodash (lodash-node/modern)
- jQuery

As far as actually *writing* code we use:

- for *Javascript* - [**typescript**](http://www.typescriptlang.org/)
- for *CSS* - [**stylus**](http://learnboost.github.io/stylus/)
- for *HTML* - [**jade**](http://jade-lang.com/)

### Grunt

We use grunt extensively for everything from building deploys to preprocessing to browserifying. Everything under development resides in under `/app` and production is under `/build`

Some important things to know:

- Do not touch `views` folder as all jade templates are copied from the `scripts` folder to their
- ...

-------

## Structure

Everything under development is in `app`. Jade template files live together with Coffeescript files in the `scripts` folder. If it's a site section, it gets its own folder. If it's a sub section, it gets it's own folder under the parent, etc...

### Browserify

... is used to require the *app* module and any other libraries *(mainly 'lodash-node/modern') or json data files

### Directives (and Templates)

General directives live under `scripts/misc` and they get specified further on down. Section specific directives live in a `directives` folder underneath a section folder
*Example* `updatePassword` directive is specific to the accounts section so it would live under `app/scripts/account/updatePassword`

Directive folders are the only things that are uniquely named. All templates are named `Template.jade` and all directives are named `Service.coffee`


### Controllers

Controller files are named `Ctrl.coffee` if they are the main controller in the section. **In** the file they are named {folder_name}Ctrl so the account settings controller would be named *SettingsCtrl* and it's filepath would be `app/scripts/account/settings/Ctrl.coffee`


### Formatting

Use the `Config` service to format anything that applies to user's preference (date format, time, etc...)

Use the `Util` service to format anything that goes to the server. Also has date/time formatting but only for data types corresponding to Elasticsearch (for now)

------

## Language

### Locale Change

On locale change the following event gets called

`DataBus.emit('/locale/change', 'ru-RU')`

*Where `'ru-RU'` replace with the locale code of the new locale*



## Routes

### reloadOnSearch

This functionality has been replaced by ui routers `$urlRouter#sync` method. The functionality lies in `scripts/always/stateTransition.coffee`. Look at the functions/methods `_initURLWatch` & `events.locationChangeSuccess`

DataBus listens to specific keys to add regex watches. On `locationChangeSuccess`, if there are any regexes to watch then it iterates over each pattern, and compares the result of applying the regex to both the `toURL` and `fromURL` and if their results differ then the page will **NOT** be reloaded

Example - Do not reload the controller if we strip the `action=` from a URL's search

```js
var ignoreUrlChange = [/(?:action)=/];
DataBus.emit('/state/url/watch', ignoreUrlChange);

// Destroy the listener on scope destroy
$scope.$on('$destroy', function () {
    DataBus.emit('/state/url/unwatch', ignoreUrlChange);
});
```

------

## Site Sections

### Maps

CP uses google maps V3 along with [Angular Google Maps](http://angular-ui.github.io/angular-google-maps) to support all map related functionality. The other included libraries we use with google maps include:

- [Infobox](http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox)
- [Marker Cluster V3](http://google-maps-utility-library-v3.googlecode.com/svn/tags/markerclusterer/1.0.2)
- Places: *provides location search/autocomplete* `https://maps.googleapis.com/maps/api/js?libraries=places`
- *Map Styling [Mapstylr](http://www.mapstylr.com)*

To implement this we have a {@link Maps} service which contains map styles, defaults and utility methods. Map instances are created via {@link MapsCtrl} (which is currently way bigger than it should be filesize-wise). Any page that uses the map must be a child of a map template. The map is created in a transclude, and when it is created, it registers itself via {@link Maps#setInstance} and because it is a transclude, the page that is using the map created alongside in a separate scope (it is **NOT** a descendant of MapsCtrl). To get a reference to the Map instance, the controller must require {@link Maps} and call {@link Maps#getInstance}.
 
The reason we don't have the map instance in the Service is so that, down the line if/when we ever need to have more than 1 map on the page, they can live side-by-side disconnected and bound to their controller. (it's currently not set up for this but it wouldn't be too much work)
 
#### Search Stacks
 
The way we keep track of different sets of markers on the same map instance when we switch pages (example: going from a search page to a profile page) is by storing each set on it's own layer. Think of it as a stack:

- Load Search results => initate the `Search` stack => Get results and append them to the `Search` stack =>
- Click on a user in the search results ->
- Load the Profile page => initate the `Profile` stack (the search stack gets moved to the history) => repeat…

Each search stack can have it's own events, own everything. There are utility methods in MapsCtrl to take care of selecting, deselecting sets: MapsCtrl#(selectSet|getSet|setHistory|deselectSet)


------

### Translations

To add a new language you need to add the language keys to:

- *Grunt* to the `locale` section
- Add to `common/config/locales.json`
- Add app.js file to `common/front/sections/config/locales` && route translations to `common/front/sections/config/routes/translations`
- Make sure an entry exists in `app/config/defaultUserSettings.coffee` for the new language
- Add available subdomains to `lib/config/env/all.js`.subdomains

- Copy the translated rows from the csv to [Mr DataConverter](http://shancarter.github.io/mr-data-converter/) (Output as *JSON-Dictionary* - Delimiter *Tab*)
- Add the translated text to the corresponding `app/assets/languages/<locale_code>/*.lang.json` files

## Style Guide

There is a whole folder of styled elements under `styles/elements` as well as directives

### Modals

See the images in `ng/misc/modals` for the different kinds of modals

### Lists

See the images in `ng/misc/layouts` for the types of (right now only dropdown) lists

## Utility Functions

### Venue Utilities

`app/service/venues.coffee`

Set of methods to help handle addresses and scraping/parsing location data

--------

### User must Login/Signup to Do that

For all actions where a user must be authenticated to complete, the `Auth#loginRequired` is called passing in an `action` and an `args` parameter (see for details). 

Whichever route is expecting the *must be logged in* action must accept both `action` and `args` in it's Route controller as otherwise UI Router will not pass it. Upon authentication, user will be redirected back to the original state with the passed params. `$location` or `$stateParams` are needed to retrieve the params and to decode the args, the `Util.parseActionArgs` is called which will convert the string back to an object;

### Form Error Messages (Popovers)

Using the directive `popover-msg` found at `scripts/misc/form/popoverMsg` to handle input field errors. To set up, add the `popover-msg` as an *attribute*, *class* or *element* - *(Additionally add a `nom` class if you want no margin-bottom added)*

```jade
.popover-msg(popover-msg)
    .popover.error(
        data-pos="top"
        ng-messages="register.email.$error"
        ng-if="(register.$submitted || register.email.$touched) && register.email.$invalid"
    )
        span.form-error(ng-message="required")
            span(i18n="auth.ERROR-REQUIRED")

        span.form-error(ng-message="pattern")
            span(i18n="auth.ERROR-INVALID_EMAIL")
        span.form-error(ng-message="emailAvailable")
            span(i18n="auth.ERROR-EMAIL_IN_USE")

    .popover.info(
        data-pos="top"
        ng-if="(register.email.$viewValue && register.email.$touched) && register.email.$pending"
    )
        span.form-update(
            i18n="auth.VERIFYING_EMAIL_UNUSED"
        )

    .popover.on-focus(
        data-pos="right"
    )
        span(i18n="auth.PASSWORD_STRENGTH")
        .password-meter(
            ng-class="vars.passwordStrength"
            password-meter="vars.passwordComplexity"
        )

    //- Important to add .field class to the input
    input.field(
        type="text"
        data-i18n-attr="{placeholder: 'auth.YOUR_EMAIL'}"
        name="email"
        ng-model="vars.email"
        ng-min="7"
        required=""
        email-available-validator
        ng-pattern="vars.EMAIL_REGEXP"
    )
```

The three classes/types are `error`, `info` and `on-focus`. They each take a `data-pos="{position}"` attribute signifying where they show up relative to the input. Add a `.field` class to the input.

**Important** Element positioning is important for `.error` and `.field`. The field get's a red glow if it's previous sibling is the error element: `.popover.error + .field { border 1px solid $red; box-shadow 0 0 6px 1px $red !important; }`

--------

### Conditional attributes directive

<small>Found in `modules/miscDirectives`</small>

Applies an attribute to the element, only if the express results to true

**Format:** `div(conditional-attr="{ '<attr>|<value>': <expression> }")`

Example - Adds a conditional id attribute

```jade
tr(
    ng-repeat="Event in vars.eventsList track by Event.eID"
    conditional-attr="{ 'id|activity-date': vars.firstDateOccurrence(Event.date) }"
)
```

Example - Sets a href attribute only if the value exists

```jade
a(conditional-attr="{ 'href': translator.link }")
```

Optionally you can set to have the value be interpolated. Don't know how slow this is though...

```jade
tr(
    conditional-attr="{ 'scroll-body|sticky-group-{{::Group[0].eID}}': 1, parseValue: true }"
)
```

# Backend

We use **Nodejs** *(iojs at the moment)* along with **Express**
For the databases we use:
- **Elasticsearch** as the main data store
- **MariaDB** as the secure persistent storage
- **Redis** for hot data as well as our pubsub across our (future) cluster

## Databases

### MariaDB

MariaDB stores all information of a user pertaining to 3rd party social media service tokens, user password, user permissions (`role`) and a few tables just for defining permissions

#### Permissions

Permissions are stored in MariaDB as a bitmask with a corellating string

- `1`    : `USER_EDIT` - (Admin privilege) allows editing any user
- `2`    : `USER_DELETE` - (Admin privilage) allows deleting any user
- `4`    : `USER_ADD` - (Admin privilage) allows creating users
- `8`    : `USER_VIEW` - (Admin privilage) allows viewing any user including otherwise non-public information
- `16`   : `EDIT_PERMISSIONS` - (Admin privilage) allows changing any users permissions
- `32`   : `SELF_VIEW` - allows viewing your own information
- `64`   : `SELF_EDIT` - allows editing your own information
- `128`  : `CREATE_EVENT` - allows creating an event in & storing in the database
- `256`  : `PUBLISH_EVENT` - allows publishing one of your events
- `512`  : `ABUSE_VIEW` - (Admin privilage) allows viewing any message conversation between 2 people
- `1024` : `SEND_MSG` - allows sending messages to other users

--------

## TODO:

### Minor

- Replace CK Editor with [Angular-trix](https://github.com/sachinchoolur/angular-trix)
- *offline* Replace all +media names to be one of `[xs, sm, md, lg]`
- *offline* See TODO at `app/scripts/misc/form/timePicker/Service.coffee`
- *offline* Replace the language code to key api call to read from local file
- *offline* **Tooltip** (and trancludeDir) move the *post* functions into a controller to share their logic instead of repeating
- *offline* Remove $locale and replace with our instances of moment. Also extract the months from moment for the date picker
- find a way to remove duplicate code from `events/list/Dir` and `events/single/Ctrl` (having events list load the single Ctrl won't work because single depends on a resolve dependency which throws an error on events list). Maybe a factory is the route to go

### Major

- Reintegrate Mailchimp into the signup
- Build in tests (start separating what needs to be less coupled)
- Integrate JSDoc
- Implement github and cache files based on the commit key, instead of the deployVersion rounding to the closest 5 minutes in `express.coffee`
- Possibly replace socket.io + Redis with [SocketCluster](https://github.com/TopCloud/socketcluster)

### Potential Issues

- If there's a several day long event, the event has started but has not ended, it should still show up in the search results

--------

## Difficult Repeated Problems

#### How to update pages url without reloading?

**Out of the blue the following code stopped working...**

```coffee
$state.transitionTo $state.current.name, null,
    location: 'replace' # Set to false if you want it to exist in window.history
    inherit: false # inherit URL parameters/resolves
    relative: $state.$current
    notify: false # If `true` will broadcast $stateChangeStart and $stateChangeSuccess events.

```

Full details see [https://github.com/angular-ui/ui-router/wiki/Quick-Reference#options](https://github.com/angular-ui/ui-router/wiki/Quick-Reference#options)

--------

## Flaws

- `scripts/misc` should be renamed to `directives` or something more descriptive
- On the **backend**, there is a bit of mix up between what Utilities and Services do. Controllers (sockets) should talk to services. Services should talk to their helpers to offload the grunt work and stay more readable
- Starting to think Neo4j would be better than Elasticsearch for this use case
