#!/bin/bash

npm install -g bower grunt grunt-cli forever nodemon
npm install
bower install

HOME_DIR=$(pwd)
MODULES=${HOME_DIR}/app/modules
NODE_MODULES=${HOME_DIR}/node_modules
cd ./app
rm -fr modules
mkdir modules
cd modules

ln -s ${NODE_MODULES}/elasticsearch ${MODULES}/elasticsearch
ln -s ${NODE_MODULES}/ng-flow ${MODULES}/ng-flow

echo "Make sure Elasticsearch, Redis and MariaDB are installed and running before starting the server"
