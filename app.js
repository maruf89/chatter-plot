'use strict';

if (process.env.NODE_ENV === 'production') {
    require('newrelic');
}

/**
 * This script initiates individual cluster workers & is also
 * the main script when running in dev so that we can debug properly
 */

const env                 = process.env.NODE_ENV = process.env.NODE_ENV || 'development';
const path                = require('path');
const libDir              = path.resolve(process.cwd(), 'lib');
const express             = require('express');
const sio                 = require('socket.io');
const sio_redis           = require('socket.io-redis');
const http                = require('http');
const Fetch               = require(path.join(libDir, 'Fetcher')); // Define the global Fetcher
const config              = Fetch.config('config', true, 'js');
const redisClients        = Fetch.database('redis');
const logger              = require('bragi');
const helmet              = require('helmet');
const port                = 9000;

logger.log('init:server:worker', 'init');

//  Service instantiation
const app     = express();

    // Start ssl server
const server = http.createServer(app).listen(port, config.hostname, function () {
    if (env !== 'production') {
        logger.log('server:worker', 'Server started on', config.hostname, 'on port', port);
    }
});

    // Bind Socket.io to our http server
const io      = sio.listen(server);

const ONE_YEAR = 31536000000;
app.use(helmet.hsts({
    maxAge: ONE_YEAR,
    includeSubdomains: true,
    force: true
}));

// Tell Socket.IO to use the redis adapter. By default, the redis
// server is assumed to be on localhost:6379. You don't have to
// specify them explicitly unless you want to change them.
io.adapter(sio_redis({
    host: 'localhost',
    port: 6379,
    pubClient: redisClients.publish,
    subClient: redisClients.subscribe,
    return_buffers: true,
}));

const socket                  = io.of('/');
const secureSocket            = io.of('/secure');

const socketRoutes            = require(path.join(libDir, 'socketRoutes'));
const secureSocketRoutes      = require(path.join(libDir, 'secureSocketRoutes'));

// Bind both secure/insecure routes to the basic socket Routes
socketRoutes(secureSocket);
socketRoutes(socket);

// Only bind secure socket to the secure routes
secureSocketRoutes(secureSocket);

// Express settings
Fetch.config('express', true, 'js')(app);
Fetch.config('sitemap', true, 'js').init();

// Routing
require(path.join(libDir, 'routes'))(app);

Fetch.cron('index').init();
