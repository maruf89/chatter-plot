/**
 * @module bDatabaseElasticsearch
 */

'use strict';

var _ = require('lodash-node/modern'),
    Q = require('q'),
    elastic = require('elasticsearch'),
    logger = require('bragi'),
    sequence = require('es-sequence'),

    Fetch = GLOBAL.Fetcher,

    client,
    config = Fetch.config('config', true, 'js'),
    clientConfig = {
        host: config.elasticsearch.host + ":" + config.elasticsearch.port
    };
_.extend(clientConfig, config.elasticsearch.options);

logger.log('init:service:elasticsearch', 'initiating elasticsearch client');
exports.client = client = new elastic.Client(clientConfig);

logger.log('init:service:elasticsearch', 'initiating es-sequence');
sequence.init(client);


/**
 * Formats an array of objects for bulk purchase
 *
 * @param  {object}        data
 * @option data {string}   id        Must contain a property that matches what's passed `idField` if delete or update
 * @param  {string}        action    Either: `index`, `create`, `update` or `delete`
 * @param  {string}        index     database index
 * @param  {string}        type      index type
 * @param  {String|Number} idField   an id that's optional if action = create/index but required if delete
 * @param  {boolean}       saveID    whether to keep the id field after saving
 * @param  {Object=}       script    if `update` then can pass a script which must contain a `script` property
 * @return {array}                   a formatted array ready for elasticsearch
 */

exports.bulkFormat = function (data, action, index, type, idField, saveId, script) {
    var bulkResponse;
    if (idField == null) {
        idField = 'id';
    }
    if (saveId == null) {
        saveId = false;
    }
    logger.log('service:elasticsearch:bulkFormat', 'called');
    bulkResponse = [];
    if (script && typeof script.script !== 'string') {
        logger.log('error:service:elasticsearch:bulkFormat', 'expecting script parameter to contain a string `script` parameter');
        throw new Error();
    }
    _.each(data, function (entry) {
        var key;
        key = {};
        key[action] = {
            _index: index || entry._index,
            _type: type || entry._type,
            _id: entry[idField] || entry._id
        };
        if (entry._parent) {
            key[action]._parent = entry._parent;
            delete entry._parent;
        }
        bulkResponse.push(key);
        if (!saveId || script) {
            delete entry[idField];
        }
        if (action === 'index' || action === 'create' || action === 'search') {
            return bulkResponse.push(entry);
        } else if (action === 'update') {
            if (script) {
                if (script.upsert && typeof script.upsert === 'boolean') {
                    script.upsert = entry._upsert || entry;
                }
                if (!script.params) {
                    script.params = entry._params || entry;
                }
            }
            return bulkResponse.push(script || {
                    doc: entry
                });
        }
    });
    return bulkResponse;
};


/**
 * Most basic Elasticsearch index implementation
 *
 * @param  {object} body     what to index
 * @param  {object} options  where
 * @return {Promise}         Contains indexing response
 */
exports.index = function (body, options) {
    logger.log('service:elasticsearch:index', 'called');
    return client.index({
        index: options.index || options.INDEX,
        type: options.type || options.TYPE,
        body: body,
        id: options._id
    })["catch"](function (err) {
        logger.log('error:service:elasticsearch:index', 'Error indexing using options:', options, 'with body:', body, 'error:\n\n', err);
        throw err;
    });
};

exports.searchExtractSource = function (results) {
    return _.pluck(results.hits.hits, '_source');
};


/**
 * Corresponds to `mget` and returns just the source of the users as an array
 *
 * @param {object} results  response
 * @returns {Array}
 */
exports.mgetExtractSource = function (results) {
    return results.docs.map(function (result) {
        return result._source;
    });
};


/**
 * Corresponds to `mget` and returns a JSON object mapping of each items { _id: _source }
 *
 * @param {object} results  response
 * @returns {Array}
 */
exports.mgetIdMap = function (results) {
    var obj;
    obj = {};
    _.each(results.docs, function (result) {
        obj[result._id] = result._source;
        return true;
    });
    return obj;
};
