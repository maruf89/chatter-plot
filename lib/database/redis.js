'use strict';

var redis = require('redis'),
    Q = require('q'),
    _ = require('lodash-node/modern'),
    logger = require('bragi'),

    Fetch = GLOBAL.Fetcher,

    config = Fetch.config('config', true, 'js'),
    publish = redis.createClient(config.redis.port, config.redis.host, {
        return_buffers: true
    }),
    subscribe = redis.createClient(config.redis.port, config.redis.host, {
        return_buffers: true
    }),

    client = redis.createClient(config.redis.port, config.redis.host, {
        return_buffers: true
    });

exports.publish = publish;
exports.subscribe = subscribe;
exports.client = client;

logger.log('init:service:redis', 'init');

client.on('ready', function () {
    logger.log('service:redis', 'ready');
    _.each(redis.RedisClient.prototype, function (func, key) {
        if (key.toLowerCase() === key && typeof func === 'function') {
            return client["p" + key] = Q.nbind(client[key], client);
        }
    });

    exports.onClientReady = Q.when(client);
    _.each(_onReadyDeferred, function (deferred) {
        deferred.resolve(client);
    });
    _onReadyDeferred = null;
});

/**
 * @description an array of deferred objects
 * @type {Array}
 * @private
 */
let _onReadyDeferred = [];

/**
 * @description until the client has all of the promise methods bounds, it will return a
 * promise that gets resolved when ready
 * - When client is ready, this fn will be replaced with Q.when()
 * @returns {*|promise}
 */
exports.onClientReady = function () {
    const deferred = Q.defer();

    _onReadyDeferred.push(deferred);

    return deferred.promise;
};
