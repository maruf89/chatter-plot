'use strict';

const _ = require('lodash-node/modern'),
    Q = require('q'),
    logger = require('bragi'),

    Fetch = GLOBAL.Fetcher;

logger.log('init:service:socket.io', 'init');

class SocketIo {
    init(instance, insecure) {
        let key = 'io' + (insecure ? 'Insecure' : '');
        this[key] = instance;
    }

    /**
     * @description Emits to specific userID
     *
     * @param  {number} userID
     * @param  {string} emitKey  The first argument for `emit`
     * @param  {*=}     data     Optional aribtrary data
     * @returns {Promise}
     */
    emitUser(userID, emitKey, data) {
        logger.log('service:SocketIo:emitUser', 'called userID: ', userID);

        if (typeof emitKey !== 'string') {
            let message = 'Expecting second argument of SocketIo#emitUser to be a string key';
            logger.log('error:service:SocketIo:emitUser', message);
            return Q.reject(500);
        }

        return Q.when(this.emitToChannel(String(userID), data, emitKey));
    }

    /**
     * @description Saves a user to our connections object
     *
     * @param  {Socket} socket
     * @param  {number} userID
     */
    saveConnection(socket, userID) {
        this._room('join', socket, userID);
    }

    /**
     * @description Disconnects any existing sockets
     *
     * @param  {number}  userID
     * @param  {*=} data - arbitrary data to pass to the client upon disconnecting
     */
    disconnectUser(userID, data) {
        logger.log('service:SocketIo:disconnectUser', 'called');

        let sock = this.io.to(String(userID));
        sock.emit('/disconnect/user', data);

        setTimeout(function () {
            _.each(sock.sockets, function (socket) {
                if (socket && socket.connected) {
                    return socket.disconnect();
                }
            });
        }, 5000);
    }

    emitToChannel(channel, data, emitKey, insecure) {
        const instance = this[insecure ? 'ioInsecure' : 'io'];

        instance.to(channel).emit(emitKey || channel, data);
    }

    generateChannelKey(channel) {
        return 'cl:' + channel;
    }

    /**
     * @description returns whether there are any socket connections for a room.
     * Works for checking if a user is connected - just pass their userID
     * @param {(string|number)} roomName
     * @returns {boolean} whether a socket room is empty
     */
    isRoomEmpty(roomName) {
        let room = this.io.adapter.rooms[roomName];
        return !(room && Object.keys(room).length);
    }

    /**
     * @description On token generation, saves the token to our connection object
     *
     * @param {string} socket - id of the socket
     */
    join(socket, channel) {
        this._room('join', socket, channel);
    }

    leave(socket, channel) {
        this._room('leave', socket, channel);
    }

    _room(action, socket, channel) {
        socket[action](channel);
    }
}

module.exports = new SocketIo();
