'use strict';

const SERVICE_NAME = 'VENUE';

const _ = require('lodash-node/modern');
const Q = require('q');
const logger = require('bragi');
const request = require('request');
const sequence = require('es-sequence');

const Fetch = GLOBAL.Fetcher;

const elasticsearch = Fetch.database('elasticsearch');
const esClient = elasticsearch.client;

const Constants = Fetch.config('misc/constants');

const db = Constants.elasticsearch[SERVICE_NAME];


/**
 * Google api endpoint to get the timezone info of a location
 * Set the timestamp to january first so that we have a consistent non-DST timestamp
 * @type {string}
 */
const timestampURL = 'https://maps.googleapis.com/maps/api/timezone/json?location=<lat>,<lon>' +
                     '&timestamp=' + ((new Date(2015, 0, 1)).getTime() / 1000);

/**
 * @class bTypeVenue
 */
class bTypeVenue {
    constructor() {
        logger.log('init:service:venue', 'init');
        this.name = SERVICE_NAME;
        this.db = db;
    }

    /**
     * @description gets multiple users from Elasticsearch
     * @name bTypeVenue#mget
     * @param {array<number>} IDs - user IDs
     * @param {object} options
     * @param {array<string>} options._source
     * @returns {Promise<object>}
     */
    mget(IDs, options) {
        options = options || {};

        var query = {
            index: db.INDEX,
            type: db.TYPE,
            body: {
                ids: IDs
            }
        };

        if (options._source) {
            query._source = options._source;
        }

        return esClient.mget(query);
    }

    get(vID, _source) {
        var query = {
            index: db.INDEX,
            type: db.TYPE,
            id: String(vID),
        };

        if (_source) {
            query._source = _source;
        }

        return esClient.get(query);
    }

    search(query) {
        logger.log('service:event:search', 'called');
        query.index = db.INDEX;

        if (!query.type) {
            query.type = db.TYPE;
        }

        return esClient.search(query)["catch"](function (err) {
            logger.log('error:service:event:search', 'Error searching for users using query:', query, '\nerror:\n', err);
            throw Constants.RESPONSE['500'];
        });
    }

    bulkFormatSave(preparedVenues) {
        return esClient.bulk({
            body: elasticsearch.bulkFormat(
                preparedVenues,
                'index',
                this.db.INDEX,
                this.db.TYPE,
                this.db._id,
                true
            )
        });
    }

    appendTimeZone(venue) {
        var deferred = Q.defer(),
            tzUrl = timestampURL.replace('<lat>', venue.coords.lat)
                .replace('<lon>', venue.coords.lon);

        request({
            url: tzUrl,
            json: true
        }, function (err, a, body) {
            if (err) {
                return deferred.reject(err);
            }

            venue.tz = parseInt(body.rawOffset, 10) / 60;
            return deferred.resolve(venue);
        });

        return deferred.promise;
    }

    /**
     * @description extends a venue object with it's own unique venue ID
     * @param  {object} venue - a single venue object
     * @return {Promise<object>} returns the venue with a `vID` property added
     */
    generateID(venue) {
        return sequence.get(db._id).then(function (vID) {
            venue.vID = vID;
            return venue;
        });
    }
};

module.exports = new bTypeVenue();
