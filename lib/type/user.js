/**
 * @module bTypeUser
 */

'use strict';

const SERVICE_NAME = 'USER';

const _ = require('lodash-node/modern');
const Q = require('q');
const logger = require('bragi');
const moment = require('moment');

const Fetch = GLOBAL.Fetcher;
const config = Fetch.config('config', true, 'js');
const sqlUser = Fetch.util('mysql', 'userHelper');
const esUser = Fetch.util('elasticsearch', 'userHelper');
const Util = Fetch.util('utility');
const SocketIo = Fetch.connection('socket.io');
const Mailchimp = Fetch.service('mailchimp');
const permissions = Fetch.service('permissions');
const Redis = Fetch.database('redis').client;
const Settings = Fetch.service('settings');
const DataBus = Fetch.service('DataBus');
const elasticsearch = Fetch.database('elasticsearch');

// Load Elasticsearch clien;
const esClient = elasticsearch.client;
const Constants = Fetch.config('misc/constants');
const db = Constants.elasticsearch[SERVICE_NAME];

const updateScript = 'if (ctx._source.<field> == null) { ctx._source.<field> = newField } else { ctx._source.<field> += newField }';
const commonUser = Fetch.common('type', 'user');

/**
 * Holds the name of the { <Service> : <MySQL Field Name> }
 * @type {Object}
 */
const serviceKeys = Fetch.config(['misc', 'authServiceKeys']);

const _deleteService = function (userData, serviceName) {
    var key, where;
    where = {};
    key = serviceKeys[serviceName];
    if (!(userData[key] && userData[key] !== "0")) {
        return true;
    }
    where[key] = userData[key];
    return sqlUser["delete"].from(serviceName, where);
};

const _deleteTasks = {
    elasticsearch: function (userData) {
        return esUser["delete"](userData.user_id);
    },
    mysql: function (userData) {
        return sqlUser["delete"].from('users', {
            user_id: userData.user_id
        });
    },
    facebook: _deleteService,
    google: _deleteService,
    linkedin: _deleteService
};

const _mysqlDependentTasks = ['mysql', 'facebook', 'linkedin', 'google'];

const NOTIFICATION_KEY = SERVICE_NAME;
const SOURCE = Fetch.config('misc/notificationTypes')[NOTIFICATION_KEY];
const SOURCE_DEFINITIONS = {};

SOURCE_DEFINITIONS[NOTIFICATION_KEY] = {
    "NEW": 1
};

const User = (function (superClass) {
    Util.extendClass(User, superClass);

    function User() {
        logger.log('init:service:user', 'init');
        var suspend = this.suspend.bind(this);

        this.db = db;

        this.name = SERVICE_NAME;
        this.SOURCE = SOURCE;
        this.NOTIFICATION_KEY = NOTIFICATION_KEY;
        this.SOURCE_DEFINITIONS = SOURCE_DEFINITIONS;

        this.on('/user/suspended', suspend);
    }

    User.prototype._fields = {
        insecureExclude: [
            'email',
            'following',
            'address',
            'accounts',
            'attending',
            'settings',
            'unreadNotification',
        ],

        insecureInclude: commonUser.insecureFields,

        genaralFields: [
            'userID',
            'picture',
            'firstName',
            'lastName',
        ]
    };

    /**##############################################
    #################    !CREATE    #################
    ###############################################*/


    /**
     * @description Save the user to the databases based on the data passed
     *
     * !IMPORTANT! - the passed in `userData` object will get modified. DO NOT CLONE IT!
     * @name bTypeUser#save
     * @param  {object} userData - Elasticsearch user document. Ready to be stored as is.
     * @param  {object} otherData - Depending on what fields are available, will save to additional places
     * @param  {string} otherData.password - Will hash the password and save the user into the users table
     * @param  {array<object>} otherData.services
     * @param  {string} otherData.services[].type - the service name (facebook|google|linkedin)
     * @param  {string} otherData.services[].id - the user id of the user's account associated with the service
     * @param  {string} otherData.services[].accessToken - token associated with account connection
     * @param {number} otherData.permissions - Users permissions
     * @return {Promise<number>} returns the newly created userID
     */
    User.prototype.save = function (userData, otherData) {
        logger.log('service:user:save', 'called');

        otherData = otherData || {};

        let userCall = null,
            serviceCalls = [],
            sqlInsert = {
                email: userData.email
            },

            serviceInserts = null,
            userID = userData.userID;

        // Set user's join date
        userData.joinDate = moment().format('YYYYMMDDThhmmss.SSSZ');

        // Create an object for future events
        userData.attending = {
            events: []
        };

        userData.favorite = {
            all: [] // default favorites folder
        };

        // Cannot be empty
        userData.accounts = userData.accounts || [];

        // Hash Password
        if (otherData.password) {
            sqlInsert.password = Util.hashPassword(otherData.password);
        }

        // User Permissions
        if (typeof otherData.permissions === 'number') {
            sqlInsert.role = otherData.permissions;
        } else {
            let curUserGroup = otherData.userGroup || config.defaults.permissions.default;
            let userGroup = permissions.getUserGroups()[curUserGroup];
            sqlInsert.group_id = userGroup.id;
            sqlInsert.role = userGroup.permissions;
        }

        // Create Settings
        userData.settings = _.merge(Settings.generateSettings(userData.settings), userData.settings || {});

        // Push each services id
        if (Array.isArray(otherData.services) && otherData.services.length) {
            // If user signs up with social media, their account is already activated/verified
            sqlInsert.activated = 1;

            _.each(otherData.services, function (service) {
                var serviceInsert,
                    keyName = serviceKeys[service.type];

                // Store service user's ID in `users` table
                sqlInsert[keyName] = service.id;

                // Create service row data
                serviceInsert = {
                    access_token: service.accessToken
                };

                serviceInsert[keyName] = service.id;

                // Format to add to Elasticsearch
                if (!Array.isArray(userData.accounts)) {
                    userData.accounts = [];
                }

                userData.accounts.push({
                    type: service.type,
                    id: service.id
                });

                logger.log('req:service:user:save', "Adding user to MySQL `" + service.type + "` table");
                return serviceCalls.push(sqlUser.insert.intoTable(serviceInsert, service.type));
            });
        }

        // Only add to MySQL `users` table if we don't have a userID
        if (!userID) {
            logger.log('req:service:user:save', 'Adding user to MySQL `users` table');
            userCall = sqlUser.insert.intoTable(sqlInsert, 'users');
        } else {
            userCall = Q.when(userID);
        }

        if (serviceInserts) {
            Q.all(serviceCalls)["catch"](function (err) {
                return logger.log('error:service:user:save', 'Error saving user to service using provided:', serviceInserts, err);
            });
        }

        return userCall.then(function (id) {
            logger.log('res:service:user:save', 'User saved to MySQL');
            userID = parseInt(id, 10);
            userData.userID = userID;
            logger.log('req:service:user:save', 'Saving user in Elasticsearch');

            return esUser.addUser(userData, userID);
        }, function (err) {
            logger.log('error:service:user:save', 'Error saving user to `users` table in MySQL using:', sqlInsert, err);
            throw false;
        }).then(function () {
            logger.log('res:service:user:save', 'User saved to Elasticsearch');
            userData.notActivated = !sqlInsert.activated;
            userData.permissions = sqlInsert.role;
            this.emit('/user/saved', userData);
            return userData;
        }.bind(this));
    };

    /**##############################################
     #################     !READ     #################
     ###############################################*/

    /**
     * @description Fetches the user's data
     * @name bTypeUser#get
     * TODO: build secure _source for mysql fields to get
     * @param  {number}  userID
     * @param  {object=} options Optional get options
     * @param  {number}  from - Enumerable: 0/null/false = Elasticsearch  :  1 = MySQL  : 2 = Both (default:Elasticsearch)
     * @return {Promise}         Promise contains user object
     * @deprecated
     */
    User.prototype.get = function (userID, options, from) {
        // Get Elasticsearch (default)
        if (!from) {
            return this.getInsecure(userID, options);
        }

        // Get just MySQL
        if (from === 1) {
            return sqlUser.get.userByID(userID, options);
        }

        // Get both Elasticsearch + MySQL
        return Q.all([
            this.getInsecure(userID, options),
            sqlUser.get.userByID(userID, options)
        ]).then(function (arg) {
            var es = arg[0],
                sql = arg[1];

            return _.extend(es, sql);
        });
    };

    User.prototype.getSecure = sqlUser.get.from;

    /**
     * @description gets a user from Elasticsearch
     * @name bTypeUser#getInsecure
     * @param {(number|array<number>)} userID - if passed an array, must only contain 1 id
     * @param {object} options
     * @param {array<string>} options._source
     * @returns {Promise<object>}
     */
    User.prototype.getInsecure = function (userID, options) {
        options = options || {};

        var query = {
            index: this.db.INDEX,
            type: options.type || this.db.TYPE,
            id: String(userID)
        };

        if (options._source) {
            query._source = options._source;
        }

        return esClient.get(query);
    };

    /**
     * @description gets multiple users from Elasticsearch
     * @name bTypeUser#mget
     * @param {array<number>} IDs - user IDs
     * @param {object} options
     * @param {array<string>} options._source
     * @returns {Promise<object>}
     */
    User.prototype.mget = function (IDs, options) {
        options = options || {};

        var query = {
            index: this.db.INDEX,
            type: options.type || this.db.TYPE,
            body: {
                ids: IDs
            }
        };

        if (options._source) {
            query._source = options._source;
        }

        return esClient.mget(query);
    };

    User.prototype.searchSecure = sqlUser.get.from


    /**
     * @description Does a search for users in Elasticsearch
     * @name bTypeUser#search
     * @param  {object} query - Elasticsearch query
     * @return {Promise<object>} returns the search query results
     */
    User.prototype.search = function (query) {
        logger.log('service:user:search', 'called');
        query.index = db.INDEX;
        if (query.type === void 0) {
            query.type = db.TYPE;
        }
        return esClient.search(query)["catch"](function (err) {
            logger.log('error:service:user:search', 'Error searching for users using query:', query, '\nerror:\n', err);
            throw Constants.RESPONSE['500'];
        });
    };


    /**
     * @description Does a multi search for users in Elasticsearch
     * @name bTypeUser#msearch
     * @param  {array} query - Elasticsearch bulk formatted queries
     * @return {Promise<object>} returns the search query results
     */
    User.prototype.msearch = function (query) {
        logger.log('service:user:msearch', 'called');
        query.index = db.INDEX;

        if (query.type === void 0) {
            query.type = db.TYPE;
        }

        return esClient.msearch(query)["catch"](function (err) {
            logger.log('error:service:user:msearch', 'Error searching for users using query:', query, '\nerror:\n', err);
            throw Constants.RESPONSE['500'];
        });
    };

    /**
     * @description get a users group + permissions
     * @param {number} userID
     * @returns {Promise<object>} return
     * @returns {string}          return.group - the group id as a string
     * @returns {number}          return.permissions - the users permissions
     */
    User.prototype.getPermissionsGroup = function (userID) {
        logger.log('req:type:user:getPermissions', 'Fetching user\'s permissions');

        return sqlUser.get.from(null, { user_id: userID }, ['role', 'group_id'])

            .then(function (row) {
                var group = permissions.indexToUserGroup(row.group_id);

                // only return if it exists
                if (group) {
                    return {
                        group: group,
                        permissions: row.role && parseInt(row.role, 10)
                    }
                }

                throw false;
            })
            .catch(function (err) {
                // If not found
                if (err === 404) {
                    /**
                     * TEMPORARY - only in here until we finish transitioning `userGroup` from Elasticsearch to MySQL
                     *
                     * returns the `non_activated`
                     */
                    let curUserGroup = config.defaults.permissions.default;
                    return permissions.getUserGroups()[curUserGroup];
                }

                throw err;
            })
    };

    User.prototype.getPermissions = function (userID) {
        logger.log('req:type:user:getPermissions', 'Fetching user\'s permissions');

        return sqlUser.get.from(null, { user_id: userID }, ['role']).then(function (row) {
            return parseInt(row.role, 10);
        });
    };


    /**
     * @description Verifies an email/password combination
     * TODO: Make this more general instead of just testing user authentication
     * @name bTypeUser#verify
     * @param  {object} credentials - Containing `password` and `email`
     * @return {Promise<object>}      Containing `userID`, `permissions` and `activated`
     */
    User.prototype.verify = function (credentials) {
        logger.log('service:user:verify', 'called');

        var where = _.clone(credentials);
        where.password = Util.hashPassword(credentials.password);

        return sqlUser.get.from('users', where, ['user_id AS userID', 'role AS permissions', 'activated'])
        .catch(function (err) {
            // 404 is the expected not found response
            if (err !== 404) {
                logger.log('error:controller:user:login', 'Error verifying user credentials\nerror:\n', err);
                throw 500;
            }

            throw err;
        })

        .then(function (args) {
            return args;
        });
    };

    /**
     * @description Checks whether the user with the passed in userID is suspended
     * @name bTypeUser#isSuspended
     * @param  {number}   userID     user to check
     * @param  {boolean}  redisOnly  Whether to only check Redis
     * @return {boolean}             Promise containing true if the user is suspended
     */
    User.prototype.isSuspended = function (userID, redisOnly) {
        logger.log('service:user:isSuspended', 'called');
        if (redisOnly) {
            return Redis.pget("suspend:user:" + userID);
        }

        logger.log('req:service:user:isSuspended', 'Fetching user\'s `active` value from MySQL');
        return sqlHelper.get.from('users', {
            user_id: userID
        }, 'active').then(function (row) {
            logger.log('res:service:user:isSuspended', 'Received `active` field');
            return !row.active;
        }, function (err) {
            logger.log('error:service:user:isSuspended', 'Error fetching user\'s `active` field', err);
            throw false;
        });
    };


    /**
     * @description Checks Elasticsearch for any 3rd party account credentials
     * If none found, we know they signed up by e-mail and password IS required
     * (Otherwise they would have no way to log in)
     * @name bTypeUser#requiresPassword
     * @param  {number} userID
     * @return {Promise}         Promise returns {boolean} whether it's required
     */
    User.prototype.requiresPassword = function (userID) {
        logger.log('req:service:user:requiresPassword', 'Fetching user from Elasticsearch');
        return esUser.retrieveUserById(userID, {
            _source: 'accounts'
        }).then(function (resp) {
            logger.log('res:service:user:requiresPassword', 'Fetched user from Elasticsearch');
            var accounts = resp.accounts;
            return !(Array.isArray(accounts) && accounts.length);
        });
    };

    User.prototype.update = function (userID, updates) {
        return this.scriptUpdate({
            _id: userID
        }, updates);
    };


    User.prototype.updateScript = function (userID, opts) {
        return this.scriptUpdate({
            _id: userID
        }, null, true, null, opts);
    };

    /**
     * @description Updates a user document
     * @name bTypeUser#scriptUpdate
     * @example
     *     // Update the users email on Elasticsearch's `mailing` type
     *     esUser.updateUser({ _id: 12, type: 'mailing' }, { email: 'mat@gmail.com' })
     *
     *     // Add additional values to the `accounts` field (Adding another `true` parameter, would replace the existing)
     *     esUser.updateUser('12', { type: 'facebook_id', id: 'AAAW3EANT0TH2...' }, 'accounts')
     *
     * @param  {(object|string)} types - if not object - must be _id of the doc. else -
     * @param  {string} types._id - the Elasticsearch documents id
     * @param  {string=} types.index - optional database index
     * @param  {string=} types.type - optional database type
     * @param  {object} replacement - data to update
     * @param  {string=} arrayField - if field to update is an array then the field's name
     * @param  {boolean=} replaceArray if arrayField is set, and the field should completely replace the entry instead of appending to it
     * @return {Promise}
     * @deprecated
     */
    User.prototype.scriptUpdate = function (types, replacement, arrayField, replaceArray, opts) {
        opts = opts || {};

        var body, query;

        logger.log('type:user:scriptUpdate', 'Updating user');
        if (!_.isPlainObject(types)) {
            types = {
                _id: types
            };
        }

        // Check if we're appending to a field
        if (arrayField && !replaceArray) {
            body = {
                script: opts.script || updateScript.replace(/<field>/g, arrayField),
                params: opts.params || {
                    newField: [replacement]
                }
            };
        } else {
            body = {
                doc: replacement
            };
        }

        query = {
            index: types.index || this.db.INDEX,
            type: types.type || this.db.TYPE,
            id: String(types._id),
            body: body
        };

        return esClient.update(query)
        ["catch"](function (err) {
            logger.log('error:type:user:scriptUpdate', 'Error updating user using types:', types, 'replacement:\n', replacement, '\narrayField:', arrayField, 'replaceArray set to:', !!replaceArray, 'and query:\n', query, '\n', err);
            throw err;
        });
    };

    User.prototype.updateSecure = sqlUser.update.userWithFields;

    User.prototype.updatePermissions = function (userID, newPermissions) {
        return sqlUser.update.userWithFields({
            role: newPermissions
        }, {
            user_id: userID
        }, 'users');
    };

    /**##############################################
    #################    !DELETE    #################
    ###############################################*/

    /**
     * @description Deletes a user from the any/all of the available locations
     * @name bTypeUser#delete
     * @param  {number} userID
     * @param  {object} opts        More specific options
     * @option opts {array} tasks   Services to delete the user from
     * @option opts {string} source Where the call originated from
     * @return {Deferred}           On success returns the userID of the deleted user
     */
    User.prototype["delete"] = function (userID, opts) {
        opts = opts || {};

        logger.log('service:user:delete', 'called');

        const deferred = Q.defer();
        let userData = {
            user_id: userID
        };
        const tasks = opts.tasks || ['elasticsearch', 'mysql', 'facebook', 'linkedin', 'google'];

        if (!tasks.length) {
            logger.log('error:service:user:delete', 'Tasks list is empty. There is nothing we can delete the user from, userID:', userID);
            deferred.reject({
                type: 'updates.ERROR-INVALID_REQUEST',
                response: 400
            });
        }

        // Fetch user data + at the same time check if exists in MySQL
        logger.log('req:service:user:delete', 'Fetching user from MySQL');
        Q.all([esUser.retrieveUserById(userID), sqlUser.get.from('users', userData)])
            .then(function (arg) {
                var sqlUser, user;
                user = arg[0], sqlUser = arg[1];
                logger.log('res:service:user:delete', 'Returned user from MySQL');
                userData = _.extend(user, sqlUser, userData);

                return tasks;
            }).catch(function (err) {
                if (err) {
                    logger.log('error:service:user:delete', 'Error fetching user from MySQL for user', userID, 'error:\n\n', err);
                    throw err;
                }
                logger.log('error:service:user:delete', 'No MySQL information found for userID:', userID, 'Removing MySQL dependent tasks');
                _.each(_mysqlDependentTasks, function (task) {
                    var index;
                    if (index = ~tasks.indexOf(task)) {
                        return tasks.splice(~index, 1);
                    }
                });
                return tasks;
            })

            .then(function (tasks) {
                var errors, run;
                if (!tasks.length) {
                    logger.log('error:service:user:delete', 'Tasks list is empty. There is nothing we can delete the user from, userID:', userID);
                    deferred.reject({
                        type: 'updates.ERROR-INVALID_REQUEST',
                        response: 400
                    });
                }
                errors = [];
                run = [];
                logger.log('req:service:user:delete', 'Deleting user from the possible tasks');
                _.each(tasks, function (task) {
                    run.push(_deleteTasks[task](userData, task));
                    return true;
                });

                return Q.allSettled(run).then(function (results) {
                    let disconnectData;
                    let response = null;

                    _.each(results, function (result, index) {
                        if (result.state !== 'fulfilled') {
                            return errors.push({
                                task: tasks[index],
                                reason: result.reason
                            });
                        }
                    });

                    if (errors.length) {
                        logger.log('error:service:user:delete', 'Error deleting user.', errors);
                        return deferred.reject({
                            type: 'updates.ERROR-SERVER_ERROR',
                            response: 500
                        });
                    }

                    logger.log('res:service:user:delete', 'Successfully deleted', userID);
                    DataBus.emit('/user/deleted', {
                        source: opts.source,
                        userID: userID,
                        userData: userData,
                    });

                    disconnectData = {
                        source: opts.source,
                        title: 'updates.ACCNT_DELETED'
                    };
                    disconnectData.body = opts.source === 'admin' ? 'updates.ADMIN-USER_DELETED' : 'updates.ACCNT_DELETED_BODY';
                    SocketIo.disconnectUser(userID, disconnectData);
                    return deferred.resolve(userID);
                });
            })
            .catch(function (err) {
                logger.log('error:type:user:delete', 'Error deleting user\nerror:\n', err);
                throw err;
            });
        return deferred.promise;
    };


    /**
     * @description Given a userID marks the user as suspended in Redis for the token duration
     * (Until all possible tokens expire) and marks the user as inactive in MySQL
     * @name bTypeUser#suspend
     * @param {number} userID
     */
    User.prototype.suspend = function (userID) {
        Redis.set("suspend:user:" + userID, 1, 'PX', config.token.ttl);
        logger.log('req:service:user:suspend', 'suspending user');

        return sqlUser.update.userWithFields({
            active: 0
        }, {
            user_id: userID
        }, 'users')["catch"](function (err) {
            logger.log('error:service:user:suspend', 'Error deactivating userID:', userID, err);
            throw false;
        });
    };

    return User;
})(require('events').EventEmitter);

module.exports = new User();
