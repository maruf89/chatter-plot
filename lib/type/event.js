'use strict';

var SERVICE_NAME = 'EVENT',

    _ = require('lodash-node/modern'),
    Q = require('q'),
    logger = require('bragi'),
    sequence = require('es-sequence'),
    moment = require('moment'),

    Fetch = GLOBAL.Fetcher,

    config = Fetch.config('config', true, 'js'),
    mandrillKey = Fetch.config('templates/mandrillKey', true, 'js')[SERVICE_NAME],
    Constants = Fetch.config('misc/constants'),
    db = Constants.elasticsearch[SERVICE_NAME],
    Mandrill = Fetch.service('mandrill'),
    DataBus = Fetch.service('DataBus'),
    elasticsearch = Fetch.database('elasticsearch'),
    notificationCtrl = Fetch.controller('notification'),
    esClient = elasticsearch.client,
    esUser = Fetch.util('elasticsearch', 'userHelper'),
    crypter = Fetch.util('encryptDecrypt'),

    languageHelper = Fetch.util('elasticsearch', 'languageHelper'),
    utilSearchHelper = Fetch.util('elasticsearch', 'helper'),
    utilEventSearch = Fetch.util('elasticsearch', 'event', 'index'),
    Util = Fetch.util('utility'),

    /**
     * What type of event notification to use.
     * Corresponds to `sourceType` in `notifications.json`
     */
    _notificationSourceTypes = {
        "NEW": 1,
        "UPDATED": 2,
        "DELETED": 3,
        "OPEN_SPOT": 4,
        "REMINDER": 5
    },

    SOURCE_DEFINITIONS = {
        'EVENT': _notificationSourceTypes,
        'LISTING': _notificationSourceTypes
    },

    /**
     * Returns a formatted time (ignoring the timezone since each server may have it's own Timezone)
     *
     * @param {string}   time   elasticsearch formatted string
     */
    _formatDate = function (time) {
        time = time.substr(0, 15);
        return moment(time, 'YYYYMMDDTHHmmss').format('MMMM Do YYYY, h:mm a');
    };

var EventClass = (function (superClass) {
    Util.extendClass(Event, superClass);

    function Event() {
        logger.log('init:service:event', 'init');

        this.name = SERVICE_NAME;
        this.eventID = db._id;
        this.db = db;

        /**
         * Use EventClass.sourceDefinitions instead
         * @type {object}
         * @deprecated
         */
        this.SOURCE_DEFINITIONS = SOURCE_DEFINITIONS;
        this.notificationTypes = _notificationSourceTypes;

        // List of all the email types that a user can directly email with
        this.allowedUserEmailTypes = Object.freeze([
            "NOTIFY_ATTENDEES"
        ]);
    }

    Event.prototype.get = function (eID, opts) {
        if (opts == null) {
            opts = {};
        }
        var query = {
            index: db.INDEX,
            type: opts.type || db.TYPE,
            id: String(eID)
        };

        if (opts._source) {
            query._source = opts._source;
        }

        return esClient.get(query);
    };

    Event.prototype.mget = function (eIDs, opts) {
        opts = opts || {};

        var query = {
            index: db.INDEX,
            type: opts.type || db.TYPE,
            body: {
                ids: eIDs
            }
        };

        if (opts._source) {
            query._source = opts._source;
        }

        return esClient.mget(query);
    };

    Event.prototype.mgetDocs = function (docs, opts) {
        opts = opts || {};

        var query = {
            index: db.INDEX,
            body: {
                docs: docs
            }
        };

        if (opts._source) {
            query._source = opts._source;
        }

        return esClient.mget(query);
    };

    /**
     * Method creates an Event
     *
     * @param  {object} eventData  Contains any of the fields found in `ES_DATA/mappings/event.json`
     * @return {Promise}           Returns the entire event object including it's `eID` (event ID)
     */

    Event.prototype.create = function (eventData, opts) {
        opts = opts || {};

        logger.log('req:service:event:create', 'Getting event\'s sequence ID');
        return sequence.get(db._id).then(function (eID) {
            eventData[db._id] = eID;
            utilEventSearch.prepareEvent(eventData);
            logger.log('req:service:event:create', 'Saving venues to Elasticsearch');
            return elasticsearch.index(eventData, {
                index: 'events',
                type: opts.type || 'event',
                _id: String(eID),
            });
        })

        .then(function () {
            logger.log('res:service:event:create', 'Successfully saved event');
            return eventData;
        }).catch(function (err) {
            logger.log('error:service:event:create', 'Error saving event:', eventData, '\nerror:\n', err);
            throw Constants.RESPONSE['500'];
        });
    };

    Event.prototype.update = function (eID, eventUpdates, opts) {
        logger.log('req:service:event:update', 'called and updating event');
        utilEventSearch.prepareEvent(eventUpdates);
        return esClient.update({
            index: db.INDEX,
            type: (opts && opts.type) || db.TYPE,
            id: String(eID),
            body: {
                doc: eventUpdates
            }
        })["catch"](function (err) {
            logger.log('error:service:event:update', 'Error updating event', eID,
                                                            'with:\n', eventUpdates,
                                                            '\ntype:', opts.type,
                                                            '\nerror:', err);
            throw err;
        });
    };

    Event.prototype.msearch = function (query) {
        logger.log('service:event:msearch', 'called');
        query.index = db.INDEX;

        if (query.type === void 0) {
            query.type = db.TYPE;
        }
        return esClient.msearch(query)["catch"](function (err) {
            logger.log('error:service:event:msearch', 'Error searching for users using query:', query, '\nerror:\n', err);
            throw Constants.RESPONSE['500'];
        });
    };

    Event.prototype.search = function (query) {
        logger.log('service:event:search', 'called');
        query.index = db.INDEX;

        if (query.hasOwnProperty('type') && query.type === void 0) {
            query.type = '';
        }

        return esClient.search(query)["catch"](function (err) {
            logger.log('error:service:event:search', 'Error searching for users using query:', query, '\nerror:\n', err);
            throw Constants.RESPONSE['500'];
        });
    };


    /**
     * Guess what this does.
     *
     * @param  {number}  eID      event id
     * @param  {Object=} options  can contain a different index `type` property
     * @return {Promise}
     */

    Event.prototype["delete"] = function (eID, options) {
        options = options || {};

        logger.log('util:userHelper-elasticsearch:delete', 'deleting event');
        return esClient["delete"]({
            index: db.INDEX,
            type: options.type || db.TYPE,
            id: String(eID)
        });
    };

    Event.prototype.deleteQuery = function (opts, filterTypes) {
        var query;
        logger.log('service:event:deleteQuery', 'called');
        if (!(Array.isArray(filterTypes) && filterTypes.length)) {
            throw new Error('expecting querytypes to have something to match');
        }
        query = utilSearchHelper.buildSearch('event', null, filterTypes, null, opts);
        query = {
            index: db.INDEX,
            body: query
        };
        if (opts.type) {
            query.type = opts.type;
        }
        logger.log('req:service:event:deleteQuery', 'deleting events in Elasticsearch');
        return esClient.deleteByQuery(query)["catch"](function (err) {
            logger.log('error:service:event:deleteQuery', 'Error deleting events using:', opts, 'query:', query, '\nerror:\n', err);
            throw Constants.RESPONSE['500'];
        });
    };




    /**
     * Deletes all of a users events that have yet to occur
     *
     * @param  {object} user         contains:
     * @option user {number} userID
     */

    Event.prototype.deleteUsersEvents = function (user) {
        var query, self, sent;
        self = this;
        sent = false;
        query = utilSearchHelper.buildSearch('event', null, ['hosting', 'current'], null, {
            opts: {
                hosting: user.userID
            }
        });

        logger.log('req:service:event:deleteUsersEvents', 'Deleting user\'s events, first fetching all events');
        return esClient.search({
            index: db.INDEX,
            size: 250,
            body: query,
            _source: ['goingList', 'waitList', 'eID', 'name', 'venue', 'when']
        })

        .then(function (events) {
            logger.log('res:service:event:deleteUsersEvents', 'Got all relevant events, going to notify attending users');
            var eventIDs = [];

            _.each(events.hits.hits, function (event) {
                eventIDs.push(event._id);
                return self.notifyUsers(event._source, {
                    exclude: [user.userID],
                    type: 'DELETED',
                    reason: 'USER_DELETED'
                });
            });

            logger.log('req:service:event:deleteUsersEvents', 'deleting all of users events');
            return self.deleteQuery({
                opts: {
                    hosting: user.userID
                }
            }, ['hosting']);
        }, function (err) {
            logger.log('error:service:event:deleteUsersEvents', 'Error fetching events for user', user, '\nerror:\n', err);
            throw sent = Constants.RESPONSE['500'];
        })

        .then(null, function (err) {
            if (sent) {
                throw err;
            }
            logger.log('error:service:event:deleteUsersEvents', 'Error deleting events for user', user, '\nerror:\n', err);
            throw sent = Constants.RESPONSE['500'];
        });
    };





    /**
     * Updates an event moving any users in the waiting list into open spots in the going list
     *
     * @param  {number} eID  event ID
     * @return {Promise}
     */
    Event.prototype.updateAttendance = function (eID) {
        var Event, getOptions, luckyPeeps, self, sent;
        self = this;
        sent = null;
        luckyPeeps = null;
        Event = null;
        getOptions = {
            source: ['goingList', 'waitList', 'maxGoing', 'eID', 'when', 'venue']
        };
        logger.log('req:service:event:updateAttendance', 'called - fetching event');
        return self.get(eID, getOptions).then(function (_Event) {
            var going, openSpots, waiting;
            Event = _Event;
            if (!Event.maxGoing) {
                return sent = true;
            }
            if (!Array.isArray(Event.waitList) || !(waiting = Event.waitList.length)) {
                return sent = true;
            }
            going = Event.goingList.length;
            openSpots = Event.maxGoing - going;
            if (openSpots <= 0) {
                return sent = true;
            }
            luckyPeeps = Event.waitList.splice(0, openSpots);
            Event.goingList = Event.goingList.concat(luckyPeeps);
            logger.log('req:service:event:updateAttendance', 'Moving', openSpots, 'users from wait list to going');
            return self.update(eID, {
                goingList: Event.goingList,
                waitList: Event.waitList
            });
        }).then(function () {
            if (sent) {
                return sent;
            }
            logger.log('req:service:event:updateAttendance', 'Successfully moved users, now notifying the new users');
            return self.notifyUsers(Event, {
                type: 'OPEN_SPOT',
                sourceIDs: luckyPeeps
            });
        }, function (err) {
            if (sent) {
                throw sent;
            }
            logger.log('error:service:event:updateAttendance', 'Error updating the lists\nerror:', err);
            throw err;
        });
    };

    Event.prototype.util = utilEventSearch.util;


    /**
     * Notifies the attending users of an event of something...
     *
     * @param  {object}       event          the full event object
     * @param  {object}       data           extra parameters
     * @option data {string}  type           refers to template details in `mandrillKey.js` under `EVENT`
     * @option data {Array=}  exclude        userID's to exclude
     * @option data {Array=}  sourceIDs      where to read userID's from
     * @option data {String=} notifyMessage  message to pass as `NOTIFY_MESSAGE` variable
     * @option data {String=} reason         all-caps single word reason
     * @return {Promise}
     * @deprecated
     */
    Event.prototype.notifyUsers = function (event, data) {
        var emailOpts, eventType, mandrillOptions, sent, sourceIDs, to, userIDs;
        if (data == null) {
            data = {};
        }
        logger.log('service:event:notifyUsers', 'called');
        if (!data.type) {
            logger.log('error:service:event:notifyUsers', 'Expecting data to contain a `type` parameter');
            return Q.reject(false);
        }
        emailOpts = mandrillKey[data.type];
        sourceIDs = Array.isArray(data.sourceIDs) ? data.sourceIDs : event.goingList;
        userIDs = Array.isArray(data.exclude) ? _.xor(sourceIDs, data.exclude) : sourceIDs;
        sent = false;

        eventType = event.indexType;
        if (!eventType) {
            eventType = event.creator ? 'event' : 'listing';
        }

        // if noone to notify then return
        if (!userIDs.length) {
            logger.log('service:event:notifyUsers', 'nobody to notify');
            return Q.resolve(false);
        }
        to = [];
        mandrillOptions = {
            track_clicks: true,
            subject: emailOpts.subject,
            global_merge_vars: [
                {
                    name: 'EVENT_ID',
                    content: event.eID
                }, {
                    name: 'EVENT_TITLE',
                    content: event.name
                }, {
                    name: 'EVENT_DATE',
                    content: _formatDate(event.when)
                }, {
                    name: 'EVENT_ADDRESS',
                    content: Util.getAddress(event.venue)
                }, {
                    name: 'EVENT_LOCATION',
                    content: Util.eventToGMapUrl(event.venue)
                },
            ]
        };
        if (data.notifyMessage) {
            mandrillOptions.global_merge_vars.push({
                name: 'NOTIFY_MESSAGE',
                content: data.notifyMessage
            });
        }
        if (Array.isArray(data.extendGlobal)) {
            [].push.apply(mandrillOptions.global_merge_vars, data.extendGlobal);
        }

        // Format the merge vars
        logger.log('req:service:event:notifyUsers', 'Fetching affected users');
        return esUser.mget(userIDs, {
            _source: ['firstName', 'lastName', 'email', 'settings.locale']
        }).then(elasticsearch.mgetExtractSource).then(function (users) {
            logger.log('res:service:event:notifyUsers', 'got affected users');
            to = [];

            let getSubdomain = function (user) {
                let locale = user.settings && user.settings.locale,
                    subdomain = config.prefix;

                if (locale) {
                    let substr = locale.substr(0, 2);
                    if (config.subDomains.indexOf(substr) !== -1) {
                        subdomain = substr;
                    }
                }

                return subdomain;
            };

            mandrillOptions.merge_vars = users.reduce(function (prev, user) {
                if (!user) {
                    return prev;
                }
                to.push({
                    email: user.email,
                    name: user.firstName + ' ' + user.lastName
                });

                // mergeVars.push
                prev.push({
                    rcpt: user.email,
                    vars: [
                        {
                            name: 'FNAME',
                            content: user.firstName
                        }, {
                            name: 'emailKey',
                            content: crypter.encrypt(user.email)
                        }, {
                            name: 'env',
                            content: getSubdomain(user)
                        },
                    ]
                });
                return prev;
            }, []);
            logger.log('req:service:event:notifyUsers', 'sending emails');
            return Q.all([
                Mandrill.sendTemplate(emailOpts.template, [], to, mandrillOptions),
                notificationCtrl.notify(userIDs, eventType.toUpperCase(), event.eID, {
                    sourceType: _notificationSourceTypes[data.type],
                    snippet: event.name
                })
            ]);
        }).then(function () {
            logger.log('res:service:event:notifyUsers', 'successfully notified the users');
            return Constants.RESPONSE['200'];
        }, function (err) {
            logger.log('error:service:event:notifyUsers', 'error notifying users using template:', emailOpts.template, 'data:', data, '\n mandrillOptions:', mandrillOptions, '\nfor eID:', event.eID, '\nto:', to, '\nerror:\n', err);
            throw err;
        });
    };

    return Event;

})(require('events').EventEmitter);

module.exports = new EventClass();
