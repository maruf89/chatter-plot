
'use strict';

const SERVICE_NAME = 'MESSAGE';

const _ = require('lodash-node/modern');
const Q = require('q');
const logger = require('bragi');
const moment = require('moment');
const sequence = require('es-sequence');

const Fetch = GLOBAL.Fetcher;

const config = Fetch.config('config', true, 'js');
const Constants = Fetch.config('misc/constants');

const db = Constants.elasticsearch[SERVICE_NAME];
const elasticsearch = Fetch.database('elasticsearch');
const esClient = elasticsearch.client;
const esUser = Fetch.util('elasticsearch', 'userHelper');
const Util = Fetch.util('utility');

const NOTIFICATION_KEY = 'INTERACTIONS';
const SOURCE = Fetch.config('misc/notificationTypes')[NOTIFICATION_KEY];
const SOURCE_DEFINITIONS = {};

const REQUEST = 'REQUEST';

SOURCE_DEFINITIONS[NOTIFICATION_KEY] = {
    "MESSAGE": 1,
    "REQUEST_TANDEM": 2,
    "REQUEST_TEACHER": 3,
    "REQUEST_TANDEM_ACCEPT": 4,
    "REQUEST_TANDEM_DENY": 5,
    "REQUEST_TEACHER_ACCEPT": 6,
    "REQUEST_TEACHER_DENY": 7,
    "REQUEST_TANDEM_REMINDER": 8,
    "REQUEST_TEACHER_REMINDER": 9,
};

// Ensure that the object is not editable
Object.freeze(SOURCE_DEFINITIONS[NOTIFICATION_KEY]);

class Interaction {
    constructor() {
        logger.log('init:service:message', 'init');
        this.name = SERVICE_NAME;

        this.SOURCE = SOURCE;
        this.NOTIFICATION_KEY = NOTIFICATION_KEY;
        this.SOURCE_DEFINITIONS = SOURCE_DEFINITIONS;
        this.db = db;

        this.interactionStatuses = [
            'accept',
            'deny',
            'cancel',
            'edit',
        ];

        Object.freeze(this.interactionStatuses);
    }

    get(msgID, opts) {
        logger.log('req:service:message:fetch', 'fetching conversation parent');

        const TYPE = opts.type ? opts.type.toUpperCase() : 'MESSAGE';

        let query = {
            index: this.db.INDEX,
            type: this.db[TYPE],
            id: msgID
        };

        if (opts && opts._source) {
            query._source = opts._source;
        }

        return esClient.get(query);
    }

    getMessages(msgID, opts) {
        opts = opts || {};

        logger.log('req:service:message:fetch', 'fetching conversation log');
        let query = {
            index: this.db.INDEX,
            type: this.db.CHILD,
            size: opts.size || 10,
            from: opts.from || 0,
            sort: 'timestamp:desc',
            body: {
                query: {
                    filtered: {
                        filter: {
                            term: {
                                "_parent": msgID
                            }
                        }
                    }
                }
            }
        };

        if (opts._source) {
            query._source = opts._source;
        }

        return esClient.search(query);
    }

    search(query, opts) {
        logger.log('type:interaction:search', 'called');
        query.index = this.db.INDEX;
        query.type = this.db[opts.type.toUpperCase()];

        return esClient.search(query)["catch"](function (err) {
            logger.log('error:service:user:search', 'Error searching for users using query:', query, '\nerror:\n', err);
            throw Constants.RESPONSE['500'];
        });
    }

    /**
     * @param opts
     * @returns {*}
     * @deprecated in favor of search
     */
    fetch(opts) {
        logger.log('req:service:message:fetch', 'fetching user\'s messages');

        const TYPE = opts.type ? opts.type.toUpperCase() : 'MESSAGE';

        let query = {
            index: this.db.INDEX,
            type: this.db[TYPE],
            from: opts.from || 0,
            size: opts.size || 10,
            body: {
                "query": {
                    "filtered": {
                        "filter": {
                            "term": {
                                "participants": opts.userID
                            }
                        }
                    }
                }
            }
        };

        if (opts._source) {
            query._source = opts._source;
        }

        if (opts.sort) {
            query.sort = opts.sort;
        }

        return esClient.search(query)["catch"](function (err) {
            logger.log('error:service:message:fetch',
                'error fetching user\'s messages using query:',
                JSON.stringify(query), '\nerror:\n', err);
            throw Constants.RESPONSE['500'];
        });
    }

    /**
     * @description generates an id for a message or request
     *
     * @param {array<number>} participants - array of user IDs
     * @param {(number|string)=} prefix - if provided will be added before the ID and a hyphen will be appended
     * @returns {string}
     * @deprecated use commonInteraction.generateID
     */
    generateID(participants, prefix) {
        return (prefix ? prefix + '-' : '') + participants.sort().join('_');
    }

    generateRedisToken(requestID, salt) {
        return salt + requestID;
    }

    formatBody(messages) {
        return _.map(messages, function (msg) {
            return {
                msgID: msg.msgID,
                from: msg.from,
                message: msg.message,
                participants: msg.participants,
                read: [msg.from],
                timestamp: Util.format.dateString('basic_date_time', moment()),
                excluded: [],
            };
        });
    }

    /**
     * @descrption checks for the existence of a conversation in the database by searching it's msgID
     * @param {string} requestID - example "149_564" or "1964-149_546"
     * @param {string} type - denotes an index type
     * @returns {Promise<boolean>}
     */
    checkExistence(requestID, type) {
        const TYPE = type ? type.toUpperCase() : 'MESSAGE';

        return esClient.exists({
            index: this.db.INDEX,
            type: this.db[TYPE],
            id: requestID
        }).catch(function (err) {
            logger.log('error:type:message:checkExistence', 'error checking whether message exists\nerror\n', err);
            throw 500;
        });
    }

    /**
     * @descrption creates a single message in the database
     * TODO: when we include messaging multiple people this will have to create more than just 1 instance
     * @param {string} msgID - example "149_564"
     * @returns {Promise<boolean>}
     */
    createMessage(formattedBody) {
        return esClient.create({
            index: this.db.INDEX,
            type: this.db.CHILD,
            body: {
                message: formattedBody.message,
                timestamp: formattedBody.timestamp,
                from: formattedBody.from
            },
            parent: formattedBody.msgID
        }).catch(function (err) {
            logger.log('error:type:message:checkExistence', 'error creating a message\nerror\n', err);
            throw 500;
        });
    }

    /**
     * @description Updates an existing conversation
     * @param {object} formattedBody - the results of #formatBody
     * @param {string} type - denotes an index type
     * @returns {Promise}
     */
    updateConversation(formattedBody, type) {
        const TYPE = type ? type.toUpperCase() : 'MESSAGE';

        return esClient.update({
            index: this.db.INDEX,
            type: this.db[TYPE],
            id: formattedBody.msgID,
            body: {
                doc: {
                    updated: formattedBody.timestamp,
                    read: formattedBody.read,
                    snippet: formattedBody.message.substr(0, 15).trim()
                }
            },
            retryOnConflict: 2
        }).catch(function (err) {
            logger.log('error:type:message:updateConversation', 'error updating a message conversation\nerror\n', err);
            throw 500;
        });
    }

    update(docID, TYPE, doc) {
        return esClient.update({
            index: this.db.INDEX,
            type: this.db[TYPE],
            id: docID,
            body: {
                doc: doc
            }
        }).catch(function (err) {
            logger.log('error:type:message:update', 'error updating interaction\nerror\n', err);
            throw 500;
        });
    }

    /**
     * @description Creates a new conversation between 2+ users
     * @param {object} formattedBody - the results of #formatBody
     * @param {string} type - denotes an index type
     * @returns {Promise}
     */
    createConversation(formattedBody, type) {
        const TYPE = type ? type.toUpperCase() : 'MESSAGE';

        return esClient.index({
            index: this.db.INDEX,
            type: this.db[TYPE],
            _id: formattedBody.msgID,
            body: {
                msgID: formattedBody.msgID,
                participants: formattedBody.participants,
                excluded: [],
                updated: formattedBody.timestamp,
                snippet: formattedBody.message.substr(0, 15).trim(),
                read: formattedBody.read
            }
        }).catch(function (err) {
            logger.log('error:type:message:checkExistence', 'error updating a message conversation\nerror\n', err);
            throw 500;
        });
    }

    /**
     * @description produces a key that resembles something like REQUEST_TANDEM or REQUEST_TEACHER
     * @param {string} requestType - so far one of (tandem|teacher)
     * @param {string=} status - one of the possible interaction statuses => REQUEST_TANDEM_ACCEPT
     * @returns {string}
     */
    generateRequestSourceType(requestType, status) {
        let statusFormatted = '';

        // We only have 2 variations of email templates depending on whether
        // request is accepted or (denied|canceled)
        if (status) {
            statusFormatted = status === 'accept' ? '_ACCEPT'
                            : status === 'reminder' ? '_REMINDER'
                            : '_DENY';
        }

        return 'REQUEST_' + requestType.toUpperCase() + statusFormatted;
    }

    createRequest(formatted) {
        const self = this,
            idKey = this.db[REQUEST + '_id'];

        return sequence.get(idKey).then(function (requestIDPrefix) {
            formatted[idKey] = self.generateID(formatted.participants, requestIDPrefix);

            return esClient.index({
                index: self.db.INDEX,
                type: self.db[REQUEST],
                body: formatted
            }).then(function () {
                return formatted;
            });
        });
    }

    /**
     * @description deletes the messages of a user
     * TODO: 1. test this - 2. Delete tandem requests also
     * @param userID
     * @returns {Promise.<*>}
     */
    deleteID(userID) {
        logger.log('req:type:message:deleteID', 'deleting users document');
        return esClient["delete"]({
            index: this.db.INDEX,
            type: this.db.MESSAGE,
            id: userID
        });
    }
}

module.exports = new Interaction();
