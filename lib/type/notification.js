'use strict';
var Constants, DataBus, Fetch, Notification, Q, SERVICE_NAME, User, Util, _,
    _notificationKeys, _notificationTypes, config, dataFerry, db, elasticsearch,
    esClient, esUser, logger, mandrillKey, moment;

SERVICE_NAME = 'NOTIFICATION';

_ = require('lodash-node/modern');
Q = require('q');
logger = require('bragi');
moment = require('moment');

Fetch = GLOBAL.Fetcher;

config = Fetch.config('config', true, 'js');
mandrillKey = Fetch.config('templates/mandrillKey', true, 'js')[SERVICE_NAME];
Constants = Fetch.config('misc/constants');
db = Constants.elasticsearch[SERVICE_NAME];
DataBus = Fetch.service('DataBus');
elasticsearch = Fetch.database('elasticsearch');
esClient = elasticsearch.client;
User = Fetch.type('user');
esUser = Fetch.util('elasticsearch', 'userHelper');
dataFerry = Fetch.util('dataFerry');
Util = Fetch.util('utility');

_notificationTypes = Fetch.config('misc/notificationTypes');


//    'INTERACTIONS': 1
//    'EVENT': 2
//    'LISTING': 3
//    'USER': 4


/**
 * Key for _notificationTypes to be sent to the front end
 */
_notificationKeys = {
    source: {},
    sourceTypes: {}
};

Notification = (function (superClass) {
    Util.extendClass(Notification, superClass);

    function Notification() {
        logger.log('init:service:notification', 'init');
        this.name = SERVICE_NAME;
        this.db = db;
        this.notificationTypes = _notificationTypes;
        _.each(_notificationTypes, function (val, key) {
            return _notificationKeys.source[val] = key;
        });
        dataFerry.store(SERVICE_NAME, _notificationKeys);
        DataBus.on('/user/deleted', function (user) {
            return this._deleteUsersDoc(user);
        }.bind(this));
    }

    Notification.prototype.registerSourceType = function (notificationKey, subTypes) {
        var sourceKey = _notificationTypes[notificationKey],
            obj = _notificationKeys.sourceTypes[sourceKey] = {};

        return _.each(subTypes, function (val, key) {
            return obj[val] = key;
        });
    };

    Notification.prototype.initUser = function (userID) {
        logger.log('req:service:notification:initUser', 'creating doc');
        return esClient.create({
            index: db.INDEX,
            type: db.TYPE,
            id: userID,
            body: {
                notifications: []
            }
        });
    };

    Notification.prototype.fetch = function (data) {
        logger.log('req:service:notification:fetch',
            'fetching user\'s notifications');
        return esClient.search({
            index: db.INDEX,
            type: db.TYPE,
            size: data.size || 10,
            from: data.from || 0,
            sort: 'when:desc',
            body: {
                query: {
                    filtered: {
                        filter: {
                            term: {
                                _parent: data.userID
                            }
                        }
                    }
                }
            }
        });
    };


    /**
     * Updates an array of notification ID's all belonging to 1 user
     *
     * @param {number} uID     user ID - parent of the notification
     * @param {array}  nIDs    Id's of the notifications to update
     * @param {string} field   which 1 field to update
     * @param {string} value   what to set that field as
     * @returns {Promise}
     */

    Notification.prototype.bulkUpdate = function (userID, docIDs, field, value) {
        var bulk, data;
        if (!docIDs.length) {
            return Q.when();
        }

        data = docIDs.map(function (docID) {
            var entry;
            entry = {
                _id: docID,
                _parent: userID
            };
            entry[field] = value;
            return entry;
        });

        bulk = elasticsearch.bulkFormat(data, 'update', db.INDEX, db.TYPE, '_id');

        logger.log('req:service:notification:bulkUpdate', 'Updating notifications by the bulk');
        return esClient.bulk({
            body: bulk
        })
        .then(function (response) {
            if (response.errors) {
                throw _.pluck(response.items, 'error');
            }

            return response;
        })

        ["catch"](function (err) {
            logger.log('error:service:notification:bulkUpdate',
                'error running bulk update for query:', bulk,
                '\nerror:', err);
            throw Constants.RESPONSE['500'];
        });
    };


    /**
     * @description Called upon user deletion
     * @callback
     * @param {object} user - complete user object of the recently deleted user
     */

    Notification.prototype._deleteUsersDoc = function (user) {
        var userID;
        if (!(userID = String(user.userID))) {
            return true;
        }
        logger.log('req:service:notification:_deleteUsersDoc',
            'deleting users notifications');
        return esClient.deleteByQuery({
            index: db.INDEX,
            type: db.TYPE,
            routing: userID,
            body: {
                query: {
                    term: {
                        _parent: userID
                    }
                }
            }
        });
    };

    return Notification;

})(require('events').EventEmitter);

module.exports = new Notification();
