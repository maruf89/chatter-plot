/**
 * @deprecated - Start moving this over to a type/user helper implementation not here. Remove user specific method names
 * @module elasticsearchUserHelper
 */

'use strict';

var Fetch, Q, _, client, db, elasticsearch, logger, updateScript;

_ = require('lodash-node/modern');

Q = require('q');

logger = require('bragi');

Fetch = GLOBAL.Fetcher;

exports.userDB = db = {
    index: 'users',
    type: 'user'
};

elasticsearch = Fetch.database('elasticsearch');

client = elasticsearch.client;

exports.addUser = function (body, userID, options) {
    if (options == null) {
        options = {};
    }
    logger.log('util:userHelper-elasticsearch:addUser', 'Adding userID:', userID);
    return client.index({
        index: options.index || db.index,
        type: options.type || db.type,
        body: body,
        id: userID
    })["catch"](function (err) {
        logger.log('error:util:userHelper-elasticsearch:addUser', 'Error adding userID:', userID, 'to index:', db.index, 'to type:', db.type, 'with body:', body, '\nerror:\n', err);
        throw err;
    });
};

exports.mailingList = function (body, userID) {
    return client.index({
        index: db.index,
        type: 'mailing',
        body: body,
        id: userID
    })["catch"](function (err) {
        logger.log('error:util:userHelper-elasticsearch:mailingList', 'Error adding userID:', userID, 'to index:', db.index, 'to type: mailing with body:', body);
        throw err;
    });
};


/**
 * Retrieves a user
 * http://www.elasticsearch.org/guide/en/elasticsearch/client/javascript-api/current/api-reference.html#api-get
 *
 * @param  {String|Number} userID   Users unique ID
 * @param  {Object=}       options  Additional search options
 * @return {Promise}                Requested fields or entire _source
 */

exports.retrieveUserById = function (userID, options) {

    if (options == null) {
        options = {};
    }

    logger.log('util:userHelper-elasticsearch:retrieveUserById', 'retrieving userID: ', userID);

    // make the get object overridable
    var get = {
        index: db.index,
        type: options.type || db.type,
        id: userID
    };
    if (options._source) {
        get._source = options._source;
    }
    return client.get(get).then(function (res) {
        return res._source;
    })["catch"](function (err) {
        logger.log('error:util:userHelper-elasticsearch:retrieveUserById', 'Error retrieving User using:', get);
        throw err;
    });
};

exports.mget = function (IDs, options) {
    var get;
    if (options == null) {
        options = {};
    }
    logger.log('util:userHelper-elasticsearch:mget', 'retrieving multiple users');
    get = {
        index: db.index,
        type: options.type || db.type,
        body: {
            ids: IDs
        }
    };
    if (options._source) {
        get._source = options._source;
    }
    return client.mget(get)["catch"](function (err) {
        logger.log('error:util:userHelper-elasticsearch:retrieveUserById', 'Error retrieving User using:', get);
        throw err;
    });
};

exports.userExists = function (userID) {
    logger.log('util:userHelper-elasticsearch:userExists', 'checking...');
    return client.exists({
        index: db.index,
        type: db.type,
        id: userID
    })["catch"](function (err) {
        return false;
    });
};


/**
 * Checks for the existence of a 3rd party service connection
 *
 * @param  {number} userID
 * @param  {string} service  service name to check for
 * @return {Promise}         Boolean
 */

exports.userAccountExists = function (userID, service) {
    logger.log('util:userHelper-elasticsearch:userAccountExists', 'checking...');
    return client.search({
        index: db.index,
        type: db.type,
        body: {
            "query": {
                "filtered": {
                    "filter": {
                        "bool": {
                            "must": [{
                                "nested": {
                                    "path": "accounts",
                                    "query": {
                                        "filtered": {
                                            "filter": {
                                                "term": {
                                                    "accounts.type": [service]
                                                }
                                            }
                                        }
                                    }
                                }
                            }, {
                                "ids": {
                                    "values": [userID]
                                }
                            }]
                        }
                    }
                }
            }
        }
    })["catch"](function (err) {
        logger.log('error:util:userHelper-elasticsearch:userAccountExists', 'Error checking userAccount for userID: ', userID, ' for service: ', service);
        throw err;
    });
};

// To help update fields
updateScript = 'if (ctx._source.<field> == null) { ctx._source.<field> = newField } else { ctx._source.<field> += newField }';


/**
 * @deprecated use bTypeUser#scriptUpdate
 * @description Updates a user document
 * @name elasticsearchUserHelper~updateUser
 * @example
 *     // Update the users email on Elasticsearch's `mailing` type
 *     esUser.updateUser({ _id: 12, type: 'mailing' }, { email: 'mat@gmail.com' })
 *
 *     // Add additional values to the `accounts` field (Adding another `true` parameter, would replace the existing)
 *     esUser.updateUser('12', { type: 'facebook_id', id: 'AAAW3EANT0TH2...' }, 'accounts')
 *
 * @param  {(object|string)} types - if not object - must be _id of the doc. else -
 * @param  {string} types._id - the Elasticsearch documents id
 * @param  {string=} types.index - optional database index
 * @param  {string=} types.type - optional database type
 * @param  {object} replacement - data to update
 * @param  {string=} arrayField - if field to update is an array then the field's name
 * @param  {boolean=} replaceArray if arrayField is set, and the field should completely replace the entry instead of appending to it
 * @return {Promise}
 */

exports.updateUser = function (types, replacement, arrayField, replaceArray, opts) {
    var body, query;

    if (opts == null) {
        opts = {};
    }

    logger.log('util:userHelper-elasticsearch:updateUser', 'Updating user');
    if (!_.isPlainObject(types)) {
        types = {
            _id: types
        };
    }

    // Check if we're appending to a field
    if (arrayField && !replaceArray) {
        body = {
            script: opts.script || updateScript.replace(/<field>/g, arrayField),
            params: opts.params || {
                newField: [replacement]
            }
        };
    } else {
        body = {
            doc: replacement
        };
    }

    query = {
        index: types.index || db.index,
        type: types.type || db.type,
        id: String(types._id),
        body: body
    };

    return client.update(query)["catch"](function (err) {
        logger.log('error:util:userHelper-elasticsearch:updateUser', 'Error updating user using types:', types, 'replacement:\n', replacement, '\narrayField:', arrayField, 'replaceArray set to:', !!replaceArray, 'and query:\n', query, '\n', err);
        throw err;
    });
};

exports["delete"] = function (userID) {
    logger.log('util:userHelper-elasticsearch:delete', 'deleting userID:', userID);
    return client["delete"]({
        index: db.index,
        type: db.type,
        id: userID
    })["catch"](function (err) {
        logger.log('error:util:userHelper-elasticsearch:delete', 'Error deleting userID:', userID);
        throw err;
    });
};
