'use strict';

var _   = require('lodash-node/modern'),

    Fetch       = GLOBAL.Fetcher,

    Util        = Fetch.util('utility');

/**
 * Accepts an array of venues and builds search filters out of that objects source fields
 *
 * @param  {array} venues             Array of objects containing a {object} `source`
 * @param  {object} venues[0].source  contains both: {string} `name` - Service Name && {string} `id` - unique location id
 * @return {array}                    Array of Elasticsearch readable bool filters
 */

exports.buildSearchConditions = function (venues) {
    let service;

    return _.reduce(venues, function (conditions, venue) {
        if (service = venue.google ? 'google' : venue.yelp ? 'yelp' : null) {
            let term = {};
            term[service] = venue[service];

            conditions.push({ term: term });
        }

        return conditions;
    }, [])
};

exports.prepareVenue = function (venue) {
    if (typeof venue.coords === 'object') {
        venue.coords = {
            lat: venue.coords.lat || venue.coords.latitude,
            lon: venue.coords.lon || venue.coords.longitude
        };
    }

    return venue;
};

exports.venueArr2Obj = function (venueArr) {
    return _.reduce(venueArr, function (obj, venue) {
        obj[venue.vID] = venue;
        return obj;
    }, {});
};
