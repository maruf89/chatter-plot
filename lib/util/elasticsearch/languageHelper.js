/**
 * @deprecated - move to a more suitable place then here
 */
'use strict';
var Fetch, _, elasticsearch, esClient, logger;

_ = require('lodash-node/modern');

logger = require('bragi');

Fetch = GLOBAL.Fetcher;


// Load Elasticsearch client 

elasticsearch = Fetch.database('elasticsearch');

esClient = elasticsearch.client;


/**
 * Accepts a languages response object, and converts a response object to an
 * object displayd as { <language> : <languadeID> }
 *
 * @param  {object} response  ES response to /sources/languages request
 * @param  {string} format    Language/ID format
 *                                idArray  : [<25>, <3>, <14>]
 *                                langArray: [<italian>, <english>, <german>]
 *                                keyObject: { italian: 25, mandarin: 1, swahili: 46 }
 *                                <default>: [{ languageID: 25, name: italian }, { languageID: 46, name: 'swahili' }]
 * @return {object}           language:languageID object
 */

exports.respToLangObj = function (response, format) {
    var docs, languages, ref;
    languages = format && format.substr(-6) === 'Object' ? {} : [];
    docs = response.docs || ((ref = response.hits) != null ? ref.hits : void 0);
    if (!docs && response._source) {
        docs = [response];
    }
    if (!docs) {
        return [];
    }
    _.each(docs, function (doc) {
        var id, name, source;
        source = doc._source;
        name = source.language.toLowerCase();
        id = +doc._id;
        switch (format) {
            case 'idArray':
                return languages.push(id);
            case 'langArray':
                return languages.push(name);
            case 'keyObject':
                return languages[name] = id;
            default:
                return languages.push({
                    name: name,
                    languageID: id
                });
        }
    });
    return languages;
};


/**
 * Gets corresponding ID's for each passed in language
 *
 * @param  {String|Array} languages  Languages to get id's from Elasticsearch for
 * @return {Promise}
 * @option                           success returns Elasticsearch hits response
 */

exports.getIdForLanguages = function (languages) {
    var matches, query;
    if (typeof languages === 'string') {
        languages = [languages];
    }

    // Lowercase all languages for ES 
    languages = languages
        .join('0')
        .toLowerCase()
        .split('0');

    // Build the Elasticsearch search body 

    // First by building all the bool matches 
    matches = languages.map(function (lang) {
        return {
            term: {
                language: lang
            }
        };
    });

    query = {
        query: {
            filtered: {
                filter: {
                    bool: {
                        should: matches
                    }
                }
            }
        }
    };

    // Query Elasticsearch for the language keys 
    return esClient.search({
        index: 'sources',
        type: 'languages',
        body: query,
        _source: 'language',
        size: 300
    })["catch"](function (err) {
        logger.log(
            'error:util:elasticsearch-languageHelper:getIdForLanguages',
            'Error fetching languageID\'s from the elasticsearch database, languages:',
            languages, 'matches:', matches, 'error:\n\n', err);
        throw err;
    });
};


/**
 * Converts an array of language ID's into an array of objects containing the
 * language names & keys
 *
 * For options see: http://www.elasticsearch.org/guide/en/elasticsearch/client/javascript-api/current/api-reference.html
 * 
 * @param  {array}   keys      contains {Integer} keys of language ID's
 * @param  {Object=} options   anything to extend the query object
 * @return {Promise}           returns {object} containing 'hits.hits' with the second hits being an array of results
 */

exports.ID2Language = function (keys, options) {
    var method, query;
    if (!Array.isArray(keys)) {
        logger.log('error:util:elasticsearch-languageHelper:ID2Language',
            'Expected keys parameter to be of type Array instead is:',
            Object.prototype.toString.call(keys));
        throw false;
    }
    query = {
        index: 'sources',
        type: 'languages'
    };
    if (options) {
        _.extend(query, options);
    }
    if (keys.length === 1) {
        method = 'get';
        query.id = keys[0];
    } else {
        method = 'mget';
        query.body = {
            ids: keys
        };
    }
    return esClient[method](query)["catch"](function (err) {
        return logger.log(
            'error:util:elasticsearch-languageHelper:ID2Language',
            'Error fetching languages using query:', query,
            'method:', method);
    });
};
