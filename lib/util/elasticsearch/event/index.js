'use strict';

var _   = require('lodash-node/modern'),

    Fetch       = GLOBAL.Fetcher,

    esEventScript = Fetch.config('esScripts/event', true, 'js'),

    Util        = Fetch.util('utility'),

    Query       = Fetch.util('elasticsearch', 'event', 'query'),
    Filter      = Fetch.util('elasticsearch', 'event', 'filter'),
    Sort        = Fetch.util('elasticsearch', 'event', 'sort');

exports.buildQuery = Query.build;
exports.buildFilter = Filter.build;
exports.buildSort = Sort.build;

exports.prepareEvent = function (event) {
    if (event.venue && typeof event.venue.coords === 'object') {
        event.venue.coords = Util.normalizeCoords(event.venue.coords);
    }

    return event;
};

/**
 * Builds the update query to add/remove user from event
 * @param  {boolean} isGoing - whether to add or remove userID from the list
 * @param  {object} data
 * @return {object} an elasticsearch query
 */

exports.userGoingQuery = function (isGoing, data) {
    var query = {
        index: 'events',
        type: data.type || 'event',
        id: String(data.eID)
    };
    if (isGoing) {
        query.body = {
            script: data.waitList ? esEventScript.appendToWaitlist : esEventScript.appendGoing,
            params: {
                newField: [parseInt(data.insert, 10)]
            }
        };
    } else {
        query.body = {
            script: esEventScript.removeGoing,
            params: {
                user_id: parseInt(data.userID, 10)
            }
        };
    }
    return query;
};

exports.util = {
    fillQueryData: function (searchOpts) {
        var q,
            userID;

        if (!Array.isArray(q = searchOpts.filterTypes)) {
            return;
        }

        if (userID = searchOpts.userID) {
            if (q.indexOf('hosting') !== -1) {
                searchOpts.opts.hosting = userID;
            }
            if (q.indexOf('notHosting') !== -1) {
                searchOpts.opts.notHosting = userID;
            }
            if (q.indexOf('attending') !== -1) {
                searchOpts.opts.attending = userID;
            }
            if (q.indexOf('notAttending') !== -1) {
                return searchOpts.opts.notAttending = userID;
            }
        }
    }
};

/**
 * @description formats an array of event request objects from the client for Elasticsearch
 * looks like:
 *              {
 *                  eID: "12312",       => _id: "12312
 *                  type: "listing"     => _type: "listing"
 *              }
 *
 * @param clientTypesArr
 * @returns {array<object>}
 */
exports.parseMGetRequest = function (clientTypesArr) {
    return _.map(clientTypesArr, function (single) {
        return {
            _id: single.eID,
            _type: single.type,
        };
    });
};
