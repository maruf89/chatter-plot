'use strict';

const logger      = require('bragi');

const Fetch       = GLOBAL.Fetcher;

const Util        = Fetch.util('utility');
const commonGeneral = Fetch.common('util', 'general');

const defaultQuery    = {};

const fnOptions = {
    /**
     * @description Filters by all events with 'when' property AFTER 'data.when' or now()
     *
     * @param {object} query - the current query object
     * @param {object} opts - argument data
     * @param {string=} opts.when - formatted as 'YYYYMMDDTHHmmssZ'
     * @return {object} original query with added filter
     */
    current: function (query, opts) {
        return query.bool.must.push({
            range: {
                when: {
                    gte: opts.when || commonGeneral.format.dateString('basic_date_time_no_millis')
                }
            }
        });
    },

    /**
     * @description Filters by all events with 'when' property BEFORE 'data.when' or now()
     *
     * @param {object} query - the current query object
     * @param {object} opts - argument data
     * @param {string=} opts.when - formatted as 'YYYYMMDDTHHmmssZ'
     * @return {object} original query with added filter
     */
    past: function (query, opts) {
        return query.bool.must.push({
            range: {
                when: {
                    lte: opts.when || commonGeneral.format.dateString('basic_date_time_no_millis')
                }
            }
        });
    },

    /**
     * @description Transforms an array of date ranges into a query
     *
     * @param {object} query - the current query object
     * @param {object} opts - argument data
     * @param {array} opts.dateRange - Collection of objects containing:
     * @param {string} opts.date - basic_date_time_no_millis format string or Date Object
     * @param {String=} opts.operator - one of gte|gt|lte|lt (default:gte)
     */
    dateRange: function (query, opts) {
        if (!Array.isArray(opts.dateRange)) {
            return false;
        }

        return [].push.call(query.bool.must, opts.dateRange.map(function (range) {
            var value = range.date instanceof Date ?
                commonGeneral.format.dateString('basic_date_time_no_millis', range.date)
                :
                range.date;

            const rangeObj = {
                range: {
                    when: {}
                }
            };

            rangeObj.range.when[range.operator || 'gte'] = value;

            return rangeObj;
        }));
    },

    /**
     * Filters by all events within a set distance
     *
     * @param {object} query - the current query object
     * @param {object} opts
     * @param {object} opts.location - argument opts
     * @param {number=} opts.location.distance - distance in kilometers
     * @param {object} opts.location.coords - an object containing
     *                                          both {Float} 'lat', 'lon' (latitude, longitude)
     * @return {object} original query with added filter
     */
    location: function (query, opts) {
        let coords, distance;
        const loc = opts.location;

        if (!(loc ? (coords = loc.coords) ? coords.lon : false : false)) {
            return false;
        }
        
        distance = opts.location.distance || 50;
        
        return query.bool.must.push({
            nested: {
                path: 'venue',
                filter: {
                    geo_distance: {
                        distance: distance + 'km',
                        coords: {
                            lat: coords.lat,
                            lon: coords.lon
                        }
                    }
                }
            }
        });
    },

    /**
     * Finds all events within a bounding box
     *
     * @option opts {string} bounds  4 comma separated floats representing the
     * top, left, bottom, right of a box
     */
    withinBounds: function (query, opts) {
        var loc, coords, splat;

        // Check to see if coordinates exist (and at the same time set some variables)
        if (!opts.bounds ||
            typeof opts.bounds !== 'string' ||
            !((loc = opts.location) ? (coords = loc.coords) ? coords.lat : false : false)
        ) {
            return false;
        }
        
        splat = opts.bounds.split(',');
        
        if (splat.length !== 4) {
            logger.log('error:speaker:searchTypes:withinBounds',
                'invalid bounds passed expecting a comma separated string', opts.bounds);
            return false;
        }
        
        return query.bool.must.push({
            nested: {
                path: 'venue',
                filter: {
                    geo_bounding_box: {
                        coords: {
                            top_left: {
                                lat: parseFloat(splat.shift()),
                                lon: parseFloat(splat.shift())
                            },
                            bottom_right: {
                                lat: parseFloat(splat.shift()),
                                lon: parseFloat(splat.shift())
                            }
                        }
                    }
                }
            }
        });
    },
    
    language: function (query, opts) {
        var languages = opts.languages;

        if (Array.isArray(languages) && languages.length) {
            if (opts.allLangs) {
                languages.push(0);
            }

            return query.bool.must.push({
                terms: {
                    'languages': languages
                }
            });
        }
        
        return false;
    },

    attending: function (query, opts) {
        var attending = opts.attending;
        
        if (!attending) {
            return false;
        }
        
        return query.bool.must.push({
            bool: {
                should: [
                    {
                        term: {
                            goingList: [attending]
                        }
                    }, {
                        term: {
                            waitList: [attending]
                        }
                    }
                ]
            }
        });
    },
    
    notAttending: function (query, opts) {
        var notAttending = opts.notAttending,
            mustnt;
        
        if (!notAttending) {
            return false;
        }
        mustnt = query.bool.must_not = query.bool.must_not || [];
        
        return mustnt.push({
            bool: {
                should: [
                    {
                        term: {
                            goingList: [notAttending]
                        }
                    }, {
                        term: {
                            waitList: [notAttending]
                        }
                    }
                ]
            }
        });
    },

    hosting: function (query, opts) {
        var hosting = opts.hosting;
        
        if (!hosting) {
            throw logger.log('error:searchTypes:hosting',
                'expecting an opts.hosting parameter representing a userID \nopts:\n', opts);
        }
        return query.bool.must.push({
            term: {
                'creator': hosting
            }
        });
    },

    notHosting: function (query, opts) {
        var notHosting = opts.notHosting,
            mustnt;
        
        if (!notHosting) {
            throw logger.log('error:searchTypes:notHosting',
                'expecting an opts.notHosting parameter representing a userID \opts:\n', opts);
        }

        mustnt = query.bool.must_not = query.bool.must_not || [];
        
        return mustnt.push({
            term: {
                'creator': notHosting
            }
        });
    },
    published: function (query) {
        return query.bool.must.push({
            term: {
                'published': true
            }
        });
    },

    /**
     * TODO: Test
     */
    unpublished: function (query) {
        return query.bool.must.push({
            filter: {
                bool: {
                    should: [
                        {
                            term: {
                                'published': false
                            }
                        }, {
                            missing: {
                                field: 'published'
                            }
                        }
                    ]
                }
            }
        });
    }
};

exports.build = function (filterTypes, opts) {
    var hasLength = 0,
        base = {
            bool: {
                must: []
            }
        },
        builtQuery;

    if (!Array.isArray(filterTypes) || !filterTypes.length) {
        return defaultQuery;
    }

    builtQuery = filterTypes.reduce(function (query, queryType) {
        if (typeof fnOptions[queryType] === 'function') {
            if (fnOptions[queryType](query, opts)) {
                hasLength++;
            }
        }

        return query;
    }, base);

    return hasLength ? builtQuery : defaultQuery;
};