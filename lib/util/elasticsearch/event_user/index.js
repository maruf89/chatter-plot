/**
 * @module bUtilElasticsearchEvent_User
 */

'use strict';

/**
 * @description builds a query that returns all users within 50km that are learning, or are interested
 * in the language of thi event
 * @name bUtilElasticsearchEvent_User#relevantEventUsersSearch
 * @param {object} event - an event object
 * @param {object} event.venue
 * @param {object} event.venue.coords
 * @param {float} event.venue.coords.lat
 * @param {float} event.venue.coords.lon
 * @param {array<number>} event.languages
 * @returns {object} elasticsearch search object
 */
exports.relevantEventUsersSearch = function (event) {
    // TODO: grab the query from util/elasticsearch/user/filter.js instead of rewriting it here
    var q = {
            query: {
                filtered: {
                    filter: {
                        bool: {
                            must: [{
                                nested: {
                                    path: 'locations',
                                    filter: {
                                        geo_distance: {
                                            distance: '50km',
                                            coords: event.venue.coords
                                        }
                                    }
                                }
                            }]
                        }
                    }
                }
            }
        },
        bool = q.query.filtered.filter.bool;

    if (String(event.languages[0]) === '0') {
        bool.must.push({
            term: {
                "settings.notifs.email.eventsAllLang": true
            }
        });
    } else {
        bool.must.push({
            nested: {
                path: "languages",
                filter: {
                    terms: {
                        languageID: event.languages
                    }
                }
            }
        });
        bool.should = [
            {
                term: {
                    "settings.notifs.email.eventsISpeak": true
                }
            }, {
                bool: {
                    must: [
                        {
                            term: {
                                "settings.notifs.email.eventsILearn": true
                            }
                        }, {
                            nested: {
                                path: "languages",
                                filter: {
                                    bool: {
                                        should: event.languages.map(function (languageID) {
                                            return {
                                                bool: {
                                                    must: [{
                                                        term: {
                                                            learning: true
                                                        }
                                                    }, {
                                                        term: {
                                                            languageID: languageID
                                                        }
                                                    }]
                                                }
                                            };
                                        })
                                    }
                                }
                            }
                        }
                    ]
                }
            }
        ];
    }

    return q;
};