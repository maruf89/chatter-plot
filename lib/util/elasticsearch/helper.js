/**
 * @module utilElasticsearchHelper
 */

'use strict';

const _   = require('lodash-node/modern');
const helper = exports;

const Fetch = GLOBAL.Fetcher;

const Util = Fetch.util('utility');

const types = {
    event: Fetch.util('elasticsearch', 'event'),
    user: Fetch.util('elasticsearch', 'user'),
    interaction: Fetch.util('elasticsearch', 'interaction'),
};

/**
 * @description Builds the searches base query - this is a preBuild
 *
 * @name _buildBase
 * @param {array<string>=} queryTypes - array of filter/ort types
 * @param {array<string>=} filterTypes - array of filter/ort types
 * @param {array<string>=} sortTypes - array of filter/ort types
 * @param {object} opts - search option data
 * @returns {array<object>} returnArr
 * @returns {object}        returnArr[0] - the parent search object
 * @returns {object}        returnArr[1] - what to attach additional query/filters to
 *
 * @private
 */
const _buildBase = function (queryTypes, filterTypes, sortTypes, opts) {
    let baseQuery;
    const base = {};

    // If we're shuffling set the base of the query to 'function_score'
    // both function_score and filtered are not supported - one or the other
    if (Array.isArray(sortTypes)) {
        let index;

        if ((index = sortTypes.indexOf('shuffle')) !== -1) {
            baseQuery = base.function_score = base.function_score || {};
            baseQuery.random_score = {
                seed: opts.sessionID || Math.random()
            };

            // remove the shuffle
            sortTypes.splice(index, 1);
        }
    }

    // set filtered default
    if (!baseQuery) {
        baseQuery = base.filtered = {};
    }

    return [base, baseQuery];
};

/**
 * @description Builds the search
 * @name utilElasticsearchHelper#buildSearch
 * @param  {array<string>} filterTypes
 * @return {object} an elasticsearch query
 */
exports.buildSearch = function (type, queryTypes, filterTypes, sortTypes, data) {
    if (!types[type]) {
        throw 'Invalid invocation at util/elasticsearch/helper#buildSearch type doesn\'t exist'
    }

    data = data || {};

    const opts = data.opts = data.opts || {};
    let query;

    type = types[type];
    queryTypes = queryTypes && _.clone(queryTypes);
    filterTypes = filterTypes && _.clone(filterTypes);

    helper.trimTypes(queryTypes, filterTypes, sortTypes, opts);

    query = {
        query: (function () {
            const baseBuild = _buildBase(queryTypes, filterTypes, sortTypes, opts);
            const base = baseBuild[0];
            const baseQuery = baseBuild[1];

            baseQuery.query = type.buildQuery(queryTypes, opts);
            baseQuery.filter = type.buildFilter(filterTypes, opts);

            return base;
        })()
    };

    if (Array.isArray(sortTypes) && sortTypes.length) {
        query.sort = type.buildSort(sortTypes, data);
    }

    return query;
};

/**
 * @description proxy to build sort
 * @name utilElasticsearchHelper#buildSort
 * @param type
 * @param sortTypes
 * @param data
 * @returns {*}
 */
exports.buildSort = function (type, sortTypes, data) {
    if (!types[type]) {
        throw 'Invalid invocation at util/elasticsearch/helper#buildSearch type doesn\'t exist'
    }

    if (data == null) {
        data = {};
    }

    if (Array.isArray(sortTypes) && sortTypes.length) {
        return types[type].buildSort(sortTypes, data);
    } else return [];
};

/**
 * @description Accepts all of the query + filter array types and removes incompatible searches
 * directly modifies the passed in variables
 * @name utilElasticsearchHelper#trimTypes
 * @param {array<string>} query - filter types
 * @param {array<string>} filter - filter types
 * @param {object} opts - search options
 */
exports.trimTypes = function (query, filter, sort, opts) {
    var querySearch = !!opts.query,
        hasSort = Array.isArray(sort) && sort.length,
        hasFilter = Array.isArray(filter) && filter.length,
        hasNameEmail,
        boundsAndLoc,
        index;

    // Only check if we have a query to search on
    if (querySearch) {
        // Check if we're querying for name or e-mail
        hasNameEmail = Array.isArray(query) &&
                       query.length &&
                       (~query.indexOf('name') || ~query.indexOf('email'));

        // If we have a query to search on, we don't want to shuffle
        if (hasSort && (index = sort.indexOf('shuffle')) !== -1) {
            sort.splice(index, 1);
        }
    } else if (Array.isArray(query)) {
        query.length = 0;
    }

    if (hasFilter && hasNameEmail) {
        if ((index = filter.indexOf('language')) !== -1)
            filter.splice(index, 1);

        if ((index = filter.indexOf('location')) !== -1)
            filter.splice(index, 1);

        if ((index = filter.indexOf('withinBounds')) !== -1)
            filter.splice(index, 1);

        boundsAndLoc = false;
    }

    // check if both location & withinBounds exist
    boundsAndLoc = boundsAndLoc === undefined ? filter.indexOf('withinBounds') !== -1 &&
                                                opts.bounds &&
                                                (index = filter.indexOf('location')) !== -1
                                              : boundsAndLoc;

    // Remove `location` if we have `withinBounds` which is more specific
    if (boundsAndLoc) {
        filter.splice(index, 1);
    }

    // If sorting by native speakers but we don't have a language
    if (hasFilter && hasSort) {
        if ((index = sort.indexOf('nativeSpeaker')) !== -1 &&
            (filter.indexOf('language') === -1 ||
            !Array.isArray(opts.languages) ||
            !opts.languages[0]) // if languages is falsey (remember 0 = 'All Languages')
        ) {
            // remove native speaker sort
            sort.splice(index, 1);
        }
    }
};

/**
 * @description formats any malformed variables to be elasticsearch ready
 * @name utilElasticsearchHelper#prepareQuery
 * @param query
 * @returns {*}
 */
exports.prepareQuery = function (query) {
    if (query.opts.location.coords) {
        query.opts.location.coords = Util.normalizeCoords(query.opts.location.coords);
    }

    return query;
};

exports.searchExtractSource = function (results) {
    return _.pluck(results.hits.hits, '_source');
};

/**
 * @description returns an object with results + total
 *
 * @param {string} key - key name to set object
 * @param {object} results - Elasticsearch results object
 * @returns {object} resultsObj
 * @returns {number} resultsObj.total
 * @returns {array<object>} resultsObj.results
 */
exports.formatSearchSource = function (results) {
    return {
        total: results.hits.total,
        results: helper.searchExtractSource(results)
    };
};

/**
 * Corresponds to `get` and returns just the source of the users as an array
 *
 * @param {object} result - the elasticsearch response
 * @param {object} result._source - the data
 * @returns {array<object>}
 */
exports.getExtractSource = function (result) {
    return [result._source];
};

/**
 * Corresponds to `mget` and returns just the source of the results as an array
 *
 * @param {object} results  response
 * @returns {Array}
 */
exports.mgetExtractSource = function (results) {
    return results.docs.reduce(function (array, result) {
        result._source && array.push(result._source);
        return array;
    }, []);
};


/**
 * Corresponds to `mget` and returns a JSON object mapping of each items { _id: _source }
 *
 * @param {object} results  response
 * @returns {Array}
 */
exports.mgetIdMap = function (results) {
    var obj;
    obj = {};
    _.each(results.docs, function (result) {
        obj[result._id] = result._source;
        return true;
    });
    return obj;
};

exports.appendResultsToObjectAs = function (toExtend, key, results) {
    toExtend[key] = results;
};
