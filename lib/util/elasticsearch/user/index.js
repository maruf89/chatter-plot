'use strict';

const _         = require('lodash-node/modern');
const logger      = require('bragi');

const Fetch       = GLOBAL.Fetcher;

const Util        = Fetch.util('utility');

const Query       = Fetch.util('elasticsearch', 'user', 'query');
const Filter      = Fetch.util('elasticsearch', 'user', 'filter');
const Sort        = Fetch.util('elasticsearch', 'user', 'sort');

exports.buildQuery = Query.build;
exports.buildFilter = Filter.build;
exports.buildSort = Sort.build;

const expiredField = function (field, use) {
    return function (user) {
        delete user[field];
        logger.log('warning:user:update',
            'Updating of field:', field,
            'is deprecated. Instead use:', use
        );
    };
};

const sanitizeChecks = {
    locations: function (user) {
        if (!Array.isArray(user.locations)) {
            user.locations = null;
            return;
        }

        user.locations = _sanitizeLocation(user.locations);
    },
    languages: function (user) {
        if (!Array.isArray(user.languages)) {
            user.languages = null;
            return;
        }

        user.languages = _.uniq(user.languages, 'languageID');
    },
    interests: function (user) {
        if (!Array.isArray(user.interests)) {
            user.interests = null;
            return;
        }

        user.interests = _.map(user.interests, function (qa) {
            return {
                question: qa.question,
                answer: qa.answer
            };
        });
    },
    coords: expiredField('coords', 'locations'),
    state: expiredField('state', '---'),
    city: expiredField('city', '---'),
    address: expiredField('address', '---'),
    country: expiredField('country', '---'),
    sublocality: expiredField('sublocality', '---'),
    postalCode: expiredField('postalCode', '---'),
};

const sanitizeFields = Object.keys(sanitizeChecks);

const _sanitizeLocation = function (locations) {
    let primary;
    let safeLoc;
    let tempSource = [];

    // Filter over each possible location and make sure they have only the correct fields
    const sanitized = _.reduce(locations, function (returnArr, location) {
        if (location.vID) {
            safeLoc = {
                vID: location.vID
            };
            if (location.google) {
                safeLoc.google = location.google;
            }
            if (location.yelp) {
                safeLoc.yelp = location.yelp;
            }
            if (!primary && location.primary && location.coords) {
                primary = safeLoc.primary = true;
                safeLoc.coords = Util.normalizeCoords(location.coords);

                if (location.sublocality) {
                    safeLoc.sublocality = location.sublocality;
                }

                if (location.city) {
                    safeLoc.city = location.city;
                }

                if (location.state) {
                    safeLoc.state = location.state;
                }

                if (location.country) {
                    safeLoc.country = location.country;
                }
            }
            tempSource.push(location);
            returnArr.push(safeLoc);
        }
        return returnArr;
    }, []);

    // If there is no primary set then set one automatically
    if (!primary && sanitized[0] && tempSource[0].coords) {
        sanitized[0].primary = true;
        sanitized[0].coords = Util.normalizeCoords(tempSource[0].coords);
    }

    return sanitized;
};

/**
 * Modifies the passed in object and sanitizes certain fields
 * @param {object} object - fields pertaining to the users mapping
 */
exports.sanitizeFields = function (object) {
    _.each(sanitizeFields, function (field) {
        if (field in object) {
            sanitizeChecks[field](object);
        }
    });
};
