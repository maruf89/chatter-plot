'use strict';

var _   = require('lodash-node/modern'),

types = {
    /**
     * If a language is being search, Show native speakers at the top
     *
     * @param sort
     * @param opts
     */
    nativeSpeaker: function (sort, opts) {
        if (opts.languages) {
            _.each(opts.languages, function (langID) {
                // If langID is 0, or falsey, we don't want a sort param
                if (!langID) return true;

                sort.push({
                    'languages.level': {
                        order: 'desc',
                        nested_filter: {
                            term: {
                                'languages.languageID': langID
                            }
                        },
                    },
                });
            });
        }

        return sort;
    }
};

exports.build = function (sortTypes, data) {
    if (!Array.isArray(sortTypes) || !sortTypes.length) {
        return [];
    }

    data = data || {};

    var sort = [];

    return sortTypes.reduce(function (arr, type) {
        if (typeof types[type] === 'function') {
            return types[type](arr, data.opts || {});
        }
        return arr;
    }, sort);
};