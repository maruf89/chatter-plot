'use strict';

var logger = require('bragi'),

defaultQuery = {},

fnOptions = {
    /**
     * Filters by all events within a set distance
     *
     * @param  {object} query - the current query object
     * @param {object} opts - argument options
     * @option {number} [opts.distance=20] - distance in kilometers
     * @option {object} opts.coords - an object containing both {Float} 'lat', 'lon' (latitude, longitude)
     * @return {object} original query with added filter
     */
    location: function (query, opts) {
        var distance, loc, coords;

        // Check to see if coordinates exist (and at the same time set some variables)
        if (!(((loc = opts.location) ? (coords = loc.coords) ? coords.lat : false : false) && coords.lon)
        ) {
            return false;
        }

        distance = opts.location.distance || 20;

        return query.bool.must.push({
            nested: {
                path: 'locations',
                filter: {
                    geo_distance: {
                        distance: distance + 'km',
                        coords: {
                            lat: coords.lat,
                            lon: coords.lon
                        }
                    }
                }
            }
        });
    },

    /**
     * Finds all events within a bounding box
     *
     * @option opts {string} bounds  4 comma separated floats representing the top, left, bottom, right of a box
     */
    withinBounds: function (query, opts) {
        var loc, coords, splat;

        // Check to see if coordinates exist (and at the same time set some variables)
        if (!opts.bounds || typeof opts.bounds !== 'string' || !((loc = opts.location) ? (coords = loc.coords) ? coords.lat : false : false)) {
            return false;
        }

        splat = opts.bounds.split(',');

        if (splat.length !== 4) {
            logger.log('error:speaker:searchTypes:withinBounds', 'invalid bounds passed expecting a comma separated string', opts.bounds);
            return false;
        }

        return query.bool.must.push({
            nested: {
                path: 'locations',
                filter: {
                    geo_bounding_box: {
                        coords: {
                            top_left: {
                                lat: parseFloat(splat.shift()),
                                lon: parseFloat(splat.shift())
                            },
                            bottom_right: {
                                lat: parseFloat(splat.shift()),
                                lon: parseFloat(splat.shift())
                            }
                        }
                    }
                }
            }
        });
    },
    hasLocation: function (query) {
        query.bool.must.push({
            nested: {
                path: 'locations',
                filter: {
                    exists: {
                        field: "primary"
                    }
                }
            }
        });
    },

    hasName: function (query) {
        return query.bool.must.push({
            exists: {
                field: "firstName"
            }
        });
    },

    language: function (query, opts) {
        const langsLength = opts.languages && opts.languages.length;

        if (langsLength) {
            // If the only language being search is `0` then ignore
            if (langsLength === 1 && opts.languages[0] === 0) {
                return false;
            }

            return query.bool.must.push({
                nested: {
                    path: 'languages',
                    filter: {
                        terms: {
                            'languageID': opts.languages
                        }
                    }
                }
            });
        }

        return false;
    },

    teachesLanguage: function (query, opts) {
        const langsLength = opts.languages && opts.languages.length;
        const check = {
            nested: {
                path: 'languages',
                filter: {
                    bool: {
                        must: [{
                            term: {
                                teaching: true
                            }
                        }]
                    }
                }
            }
        };

        if (langsLength && (langsLength > 1 || opts.languages[0] !== 0)) {
            check.nested.filter.bool.must.push({
                terms: {
                    languageID: opts.languages
                }
            });
        }

        return query.bool.must.push(check);
    },

    acceptMsg: function (query) {
        return query.bool.must.push({
            term: {
                'settings.notifs.messaging.acceptAll': true
            }
        });
    },
};

exports.build = function (filterTypes, opts) {
    var hasLength = 0,
        index,
        base = {
            bool: {
                must: []
            }
        },
        builtQuery;

    if (!Array.isArray(filterTypes) || !filterTypes.length) {
        return defaultQuery;
    }

    builtQuery = filterTypes.reduce(function (query, queryType) {
        if (typeof fnOptions[queryType] === 'function') {
            if (fnOptions[queryType](query, opts)) {
                hasLength++;
            }
        }

        return query;
    }, base);

    return hasLength ? builtQuery : defaultQuery;
};