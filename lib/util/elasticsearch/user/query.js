'use strict';

var logger      = require('bragi'),

    Fetch       = GLOBAL.Fetcher,
    
    Util        = Fetch.util('utility'),

    defaultQuery = {
        match_all: {}
    },
        
    queryChecks = {
        email: Util.string.isEmail,
        name: function (query) {
            return query && !Util.string.isEmail(query);
        }
    },
    
    filterQueryTypes = function (queryTypes, queryString) {
        return queryTypes.filter(function (type) {
            return queryChecks[type] && queryChecks[type](queryString);
        })
    },

    fnOptions = {
        email: function (query, queryString) {
            return query.bool.must.push({
                match: {
                    email: queryString
                }
            })
        },
        name: function (query, queryString) {
            // Convert a string into a first/last name object
            let name = Util.user.destructName(queryString),

                // Set a variable for the should array
                should = query.bool.should = query.bool.should || [],

                /**
                 * @description function that builds elasticsearch term checks
                 * @private
                 * @param {array} arr - reference to an array to populate
                 * @param {string} field - the key name
                 * @param {string} value - the name value to search
                 * @returns {number} returns the length of the newly populated array
                 */
                queryGenerator = function (arr, field, value) {
                    var term1 = { term: {} },
                        term2 = { term: {} },
                        term3 = { term: {} };

                    term1.term[field] =  {
                        'boost' : 1,
                        'value' : value
                    };
                    term2.term[field + '.partial'] = value.toLowerCase();
                    term3.term[field + '.metaphone'] = value;

                    return arr.push(term1, term2, term3);
                };

            queryGenerator(should, 'firstName', name.firstName);

            // If no last name provided than search all last names with the firstName value
            queryGenerator(should, 'lastName', name[(name.lastName ? 'last' : 'first') + 'Name']);

            return should;
        }
    };

exports.build = function (queryTypes, opts) {
    if (!opts.query || !Array.isArray(queryTypes) || !queryTypes.length) {
        return defaultQuery;
    }

    let hasLength = 0;
    const base = {
        bool: {
            must: []
        }
    };
    
    const builtQuery = filterQueryTypes(queryTypes, opts.query).reduce(function (query, queryType) {
        if (typeof fnOptions[queryType] === 'function' && fnOptions[queryType](query, opts.query, opts)) {
            hasLength++;
        }

        return query;
    }, base);

    return hasLength ? builtQuery : defaultQuery;
};