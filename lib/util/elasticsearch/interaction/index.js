"use strict";

const _           = require('lodash-node/modern');
const logger      = require('bragi');

const Fetch       = GLOBAL.Fetcher;

const commonGeneral = Fetch.common('util', 'general');

const Query       = Fetch.util('elasticsearch', 'interaction', 'query');
const Filter      = Fetch.util('elasticsearch', 'interaction', 'filter');
const Sort        = Fetch.util('elasticsearch', 'interaction', 'sort');

exports.buildQuery = Query.build;
exports.buildFilter = Filter.build;
exports.buildSort = Sort.build;

exports.reduceUsers = function (userIDs, exclude) {
    return _.reduce(userIDs, function (array, userID) {
        if (userID != exclude && array.indexOf(userID) === -1) {
            array.push(userID);
        }

        return array;
    }, []);
};

exports.reduceVenues = function (venues) {
    const venueIDs = [];

    return _.reduce(venues, function (array, venue) {
        if (venueIDs.indexOf(venue.vID) === -1) {
            venueIDs.push(venue.vID);
            array.push(venue);
        }

        return array;
    }, []);
};

/**
 * @description prepares an object for the database
 *
 * @param {object} requestData
 * @private
 */
exports.sanitize = {
    request: function (request) {
        if (request.message) {
            request.message = String(request.message).trim();
        }

        const reqObj = {
            status: 'pending',
            created: commonGeneral.format.dateString('basic_date_time_no_millis'),
            type: request.type,
            creator: request.creator,
            when: request.when,
            participants: request.participants,
            venue: request.venue,
            message: request.message,
        };

        if (request.oldReqID) {
            reqObj.oldReqID = request.oldReqID;
        }

        return reqObj;
    }
};
