'use strict';

const logger      = require('bragi');

const Fetch       = GLOBAL.Fetcher;

const Util        = Fetch.util('utility');
const commonGeneral = Fetch.common('util', 'general');

const defaultQuery    = {};

const fnOptions = {
    mine: function (query, opts) {
        if (!opts.userID) {
            logger.log('error:util:elasticsearch;interaction-mine',
                'mine filter made without userID');
            throw new Error();
        }

        return query.bool.must.push({
            term: {
                "participants": opts.userID
            }
        });
    },

    /**
     * @description Filters by all events with 'when' property AFTER 'opts.when' or now()
     *
     * @param {object} query - the current query object
     * @param {object} opts - argument data
     * @param {string=} opts.when - formatted as 'YYYYMMDDTHHmmssZ'
     * @return {object} original query with added filter
     */
    currentRequest: function (query, opts) {
        return query.bool.must.push({
            range: {
                when: {
                    gte: opts.when || commonGeneral.format.dateString('basic_date_time_no_millis')
                }
            }
        });
    },

    /**
     * @description Filters by all events with 'when' property BEFORE 'opts.when' or now()
     *
     * @param {object} query - the current query object
     * @param {object} opts - argument data
     * @param {string=} opts.when - formatted as 'YYYYMMDDTHHmmssZ'
     * @return {object} original query with added filter
     */
    pastRequest: function (query, opts) {
        return query.bool.must.push({
            range: {
                when: {
                    lte: opts.when || commonGeneral.format.dateString('basic_date_time_no_millis')
                }
            }
        });
    },

    /**
     * @description Transforms an array of date ranges into a query
     *
     * @param {object} query - the current query object
     * @param {object} opts - argument data
     * @param {array} opts.dateRange - Collection of objects containing:
     * @param {string} opts.date - basic_date_time_no_millis format string or Date Object
     * @param {String=} opts.operator - one of gte|gt|lte|lt (default:gte)
     */
    dateRange: function (query, opts) {
        if (!Array.isArray(opts.dateRange)) {
            return false;
        }

        return [].push.call(query.bool.must, opts.dateRange.map(function (range) {
            var value = range.date instanceof Date ?
                    commonGeneral.format.dateString('basic_date_time_no_millis', range.date)
                    :
                    range.date;

            const rangeObj = {
                range: {
                    when: {}
                }
            };

            rangeObj.range.when[range.operator || 'gte'] = value;

            return rangeObj;
        }));
    },

    requestOfStatus: function (query, opts) {
        if (!opts.ofStatus) {
            return false;
        }

        let must = {};

        // If only a single status passed as a string, then use the correct term/terms matcher
        must[typeof opts.ofStatus === 'string' ? 'term' : 'terms'] = {
            status: opts.ofStatus,
        };

        return query.bool.must.push(must);
    },
};

exports.build = function (filterTypes, opts) {
    var hasLength = 0,
        base = {
            bool: {
                must: []
            }
        },
        builtQuery;

    if (!Array.isArray(filterTypes) || !filterTypes.length) {
        return defaultQuery;
    }

    builtQuery = filterTypes.reduce(function (query, queryType) {
        if (typeof fnOptions[queryType] === 'function') {
            if (fnOptions[queryType](query, opts)) {
                hasLength++;
            }
        }

        return query;
    }, base);

    return hasLength ? builtQuery : defaultQuery;
};