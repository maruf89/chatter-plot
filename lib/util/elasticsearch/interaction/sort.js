'use strict';

const _   = require('lodash-node/modern');

const types = {

};

exports.build = function (sortTypes, data) {
    if (!Array.isArray(sortTypes) || !sortTypes.length) {
        return [];
    }

    data = data || {};

    const sort = [];

    return sortTypes.reduce(function (arr, type) {
        if (typeof types[type] === 'function') {
            return types[type](arr, data.opts || {});
        }
        return arr;
    }, sort);
};