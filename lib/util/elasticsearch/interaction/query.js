'use strict';

const defaultQuery = {
    match_all: {}
};

const queryChecks = {

};

const filterQueryTypes = function (queryTypes, queryString) {
    return queryTypes.filter(function (type) {
        return queryChecks[type] && queryChecks[type](queryString);
    });
};

const fnOptions = {

};

exports.build = function (queryTypes, opts) {
    if (!opts.query || !Array.isArray(queryTypes) || !queryTypes.length) {
        return defaultQuery;
    }

    let hasLength = 0;
    const base = {
        bool: {
            must: []
        }
    };

    const builtQuery = filterQueryTypes(queryTypes, opts.query).reduce(function (query, queryType) {
        if (typeof fnOptions[queryType] === 'function' &&
            fnOptions[queryType](query, opts.query, opts)
        ) {
            hasLength++;
        }

        return query;
    }, base);

    return hasLength ? builtQuery : defaultQuery;
};