'use strict';
var Fetch, Q, _, _query, logger, maria, objToInsertFormat, objToOrFormat,
    objToUpdateFormat, services;

Q = require('q');
_ = require('lodash-node/modern');
logger = require('bragi');

Fetch = GLOBAL.Fetcher;

maria = Fetch.database('mariadb');


/**
 * Hardcoded service specific keys pertaining
 * to the MariaDB tables' schemas
 *
 * @type {Object}
 */

services = {
    google: {
        serviceID: 'gplus_id',
        insertFields: ['gplus_id', 'access_token', 'refresh_token']
    },
    linkedin: {
        serviceID: 'linkedin_id',
        insertFields: ['linkedin_id', 'access_token']
    },
    facebook: {
        serviceID: 'fb_id',
        insertFields: ['fb_id', 'access_token']
    },
    user: {
        serviceID: 'user_id',
        insertFields: ['user_id', 'email', 'fb_id', 'gplus_id', 'linkedin_id']
    }
};

objToInsertFormat = function (obj) {
    let fields = [],
        values = [];

    _.each(Object.keys(obj), function (field) {
        fields.push(field);
        values.push(":" + field);
    });

    return {
        fields: fields.join(','),
        values: values.join(',')
    };
};


/**
 * Modifies the passed in object

 * @returns {string}        a formatted OR section
 */

objToOrFormat = function (obj, key, delimiter) {
    var vars;
    if (delimiter == null) {
        delimiter = ' OR ';
    }
    vars = [];
    _.each(obj[key], function (value, index) {
        var newKey;
        if (value === 'NULL') {
            return vars.push(key + " IS NULL");
        }
        newKey = key + index;
        obj[newKey] = value;
        return vars.push(key + " = :" + newKey);
    });
    return "(" + (vars.join(delimiter)) + ")";
};

objToUpdateFormat = function (obj, delimiter) {
    var vars;
    if (delimiter == null) {
        delimiter = ',';
    }
    vars = [];
    _.each(Object.keys(obj), function (key) {
        if (Array.isArray(obj[key])) {
            return vars.push(objToOrFormat(obj, key));
        }
        return vars.push(key + " = :" + key);
    });
    return vars.join(delimiter);
};


/**
 * An object with helper methods to use the
 * MariaDB table for Chatterplot
 *
 * @type {Object}
 */

module.exports = _query = {
    /**
     * Get Methods
     * @type {Object}
     */
    "get": {
        from: function (table, where, select) {
            table = table || 'users';
            select = (select || ['*']).join(',');

            let deferred = Q.defer(),
                whereString = objToUpdateFormat(where, ' AND '),
                query = "SELECT " + select + " FROM " + table + " WHERE " + whereString;

            logger.log('req:service:mysql-userHelper:get.from', 'querying MySQL');

            maria.query(query, where)
                .on('result', function (res) {
                    logger.log('res:service:mysql-userHelper:get.from', 'got response');
                    res.on('row', function (row) {
                        return deferred.resolve(row);
                    });

                    return res.on('end', function () {
                        return deferred.reject(404);
                    });
                })
                .on('error', function (err) {
                    logger.log('error:service:mysql-userHelper:get.from',
                        'query:', query, 'where:', where,
                        '\nerror:\n', err);

                    return deferred.reject(500);
                });

            return deferred.promise;
        },

        userByID: function (userID) {
            return _query.get.from('users', {
                user_id: userID
            });
        },

        /**
         * Get Users Chatterplot Id by their email
         *
         * @param  {string} email  address
         * @return {number}        User Id
         */
        userIdByEmail: function (email) {
            if (!email) {
                logger.log('error:util:mysql-userHelper:get-userIdByEmail',
                    'missing email parameter');
                return Q.reject(400);
            }

            let deferred = Q.defer();

            maria.query('SELECT user_id AS id FROM users WHERE email = :email LIMIT 1', {
                    email: email
                })
            .on('result', function (res) {
                return res.on('row', function (row) {
                        return deferred.resolve(row.id);
                    })
                    .on('end', function () {
                        return deferred.reject(404);
                    });
            })
            .on('error', function (err) {
                logger.log('error:util:mysql-userHelper:get-userIdByEmail', err);
                return deferred.reject(500);
            });

            return deferred.promise;
        },

        /**
         * Get user's Chatterplot row by their email
         *
         * @param  {string} email  address
         * @return {number}        User Id
         */
        userByEmail: function (email) {
            if (!email) {
                logger.log('error:util:mysql-userHelper:get-userByEmail',
                    'missing email parameter');

                return Q.reject(400);
            }

            let deferred = Q.defer();

            maria.query('SELECT * FROM users WHERE email = :email LIMIT 1', {
                    email: email
                })
                .on('result', function (res) {
                    return res.on('row', function (row) {
                            row.userID = row.user_id;
                            return deferred.resolve(row);
                        })
                        .on('end', function () {
                            return deferred.reject(404);
                        });
                })
                .on('error', function (err) {
                    logger.log('error:util:mysql-userHelper:get-userByEmail', err);
                    return deferred.reject(500);
                });
            return deferred.promise;
        },

        /**
         * Get Users Chatterplot Id by matching email + service Id. Useful for verifying
         * that the user belongs to both
         *
         * @param  {string}        email      address
         * @param  {string}        service    3rd party service (Facebook|Google|LinkdeIn)
         * @param  {String|Number} serviceID  Service specific Id
         * @return {number}                   User Id
         */
        userIdByServiceAndEmail: function (email, service, serviceID) {
            var ref, serviceField,
                deferred = Q.defer();

            if (!((ref = services[service]) != null ? ref.serviceID : void 0)) {
                logger.log(
                    'error:util:mysql-userHelper:get-userIdByServiceAndEmail',
                    'invalid service:', service, 'provided');
                return Q.reject(400);
            }

            serviceField = services[service].serviceID;
            maria.query("SELECT user_id AS id FROM users WHERE " +
                    serviceField + " = :serviceID AND email = :email", {
                        serviceField: serviceField,
                        serviceID: serviceID,
                        email: email
                    })
                .on('result', function (res) {
                    return res.on('row', function (row) {
                            return deferred.resolve(row.id);
                        })
                        .on('end', function () {
                            return deferred.reject(404);
                        });
                })
                .on('error', function (err) {
                    logger.log('error:util:mysql-userHelper:get-userIdByServiceAndEmail', err);
                    return deferred.reject(500);
                });

            return deferred.promise;
        }
    },

    /**
     * Insert into MariaDB
     *
     * @type {Object}
     */
    "insert": {
        /**
         * Insert users 3rd party service specific info into that table
         * So far supporting: Facebook, Google & LinkedIn
         *
         * @param  {string} service  3rd party service (Facebook|Google|LinkdeIn)
         * @param  {object} data     Keys => Values matching the tables Schema
         * @return {array}           On success returns array of the passed in values
         */
        userIntoService: function (service, data) {
            var deferred, fields, serviceKey, serviceObj, values;
            deferred = Q.defer();
            serviceObj = services[service];

            if (!(serviceObj != null ? serviceObj.serviceID : void 0)) {
                logger.log(
                    'error:util:mysql-userHelper:insert-userIntoService',
                    'invalid service:', service, 'provided');

                return Q.reject(400);
            }

            // Push the correct database key for the service to the data object 
            serviceKey = serviceObj.serviceID;

            if (data.serviceID) {
                data[serviceKey] = data.serviceID;
            }

            fields = [];
            values = [];

            _.each(serviceObj.insertFields, function (field, index) {
                if (data[field]) {
                    fields.push(field);
                    return values.push(":" + field);
                }
            });

            if (!fields.length) {
                logger.log(
                    'error:util:mysql-userHelper:insert-userIntoService',
                    'no valid data fields using:', data);

                return Q.reject(400);
            }

            fields = fields.join(',');
            values = values.join(',');

            maria.query(
                "INSERT IGNORE INTO " + service + " (" + fields +") VALUES (" + values + ")",
                data
            )
                .on('result', function (res) {
                    return res.on('end', function (results) {
                        return deferred.resolve([service, data]);
                    });
                })
                .on('error', function (err) {
                    logger.log('error:util:mysql-userHelper:insert-userIntoService', err);
                    return deferred.reject(500);
                });
            return deferred.promise;
        },

        intoTable: function (values, table) {
            table = table || 'users';

            let deferred = Q.defer(),
                insertData = objToInsertFormat(values, false),
                insertStatement =
                    "INSERT IGNORE INTO " + table + " (" + insertData.fields + ") VALUES (" + insertData.values + ")";

            maria.query(insertStatement, values)
                .on('result', function (res) {
                    return res.on('end', function (results) {
                        if (!results.affectedRows) {
                            let error = {
                                results: results,
                                query: insertStatement,
                                data: values,
                                message: "Error inserting into TABLE " +
                                    table
                            };

                            return deferred.reject(error);
                        }

                        return deferred.resolve(results.insertId);
                    });
                })
                .on('error', function (err) {
                    logger.log('error:util:mysql-userHelper:insert-intoTable', 'insert statement:', insertStatement, '\nvalues:', values);
                    logger.log('error:util:mysql-userHelper:insert-intoTable', err);
                    return deferred.reject(err);
                });

            return deferred.promise;
        }
    },

    /**
     * Update existing rows
     *
     * @type {Object}
     */
    "update": {

        /**
         * Updates the chatterplot sql table
         *
         * @param  {object}  updates   key/value correspond to the table name and update value
         * @param  {object}  where     same format as updates for where statement
         * @param  {String=} table     the table to perform the update on
         * @return   ----
         */
        userWithFields: function (updates, where, table) {
            var deferred, updateString, whereString;

            table = table || 'users';

            deferred = Q.defer();
            if (!_.isPlainObject(updates)) {
                logger.log(
                    'error:util:mysql-userHelper:update-userWithFields',
                    'no valid `updates` field passed as first parameter'
                );
                return Q.reject(400);
            }

            // If a number is passed, assume it's a userID 
            if (isFinite(+where)) {
                where = {
                    user_id: where
                };
            }
            if (!_.isPlainObject(where)) {
                logger.log(
                    'error:util:mysql-userHelper:update-userWithFields',
                    'missing second `where` parameter of type object');
                return Q.reject(400);
            }

            updateString = objToUpdateFormat(updates);
            whereString = objToUpdateFormat(where, ' AND ');
            maria.query(
                "UPDATE " + table + " SET " + updateString + " WHERE " + whereString,
                _.extend(updates, where)
            )
            .on('result', function (res) {
                return res.on('end', function (results) {
                    return deferred.resolve(results);
                });
            })
            .on('error', function (err) {
                logger.log('error:util:mysql-userHelper:update-userWithFields', err);
                return deferred.reject(err);
            });

            return deferred.promise;
        }
    },

    "delete": {
        from: function (table, where) {
            if (typeof table !== 'string') {
                logger.log('error:util:mysql-userHelper:delete-from', 'first parameter `table` must be of type string');
                return Q.reject(400);
            }

            let whereString = objToUpdateFormat(where, ' AND '),
                deferred = Q.defer();

            maria.query(
                "DELETE FROM " + table + " WHERE " + whereString,
                where
            )
                .on('result', function (res) {
                    return res.on('end', function (result) {
                        return deferred.resolve(result);
                    });
                })
                .on('error', function (err) {
                    logger.log('error:util:mysql-userHelper:delete-from', err);
                    return deferred.reject(err);
                });

            return deferred.promise;
        }
    }
};
