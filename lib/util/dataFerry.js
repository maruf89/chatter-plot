'use strict';

const _ = require('lodash-node/modern');
const Q = require('q');
const logger = require('bragi');

const Fetcher = GLOBAL.Fetcher;

const Emitter = new (require('events').EventEmitter)();
const SocketIo = Fetcher.connection('socket.io');

const CP = { Settings: {}};
const awaiting = [];

/**
 * Whether the dataFery is ready to send
 * @type {Boolean}
 */
let ready = true;

/**
 * Store a piece of data to be passed to the front end.
 *
 * If not ready, check the passed in key against the awaiting array
 * and set to ready if it fulfills the last awaiting
 *
 * @param  {string} key - Object Key
 * @param  {*} data - Arbitrary data to pass
 * @param  {boolean=} notify
 * @param  {string} notify.channel - the socket io channel to send this data to
 */
exports.store = function (key, data, notify) {
    CP.Settings[key] = data;

    // the channel to broadcast to if notify = true
    const STORE_NOTIFY_CHANNEL = SocketIo.generateChannelKey('settings/change');

    if (!ready) {
        let index = awaiting.indexOf(key);
        if (index !== -1) {
            if (awaiting.length === 1) {
                awaiting.length = 0;
                ready = true;
                Emitter.emit('/ready');
            }

            awaiting.splice(index, 1);
        }
    }

    if (notify) {
        let obj = {};
        obj[key] = data;
        SocketIo.emitToChannel(STORE_NOTIFY_CHANNEL, obj, null, true);
    }
};

/**
 * @description removes a key from the store
 * @param key
 */
exports.unset = function (key) {
    if (typeof CP.Settings[key] !== void 0) {
        delete CP.Settings[key];
    }
};


/**
 * Retrieve a key from the dataStore
 * @param  {string} key  Object Key
 * @return {*}           Key value if exists
 */
exports.get = function (key) {
    return CP.Settings[key];
};


/**
 * Let the dataFerry know to wait for this key before marking
 * itself as ready
 *
 * If no key passed will return the awaiting array
 *
 * @param  {string} key  Object Key to wait for
 * @return {boolean}     Whether the key was successfully stored
 */
exports.await = function (key) {
    if (!key) {
        return awaiting;
    }

    // Add the key
    if (awaiting.indexOf(key) === -1) {
        awaiting.push(key);

        // Mark self as not ready
        ready = false;
        return true;
    }

    return false;
};

exports["export"] = function () {
    return CP;
};

exports.exportOnReady = function () {
    if (ready) {
        return Q.resolve(exports.export());
    } else {
        let deferred = Q.defer();
        Emitter.once('/ready', function () {
            return deferred.resolve(exports.export());
        });

        return deferred.promise;
    }
};
