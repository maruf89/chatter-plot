'use strict';
const _ = require('lodash-node/modern'),
    crypto = require('crypto'),

    Fetch = GLOBAL.Fetcher,

    credentials = Fetch.config('socialServices').cloudinary,
    commonCloudinary = Fetch.common('util', 'cloudinary').init({
        cloudName: credentials.cloudName,
        typePostfix: process.env.NODE_ENV === 'production' ? null : Fetch.config('config', true, 'js').prefix
    }),
    cloudinary = exports;

exports.timestamp = commonCloudinary.timestamp;
exports.transform = commonCloudinary.transform;
exports.buildTransform = commonCloudinary.buildTransform;
exports.buildArray = commonCloudinary.buildArray;

exports.servicePhoto = commonCloudinary.servicePhoto;
exports.fbPhoto = _.partial(commonCloudinary.servicePhoto, 'facebook');

exports.buildUploadParams = function (options) {
    var params = {
        timestamp: commonCloudinary.timestamp()
    };

    if (options.uploadPreset) {
        params.upload_preset = options.uploadPreset;
    }
    if (options.eager) {
        params.eager = commonCloudinary.transform(options.eager);
    }
    if (options.transformation) {
        params.transformation = commonCloudinary.transform(options.transformation);
    }
    if (options.publicID) {
        params.public_id = options.publicID;
    }
    cloudinary.signRequest(params);
    return params;
};

exports.apiSignRequest = function (paramsToSign, apiSecret) {
    var toSign = _.sortBy((function () {
            let results = [],
                k;

            for (k in paramsToSign) {
                let v = paramsToSign[k];
                if (v !== null) {
                    results.push(k + "=" + (commonCloudinary.buildArray(v).join(',')));
                }
            }

            return results;
        })(), _.identity).join('&'),
        shasum = crypto.createHash('sha1');

    shasum.update(utf8Encode(toSign + apiSecret));
    return shasum.digest('hex');
};

exports.signRequest = function (params) {
    params.signature = cloudinary.apiSignRequest(params, credentials.apiSecret);
    params.api_key = credentials.apiKey;
    return params;
};

const utf8Encode = function (argString) {
    var c1, enc, end, n, start, string, stringl, utftext;
    if (argString == null) {
        return "";
    }
    string = argString + "";
    utftext = "";
    start = end = 0;
    stringl = string.length;
    n = 0;
    while (n < stringl) {
        c1 = string.charCodeAt(n);
        enc = null;
        if (c1 < 128) {
            end++;
        } else if (c1 > 127 && c1 < 2048) {
            enc = String.fromCharCode((c1 >> 6) | 192, (c1 & 63) | 128);
        } else {
            enc = String.fromCharCode((c1 >> 12) | 224, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128);
        }
        if (enc !== null) {
            if (end > start) {
                utftext += string.slice(start, end);
            }
            utftext += enc;
            start = end = n + 1;
        }
        n++;
    }
    if (end > start) {
        utftext += string.slice(start, stringl);
    }
    return utftext;
};
