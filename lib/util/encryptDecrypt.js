'use strict';

var crypto = require('crypto');

exports.encrypt = function (text) {
    var cipher, crypted;
    cipher = crypto.createCipher("aes-256-cbc", "d6F3Efeq");
    crypted = cipher.update(text, "utf8", "hex");
    crypted += cipher.final("hex");
    return crypted;
};

exports.decrypt = function (text) {
    var dec, decipher;
    decipher = crypto.createDecipher("aes-256-cbc", "d6F3Efeq");
    dec = decipher.update(text, "hex", "utf8");
    dec += decipher.final("utf8");
    return dec;
};
