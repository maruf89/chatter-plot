"use strict";

const PHOTO_SIZE_MAP = {
    42: 'thumb',
    150: 'default',
};

/**
 * @description accepts a users picture object and builds a desired image out of it
 *
 * @param {object} pictureObj - if hasPhoto is ture, then thumb/default will be ignored
 * @param {string=} pictureObj.thumb - 42x42 full image url
 * @param {string=} pictureObj.default - 150x150 full image url
 * @param {boolean=} pictureObj.hasPhoto - has cloudinary photo
 * @param {string=} pictureObj.service - if facebook or google
 * @param {(string|number)=} pictureObj.id - the user id corresponding to the service
 * @param {number} size - width (and height if height not explicity passed)
 * @param {number=} height
 * @param {string=} avatarFallback - (MALE|FEMALE) if passed will load a fallback avatar
 */
exports.buildPhoto = function (pictureObj, size, height, avatarFallback) {
    height = height || size;

    if (typeof pictureObj === 'object') {

    }

    if (typeof avatarFallback === 'string') {

    }
};