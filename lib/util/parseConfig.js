'use strict';
var Config, Fetch, _, funcArgsPattern, funcPattern, logger, parseFuncs, varPattern;

_ = require('lodash-node/modern');

logger = require('bragi');

Fetch = GLOBAL.Fetcher;

Config = Fetch.config('config', true, 'js');

funcPattern = /<\$=\s?([a-zA-Z_]+)\(?([a-zA-Z0-9,:\/\._\s']*)\)?\s?\$>/gi;

funcArgsPattern = /[^,\s]+/g;

varPattern = /<\%=\s?([a-zA-Z_]+)\s?\%>/gi;

parseFuncs = {
    random_number: function (numberLength) {
        if (numberLength == null) {
            numberLength = 10;
        }
        return Math.random()
            .toFixed(numberLength);
    },
    encode_url: function (url) {
        return encodeURIComponent(url);
    },

    /**
     * Returns either a process environment variable, or key from its own json object
     *
     * @param  {object} obj  its own JSON object
     * @param  {string} key  variable key
     * @return {string}      variable
     */
    getVal: function (obj, key) {
        return process.env[key] || obj[key];
    },
    getConfig: function (key) {
        return Config[key];
    },
    only_prod: function () {
        if (!(Config.env === 'production' || Config.env === 'staging')) {
            return '';
        }
        return parseFuncs._conditional.apply(this, arguments);
    },
    only_dev: function () {
        if (Config.env !== 'development') {
            return '';
        }
        return parseFuncs._conditional.apply(this, arguments);
    },
    _conditional: function () {
        var args, fullString;
        args = arguments;
        fullString = '';
        _.each(args, (function (_this) {
            return function (arg) {
                return fullString += parseFuncs.getVal(_this,
                    arg) || arg;
            };
        })(this));
        return fullString;
    }
};


/**
 * Parses a JSON config object for `function patterns` and
 * `variable patterns` as defined above and replaces them with
 * either **environment** variables or other key values defined
 * in the current config object. Functions are predefined in `parseFuncs'
 *
 * @param  {JSON} config  JSON configuration object
 * @return {JSON}         parsed/updated JSON configuration object
 */

exports.parse = function (config) {
    var error, retConfig;
    if (!_.isObject(config)) {
        logger.log({
            'error:util:parseConfig:parse': 'config parameter must be a json object'
        });
    }

    // Safety measure 
    retConfig = _.clone(config);
    error = null;
    _.each(retConfig, function (val, key) {

        // Next search for environment and previously declared variables 
        var _args, _func, _replace, _value, _variable, allVars, funcs,
            vars;
        vars = varPattern.exec(val);
        allVars = [];
        while (vars != null) {
            _replace = vars[0];
            _variable = vars[1];
            _value = parseFuncs.getVal(retConfig, _variable);
            val = val.replace(_replace, _value);
            vars = varPattern.exec(val) || varPattern.exec(val);
        }

        // first search for functions 
        funcs = funcPattern.exec(val);
        while (funcs != null) {
            _replace = funcs[0];
            _func = funcs[1];

            // Return an error if no method name exists 
            if (!parseFuncs[_func]) {
                logger.log({
                    'error:util:parseConfig:parse': "No such parsing method \"" +
                        _func + "\" exists for ParseConfig"
                });
            }
            _args = funcs.slice(2);

            // if any args do actually exist, split them on commas and spaces to make an array of params 
            if (_args.length) {
                _args = String(_args)
                    .match(funcArgsPattern);
            }

            // value is the result of the method and args 
            _value = parseFuncs[_func].apply(retConfig, _args);

            // result string is the result of replacing the whole pattern with the value 
            val = val.replace(_replace, _value);

            // check for more matches for while loop 
            funcs = funcPattern.exec(val);
        }
        return retConfig[key] = val;
    });
    return retConfig;
};
