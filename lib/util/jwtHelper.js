/**
 * @module bUtilJWTHelper
 */

'use strict';

var jwt = require('jsonwebtoken'),
    _ = require('lodash-node/modern'),
    moment = require('moment'),
    logger = require('bragi'),
    Q = require('q'),

    Fetch = GLOBAL.Fetcher,

    config = Fetch.config('config', true, 'js'),
    Redis = Fetch.database('redis').client,
    Util = Fetch.util('utility'),

    requiredFields = ['userID', 'email'];

var JWTHelper = (function (superClass) {
    Util.extendClass(JWTHelper, superClass);

    function JWTHelper() {
        return JWTHelper.__super__.constructor.apply(this, arguments);
    }


    /**
     * @description Generates a socket.io token for the given user
     * @name bUtilJWTHelper#tokenFromUser
     * @param  {object} userData
     * @return {string} A long token
     */
    JWTHelper.prototype.tokenFromUser = function (userData) {
        logger.log('util:jwtHelper:tokenFromUser', 'called');

        var expiresSeconds = config.token.ttl / 1000,
            expires = moment().add(config.token.ttl).valueOf(),
            missing = null,
            user = {},
            token;

        _.each(requiredFields, function (prop) {
            if (userData[prop]) {
                return user[prop] = userData[prop];
            } else {
                logger.log('error:util:jwtHelper:tokenFromUser', "Missing `" + prop + "` property for signing user");
                return missing = true;
            }
        });

        if (missing) {
            logger.log('error:util:jwtHelper:tokenFromUser', 'Missing parameters');
            return Q.reject({
                type: 500,
                reponse: 'updates.ERROR-MISSING_PARAMETERS'
            });
        }

        logger.log('req:util:jwtHelper:tokenFromUser', 'Generating token');
        token = jwt.sign(user, config.secretKey, {
            expiresInMinutes: expiresSeconds
        });

        if (typeof token !== 'string') {
            logger.log('error:util:jwtHelper:tokenFromUser', 'Error generating token');
            return Q.reject({
                type: 500,
                reponse: 'updates.ERROR-MISSING_PARAMETERS'
            })
        }

        logger.log('util:jwtHelper:tokenFromUser', 'Saving token in redis');
        Redis.set(token, 1, 'EX', expiresSeconds);

        this.emit('/user/tokenGenerated', {
            token: token,
            userID: userData.userID,
            expiresInSeconds: expiresSeconds,
            expires: expires
        });

        return Q.resolve(token);
    };


    /**
     * @description Generates a signed token
     * @name bUtilJWTHelper#sign
     * @param  {object}  data     arbitrary data to sign
     * @param  {Number=} expires  how many minutes the token should last before before expiring
     * @param  {String=} secret   Secret key to sign the token with
     * @return {string}           Signed token
     */
    JWTHelper.prototype.sign = function (data, expires, secret) {
        expires = expires || (config.token.ttl / 1000);
        secret = secret || config.secretKey;

        return jwt.sign(data, secret, expires);
    };


    /**
     * @description Decodes a jwt Token
     * @name bUtilJWTHelper#decode
     * @param  {string} token  the token to be decoded
     * @return {object}        the decoded token object
     */
    JWTHelper.prototype.decode = jwt.decode;

    return JWTHelper;

})(require('events').EventEmitter);

module.exports = new JWTHelper();
