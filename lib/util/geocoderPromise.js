'use strict';
var Q, _, _defaultFields, _fieldObjects, _googleURL, _preparer, _services, geocoder,
    logger, request;

Q = require('q');

geocoder = require('geocoder');

request = require('request');

logger = require('bragi');

_ = require('lodash-node/modern');

_googleURL =
    'http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=';


//_defaultFields = {
//    name: null,
//    coords: null,
//    address: null,
//    sublocality: null,
//    city: null,
//    state: null,
//    country: null,
//    postal: null
//};

//_fieldObjects = {
//    google: {
//        coords: function (place, key) {
//            return place.geometry.location[key];
//        },
//        isBusiness: function (place) {
//            return _.isArray(place.types) && place.types.indexOf('establishment') !== -1;
//        },
//        address: 'address_components',
//        fields: ['address', 'sublocality', 'city', 'state', 'country', 'postal'],
//        fieldsGetLong: ['locality', 'administrative_area_level_1'],
//        keys: {
//            "establishment": "locationName",
//            "street_number": 1,
//            "route": 2,
//            "sublocality_level_1": "sublocality",
//            "sublocality_level_2": "sublocality",
//            "neighbourhood": "sublocality",
//            "neighborhood": "sublocality",
//            "locality": "city",
//            "postal_town": "city",
//            "administrative_area_level_1": "state",
//            "country": "country",
//            "postal_code": "postal",
//            "tz": "tz"
//        },
//        placeID: 'sourceID',
//        specLevel: [{
//            level: 1,
//            type: 'street_address',
//            zoom: 16,
//            name: false,
//            radius: 1000
//        }, {
//            level: 1,
//            type: 'establishment',
//            zoom: 16,
//            name: true,
//            radius: 1000
//        }, {
//            level: 2,
//            type: 'neighbourhood',
//            zoom: 15,
//            name: true,
//            radius: 1650
//        }, {
//            level: 3,
//            type: 'postal_code',
//            zoom: 15,
//            name: true,
//            radius: 1650
//        }, {
//            level: 3,
//            type: 'sublocality_level_2',
//            zoom: 14,
//            name: true,
//            radius: 1715
//        }, {
//            level: 3,
//            type: 'sublocality_level_1',
//            zoom: 14,
//            name: true,
//            radius: 2520
//        }, {
//            level: 4,
//            type: 'locality',
//            zoom: 12,
//            name: true,
//            radius: 10001
//        }, {
//            level: 5,
//            type: 'administrative_area_level_2',
//            zoom: 10,
//            name: true,
//            radius: 30000
//        }, {
//            level: 6,
//            type: 'administrative_area_level_1',
//            zoom: 8,
//            name: false,
//            radius: 187500
//        }, {
//            level: 7,
//            type: 'country',
//            zoom: 6,
//            name: false,
//            radius: 270000
//        }],
//        defaultSpecifity: {
//            type: 'coords',
//            zoom: 15,
//            name: false,
//            radius: 1650
//        },
//        address2Spec: [{
//            attr: 'locationName',
//            type: 'establishment'
//        }, {
//            attr: 'sublocality',
//            type: 'neighbourhood'
//        }, {
//            attr: 'address',
//            type: 'street_address'
//        }, {
//            attr: 'postal',
//            type: 'postal_code'
//        }, {
//            attr: 'city',
//            type: 'locality'
//        }, {
//            attr: 'state',
//            type: 'administrative_area_level_1'
//        }, {
//            attr: 'country',
//            type: 'country'
//        }]
//    }
//};


/**
 * Object of service specific methads that add additional
 * metadata to the passed in objects
 *
 * @type {Object}
 */

_preparer = {
    google: function (place, location) {
        // Only save the name if it's an establishment 
        if (Array.isArray(place.types) && ~place.types.indexOf('establishment')) {
            location.name = place.name;
        }

        return location;
    }
};

exports.getCoords = function (place, service) {
    service = service || _getService(place);

    return {
        lat: _fieldObjects[service].coords(place, 'lat'),
        lon: _fieldObjects[service].coords(place, 'lng')
    };
},

exports.scrapePlace = function (place, service, getSpecificity) {
    var service = (service || 'google').toLocaleLowerCase(),
        street = [],
        fields = _fieldObjects[service],
        data = _.clone(_defaultFields),
        mod, specificity;

    // Get the services unique id
    data[service] = place[fields.idField];

    data.coords = exports.getCoords(place, service);
    data.type = fields.isBusiness(place) ? 'business' : 'home';

    _.each(place[fields.address], function (comp) {
        _.each(comp.types, function (type) {
            var key,

                // We want the long form of certain address fields (Get 'Potsdam' instead of 'P')
                propName;

            if (key = fields.keys[type]) {
                propName = fields.fieldsGetLong.indexOf(type) !== -1 ? 'long_name' : 'short_name';

                if (typeof key === 'number') {
                    return street[key - 1] = String(comp[propName]).trim();
                } else {
                    return data[key] = String(comp[propName]).trim();
                }
            }
        });
    });

    if (street.length) {
        data.address = street.join(' ');
    }

    if (data.sublocality) {
        // remove unnecessary text
        data.sublocality = data.sublocality.replace('Bezirk ', '');
    }

    if (place.tz) {
        // We must convert "GMT+0200" => 120
        mod = place.tz.substr(-5, 1);
        data.tz = (p(place.tz.substr(-4, 2)) * 60) * (mod === '-' ? -1 : 1);
    }

    if (getSpecificity) {
        specificity = exports.scrapeSpecificity(place, service);

        // check if we want just the string value here
        if (getSpecificity === 2) {
            data.specificity = specificity.type;
        } else {
            data.specObj = specificity;
        }
        data.coords.zoom = specificity.zoom;
    }

    return data;
};

exports.queryPlace = function (address) {
    var deferred, url;
    deferred = Q.defer();
    url = _googleURL + address.replace(/\s/g, '+');
    logger.log('req:geocoder:queryPlace', 'querying for:', url);
    request({
        method: 'GET',
        url: url,
        json: true
    }, function (err, res, body) {
        if (err) {
            return deferred.reject(err);
        }
        return deferred.resolve(body);
    });
    return deferred.promise;
};

exports.scrapeSpecificity = function (place, service) {
    var fields, specificity;
    fields = _fieldObjects[service];
    specificity = fields.defaultSpecifity;
    if (Array.isArray(place.types)) {
        _.each(fields.specLevel, function (level) {
            if (Array.isArray(place.types) && ~place.types.indexOf(level
                    .type)) {
                return specificity = level;
            }
        });
    }
    return specificity;
};

exports.geocode = Q.denodeify(geocoder.geocode.bind(geocoder));

exports.reverseGeocode = Q.denodeify(geocoder.reverseGeocode.bind(geocoder));


/**
 * Performs a google query to retrieve an address for a certain location
 *
 * @param  {object} latLng     Object containing either 'lat/lon' or 'latitude/longitude'
 * @returns {object}            Google response
 */

exports.latlng2Address = function (latLng) {
    var deferred;
    deferred = Q.defer();
    if (typeof latLng === 'object') {
        latLng = (latLng.lat || latLng.latitude) + ',' + (latLng.lon || latLng.longitude);
    }
    request({
        url: "https://maps.googleapis.com/maps/api/geocode/json?latlng=" +
            latLng + "&sensor=true",
        json: true
    }, function (err, res, body) {
        if (err) {
            return deferred.reject(err);
        }
        return deferred.resolve(body.results);
    });
    return deferred.promise;
};
