/**
 * @module Utility
 */

'use strict';

const Fetch = GLOBAL.Fetcher,

    commonGeneral = Fetch.common('util', 'general'),
    commonMap = Fetch.common('util', 'map'),
    commonVenue = Fetch.common('util', 'venue', 'index'),

    _ = require('lodash-node/modern'),
    crypto = require('crypto'),
    moment = require('moment'),

    now = moment(),
    utils = exports,

    hasProp = {}.hasOwnProperty;

exports.map = commonMap;
exports.venue = commonVenue;
exports.format = commonGeneral.format;
exports.string = commonGeneral.string;
exports.array = commonGeneral.array;

/**
 * @description Checks that the passed in object has all of the fields passed in
 * @name Utility#verifyHas
 * @param  {object} obj     Object to test
 * @param  {array}  fields  Array of {string} fields to check the object for
 * @param  {boolean} returnNew   Whether to return a new object with just the passed in fields
 * @return {Object}
 *                  {Array/Boolean} missing   Array of missing fields or false
 *                  {boolean}       green     Whether we're good
 *                  {object}        obj       If returnNew is true, will return the slimmed object
 */
exports.verifyHas = function (obj, fields, returnNew) {
    var res = {
        missing: [],
        green: true
    };
    if (returnNew) {
        res.obj = {};
    }
    _.each(fields, function (prop) {
        if (!obj[prop]) {
            res.green = false;
            if (withError) {
                return missing.push(new Error("Missing `" + prop + "` property"));
            } else {
                return missing.push(prop);
            }
        } else {
            if (returnNew) {
                return res.obj[prop] = obj[prop];
            }
        }
    });
    return res;
};

exports.hashPassword = function (password) {
    return crypto.createHash('md5').update(password).digest('hex');
};

/**
 * @description Prepares an object for MySQL Users field.
 * COUPLED: with
 * @name Utility#parseMySQLFields
 * @param  {object} obj  to parse
 * @return {object}      parsed object
 */
exports.parseMySQLFields = function (obj) {
    let copy = _.clone(obj);
    if (obj.hasOwnProperty('password')) {
        if (obj.password) {
            copy.password = utils.hashPassword(obj.password);
        } else {
            copy.password = ['', 'NULL'];
        }
    }
    return copy;
};


/**
 * @description Joins 2 object based on a common key
 * @name Utility#objectJoins
 * @param  {object} obj1      [description]
 * @param  {Function} selector1 [description]
 * @param  {object} obj2      [description]
 * @param  {Function} selector2 [description]
 * @return {object}           [description]
 */
exports.objectJoins = function (obj1, selector1, obj2, selector2) {
    _.each(obj1, function (_obj1) {
        var _selector1 = selector1(_obj1);
        return _.each(obj2, function (_obj2) {
            if (_selector1 === selector2(_obj2)) {
                _.extend(_obj1, _obj2);
                return false;
            }
        });
    });
    return obj1;
};

/**
 * @deprecated
 * use commonMap.bounds2Diameter
 */
exports.bounds2Diameter = commonMap.bounds2Diameter;

/**
 * @deprecated
 * use commonVenue.addressObjToUrl
 */
exports.eventToGMapUrl = _.partial(commonVenue.addressObjToUrl, 'google');

/**
 * @deprecated
 * use commonVenue.objToAddress
 */
exports.getAddress = commonVenue.objToAddress;

/**
 * @deprecated
 * use commonMap.normalizeCoords
 */
exports.normalizeCoords = commonMap.normalizeCoords;

/**
 * @deprecated
 * use commonGeneral.format.dateString
 */
exports.formatDateString = commonGeneral.format.dateString;


exports.offsetTimeZone = function (time, tz) {
    return time.utcOffset(tz).add(now.utcOffset() - tz, 'minute');
};

exports.user = {
    /**
     * @description transforms a users full name into a first & last name
     * @name Utility.user#destructName
     * @param {string} fullName
     * @returns {{lastName: string, firstName: string}}
     */
    destructName: function (fullName) {
        fullName = fullName.trim();
        var parts = fullName.split(' ');

        // If we only have 1 thing then it must be the name
        if (parts.length === 1) {
            return {
                firstName: parts[0]
            };
        }

        return {
            lastName: parts.pop(),
            firstName: parts.join(' ')
        };
    }
};

// Copied straight from coffescript
exports.extendClass = function (child, parent) {
    for (var key in parent) {
        if (hasProp.call(parent, key)) child[key] = parent[key];
    }

    function ctor() {
        this.constructor = child;
    }

    ctor.prototype = parent.prototype;
    child.prototype = new ctor();
    child.__super__ = parent.prototype;
    return child;
};
