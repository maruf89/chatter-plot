'use strict';

const _ = require('lodash-node/modern');
const Q = require('q');
const logger = require('bragi');

const Fetch = GLOBAL.Fetcher;

const config = Fetch.config('config', true, 'js');
const Constants = Fetch.config('misc/constants');
const esUser = Fetch.util('elasticsearch', 'userHelper');
const Util = Fetch.util('utility');
const SocketIo = Fetch.connection('socket.io');
const User = Fetch.type('user');
const Message = Fetch.type('message');
const interactionController = Fetch.controller('interaction');
const Permissions = Fetch.service('permissions');
const elasticsearch = Fetch.database('elasticsearch');
const ESClient = elasticsearch.client;
const utilSearchHelper = Fetch.util('elasticsearch', 'helper');

logger.log('init:socket:message', 'init');

exports.onSecureSocketConnection = function () {
    const socket = this;
    let userPermissions = socket.userPermissions;

    /**
     * @description !Settings - Returns a users settings if they very from the default settings
     *
     * @return {object}      Users settings if they exist
     */
    socket.on('/message/send>', function (message) {
        logger.log('s-socket:message:send', 'request initiated');
        if (!Permissions.can('SEND_MSG', userPermissions)) {
            return socket.emit('/message/send<', [{
                type: 'updates.ERROR-CONFIRM_FIRST',
                response: 403
            }]);
        }
        if (!Array.isArray(message.to)) {
            logger.log('s-socket:message:send', 'to property must be an array');
            return socket.emit('/message/send<', [{
                type: 'updates.ERROR-MISSING_REQUIRED_FIELDS',
                response: 400
            }]);
        }

        message.from = socket.decoded_token.userID;
        
        return interactionController.sendMessageProcess(message).then(function (msgID) {
            logger.log('res:s-socket:message:send', 'Successfully sent message');
            return socket.emit('/message/send<', [null, msgID]);
        })["catch"](function (err) {
            logger.log('error:res:s-socket:message:send', 'error sending message', err);
            return socket.emit('/message/send<', [Constants.RESPONSE['500']]);
        });
    });

    /**
     * Fetches a single conversation from the server
     * @deprecated in favor of /interaction/get
     */
    socket.on('/message/get>', function (opts) {
        var onSuccess, participants, promises, response, sent;
        if (typeof opts !== 'object' || !opts.msgID) {
            logger.log('error:s-socket:message:get', 'expected to receive and object with `msgID` parameter as the argument');
            return socket.emit('/message/get<', [Constants.RESPONSE['400']]);
        }
        logger.log('s-socket:', 'Fetching users messages');

        opts.userID = socket.decoded_token.userID;
        sent = false;
        response = {};
        participants = interactionController.getParticipants(opts.msgID);

        // Check that either the user is part of this conversation OR the user has permissions to view
        if (!~participants.indexOf(opts.userID) && !Permissions.can('ABUSE_VIEW', userPermissions)) {
            return socket.emit('/message/get<', [Constants.RESPONSE['403']]);
        }

        promises = [];

        if (opts.messages) {
            promises.push(Message.getMessages(opts.msgID, opts).then(utilSearchHelper.searchExtractSource));
        }
        if (opts.conversation) {
            promises.push(Message.get(opts.msgID, opts).then(utilSearchHelper.getExtractSource));
        }
        if (opts.withUsers) {
            promises.push(esUser.mget(participants, {
                _source: ['firstName', 'lastName', 'picture', 'details.gender']
            }).then(elasticsearch.mgetIdMap));
        }

        onSuccess = function () {
            return socket.emit('/message/get<', [null, response]);
        };

        logger.log('req:s-socket:message:get', 'requesting conversation');
        return Q.all(promises).then(function (results) {
            logger.log('res:s-socket:message:get', 'recived conversation');
            if (opts.messages) {
                response.messages = results.shift();
            }
            if (opts.conversation) {
                response.conversations = results.shift();
            }
            if (opts.withUsers) {
                response.people = results.shift();
            }
            return sent = onSuccess();
        }, function (err) {
            logger.log('error:s-socket:message:get', 'error fetching conversation using opts:', opts, '\nerror:', err);
            throw sent = socket.emit('/message/get<', [err]);
        });
    });

    /**
     * Returns all relevant messages relating to a specific user
     */
    socket.on('/message/fetch>', function (opts) {
        opts = opts || {};

        logger.log('s-socket:', 'Fetching users messages');

        opts.sort = opts.sort || 'updated:desc';

        // When we add admin to load another users messages, update this
        const user = opts.userID = socket.decoded_token.userID;
        const withUsers = opts.withUsers;
        const response = {};
        const onSuccess = function () {
            return socket.emit('/message/fetch<', [null, response]);
        };
        let sent = false;

        return Message.fetch(opts)

        .then(function (conversations) {
            var userArrs, userGet;
            logger.log('res:s-socket:', 'Successfully retrieved users messages');

            response.conversations = _.pluck(conversations.hits.hits, '_source');
            response.total = conversations.hits.total;

            if (!withUsers || !response.conversations.length) {
                return sent = onSuccess();
            }

            userArrs = _.pluck(response.conversations, 'participants');
            userGet = userArrs.reduce(function (prev, next) {
                prev = prev.concat(next);
                return prev;
            }, []);

            userGet = _.uniq(userGet).filter(function (userID) {
                return userID && userID !== user;
            });

            logger.log('req:s-socket:message:fetch', 'Fetching related users with messages');
            return esUser.mget(userGet, {
                _source: ['firstName', 'lastName', 'picture', 'details.gender']
            })

            .then(elasticsearch.mgetIdMap);
        }, function (err) {
            logger.log('error:s-socket:message:fetch', 'error receiving users messages for user:', user, '\nerror:\n', err);
            throw sent = socket.emit('/message/fetch<', [err]);
        })

        // Optional get related users
        .then(function (users) {
            if (sent) {
                return sent;
            }
            logger.log('res:s-socket:message:fetch', 'received users from related messages');
            response.users = users;
            return sent = onSuccess();
        }, function (err) {
            if (sent) {
                throw sent;
            }
            logger.log('error:s-socket:message:fetch', 'error gettings related users from userID:', user, '\nerror:\n', err);
            throw sent = socket.emit('/message/fetch<', [err]);
        });
    });

    socket.on('/message/markRead>', function (msgID) {
        if (!msgID) {
            logger.log('error:s-socket:message:markRead', 'Missing msgID parameter');
            return socket.emit('/message/markRead<', [Constants.RESPONSE['500']]);
        }

        logger.log('req:s-socket:message:markRead', 'Marking message as read');
        return interactionController.markAsRead([{
            userID: socket.decoded_token.userID,
            _id: msgID
        }]).then(function () {
            logger.log('res:s-socket:message:markRead', 'Successfully marked message as read');
            return socket.emit('/message/markRead<', [null, Constants.RESPONSE['200']]);
        }, function (err) {
            logger.log('error:s-socket:message:markRead', 'Error marking msgID:', msgID, 'as read\nerror:\n', err);
            return socket.emit('/message/markRead<', [Constants.RESPONSE['500']]);
        });
    });
};
