'use strict';

const _ = require('lodash-node/modern'),
    Q = require('q'),
    logger = require('bragi'),

    Fetch = GLOBAL.Fetcher,

    config = Fetch.config('config', true, 'js'),
    sqlUser = Fetch.util('mysql', 'userHelper'),
    esUser = Fetch.util('elasticsearch', 'userHelper'),
    Email = Fetch.service('email'),
    emailController = Fetch.controller('email'),

    // Load Elasticsearch client
    Constants = Fetch.config('misc/constants');

logger.log('init:socket:email', 'init');

exports.onSocketConnection = function () {
    const socket = this;

    /**
     * !Exists - Checks whether an e-mail exists in MySQL
     * 
     * @param  {string} email  Bare email string to check against
     * @return {Boolean}
     */
    socket.on('/email/exists>', function (email) {
        if (!email) {
            return socket.emit('/email/exists<', [{
                type: 'updates.ERROR-REQUEST_EMPTY',
                response: 400
            }]);
        }

        logger.log('req:us-socket:email:exists', 'Checking if email exists in MySQL');

        return sqlUser.get.from('users', { email: email}, ['user_id'])
        .then(function (res) {
            logger.log('res:us-socket:email:exists', 'exists');
            return socket.emit('/email/exists<', [null, true]);
        }, function (err) {
            logger.log('res:us-socket:email:exists', 'doesn\'t exist');
            return socket.emit('/email/exists<', [null, false]);
        });
    });

    /**
     * !Reset - Sends a password reset e-mail with a 24 hour expiration time
     * 
     * @param  {object} userData  Object containing `email`
     * @return {boolean}          True if successful
     */
    socket.on('/email/password-reset>', function (userData) {
        var email = userData.email;

        if (!email) {
            return socket.emit('/email/password-reset<', [{
                type: 'updates.ERROR-REQUEST_EMPTY',
                response: 400
            }]);
        }

        logger.log('req:us-socket:email:password-reset', 'Checking if email exists in MySQL');
        return sqlUser.get.from('users', { email: email }, ['user_id'])
        .then(function (res) {
            logger.log('res:us-socket:email:password-reset', 'User exists in MySQL');

            userData.userID = res.user_id;
            logger.log('req:us-socket:email:password-reset', 'Triggering email password reset');

            return Email.resetPassword(userData);
        })["catch"](function (err) {
            logger.log('error:us-socket:email:password-reset', 'Error sending password reset email', err);
            throw socket.emit('/email/password-reset<', [{
                response: 404,
                type: 'updates.ERROR-NO_SUCH_REGISTER_EMAIL'
            }]);
        })
        .then(function () {
            // email sent
            logger.log('req:us-socket:email:password-reset', 'Email successfully sent');
            return socket.emit('/email/password-reset<', [null, true]);
        });
    });

    /**
     * @description sends a contact  form message to the team
     */
    socket.on('/email/message-form>', function (messageData) {
        if (!messageData.type) {
            logger.log('warning:us-socket:email:message-form', 'missing type');
            return socket.emit('/email/message-form<', [Constants.RESPONSE['400']]);
        }

        return emailController.contactMessage(messageData)
        .then(function () {
            logger.log('res:us-socket:email:message-form', 'successfully sent email to the team');
            return socket.emit('/email/message-form<', [null, {
                type: 'updates.SENT',
                response: 200
            }]);
        }).catch(function (err) {
            return socket.emit('/email/message-form<', [err]);
        })
    });
};

exports.onSecureSocketConnection = function () {
    const socket = this;
    let userPermissions = socket.userPermissions;

    /**
     * Resends an activation email
     * 
     * @return {object}  Object containing response + message
     */
    return socket.on('/email/activation/resend>', function () {
        logger.log('req:s-socket:email:activation-resend',
            'Get the user and his activation status form ES and MySQL'
        );
        return Q.all([
                esUser.retrieveUserById(socket.decoded_token.userID),
                sqlUser.get.from('users', {
                    user_id: socket.decoded_token.userID
                }, ['activated'])
            ])
            .then(function (response) {

                // Double check that the user is in fact not activated then send activation 
                var res, sql, user;
                logger.log('res:s-socket:email:activation-resend',
                    'Got user + permissions');
                user = response[0], sql = response[1];
                if (parseInt(sql.activated, 10) === 1) {
                    logger.log('s-socket:email:activation-resend', 'Account already activated');

                    res = {
                        notify: {
                            title: 'auth.ACCNT_ACTIVATED',
                            body: 'auth.ACCNT_ACTIVATED_BODY',
                            type: 'info'
                        },
                        success: true,
                        remove: true
                    };

                    return socket.emit('/email/activation/resend<', [
                        null, res
                    ]);
                }
                logger.log('req:s-socket:email:activation-resend', 'Activating user');
                return Email.activation(user);
            })
            .then(function () {
                // Let the user know that another e-mail has been sent 
                logger.log('res:s-socket:email:activation-resend',
                    'Email activated. Emitting to front-end');

                let res = {
                    response: 200,
                    type: 'auth.ACTIVATION_RESENT'
                };

                return socket.emit('/email/activation/resend<', [null, res]);
            })["catch"](function (err) {
                logger.log('error:s-socket:email:activation-resend', 'Error retrieving user from Elasticsearch or MySQL trying to get `userData` and `activate` and resending `activation` email\n\n', err);

                return socket.emit('/email/activation/resend<', [Constants.RESPONSE['500']]);
            });
    });
};
