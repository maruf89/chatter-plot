'use strict';

const _ = require('lodash-node/modern'),
    logger = require('bragi'),

    Fetch = GLOBAL.Fetcher,

    venueController = Fetch.controller('venue'),
    utilSearchHelper = Fetch.util('elasticsearch', 'helper'),
    Constants = Fetch.config('misc/constants');

logger.log('init:socket:venue', 'loaded');

exports.onSocketConnection = function () {
    var socket = this;

    /**
     * Grabs venues by their eID (eventID)
     *
     * @param  {array} ids
     * @return {array}      Array of object venues
     */
    socket.on('/venue/get>', function (ids) {
        logger.log('socket:venue:get', 'called');

        if (!Array.isArray(ids)) {
            logger.log('error:socket:venue:get',
                'Expected passed in argument to be an array. instead got', typeof ids);

            return socket.emit('/venue/get<', [Constants.RESPONSE['400']]);
        }

        logger.log('req:socket:venue:get', 'Requesting venues from Elastcisearch');

        return venueController.getMultiple(ids).then(utilSearchHelper.mgetExtractSource)

        .then(function (venues) {
            logger.log('res:socket:venue:get', 'Successfully fetched venues from Elasticsearch');

            return socket.emit('/venue/get<', [null, venues]);
        }, function (err) {
            logger.log('error:socket:venue:get',
                'Error fetching venues from elastcisearch with IDs:',
                ids, '\nerror:\n', err);

            return socket.emit('/venue/get<', [Constants.RESPONSE['500']]);
        });
    });

    /**
     * Checks for the existence of venues. If don't exist will save.
     * Will return locations with `vid` attached back to client
     *
     * @param  {array} venues  Array of venues to potentially save
     * @return {array}         Array of venues w/ Venue ID's (vid)
     */
    socket.on('/venue/save>', function (venues) {
        if (!Array.isArray(venues) || typeof venues[0] !== 'object') {
            return socket.emit('/venue/save<', [Constants.RESPONSE['400']]);
        }

        logger.log('req:socket:venue:save', 'Saving Venues');
        return venueController.safeSave(venues).then(function (fullVenues) {
            logger.log('res:socket:venue:save','Saved/Fetched venues to elasticsearch');
            return socket.emit('/venue/save<', [null, fullVenues]);
        }, function (err) {
            logger.log('error:socket:venue:save',
                'Error Saving/Fetching venues to elasticsearch using:', venues,
                '\nerror:\n', err);

            return socket.emit('/venue/save<', [Constants.RESPONSE['500']]);
        });
    });

    socket.on('/venue/getTz>', function (vID) {
        return venueController.get(vID, ['tz']).then(function (res) {
            socket.emit('/venue/getTz<', [null, res._source.tz]);
        }).catch(function (err) {
            logger.log('error:socket:venue:getTz', 'Error getting timezone of venue\nerr:\n', err);
            socket.emit('/venue/getTz<', [Constants.RESPONSE['500']]);
        });
    });
};

exports.onSecureSocketConnection = function () {
    var socket = this;
};
