"use strict";

const logger = require('bragi');

exports.routeForbidden = function (socket, route) {
    logger.log('error:s-socket:route', 'Forbidden Route', route);
    return socket.emit(route, [{
        response: 403,
        type: 'updates.ERROR-FORBIDDEN_ROUTE'
    }]);
};
