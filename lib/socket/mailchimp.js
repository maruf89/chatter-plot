/**
 * NOT CURRENTLY IN USE
 *
 * To readd: readd in socketRoutes.js
 */

'use strict';
var Email, Fetch, Q, _, esUser, logger, mcHelper;

_ = require('lodash-node/modern');

Q = require('q');

logger = require('bragi');

Fetch = GLOBAL.Fetcher;

esUser = Fetch.util('elasticsearch', 'userHelper');

mcHelper = Fetch.controller('mailchimp');

Email = Fetch.service('email');

logger.log('init:socket:mailchimp', 'init');

exports.onSocketConnection = function () {
    var socket = this;

    /**
     * Called on Mailing list user signup, which in turn adds the user to MySQL
     * as a mail only user
     * 
     * @param  {object} user  contains users `email`
     */
    socket.on('/mailchimp/addUser>', function (user) {
        var errorMsg, mcResponse;
        logger.log('us-socket:mailchimp:addUser', 'request initiated');
        user.mergeVars = user.mergeVars || {};
        user.mergeVars.groupings = user.mergeVars.groupings || [];
        user.mergeVars.groupings.push(mcHelper.getEmailGrouping('news'));
        errorMsg = 'Error adding user to the mailing list.';
        mcResponse = null;

        // Add user to Mailchimp 
        logger.log('req:us-socket:mailchimp:addUser',
            'adding user to Mailchimp', user.email);
        return mcHelper.addUser(null, user)
            .then(function (resp) {

                // Then add to Elasticsearch 
                logger.log('res:us-socket:mailchimp:addUser',
                    'User added to Mailchimp. Adding to ES Mailing list'
                );
                mcResponse = resp;
                return esUser.mailingList({
                    email: mcResponse.email
                }, mcResponse.userID);
            }, function (err) {
                logger.log('error:us-socket:mailchimp:addUser',
                    'Error adding user to Mailchimp or MySql using:',
                    user);
                return socket.emit('/mailchimp/addUser<', [
                    'Error adding user...'
                ]);
            })
            .then(function () {

                // Then send welcome email 
                logger.log('res:us-socket:mailchimp:addUser',
                    'User added to ES Mailing list. Sending Welcome Email via Mandrill'
                );
                return Email.welcomeEmail([mcResponse.email]);
            }, function (err) {
                logger.log('error:us-socket:mailchimp:addUser',
                    'Error adding user to ES Mailing List');
                return socket.emit('/mailchimp/addUser<', [
                    'Error adding user...'
                ]);
            })
            .then(function () {

                // then send response 
                logger.log('res:us-socket:mailchimp:addUser',
                    'Email successfully sent out via Mandrill. Emitting to front-end'
                );
                return socket.emit('/mailchimp/addUser<', [null,
                    mcResponse
                ]);
            }, function (err) {
                logger.log('error:us-socket:mailchimp:addUser',
                    'Error sending email via Mandrill');
                return socket.emit('/mailchimp/addUser<', [null,
                    mcResponse
                ]);
            });
    });
};
