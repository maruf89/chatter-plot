'use strict';
var Constants, Fetch, User, _, _userGroups, config, esUser, logger, permissions,
    sqlUser;

_ = require('lodash-node/modern');

logger = require('bragi');

Fetch = GLOBAL.Fetcher;

config = Fetch.config('config', true, 'js');

User = Fetch.type('user');

permissions = Fetch.service('permissions');

sqlUser = Fetch.util('mysql', 'userHelper');

esUser = Fetch.util('elasticsearch', 'userHelper');

Constants = Fetch.config('misc/constants');

logger.log('init:socket:account', 'init');

_userGroups = null;

exports.onSocketConnection = function () {
    var socket;
    socket = this;
    return logger.log('us-socket:account:connection',
        'account socket connection', socket.id);
};

exports.onSecureSocketConnection = function () {
    let socket = this,
        userPermissions = socket.userPermissions;

    //socket.on('/account/upgrade/host>', function (user) {
    //    var groupIndex;
    //    logger.log('s-socket:account:upgrade-host', 'request initiated');
    //    _userGroups = _userGroups || permissions.getUserGroups();
    //    groupIndex = _userGroups['host'].id;
    //    return User.updateUserPermissions(user.userID, groupIndex,
    //            userPermissions)
    //        .then(function (newPermissions) {
    //            userPermissions = socket.userPermissions =
    //                newPermissions;
    //            return socket.emit('/account/upgrade/host<', [null,
    //                newPermissions
    //            ]);
    //        }, function (err) {
    //            logger.log('error:s-socket:account:upgrade-host',
    //                'Error upgrading user to host using', user,
    //                '\nerror:\n', err);
    //            throw socket.emit('/account/upgrade/host<', [
    //                Constants.RESPONSE['500']
    //            ]);
    //        });
    //});

    //socket.on('/account/preUpgrade>', function (opts) {
    //    var preRole, role, sent, userID;
    //    logger.log('s-socket:account:preUpgrade', 'Preupgrading user');
    //    if (!_.isPlainObject(opts)) {
    //        logger.log('error:s-socket:account:preUpgrade',
    //            'Expecting parameter to be an object');
    //        return socket.emit('/account/preUpgrade<', [Constants.RESPONSE[
    //            '400']]);
    //    }
    //    role = opts.role;
    //    userID = socket.decoded_token.userID;
    //    if (typeof role !== 'string') {
    //        logger.log('error:s-socket:account:preUpgrade',
    //            'Expecting a {String} `role` parameter');
    //        return socket.emit('/account/preUpgrade<', [Constants.RESPONSE[
    //            '400']]);
    //    }
    //    preRole = 'pre_' + role;
    //    sent = false;
    //
    //    // Get the pre role ID
    //    logger.log('res:s-socket:account:preUpgrade', 'Searching for preRole in userGroups');
    //    return sqlUser.get.from('user_groups', {
    //        name: preRole
    //    }, ['id'])
    //    .then(function (row) {
    //        logger.log('res:s-socket:account:preUpgrade',
    //            'Found role in userGroups, now updating user in ES'
    //        );
    //        return esUser.updateUser({
    //            _id: userID
    //        }, {
    //            userGroup: row.id
    //        });
    //    }, function (err) {
    //        logger.log('error:s-socket:account:preUpgrade',
    //            'Role not found in userGroups \nerror:\n',
    //            err);
    //        throw sent = socket.emit('/account/preUpgrade<', [
    //            Constants.RESPONSE['404']
    //        ]);
    //    })
    //    .then(function () {
    //        // On user pre upgrade in ES
    //        logger.log('res:s-socket:account:preUpgrade',
    //            'Successfully updated users group');
    //        return sent = socket.emit('/account/preUpgrade<', [
    //            null, true
    //        ]);
    //    }, function (err) {
    //        if (sent) {
    //            throw sent;
    //        }
    //        logger.log('error:s-socket:account:preUpgrade',
    //            'Error updating user \nerror:\n', err);
    //        throw sent = socket.emit('/account/preUpgrade<', [
    //            Constants.RESPONSE['500']
    //        ]);
    //    });
    //});
};
