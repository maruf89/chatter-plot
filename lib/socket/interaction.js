'use strict';

const _ = require('lodash-node/modern');
const logger = require('bragi');

const Fetch = GLOBAL.Fetcher;

const interactionController = Fetch.controller('interaction');
const Constants = Fetch.config('misc/constants');
const socketUtil = Fetch.socket('socketUtility');
const Permissions = Fetch.service('permissions');
const commonInteraction = Fetch.common('type', 'interaction');


logger.log('init:socket:interaction', 'init');

exports.onSecureSocketConnection = function () {
    const socket = this;
    let userPermissions = socket.userPermissions;

    /**
     * @description gets either message or request related documents
     */
    socket.on('/interaction/get>', function (opts) {
        if (typeof opts !== 'object' || !opts.objID) {
            logger.log('error:s-socket:interaction:get',
                'expected to receive and object with `msgID` parameter as the argument');
            return socket.emit('/interaction/get<', [Constants.RESPONSE['400']]);
        }

        opts.userID = socket.decoded_token.userID;

        // make sure that the user has the permissions to fetch this interaction
        if (!commonInteraction.isParticipant(opts.objID, opts.userID) &&
            !Permissions.can('ABUSE_VIEW', userPermissions)
        ) {
            return socket.emit('/interaction/get<', [Constants.RESPONSE['403']]);
        }

        opts.type = opts.type ? opts.type.toUpperCase() : commonInteraction.getIndexType(objID);

        logger.log('req:s-socket:interaction:get', 'requesting interaction');
        return interactionController.get(opts.objID, opts).then(function (results) {
            logger.log('res:s-socket:interaction:get', 'received interaction');

            return socket.emit('/interaction/get<', [null, results]);
        }).catch(function (err) {
            logger.log('error:s-socket:interaction:get', 'error requesting interaction');
            return socket.emit('/interaction/get<', [err]);
        })
    });

    socket.on('/interaction/request/new>', function (request) {
        // Ensure that request is an object with the necessary fields
        if (!(typeof request === 'object' && request.type && request.creator && request.participants)) {
            logger.log('warning:s-socket:interaction:request-new', 'missing either type, creator or participants');
            return socket.emit('/interaction/request/new<', [Constants.RESPONSE['400']]);
        }

        if (!request.venue || (!request.venue.google && !request.venue.yelp)) {
            logger.log('warning:s-socket:interaction:request-new', 'invalid venue type, must have a service sourceID');
            return socket.emit('/interaction/request/new<', [Constants.RESPONSE['400']]);
        }

        // Send request
        logger.log('res:s-socket:interaction:request-new', 'Creating new request');
        return interactionController.newRequestProcess(request).then(function (fullRequest) {
            // Success
            logger.log('res:s-socket:interaction:request-new', 'Successfully sent new request');
            return socket.emit('/interaction/request/new<', [null, fullRequest]);
        }).catch(function (err) {
            // Error
            logger.log('error:s-socket:interaction:request-new',
                'Error creating new request using:\n', request,
                '\nerror:\n', err
            );
            return socket.emit('/interaction/request/new<', [Constants.RESPONSE['500']]);
        });
    });

    socket.on('/interaction/fetch>', function (options) {
        options = options || {};
        options.opts = options.opts || {};
        options.filterTypes = options.filterTypes || [];

        const userID = socket.decoded_token.userID;

        options.opts.userID = userID;
        options.filterTypes.push('mine');

        options.type = options.type ? options.type.toUpperCase() :  'REQUEST';

        if (options.withUsers) {
            options.userID = socket.decoded_token.userID;
        }

        logger.log('req:interaction:request-fetch', 'Requesting users activities');
        return interactionController.search(options)
            .then(function (activities) {
                logger.log('res:interaction:request-fetch', 'Succesfully retrieved users activities');
                return socket.emit('/interaction/fetch<', [null, activities]);
            }).catch(function (err) {
                logger.log('error:interaction:request-fetch', 'Error trying to get users activities');
                return socket.emit('/interaction/fetch<', [err]);
            })
    });

    socket.on('/interaction/request/respond>', function (data) {
        logger.log('s-socket:interaction:request-respond', 'called');
        const reqID = data.reqID;
        const responderID = socket.decoded_token.userID;

        const status = data.status;

        if (!status || interactionController.interactionStatuses.indexOf(status) === -1) {
            logger.log('warning:s-socket:interaction:request-respond', 'invalid status type passed:', data);
            return socket.emit('/interaction/request/respond<', [Constants.RESPONSE['400']]);
        }

        if (!reqID) {
            logger.log('warning:s-socket:interaction:request-respond', 'missing reqID:', data);
            return socket.emit('/interaction/request/respond<', [Constants.RESPONSE['400']]);
        }

        // If user is not a participant
        if (!interactionController.isParticipant(reqID, responderID)) {
            return socketUtil.routeForbidden(socket, '/interaction/request/respond<');
        }

        return interactionController.respondRequestProcess(reqID, status, responderID, data)
            .then(function (response) {
                logger.log('res:interaction:request-respond', 'successfully updated request');
                return socket.emit('/interaction/request/respond<', [null, response]);
            }).catch(function (err) {
                logger.log('error:interaction:request-respond', 'Error updating/notifying request response');
                return socket.emit('/interaction/request/respond<', [err]);
            })
    });
};