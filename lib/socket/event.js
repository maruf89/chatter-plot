'use strict';

const _ = require('lodash-node/modern');
const logger = require('bragi');
const Q = require('q');

const Fetch = GLOBAL.Fetcher;

const Event = Fetch.type('event');
const Venue = Fetch.type('venue');
const elasticsearch = Fetch.database('elasticsearch');
const esClient = elasticsearch.client;
const eventController = Fetch.controller('event');
const eventUserImpl = Fetch.impl('event_user');
const esUser = Fetch.util('elasticsearch', 'userHelper');
const util = Fetch.util('utility');
const Constants = Fetch.config('misc/constants');

logger.log('init:socket:event', 'loaded');

exports.onSocketConnection = function () {
    const socket = this;

    /**
     * @description Grabs events based on the search options passed
     * @param  {object=} opts - see options in {@link bControllerEvent#search}
     * @return {(array<object>|{events:Array<object>, markers:Array<object>)}
     */
    socket.on('/event/fetch>', function (opts) {
        var promise, rID;
        if (opts == null) {
            opts = {};
        }
        logger.log('us-socket:event:fetch', 'called');

        // check for request ID 
        rID = opts.__rID;
        if (socket.decoded_token) {
            opts.userID = socket.decoded_token.userID;
        }

        // Fetch the initial query 
        promise = eventController.search(opts).then(eventController.formatSearchResults);

        // If we're fetching venues 
        if (opts.venues) {
            // compose the callback to use our options 
            promise = promise.then(eventController.searchResult2VenueExtend.bind(eventController, opts));
        }

        promise.then(function (response) {
            return socket.emit('/event/fetch<', [null, response, rID]);
        })["catch"](function (err) {
            logger.log('error:us-socket:event:fetch', 'error fetching events', err);
            return socket.emit('/event/fetch<', [err, null, rID]);
        });
    });

    socket.on('/event/get>', function (opts) {
        logger.log('us-socket:event:get', 'called');

        var promise = eventController.get(opts)
            .then(eventController.formatGetResults);

        // If we're fetching venues 
        if (opts.venues) {
            // compose the callback to use our options 
            promise = promise.then(eventController.getResult2VenueExtend.bind(eventController, opts));
        }

        return promise
            .then(function (events) {
                return socket.emit('/event/get<', [null, events]);
            })
            .catch(function (err) {
                return socket.emit('/event/get<', [err]);
            });
    });
};

exports.onSecureSocketConnection = function () {
    const socket = this;

    /**
     * @description Checks for the existence of events. If don't exist will save.
     * Will return locations with `eID` attached back to client
     *
     * @param  {object} eventData - event data object with the following:
     * @param  {string} eventData.indexType - event type (event|listing)
     * @param  {object} eventData.event - actual event data to save
     * @return {object} will return the entire saved event with eID and goingList
     */
    socket.on('/event/create>', function (eventData) {
        logger.log('s-socket:event:create', 'creating event process');
        return eventController.createEventProcess(
            eventData.event,
            eventData.indexType,
            socket.decoded_token.userID
        )
            .then(function (event) {
                // Event added to User
                logger.log('res:s-socket:event:create', 'Added event to users doc');
                return socket.emit('/event/create<', [null, event]);
            })
            .catch(function (errCode) {
                logger.log('error:s-socket:event:create', 'Error creating event');
                socket.emit('/event/create<', [Constants.RESPONSE[errCode]]);
            });
    });

    /**
     * Handles an event update request
     *
     * @param  {object} data          Must contain:
     * @option data {number} eID      the event id
     * @option data {object} updates  object containing only the fields to update
     */
    socket.on('/event/update>', function (data) {
        var eID, onError, opts, promise, sent, updates, userID;
        logger.log('s-socket:event:update', 'called');
        sent = null;
        onError = function (err) {
            if (err == null) {
                err = Constants.RESPONSE['500'];
            }
            if (sent) {
                return false;
            }
            socket.emit('/event/update<', [err]);
            return sent = true;
        };
        eID = data.eID;
        updates = data.updates;
        opts = {
            eIDs: [eID],
            type: data.type
        };
        if (!(typeof eID === 'number' && _.isPlainObject(updates))) {
            return onError(Constants.RESPONSE['400']);
        }
        userID = socket.decoded_token.userID;
        promise = eventController.get(opts)
            .then(eventController.formatGetResults);
        if (data.notify) {
            promise.then(eventController.searchResult2VenueExtend.bind(
                eventController, opts));
        }
        logger.log('s-socket:event:update',
            'fetching event to confirm user can update');
        return promise.then(function (eventArr) {
                var calls, event;
                event = eventArr[0];

                // Throw an error if we're trying to update an event that's not ours 
                if (event.creator !== userID) {
                    logger.log('error:s-socket:event:update',
                        'user is not event creator therefore cannot update event'
                    );
                    throw onError({
                        response: 400,
                        type: 'updates.ERROR-INVALID_PERMISSIONS'
                    });
                }
                logger.log('req:s-socket:event:update', 'updating event');

                // Execute the event update 
                calls = [Event.update(eID, updates, opts)];

                // If we have a message, notify the attendees 
                if (data.notify && data.notifyMessage && event.goingList.length > 1) {
                    calls.push(Event.notifyUsers(event, {
                        exclude: [userID],
                        notifyMessage: data.notifyMessage,
                        type: 'UPDATED',
                        reason: 'MANUAL'
                    }));
                }
                return Q.all(calls);
            }, function (err) {
                if (sent) {
                    throw err;
                }
                logger.log('error:s-socket:event:update',
                    'error updating event using:', data,
                    '\nerror:\n', err);
                throw onError();
            })
            .then(function () {
                logger.log('res:s-socket:event:update',
                    'event successfully updated');
                return sent = socket.emit('/event/update<', [null, true]);
            }, function (err) {
                if (sent) {
                    throw err;
                }
                logger.log('error:s-socket:event:update',
                    'error updating event:', data, '\nerror:\n', err);

                throw onError();
            });
    });

    /**
     * @description Updates an event to include the user AND updates a user to include the event
     */
    socket.on('/event/updateAttendance>', function (data) {
        var eID, method, sent;
        logger.log('s-socket:event:updateAttendance', 'called');

        if (!_.isPlainObject(data) || !data.eID || (data.attending == null)) {
            logger.log('warning:s-socket:event:updateAttendance',
                'Missing necessary fields: `eID` and/or `attending`'
            );
            return socket.emit('/event/updateAttendance<', [{
                response: 400,
                type: 'updates.ERROR-INVALID_REQUEST'
            }]);
        }

        method = data.attending ? 'userGoing' : 'userNotGoing';
        sent = null;
        eID = data.eID;
        logger.log('req:s-socket:event:updateAttendance', 'Updating users going list');

        return eventController[method](socket.decoded_token.userID, eID, data)
            .then(function (response) {
                logger.log('res:s-socket:event:updateAttendance',
                    'Successfully updated event/users going status'
                );

                // If removing a user from going list and there's a wait list 
                if (!data.attending && response.vars.waitList &&
                    response.vars.fromGoing
                ) {
                    logger.log('req:s-socket:event:updateAttendance', 'Updating users going list');
                    Event.updateAttendance(eID, true);
                }
                return sent = socket.emit('/event/updateAttendance<', [null, response]);
            }, function (err) {
                logger.log('error:s-socket:event:updateAttendance',
                    'Error updating event/users going status');
                throw sent = socket.emit('/event/updateAttendance<', [err]);
            })
            .then(function () {
                // Optional update waiting/going list if someone removed from going list 
                if (sent) {
                    return sent;
                }
                return logger.log(
                    'res:s-socket:event:updateAttendance',
                    'Successfully swapped event/waiting list users around'
                );
            }, function (err) {
                if (sent) {
                    throw sent;
                }
                return logger.log(
                    'error:s-socket:event:updateAttendance',
                    'Error updating events going/waiting list after removing user'
                );
            });
    });

    /**
     * Publishes an event, duh
     *
     * @param  {number} eID  unique event id
     */
    socket.on('/event/publish>', function (eventData) {
        logger.log('s-socket:event:publish', 'called - fetching event');
        if (!_.isPlainObject(eventData) || !eventData.eID) {
            logger.log('warning:s-socket:event:publish', 'invalid arguments');
            return socket.emit('/event/publish<', [{ response: 400, type: 'updates.ERROR-INVALID_REQUEST' }]);
        }

        eventData.callerID = socket.decoded_token.userID;

        let promise = eventController.publish(eventData);

        // Notify nearby users of the new event
        if (!eventData.supressNotify) {
            // IMPORTANT: we do not want to block the response so do NOT: `promise = promise.then`
            promise.then(_.partial(eventController.newEventNotify, eventData));
        }

        return promise.then(function () {
            logger.log('res:s-socket:event:publish', 'successfully published event');
            return socket.emit('/event/publish<', [null, { response: 200, type: 'events.PUBLISHED' }]);
        }, function (err) {
            logger.log('error:s-socket:event:publish',
                'error publishing event - eID:', eID,
                '\nerror\n', err);

            return socket.emit('/event/publish<', [Constants.RESPONSE['500']]);
        });
    });

    /**
     * @description Deletes an event, duh
     *
     * @param {object} data
     * @param {number} data.eID - event ID
     * @param {string} data.type - listing|event
     * @param {boolean} data.notify - whether to notify recipients by notification/email
     * @param {string=} data.notifyMessage - message to send along with the e-mail
     */
    socket.on('/event/delete>', function (data) {
        if (!data.eID || !data.type) {
            logger.log('warning:s-socket:event:delete', 'missing either eID or type');
            return socket.emit('/event/delete<', [Constants.RESPONSE['400']]);
        }

        data.userID = socket.decoded_token.userID;

        logger.log('s-socket:event:delete', 'called - deleting event');
        return eventController.deleteEventProcess(data)
            .then(function () {
                logger.log('res:s-socket:event:delete', 'successfully deleted the event');
                return socket.emit('/event/delete<', [null, { response: 200, type: 'events.DELETED' }]);
            })
            .catch(function (err) {
                logger.log('error:s-socket:event:delete', 'error deleting event');
                return socket.emit('/event/delete<', [err]);
            });
    });

    socket.on('/event/authNotify>', function (data) {
        if (!_.isPlainObject(data) || !data.eID || !data.type) {
            logger.log('error:s-socket:event:authNotify', 'Invalid data arguments');
            return socket.emit('/event/authNotify<', [Constants.RESPONSE['400']]);
        }

        // Check if user is trying to use an illegal template
        if (data.emailKey && eventController.allowedUserEmailTypes.indexOf(data.emailKey) === -1) {
            logger.log('error:s-socket:event:authNotify',
                'Cannot call this email template directly.',
                'emailKey:', data.emailKey
            );
            return socket.emit('/event/authNotify<', [Constants.RESPONSE['404']]);
        }

        logger.log('req:s-socket:event:authNotify', 'Notifying users via authNotify');
        return eventController.eventGetNotify(data.eID, data.type, {
            type: data.type.toUpperCase(),
            emailKey: data.emailKey,
            excludeUsers: data.excludeUsers,
            excludeCreator: data.excludeCreator,
            data: data.data,
        })
            .then(function () {
                logger.log('res:s-socket:event:authNotify', 'Successfully notified users');
                return socket.emit('/event/authNotify<', [null, Constants.RESPONSE['200']]);
            }).catch(function (err) {
                logger.log('error:s-socket:event:authNotify',
                    'error marking viewed\nerror:\n', err
                );

                return socket.emit('/event/authNotify<', [Constants.RESPONSE['500']]);
            });
    });
};
