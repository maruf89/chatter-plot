'use strict';
var Constants, Fetch, _, cloudinary, dataIndex, elasticsearch, esClient,
    languageHelper, logger;

_ = require('lodash-node/modern');

logger = require('bragi');

Fetch = GLOBAL.Fetcher;

elasticsearch = Fetch.database('elasticsearch');

esClient = elasticsearch.client;

languageHelper = Fetch.util('elasticsearch', 'languageHelper');

cloudinary = Fetch.util('cloudinary');

Constants = Fetch.config('misc/constants');

dataIndex = 'sources';

logger.log('init:socket:api', 'init');

exports.onSocketConnection = function () {
    const socket = this;

    socket.on('api/settings/tz>', function () {
        return socket.emit('api/settings/tz<', Fetch.config('misc/time_zones'));
    });

    /**
     * Accepts an array of language ID's and returns:
     * 
     *     [{
     *         name: <language_name>
     *         id:   <language_id>
     *     }, {
     *         ...
     *     }]
     * 
     * @param  {array} keys  Language IDs
     * @return {array}       Objects containing language 'name' + 'id'
     */
    socket.on('api/convert/ID2Language>', function (keys) {
        var emitKey, sent;
        logger.log('us-socket:api:convert-ID2Language', 'Called');
        emitKey = 'api/convert/ID2Language<';
        sent = false;
        if (!Array.isArray(keys)) {
            logger.log('error:us-socket:api:convert-ID2Language',
                'Expected keys parameter to be of type Array instead is:',
                Object.prototype.toString.call(keys));
            return socket.emit(emitKey, [Constants.RESPONSE['400']]);
        }
        if (!keys.length) {
            logger.log('warning:us-socket:api:convert-ID2Language',
                'Returning empty array since no keys were passed');
            return socket.emit(emitKey, [null, []]);
        }
        logger.log('req:us-socket:api:convert-ID2Language',
            'Getting language names from ID\'s');
        return languageHelper.ID2Language(keys, {
                _source: 'language'
            })
            .then(languageHelper.respToLangObj, function (err) {
                logger.log('error:us-socket:api:convert-ID2Language',
                    'Error getting language from IDs with keys:',
                    keys, err);
                socket.emit(emitKey, [Constants.RESPONSE['500']]);
                throw sent = true;
            })
            .then(function (languages) {
                logger.log('res:us-socket:api:convert-ID2Language',
                    'Got language names from IDs');
                return socket.emit(emitKey, [null, languages]);
            }, function (err) {
                if (sent === true) {
                    return;
                }
                logger.log('error:us-socket:api:convert-ID2Language',
                    'Error converting language to Object', err);
                return socket.emit(emitKey, [Constants.RESPONSE[
                    '500']]);
            });
    });
    return socket.on('api/cloudinary/signRequest>', function (params) {
        return socket.emit('api/cloudinary/signRequest<', [null,
            cloudinary.buildUploadParams(params)
        ]);
    });
};
