'use strict';

const _ = require('lodash-node/modern');
const logger = require('bragi');

const Fetch = GLOBAL.Fetcher;

const Constants = Fetch.config('misc/constants');
const notificationCtrl = Fetch.controller('notification');
//const socketUtil = Fetch.socket('socketUtility');

logger.log('init:socket:notification', 'init');

exports.onSecureSocketConnection = function () {
    const socket = this;

    /**
     * !Settings - Returns a users settings if they very from the default settings
     *
     * @return {object}      Users settings if they exist
     */
    socket.on('/notification/fetch>', function (opts) {
        opts = opts || {};

        opts.userID = socket.decoded_token.userID;
        return notificationCtrl.fetch(opts)
            .then(function (response) {
                return socket.emit('/notification/fetch<', [null, response]);
            })["catch"](function (err) {
                return socket.emit('/notification/fetch<', [err]);
            });
    });

    /**
     * Marks a notification as being read
     *
     * Can also update other services if they have a `read` property
     *
     * @param data {array}   nIDs           Array of notification IDs
     * @param data {string}  mark           What to mark it as
     * @param data {string}  value          what to set the value as
     * @param data {Object=} updateAlso     Optional other service that corresponds to `_updateAlso` to call other service
     */
    socket.on('/notification/mark>', function (data) {
        if (!_.isPlainObject(data) || !data.mark || !data.nIDs || !data.value ||
            !data.nIDs.length) {
            logger.log('error:s-socket:notification:mark', 'Invalid data arguments');
            return socket.emit('/notification/mark<', [Constants.RESPONSE['400']]);
        }

        userNotificationsRead();

        logger.log('s-socket:notification:mark', 'called');
        return notificationCtrl.markRead(socket.decoded_token.userID, data.nIDs, data.mark, data.value)
            .then(function () {
                return socket.emit('/notification/mark<', [null, true]);
            }).catch(function (err) {
                return socket.emit('/notification/mark<', [err]);
            });
    });

    const userNotificationsRead = function () {
        return notificationCtrl.userNotified(socket.decoded_token.userID, false)
            .then(function () {
                return socket.emit('/notification/viewed<', [null, Constants.RESPONSE['200']]);
            }).catch(function (err) {
                logger.log('error:s-socket:notification:viewed',
                    'error marking viewed\nerror:\n', err
                );

                return socket.emit('/notification/viewed<', [Constants.RESPONSE['500']]);
            });
    };

    socket.on('/notification/viewed>', userNotificationsRead);
};
