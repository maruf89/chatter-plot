/**
 * @module favoriteSocket
 */

'use strict';

var _ = require('lodash-node/modern'),
    Q = require('q'),
    logger = require('bragi'),

    Fetch = GLOBAL.Fetcher,

    favoriteController = Fetch.controller('favorite'),

    Constants = Fetch.config('misc/constants');

exports.onSecureSocketConnection = function () {
    var socket = this;

    logger.log('s-socket:connection', 'user socket connection', socket.id);

    socket.on('/favorite/fetch>', function (folders) {
        var userID = socket.decoded_token.userID;

        // request favorites
        return favoriteController.fetch(userID, folders)
            // return results
            .then(function (results) {
                // results returns an array
                var favorites = results[0];
                return socket.emit('/favorite/fetch<', [null, favorites]);
            }).catch(function (err) {
                logger.log('error:s-socket:user:fetch', 'Error users favorites with folders:', folders, '\nerror:\n', err);

                return socket.emit('/favorite/fetch<', [{
                    type: 'updates.ERROR-SERVER_ERROR',
                    response: 500
                }]);
            })
    });

    socket.on('/favorite/toggle>', function (data) {

        if (typeof data !== 'object') {
            logger.log('error:s-socket:user:favorite', 'Invalid call, data isnt an object');
            return socket.emit('/favorite/toggle<', [Constants.RESPONSE['400']]);
        }

        var possibleActions = ['add', 'remove'];

        data.sourceID = socket.decoded_token.userID;

        if (possibleActions.indexOf(data.method) === -1 || typeof data.targetID !== 'number') {
            logger.log('error:s-socket:user:favorite', 'Invalid call, either invalid `method` or `target` isnt a number');
            return socket.emit('/favorite/toggle<', [Constants.RESPONSE['400']]);
        }

        // attempt request
        logger.log('req:s-socket:user:favorite', 'toggling user favorite');
        return favoriteController.toggle(data)

            // respond to front-end
            .then(function (res) {
                return socket.emit('/favorite/toggle<', [null, res]);
            }).catch(function (err) {
                logger.log('error:s-socket:user:favorite', 'Error (un)/favoriting user using data:', data, '\nerror:\n', err);

                return socket.emit('/favorite/toggle<', [Constants.RESPONSE['500']]);
            });
    });

    socket.on('/favorite/bulkRemove>', function (data) {

        if (typeof data !== 'object' ||
            !Array.isArray(data.targetIDs) ||
            !data.targetIDs[0]
        ) {
            logger.log('error:s-socket:user:bulkRemove', 'Invalid call, data isnt an object or targetIDs are empty');
            return socket.emit('/favorite/bulkRemove<', [Constants.RESPONSE['400']]);
        }

        data.sourceID = socket.decoded_token.userID;

        logger.log('req:s-socket:user:bulkRemove', 'bulk removing users');
        return favoriteController.bulkRemove(data)

            // respond to front-end
            .then(function (res) {
                return socket.emit('/favorite/bulkRemove<', [null, res]);
            }).catch(function (err) {
                logger.log('error:s-socket:user:bulkRemove', 'Error bulk removing using data:', data, '\nerror:\n', err);

                return socket.emit('/favorite/bulkRemove<', [Constants.RESPONSE['500']]);
            });
    })
};
