"use strict";

var logger = require('bragi'),

    Fetch = GLOBAL.Fetcher,

    Constants = Fetch.config('misc/constants'),
    authController = Fetch.controller('auth');


exports.onSocketConnection = function () {
    var socket = this;

    // Only bind on insecure socket
    if (!socket.decoded_token) {
        socket.on('/auth/serviceToken>', function (credentials) {
            // Throw error if invalid credentials passed in

            if (!credentials ||
                typeof credentials.service !== 'string' ||
                typeof credentials.data !== 'object'
            ) {
                logger.log('warning:us-socket:auth:serviceToken', 'Called with invalid parameters');
                return socket.emit('/auth/serviceToken', [Constants.RESPONSE['400']]);
            }

            logger.log('req:us-socket:auth:serviceToken', 'Attempting to connect with 3rd party service');
            authController.attemptSocialConnect(credentials.service, credentials.data).then(function (response) {
                logger.log('res:us-socket:auth:serviceToken',
                    'Successful 3rd party service login attempt');
                return socket.emit('/auth/serviceToken<', [null, response]);
            }).catch(function (msg) {
                logger.log('info:us-socket:auth:serviceToken',
                    'Attempted to connect with 3rd party service \nmessage:\n', msg);

                return socket.emit('/auth/serviceToken<', [Constants.RESPONSE['500']]);
            })
        });
    }

    return socket;
};
