'use strict';

const _ = require('lodash-node/modern'),
    logger = require('bragi'),
    path = require('path'),
    Q = require('q'),

    Fetch = GLOBAL.Fetcher,

    elasticsearch = Fetch.database('elasticsearch'),
    Permissions = Fetch.service('permissions'),
    User = Fetch.type('user'),
    sqlUser = Fetch.util('mysql', 'userHelper'),
    esUser = Fetch.util('elasticsearch', 'userHelper'),
    Constants = Fetch.config('misc/constants'),
    dataIndex = 'sources',
    adminController = Fetch.controller('admin'),

    socketUtil = Fetch.socket('socketUtility');

logger.log('init:socket:api', 'init');

var featureCalled = false;

exports.onSecureSocketConnection = function () {
    const socket = this;
    let userPermissions = socket.userPermissions;

    /**
     * Sets some data to the be sent to users on page load
     */
    socket.on('/admin/misc/siteMessage>', function (args) {
        if (!Permissions.can('USER_EDIT', userPermissions)) {
            return socketUtil.routeForbidden(socket, '/admin/misc/siteMessage<');
        }

        if (!args.key) {
            return socket.emit('/admin/misc/siteMessage<', [Constants.RESPONSE['400']]);
        }

        return adminController.storeDynamicSetting(args.key, args.data).then(function (keyName) {
            return socket.emit('/admin/misc/siteMessage<', [null, keyName]);
        })
    });

    /**
     * Suggests languages for the passed in `text`
     * 
     * @param  {object} data  Object containing `text` represting the language to suggest
     * @return {array}        Languages that fit the suggestion
     */
    socket.on('/admin/users/addFrom>', function (data) {
        var endID, es, length, sql, startID;
        logger.log('s-socket:admin:users-addFrom', 'called');

        if (!Permissions.can('USER_ADD', userPermissions)) {
            return socketUtil.routeForbidden(socket, '/admin/users/addFrom<');
        }

        if (!data.es || !data.sql) {
            return socket.emit('/admin/users/addFrom<', [Constants.RESPONSE['400']]);
        }

        es = Fetch.config("misc/useradd/" + data.es);
        sql = Fetch.config("misc/useradd/" + data.sql);
        length = es.length;
        startID = endID = null;

        if (length !== sql.length) {
            throw socket.emit('/admin/users/addFrom<', [Constants.RESPONSE[
                '400']]);
        }

        return Q.all(_.map(sql, function (user, index) {
                var esData;
                esData = es[index];
                return sqlUser.insert.intoTable(user, 'users')
                    .then(function (userID) {
                        if (!index) {
                            startID = userID;
                        } else if (index === (length - 1)) {
                            endID = userID;
                        }
                        if (Array.isArray(esData.languages)) {
                            _.each(esData.languages,
                                function (lang) {
                                    if (typeof lang.languageID === 'string') {
                                        lang.languageID = + lang.languageID;
                                    }
                                    if (typeof lang.level === 'string') {
                                        return lang.level = + lang.level;
                                    }
                                });
                        }
                        if (esData.firstName) {
                            esData.firstName = esData.firstName.trim();
                        }
                        if (esData.lastName) {
                            esData.lastName = esData.lastName.trim();
                        }
                        esData.userID = userID;
                        return esUser.addUser(esData, userID);
                    });
            }))
            .then(function () {
                var res = _.clone(Constants.RESPONSE['200']);
                res.startID = startID;
                res.endID = endID;
                return socket.emit('/admin/users/addFrom<', [null,res]);
            }, function (err) {
                logger.log('error:s-socket:admin:users-addFrom', err);
                return socket.emit('/admin/users/addFrom<', [Constants.RESPONSE['500']]);
            });
    });

    //socket.on('/admin/feature/desired>', function () {
    //    if (featureCalled) {
    //        return socket.emit('/admin/feature/desired<', [{
    //            "customMsg": "Already sent.",
    //            "response": 400
    //        }]);
    //    }
    //
    //    adminController.featureDesired().then(function () {
    //        featureCalled = true;
    //        return socket.emit('/admin/feature/desired<', [{
    //            "customMsg": "Successfully sent!",
    //            "response": 200
    //        }]);
    //    }).catch(function (err) {
    //        logger.log('error:s-socket:admin:feature-desired', 'Error sending:', err);
    //        return socket.emit('/admin/feature/desired<', [Constants.RESPONSE['500']]);
    //    })
    //});
};
