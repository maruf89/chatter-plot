'use strict';

const _ = require('lodash-node/modern');
const logger = require('bragi');

const Fetch = GLOBAL.Fetcher;

const SocketIo = Fetch.connection('socket.io');
const Constants = Fetch.config('misc/constants');

logger.log('init:socket:pubSub', 'init');

exports.onSocketConnection = function () {
    const socket = this;

    socket.on('/socket/channel/join>', function (data) {
        try {
            SocketIo.join(socket, data.channel);
        } catch (e) {
            logger.log('error:s-socket:pubSub:join', 'Error joining channel using', data, e);
            return socket.emit('/socket/channel/join<', [Constants.RESPONSE["500"]]);
        }
        logger.log('s-socket:pubSub:join', 'joined socket channel:', data.channel);
        socket.emit('/socket/channel/join<', [null, Constants.RESPONSE["200"]]);
    });

    socket.on('/socket/channel/leave>', function (data) {
        var error;
        try {
            SocketIo.leave(socket, data.channel);
        } catch (_error) {
            error = _error;
            logger.log('error:s-socket:pubSub:leave', 'Error leaving channel using', data);
            return socket.emit('/socket/channel/leave<', [Constants.RESPONSE["500"]]);
        }
        logger.log('s-socket:pubSub:leave', 'left socket channel');
        socket.emit('/socket/channel/leave<', [null, Constants.RESPONSE["200"]]);
    });
};
