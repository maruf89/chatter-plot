'use strict';

const _ = require('lodash-node/modern'),
    Q = require('q'),
    logger = require('bragi'),
    generatePassword = require('password-generator'),

    Fetch = GLOBAL.Fetcher,

    config = Fetch.config('config', true, 'js'),
    Constants = Fetch.config('misc/constants'),

    sqlUser = Fetch.util('mysql', 'userHelper'),
    esUser = Fetch.util('elasticsearch', 'userHelper'),
    jwtHelper = Fetch.util('jwtHelper'),
    Util = Fetch.util('utility'),
    User = Fetch.type('user'),

    Settings = Fetch.service('settings'),
    Mailchimp = Fetch.service('mailchimp'),
    Permissions = Fetch.service('permissions'),

    Redis = Fetch.database('redis').client,
    Email = Fetch.service('email'),
    socketUtil = Fetch.socket('socketUtility'),
    userController = Fetch.controller('user'),
    mailchimpController = Fetch.controller('mailchimp'),

    utilSearchHelper = Fetch.util('elasticsearch', 'helper'),
    utilUserSearch = Fetch.util('elasticsearch', 'user', 'index'),
    elasticsearch = Fetch.database('elasticsearch'),
    ESClient = elasticsearch.client,


    /**
     * Holds elasticsearch's user database index & type
     * @type {Object}
     */
    db = esUser.userDB,


    /**
     * Holds the name of the { <Service> : <MySQL Field Name> }
     * @type {Object}
     */
    serviceKeys = Fetch.config(['misc', 'authServiceKeys']);

logger.log('init:socket:user', 'init');

exports.onSocketConnection = function () {
    const socket = this;
    logger.log('us-socket:user:connection', 'user socket connection', socket.id);

    /**
     * Grabs speakers based on the search options passed
     * @param  {object=} opts - see options in {@link User#search}
     * @return {array} Array of speaker documents
     */
    socket.on('/user/fetch>', function (opts) {
        var rID;
        if (opts == null) {
            opts = {};
        }

        // check for request ID 
        rID = opts.__rID;

        // might be used in the search to perform a random search 
        opts.opts.sessionID = socket.id;
        logger.log('req:us-socket:user:fetch',
            'called: Requesting users from Elastcisearch');
        return userController.search(opts)
            .then(userController.formatSearchResults)
            .then(function (response) {
                return socket.emit('/user/fetch<', [null, response,
                    rID
                ]);
            })["catch"](function (err) {
                logger.log('error:us-socket:user:fetch',
                    'Error fetching users error:', err);
                return socket.emit('/user/fetch<', [Constants.RESPONSE[
                    '500'], null, rID]);
            });
    });

    /**
     * !Login - Logs a user in via the password/email form
     * TODO: return the complete user in a single go here
     * @param  {object} data  Object containing the `password` and `email`
     * @return {object}                 Response object primarily containing `token`, `newUser`, and `data` (userData)
     */
    socket.on('/user/login>', function (data) {
        var respond;
        logger.log('us-socket:user:login', 'called');
        respond = {
            name: 'basicAuth',
            data: null,
            success: null,
            error: null,
            token: null
        };
        if (!data.password || !data.email) {
            logger.log('us-socket:user:login',
                'Missing password or email');
            socket.emit('/user/login<', [{
                type: 'updates.ERROR-MISSING_REQUIRED_FIELDS',
                response: 400
            }]);
        }

        // Verify that the user is logged # Then prepare the user (get user + token) 
        return userController.login(data)
            .then(userController.prepareLoggedInUser)
            .then(function (response) {
                logger.log('service:user:login',
                    'Returning successful user auth to client');
                respond.user = response.user;
                respond.token = response.token;
                return respond.success = true;
            })["catch"](function (err) {
                logger.log('warning:user:login',
                    'unsuccessful login');
                respond.error = err;
                respond.type = err.type || 'auth.ERROR-LOGGING_IN';
                return respond.response = err.response || 500;
            })["finally"](function () {
                return socket.emit('/user/login<', [null, respond]);
            });
    });

    /**
     * !Signup - Signs a user up
     *
     * @param  {object} user
     * @option user {string} email      Users email
     * @return {object}                 Response object primarily containing `token`, `newUser`, and `data` (userData)
     */
    socket.on('/user/signup>', function (userData) {
        var error, respond, saveOpts, sent, setPassword, signupFields,
            user;
        logger.log('us-socket:user:signup', 'called');
        sent = false;
        respond = {
            name: 'basicAuth',
            newUser: true,
            data: null,
            success: null,
            error: null,
            token: null
        };
        signupFields = [
            'firstName',
            'lastName',
            'email',
            'password',
            'languages',
            'settings',
            'locations',
        ];

        user = _.pick(userData, signupFields);
        saveOpts = {
            password: null
        };

        if (user.password) {
            saveOpts.password = user.password;
            delete user.password;
        } else if (setPassword = config.onSignup.setPassword) {
            saveOpts.password = generatePassword(12, false);
        }

        error = function (err) {
            if (err == null) {
                err = {};
            }
            if (sent) {
                return false;
            }
            sent = true;
            respond.error = true;
            respond.type = err.type || 'updates.ERROR-REGISTERING_USER';
            respond.response = err.response || 500;
            socket.emit('/user/signup<', [respond]);
            return true;
        };

        if (!user.email) {
            logger.log('error:us-socket:user:signup', 'Missing email');
            return error({
                type: 'updates.ERROR-MISSING_REQUIRED_FIELDS',
                response: 400
            });
        }

        // Check if email already exists 
        logger.log('req:s-socket:email:exists', 'Checking if email exists in MySQL');

        return sqlUser.get.from('users', {
            email: user.email
        }, ['user_id'])["catch"](function () {
            logger.log('req:us-socket:user:signup', 'Email not in use, Saving user…');
            return true;
        })
        .then(function (res) {
            // Email not in use -> Save User
            if (typeof res === 'object' && res.user_id) {
                logger.log('warning:us-socket:user:signup', 'Email in use');
                throw error({
                    type: 'auth.ERROR-EMAIL_IN_USE'
                });
            }
            logger.log('req:us-socket:user:signup', 'Saving user…');
            utilUserSearch.sanitizeFields(user);
            return User.save(user, saveOpts);
        })["catch"](function (err) {
            if (error()) {
                logger.log('err:us-socket:user:signup',
                    'Error saving user using:', user, '\n\n', err);
            }
            throw err;
        })
        .then(function (userData) {
            // User saved -> send email
            var activationOpts, method;
            logger.log('res:us-socket:user:signup', 'User successfully saved');

            // user.userID = arguments[0].userID

            // user.permissions = arguments[0].permissions
            activationOpts = {};
            if (setPassword) {
                activationOpts.additionalMergeVars = [{
                    name: 'PASSWORD',
                    content: saveOpts.password
                }];
            }

            mailchimpController.extendMCUser(userData);

            method = userData.notActivated ? 'activation' : 'welcomeEmail';

            logger.log('req:us-socket:user:signup',
                'Sending activation/confirmation email and creating default Settings');

            return Email[method](user, activationOpts);
        })["catch"](function (err) {
            if (error()) {
                logger.log('err:us-socket:user:signup',
                    'Error sending activation email or generating user settings using:', user,
                    '\nerror:\n', err);
            }
            throw err;
        })
        .then(function (activation) {
            // Email sent -> respond to client
            logger.log('res:us-socket:user:signup',
                'Activation email sent + User settings generated/saved to Elasticsearch');

            respond.success = true;
            respond.data = user;

            return jwtHelper.tokenFromUser(user)
                .then(function (token) {
                    respond.token = token;
                    return socket.emit('/user/signup<', [null, respond]);
                });
        })["catch"](function (err) {
            if (error(err)) {
                return logger.log('err:us-socket:user:signup', 'error:\n', err);
            }
        });
    });

    /**
     * !Resets a user's password
     * Does same thing as Password create but uses a different token to verify
     * For users who signed up by Social Media
     *
     * @param  {object} data  Contains the newly created `password` and the valid `token`
     * @return {boolean}      True on success
     */
    socket.on('/user/passwordReset>', function (data) {
        if (!(_.isPlainObject(data) || data.token || data.password)) {
            logger.log('error:us-socket:user:passwordReset', 'Invalid passed in data or missing fields');
            return error({
                type: 'updates.ERROR-MISSING_REQUIRED_FIELDS',
                response: 400
            });
        }

        logger.log('req:us-socket:user:passwordReset', 'resetting user password');
        return userController.passwordResetProcess(data.token, data.password)

        // response
        .then(function () {
            logger.log('res:us-socket:user:passwordReset', 'successfully reset password for user');
            return socket.emit('/user/passwordReset<', [null, true]);
        }).catch(function (err) {
            logger.log('error:us-socket:user:passwordReset', 'resetting password\nerror:\n', err);
            return socket.emit('/user/passwordReset<', [err]);
        })
    });

    /**
     * !Get - Grabs a user's record from Elasticsearch
     *
     * @param  {object} user  the user to grab
     * @option user {boolean} account  whether to pull the users account info as well
     * @return {object}       the Elasticsearch record
     */
    socket.on('/user/get>', function (opts) {
        var userID = socket.decoded_token ? socket.decoded_token.userID : null,
            rID = opts.__rID;

        opts.userPermissions = socket.userPermissions;

        // if we have a secure user 
        if (userID) {
            // but we're missing any mention of who to get 
            if (!opts.userID && !opts.userIDs) {
                // add self 
                opts.userID = userID;
            }
        }
        return userController.get(opts, !!socket.decoded_token, userID)
            .then(function (data) {
                return socket.emit('/user/get<', [null, data, rID]);
            }, function (err) {
                return socket.emit('/user/get<', [err, null, rID]);
            });
    });

    // Only bind this listener if not a secure user
    if (!socket.decoded_token || !socket.decoded_token.userID) {
        /**
         * Activates a user
         */
        socket.on('/user/activate>', function (data) {
            return userController.activateProcess(data)
                .then(function () {
                    return socket.emit('/user/activate<', [null, true]);
                }, function (err) {
                    return socket.emit('/user/activate<', [err]);
                });
        });
    }
};

exports.onSecureSocketConnection = function () {
    const socket = this;
    let userPermissions = socket.userPermissions;
    logger.log('s-socket:connection', 'user socket connection', socket.id);

    /**
     * !Settings - Returns a users settings if they very from the default settings
     *
     * @return {object}      Users settings if they exist
     */
    socket.on('/user/settings>', function () {
        let userID = socket.decoded_token.userID;

        logger.log('req:s-socket:user:settings', 'request user settings');
        return esUser.retrieveUserById(userID, {
            _source: 'settings'
        })
        .then(function (settings) {
            logger.log('res:s-socket:user:settings', 'returned users settings');
            return socket.emit('/user/settings<', [null, settings]);
        }).catch(function (err) {
            logger.log('warning:s-socket:user:settings', 'User settings does not exist');
            return socket.emit('/user/settings<', [null, null]);
        });
    });

    /**
     * !SettingsEdit - Edits the users settings
     *
     * @param  {object} opts
     * @option opts {number} userID
     * @option opts {object} edits      The edits to apply
     * @return {Response}
     */
    socket.on('/user/settings/edit>', function (opts) {
        var callerID, edits;
        logger.log('s-socket:user:settings-edit', 'request initiated');
        if (!Permissions.can('SELF_EDIT', userPermissions)) {
            return socketUtil.routeForbidden(socket, '/user/settings/edit<');
        }
        if (!(_.isPlainObject(opts) && _.isPlainObject(opts.edits))) {
            logger.log('s-socket:user:settings-edit',
                'no edits passed in, or edits are not an object:',
                opts.edits);
            return socket.emit('/user/settings/edit<', [{
                type: 'updates.ERROR-INVALID_REQUEST',
                response: 400
            }]);
        }
        opts.userID = opts.userID || socket.decoded_token.userID;
        callerID = socket.decoded_token.userID;
        if (opts.userID !== callerID) {
            logger.log('s-socket:user:settings-edit',
                'request to edit another user');
            if (!Permissions.can('USER_EDIT', userPermissions)) {
                logger.log('warning:s-socket:user:settings-edit',
                    'user:', callerID,
                    'does not have permissions to edit user:', opts.userID
                );
                return socketUtil.routeForbidden(socket, '/user/settings/edit<');
            }
        } else {
            logger.log('s-socket:user:settings-edit',
                'user editing themself');
            opts.userID = callerID;
        }
        edits = {
            settings: opts.edits
        };

        logger.log('req:s-socket:user:settings-edit', 'Updating user\'s settings');

        return esUser.updateUser({
            _id: opts.userID
        }, edits)
        .then(function () {
            logger.log('res:s-socket:user:settings-edit',
                'successfully updated user\'s settings');
            return socket.emit('/user/settings/edit<', [
                null, {
                    type: 'updates.SUCCESS-FIELDS_UPDATED',
                    response: 204
                }
            ]);
        }, function (err) {
            logger.log('error:s-socket:user:settings-edit',
                'Error updating user:', opts.userID,
                'with edits:', edits);
            return socket.emit('/user/settings/edit<', [{
                type: 'updates.ERROR-UPDATING_USER',
                response: 500
            }]);
        });
    });

    /**
     * @description Updates a users Elasticsearch record
     * @param  {object} opts
     * @param  {object} opts.edits - what to update
     * @param  {number=} opts.userID - if not passed will use the current users ID
     * @return {boolean}      True on Success, Error object on error
     */
    socket.on('/user/edit>', function (opts) {
        logger.log('s-socket:user:edit', 'request initiated');
        if (!Permissions.can('SELF_EDIT', userPermissions)) {
            return socketUtil.routeForbidden(socket, '/user/edit<');
        }

        let callerID = socket.decoded_token.userID;
        if (opts.userID !== callerID) {
            logger.log('s-socket:user:edit', 'request to edit another user');

            if (!Permissions.can('USER_EDIT', userPermissions)) {
                logger.log('warning:s-socket:user:edit', 'user:', callerID,
                    'does not have permissions to edit user:', opts.userID
                );

                return socketUtil.routeForbidden(socket, '/user/edit<');
            }
        } else {
            logger.log('s-socket:user:edit', 'user editing themself');
            opts.userID = callerID;
        }

        if (!_.isPlainObject(opts.edits)) {
            logger.log('s-socket:user:edit', 'no edits passed in, or edits are not an object:', opts.edits);

            return socket.emit('/user/edit<', [{
                type: 'updates.ERROR-INVALID_REQUEST',
                response: 400
            }]);
        }

        return userController.updateInsecure(opts.userID, opts.edits)

        .then(function () {
            logger.log('res:s-socket:user:edit', 'successfully updated user');

            return socket.emit('/user/edit<', [null, true]);
        }, function (err) {
            logger.log('error:s-socket:user:edit', 'Error updating user', err);

            return socket.emit('/user/edit<', [{
                type: 'updates.ERROR-UPDATING_USER',
                response: 500
            }]);
        });
    });

    /**
     * !Persistent - Updates the user in MySQL and Elasticsearch where fields overlap
     *
     * @param  {object} opts  Object containing `edits` and `userID` to edit
     * @return {boolean}      True on Success, Error object on error
     */
    socket.on('/user/persistentEdit>', function (opts) {
        var callerID, edits, esUpdatable, esUpdatableFields, esUpdates,
            promiseCalls, sqlUpdatable, sqlUpdatableFields, sqlUpdates,
            userID;
        logger.log('s-socket:user:persistentEdit', 'request initiated');
        if (!Permissions.can('SELF_EDIT', userPermissions)) {
            return socketUtil.routeForbidden(socket, '/user/edit<');
        }
        userID = opts.userID;
        callerID = socket.decoded_token.userID;
        if (opts.userID && opts.userID !== callerID) {
            userID = callerID;
            logger.log('s-socket:user:persistentEdit',
                'request to edit another user');
            if (!Permissions.can('USER_EDIT', userPermissions)) {
                logger.log('warning:s-socket:user:persistentEdit',
                    'user:', callerID,
                    'does not have permissions to edit user:', opts.userID
                );
                return socketUtil.routeForbidden(socket,
                    '/user/persistentEdit<');
            }
        } else {
            logger.log('s-socket:user:persistentEdit',
                'user editing themself');
            userID = callerID;
        }
        if (!_.isPlainObject(edits = opts.edits)) {
            logger.log('error:s-socket:user:persistentEdit',
                'No edits passed in, or edits are not an object:',
                opts);
            return socket.emit('/user/persistentEdit<', [{
                type: 'updates.ERROR-INVALID_REQUEST',
                response: 400
            }]);
        }
        sqlUpdatableFields = ['email', 'password'];
        sqlUpdates = {};
        sqlUpdatable = false;
        esUpdatableFields = ['email'];
        esUpdates = {};
        esUpdatable = false;
        _.each(edits, function (val, key) {
            if (~sqlUpdatableFields.indexOf(key)) {
                sqlUpdatable = true;
                sqlUpdates[key] = val;
            }
            if (~esUpdatableFields.indexOf(key)) {
                esUpdatable = true;
                return esUpdates[key] = val;
            }
        });
        promiseCalls = [];
        if (!sqlUpdatable && !esUpdates) {
            logger.log('error:s-socket:user:persistentEdit',
                'No valid edits', edits);
            return socket.emit('/user/persistentEdit<', [{
                type: 'updates.ERROR-INVALID_REQUEST',
                response: 400
            }]);
        }
        if (sqlUpdatable) {
            logger.log('req:s-socket:user:persistentEdit',
                'Updating user in MySQL');

            // If a password was set, we need to hash it 
            sqlUpdates = Util.parseMySQLFields(sqlUpdates);
            promiseCalls.push(sqlUser.update.userWithFields(sqlUpdates, {
                user_id: userID
            }, 'users'));
        }
        if (esUpdatable) {
            logger.log('req:s-socket:user:persistentEdit',
                'Updating user in Elasticsearch');
            promiseCalls.push(esUser.updateUser(opts.userID, esUpdates));
        }
        return Q.all(promiseCalls)
            .then(function () {
                logger.log('res:s-socket:user:persistentEdit',
                    'Completed updates, replying to front end');
                return socket.emit('/user/persistentEdit<', [null,
                    true
                ]);
            }, function (err) {
                logger.log('error:s-socket:user:persistentEdit',
                    'Error updating user:', userID,
                    'with edits:', edits);
                return socket.emit('/user/persistentEdit<', [{
                    type: 'updates.ERROR-UPDATING_USER',
                    response: 500
                }]);
            });
    });

    /**
     * !Verify - Verifies specific user fields
     *
     * TODO: Build the elasticsearch get - verify
     *
     * @param  {object} opts
     * @return {boolean}       Whether all fields match
     */
    socket.on('/user/verify>', function (opts) {
        var call, callerID, table, verifyFields;
        logger.log('s-socket:user:verify', 'request initiated');
        if (!Permissions.can('SELF_VIEW', userPermissions)) {
            return socketUtil.routeForbidden(socket, '/user/verify<');
        }
        if (!(_.isPlainObject(opts) && _.isPlainObject(opts.verify))) {
            logger.log('s-socket:user:verify',
                'no edits passed in, or edits are not an object:',
                opts.edits);
            return socket.emit('/user/verify<', [{
                type: 'updates.ERROR-INVALID_REQUEST',
                response: 400
            }]);
        }
        opts.userID = opts.userID || socket.decoded_token.userID;
        callerID = socket.decoded_token.userID;

        // Verify user has permission to edit other user or self 
        if (opts.userID !== callerID) {
            logger.log('s-socket:user:verify', 'request to edit another user');
            if (!Permissions.can('USER_VIEW', userPermissions)) {
                logger.log('warning:s-socket:user:verify', 'user:',
                    callerID,
                    'does not have permissions to edit user:', opts.userID
                );
                return socketUtil.routeForbidden(socket, '/user/verify<');
            }
        } else {
            logger.log('s-socket:user:verify', 'user verifying themself');
            opts.userID = callerID;
        }

        // `call` will hold the promise which differs depending on if it's MySQL or ES 
        call = null;

        // Store fields to check in array 
        verifyFields = Object.keys(opts.verify);
        if (opts.persistent) {

            // If MySQL 

            // Hash any password or whatnot 
            opts.verify.user_id = opts.userID;
            opts.verify = Util.parseMySQLFields(opts.verify);
            table = opts.table || 'users';
            logger.log('req:s-socket:user:verify',
                'Verifying fields in persistent MySQL');
            call = sqlUser.get.from(table, opts.verify, verifyFields)
                .then(function () {
                    return true;
                }, function (err) {
                    if (err === 404) {

                        // 404 signifying not found therefore false 
                        return false;
                    }
                    throw err;
                });
        } else {

            // If Elasticsearch 
        }
        return call.then(function (exists) {
            logger.log('res:s-socket:user:verify', 'Got verification match from MySQL');
            return socket.emit('/user/verify<', [null, exists]);
        }, function (err) {
            if (err) {
                logger.log('error:s-socket:user:verify',
                    'Error verifying fields with opts:',
                    opts, 'error:\n\n', err);
                return socket.emit('/user/verify<', [{
                    type: 'updates.ERROR-UPDATING_USER',
                    response: 500
                }]);
            }
        });
    });

    /**
     * !Delete - user
     *
     * @param  {object} opts  Contains the userID of the user to be deleted
     * @return {number}       User id of the deleted user
     */
    socket.on('/user/delete>', function (opts) {
        var userID;
        if (opts == null) {
            opts = {};
        }
        logger.log('s-socket:user:delete', 'request initiated with:', opts);
        if (!(opts.deleteSelf || Permissions.can('USER_DELETE', userPermissions))) {
            return socketUtil.routeForbidden(socket, '/user/delete<');
        }
        userID = opts.userID;

        // Notify that the user was deleted by an admin 
        opts.source = opts.deleteSelf ? 'user' : 'admin';

        // Cannot delete yourself unless specified 
        if (+userID === +socket.decoded_token.userID && !opts.deleteSelf) {
            logger.log('error:s-socket:user:delete', 'User', userID,
                'trying to delete himself... dummy');
            return socket.emit('/user/delete<', [{
                type: 'updates.ERROR-CANT_DELETE_SELF',
                response: 400
            }]);
        }
        logger.log('req:s-socket:user:delete', 'Deleting user');
        return User["delete"](userID, opts)
            .then(function (userID) {
                logger.log('res:s-socket:user:delete',
                    'User successfully deleted. user_id:',
                    userID);
                return socket.emit('/user/delete<', [null, userID]);
            }, function (error) {
                logger.log('error:s-socket:user:delete',
                    'Error deleting user:', error);
                return socket.emit('/user/delete<', [{
                    type: 'updates.ERROR-SERVER_ERROR',
                    response: 500
                }]);
            });
    });

    /**
     * !Allusers - returns information for all users
     *
     * @param  {object} opts
     * @option opts {number} size  number of documents to return
     * @option opts {number} from  from where to return users
     * @option opts {string} sort  direction to return users
     * @return {object}            User data
     */
    socket.on('/user/allUsers>', function (opts) {
        opts = opts || {}

        if (!Permissions.can('USER_VIEW', userPermissions)) {
            return socketUtil.routeForbidden(socket, '/user/allUsers<');
        }

        logger.log('s-socket:user:allUsers', 'request initiated with:', opts);
        let sortBy = opts.sortBy || 'userID',
            sort = sortBy + ':' + (opts.sort || 'desc'),
            params = {
                index: db.index,
                type: db.type,
                size: opts.size || 25,
                from: opts.from || 0,
                sort: sort
            };

        // If searching for empty/valued fields, then change filter 
        if (opts.empty === 'true') {
            params.body = {
                query: {
                    constant_score: {
                        filter: {
                            missing: {
                                field: sortBy,
                                existent: true,
                                null_value: true
                            }
                        }
                    }
                }
            };
        }

        logger.log('req:s-socket:user:allUsers', 'searching user in ES with:', params);
        return ESClient.search(params)
            .then(function (users) {
                var count, data;
                count = users.hits.total;
                data = users.hits.hits;
                logger.log('res:s-socket:user:allUsers', 'Users found. Emitting to front-end. User count:',  count);

                return socket.emit('/user/allUsers<', [
                    null, {
                        count: count,
                        data: data
                    }
                ]);
            }, function (err) {
                logger.log('error:s-socket:user:allUsers',
                    'Error retrieving users from the database using params:', params);
                return socket.emit('/user/allUsers<', [{
                    type: 'updates.ERROR-SERVER_ERROR',
                    response: 500
                }]);
            });
    });

    socket.on('/user/feature/seen>', function (feature) {
        if (typeof feature !== 'string') {
            return socket.emit('/user/feature/seen<', [Constants.RESPONSE['400']]);
        }

        var data = {
            feature: feature,
            userID: socket.decoded_token.userID
        };

        logger.log('req:s-socket:user:feature-seen', 'marking feature as seen');
        return userController.featureSeen(data).then(function () {
            return socket.emit('/user/feature/seen<', [null, true]);
        }).catch(function (err) {
            logger.log('error:s-sockt:user:feature-seen', 'error marking feature seen using:', data, '\nerror:\n', err);
            return socket.emit('/user/feature/seen<', [Constants.RESPONSE['500']]);
        })
    });

    socket.on('/user/feature/unseen>', function (feature) {
        if (typeof feature !== 'string') {
            return socket.emit('/user/feature/unseen<', [Constants.RESPONSE['400']]);
        }

        var data = {
            feature: feature,
            userID: socket.decoded_token.userID
        };

        logger.log('req:s-socket:user:feature-seen', 'adding feature as unseen');
        return userController.featureSeen(data, true).then(function () {
            return socket.emit('/user/feature/unseen<', [null, true]);
        }).catch(function (err) {
            logger.log('error:s-sockt:user:feature-seen', 'error marking feature unseen using:', data, '\nerror:\n', err);
            return socket.emit('/user/feature/unseen<', [Constants.RESPONSE['500']]);
        })
    });

    socket.on('/user/favorite>', function (data) {
        var possibleActions = ['add', 'remove'];

        data.sourceID = socket.decoded_token.userID;

        if (possibleActions.indexOf(data.method) === -1 || typeof data.targetID !==
            'number') {
            logger.log('error:s-socket:user:favorite',
                'invalid call, either invalid `method` or `target` isnt a number'
            );
            return socket.emit('/user/favorite<', [{
                type: 'updates.ERROR-SERVER_ERROR',
                response: 500
            }]);
        }

        logger.log('req:s-socket:user:favorite', 'un/favoriting user');
        return userController.favorite(data)
            .then(function (res) {
                return socket.emit('/user/favorite<', [null, res]);
            })["catch"](function (err) {
                logger.log('error:s-socket:user:favorite',
                    'Error (un)/favoriting user using data:',
                    data, '\nerror:\n', err);
                return socket.emit('/user/favorite<', [{
                    type: 'updates.ERROR-SERVER_ERROR',
                    response: 500
                }]);
            });
    });
};
