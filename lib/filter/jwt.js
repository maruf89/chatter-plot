'use strict';
var Fetch, Redis, User, _destroySession, config, cookieParser, jwt, logger;

jwt = require('jsonwebtoken');

logger = require('bragi');

Fetch = GLOBAL.Fetcher;

config = Fetch.config('config', true, 'js');

cookieParser = require('cookie-parser')(config.secretKey);

Redis = Fetch.database('redis').client;

User = Fetch.type('user');


/**
 * Removes Redis session data for a given socket connection
 *
 * @param  {object} handshake  request handshake
 */

_destroySession = function (handshake) {
    if (!handshake.headers.cookie) {
        return;
    }
    logger.log('util:jwtHelper:_destroySession', 'Destroying socket session');
    return cookieParser(handshake, null, function (err) {
        var sessID;
        sessID = handshake.signedCookies['connect.sid'];
        sessID = "sess:" + sessID;

        // Delete the session 
        return Redis.pdel(sessID);
    });
};


/**
 * Callback for verifying a secure socket connection
 * Checks whether the requested token exists in Redis
 *
 * @callback
 * @param  {object}   data   Socket Data
 * @param  {Function} accept  pass null for success, truthy value for error
 */

exports.verifyToken = function (data, accept) {
    var error, sent, token, userID;
    logger.log('util:jwtHelper:verifyToken', 'called');

    token = (data.request._query || data.request.query).token;
    error = null;
    userID = data.decoded_token.userID;
    sent = false;
    logger.log('req:util:jwtHelper:verifyToken',
        'Checking for existing user token in Redis');

    return Redis.pget(token)
        .then(function (exists) {
            logger.log('res:util:jwtHelper:verifyToken', 'Received result from Redis');

            if (!exists) {
                _destroySession(data.handshake);
                logger.log('warning:util:jwtHelper:verifyToken', 'Token:', userID);
                sent = true;
                throw accept({
                    message: {
                        title: 'updates.ERROR-INVALID_TOKEN',
                        response: 400,
                        msg: 'incorrectly_generated_token'
                    }
                });
            }
            logger.log('req:util:jwtHelper:verifyToken',
                'Checking service:user whether user has been suspended');

            /**
             * Currently aren't suspending any users so this is not needed
             */
            //return User.isSuspended(userID, true);
            return false
        }, accept)
        .then(function (suspended) {
            logger.log('res:util:jwtHelper:verifyToken',
                'Received result from Redis');
            if (suspended) {
                _destroySession(data.handshake);
                logger.log('warning:util:jwtHelper:verifyToken',
                    'User has been suspended - userID:', userID);

                return accept({
                    message: {
                        title: 'updates.ERROR-USER_SUSPENDED',
                        response: 403,
                        msg: 'user_suspended',
                        body: 'updates.ERROR-USER_SUSPENDED'
                    }
                });
            }
            logger.log('util:jwtHelper:verifyToken', 'Users jsonwebtoken is valid');
            return accept();
        }, function (err) {
            if (!sent) {
                logger.log('error:util:jwtHelper:verifyToken', 'Error checking whether user is suspended', err);
            }
            throw err;
        });
};
