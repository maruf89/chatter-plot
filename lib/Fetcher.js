'use strict';
const path = require('path'),
    _ = require('lodash-node/modern'),

    root = process.cwd(),
    libDir = path.resolve(root, 'lib'),
    commonPath = path.join(root, 'common'),
    configPath = path.join(libDir, 'config'),

    libraryDirectories = {
        service: 'services',
        controller: 'controllers',
        impl: 'controllers/impl',
        util: 'util',
        socket: 'socket',
        database: 'database',
        type: 'type',
        filter: 'filter',
        connection: 'connection',
        cron: 'cron',
    },

    _fetch = function (_path, what) {
        return require(path.join(_path, what));
    };

GLOBAL.Fetcher = {
    config: function (config, notJSON, mime) {
        if (Array.isArray(config)) {
            config = path.join.apply(path, config);
        }
        config += '.';
        config += notJSON ? mime || 'js' : 'json';
        return _fetch(configPath, config);
    },
    common: function (arg1, arg2, arg3) {
        let pathToFile = path.join(arg1, arg2 || '', arg3 || '');
        return _fetch(commonPath, pathToFile);
    },
};

/**
 * Appends the Fetch object with fetch methods for each key in `libraryDirectories`
 */

_.each(libraryDirectories, function (dir, key) {
    let base = path.join(libDir, dir);
    GLOBAL.Fetcher[key] = function () {
        let file = path.join.apply(path, arguments);
        return _fetch(base, file);
    };
});

module.exports = GLOBAL.Fetcher;
