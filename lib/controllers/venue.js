/**
 * @module bControllerVenue
 * @description handles getting/checking/parsing/saving of venues
 */

'use strict';

const _ = require('lodash-node/modern');
const Q = require('q');
const logger = require('bragi');

const Fetch = GLOBAL.Fetcher;

const Venue = Fetch.type('venue');
const utilVenueSearch = Fetch.util('elasticsearch', 'venue', 'index');
const utilSearchHelper = Fetch.util('elasticsearch', 'helper');
const Constants = Fetch.config('misc/constants');
const Util = Fetch.util('utility');
const commonVenue = Fetch.common('util', 'venue');

const controller = exports;

exports.get = Venue.get;

exports.getMultiple = Venue.mget;

/**
 * Taking an array of Google (Yelp in the future???) locations, will search through the database
 * to see what we already have stored, and save the ones that haven't yet
 *
 * @param  {array<object>} venuesArr - Array of Objects structured like `ES_DATA/mappings/venues.json`
 * @return {Promise<array>} allVenues
 * @return {array<object>} allVenues - array of all the venues with full info
 */
exports.safeSave = function (venuesArr) {
    var retVenues = [],
        venuesToSave = [],
        venues = _.clone(venuesArr);

    logger.log('req:controller:venue:save', 'Checking which venues already exist in Elasticsearch');

    return controller.searchForExisting(venues).then(utilSearchHelper.searchExtractSource)

    .then(function (existingVenues) {
        // Filter out the preexisting venues into `retVenues`
        retVenues = _filterPreexisting(venues, existingVenues);

        if (!venues.length) {
            logger.log('service:venue:save', 'No new venues to save');
            return retVenues;
        }

        logger.log('req:controller:venue:save', 'Preparing venues for the database & saving');
        return Q.all(
            _.map(venues, controller.prepareVenue)
        )
        .then(function (_venuesToSave) {
            venuesToSave = _venuesToSave;
            return Venue.bulkFormatSave(venuesToSave);
        });
    }).catch(function (err) {
        logger.log('error:controller:venue:save',
            'Error saving venues to Elasticsearch\nerror:\n', err);

        throw Constants.RESPONSE['500'];
    })

    .then(function (response) {
        if (response.errors) {
            logger.log('error:controller:venue:save',
                'Error saving venues to Elasticsearch\nerror:\n', response.items[0].index.error);

            throw Constants.RESPONSE['500'];
        }
        return retVenues.concat(venuesToSave);
    });
};

/**
 * @description accepts 2 arrays of venues and returns the duplicates
 * IMPORTANT: Modifies the passed in venues object
 * @param {array<object>} venues
 * @param {array<object>} compare
 * @returns {object} returns a { <number_id>: <venue_object> } mapping
 * @private
 */
const _filterPreexisting = function (venues, compare) {
    var preexisting = [];

    _.each(compare, function (fVenue) {
        var sVenue, vID,
            counter = venues.length,
            results = [];

        while (--counter >= 0) {
            sVenue = venues[counter];

            if (fVenue.google === sVenue.google || fVenue.yelp === sVenue.yelp) {
                sVenue.vID = vID = parseInt(fVenue.vID, 10);
                preexisting.push(venues.splice(counter, 1)[0]);
                break;
            }
        }
        return results;
    });
    return preexisting;
};

/**
 * @description prepares a single venue for the database by checking if it has the required fields - if not it loads
 * the venue from it's source then recalls this upon completion - then it
 * fetching it's timezone & generating a unique ID
 *
 *
 * @param venue
 * @returns {Promise.<T>}
 */
exports.prepareVenue = function (venue) {
    if (!venue.type) {
        return controller.getPlacePrepare(venue).then(controller.prepareVenue);
    }

    const safeVenue = {
        tz: venue.tz,
        coords: Util.normalizeCoords(venue.coords),
        type: venue.type,
    };

    if (venue.name) safeVenue.name = venue.name;
    if (venue.google) safeVenue.google = venue.google;
    if (venue.yelp) safeVenue.yelp = venue.yelp;

    return Venue.appendTimeZone(safeVenue).then(Venue.generateID);
};

exports.getPlacePrepare = function (incomplete) {
    return commonVenue.getPlace(incomplete)
        .then(commonVenue.scrapePlace)
        .then(function (venue) {
            incomplete.type = venue.type;
            incomplete.tz = venue.tz;
            incomplete.name = venue.name;
            incomplete.coords = venue.coords;
            return incomplete;
        });
};

exports.searchForExisting = function (venuesArr) {
    var query,
        shouldConditions = utilVenueSearch.buildSearchConditions(venuesArr);


    if (!shouldConditions.length) {
        logger.log('warning:controller:venue:search', 'No searchable venues');
        return Q.reject(405);
    }

    query = {
        body: {
            "query": {
                "filtered": {
                    "filter": {
                        "bool": {
                            "should": shouldConditions
                        }
                    }
                }
            }
        }
    };

    logger.log('req:controller:venue:search', 'Searching for existing venues');
    return Venue.search(query)["catch"](function (err) {
        logger.log('error:controller:venue:search',
            'Error querying venues using query:', query,
            '\n\nerror:', err);

        throw Constants.RESPONSE['500'];
    });
};
