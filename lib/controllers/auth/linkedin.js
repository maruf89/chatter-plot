'use strict';

const SERVICE_NAME = 'linkedin',
    SESSION_KEY = 'linkedinUser',
    OAUTH_RESPONSE_KEY = 'code',
    ACCESS_TOKEN_URI = 'https://www.linkedin.com/uas/oauth2/accessToken',

    _ = require('lodash'),
    Q = require('q'),
    request = require('request'),
    linkedinapi = require('node-linkedin'),
    logger = require('bragi'),

    Fetch = GLOBAL.Fetcher,

    dataFerry = Fetch.util('dataFerry'),
    geocoder = Fetch.util('geocoderPromise'),
    Util = Fetch.util('utility'),
    Auth = Fetch.controller('auth', 'auth');

var serviceConfig = Fetch.config('socialServices')[SERVICE_NAME],
    linkedin;

const credentials = Fetch.util('parseConfig').parse(serviceConfig),
    languageHelper = Fetch.util('elasticsearch', 'languageHelper'),

/**
 * Initiates the linkedin oauth2 client
 */
_oauthClientInit = function () {
    return this.client = linkedinapi(credentials.apiKey, credentials.apiSecret,
        credentials.connectURL);
},

_formatUser = function (user) {
    user.email = user.emailAddress;
    return user;
};

// Front end data transfer
dataFerry.await(SERVICE_NAME);

const Linkedin = (function (superClass) {
    Util.extendClass(Linkedin, superClass);

    function Linkedin() {
        var ferryData, opts;
        this.sessionKey = SESSION_KEY;
        this.serviceName = SERVICE_NAME;
        this.oauthResponseKey = OAUTH_RESPONSE_KEY;
        _oauthClientInit.call(this);
        opts = {
            appId: credentials.apiKey,
            redirectUri: credentials.connectURL
        };
        ferryData = {
            connectURL: this.client.auth.generateAuthUrl(opts,
                credentials.scope),
            verifyURL: this.client.auth.generateAuthUrl(_.extend(opts, {
                redirectUri: credentials.verifyURL
            }), credentials.scope)
        };
        dataFerry.store(SERVICE_NAME, ferryData);
        Linkedin.__super__.constructor.apply(this, arguments);
    }

    Linkedin.prototype.exchangeToken = function (code, verify, state) {
        let deferred = Q.defer();
        const redirectURI = 'https://' + state + credentials[verify ? 'verifyURL' : 'connectURL'];

        // Make the request 
        request({
            method: 'POST',
            url: ACCESS_TOKEN_URI,
            qs: {
                grant_type: 'authorization_code',
                code: code,
                redirect_uri: redirectURI,
                client_id: credentials.apiKey,
                client_secret: credentials.apiSecret
            },
            json: true
        }, function (err, response, body) {
            if (err || body.error) {
                return deferred.reject(err || body.error_description);
            }
            return deferred.resolve(body);
        });

        return deferred.promise;
    };


    /**
     * Returns the full user profile
     *
     * @param  {object} authData  object w/ linkedin access_token
     * @return {object}           user profile
     */

    Linkedin.prototype.getUser = function (authData) {
        var deferred, params, peopleFields, queryURL;
        deferred = Q.defer();

        // Define the fields we want 
        peopleFields = ['firstName', 'lastName', 'industry',
            'date-of-birth', 'picture-urls::(original)',
            'picture-url', 'languages', 'interests', 'associations',
            'email-address', 'id', 'summary',
            'specialties'
        ];

        // Build the uRL 
        queryURL = linkedin.client.options.api_url + 'people/~:';
        queryURL += '(' + peopleFields.join(',') + ')';
        params = {
            oauth2_access_token: authData.access_token,
            format: 'json'
        };
        request({
            method: 'GET',
            url: queryURL,
            qs: params,
            json: true
        }, function (err, res, body) {
            if (err || body.error) {
                return deferred.reject(err || body);
            }
            return deferred.resolve(_formatUser(body));
        });
        return deferred.promise;
    };

    Linkedin.prototype._formatUserData = function (userData) {
        var dob, ref, self, user;
        self = this;
        user = {
            userID: userData.userID,
            firstName: userData.firstName,
            lastName: userData.lastName,
            email: userData.emailAddress,
            personal: userData.summary,
            picture: {
                "default": null,
                thumb: userData.pictureUrl
            },
            accounts: [{
                type: SERVICE_NAME,
                id: userData.id
            }]
        };

        if ((ref = userData.pictureUrls) != null ? ref._total : void 0) {
            user.picture["default"] = userData.pictureUrls.values[0];
        }

        if (dob = userData.dateOfBirth) {
            user.details = {
                birthday: dob.year + "-" + dob.month + "-" + dob.day
            };
        }

        return self._extractLanguages(userData)
        ["catch"](function (err) {
            logger.log('error:auth-linkedin:_formatUserData',
                'Error Extracting languages from Linkedin Account', userData,
                '\nerror:\n', err);

            throw err;
        })
        .then(function (langs) {
            if (Array.isArray(langs) && langs.length) {
                user.languages = _.map(langs, function (id) {
                    return {
                        languageID: id
                    };
                });
            }
            return user;
        });
    };


    Linkedin.prototype._extractLanguages = function (userData) {
        if (!(userData.languages && userData.languages._total)) {
            return Q.resolve([]);
        }

        var languages = _.pluck(userData.languages.values, 'language').map(function (lang) {
            return lang.name;
        });

        return languageHelper.getIdForLanguages(languages).then(function (res) {
            return languageHelper.respToLangObj(res, 'idArray');
        });
    };

    return Linkedin;

})(Auth);

exports.client = linkedin = new Linkedin();
