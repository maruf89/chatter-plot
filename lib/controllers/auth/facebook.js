'use strict';

const SERVICE_NAME = 'facebook';

// the session key user value
const SESSION_KEY = 'facebookUser';

// the query param that returned via oauth connect to use to trade for tokens
const OAUTH_RESPONSE_KEY = 'code';

const SECURE_USER_KEY = 'fb_id';

const FB = require('fb');
const _ = require('lodash-node/modern');
const Q = require('q');
const logger = require('bragi');
const uuid = require('node-uuid');

const Fetch = GLOBAL.Fetcher;

const Auth = Fetch.controller('auth', 'auth');
const fbLanguages = Fetch.config(['misc', 'facebookLanguages']).languages;
const configParser = Fetch.util('parseConfig');
const languageHelper = Fetch.util('elasticsearch', 'languageHelper');
const dataFerry = Fetch.util('dataFerry');
const Cloudinary = Fetch.util('cloudinary');
const Util = Fetch.util('utility');
const credentials = configParser.parse(Fetch.config('socialServices')[SERVICE_NAME]);

let _redirectBuster = null;

// Front end data transfer
dataFerry.await(SERVICE_NAME);

const Facebook = (function (superClass) {
    Util.extendClass(Facebook, superClass);

    function Facebook() {
        this.sessionKey = SESSION_KEY;
        this.serviceName = SERVICE_NAME;
        this.oauthResponseKey = OAUTH_RESPONSE_KEY;
        this.secureUserKey = SECURE_USER_KEY;

        // Updates the cachebuster URL every 2 hours 

        // Would be ideal if we could notify all insecure socket routes when this change occurs 
        setInterval(this._updateClientData, 1000 * 60 * 60 * 5);
        this._updateClientData(true);

        // Call base class 
        Facebook.__super__.constructor.apply(this, arguments);
    }

    Facebook.prototype._updateClientData = function (dontNotify) {
        logger.log('req:auth-facebook:request', 'updating facebook cache buster token');
        _redirectBuster = 'cache_buster=' + uuid.v1();

        dataFerry.store(SERVICE_NAME, {
            clientID: credentials.appId,
            connectURL: credentials.oauthConnectURL + '?' + _redirectBuster,
            verifyURL: credentials.oauthVerifyURL + '?' + _redirectBuster
        }, !dontNotify);
    };

    Facebook.prototype.exchangeToken = function (code, verify, state) {
        let redirectURI = 'https://' + state + credentials[verify ? 'verifyURL' : 'connectURL'];

        return fbAPI('oauth/access_token', {
            client_id: credentials.appId,
            client_secret: credentials.appSecret,
            redirect_uri: redirectURI + '?' + _redirectBuster,
            code: code
        });
    };

    Facebook.prototype._formatUserData = function (userData, authData) {
        let self = this,
            user = _.clone(userData);

        delete user.id;

        return self._extractLanguages(user)
        ["catch"](function (err) {
            logger.log('error:auth-facebook:_formatUserData',
                'Error Extracting languages from Facebook Account', userData,
                '\nerror:\n', err);

            throw err;
        })
        .then(function (langs) {
            // Get languages
            if (Array.isArray(langs) && langs.length) {
                user.languages = _.map(langs, function (id) {
                    return {
                        languageID: id
                    };
                });
            }
            return user;
        });
    };

    Facebook.prototype._extractLanguages = function (userData) {
        if (!Array.isArray(userData.languages)) {
            return Q.resolve([]);
        }

        var languages = userData.languages.map(function (lang) {
            return fbLanguages[lang] || lang;
        });

        return languageHelper.getIdForLanguages(languages).then(function (res) {
            return languageHelper.respToLangObj(res, 'idArray');
        });
    };


    /**
     * Exchange temporary facebook access token for a long-living one
     *
     * @param {string} authData - temporary access token
     * @return {promise}              long living access token
     */

    Facebook.prototype.getUser = function (authData) {

        /**
         * http://stackoverflow.com/questions/17211255/retrieve-zip-code-of-a-facebook-user-by-facebook-graph-api
         */
        return fbAPI('me', {
                fields: ['gender', 'bio', 'birthday', 'hometown',
                    'locale', 'email', 'languages',
                    'first_name', 'last_name', 'id'
                ],
                access_token: authData.access_token
            })
            .then(function (res) {
                var ref, userData;
                userData = {
                    id: res.id,
                    firstName: res.first_name,
                    lastName: res.last_name,
                    personal: res.bio,
                    email: res.email,
                    details: {
                        birthday: res.birthday ?
                            res.birthday.substr(-4) + '-' + res.birthday.substr(0, 2) + '-' + res.birthday.substr(3,2)
                            : null,
                        gender: res.gender ? res.gender.toUpperCase() : null,
                        hometown: (ref = res.hometown) != null ?
                            ref.name : void 0,
                        locale: res.locale
                    },
                    languages: Array.isArray(res.languages) ?
                        res.languages.map(function (obj) {
                            return obj.name;
                        }) : null,
                    picture: {
                        hasPhoto: true,
                        service: 'facebook',
                        id: res.id
                    },
                    accounts: [{
                        type: SERVICE_NAME,
                        id: res.id
                    }]
                };
                return userData;
            })["catch"](function (err) {
                logger.log('error:auth-facebook:getUser',
                    'Error getting user\nerror:\n', err);
                throw err;
            });
    };


    /**
     * @description accepts an authData object and from that it queries for the users facebook id + email
     *
     * @param {object} authData
     * @param {string} authData.accessToken
     * @returns {Promise<object>} response
     * @returns {string} response.email
     * @returns {string} response[SECURE_USER_KEY] - the key corresponds the key in MySQL that holds the users fb id (fb_id)
     */

    Facebook.prototype.formatAttemptData = function (authData) {
        if (typeof authData.accessToken !== 'string') {
            return Q.reject();
        }
        return fbAPI('me', {
                fields: ['id', 'email'],
                access_token: authData.accessToken
            })
            .then(function (user) {
                var obj;
                obj = {
                    email: user.email
                };
                obj[SECURE_USER_KEY] = user.id;
                return obj;
            });
    };

    return Facebook;

})(Auth);

exports.client = new Facebook();

exports.getUser = exports.client.getUser;


/**
 * Handles any requests to facebook
 *
 * @param  {string}         The uri of the facebook method to call
 * @param  {object}         Facebook request object
 * @return {object}         Facebook response object
 */

const fbAPI = exports.request = function () {
    var args, callback, deferred;
    deferred = Q.defer();
    callback = function (response) {
        if (!response || response.error) {
            logger.log('error:auth-facebook:request', response,
                '\nusing arguments:', args);
            return deferred.reject(response);
        }
        return deferred.resolve(response);
    };
    args = [].slice.call(arguments);
    args.push(callback);
    FB.api.apply(FB, args);
    return deferred.promise;
};
