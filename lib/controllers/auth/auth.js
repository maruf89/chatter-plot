'use strict';

var _ = require('lodash-node/modern'),
    Q = require('q'),
    moment = require('moment'),
    logger = require('bragi'),
    
    Fetch = GLOBAL.Fetcher,
    
    config = Fetch.config('config', true, 'js'),
    Constants = Fetch.config('misc/constants'),
    localeMap = Fetch.common('config', 'locales').defaultMap,
    Util = Fetch.util('utility'),
    jwtHelper = Fetch.util('jwtHelper'),
    geocoder = Fetch.util('geocoderPromise'),
    sqlUser = Fetch.util('mysql', 'userHelper'),
    esUser = Fetch.util('elasticsearch', 'userHelper'),
    permissions = Fetch.service('permissions'),
    mailchimpController = Fetch.controller('mailchimp'),
    User = Fetch.type('user'),
    Email = Fetch.service('email'),
    Settings = Fetch.service('settings'),
    
    requiredProperties = ['serviceName', 'sessionKey', 'oauthResponseKey'],
    onError,


    /**
     * Holds the name of the { <Service> : <MySQL Field Name> }
     * @type {object}
     */
    serviceKeys = Fetch.config(['misc', 'authServiceKeys']);


/**
 * @description Base authentication class that all other services inherit from
 * @type {Auth}
 */
module.exports = (function () {
    function Auth() {
        logger.log('controller:auth-auth:constructor', 'Initiating. Verifying service has the required fields');
        var test = Util.verifyHas(this, requiredProperties);
        
        if (!test.green) {
            logger.log('error:controller:auth-auth:constructor', 'Missing: `' +
                                                                 test.missing.join(', ') +
                                                                 ("` while extending " + this.serviceName)
            );
            return false;
        }
        this.displayName = this.serviceName[0].toUpperCase() + this.serviceName.substr(1);
    }


    /**
     * Checks whether the user exists in MySQL, is connected with this service and
     * if they're in the elasticsearch database
     *
     * @param  {object} userData  Facebook profile data
     * @param  {object} authData  Facebook OAuth data
     * @return {Promise}          resolve @returns {string} userID
     *                            reject @returns {Object}
     *                                {string}  userID   users MySQL Chatterplot ID (Null if not in MySQL)
     *                                {boolean} elastic  user exists in elasticsearch DB
     *                                {boolean} service  user is connected with Facebook in MySQL
     */
    Auth.prototype._userExists = function (userData, authData, basicCheck) {
        var self = this;

        logger.log('req:controller:auth-auth:_userExists', 'Using our e-mail and Service ID, check if the user is in MySQL');
        return sqlUser.get.userIdByServiceAndEmail(userData.email, self.serviceName, userData.id)

        // User + Service exist in SQL, check if it exists in Elasticsearch
        .then(function (userID) {
            // If we're only doing a basic, we know the user + service exist, so return just the userID
            logger.log('res:controller:auth-auth:_userExists', 'user exists in MySQL');
            if (basicCheck) {
                return userID;
            }

            logger.log('req:controller:auth-auth:_userExists', 'Checking if user exists in Elasticsearch');
            return esUser.userExists(userID).then(function (exists) {
                logger.log('res:controller:auth-auth:_userExists', 'Response returned');

                // User is connected with Service and we have an Elasticsearch document
                if (exists) {
                    logger.log('req:controller:auth-auth:_userExists',
                        'User exists en Elasticsearch. Check if Service exists in the elasticsearch document');

                    return esUser.userAccountExists(userID, self.serviceName).then(function (res) {
                        // User exists everywhere
                        if (res.hits.total) {
                            logger.log('res:controller:auth-auth:_userExists', 'User exists and is fully connected');
                            return res.hits.hits[0]._source;
                        } else {
                            logger.log('res:controller:auth-auth:_userExists',
                                'User is connected with Service in MySQL but not in ES for some reason');

                            throw {
                                userID: userID,
                                elastic: true,
                                service: true,
                                missingElasticService: false,
                                onlyMail: false
                            };
                        }
                    }, onError);

                } else {
                    logger.log('res:controller:auth-auth:_userExists',
                        'User connected with Service, we do not have an Elasticsearch entry for them');

                    throw {
                        userID: userID,
                        elastic: false,
                        service: true,
                        onlyMail: false
                    };
                }
            }, onError);
        }, function () {
            logger.log('res:controller:auth-auth:_userExists', 'user doesn\'t exist in MySQL');

            // If we're only doing a basic userID + email match check, we don't need anything else
            if (basicCheck) {
                throw false;
            }

            // User not connected with Service, check if email exists anywhere in the MySQL database
            logger.log('req:controller:auth-auth:_userExists', 'check if email exists anywhere in MySQL');
            return sqlUser.get.userByEmail(userData.email)

            .then(function (user) {
                logger.log('res:controller:auth-auth:_userExists', 'User\'s e-mail exists in MySQL');
                var userID = user.userID;

                logger.log('req:controller:auth-auth:_userExists', 'Check if the user exists in Elasticsearch');
                return esUser.userExists(user.userID).then(function (exists) {
                    logger.log('res:controller:auth-auth:_userExists', 'Response returned');

                    // User exists in elasticsearch
                    if (exists) {
                        logger.log('controller:auth-auth:_userExists', 'User exists in Elasticsearch');
                        throw {
                            userID: userID,
                            elastic: true,
                            service: false
                        };
                    }

                    logger.log('controller:auth-auth:_userExists', 'User does not exist in Elasticsearch');
                    // No Service connection, no Elasticsearch
                    throw {
                        userID: userID,
                        elastic: false,
                        service: false
                    };
                }, onError);

            }, function () {
                logger.log('res:controller:auth-auth:_userExists', 'User is not connected in any way');
                // Ain't connected with nothing
                throw {
                    userID: null,
                    elastic: false,
                    service: false,
                    onlyMail: false
                };
            });
        });
    };


    /**
     * Extends existing MySQL and Elasticsearch user rows/docs
     * with 3rd party serice account info
     *
     * @param  {string} userID    Unique Chatterplot user id
     * @param  {object} userData
     * @param  {object} authData
     */
    Auth.prototype._extendAccount = function (userID, userData, authData) {
        logger.log('req:controller:auth-auth:_extendAccount', 'Extending users account in MySQL and Elasticsearch');
        return Q.all([
            this._extendMysqlAccount(userID, userData, authData), esUser.updateUser(userID, {
                type: this.serviceName,
                id: userData.id
            }, 'accounts')
        ]);
    };


    /**
     * Extends an existing MySQL user entry with account info
     * for the selected 3rd party service
     *
     * @param  {string} userID    Unique Chatterplot user id
     * @param  {object} userData
     * @param  {object} authData
     */
    Auth.prototype._extendMysqlAccount = function (userID, userData, authData) {
        logger.log('controller:auth-auth:_extendMysqlAccount', 'called');

        let updateData = {},
            where = {
                user_id: userID
            };

        updateData[serviceKeys[this.serviceName]] = userData.id;

        logger.log('req:controller:auth-auth:_extendMysqlAccount',
            'updating users service field and inserting into that service\'s row');



        return Q.all([
            sqlUser.update.userWithFields(updateData, where),
            sqlUser.insert.userIntoService(this.serviceName, {
                serviceID: userData.id,
                access_token: authData.access_token
            })
        ])["catch"](function (err) {
            logger.log('req:controller:auth-auth:_extendMysqlAccount', 'Error Updating user in MySQL', err);
            throw err;
        });
    };


    /**
     * Extends an existing Elasticsearch document with 3rd party account info
     *
     * @param  {string} userID    Unique Chatterplot user id
     * @param  {object} userData
     */
    Auth.prototype._extendElasticAccount = function (userID, userData) {
        logger.log('req:controller:auth-auth:_extendElasticAccount',
            'Adding service account data for user in existing Elasticsearch document');

        return esUser.updateUser(userID, {
            type: this.serviceName,
            id: userData.id
        }, 'accounts');
    };


    /**
     * Formats user data depending on the data passed in by the 3rd party
     * service then inserts the user into Elasticsearch
     *
     * @param  {object} userData
     * @return {object}           Formatted user data
     */
    Auth.prototype._elasticInsertUser = function (userData, authData) {
        logger.log('req:controller:auth-auth:_elasticInsertUser',
            'Formatting user to save in Elasticsearch');

        var formattedUser;

        return this.formatUser(userData, authData).then(function (user) {
            logger.log('req:controller:auth-auth:_elasticInsertUser',
                'Successfully formatted the user - Saving to Elasticsearch');

            return esUser.addUser(formattedUser = user, user.userID).then(function () {
                return user;
            });
        }, function (err) {
            logger.log('error:controller:auth-auth:_elasticInsertUser',
                'Error Formatting user using userData:', userData, err);

            throw err;
        }).then(null, function (err) {
            logger.log('error:controller:auth-auth:_elasticInsertUser',
                'Error adding user to Elasticsearch using:', formattedUser, err);

            throw err;
        });
    };


    /**
     * Formats a user for elasticsearch getting all the details we can pull from a service
     */
    Auth.prototype.formatUser = function (userData, authData, requestLocale) {
        var formattedUser = null,
            self = this;

        if (requestLocale) {
            userData.settings = userData.settings || {};
            userData.settings.locale = localeMap[requestLocale];
        }

        logger.log('req:controller:auth-auth:formatUser',
            'Formatting user to save in Elasticsearch');

        return this._formatUserData(userData, authData).then(function (user) {
            logger.log('res:controller:auth-auth:formatUser',
                'Successfully formatted the user via service - getting location details');

            formattedUser = user;
            return self._extractLocationNames(user.coords);
        }, function (err) {
            logger.log('error:controller:auth-auth:formatUser',
                'Error Formatting user using userData:', userData, err);

            throw err;
        }).then(function (location) {
            if (location) {
                _.extend(formattedUser, location);
            }
            return formattedUser;
        }, function (err) {
            logger.log('error:controller:auth-auth:formatUser',
                'Error adding user to Elasticsearch using:', formattedUser, err);

            throw err;
        });
    };

    Auth.prototype._formatUserData = Q.when;

    Auth.prototype._extractLocationNames = function (coords) {
        if (!coords) {
            return Q.resolve();
        }

        return geocoder.latlng2Address(coords).then(function (places) {
            if (!places[0]) {
                return false;
            }
            var length = places.length,
                index = Math.floor(length * .666);

            return geocoder.scrapePlace(places[index], 'google');
        });
    };


    /**
     * Returns an existing session for a service if it exists
     *
     * @param  {IncomingRequest}  request
     * @param  {OutgoingResponse} response
     * @return {Object|Null}                User data if exists
     */
    Auth.prototype.getSession = function (request, response) {
        let ref = request.session;

        if ((ref ? ref[this.sessionKey] : null)) {
            return response.json(request.session[this.sessionKey]);
        } else {
            return response.json({
                nodata: true
            });
        }
    };


    /**
     * Checks for an existing session for specific service and user data
     * If it exists, returns the user data
     *
     * @param  {IncomingRequest}  request
     */
    Auth.prototype._checkSession = function (request) {
        var deferred, ref;
        deferred = Q.defer();
        if ((((ref = request.session) != null ? ref[this.sessionKey] : void 0) != null) && (request.session.user != null)) {
            deferred.resolve(request.session.user);
        }
        deferred.reject();
        return deferred.promise;
    };


    /**
     * Saves user to the current Session
     *
     * @param  {object}          userData
     * @param  {object}          authData
     */
    Auth.prototype._saveSession = function (request, userData, authData) {
        var deferred;
        deferred = Q.defer();
        if (request.session) {
            request.session[this.sessionKey] = authData;
            request.session.user = userData;
        }
        deferred.resolve(userData);
        return deferred.promise;
    };


    /**
     * Removes all session data for the service if provided.
     * If no service provided, removes all session data
     *
     * @param  {IncomingRequset}  request
     * @return {boolean}          Whether the key was removed
     */
    Auth.prototype._clearSession = function (request) {
        var session;
        if (!request.session) {
            return false;
        }
        session = request.session;
        if (this.sessionKey) {
            session[this.sessionKey] = null;
        } else {
            _.each(session, function (val, key) {
                if (key === 'cookie') {
                    return true;
                }
                if (session.hasOwnProperty(key)) {
                    return session[key] = null;
                }
            });
        }
        return true;
    };


    /**
     * Handles the oauth response coming back from the service with
     * the url containing the code to extend the oauth token
     *
     * @param  {IncomingRequest} request
     * @param  {OutboundResponse} response
     */
    Auth.prototype.oauthConnect = function (request, response) {
        const self = this,
            sent = {
                sent: false
            },

            respond = {
                name: this.displayName,
                data: null,
                success: null,
                error: null,
                token: null,
                newUser: null,
                newConnection: null
            },

            requestLocale = request.query.state,

            send = function () {
                sent.sent = true;
                return response.render('standalone/oauthPopupCallback', {
                    service: respond
                });
            },

            successCallback = function (userData) {
                var token;
                logger.log('controller:auth-auth:oauthConnect-successCallback', 'called');
                if (sent.sent) {
                    throw false;
                }
                sent.sent = true;
                respond.data = userData;
                respond.success = true;

                throw jwtHelper.tokenFromUser(userData).catch(function (err) {
                    logger.log('error:controller:auth-auth:oauthConnect-successCallback',
                        'Error creating token from passed in userData\nerror:\n', err);

                    throw failCallback({
                        type: 500,
                        response: 'ERROR.SERVER_ERROR'
                    });
                })

                .then(function (token) {
                    logger.log('controller:auth-auth:oauthConnect-successCallback',
                        'Successfully verified');

                    respond.token = token;
                    send();

                    if (respond.newUser) {
                        Email.welcomeEmail(userData);
                    }
                    if (respond.newConnection) {
                        User.emit('/new/account-connection', self.displayName, userData);
                    }
                });
            },

            failCallback = function (err, dontThrow) {
                logger.log('controller:auth-auth:oauthConnect-failCallback', 'called');
                if (sent.sent) {
                    if (dontThrow) {
                        return false;
                    }
                    throw false;
                }
                logger.log('warning:controller:auth-auth:oauthConnect-failCallback',
                    'Error verifying user', err);

                respond.success = false;
                respond.error = err;
                self._clearSession(request);
                send();
                if (dontThrow) {
                    return false;
                }
                throw false;
            };

        if (request.query.error) {
            return failCallback(request.query, true);
        }

        // Check whether a session for this user already exists
        return this._checkSession(request)

        .then(successCallback)["catch"](function () {
            // Session exists, return user
            var authData, userData;

            // Else, exchange for oauth code
            return self.exchangeToken(request.query[self.oauthResponseKey], false, requestLocale)

            // Grabs the users info from the service
            .then(function (_authData) {
                authData = _authData;
                return self.getUser(authData);
            })["catch"](failCallback)

            // Check if the user is already registered with us
            .then(function (_userData) {
                userData = _userData;
                if (!userData.email) {
                    throw failCallback({
                        response: 404,
                        customMsg: 'This ' + self.serviceName + ' account is missing an email. Cannot continue.'
                    });
                }
                return self._userExists(userData, authData);
            })["catch"](function (connection) {
                var authInsert, calls, lastCall, saveCall, subCalls, userID;

                if (sent.sent) {
                    throw false;
                }

                // If no user with exact connections,
                // then check what we do have of the user and create if nothing exists
                logger.log('res:controller:auth-auth:oauthConnect',
                    'User\'s not fully connected. Continuing connection process.');

                calls = [];

                // User exists in MySQL
                if (connection.userID) {
                    logger.log('controller:auth-auth:oauthConnect',
                        'User has userID in database. Building subCalls');

                    userData.userID = userID = connection.userID;
                    subCalls = [];
                    logger.log('req:controller:auth-auth:oauthConnect',
                        'Retrieving users permissions & if activated from MySQL `users`');

                    subCalls.push(sqlUser.get.from('users', {
                        user_id: userID
                    }, ['role', 'activated']));

                    // User does NOT exist in elasticsearch
                    if (!connection.elastic) {
                        logger.log('req:controller:auth-auth:oauthConnect',
                            'Inserting user into Elasticsearch');

                        subCalls.push(self._elasticInsertUser(userData));
                    }

                    // User has NO @service account in MySQL
                    if (!connection.service) {
                        respond.newConnection = true;
                        logger.log('req:controller:auth-auth:oauthConnect',
                            'Extending user with new Service');

                        subCalls.push(self._extendMysqlAccount(userID, userData, authData));
                    }

                    // User's elasticsearch account does not have @service info though MySQL does
                    if ((connection.elastic && !connection.service) || connection.missingElasticService) {
                        logger.log('req:controller:auth-auth:oauthConnect',
                            'Adding new service connection into Elasticsearch');

                        subCalls.push(self._extendElasticAccount(userID, userData));
                    }

                    // Resolve all promises
                    lastCall = Q.all(subCalls).then(function (arg) {
                        var userSQL = arg[0];
                        logger.log('req:controller:auth-auth:oauthConnect',
                            'Completed all subCalls - Retrieving user from Elasticsearch');

                        return esUser.retrieveUserById(connection.userID).then(function (user) {
                            user.permissions = parseInt(userSQL.role, 10);
                            if (!parseInt(userSQL.activated, 10)) {
                                user.notActivated = true;
                            }
                            return user;
                        }, function (err) {
                            return logger.log('error:controller:auth-auth:oauthConnect',
                                'Error retrieving user from Elasticsearch', err);

                        });
                    })["catch"](failCallback);
                    calls.push(lastCall);
                } else {
                    // User doesn't exist anywhere

                    logger.log('controller:auth-auth:oauthConnect',
                        'User does not exist anywhere. Creating new User.');
                    respond.newUser = true;

                    authInsert = {
                        services: [
                            {
                                type: self.serviceName,
                                id: userData.id,
                                accessToken: authData.access_token || authData.accessToken
                            }
                        ],
                        userGroup: config.defaults.permissions.activated
                    };
                    logger.log('req:controller:auth-auth:oauthConnect',
                        'Formatting userData for Elasticsearch');

                    saveCall = self.formatUser(userData, authData, requestLocale)["catch"](function (err) {
                        logger.log('error:controller:auth-auth:oauthConnect',
                            'Error formatting user for Elasticsearch for service:', self.serviceName,
                            'using userData:', userData, '\nerror:\n', err);

                        throw false;
                    })

                    .then(function (formattedUser) {
                        logger.log('req:controller:auth-auth:oauthConnect',
                            'User formatted for Elasticsearch - Saving user to the databases');

                        // Update userData with updated formatted value
                        return User.save(userData = formattedUser, authInsert);
                    })["catch"](function (err) {
                        logger.log('error:controller:auth-auth:oauthConnect',
                            'Error saving user into the databases using userData:', userData,
                            'authInsert:', authInsert, '\nerror:\n', err);

                        throw failCallback();
                    })

                    .then(function (user) {
                        logger.log('res:controller:auth-auth:oauthConnect',
                            'User successfully saved');

                        //* User.save appends the userID and permissions to the passed in object */
                        // userData.userID = user.userID;
                        // userData.notActivated = true;
                        // userData.permissions = authInsert.permissions;
                        mailchimpController.extendMCUser(userData);

                        return userData;
                    });
                    calls.push(saveCall);
                }

                // On success save session
                return Q.all(calls).then(function (arg) {
                    var user;
                    user = arg[0];
                    logger.log('res:controller:auth-auth:oauthConnect',
                        'Q.all continue. User either saved/extended/verified');

                    return self._saveSession(request, user, authData);
                })["catch"](failCallback);
            })

            // User exists, grab permissions, save session and return
            .then(function (user) {
                return sqlUser.get.from('users', {
                    user_id: user.userID
                }, ['role']).then(function (data) {
                    user.permissions = data.role;
                    return self._saveSession(request, user, authData);
                });
            })["catch"](failCallback)

            // And return
            .then(successCallback)["catch"](failCallback);
        });
    };


    /**
     * Verify whether user exists and is connected with the passed in credentials
     *
     * @param  {IncomingRequest}  request
     * @param  {OutgoingResponse} response [description]
     */
    Auth.prototype.oauthVerify = function (request, response) {
        var failCallback, respond, self, send, sent, successCallback;
        logger.log('controller:auth-auth:oauthVerify', 'called');
        self = this;
        sent = {
            sent: false
        };

        respond = {
            name: this.displayName,
            data: null,
            success: null,
            error: null,
            token: null
        };

        send = function () {
            sent.sent = true;
            return response.render('standalone/oauthPopupCallback', {
                service: respond
            });
        };

        successCallback = function (userData) {
            logger.log('controller:auth-auth:oauthVerify-successCallback', 'called');
            if (sent.sent) {
                throw false;
            }

            logger.log('controller:auth-auth:oauthVerify-successCallback', 'Successfully verified');
            sent.sent = true;
            respond.data = userData;
            respond.success = true;

            throw jwtHelper.tokenFromUser(userData).catch(function (err) {
                logger.log('error:controller:auth-auth:oauthVerify-successCallback',
                    'Error creating token from passed in userData\nerror:\n', err);

                throw failCallback({
                    type: 500,
                    response: 'ERROR.SERVER_ERROR'
                });
            })

            .then(function (token) {
                logger.log('controller:auth-auth:oauthVerify-successCallback',
                    'Successfully verified');

                respond.token = token;
                send();
            });
        };

        failCallback = function (err, dontThrow) {
            logger.log('controller:auth-auth:oauthVerify-failCallback', 'called');
            if (sent.sent) {
                if (dontThrow) {
                    return false;
                }
                throw false;
            }
            logger.log('warning:controller:auth-auth:oauthVerify-failCallback',
                'Error verifying user', err);

            respond.success = false;
            respond.error = err || true;
            send();
            if (dontThrow) {
                return false;
            }
            throw false;
        };

        if (request.query.error) {
            logger.log('error:controller:auth-auth:oauthVerify',
                'Missing `request.query.error`. query:', request.query);

            return failCallback(request.query, true);
        }

        logger.log('req:controller:auth-auth:oauthVerify',
            'Checking whether a session for this user already exists');

        return self._checkSession(request)

        // Session exists, return user
        .then(successCallback)
        ["catch"](function () {
            var authData, userData;

            logger.log('req:controller:auth-auth:oauthVerify',
                'Session does not exist for request -',
                'Running services exchangeToken');

            // Else, exchange for oauth verify code
            return self.exchangeToken(request.query[self.oauthResponseKey], true, request.query.state)

            // Grabs the users info from said service
            .then(function (_authData) {
                authData = _authData;
                logger.log('req:controller:auth-auth:oauthVerify',
                    'ExchangedToken && Requesting user from service:', self.serviceName);

                return self.getUser(authData);
            })["catch"](failCallback)

            // Check that the user is already registered with us
            .then(function (_userData) {
                logger.log('res:controller:auth-auth:oauthVerify',
                    'Successfully fetched userData from service');

                userData = _userData;
                logger.log('req:controller:auth-auth:oauthVerify',
                    'Verifying whether the user is connected');

                return self._userExists(userData, authData, true);
            })["catch"](failCallback)

            // get User + Permissions
            .then(function (userID) {
                logger.log('req:controller:auth-auth:oauthVerify',
                    'Successfully verified user -',
                    'Fetching users permissions, activation status, and data from MySQL & Elasticsearch');

                return Q.all([
                    esUser.retrieveUserById(userID), sqlUser.get.from('users', {
                        user_id: userID
                    }, ['role', 'activated'])
                ])

                .then(function (arg) {
                    var user = arg[0],
                        sqlData = arg[1];

                    logger.log('res:controller:auth-auth:oauthVerify',
                        'Successfully received userdata, permissions + activation status');

                    user.permissions = parseInt(sqlData.role, 10);
                    if (sqlData.activated === '0') {
                        user.notActivated = true;
                    }
                    return user;
                });
            })["catch"](failCallback)

            // Save to session
            .then(function (user) {
                logger.log('req:controller:auth-auth:oauthVerify',
                    'Saving fetched user to session');

                return self._saveSession(request, user, authData);
            })["catch"](failCallback)

            .then(successCallback)["catch"](failCallback);
        });
    };

    return Auth;
})();

onError = function (err) {
    return logger.log('error:controller:auth:*', err);
};
