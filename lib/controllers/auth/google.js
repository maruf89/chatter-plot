'use strict';

const SERVICE_NAME = 'google',
    SESSION_KEY = 'googleUser',
    OAUTH_RESPONSE_KEY = 'code',

    hostURL = 'https://accounts.google.com',
    exchangeURI = '/o/oauth2/token',
    googleapis = require('googleapis'),
    Q = require('q'),
    _ = require('lodash-node/modern'),
    logger = require('bragi'),

    Fetch = GLOBAL.Fetcher,

    dataFerry = Fetch.util('dataFerry'),
    Util = Fetch.util('utility'),
    Auth = Fetch.controller('auth', 'auth'),
    OAuth2 = googleapis.auth.OAuth2,
    plus = googleapis.plus('v1'),

    // The length of time to await an oauth callback
    timeoutLength = 2.5 * 60 * 1000;


var instance,
    serviceConfig = Fetch.config('socialServices')[SERVICE_NAME];

const credentials = Fetch.util('parseConfig').parse(serviceConfig);

// Tell the dataferry to expect data for 'google'
dataFerry.await(SERVICE_NAME);

const _oauthClientInit = function () {
    // return if already exists
    if (this.oauth2Client) {
        return this.oauth2Client;
    }

    return this.oauth2Client = new OAuth2(
        credentials.clientID,
        credentials.clientSecret,
        credentials.verifyURL
    );
},

Google = (function (superClass) {
    Util.extendClass(Google, superClass);

    function Google() {
        var ferryData;

        this.sessionKey = SESSION_KEY;
        this.serviceName = SERVICE_NAME;
        this.oauthResponseKey = OAUTH_RESPONSE_KEY;

        _oauthClientInit.call(this);

        ferryData = {
            clientID: credentials.clientID,
            cookiePolicy: credentials.cookiePolicy,
            scope: credentials.scope,
            verifyURL: this.oauth2Client.generateAuthUrl({
                access_type: 'offline',
                scope: credentials.scope
            }),
            apiKey: credentials.apiKey
        };

        // update the redirect to the new verify value 
        this._setRedirectUri(credentials.connectURL);
        ferryData.connectURL = this.oauth2Client.generateAuthUrl({
            access_type: 'offline',
            scope: credentials.scope
        });

        // revert the uri 
        this._revertRedirectUri();
        dataFerry.store(SERVICE_NAME, ferryData);
        Google.__super__.constructor.apply(this, arguments);
    }

    Google.prototype._setRedirectUri = function (uri, state) {
        this._prevRedirect = this.oauth2Client.redirectUri_;
        this.oauth2Client.redirectUri_ = (state ? 'https://' + state : '') + uri;
    };

    Google.prototype._revertRedirectUri = function (state) {
        this.oauth2Client.redirectUri_ = 'https://' + state + this._prevRedirect;
        this._prevRedirect = null;
    };

    Google.prototype.exchangeToken = function (code, verify, state) {
        let deferred = Q.defer();
        const URL = credentials[verify ? 'verifyURL' : 'connectURL'];

        this._setRedirectUri(URL, state);

        this.oauth2Client.getToken(code, function (err, tokens) {
            if (!verify) {
                this._revertRedirectUri(state);
            }
            if (err) {
                return deferred.reject(err);
            }
            return deferred.resolve(tokens);
        }.bind(this));

        return deferred.promise;
    };

    /**
     * Gets the primary email of a user
     *
     * @param  {object} user  the google user object
     * @return {string}       the primary email
     */
    Google.prototype._extractEmail = function (user) {
        var primaryEmail = null;

        user.emails.every(function (email) {
            if (email.type === 'account') {
                primaryEmail = email.value;
                return false;
            }
            return true;
        });

        return primaryEmail;
    };

    Google.prototype.getUser = function (authData) {
        var deferred = Q.defer(),
            accessToken = authData.access_token || authData.accessToken,
            self = this;

        this.oauth2Client.credentials = {
            access_token: accessToken
        };

        plus.people.get({
            userId: 'me',
            auth: this.oauth2Client
        }, function (err, data) {
            if (err) {
                return deferred.reject(err);

            }
            var userData = {
                id: data.id,
                firstName: data.name.givenName,
                lastName: data.name.familyName,
                details: {
                    gender: data.gender ? data.gender.toUpperCase() : null
                },
                locale: data.language,
                email: self._extractEmail(data),
                picture: {
                    hasPhoto: true,
                    service: 'gplus',
                    id: data.id
                },
                accounts: [{
                    type: 'google',
                    id: data.id
                }]
            };

            return deferred.resolve(userData);
        });

        return deferred.promise;
    };

    return Google;
})(Auth);

exports.client = instance = new Google();

exports.getUser = instance.getUser;
