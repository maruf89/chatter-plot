"use strict";

const logger = require('bragi'),
    Q = require('q'),

    Fetch = GLOBAL.Fetcher,

    User = Fetch.type('user'),
    jwtHelper = Fetch.util('jwtHelper'),
    Constants = Fetch.config('misc/constants'),

    services = {
        // facebook
        // linkedin
        // google
    };

exports.services = services;

var _send = function (sent, err, response) {
    if (sent.sent) {
        return true;
    }

    this[err ? 'reject' : 'resolve'](sent.sent = (err || response));
    return err || response;
};

exports.attemptSocialConnect = function (serviceName, data) {

    if (!services[serviceName]) {
        return Q.reject('Invalid call, service does not exist');
    }

    var deferred = Q.defer(),
        sent = {
            sent: null
        },
        send = _send.bind(deferred, sent),

        service = services[serviceName],
        userData = {};

    logger.log('res:controller:auth:attemptSocialConnect', 'Getting user from facebook using client side token');
    service.formatAttemptData(data).catch(function (err) {
        logger.log('error:controller:auth:attemptSocialConnect',
            'getting users from 3rd party service\nerror:\n', err);
    })

    .then(function (userWhere) {
        // returns an object with fields corresponding to MySQL where
        userData.email = userWhere.email;

        logger.log('res:controller:auth:attemptSocialConnect', 'Checking whether the users in our DB');
        return User.searchSecure('users', userWhere, ['user_id']);
    }).catch(function (err) {
        if (sent.sent) { throw err; }

        if (err !== 404) {
            logger.log('error:controller:auth:attemptSocialConnect',
                'Error checking if user exists using service:', serviceName,
                'data:', data, '\nerror:\n', err);
        } else err = 'User not found';

        throw send(err);
    })

    // user exists in our db
    .then(function (_user) {
        logger.log('res:controller:auth:attemptSocialConnect', 'user exists');

        userData.userID = parseInt(_user.user_id, 10);
            
        return jwtHelper.tokenFromUser(userData).then(function (token) {
            userData.token = token;
            return send(null, userData);
        }).catch(function (err) {
            logger.log('error:controller:auth:attemptSocialConnect',
                'Error generating token from user using:', userData, 'data:', data);
            throw send(err);
        });

    }).catch(function (err) {
        if (sent.sent) { throw err; }

        throw send(err);
    });

    return deferred.promise;
};

process.nextTick(function () {
    services.facebook = Fetch.controller('auth', 'facebook').client;
    services.linkedin = Fetch.controller('auth', 'linkedin').client;
    services.google = Fetch.controller('auth', 'google').client;
});