'use strict';
var Flow, Q, _, addMissingCoords, client, db, elasticsearch, fieldActions, flow, fs, geocoder, getSchools, importSchools, libDir, parseSchools, path, schoolFields, uploadSchools,
    slice = [].slice;

path = require('path');

fs = require('fs');

libDir = path.join(process.cwd(), 'lib');

_ = require('lodash-node/modern');

Q = require('q');

Flow = require(path.join(libDir, 'services', 'flow')).Flow;

flow = new Flow();

db = {
    index: 'users',
    type: 'schools',
    idKey: 'esId'
};

elasticsearch = require(path.join(libDir, 'services', 'elasticsearch'));

client = elasticsearch.client;

geocoder = require(path.join(libDir, 'util', 'geocoderPromise'));


/**
 * Fields from the SIBL database and their elasticsearch key names on right
 * @type {Object}
 */

schoolFields = {
    "Company Name": "companyName",
    "Executive First Name": "executiveFirstName",
    "Executive Last Name": "executiveLastName",
    "Address": "address",
    "City": "city",
    "State": "state",
    "ZIP Code": "zipCode",
    "Phone Number Combined": "phoneNumberCombined",
    "Website": "website",
    "Company Description": "companyDescription",
    "Primary SIC Description": "primarySicDescription",
    "SIC Code 1 Description": "sicCode1Description",
    "SIC Code 2 Description": "sicCode2Description",
    "SIC Code 3 Description": "sicCode3Description",
    "Primary NAICS Description": "primaryNaicsDescription",
    "NAICS 1 Description": "Naics1Description",
    "NAICS 2 Description": "Naics2Description",
    "NAICS 3 Description": "Naics3Description",
    "Location Employee Size Range": "locationEmployeeSizeRange",
    "Type of Business": "Private",
    "Location Type": "locationType"
};


/**
 * Object of actions to apply to fields
 * @type {Object}
 */

fieldActions = {

    /**
     * Removes non-number characters from the phone number string
     * @param  {string} phoneString  Formatted phone #
     * @return {string}              Raw phone #
     */
    "phoneNumberCombined": function (phoneString) {
        return phoneString.match(/(\d{3,4})/g).join('');
    }
};


/**
 * Accepts an array of JSON data. It prepares it for elastic search and adds it
 *
 * @param  {array} data  JSON objects for indexing
 * @return {Promise}
 */

exports.importSchools = importSchools = function (data) {
    var bulkData;
    bulkData = elasticsearch.bulkFormat(data, 'create', db.index, db.type, db.idKey);
    return client.bulk({
        body: bulkData
    });
};


/**
 * Iterates over a JSON file and extracts/formats only the fields we desire
 *
 * @param  {string} filename  A
 */

parseSchools = function (filename) {
    var deferred;
    deferred = Q.defer();
    fs.exists(filename, function (exists) {
        var basename, data, e, parsedData;
        if (!exists) {
            return deferred.reject(new Error("Trying to read file " + filename + " which does not exist in " + __filename));
        }
        try {
            data = require(filename);
        } catch (_error) {
            e = _error;
            return deferred.reject(new Error(e, "Error opening JSON schools file " + filename + " in " + __filename));
        }
        parsedData = [];
        basename = path.basename(filename);
        _.each(data, function (school, index) {
            var hasValues, save;
            save = {};
            hasValues = false;
            _.each(schoolFields, function (dbKey, xlKey) {
                var value;
                value = school[xlKey];

                // Skip if it's a useless value
                if (!value) {
                    return true;
                }

                // Otherwise mark that this object won't be empty
                hasValues = true;

                // if the field needs any parsing eg. extract phone #'s from different formats
                if (fieldActions[dbKey]) {
                    value = fieldActions[dbKey](value);
                }
                return save[dbKey] = value;
            });
            if (hasValues) {
                // Save unique elasticsearch id reference - See `schoolIdKey` definition for reason why we need this
                save[db.idKey] = "" + basename + index;
                return parsedData.push(save);
            }
        });
        return deferred.resolve(parsedData);
    });
    return deferred.promise;
};

exports.getSchools = getSchools = function (req, res) {
    return flow.get(req, function (status, filename, original_filename, identifier) {
        return console.log('getSchools');
    });
};


/**
 * Expects a JSON document to be uploaded via Flow.
 * Sends the request to flow to assemble the file, and once the file is downloaded
 * sends it's contents to be parsed and added to the database
 *
 * @param  {IncomingRequest}  req
 * @param  {OutgoingResponse} res
 */

exports.uploadSchools = uploadSchools = function (req, res) {
    return flow.post(req, res, function (response) {
        switch (response.response) {
            case 500:
                return res.json(response);
            case 200:
                return parseSchools(response.filename).then(importSchools).then(function (esResp) {
                    res.json({
                        success: true,
                        file: response.filename,
                        errors: esResp.errors,
                        items: esResp.items
                    });

                    // We don't need the file after it's data has been added to the database
                    return fs.unlink(response.filename);
                }, function (err) {
                    var error;
                    error = new Error(err, 'Error importing schools into the database');
                    return res.json({
                        success: false,
                        file: response.filename,
                        error: error
                    });
                });
        }
    });
};


/**
 * Queries the database for all schools without lat/long coords and adds them
 */

exports.addMissingCoords = addMissingCoords = function () {
    return client.search({
        index: db.index,
        type: db.type,
        body: {
            query: {
                filtered: {
                    filter: {
                        bool: {
                            must: [
                                {
                                    missing: {
                                        field: 'location',
                                        existence: true,
                                        null_value: true
                                    }
                                }, {
                                    exists: {
                                        field: 'address'
                                    }
                                }
                            ]
                        }
                    }
                }
            }
        }
    }).then(function (schools) {
        return Q.all(schools.hits.hits.map(function (school) {
            var search;
            search = school._source.address + " " + school._source.zipCode + ", " + school._source.state;
            return geocoder.geocode(search).then(function (coords) {
                coords = coords.results[0] != null ? coords.results[0].geometry.location : false;
                return {
                    location: coords,
                    id: school._id
                };
            });
        }));
    }, function (err) {
        return console.log(new Error(err, 'Error fetching schools with missing location coords'));
    }).then(function () {
        var formatted, schools;
        schools = 1 <= arguments.length ? slice.call(arguments, 0) : [];
        formatted = elasticsearch.bulkFormat(schools[0], 'update', db.index, db.type);
        return client.bulk({
            body: formatted
        });
    }).then(function (resp) {
        return console.log('response: ', resp);
    }, function (err) {
        return console.log(new Error(err, 'Error updating schools with missing coords'));
    });
};

exports.onSocketConnection = function () {
    var socket;
    return socket = this;
};

exports.onSecureSocketConnection = function () {
    var socket;
    socket = this;
    socket.on('languageSchools>', function (opts) {
        var params, sort, sortBy;
        if (opts == null) {
            opts = {};
        }
        sortBy = opts.sortBy || 'companyName';
        sort = sortBy + ':' + (opts.sort || 'asc');
        params = {
            index: db.index,
            type: db.type,
            size: opts.size || 25,
            from: opts.from || 0,
            sort: sort
        };

        // If searching for empty/valued fields, then change filter
        if (opts.empty) {
            if (opts.empty === 'true') {
                params.body = {
                    query: {
                        constant_score: {
                            filter: {
                                missing: {
                                    field: sortBy,
                                    existent: true,
                                    null_value: true
                                }
                            }
                        }
                    }
                };
            }
        }
        return client.search(params).then(function (schools) {
            var count, data;
            count = schools.hits.total;
            data = schools.hits.hits;
            socket.emit('languageSchools<', {
                success: true,
                count: count,
                data: data
            });
            return addMissingCoords();
        }, function (err) {
            console.log(new Error(err, 'Error retrieving schools from the database using opts:', opts));
            return socket.emit('languageSchools<', {
                success: false,
                error: err
            });
        });
    });
    return socket.on('editLanguageSchools>', function (opts) {
        var callback, emit, requests;
        emit = {
            error: !opts.action ? 'Missing opts.action' : null,
            response: null,
            success: false
        };
        callback = function () {
            return socket.emit('editLanguageSchools<', emit);
        };
        if (emit.error) {
            return callback();
        } else {
            requests = elasticsearch.bulkFormat(opts.data, opts.action, db.index, db.type, opts.idKey);
            return client.bulk({
                body: requests
            }).then(function (res) {
                emit.response = res;
                return callback();
            });
        }
    });
};
