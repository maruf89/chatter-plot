/**
 * This opens up specific functions from one controller to another
 */

"use strict";

const Fetch = GLOBAL.Fetcher;

const venueCtrl = Fetch.controller('venue');

exports.safeSave = venueCtrl.safeSave;
