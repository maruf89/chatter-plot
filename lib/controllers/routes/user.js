'use strict';

var router = require('express').Router(),

    Fetch = GLOBAL.Fetcher,

    userController = Fetch.controller('user');

router.post('/get', function (req, res) {
    return userController.get(req.body, false, false).then(function (data) {
        return res.json(data);
    });
});

exports.router = router;
