'use strict';

var logger = require('bragi'),
    router = require('express').Router(),

    Fetch = GLOBAL.Fetcher,
    eventController = Fetch.controller('event');

router.post('/get', function (req, res) {
    logger.log('routes:event:get', 'called');
    var promise = eventController.get(req.body).then(eventController.formatGetResults);

    if (req.body.venues) {
        promise = promise.then(eventController.getResult2VenueExtend.bind(eventController, req.body));
    }

    return promise.then(function (data) {
        logger.log('routes:event:get', 'got response');
        return res.json(data);
    })["catch"](function () {});
});

router.post('/fetch', function (req, res) {
    var promise = eventController.search(req.body).then(eventController.formatSearchResults);

    if (opts.venues) {
        // compose the callback to use our options
        promise = promise.then(eventController.searchResult2VenueExtend.bind(eventController, req.body));
    }

    return promise.then(function (data) {
        return res.json(data);
    })["catch"](function () {});
});

exports.router = router;
