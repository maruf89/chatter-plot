"use strict";


const router = require('express').Router(),
    _ = require('lodash-node/modern'),

    Fetch = GLOBAL.Fetcher,

    authAPI = Fetch.controller('auth', 'auth'),
    authController = Fetch.controller('auth');


router.param('service', function (request, response, next, serviceName) {
    if (!authController.services[serviceName]) {
        return response.render('standalone/invalid', {
            message: 'Invalid service'
        });
    }
    request.serviceInstance = authController.services[serviceName];
    return next();
});

router.get('/:service/callback', function (request, response) {
    return request.serviceInstance.oauthConnect(request, response);
});

router.get('/:service/verify_callback', function (request, response) {
    return request.serviceInstance.oauthVerify(request, response);
});

router.get('/clear_session', function (request, response) {
    return response.json({
        service: 'all',
        cleared: authAPI.prototype._clearSession(request)
    });
});

router.get('/:service/clear_session', function (request, response) {
    return response.json({
        service: request.params.service,
        cleared: request.serviceInstance._clearSession(request)
    });
});

function parseCookies (request) {
    var list = {},
        rc = request.headers.cookie;

    rc && rc.split(';').forEach(function ( cookie ) {
        var parts = cookie.split('=');
        list[parts.shift().trim()] = decodeURI(parts.join('='));
    });

    return list;
}

//router.get('/get_session', function (request, response) {
//    response.json({
//        cookies: parseCookies(request),
//        session: request.session
//    });
//});

exports.router = router;
