'use strict';

/**
 * Email Controller
 *
 * Handles all e-mail endpoint calls between unsubscribe, update pref links
 * between Mailchimp and Mandrill
 */
var updatePreferences,

    router = require('express').Router(),
    _ = require('lodash-node/modern'),
    logger = require('bragi'),

    Fetch = GLOBAL.Fetcher,

    mcData = Fetch.config('mailchimp'),
    Mailchimp = Fetch.service('mailchimp'),
    Mandrill = Fetch.service('mandrill'),
    Email = Fetch.service('email'),
    Redis = Fetch.database('redis').client,
    User = Fetch.type('user'),
    userController = Fetch.controller('user'),
    mcHelper = Fetch.controller('mailchimp'),
    //emailController = Fetch.controller('email'),
    crypter = Fetch.util('encryptDecrypt'),
    sqlUser = Fetch.util('mysql', 'userHelper'),
    jwtHelper = Fetch.util('jwtHelper');

logger.log('init:controller:email', 'init');


/**
 * Checks the url for specific query strings and prepares them if they're available
 */
router.use(function (request, response, next) {
    var ek, email;
    logger.log('controller:email:route', 'routing request initiated');
    if (ek = request.query.ek) {
        email = crypter.decrypt(ek);
        logger.log('controller:email:route', 'Requesting member info from Mailchimp');
        return Mailchimp.lists.memberInfo(null, [
            {
                email: email
            }
        ]).then(function (userArr) {
            var emailGroups, groupings, interests, user;
            logger.log('controller:email:route', 'Got member info from Mailchimp');
            user = userArr.data[0];
            if (!user) {
                interests = emailGroups = [];
            } else {
                groupings = user.merges.GROUPINGS;
                interests = mcHelper.extractFromGroup(mcData.groupIDs['interests'], null, groupings);
                interests = mcHelper.matchGroupInfo(interests);
                emailGroups = mcHelper.extractFromGroup(mcData.groupIDs['emailGroups'], null, groupings);
                emailGroups = mcHelper.matchGroupInfo(emailGroups);
            }
            request.emailUser = {
                email: email,
                user: user,
                groups: {
                    interests: interests,
                    emailGroups: emailGroups
                }
            };
            request.unsubURL = "/email/unsubscribe?ek=" + request.query.ek;
            return next();
        }, function (err) {
            var error;
            error = {
                message: 'Error retrieving user.'
            };
            logger.log('error:controller:email:route', error.message);
            return response.render('standalone/invalid', error);
        });
    } else if (request.url.substring(0, 23) === '/update/add/chatterblog') {
        return next();
    } else {
        logger.log('controller:email:route', 'No passed in email token. Skip');
        request.skip = true;
        return next();
    }
});


/**
 * Unsubscribes user from everything
 */

router.get('/unsubscribed', function (request, response, next) {
    var user, vars;
    logger.log('controller:email:unsubscribed', 'routed');
    if (request.skip) {
        logger.log('controller:email:unsubscribed', 'skipping');
        return next();
    }
    user = request.emailUser || {};
    vars = {
        email: user.email,
        action: 'unsubscribe',
        groups: user.groups
    };
    logger.log('controller:email:unsubscribed', 'rendering');
    return response.render('standalone/email-preferences', vars);
});

router.get('/unsubscribe', function (request, response, next) {
    var user;
    logger.log('controller:email:unsubscribe', 'routed');
    if (request.skip) {
        logger.log('controller:email:unsubscribe', 'skipping');
        return next();
    }
    user = request.emailUser || {};
    logger.log('req:controller:email:unsubscribe', 'Removing all groups from member via Mailchimp using leid: ', user.user.leid);
    return Mailchimp.lists.updateMember(null, {
        leid: user.user.leid
    }, {
        groupings: []
    }, null, true).then(function () {
        logger.log('res:controller:email:unsubscribe', 'Successfully removed all groups from Mailchimp and redirecting...');
        return response.redirect("/email/unsubscribed?ek=" + request.query.ek);
    }, function (err) {
        logger.log('error:controller:email:unsubscribe', 'Error removing all groups from member via Mailchimp', err);
        return response.render('standalone/invalid', {
            message: 'Error unsubscribing. Sorry :('
        });
    });
});

router.get('/removed', function (request, response, next) {
    var user, vars;
    logger.log('controller:email:removed', 'routed');
    if (request.skip) {
        logger.log('controller:email:removed', 'skipping');
        return next();
    }
    user = request.emailUser || {};
    vars = {
        email: user.email,
        action: 'remove',
        groups: user.groups,
        unsubURL: request.unsubURL,
        groupName: request.query.groupName
    };
    logger.log('controller:email:removed', 'rendering');
    return response.render('standalone/email-preferences', vars);
});


/**
 * Removes the user from the specific group
 */

router.get('/remove/:group', function (request, response, next) {
    var emailStruct, mergeVars, remove, user;
    logger.log('controller:email:remove-group', 'routed');
    if (request.skip) {
        logger.log('controller:email:remove-group', 'skipping');
        return next();
    }
    user = request.emailUser || {};

    // convert the short name to the full name
    remove = mcData.fullName[request.params.group];
    emailStruct = {
        leid: request.emailUser.user.leid
    };

    // Recreate the groupings without the one we want to remove
    mergeVars = {
        groupings: mcHelper.removeFromGroupings(user.user.merges.GROUPINGS, [remove])
    };
    logger.log('controller:email:remove-group', 'Removing group from member via Mailchimp');
    return Mailchimp.lists.updateMember(null, emailStruct, mergeVars, null, true).then(function () {
        logger.log('controller:email:remove-group', 'User successfully removed from group via Mailchimp');
        return response.redirect("/email/removed?ek=" + request.query.ek + "&groupName=" + remove);
    }, function (err) {
        return logger.log('error:controller:email:remove-group', 'Error removing user from group using: ', emailStruct, mergeVars);
    });
});


/**
 * Verifies a users account depending on type of signup
 *
 *  Email/Password: User hasn't created a password yet so they will need to create one upon
 *                  visiting this link.
 *                  - Their token will NOT be deleted from Redis upon visiting this.
 *
 *  Social Media:   User is not required to create a password upon account activation so
 *                  the account is activated immediately upon visiting this link.
 *                  - Their token will be deleted from Redis upon visiting this.
 *
 * @param  {IncomingRequest}    request
 * @param  {OutgoingResponse}   response
 * @param  {Function}           next
 */
router.get('/verify_user', function (request, response, next) {
    logger.log('controller:email:verify_user', 'routed');

    let token = request.query.token;

    if (!token) {
        logger.log('error:controller:email:verify_user', 'Invalid email passed in.');
        return response.render('standalone/invalid', {
            message: 'auth.ERROR-INVALID_EMAIL'
        });
    }

    logger.log('req:controller:email:verify_user', 'Confirming user with token');

    return userController.activateProcess(token).catch(function (err) {
        logger.log('error:controller:email:verify_user', err);

        if (err.errType) {
            throw response.redirect(307, '/email/invalid?source=confirm&type=' + err.errType);
        }
    })

    .then(function () {
        logger.log('res:controller:email:verify_user', 'User token confirmed & activated');

        request.frontEnd = request.frontEnd || {};
        request.frontEnd.userActivated = true;

        return next();
    });
});


/**
 * Verifies a users account
 *
 * @param  {IncomingRequest}    request
 * @param  {OutgoingResponse}   response
 * @param  {Function}           next
 */

router.get('/password_reset', function (request, response, next) {
    var token;

    logger.log('controller:email:password_reset', 'routed');
    if (!(token = request.query.token)) {
        logger.log('error:controller:email:verify_user', 'Invalid email passed in.');
        return response.redirect(307, '/email/invalid?type=invalid_link&source=password_reset');
    }

    logger.log('req:controller:email:password_reset', 'Confirming password reset token');
    return Email.passwordResetConfirmed(token).then(function (data) {
        logger.log('res:controller:email:password_reset', 'Password token confirmed');
        request.frontEnd = request.frontEnd || {};
        request.frontEnd.passwordReset = data;
        return next();
    }, function (errMessage) {
        logger.log('error:controller:email:password_reset', errMessage);
        return response.redirect(307, '/email/invalid?type=expired_token&source=password_reset');
    });
});

router.get('/invalid', function (request, response, next) {
    next();
});


/**
 * Updates a users email preferences
 *
 * Requires that the query string provides the users e-mail encrypted
 */

updatePreferences = function (request, response, _vars) {
    var user, vars;
    logger.log('controller:email:updatePreferences', 'helper fn called');
    user = request.emailUser || {};
    vars = {
        action: 'update',
        selfUrl: request.originalUrl,
        email: user.email,
        groups: user.groups,
        unsubURL: request.unsubURL
    };
    if (_vars) {
        _.extend(vars, _vars);
    }
    logger.log('controller:email:updatePreferences', 'rendering');
    return response.render('standalone/email-preferences', vars);
};

router.get('/update', updatePreferences);

router.post('/update', function (request, response, next) {
    var emailStruct, mergeVars;
    logger.log('controller:email:update', 'routed vio `POST`');
    if (request.skip) {
        logger.log('controller:email:update', 'skipping');
        return next();
    }
    emailStruct = {
        leid: request.emailUser.user.leid
    };
    mergeVars = {
        groupings: mcHelper.formPostToGroupings(request.body)
    };
    logger.log('controller:email:update', 'Updating users group via Mailchimp');
    return Mailchimp.lists.updateMember(null, emailStruct, mergeVars, null, true).then(function () {
        logger.log('controller:email:update', 'Succssfully updated users group. Redirecting...');
        return response.redirect(request.originalUrl);
    }, function (err) {
        logger.log('error:controller:email:update', 'Error updating users group via Mailchimp using:', emailStruct, mergeVars, 'Error: ', err);
        return response.render('standalone/invalid', {
            message: 'Error updating your group info... Sorry :('
        });
    });
});


/**
 * Adds a new email user to our list and as a group member
 */

router.get('/update/add/:group', function (request, response, next) {
    var email, group, timeout, user;
    logger.log('controller:email:add-group', 'routed');
    if (request.skip) {
        logger.log('controller:email:add-group', 'skipping');
        return next();
    }
    email = request.query.email;
    group = request.params.group;
    timeout = 4;
    if (!email) {
        logger.log('error:controller:email:add-group', 'missing email parameter. Query:', request.query, 'Group:', group);
        response.render('standalone/popupClose', {
            timeout: timeout * 1000,
            title: 'Email Signup',
            header: 'Missing Email',
            message: "Missing email address parameter.\n\nThis window will close in " + timeout + " seconds."
        });
    }
    user = {
        email: request.query.email,
        mergeVars: {
            groupings: [mcHelper.getEmailGrouping(group)]
        }
    };
    logger.log('controller:email:add-group', 'Adding user to group');
    // Add the user with the double opt-in set to true so that they verify
    return mcHelper.addUser(null, user, true, true).then(function (resp) {
        logger.log('controller:email:add-group', 'User successfully added to Group: ', group);
        return response.render('standalone/popupClose', {
            timeout: timeout * 1000,
            title: 'Email Signup',
            header: 'Email Signup Success!',
            message: "You have been added to the " + group + " group.\n\nThis window will close in " + timeout + " seconds."
        });
    }, function (err) {
        return logger.log('error:controller:email:add-group', 'Error adding user to Group: ', group, ' Using User: ', user, 'Error:', err);
    });
});

exports.router = router;
