'use strict';

const _ = require('lodash-node/modern');
const Q = require('q');
const logger = require('bragi');

const Fetch = GLOBAL.Fetcher;

const Util = Fetch.util('utility');
const Constants = Fetch.config('misc/constants');
const elasticsearch = Fetch.database('elasticsearch');
const esClient = elasticsearch.client;
const esUser = Fetch.util('elasticsearch', 'userHelper');
const DataBus = Fetch.service('DataBus');
const User = Fetch.type('user');
const Notification = Fetch.type('notification');
const db = Notification.db;
const SocketIo = Fetch.connection('socket.io');
const notificationAllImpl = Fetch.impl('notification_all');

const ctrlUtil = Fetch.controller('ctrlUtility');


/**
 * Fetches notifications for a user
 *
 * @param {object} opts
 * @option opts {number}  userID        the only required property
 * @option opts {boolean} withUsers     whether to load the users associated with the notifications
 * @returns {Promise}
 * @returns {object}
 */

exports.fetch = function (opts) {

    if (typeof opts !== 'object') {
        return Q.reject(Constants.RESPONSE['500']);
    }

    let deferred = Q.defer();
    let sent = { sent: null };

    let returnFn = ctrlUtil.deferredSend(deferred, sent);
    let response = {};
    let fromUsersArr = null;

    logger.log('req:ctrl:notification:fetch', 'requesting user\'s notifications');
    Notification.fetch(opts)["catch"](function (err) {
        logger.log('error:ctrl:notification:fetch', 'error receiving users notifications for user:', opts.userID, '\nerror:\n', err);
        throw returnFn(err);
    })

    .then(function (notifications) {
        logger.log('res:ctrl:notification:fetch', 'received users notifications');
        response.notifications = _.pluck(notifications.hits.hits, '_source');

        response.total = notifications.hits.total;

        if (!opts.withUsers || !response.notifications.length) {
            return returnFn(null, response);
        }

        fromUsersArr = _.uniq(_.pluck(response.notifications, 'from')).filter(_.identity);

        // If there are no users to fetch
        if (!fromUsersArr[0]) {
            response.users = [];
            return returnFn(null, response);
        }

        logger.log('req:ctrl:notification:fetch', 'Fetching related users with notifications');
        return esUser.mget(fromUsersArr, {
            _source: ['firstName', 'lastName']
        }).then(elasticsearch.mgetIdMap);
    })["catch"](function (err) {

        if (sent.sent) {
            throw sent;
        }

        logger.log('error:ctrl:notification:fetch', 'error getting related users from userID:', fromUsersArr, '\nerror:\n', err);
        throw returnFn(err);
    })

    // Optional get related users
    .then(function (users) {

        if (sent.sent) {
            return sent;
        }

        logger.log('res:ctrl:notification:fetch', 'received users from related notifications');
        response.users = users;
        return returnFn(null, response);
    })["catch"](function () {});

    return deferred.promise;
};

const SNIPPET_LENGTH = 40;
const updateNoteScript = 'ctx._source.when = when; ctx._source.read = null; if (snippet) {ctx._source.snippet = snippet;}';


/**
 * Notifies user's of something
 *
 * @param  {array<number>} to - Array of userID's
 * @param  {string} type - What type of notification (INTERACTIONS|USER|EVENT|LISTING)
 * @param  {string} ID - ID of the original object (an events eID, messages msgID, etc…)
 * @param  {object} data
 * @param  {number} data.from - the userID/group where the notification originated from
 * @param  {number} data.sourceType  Integer that corresponds solely to the notifications source and their own uses (what type of event notification? event update, new event, deleted, etc…)
 * @param  {number=} data.when - a timestamp in basic_date_time_no_millis format
 * @param  {string=} data.snippet - a text snippet to include
 * @return {Promise}
 */
exports.notify = function (to, type, ID, data) {
    logger.log('ctrl:notification:notify', 'called');
    const whenTime = data.when || Util.formatDateString('basic_date_time_no_millis');
    const source = _getSourceID(type);
    const sourceType = data.sourceType;

    if (!sourceType) {
        logger.log('error:ctrl:notification:notify', 'Missing `data.sourceType` property');
        return Q.reject(Constants.RESPONSE['500']);
    }

    //
    // #! Notification ID's are generated like:
    // #
    // #! "{userID}-{source}_{sourceType}_{sourceID}"
    // #!     Example: "{149}-{1}_{1}_{149_663}" => 149-1_1_149_663
    //
    const noteID = source + '_' + sourceType + '_' + ID;
    const snippet = typeof data.snippet === 'string' && data.snippet.length ? data.snippet.substr(0, SNIPPET_LENGTH) : '';

    logger.log('ctrl:notification:notify', 'mapping notifications to user');
    let bulk = to.map(function (userID) {
        const entry = {
            nID: userID + '-' + noteID,
            _parent: userID,
            _upsert: {
                nID: userID + '-' + noteID,
                source: source,
                sourceType: sourceType,
                sourceID: ID,
                when: whenTime,
                snippet: snippet
            },
            _params: {
                when: whenTime,
                snippet: snippet
            }
        };

        if (data.from) {
            entry._upsert.from = data.from;
        }

        if (snippet) {
            entry._upsert.snippet = snippet;
        }
        SocketIo.emitUser(userID, '/notification', {
            notifications: [entry._upsert]
        });

        return entry;
    });

    bulk = elasticsearch.bulkFormat(bulk, 'update', db.INDEX, db.TYPE, 'nID', true, {
        script: updateNoteScript,
        upsert: true
    });

    // Also mark each user object as having unread notifications
    bulk = bulk.concat(_formatUserNotified(to, true, true));

    logger.log('req:ctrl:notification:notify', 'Notifying users by the bulk');
    return esClient.bulk({ body: bulk })
        .then(function (response) {
            if (response.errors) {
                throw _.pluck(response.items, 'error');
            }

            return response;
        })
        .catch(function (err) {
            logger.log('error:ctrl:notification:notify', 'error running bulk update for query:', bulk, '\nerror:', err);
            throw Constants.RESPONSE['500'];
        });
};

/**
 * Returns the integer key of notification
 *
 * @private
 * @param {string}
 * @returns {number}
 */

const _getSourceID = function (type) {
    var intType;
    if (typeof type !== 'string') {
        throw new Error('notification#_generateID expects the first parameter to be a string corresponding to _notificationTypes.source');
    }
        if (typeof (intType = Notification.notificationTypes[type]) !== 'number') {
        throw new Error('Invalid type:' + type + 'passed to notification#_generateID - doesn\'t correspond to anything in _notificationTypes');
    }
    return intType;
};


/**
 * TODO: move this function from here to the User's domain
 *
 * @private
 */

const _formatUserNotified = function (userID, bulk, unread) {
    var data;
    data = {
        unreadNotification: !!unread
    };
    if (bulk) {
        data = {
            doc: data
        };
        return userID.reduce(function (bulk, uID) {
            bulk.push({
                update: {
                    _index: User.db.INDEX,
                    _type: User.db.TYPE,
                    _id: uID,
                    retryOnConflict: 1
                }
            }, data);
            return bulk;
        }, []);
    }
    return data;
};


/**
 * Updates a user with a new notification parameter
 *
 * @param {number} userID
 * @param {boolean} unread
 * @returns {Promise}
 */

exports.userNotified = function (userID, unread) {
    return User.update(userID, _formatUserNotified(userID, null, unread));
};


/**
 * Marks a notification as read an emits it
 *
 * @param {number} userID
 * @param {boolean} unread
 * @returns {Promise}
 */

exports.markRead = function (userID, nIDs, mark, value) {
    var docIDs, noteKey, noteKeys;
    // we're only matching the first notification right now
    if (!(noteKey = exports.unmapNID(nIDs[0]))) {
        logger.log('error:ctrl:notification:mark', 'Invalid first nID');
        return Q.reject(Constants.RESPONSE['400']);
    }

    // Unmap all of them and append the first one
    noteKeys = nIDs.slice(1).map(exports.unmapNID).concat([noteKey]);
    docIDs = noteKeys.map(exports.buildDocID.bind(null, userID));

    // Get the matched "{source}_{sourceType}" from the notification ID
    DataBus.emit("/notification/mark/" + noteKey.source + "_" + noteKey.sourceType, [
        noteKey, {
            userID: userID,
            nIDs: nIDs,
            noteKeys: noteKeys,
            mark: mark,
            value: value
        }
    ]);

    return Notification.bulkUpdate(userID, docIDs, mark, value);
};


/**
 * Breaks a string notification ID into an object with data
 *
 * @param {string} nID                      the full notification ID
 * @returns {object}
 * @option <return> {number} fromID         Who this notification originated from
 * @option <return> {number} source         where this came from (message, user, event, etc…)
 * @option <return> {number} sourceType     type of source
 * @option <return> {string} sourceID       ID of the source object
 */

exports.unmapNID = function (nID) {
    var matches = nID.match(/^(\d+)-(\d+)_(\d+)_(.+)$/);

    if (!Array.isArray(matches) || matches.length !== 5) {
        return null;
    }

    return {
        fromID: parseInt(matches[1], 10),
        source: parseInt(matches[2], 10),
        sourceType: parseInt(matches[3], 10),
        sourceID: matches[4]
    };
};

exports.buildDocID = function (userID, keys) {
    return userID + '-' + keys.source + '_' + keys.sourceType + '_' + keys.sourceID;
};

// Trigger setting the notification types
_.defer(function () {
    notificationAllImpl.registerNotificationTypes();
});
