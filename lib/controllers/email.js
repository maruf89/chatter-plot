'use strict';

const _ = require('lodash-node/modern'),
    Q = require('q'),
    logger = require('bragi'),

    Fetch = GLOBAL.Fetcher,

    config = Fetch.config('config', true, 'js'),

    Constants = Fetch.config('misc/constants'),
    EmailNotification = Fetch.service('EmailNotification'),

    notifier = EmailNotification.notifier;

class ContactEmailFields extends EmailNotification.MandrillFields {
    constructor() {
        super();
    }

    FROM_EMAIL(email) {
        return {
            name: 'FROM_EMAIL',
            content: email.email
        }
    }

    USER_AGENT(email) {
        return {
            name: 'USER_AGENT',
            content: email.userAgent
        }
    }
}

/**
 * @description sends out a message to 'The Team' as well as an autoresponse to the sender
 * @param {object} messageData
 * @param {string} messageData.type - a value corresponding to `mandrillKey.js#MESSAGE`
 * @returns {Promise}
 */
exports.contactMessage = function (messageData) {
    logger.log('controller:email:contactMessage', 'called - building message variables');
    const msgType = messageData.type.toLowerCase();

    messageData.type = 'MESSAGE';
    messageData.emailKey = msgType.toUpperCase();
    messageData.users = [{
        email: config.emails[msgType],
        firstName: 'The Team',
        lastName: ''
    }];

    messageData.globalVars = [
        'CATEGORY',
        'SUBJECT',
        'NAME',
        'DETAILS',
        'FROM_EMAIL',
        'USER_AGENT',
        'LOCALE',
    ];

    messageData.userVars = ['env'];

    messageData.mandrillOptions = {
        from_email: messageData.email,
        from_name: messageData.name,
        track_clicks: false
    };

    let autoResponseEmail = {
        type: 'MESSAGE',
        emailKey: 'CONTACT_AUTORESPONDER',
        users: [{
            email: messageData.email,
            name: messageData.name,
            settings: {
                locale: messageData.siteLocale
            }
        }]
    };

    logger.log('req:controller:email:contactMessage', 'sending email + autoresponder');
    return Q.all([
        notifier.emailNotifyUsers(messageData, new ContactEmailFields()),
        notifier.emailNotifyUsers(autoResponseEmail)
    ]).catch(function (err) {
        logger.log('error:controller:email:contactMessage', 'error emailing us\nerror:\n', err);

        throw Constants.RESPONSE['500'];
    });
};
