'use strict';
var _getFile, onError, parseEvents, parseSingle, queueVenueSave;

const fs = require('fs'),
    _ = require('lodash-node/modern'),
    Q = require('q'),
    logger = require('bragi'),
    moment = require('moment'),

    Fetch = GLOBAL.Fetcher,

    venueController = Fetch.controller('venue'),
    Event = Fetch.type('event'),
    Flow = Fetch.service('flow').Flow,
    flow = new Flow(),
    geocoder = Fetch.util('geocoderPromise'),
    commonVenue = Fetch.common('util', 'venue', 'index'),
    languageHelper = Fetch.util('elasticsearch', 'languageHelper'),
    Util = Fetch.util('utility');

_getFile = function (filename) {
    var deferred;
    logger.log('req:adminEvent:bulkAdd:_getFile', 'Getting/Checking file');
    deferred = Q.defer();

    // Check that the file exists before we require it 
    fs.exists(filename, function (exists) {
        var data, e;
        if (!exists) {
            logger.log('error:adminEvent:bulkAdd:_getFile',
                'No file exists to require');
            return deferred.reject(new Error("Trying to read file " +
                filename + " which does not exist in " +
                __filename));
        }
        try {
            data = require(filename);
        } catch (_error) {
            e = _error;
            return deferred.reject(new Error(e,
                "Error opening JSON schools file " + filename +
                " in " + __filename));
        }
        logger.log('res:adminEvent:bulkAdd:_getFile',
            'File exists, returning JSON contents');
        return deferred.resolve(data);
    });
    return deferred.promise;
};


/**
 * Iterates over a JSON file and extracts/formats only the fields we desire
 *
 * @param  {object} data
 * @return {Promise}
 */
parseEvents = function (data) {
    var length;
    console.log('loading up all', data.length, 'listings');
    length = data.length;
    return Q.allSettled(data.map(function (event, index) {
        var deferred;
        deferred = Q.defer();
        setTimeout(function () {
            logger.log('req:adminEvent:bulkAdd:parseEvent',
                'Processing event:', index + 1, 'of',
                length);
            return parseSingle(event)
                .then(deferred.resolve, deferred.reject);
        }, (index + 1) * 2000);
        return deferred.promise;
    }));
};

parseSingle = function (event) {
    var calls, languages, location, next;
    calls = [];
    if (Array.isArray(languages = event.process.languages)) {
        logger.log('req:adminEvent:bulkAdd:parseSingle', 'parsing languages');
        calls.push(languageHelper.getIdForLanguages(languages)
            .then(function (langs) {
                if (!event) {
                    logger.log('error:WTF!', 'no event');
                }
                event.languages = languageHelper.respToLangObj(langs,
                    'idArray');
                logger.log('res:adminEvent:bulkAdd:parseSingle',
                    'Received lang ids', event.languages);
                return Q.resolve();
            }));
    } else {
        event.languages = [0];
    }
    if (location = event.process.location) {
        logger.log('req:adminEvent:bulkAdd:parseSingle', 'querying for location');
        calls.push(geocoder.queryPlace(location)
            .then(function (res) {
                var venue;
                logger.log('req:adminEvent:bulkAdd:parseSingle',
                    'queried place, now preparing');
                if (res.results.length) {
                    venue = commonVenue.scrapePlace(res.results[0], 'google', 2);
                    return queueVenueSave(venue);
                }
                return Q.when();
            }, onError)
            .then(function (venue) {
                var vID;
                logger.log('res:adminEvent:bulkAdd:parseSingle',
                    'got venue');
                vID = parseInt(Object.keys(venue)[0], 10);
                venue = venue[vID];
                event.venue = {
                    vID: vID,
                    coords: venue.coords
                };
                return venueController.mget([vID], {
                        _source: ['tz']
                    })
                    .then(function (res) {
                        var tz;
                        tz = res.docs[0]._source.tz;
                        if (event.when) {
                            event.when = Util.offsetTimeZone(moment(
                                    event.when,
                                    'YYYYMMDDTHHmmss'), tz)
                                .format('YYYYMMDDTHHmmssZ');
                        }
                        if (event.until) {
                            event.until = Util.offsetTimeZone(moment(
                                    event.until,
                                    'YYYYMMDDTHHmmss'), tz)
                                .format('YYYYMMDDTHHmmssZ');
                        }
                        return Q.resolve();
                    }, onError);
            }, onError));
    }
    event.name = event.name.replace(/(\r\n|\n|\r)/gm, '');
    event.goingList = [];
    event.published = true;
    event.levels = [1, 2, 3, 4, 5, 6];
    event.creator = 0;
    event.created = Util.formatDateString('basic_date_time_no_millis');
    event.priceString = event.priceString || 'viewReg';
    delete event.process;
    next = calls.length ? Q.allSettled(calls) : Q.when();
    return next.then(function () {
        if (event.venue) {
            return Event.create(event, {
                type: 'listing'
            });
        }
        return Q.when();
    });
};

queueVenueSave = (function () {
    var delay, queue, running, saveVenue;
    delay = 100;
    queue = [];
    running = false;
    saveVenue = function () {
        var next;
        if (running || !queue.length) {
            return running = false;
        }
        next = queue.shift();
        logger.log('req:adminEvent:bulkAdd:queueVenueSave-saveVenue',
            'saving venue');
        return venueController.safeSave([next.venue])
            .then(function (venue) {
                logger.log(
                    'res:adminEvent:bulkAdd:queueVenueSave-saveVenue',
                    'saved/fetched venue');
                next.deferred.resolve(venue);
                return setTimeout(saveVenue, delay);
            }, next.deferred.reject);
    };
    return function (venue) {
        var deferred;
        deferred = Q.defer();
        logger.log('req:adminEvent:bulkAdd:queueVenueSave',
            'called: queueing event');
        queue.push({
            deferred: deferred,
            venue: venue
        });
        if (!running) {
            saveVenue();
        }
        return deferred.promise;
    };
})();


/**
 * Expects a JSON document to be uploaded via Flow.
 * Sends the request to flow to assemble the file, and once the file is downloaded
 * sends it's contents to be parsed and added to the database
 *
 * @param  {IncomingRequest}  req
 * @param  {OutgoingResponse} res
 */

exports.uploadEvents = function (req, res) {
    logger.log('adminEvent:bulkAdd:uploadEvents', 'called');
    return flow.post(req, res, function (response) {
        var errorCB;
        errorCB = function (error) {
            logger.log('error:bulkAdd', error);
            return res.json({
                success: false,
                file: response.filename,
                error: error
            });
        };
        logger.log('adminEvent:bulkAdd:uploadEvents', 'response:',
            response);
        switch (response.response) {
            case 500:
                return res.json(response);
            case 200:
                return _getFile(response.filename)
                    .then(parseEvents, errorCB)
                    .then(function () {
                        res.json({
                            success: true,
                            file: response.filename
                        });

                        // We don't need the file after it's data has been added to the database 
                        return fs.unlink(response.filename);
                    }, errorCB);
        }
    });
};

onError = function (err) {
    return logger.log('error:bulkAdd', err);
};
