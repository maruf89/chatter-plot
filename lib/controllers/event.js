/**
 * @module bControllerEvent
 */

'use strict';

const _ = require('lodash-node/modern');
const logger = require('bragi');
const Q = require('q');

const Fetch = GLOBAL.Fetcher;

const Event = Fetch.type('event');
const Venue = Fetch.type('venue');

const eventUserImpl = Fetch.impl('event_user');
const eventNotificationImpl = Fetch.impl('event_notification');
const eventVenueImpl = Fetch.impl('event_venue');
const venueImpl = Fetch.impl('venue');

const elasticsearch = Fetch.database('elasticsearch');
const esClient = elasticsearch.client;
const utilSearchHelper = Fetch.util('elasticsearch', 'helper');
const utilEventSearch = Fetch.util('elasticsearch', 'event', 'index');
const utillanguageHelper = Fetch.util('elasticsearch', 'languageHelper');
const esUser = Fetch.util('elasticsearch', 'userHelper');
const Util = Fetch.util('utility');
const ctrlUtil = Fetch.controller('ctrlUtility');

const Constants = Fetch.config('misc/constants');
const DataBus = Fetch.service('DataBus');

const _joinAccessors = {
    venue: function (obj) {
        return parseInt(obj.vID, 10);
    },
    userID: function (obj) {
        return parseInt(obj.userID, 10);
    }
};

var _send = function (sent, err, response) {
    if (sent.sent) {
        return true;
    }
    return sent.sent = this[err ? 'reject' : 'resolve'](err || response) || 1;
};

// Utility method to add pages to the 'events' site map
const sitemapAdd = function (URL, opts) {
    return DataBus.emit('/sitemap/add', ['events', URL, opts]);
};

const controller = exports;

logger.log('init:controller:event', 'loaded');

exports.allowedUserEmailTypes = Event.allowedUserEmailTypes;

/**
 * @description Gets an event
 * @name bControllerEvent#get
 * @param {(object|array<object>)} data - if is an array then assumes it has the eID/type for each one
 * @param data {array}     eIDs    event IDs
 * @param data {boolean}   venues  whether to load the venues as well
 * @param data {boolean}   host    whether to load the host's profile info as well
 * @param data {Array=}    _source array of fields to load from the event
 * @param data {String=}   type    the type of event's they are event|listing default=event
 *
 */
exports.get = function (data) {
    var len, method,
        eIDs = data.eIDs,
        withVenues = data.withVenues;

    if (withVenues && data._source) {
        data._source.push('venue.vID');
    }

    if (Array.isArray(data.evTypes)) {
        let evTypes = utilEventSearch.parseMGetRequest(data.evTypes);
        return Event.mgetDocs(evTypes, data);
    }

    if (!Array.isArray(eIDs)) {
        logger.log('error:controller:event:get', 'expected to receive an array for the eIDs parameter');
        return Q.reject(Constants.RESPONSE['400']);
    }

    if (!(len = eIDs.length)) {
        return Q.resolve([]);
    }

    method = len === 1 ? 'get' : 'mget';

    logger.log('req:controller:event:get', 'Getting events');
    return Event[method](eIDs, data);
};

/**
 * @descriptionFormats both `get` and `mget` search results. Appends an indexType and returns the data
 * @name bControllerEvent#formatGetResults
 * @param {object} results
 * @returns {array<object>}
 */
exports.formatGetResults = function (results) {
    if (results.hasOwnProperty('_source')) {
        results._source.indexType = results._type;
        return [results._source];
    } else if (Array.isArray(results.docs)) {
        return _.map(results.docs, function (result) {
            result._source.indexType = result._type;
            return result._source;
        });
    }

    return [];
};

exports.formatGetSingle = function (results) {
    return controller.formatGetResults(results)[0];
};

/**
 * @description A function meant to be composed to accept an elasticsearch response
 * which extracts the event results from it and passes it to the function to extend
 * the events with venues
 * @name bControllerEvent#getResult2VenueExtend
 * @param {object} opts - search options
 * @param {array<object>} response
 * @returns {Promise<object>}
 */
exports.getResult2VenueExtend = function (opts, events) {
    if (!Array.isArray(events) || !events[0]) {
        return Q.reject(events);
    }

    return exports.extendEventWithVenuesHost(events, opts);
};

/**
 * @description Searches for events!
 * @name bControllerEvent#search
 * @param {object} opts - options include:
 * @param {number=} opts.from - starting from where
 * @param {number=} opts.size - number of results to get
 * @param {array<string>=} opts.filterTypes
 * @param {array<string>=} opts.queryTypes
 * @param {array<string>=} opts.sortTypes
 * @param {boolean} opts.markerSearch  Whether to search for map markers as well
 * @param {*} opts.* - Depends on the filterTypes queries data
 * @return {object} An Elasticserach results object
 */
exports.search = function (opts) {
    var method, query, source;
    logger.log('controller:event:search', 'called');

    var options = {
            from: 0,
            size: 10,
            opts: {},
            filterTypes: ['current', 'published'],
            sort: 'when',
            type: '', // search both events & listings by default
        },
        
        requiredFields = [
            'creator',
            'when',
            'duplicate',
            //'description', // Sending the description of each event would bloat this up 5x
            'until',
            'eID',
            'goingList',
            'languages',
            'levels',
            'organizers',
            'name',
            'hasPhoto',
            'venue',
            'locationDisplayName',
            'waitList',
            'paymentInfo',
            'priceString',
            'published',
            'maxGoing',
            'registrationURL',
        ];

    if (typeof opts === 'object') {
        options = _.extend(options, opts);
    }

    // If we have some filter types that require the userID, adds the correct data to opts
    utilEventSearch.util.fillQueryData(options);

    query = utilSearchHelper.buildSearch('event', options.queryTypes, options.filterTypes, null, options);

    if (opts.markerSearch) {
        source = {
            index: Event.db.INDEX,
            type: options.type,
        };
        
        method = 'msearch';
        
        query = {
            body: [
                source,
                _.extend(_.cloneDeep(query), {
                    from: options.from,
                    size: options.size,
                    sort: options.sort,
                    _source: requiredFields,
                }),
                source,
                _.extend(query, {
                    size: options.markerSize || 251,
                    _source: ['venue.coords', 'venue.rsvpOnlyRadius']
                })
            ]
        };
    } else {
        method = 'search';
        query = {
            index: Event.db.INDEX,
            type: options.type,
            from: options.from,
            size: options.size,
            body: query,
            sort: options.sort,
            _source: requiredFields,
        };
    }
    if (opts._source) {
        query._source = opts._source;
    }

    logger.log('req:controller:event:search', 'Searching for events in Elasticsearch');
    return Event[method](query)["catch"](function (err) {
        logger.log('error:controller:event:search', 'Error searching for events using method:', method, '\nopts:', opts, '\nquery:', query, '\nerror:\n', err);
        throw Constants.RESPONSE['500'];
    });
};

/**
 * @description Takes an elasticsearch search result and depending on the type of response
 * it received formats it
 * @name #formatSearchResults
 * @param {object} results
 * @returns {{events: Array<object>}}
 */
exports.formatSearchResults = function (results) {
    var events,
        response = {
            events: [],
            total: 0,
        };

    logger.log('controller:event:formatSearchResults', 'called');
    if (typeof results !== 'object') {
        return Q.resolve(response);
    }

    if (Array.isArray(results.responses)) {
        response.markers = results.responses[1].hits.hits;
        events = results.responses[0];
    } else {
        events = results;
    }

    response.total = events.hits.total;

    // Return only the events along with a property or it's index type
    response.events = _.map(events.hits.hits, function (result) {
        result._source.indexType = result._type;
        return result._source;
    });

    return response;
};

/**
 * @description A function meant to be composed to accept an elasticsearch response
 * which extracts the event results from it and passes it to the function to extend
 * the events with venues
 * @name bControllerEvent#searchResult2VenueExtend
 * @param {object} opts - search options
 * @param {object} response
 * @param {array<object>} response.events - events object to be modified
 * @returns {Promise<object>}
 */
exports.searchResult2VenueExtend = function (opts, response) {
    if (typeof response !== 'object' || !Array.isArray(response.events)) {
        return Q.reject();
    }

    // if we don't have a first entry assume everything's a dud and quit
    if (!response.events[0]) {
        return Q.resolve(response);
    }

    return exports.extendEventWithVenuesHost(response.events, opts).then(function () {
        return response;
    });
};

/**
 * @description Gets venues associated with the desired events
 * @name #extendEventWithVenuesHost
 * @param {array<object>=} events - collection of event objects to get venues for
 * @param {object=} opts - options for fetching events or other
 * @param {boolean} opts.host - Whether to get the event host data as well
 * @return {Promise<array>} array of events with filled in `venue` field (`host` field for host)
 */
exports.extendEventWithVenuesHost = function (events, opts) {
    logger.log('controller:event:extendEventWithVenuesHost', 'called');

    opts = opts || {};

    if (!Array.isArray(events) || !events[0]) {
        return Q.resolve(events);
    }

    const withHost = opts.host && !!events[0].creator;
    let hostsIndex;

    // get all of the Venues
    let calls = _.map(events, function (event) {
        return venueImpl.getVenue(event.venue, { fromSource: true }).then(function (source) {
            event.venueDetails = venueImpl.formatPlace(source);
            return event;
        });
    });

    // If we're fetching hosts too then grab that from elasticsearch as well
    if (withHost) {
        logger.log('req:controller:event:extendEventWithVenuesHost', 'Getting hosts');
        hostsIndex = calls.length;
        calls.push(eventUserImpl.getEventHosts(events));

    }

    return Q.all(calls)
        .then(function (arg) {
            logger.log('res:controller:event:extendEventWithVenuesHost', 'Got venues and maybe hosts');

            var users = arg[hostsIndex],
                hosts;

            // If the value is not undefined then extend each event with a `host` {object} property
            if (Array.isArray(users)) {
                users = _.filter(users, _.identity);

                hosts = [];
                _.each(events, function (event) {
                    event.host = {
                        userID: event.creator
                    };

                    // push a reference to new property so that it can be extended
                    return hosts.push(event.host);
                });
                hosts = Util.objectJoins(hosts, _joinAccessors.userID, users, _joinAccessors.userID);
            }

            return events;
        }).catch(function (err) {
            logger.log('controller:event:extendEventWithVenuesHost',
                'error fetching venue/host',
                '\nerror:\n', err
            );
            throw 500;
        });
};

/**
 * @description Checks that a given user can modify an event
 * TODO: build the adminReq part of this method which checks the permissions for the current user and see
 * if he can complete the action
 * @name bControllerEvent#userCanEdit
 * @param {number} eID
 * @param {string} type - 'event' or 'listing'
 * @param {number} userID
 * @param {boolean=} adminReq - if true will check the user's permissions not whether he's the event creator
 * @returns {Promise} will reject if user does not have permissions to edit
 */
exports.userCanEdit = function (eID, type, userID, adminReq) {
    if (adminReq) {
        // TODO:
        //
        return logger.log('error:controller:event:userCanEdit', 'Not built, see TODO:');
    }

    return Event.get(eID, { type: type, _source: 'creator' }).then(controller.formatGetResults)

    .then(function (events) {
        if (!events[0]) {
            throw 404;
        }

        if (parseInt(events[0].creator, 10) !== parseInt(userID, 10)) {
            throw 403;
        }

        return 200;
    });
};

/**
 * @description publishes an event
 * @name bControllerEvent#publish
 * @param {object} eventData
 * @param {number} eventData.eID
 * @param {string} eventData.type
 * @param {number=} eventData.callerID - if passed will first check that this user has permissions to edit the event
 * @returns {Promise}
 */
exports.publish = function (eventData) {
    var promise;

    // If callerID is passed then we have a user to check against
    if (typeof eventData.callerID === 'number') {
        promise = controller.userCanEdit(eventData.eID, eventData.type, eventData.callerID);
    } else {
        promise = Q.when();
    }

    return promise.then(function () {
        return Event.update(eventData.eID, {
            published: true
        }, eventData);
    }).catch(function (err) {
        logger.log('error:controller:event:publish', 'Error publishing event', err);
        throw err;
    })

    .then(function () {
        // Add the site to the sitemap after successfully being published
        sitemapAdd(eventData.type + "s/" + eventData.eID, { priority: .7, });

        return 200;
    });
};

/**
 * @description Notifies all relevant users of a new event in 3 stages:
 *                      1: get the event
 *                      2: get relevant users + convert language ID's to language names
 *                      3: notify the users via e-mail + internal
 * @param {object} eventData
 * @param {number} eventData.eID
 * @param {string} eventData.type - listing or event
 * @returns {Promise}
 */
exports.newEventNotify = function (eventData) {
    const deferred = Q.defer();
    const sent = { sent: null };
    const returnFn = ctrlUtil.deferredSend(deferred, sent);
    let event;

    // Get event
    return Event.get(eventData.eID, eventData).then(controller.formatGetResults)

    // Get relevant users
    .then(function (events) {
        event = events[0];
        logger.log('req:controller:event:newEventNotify', 'searching for all users that are relevant to this event');

        // Get relevant users & the language names
        return Q.all([
            eventUserImpl.getRelevantEventUsers(event),
            utillanguageHelper.ID2Language(event.languages)
                              .then(_.partialRight(utillanguageHelper.respToLangObj, 'langArray'))
        ]);
    })

    // Notify
    .then(function (data) {
        var users = data[0],
            languages = data[1];

        if (!users[0]) {
            logger.log('res:controller:event:newEventNotify', 'Either error or no relevant users');
            return returnFn(null, users);
        }

        const INDEX_TYPE = eventData.type.toUpperCase();
        const sourceType = 'NEW';

        const notifyData = {
            event: event,
            users: users,
            languages: languages,
            type: INDEX_TYPE,
            emailKey: sourceType,
            objectID: event.eID,
            notificationType: INDEX_TYPE,
            notificationSourceType: Event.notificationTypes[sourceType],
        };

        return eventNotificationImpl.notify(notifyData);
    });
};

/**
 * @description Updates an event to include the user AND updates a user to include the event
 *
 * @param  {number} userID
 * @param  {number} eID      events id
 * @return {Promise}
 */
exports.userGoing = function (userID, eID, options) {
    options = options || {};

    var sent = null,
        updateQuery = null,
        waitList = null,

        // Used for the success response
        eventName = null;

    options._source = ['goingList', 'waitList', 'maxGoing', 'name'];

    logger.log('req:service:event:userGoing', 'Checking to see user is not already attending');
    return Event.get(eID, options).then(controller.formatGetResults)

    .then(function (Events) {
        const eventData = Events[0];

        // Check if user is already attending
        if (~eventData.goingList.indexOf(userID) ||
            (eventData.maxGoing &&
             Array.isArray(eventData.waitList) &&
             ~eventData.waitList.indexOf(userID)
            )
        ) {
            logger.log('warning:service:event:userGoing', 'User is already attending this event');
            throw sent = {
                response: 600,
                type: 'events.WARNING-ALREADY_ATTENDING'
            };
        }

        eventName = eventData.name;
        waitList = eventData.maxGoing && eventData.goingList.length >= eventData.maxGoing;

        updateQuery = utilEventSearch.userGoingQuery(true, {
            eID: eID,
            insert: userID,
            type: eventData.indexType,
            waitList: waitList
        });

        logger.log('req:service:event:userGoing', 'Updating Event: adding user to `goingList` + adding to user`s events');
        return Q.all([
            esClient.update(updateQuery),

            // TODO: move this out of here and into a separate function
            eventUserImpl.addEventToUser(userID, eID, waitList)
        ]);
    }, function (err) {
        if (sent) {
            throw sent;
        }

        logger.log('error:service:event:userGoing', 'Error preparing user:', userID, 'for event:', eID, '\nerror:\n', err);
        throw sent = Constants.RESPONSE['500'];
    })

    // User updated
    .then(function () {
        return {
            response: 200,
            type: 'events.RSVP_SUCCESS',
            vars: {
                eventName: eventName,
                waitList: waitList
            }
        };
    }, function (err) {
        if (sent) {
            throw sent;
        }
        logger.log('error:service:event:userGoing', 'Error adding event to user:', userID, 'for event:', eID, 'and adding user to event\nerror:\n', err);
        throw sent = Constants.RESPONSE['500'];
    });
};

/**
 * @description bare bones event attendance removal
 *
 * @param {object} eventData
 * @param {object} eventData.eID - eventID
 * @param {object} eventData.indexType - required if `type` is not passed as third parameter
 * @param {number} userID
 * @param {string=} type
 * @returns {*}
 */
exports.unattendUser = function (eventData, userID, type) {
    logger.log('req:controller:event:unattendUser', 'removing user from event');
    return esClient.update(
        utilEventSearch.userGoingQuery(false, {
            eID: eventData.eID,
            userID: userID,
            type: type || eventData.indexType,
        })
    )
        .catch(function (err) {
            logger.log('error:controller:event:unattendUser',
                'Error unatending user from event using event:', eventData,
                'userID:', userID, '\nerror:\n', err
            );
            throw 500;
        });
};

/**
 * Updates an event to remove the user from the event
 *
 * @param  {number} userID
 * @param  {number} eID      events id
 * @param  {object} options
 * @param  {string} options.type - the only required field
 * @return {Promise}
 * @deprecated in favor of unattendUser - which does not contain the TODO implementation
 */
exports.userNotGoing = function (userID, eID, options) {
    if (!options.type) {
        logger.log('error:controller:event:userNotGoing', 'requires options field to contain the `type` field')
        return Q.reject(Constants.RESPONSE['500'])
    }

    logger.log('controller:event:userNotGoing', 'called');

    var sent = null,
        calls = [],
        vars = {};// pass on response

    options._source = ['name', 'goingList', 'waitList'];

    logger.log('req:controller:event:userNotGoing', 'Fetching event');
    return Event.get(eID, options).then(controller.formatGetSingle)

    .then(function (eventData) {
        vars.eventName = eventData.name;
        vars.waitList = Array.isArray(eventData.waitList) && eventData.waitList.length;
        vars.fromGoing = !!~eventData.goingList.indexOf(userID);

        logger.log('req:controller:event:userNotGoing', 'removing user from event');
        calls.push(esClient.update(utilEventSearch.userGoingQuery(false, {
            eID: eID,
            userID: userID,
            type: options.type
        })));

        // TODO: move this out of here and into a separate function
        logger.log('req:controller:event:userNotGoing', 'removing event from user');
        calls.push(eventUserImpl.removeEventFromUser(userID, eID));

        return Q.all(calls);
    }).catch(function (err) {
        if (sent) {
            throw sent;
        }
        logger.log('error:controller:event:userNotGoing', 'Error fetching event', eID, '\nerror:\n', err);
        throw sent = (function () {
            throw Constants.RESPONSE['500'];
        })();
    })

    // User updated
    .then(function () {
        logger.log('res:controller:event:userNotGoing', 'Successfully removed user from event & vice-versa');
        return {
            response: 200,
            type: 'events.UNRSVP_SUCCESS',
            vars: vars
        };
    }).catch(function (err) {
        if (sent) {
            throw sent;
        }

        logger.log('error:controller:event:userNotGoing', 'Error removing event from user & vice-versa:', userID, 'for event:', eID, '\nerror:', err);
        throw sent = (function () {
            throw Constants.RESPONSE['500'];
        })();
    });
};

/**
 * @description creates a new event/listing in the database - additionally adds the creator to the goingList
 * @param {object} eventData - the data to store
 * @param {string} indexType -
 * @param creator
 * @returns {IPromise<TResult>|*|Promise.<T>}
 */
exports.createEventProcess = function (eventData, indexType, creator) {
    let deferred = Q.defer();
    let sent = { sent: null };
    let returnFn = ctrlUtil.deferredSend(deferred, sent);

    creator = parseInt(creator, 10);

    // Add self to own event
    eventData.goingList = [creator];
    eventData.creator = creator;
    eventData.published = false;

    Event.create(eventData, { type: indexType })
        .catch(function (err) {
            logger.log('error:controller:event:create',
                'Error saving the event to elasticsearch using:',
                eventData, 'error:\n\n', err
            );
            throw returnFn(500);
        })

        // Event Saved
        .then(function (savedEvent) {
            eventData = savedEvent;

            logger.log('req:s-socket:event:create',
                'Saved event to elasticsearch - Adding event to user\'s doc');

            return eventUserImpl.addEventToUser(creator, eventData[Event.eventID]);
        })
        .catch(function (err) {
            if (!sent.sent) {
                logger.log('error:controller:event:create',
                    'Error adding user to their own created event',
                    eventData, 'error:\n\n', err);
            }

            throw returnFn(500);
        })

        // Event added to users going object
        .then(function() {
            logger.log('req:s-socket:event:create', 'Added event to user');
            return returnFn(null, eventData);
        });

    return deferred.promise;
};

/**
 * @description Deletes an event, duh
 *
 * @param {object} data
 * @param {number} data.eID - event ID
 * @param {string} data.type - listing|event
 * @param {number} data.userID - the id of the user doing the action
 * @param {boolean} data.notify - whether to notify recipients by notification/email
 * @param {string=} data.notifyMessage - message to send along with the e-mail
 */
exports.deleteEventProcess = function (data) {
    let sent;

    return controller.get({
        eIDs: [data.eID],
        type: data.type
    })
        .then(controller.formatGetResults)
        .then(function (response) {
            const event = response[0];

            // If the person trying to do the action isn't the creator, throw an error
            // TODO: will eventually need to allow admin users to delete events
            if (event.creator !== data.userID) {
                logger.log('controller:event:deleteEventProcess', 'Only the creator of the event can delete it');
                throw sent = { type: 'updates.ERROR-INVALID_PERMISSIONS', response: 400 };
            }

            logger.log('req:controller:event:deleteEventProcess', 'deleting event');
            const calls = [Event["delete"](data.eID, { type: data.type })];
            const goingLen = event.goingList.length;

            // used in removeFromUsers - Stays false until we iterate over the goingList
            let waitList = false;

            /**
             * @description removes the event from the users own profile
             * @param userID
             */
            const removeFromUsers = function (userID) {
                calls.push(eventUserImpl.removeEventFromUser(userID, event.eID, waitList));
            };

            // remove the users going
            _.each(event.goingList, removeFromUsers);

            // flip the switch and remove from the waitlist if there is any
            waitList = true;
            event.waitList && _.each(event.waitList, removeFromUsers);

            // include ex-hosts message if it exists
            if (data.notify &&
                (event.goingList.length > 1 ||
                 (goingLen === 1 && event.goingList[0] !== data.userID))
            ) {
                logger.log('req:controller:event:deleteEventProcess', 'notifying event attendees');
                calls.push(
                    controller.eventNotify(event, {
                        type: data.type.toUpperCase(),
                        emailKey: 'DELETED',
                        notifyMessage: data.notifyMessage,
                        excludeUsers: [data.userID],
                    })
                );
            }

            return Q.all(calls);
        })
        .catch(function (err) {
            if (sent) { throw err; }

            logger.log('error:controller:event:deleteEventProcess',
                'error deleting or notifying event using:', data,
                '\nerror\n', err
            );

            throw Constants.RESPONSE['500'];
        })
};

/**
 * @description wrapper function for #eventNotify that also fetches the event
 *
 * @param {number} eID
 * @param {object} templateData
 * @returns {Promise}
 */
exports.eventGetNotify = function (eID, type, templateData) {
    return Event.get(eID, { type: type }).then(controller.formatGetSingle)
        .then(function (event) {
            return controller.eventNotify(event, templateData);
        });
};

/**
 * @description Notifies users of an event
 * @param {object} event - full event object (without venueDetails)
 * @param {object} templateData
 * @param {string} templateData.type - event type (event|listing)
 * @param {string=} templateData.emailKey - mandrill.js key
 * @param {array<number>=} templateData.excludeUsers - array of user IDs to exclude
 * @param {boolean=} templateData.excludeCreator - if true will add creator to the excludeUsers array
 * @returns {*}
 */
exports.eventNotify = function (event, templateData) {
    // All of the fields needed to notify a user by email + notification
    const notifyData = {
        type: templateData.type,
        emailKey: templateData.emailKey,
        notificationType: templateData.type,
        notificationSourceType: Event.notificationTypes[templateData.emailKey],
        event: event,
        users: null,
        venueDetails: null,
    };

    if (templateData.excludeCreator) {
        (templateData.excludeUsers = templateData.excludeUsers || []).push(event.creator);
    }

    // Add any additional variables that the notification/email might need
    if (templateData.data) {
        _.extend(notifyData, templateData.data);
    }

    const _source = [
        'userID',
        'firstName',
        'email',
        'settings.locale',
    ];

    logger.log('req:controller:event:eventNotify', 'getting event details/users');
    return Q.all([
        eventVenueImpl.getEventVenues([event], true),
        eventUserImpl.getEventUsers(event, { exclude: templateData.excludeUsers, _source: _source }),
    ])
        .then(function (resultsArr) {
            notifyData.venueDetails = eventVenueImpl.formatPlace(resultsArr[0][0], 'google');
            notifyData.users = resultsArr[1];

            return eventNotificationImpl.notify(notifyData);
        })
        .catch(function (err) {
            logger.log('error:controller:event:eventNotify',
                'Error fetching/trying to notify event users using event:', event,
                'notifyData:', notifyData,
                '\nerror:\n', err
            );
            throw 500;
        })
};
