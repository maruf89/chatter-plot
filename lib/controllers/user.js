/**
 * @module bControllerUser
 */

'use strict';

const _ = require('lodash-node/modern');
const Q = require('q');
const logger = require('bragi');

const Fetch = GLOBAL.Fetcher;

const Constants = Fetch.config('misc/constants');
const sqlUser = Fetch.util('mysql', 'userHelper');
const jwtHelper = Fetch.util('jwtHelper');
const Util = Fetch.util('utility');
const User = Fetch.type('user');
const permissions = Fetch.service('permissions');
const Redis = Fetch.database('redis').client;
const Email = Fetch.service('email');
const esUserScript = Fetch.config('esScripts/user', true, 'js');

const utilSearchHelper = Fetch.util('elasticsearch', 'helper');
const utilUserSearch = Fetch.util('elasticsearch', 'user', 'index');

const controller = exports;

const _send = function (sent, err, response) {
    if (sent.sent) {
        return true;
    }
    this[err ? 'reject' : 'resolve'](sent.sent = (err || response));
    return err || response;
};

logger.log('init:controller:user', 'init');


/**
 * @description Grabs a user's record from Elasticsearch
 * @name #get
 * TODO: if secure, combine it into a single request user all of User#get arguments
 * TODO: split the formatting into a separate method
 * @param {object} data
 * @param {number} data.userID - whether to grab single user
 * @param {array<number>=} data.userIDs - whether to grab multiple users
 * @param {array<string>} data._source - user's fields to get
 * @param {number=} data.userPermissions - current users permissions - is passed to user if is self
 * @param {boolean=} data.multi - if requesting a single user and this is true will return user as array
 * @param {boolean} secure - whether to pull the users account info as well
 * @param {number=} userID - current users ID - if passed will check the requested userID against this
 * @return {Promise<object>} the Elasticsearch record
 */

exports.get = function (data, secure, userID) {
    logger.log('controller:user:get', 'getting user');
    var deferred = Q.defer(),
        sent = {
            sent: null
        },
        send = _send.bind(deferred, sent),
        calls = [],

        // Is this a single user request?
        single = true,
        promise = null,
        fields, isMe;

    if (typeof data === 'number') {
        data = {
            userID: data
        };
    }

    isMe = secure && data.userID === userID;

    if (!isMe) {
        if (data._source && Array.isArray(data._source)) {
            data._source = _.difference(data._source, User._fields.insecureExclude);
        } else {
            data._source = User._fields.insecureInclude;
        }
    }

    if (typeof data.userID === 'number') {
        // Get Single user
        logger.log('req:controller:user:get', 'retrieving user + permissions');
        calls.push(User.get(data.userID, data).then(utilSearchHelper.getExtractSource));

        if (data.account &&
            (isMe || permissions.can('USER_VIEW', userPermissions))
        ) {
            fields = Array.isArray(data.accountFields) ? data.accountFields : ['activated'];
            calls.push(sqlUser.get.from('users', {
                user_id: data.userID
            }, fields));
        }

        promise = Q.all(calls);
    } else {
        // Get Multiple users
        single = false;
        logger.log('req:controller:user:get', 'retrieving multiple users');
        promise = User.mget(data.userIDs, data).then(utilSearchHelper.mgetExtractSource);
    }

    promise.catch(function (err) {
        if (sent.sent) {
            throw true;
        }

        logger.log('warning:controller:user:get', 'user not found using data:', data, '\nerror:\n', err);
        throw send({
            type: 'updates.ERROR-NO_RESULTS',
            response: 404
        });
    })

    .then(function (response) {
        var _user, sql;
        logger.log('res:controller:user:get', 'user(s) found');
        if (single) {
            _user = response[0][0];
            sql = response[1];

            if (sql) {
                _.extend(_user, sql);
                if (sql.hasOwnProperty('activated')) {
                    _user.notActivated = !parseInt(sql.activated, 10);
                }
            }

            if (isMe) {
                _user.permissions = data.userPermissions;
            }

            response = _user;

            if (data.multi) {
                response = [response];
            }
        }

        return send(null, response);
    })["catch"](function () {});

    return deferred.promise;
};

/**
 * @description returns whether the passed in user has activated their account
 * @param {number} userID
 * @returns {Promise}
 */
exports.isUserActivated = function (userID) {
    return User.getSecure(null, { user_id: userID }, ['activated'])
    .then(function (row) {
        return !!parseInt(row.activated, 10);
    });
};

/**
 * @description Gets a users' permissions group and updates to the next level
 * @param {number} userID
 * @returns {Promise}
 */
exports.upgradePermissionsGroup = function (userID) {
    return User.getPermissionsGroup(userID)

    .then(function (data) {
        // if nothing to activate then continue
        if (!data.group.onActivate || typeof data.permissions !== 'number') {
            return null;
        }

        let groupPermissions = permissions.extendGroupPermissions(data.group.onActivate, data.permissions);

        return User.updatePermissions(userID, groupPermissions);
    });
};

/**
 * @description activates a user in MySQL and updates their permissions if there's a higher user group
 * @param {number} userID
 * @returns {Promise}
 */
exports.activateUser = function (userID) {
    return Q.all([
        controller.upgradePermissionsGroup(userID),

        User.updateSecure({
            activated: 1
        }, {
            user_id: userID
        })
    ]);
};

/**
 * @description Performs a full user search
 * returns only a limited set of fields for each user (what's needed for map search)
 *
 * @param {object} opts - TODO: add properties
 * @returns {Promise<object>} returns elasticsearch results object
 */
exports.search = function (opts) {
    logger.log('service:speaker:search', 'called');
    var method, query, requiredFields, sortArray, source,

        options = {
            from: 0,
            size: 15,
            opts: {},
            filterTypes: ['location', 'hasLocation'],
            //queryTypes: []
            //sortTypes: []
            distance_type: 'sloppy_arc'
        };

    if (_.isPlainObject(opts)) {
        options = _.extend(options, opts);
    }

    options.type = options.type || 'user';
    opts = utilSearchHelper.prepareQuery(opts);

    query = utilSearchHelper.buildSearch(
        'user',
        options.queryTypes,
        options.filterTypes,
        options.sortTypes,
        options
    );

    // the fields to return for the search result
    requiredFields = [
        'userID',
        'details.gender',
        'firstName',
        'lastName',
        'picture',
        'personal',
        'languages',
        'locations.coords',
        'locations.sublocality',
        'locations.city',
        'locations.state',
        'settings.notifs.messaging'
    ];

    if (opts.markerSearch) {
        source = {
            index: User.db.INDEX,
            type: options.type
        };

        method = 'msearch';
        query = {
            body: [
                source, _.extend(_.cloneDeep(query), {
                    from: options.from,
                    size: options.size,
                    _source: requiredFields,
                }), source, _.extend(query, {
                    size: options.markerSize || 251,
                    sort: [],
                    _source: ['locations.coords'],
                })
            ]
        };
    } else {
        method = 'search';
        query = {
            index: User.db.INDEX,
            type: options.type,
            from: options.from,
            size: options.size,
            body: query
        };
    }

    if (opts._source) {
        query._source = opts._source;
    }

    return User[method](query)["catch"](function (err) {
        logger.log('error:service:speaker:search', 'Error searching for speakers using method:', method, '\nopts:', opts, '\nquery:', query, '\nerror:\n', err);
        throw Constants.RESPONSE['500'];
    });
};

/**
 * @description Takes an elasticsearch search result and depending on the type of response
 * it received formats it
 * @name bControllerUser#formatSearchResults
 * @param {object} results
 * @returns {{events: Array<object>}}
 */
exports.formatSearchResults = function (results) {
    var response = {
        users: []
    };

    if (typeof results !== 'object') {
        return Q.resolve(response);
    }

    if (Array.isArray(results.responses)) {
        response.markers = results.responses[1].hits.hits;
        response.users = results.responses[0];
    } else {
        response.users = results;
    }

    response.total = response.users.hits.total;
    response.users = utilSearchHelper.searchExtractSource(response.users);

    return response;
};

/**
 * @description Verifies a user's credentials and returns an updated JSON Webtoken to connect
 * on the front end with
 * @name bControllerUser#login
 * @param {object} credentials
 * @param {string} credentials.password
 * @param {string} credentials.email
 * @return {Promise<object>} user - If user not found, will return a 404
 * @return {string}          user.email
 * @return {number}          user.userID
 * @return {number}          user.notActivated - if the user has not been activated will return 1
 * @return {number}          user.permissions
 */
exports.login = function (credentials) {
    var userData = {
        email: credentials.email
    };

    logger.log('req:controller:user:login', 'Attempting user login');
    return User.verify(credentials).then(function (user) {
        logger.log('res:controller:user:login', 'Successfully verified user credentials');
        userData.userID = parseInt(user.userID, 10);
        userData.notActivated = !user.activated;
        userData.permissions = user.permissions;
        return userData;
    }).catch(function (err) {
        if (err !== 404) {
            throw Constants.RESPONSE[err];
        }

        throw {
            type: 'auth.ERROR-INCORRECT_LOGIN',
            response: 403
        };
    });
};

/**
 * @description Expands a new user object provided with only userID appends a token
 * @param userData
 * @returns {Promise} userObj
 * @returns {object} userObj.user
 * @returns {token} userObj.token - token to authenticate the user with on the front end
 */
exports.prepareLoggedInUser = function (userData) {
    return Q.all([
        controller.get({ userID: userData.userID }),
        jwtHelper.tokenFromUser(userData)
    ]).catch(function (err) {
        logger.log('error:controller:user:prepareLoggedInUser',
            'Error either getting user from ES',
            'or creating a token\nerror:\n', err);

        throw send(Constants.RESPONSE['500']);
    })

    .then(function (userAndToken) {
        logger.log('res:controller:user:prepareLoggedInUser', 'Got user & generated token');
        var token = userAndToken[1],
            preppedUser = userAndToken[0];

        _.extend(preppedUser, userData);

        return {
            user: preppedUser,
            token: token
        };
    });
};

/**
 * @description Updates a user in Elasticsearch
 * @param {number} userID
 * @param {object} updates
 * @returns {Promise}
 */
exports.updateInsecure = function (userID, updates) {
    logger.log('req:controller:user:updateInsecure', 'Updating a user in ES');
    // Make sure the updates are safe
    utilUserSearch.sanitizeFields(updates);

    return User.update(userID, updates);
};

exports.updatePassword = function (userID, password) {
    logger.log('req:service:user:updatePassword', 'Updating users password in MySQL');
    return User.updateSecure({
        password: Util.hashPassword(password)
    }, userID);
};

/**
 * @description Updates a feature on a user as seen/unseen
 * @param {object} data
 * @param {number} data.userID
 * @param {string} data.feature
 * @param {boolean=} opposite - whether to do the opposite and ADD the feature
 * @return {Promise}
 */
exports.featureSeen = function (data, opposite) {
    logger.log('req:controller:user:featureSeen', 'marking a feature as seen');

    let script = esUserScript[opposite ? 'featureAdd' : 'featureSeen'];

    return User.updateScript(data.userID, {
        script: script,
        params: {
            featureName: data.feature
        }
    });
};

/**
 * @description activates an unactivated user
 *          - confirms the token =>
 *          - marks the `activated` field to `true` + updates the users permissions to the new user group
 * @param {string} tokenKey - json webtoken
 * @param {object=} tokenData - if already verified token, will skip reverifying
 * @param {object=} tokenData.userData
 * @param {object=} tokenData.tokenKey
 * @returns {*|promise}
 */
exports.activateProcess = function (tokenKey, tokenData) {
    var deferred = Q.defer(),
        sent = {
            sent: null
        },
        send = _send.bind(deferred, sent),
        promise = tokenData ? Q.when(tokenData) : Email.verifyConfirmToken(tokenKey);

    logger.log('req:controller:user:activate', 'Confirming user token');
    promise
        .catch(function (err) {
            logger.log('error:controller:user:activate', 'Error confirming token. Possible expired', err);
            throw send({ response: 400, type: 'updates.ERROR-LINK_EXPIRED_FULL' });
        })

        // user activation confirmed, activate in SQL
        .then(function (resp) {
            logger.log('res:controller:user:activate', 'User token confirmed');
            let userData = resp.userData;
            tokenKey = resp.tokenKey;

            logger.log('req:controller:user:activate', 'Activating user + setting password');
            return Q.all([
                controller.activateUser(userData.userID),
                Email.welcomeEmail(userData)
            ]);
        })
        .catch(function (err) {
            if (sent.sent) {
                throw err;
            }

            logger.log('error:controller:user:activate', 'Error activating user + password', err);
            throw send(err.type || 'auth.ERROR-SERVER_ERROR');
        })

        // clean up redis
        .then(function () {
            logger.log('res:controller:user:activate', 'User activated and password set');
            Redis.pdel(tokenKey);
            return send(null, true);
        });

    return deferred.promise;
};

/**
 * @description initiates a token verification => password update on a user
 * @param {string} tokenString - json webtoken
 * @param {string} password - unhashed password
 * @returns {Promise}
 */
exports.passwordResetProcess = function (tokenString, password) {
    let deferred = Q.defer(),
        sent = {
            sent: null
        },
        send = _send.bind(deferred, sent),
        tokenData,
        userData;

    logger.log('req:controller:passwordResetProcess', 'Confirming users token is still valid');
    Email.passwordResetConfirmed(tokenString)
    .catch(function (err) {
        logger.log('error:controller:passwordResetProcess',
            'Failed to authenticate password reset token', err);

        throw send({ response: 400, type: 'updates.ERROR-LINK_EXPIRED_FULL' });
    })

    // email token confirmed
    .then(function (respData) {
        logger.log('res:controller:passwordResetProcess', 'Password reset token verified');

        tokenData = respData;
        userData = respData.userData;

        return controller.isUserActivated(userData.userID);
    })
    .catch(function (err) {
        if (sent.sent) {
            throw err;
        }

        logger.log('error:controller:passwordResetProcess', 'Checking whether the user was activated\n', err);
        throw send(Constants.RESPONSE['500']);
    })

    // checked whether user has been activated
    .then(function (activated) {
        let calls = [];

        logger.log('req:controller:passwordResetProcess', 'Updating users\' password');
        calls.push(controller.updatePassword(userData.userID, password, true));

        // if not activated then activate!
        if (!activated) {
            calls.push(controller.activateProcess(null, tokenData));
        }

        return Q.all(calls);
    })
    .catch(function (err) {
        if (sent.sent) {
            throw err;
        }

        logger.log('error:controller:passwordResetProcess',
            'Failed to update users password & possibly activate user using password:', password,
            'userID:', userData.userID, '\nerror:\n', err);

        throw send(Constants.RESPONSE['500']);
    })

    .then(function () {
        // Password updated
        logger.log('res:controller:passwordResetProcess',
            'Successfully updated user\'s password - Deleting the key from Redis');

        Redis.pdel(tokenData.tokenKey);
        return send(null, true);
    });

    return deferred.promise;
};
