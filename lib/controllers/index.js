'use strict';

const logger = require('bragi');
const path = require('path');
const Fetch = GLOBAL.Fetcher;
const dataFerry = Fetch.util('dataFerry');
const config = Fetch.config('config', true, 'js');

// Make sure this gets loaded
Fetch.controller('mailchimp');
Fetch.impl('userDeleted');

/*
 * Don't know where to put this misc init setting
 */
const credentials = Fetch.config('socialServices');

dataFerry.store('cloudinary', {
    cloudName: credentials.cloudinary.cloudName,
    apiKey: credentials.cloudinary.apiKey
});

dataFerry.store('timezoneDB', {
    apiKey: credentials.timezoneDB.apiKey
});

/*
 Send partial, or 404 if it doesn't exist
 */
exports.partials = function (req, res) {
    const stripped = req.url.split('.')[0],
        requestedView = path.join('./', stripped);

    return res.render(requestedView, function (err, html) {
        if (err) {
            return res.send(404);
        } else {
            return res.send(html);
        }
    });
};

exports.views = function (req, res) {
    // strip 'views/' from the beginning of the url and pass it on
    req.url = req.url.substr(6);
    return exports.partials.apply(this, arguments);
};

const devicePrefix = function (isMobile, localeSubstring) {
    const isSubdomain = config.subDomains.indexOf(localeSubstring) !== -1;

    return isSubdomain ? localeSubstring : config.prefix;
};

/*
 Send our single page app
 */
exports.index = function (req, res) {
    return dataFerry.exportOnReady().then(function (data) {
        const locale = req.headers['x-forward-language'] || req.locale;
        const localeSubstr = locale.substr(0, 2);
        const isMobile =  req.headers['x-forward-display'] === 'mobile';
        const indexPrefix = isMobile ? 'm/' : '';



        return res.render(indexPrefix + 'index', {
            dataFerry: data,
            env: process.env.NODE_ENV,
            envType: process.env.ENV_TYPE,
            lang: localeSubstr,
            locale: locale,

            // If loaded via a sub domain
            forceLocale: !!req.headers['x-forward-language'],

            // Array of supported locale domains we have
            localeDomains: config.subDomains,

            prefix: devicePrefix(isMobile, localeSubstr),
            angularVersion: config.angularVersion,
            session: req.frontEnd,
            deployVersion: config.dynamicVars.deployVersion,
            forceAnalytics: config.forceAnalytics,
            releases: JSON.stringify(config.releases),
            configVars: config,
        });
    });
};
