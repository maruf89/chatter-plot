'use strict';

/**
 * TODO: Change service/mailchimp to service/mailchimpAPI, make this service/mailchimp and create a mailchimpHelper
 */

const _ = require('lodash-node/modern'),
    Q = require('q'),
    logger = require('bragi'),

    Fetch = GLOBAL.Fetcher,

    mcData = Fetch.config('mailchimp'),
    Email = Fetch.service('email'),
    Mailchimp = Fetch.service('mailchimp'),
    mcGroups = Mailchimp.groups,
    sqlUser = Fetch.util('mysql', 'userHelper'),
    EMAIL_GROUPS = 'emailGroups',
    controller = exports;

exports.extendMCUser = function (userData) {
    if (process.env.NODE_ENV !== 'production') {
        // don't save users to mailchimp unless on production
        return Q.resolve();
    }

    var loc, mergeVars;
    logger.log('util:mailchimpHelper:_extendMCUser', 'called');

    mergeVars = {
        groupings: [controller.getEmailGrouping('news')]
    };

    if (userData.firstName) {
        mergeVars.FNAME = userData.firstName;
    }

    if (userData.lastName) {
        mergeVars.LNAME = userData.lastName;
    }

    if (loc = userData.coords) {
        mergeVars.mc_location = {
            latitude: loc.lat,
            longitude: loc.lon
        };
    }

    logger.log('req:util:mailchimpHelper:_extendMCUser', 'Extending user on Mailchimp');

    return Mailchimp.lists.subscribe(null, userData.email, mergeVars, false,true, false)
        ["catch"](function (err) {
            return logger.log('err:util:mailchimpHelper:_extendMCUser',
                'error subscribing user: error\n', err);
        })
        .then(function (res) {

            // On success extend the MySQL entry with the returned Mailchimp ID's
            logger.log('req:util:mailchimpHelper:_extendMCUser',
                'Updated user via Mailchimp now updating MySQL');
            return sqlUser.update.userWithFields({
                email: userData.email
            }, {
                mc_leid: res.leid,
                mc_euid: res.euid
            });
        }, function (err) {
            logger.log('error:util:mailchimpHelper:_extendMCUser',
                'Error updating user on Mailchimp using userData:',
                userData, 'mergeVars:', mergeVars, err);
            throw err;
        });
};


/**
 * @description Accepts a single interest grouping object. Compares it to the master
 * base grouping and attaches the individual id's to each group
 *
 * @example
 *     {
 *         form_field: "checkboxes",
 *         groups: [{
 *             interested: true,
 *             name: "Chatterblog",
 *             <---- id: 15317 ---> Will be inserted
 *         }],
 *         id: 4021,
 *         name: "Opt me into:"
 *     }
 *
 * @param  {object} interestGrouping - Mailchimp interest grouping object
 * @return {object} same object with the id attached to each field
 */
exports.matchGroupInfo = function (interestGrouping) {
    let groupID = interestGrouping.id,
        base = mcGroups.primary;

    _.each(base, function (baseGrouping) {
        if (baseGrouping.id !== groupID) {
            return true;
        }

        _.each(baseGrouping.groups, function (base) {
            _.each(interestGrouping.groups,
                function (interest) {
                    if (base.name === interest.name) {
                        interest.id = base.id;
                        return false;
                    }
                });
        });

        return false;
    });

    return interestGrouping;
};


/**
 * Accepts an object structured like:
 *     { <{String}-groupName> : <{Number}-groupingID>, etc... }
 *
 * It converts the object to a Mailchimp groupings array
 *
 * @param  {object} formData
 * @return {array}            Mailchimp formatted groupings array
 */
exports.formPostToGroupings = function (formData) {
    let groupings = [],
        tempObj = {};

    _.each(formData, function (groupingID, groupName) {
        if (!tempObj[groupingID]) {
            tempObj[groupingID] = {
                id: groupingID,
                groups: []
            };
        }

        tempObj[groupingID].groups.push(groupName);
    });

    _.each(tempObj, function (group) {
        groupings.push(group);
    });

    return groupings;
};


/**
 * Unsubscribes a user from select groupings
 *
 * @param  {array} interestGroupings
 * @return {array}
 */
exports.removeFromGroupings = function (interestGroupings, removeArr) {
    let groupings = [];

    _.each(interestGroupings, function (interestGrouping) {
        let groups = [];

        _.each(interestGrouping.groups, function (group) {
            if (group.interested && removeArr.indexOf(group.name) === -1) {
                return groups.push(group.name);
            }
        });

        return groupings.push({
            id: interestGrouping.id,
            groups: groups
        });
    });

    return groupings;
};


/**
 * Iterates over Mailchimp groupings and returns the group with the request id
 *
 * @param  {number} id       Group `id`
 * @param  {string} grouping Grouping `name`
 * @param  {object} Groups   Groupings object to iterate over
 * @return {object}          If found, will return the searched option
 */
exports.extractFromGroup = function (id, grouping, Groups) {
    Groups = Groups || mcGroups.primary;

    let option = {},
        toIterate = Groups;

    if (grouping != null) {
        toIterate = Groups[grouping].groups;
    }

    _.each(toIterate, function (opt) {
        if (parseInt(opt.id, 10) === parseInt(id, 10)) {
            option = opt;
            return false;
        }
    });

    return option;
};


/**
 * Returns Mailchimp groupings for a shortcut name -> display name
 * ex. {string} 'news' -> {object} 'Chatterplot News'
 *
 * @param  {Array|String} group     String containing the group short name
 * @param  {Object=} Groups         Groups to iterate over
 * @return {object}                 object ready for Mailchimp `merge_vars`
 */
exports.getEmailGrouping = function (group, Groups) {
    Groups = Groups || mcGroups.primary;

    let groupings = [],

        groupToObj = function (group) {
            var option;
            if (mcData[EMAIL_GROUPS][group]) {
                option = controller.extractFromGroup(mcData[EMAIL_GROUPS][
                    group
                ], EMAIL_GROUPS, Groups);
                return groupings.push(option.name);
            }
        };

    if (Array.isArray(group)) {
        _.each(group, groupToObj);
    } else {
        groupToObj(group);
    }

    return {
        id: mcData.groupIDs[EMAIL_GROUPS],
        groups: groupings
    };
};
