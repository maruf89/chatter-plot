"use strict";

const logger = require('bragi');

exports.deferredSend = function (deferred, sent) {
    return function (err, response) {
        if (sent.sent) {
            return true;
        }
        deferred[err ? 'reject' : 'resolve'](sent.sent = (err || response || true));
        return err || response;
    }
};
