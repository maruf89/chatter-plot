'use strict';

const _ = require('lodash-node/modern');
const Q = require('q');
const logger = require('bragi');

const Fetch = GLOBAL.Fetcher;
const controller = exports;

const config = Fetch.config('config', true, 'js');
const Constants = Fetch.config('misc/constants');
const DataBus = Fetch.service('DataBus');
const Mandrill = Fetch.service('mandrill');
const Interaction = Fetch.type('message');

const commonInteraction = Fetch.common('type', 'interaction');
const interactionUserImpl = Fetch.impl('interaction_user');
const venueImpl = Fetch.impl('venue');
const SocketIo = Fetch.connection('socket.io');

const utilSearchHelper = Fetch.util('elasticsearch', 'helper');
const interactionSearchHelper = Fetch.util('elasticsearch', 'interaction');
const ctrlUtil = Fetch.controller('ctrlUtility');

const Redis = Fetch.database('redis').client;
const elasticsearch = Fetch.database('elasticsearch');
const esClient = elasticsearch.client;

const REQUEST = 'REQUEST';
const MESSAGE = 'MESSAGE';

const _init = function () {
    let key = Interaction.NOTIFICATION_KEY,
        sourceType = Interaction.SOURCE_DEFINITIONS[key],

        messageUpdateWatch = Interaction.SOURCE + '_' + sourceType.MESSAGE;

    DataBus.on("/notification/mark/" + messageUpdateWatch, _updateWatch);

    // Upon user deletion -> delete users messages
    DataBus.on('/user/deleted', function (user) {
        if (typeof user.userID === 'number') {
            Interaction.deleteID(user.userID);
        }
    });
};

exports.interactionStatuses = Interaction.interactionStatuses;

/**
 * @description Called whenever a message notification gets marked for some reason
 *
 * @callback
 * @private
 * @param {array} arg
 * @param {object} arg.notification - {@link controllers/notification#unmapNID} return object
 * @param {object} arg.data
 * @param {number} arg.data.userID - user this corresponnds to
 * @param {array} arg.data.nIDs - notification IDs
 * @param {string} arg.data.mark - what type of action is applied to these notifications
 * @param {*} arg.data.value
 */
const _updateWatch = function (arg) {
    let data = arg[1],
        markData;

    logger.log('ctrl:message:_updateWatch', 'updating watch');
    if (data.mark === 'read') {
        markData = data.noteKeys.map(function (noteKey) {
            return {
                _id: noteKey.sourceID,
                userID: data.userID
            };
        });
        return exports.markAsRead(markData);
    }
};

exports.get = function (objID, opts) {
    const calls = [];
    const response = {};

    const generalSearch = function (appendAs) {
        calls.push(
            Interaction.get(opts.objID, opts)
                .then(utilSearchHelper.getExtractSource)
                .then(_.partial(utilSearchHelper.appendResultsToObjectAs, response, appendAs))
        );
    };

    logger.log('controller:interaction:get', 'called');
    if (opts.type === 'MESSAGE') {
        if (opts.messages) {
            calls.push(
                Interaction.getMessages(opts.objID, opts)
                    .then(utilSearchHelper.searchExtractSource)
                    .then(_.partial(utilSearchHelper.appendResultsToObjectAs, response, 'messages'))

            );
        }
        if (opts.conversation) {
            generalSearch('conversations');
        }
    } else {
        generalSearch('requests');
    }

    if (opts.withUsers) {
        let source = ['userID', 'firstName', 'lastName', 'picture', 'details.gender'];
        const participants = controller.getParticipants(objID, opts.userID);

        calls.push(
            interactionUserImpl.getUsers(participants, { _source: source })
                .then(utilSearchHelper.mgetExtractSource)
                .then(_.partial(utilSearchHelper.appendResultsToObjectAs, response, 'users'))
        );
    }

    logger.log('req:controller:interaction:get', 'made all requests, waiting…');
    return Q.all(calls).then(function () {
        logger.log('res:controller:interaction:get', 'successfully retrieved interaction');
        return response;
    }).catch(function (err) {
        logger.log('error:controller:interaction:get', 'error retrieving interaction:', objID,
            'using opts:', opts, '\nerror:\n', err
        );
        throw Constants.RESPONSE['500'];
    });
};

exports.expandMessageFields = function (message) {
    message.from = parseInt(message.from, 10);

    let indexFrom = message.to.indexOf(message.from);

    if (indexFrom !== -1) {
        // then remove himself so he doesn't get double notified
        message.participants = _.clone(message.to);
        message.to.splice(indexFrom, 1);
    } else {
        message.participants = message.to.concat([message.from]);
    }

    message.msgID = Interaction.generateID(message.participants);

    return message;
};

exports.sendMessageProcess = function (msg) {
    logger.log('controller:interaction:sendMessageProcess', 'called');

    // Extend our message object with additional needed fields
    controller.expandMessageFields(msg);

    let deferred = Q.defer(),
        sent = {
            sent: null
        },
        returnFn = ctrlUtil.deferredSend(deferred, sent),
        msgBody;

    logger.log('req:controller:interaction:sendMessageProcess', 'checking whether conversation exists');
    Interaction.checkExistence(msg.msgID)
    .catch(function (err) {
        // err will return a {number} error code
        logger.log('error:controller:interaction:sendMessageProcess', 'Checking user existence using msg:', msg);
        throw returnFn(Constants.RESPONSE[err]);
    })

    // returned whether conversation exists
    .then(function (exists) {
        msgBody = Interaction.formatBody([msg])[0];

        // Add the actual message to the db
        let calls = [
            Interaction.createMessage(msgBody)
        ];

        // If conversation exists update it
        if (exists) {
            logger.log('req:controller:interaction:sendMessageProcess', 'conversation exists - updating');
            calls.push(Interaction.updateConversation(msgBody));
        }

        // No conversation, add it
        logger.log('req:controller:interaction:sendMessageProcess', 'conversation doesnt exist - creating');
        calls.push(Interaction.createConversation(msgBody));

        return Q.all(calls);
    })
    .catch(function (err) {
        if (sent.sent)
            throw err;

        // err will return a {number} error code
        logger.log('error:controller:interaction:sendMessageProcess',
            'Updating or Creating conversation and/or creating message using message body:', msgBody);
        throw returnFn(Constants.RESPONSE[err]);
    })

    // message sent -> notify recipients
    .then(function () {
        logger.log('res:controller:interaction:sendMessageProcess',
            'Successfully stored message & conversation - notifying recipients');

        let indexType = 'MESSAGE';

        return controller.notifyRecipientsProcess(msg, msgBody, {
            indexType: indexType,
            emailKey: 'USER',
            settingsField: _getEmailSettingsKey(indexType),
        });
    })
    .catch(function (err) {
        if (sent.sent)
            throw err;

        // err will return a {number} error code
        logger.log('error:controller:interaction:sendMessageProcess',
            'notifying message recipients', msgBody);
        throw returnFn(err);
    })

    // recipients notified
    .then(function () {
        logger.log('res:controller:interaction:sendMessageProcess', 'Successfully notified recipients');
        returnFn(null, msg.msgID);
    });

    return deferred.promise;
};

const _buildSendOptions = function (type, requestData) {
    type = type.toUpperCase();

    return {
        // Notification Opts
        indexType: type,
        sourceType: Interaction.SOURCE_DEFINITIONS[Interaction.NOTIFICATION_KEY][type],

        // Email Opts
        emailKey: 'request-' + requestData.type,
    };
};

/**
 * @description notifies recipients of a message
 *
 * @param {object} message
 * @param {array<number>} message.to - recipients
 * @param {number} message.from - sender
 * @param {string=} message.channel - a web socket channel
 * @param {object} formatted - the formatted/saved elasticsearch object
 * @param {string} formatted[uniqueID] - must have a unique ID that corresponds to the
 *                                       sendOptions.indexType in Constants.elasticsearch.MESSAGE
 * @param {object} sendOptions - the options used to send
 * @param {string} sendOptions.settingsField - the user settings.notifs.email field to check
 * @param {string} sendOptions.indexType - the ES index type toUpperCase()
 *                                         so far only (MESSAGE|REQUEST) are supported
 * @param {number=} sendOptions.sourceType - corresponds to SOURCE_DEFINITIONS in type/message
 * @param {string=} sendOptions.emailKey - key corresponding to mandrillKey.js -> MESSAGE.USER
 * @returns {*|promise}
 */
exports.notifyRecipientsProcess = function (message, formatted, sendOptions) {
    // Checks only in dev
    if (process.env.NODE_ENV !== 'production' &&
        sendOptions.indexType !== sendOptions.indexType.toUpperCase()
    ) {
        logger.log('error:controller:interaction:notifyRecipientsProcess',
            'Incorrect Usage: sendOptions.type must be uppercased, instead got:', sendOptions.indexType
        );
    }

    sendOptions.sourceType = sendOptions.sourceType ||
        Interaction.SOURCE_DEFINITIONS[Interaction.NOTIFICATION_KEY][sendOptions.indexType];

    sendOptions.emailKey = sendOptions.emailKey || sendOptions.indexType;

    if (message.channel) {
        SocketIo.emitToChannel(message.channel, formatted);
    }

    const toUsers = message.to;
    const deferred = Q.defer();
    const sent = { sent: null };
    const returnFn = ctrlUtil.deferredSend(deferred, sent);

    // {string} corresponds to the key of request objects unique ID
    const objectIDKey = Interaction.db[sendOptions.indexType + '_id'];

    const notifyParams = {
        type: Interaction.NOTIFICATION_KEY,
        sourceType: sendOptions.sourceType,
        objectID: formatted[objectIDKey],
        fromID: message.from,
        snippet: formatted.message,
        users: toUsers
    };

    const emailParams = {
        type: Interaction.name,
        emailKey: sendOptions.emailKey,
        users: null,                            // first/last name of recipients
        from: null,                             // first/last name + of the sender
        message: formatted
    };

    // {array<string>} of ES fields to fetch from the sender/recipient
    const senderSourceFields = Mandrill.generateSourceFieldsFromKey('sender', Interaction.name, sendOptions.emailKey);
    const recipientSourceFields = Mandrill.generateSourceFieldsFromKey('recipient', Interaction.name, sendOptions.emailKey);

    // Get all e-mailable users + the info of the sender
    logger.log('req:controller:interaction:notifyRecipientsProcess',
        'Requesting email notifyable users + message sender user data');
    const calls = [
        // get the senders data
        interactionUserImpl.getUser(message.from, { _source: senderSourceFields })
            .then(utilSearchHelper.getExtractSource)
    ];

    // get users to email
    if (sendOptions.settingsField) {
        calls.push(
            interactionUserImpl.canEmailUsers(
                toUsers,                        // array of userIDs
                sendOptions.settingsField,      // the users settings.notifs.email field
                formatted[objectIDKey],         // request object's ID
                message.from,                   // sender's userID
                recipientSourceFields           // elasticsearch _source fields
            )
        );
    } else {
        calls.push(
            interactionUserImpl.getUsers(toUsers, { _source: recipientSourceFields })
                .then(utilSearchHelper.mgetExtractSource)
        )
    }

    // notify the users without waiting on response
    interactionUserImpl.notifyOnly(notifyParams); // regular notification

    Q.all(calls).catch(function (err) {
        logger.log('error:controller:interaction:notifyRecipientsProcess',
            'getting emailable users OR getting from user\nerror\n', err);

        throw returnFn(Constants.RESPONSE['500']);
    })

        // got emailable users + sender info
        .then(function (response) {
            logger.log('res:controller:interaction:notifyRecipientsProcess',
                'received possible emailable recipients + sender data');
            
            let emailUsers = response[1]; // holds an array of users that we can e-mail

            emailParams.from = response[0][0];
            emailParams.users = emailUsers;

            // email all emailable users if there are any
            return emailUsers.length ? interactionUserImpl.emailNotify(emailParams) : Q.when();
        })
        .catch(function (err) {
            if (sent.sent)
                throw err;

            logger.log('error:controller:interaction:notifyRecipientsProcess',
                'notifying users by email using:', emailParams,
                '\nerror\n', err
            );

            throw returnFn(Constants.RESPONSE['500']);
        })

        .then(function () {
            return returnFn(null, Constants.RESPONSE['200']);
        });

    return deferred.promise;
};

/**
 * @description stores an expiring key in redis which if checked for later on - if it hasn't
 * expired yet then the user will not be sent an e-mail
 * @param field
 * @param convoID
 */
exports.setEmailMessageTimeout = function (field, convoID) {
    let userToken = Interaction.generateRedisToken(field, convoID);

    Redis.set(userToken, 1, 'EX', config.messaging.emailAfterXSeconds);
};

/*
 * @description Eats a request ID and returns the relevant users from that
 *
 * @param {string} requestID - looks something like 624_719 or 1029-12_125
 *                              (1029 is the request ID - 12 & 125 are the user ID's)
 * @param {number=} exclude - a single userID to exclude (used to exclude the sender)
 * @return {array<number>}
 * @deprecated use commonInteraction.getParticipants
 */
exports.getParticipants = commonInteraction.getParticipants;

/**
 * @description checks a request ID for whether a passed in user as a participant in the interaction
 *
 * @param {string} requestID
 * @param {number} userID
 * @returns {boolean}
 * @deprecated commonInteraction.isParticipant
 */
exports.isParticipant = commonInteraction.isParticipant;

/**
 * Marks an array of messages as read for a single user
 *
 * @param {array<object>} markData - Objects containing:
 * @param {number}        markData[]._id - msgID
 * @param {number}        markData[].userID
 * @return {Promise}
 */
exports.markAsRead = function (markData) {
    var script  = {
        script: 'if (!ctx._source.read.contains(userID)) { ctx._source.read += userID }'
    };

    return esClient.bulk({
        body: elasticsearch.bulkFormat(
            markData,
            'update',
            Interaction.db.INDEX,
            Interaction.db.MESSAGE,
            '_id',
            false,
            script
        )
    });
};

// Corresponds to the User's settings.notifs.email field
const SETTINGS_NEW_MSG = 'newMsg';
const SETTINGS_REQUEST_TANDEM = 'requestTandem';
const SETTINGS_REQUEST_TEACHER = 'requestTeacher';

/**
 * @description returns a User settings.notifs.email key corresponding to the type of interaction
 * (Currently used to check whether we can e-mail specific users based on their settings)
 *
 * @param {string} indexType - and UPPERCASE indextype string
 * @param {string=} subType
 * @returns {string}
 * @private
 */
const _getEmailSettingsKey = function (indexType, subType) {
    switch (indexType) {
        case 'MESSAGE':
            return SETTINGS_NEW_MSG;

        case 'REQUEST':
            return subType === 'tandem' ? SETTINGS_REQUEST_TANDEM : SETTINGS_REQUEST_TEACHER;
    }
};

/**
 * @description saves a new request to the database and notifies and recipients
 *
 * @param {object} request
 */
exports.newRequestProcess = function (request) {
    const deferred = Q.defer();
    const sent = { sent: null };
    const returnFn = ctrlUtil.deferredSend(deferred, sent);

    const message = request.message;
    let formatted;

    // build the request with only the fields that will be stored
    const requestData = interactionSearchHelper.sanitize.request(request);

    // initiate the chain
    let promise = Q.when(requestData);

    // save the venue if we don't have an ID for it (and it may not exist in the DB)
    if (!requestData.venue.vID) {
        promise = venueImpl.expandVenueForRequest(requestData);
    }

    // If this is replacing a previous request cancel the other one
    if (requestData.oldReqID) {
        controller.updateRequest(requestData.oldReqID, 'edit');
    }

    logger.log('controller:interaction:newRequestProcess', 'saving request in ES');
    promise.then(Interaction.createRequest.bind(Interaction)).catch(function (err) {
            logger.log('error:controller:interaction:newRequestProcess',
                'error saving request using:\n', requestData,
                '\nerror:\n', err
            );

            throw returnFn(Constants.RESPONSE[500]);
        })

        .then(function (_formatted) {
            logger.log('res:controller:interaction:newRequestProcess', 'successfully saved in ES');

            formatted = _formatted;

            // load the venue from source so we get all of the details
            return venueImpl.getVenue(formatted.venue, { fromSource: true });

        }).catch(function (err) {
            if (sent.sent)
                throw err;

            logger.log('error:controller:interaction:newRequestProcess',
                'loading venue pertaining to the request using request:', formatted,
                '\nerror:\n', err
            );
            throw returnFn(Constants.RESPONSE[500]);
        })

        .then(function (venueData) {
            logger.log('res:controller:interaction:newRequestProcess', 'Got venue data for location');

            formatted.venueDetails = venueImpl.formatPlace(venueData, 'google');

            const rcptData = {
                to: controller.getParticipants(formatted.reqID, formatted.creator),
                from: formatted.creator,
            };

            const sourceTypeKey = Interaction.generateRequestSourceType(formatted.type);
            const sourceType = Interaction.SOURCE_DEFINITIONS[Interaction.NOTIFICATION_KEY][sourceTypeKey];
            const indexType = 'REQUEST';

            // Will look something like REQUEST_TANDEM or REQUEST_TEACHER
            let emailKey = indexType + '_' + formatted.type;
            emailKey = emailKey.toUpperCase();

            // the message is needed for the email
            formatted.message = message;

            logger.log('req:controller:interaction:newRequestProcess', 'notifying recipients');
            return controller.notifyRecipientsProcess(rcptData, formatted, {
                indexType: indexType,
                sourceType: sourceType,
                emailKey: emailKey,
                settingsField: _getEmailSettingsKey(indexType, formatted.type),
            });

        }).catch(function (err) {
            if (sent.sent)
                throw err;

            logger.log('error:controller:interaction:newRequestProcess',
                'error saving request',
                '\nerror:\n', err
            );
            throw returnFn(Constants.RESPONSE[500]);
        })

        .then(function () {
            return returnFn(null, formatted);
        });

    return deferred.promise;
};

exports.extendRequestResults = function (opts, requestResults) {
    if (!requestResults.total) {
        return Q.resolve(requestResults);
    }

    logger.log('controller:interaction:extendRequestResults', 'getting additional venues and/or users info')
    let venues = [];
    let userIDs = [];
    const promises = [];

    _.each(requestResults.results, function (requestObj) {
        opts.withVenues && venues.push(requestObj.venue);
        opts.withUsers && (userIDs = userIDs.concat(requestObj.participants));
    });

    if (opts.withVenues) {
        venues = interactionSearchHelper.reduceVenues(venues);
        promises.push(
            venueImpl.getVenues(venues, { fromSource: true })
                .then(_.partial(venueImpl.formatBulk, venues, 2))
        );
    }

    if (opts.withUsers) {
        userIDs = interactionSearchHelper.reduceUsers(userIDs, opts.userID);

        let source = [
            'userID',
            'firstName',
            'lastName',
            'picture',
            'details.gender',
        ];

        promises.push(
            interactionUserImpl.getUsers(userIDs, { _source: source }).then(utilSearchHelper.mgetExtractSource)
        );
    }

    return Q.all(promises)
        .then(function (results) {
            if (opts.withVenues) {
                requestResults.venues = results.shift();
            }

            if (opts.withUsers) {
                requestResults.users = results[0];
            }

            return requestResults;
        });
};

exports.search = function (options) {
    const searchQuery = {
        body: utilSearchHelper.buildSearch(
            'interaction',
            options.queryTypes,
            options.filterTypes,
            options.sortTypes,
            options
        ),
        size: options.size || 10,
        from: options.from || 0
    };

    let promise = Interaction.search(searchQuery, options)
        .then(utilSearchHelper.formatSearchSource)
        .catch(function (err) {
            logger.log('error:controller:interaction:fetch', 'error fetching interaction\n', err);
            throw Constants.RESPONSE[500];
        });

    if (options.withVenues || options.withUsers) {
        promise = promise
            .then(_.partial(controller.extendRequestResults, options))
            .catch(function (err) {
                logger.log('error:controller:interaction:fetch', 'error extra data\n', err);
                throw Constants.RESPONSE[500];
            });
    }

    return promise;
};

exports.updateRequest = function (reqID, status) {
    const INDEX_TYPE = 'REQUEST';
    return Interaction.update(reqID, INDEX_TYPE, { status: status });
};

const canRespondRequest = function (request, status) {
    let _setStatus = request.status;

    if (_setStatus === status ||
        _setStatus === 'deny' ||
        _setStatus === 'cancel' ||
        _setStatus === 'edit'
    ) {
        return false;
    }

    return true;
};

/**
 * @description responds to a tandem request
 *
 * @param {string} reqID
 * @param {string} status - one of (accept|deny|edit|cancel)
 * @param {number} responderID - userID
 * @param {object} opts
 * @param {string=} opts.message - optional message
 * @returns {Promise}
 */
exports.respondRequestProcess = function (reqID, status, responderID, opts) {
    const deferred = Q.defer();
    const sent = { sent: null };
    const returnFn = ctrlUtil.deferredSend(deferred, sent);
    const INDEX_TYPE = 'REQUEST';

    let formatted;
    let notifyParams;

    logger.log('req:controller:interaction:respondRequestProcess', 'updating request & fetching it');
    Interaction.get(reqID, { type: INDEX_TYPE })

        .then(function (request) {
            formatted = request._source;
            formatted.message = opts.message;

            if (!canRespondRequest(formatted, status)) {
                // If request is not updatable
                return returnFn(null, { completed: false, status: formatted.status });
            }

            logger.log('res:controller:interaction:respondRequestProcess', 'updated request & get venue');
            return Q.all([
                controller.updateRequest(reqID, status),
                venueImpl.getVenue(formatted.venue, { fromSource: true }),
            ]);
        }).catch(function (err) {
            if (sent.sent) {
                throw err;
            }

            logger.log('error:controller:interaction:respondRequestProcess',
                'loading venue pertaining to the request using reqID:', reqID,
                '\nerror:\n', err
            );
            throw returnFn(Constants.RESPONSE[500]);
        })

        // got the venue from source
        .then(function (response) {
            if (sent.sent) {
                return;
            }
            logger.log('res:controller:interaction:respondRequestProcess', 'Got venue data for location');

            let venueData = response[1];
            formatted.venueDetails = venueImpl.formatPlace(venueData, 'google');
            formatted.status = status;

            let message = {
                to: controller.getParticipants(reqID, responderID),
                from: responderID,
            };

            // Will look something like REQUEST_TANDEM_ACCEPT or REQUEST_TEACHER_DENY
            let sourceTypeKey = Interaction.generateRequestSourceType(formatted.type, status);
            let sourceType = Interaction.SOURCE_DEFINITIONS[Interaction.NOTIFICATION_KEY][sourceTypeKey];

            notifyParams = {
                indexType: INDEX_TYPE,
                sourceType: sourceType,
                emailKey: sourceTypeKey,
                forceEmail: true,
            };

            logger.log('req:controller:interaction:newRequestProcess', 'notifying recipients');
            return controller.notifyRecipientsProcess(message, formatted, notifyParams);
        }).catch(function (err) {
            if (sent.sent) {
                throw err;
            }

            logger.log('error:controller:interaction:respondRequestProcess',
                'notifyParams:', notifyParams,
                '\nerror notifying recipients using:', formatted,
                '\nerror:\n', err
            );
            throw returnFn(Constants.RESPONSE[500]);
        })

        // Notified
        .then(function () {
            if (sent.sent) {
                return;
            }

            return returnFn(null, { completed: true, status: status });
        });

    return deferred.promise;
};

exports.sendReminder = function (request) {
    logger.log('controller:interaction:sendReminder', 'called - getting venue');
    return venueImpl.getVenue(request.venue, { fromSource: true })

        .then(function (venueData) {
            request.venueDetails = venueImpl.formatPlace(venueData, 'google');

            // Will look something like REQUEST_TANDEM_ACCEPT or REQUEST_TEACHER_DENY
            const sourceTypeKey = Interaction.generateRequestSourceType(request.type, 'reminder');
            const sourceType = Interaction.SOURCE_DEFINITIONS[Interaction.NOTIFICATION_KEY][sourceTypeKey];
            const INDEX_TYPE = REQUEST;

            const notifyParams = {
                indexType: INDEX_TYPE,
                sourceType: sourceType,
                emailKey: sourceTypeKey,
                forceEmail: true,
            };

            const userA = { from: request.participants[0], to: [request.participants[1]] };
            const userB = { from: request.participants[1], to: [request.participants[0]] };

            logger.log('req:controller:interaction:sendReminder', 'notifying participants');
            return Q.all([
                controller.notifyRecipientsProcess(userA, request, notifyParams),
                controller.notifyRecipientsProcess(userB, request, notifyParams),
            ]);
        })
        .catch(function (err) {
            logger.log('error:controller:interaction:sendReminder',
                'request:', request,
                '\nerror:\n', err
            );
            throw returnFn(Constants.RESPONSE[500]);
        });
};

// Initiate startup
_init();
