"use strict";

const _ = require('lodash-node/modern'),
    logger = require('bragi'),
    Q = require('q'),

    Fetch = GLOBAL.Fetcher,

    notificationUserImpl = Fetch.impl('notification_user'),
    utilSearchHelper = Fetch.util('elasticsearch', 'helper'),
    Util = Fetch.util('utility'),
    User = Fetch.type('user'),
    dataFerry = Fetch.util('dataFerry'),
    Redis = Fetch.database('redis').client;

let init = function () {
    let prefix = 'ds_';
    let checkKey = 'siteMessage';

    Redis.get(prefix + checkKey, function (has) {
        if (has) {
            let data = Util.string.base64Decode(has);

            try {
                data = JSON.parse(data);
                exports.storeDynamicSetting(checkKey, data);
            } catch (e) {}
        }
    });
};

/**
 * @description Stores arbitrary data to be passed to the user object via CP.Settings
 * This prefixes every property with 'ds_' to prevent overwriting necessary fields
 * @param {string} key
 * @param {*} data
 * @returns {Promise<string>} returns the new key name
 */
exports.storeDynamicSetting = function (key, data) {
    // Prefix the key just so there's no collisions
    const fullKey = 'ds_' + key;

    if (data) {
        dataFerry.store(fullKey, data);
        Redis.set(fullKey, JSON.stringify(data));

    } else {
        dataFerry.unset(fullKey);
        Redis.del(fullKey);
    }

    return Q.when(fullKey);
};

/**
 * @description needs to be updated before re-use
 * @deprecated
 * @returns {*}
 */
exports.featureDesired = function () {
    // first get users

    //return User.search({
    //    query: {
    //        filtered: {
    //            filter: {
    //                term: {
    //                    features: 'locs01'
    //                }
    //            }
    //        }
    //    },
    //    _source: ['firstName', 'lastName', 'userID', 'email', 'settings.locale'],
    //    size: 500,
    //}).then(utilSearchHelper.searchExtractSource)
    //
    //.then(function (results) {
    //    let users = results.filter(function (user) {
    //        return user.userID && user.email;
    //    });
    //
    //    return notificationUserImpl.notifyUsers({
    //        users: users,
    //        type: 'locs01',
    //    })
    //});
};

_.delay(init, 5000);
