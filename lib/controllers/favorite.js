/**
 * @module favoriteController
 * @description handles getting a users favorites and updating them
 */

'use strict';

var _ = require('lodash-node/modern'),
    Q = require('q'),
    logger = require('bragi'),

    Fetch = GLOBAL.Fetcher,

    User = Fetch.type('user'),
    utilSearchHelper = Fetch.util('elasticsearch', 'helper'),
    esUserScript = Fetch.config('esScripts/user', true, 'js');

/**
 * @description Grabs all favorites for a specific user
 * @param {number} userID
 * @param {array<string>=} folders - if passed will only return the selected folders
 */
exports.fetch = function (userID, folders) {
    logger.log('req:controller:favorite:fetch', 'fetching user\'s favorites');

    var source = ['favorite'];

    if (Array.isArray(folders)) {
        source = folders.map(function (folder) {
            return 'favorite.' + folder;
        });
    }

    return User.get(userID, { _source: source }).then(utilSearchHelper.getExtractSource);
};

/**
 * @description Handles both adding/removing a userID to the user's favorites
 * @name favoriteController#toggle
 * @param {object} data
 * @param {number} data.targetID - the userID of the user to add
 * @param {number} data.sourceID - the user to add the targetID to
 * @param {string} [data.folder = 'all'] - the folder to save the user to
 * @returns {Promise}
 */
exports.toggle = function (data) {
    logger.log('req:controller:favorite:toggle', 'toggling favorite for user');
    return User.updateScript(data.sourceID, {
        script: esUserScript[data.method === 'add' ? 'addToFavorite' : 'removeFromFavorite'],
        params: {
            folder: data.folder || 'all',
            userID: data.targetID
        }
    });
};

/**
 * @description removes multiple users at a time from a folder in the users favorites
 * @param {object} data
 * @param {array<number>} data.targetIDs - array of userIDs to remove
 * @param {string} [data.folder='all'] - the folder to remove the users from
 */
exports.bulkRemove = function (data) {
    logger.log('req:controller:favorite:bulkRemove', 'removing bulk users');
    return User.updateScript(data.sourceID, {
        script: esUserScript.bulkRemoveFromFavorites,
        params: {
            folder: data.folder || 'all',
            targetIDs: data.targetIDs
        }
    });
};
