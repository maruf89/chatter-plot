/**
 * @module bImplEventNotification
 */

'use strict';

const _ = require('lodash-node/modern'),
    logger = require('bragi'),

    Fetch = GLOBAL.Fetcher,

    Notification = Fetch.type('notification');

/**
 * @description Registers all of the different types of notifications pertaining
 * to each `type` module
 */
exports.registerNotificationTypes = function () {
    var types = [
        Fetch.type('event'),    // Event
        Fetch.type('user'),     // User
        Fetch.type('message'),  // Message
    ];

    _.each(types, function (service) {
        _.each(service.SOURCE_DEFINITIONS, function (subTypeDefinitions, sourceKey) {
            Notification.registerSourceType(sourceKey, subTypeDefinitions);
        });
    });
};
