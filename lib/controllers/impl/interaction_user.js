'use strict';

const _ = require('lodash-node/modern');
const logger = require('bragi');
const Q = require('q');

const Fetch = GLOBAL.Fetcher;

const Redis = Fetch.database('redis').client;
const Message = Fetch.type('message');
const User = Fetch.type('user');
const notificationController = Fetch.controller('notification');
const utilSearchHelper = Fetch.util('elasticsearch', 'helper');
const EmailNotification = Fetch.service('EmailNotification');
const SocketIo = Fetch.connection('socket.io');

const Notifier = EmailNotification.notifier;

const commonUser = Fetch.common('util', 'user');
const commonVenue = Fetch.common('util', 'venue');

/**
 * @description the maximum # of characters an e-mail message can have
 * @type {number}
 */
const MAX_EMAIL_MESSAGE_LEN = 140;

    /**
 * @description Stores in Redis who the last sender of a conversation was
 * @param {string} requestID - the unique ID associated with this interaction
 * @param {number} senderID
 * @private
     */
const _setLastSenderToken = function (requestID, senderID) {
    let lastSenderToken = Message.generateRedisToken('MSG', requestID);

    // set the last sender
    Redis.set(lastSenderToken, senderID);
};

/**
 * @description accepts a list of userIDs and a settings field & checks which of the passed in users
 * can be sent e-mails. Runs these checks to see whether to NOT send an email:
 *              - User is currently online
 *              - User has their email setting turned off
 *              - The sender was the last person to send a message (don't send 2 e-mails for two subsequent
 *                                                                  messages from the same person)
 * @param {array<number>} _userIDs
 * @param {string} field - a field corresponding to a user's 'settings.notifs.email'
 * @param {string} requestID - the unique ID associated with this interaction
 * @param {number} senderID
 * @returns {Promise.<array<object>>}
 */
exports.canEmailUsers = function (_userIDs, field, requestID, senderID, _source) {
    const checkField = 'settings.notifs.email';

        // First filter for all of the users that currently aren't online
    const userIDs = _userIDs.filter(function (userID) {
        return SocketIo.isRoomEmpty(userID);
    });

    _source.push(checkField);

    if (!userIDs.length) {
        logger.log('res:impl:message_user:emailNotify', 'all recipients are online');

        // Update the sender
        _setLastSenderToken(requestID, senderID);
        return Q.resolve([]);
    }

    logger.log('req:impl:message_user:emailNotify', 'fetching message to users');
    return User.mget(userIDs, { _source: _source })
        .then(utilSearchHelper.mgetExtractSource)
        .then(function (users) {
            // if no e-mailable users, return
            if (!users.length) {
                logger.log('res:impl:message_user:emailNotify', 'recipients disabled email notifications');
                return [];
            }

            logger.log('res:impl:message_user:emailNotify', 'got users with email notification enabled');
            // build an array of promises from the returned users
            let promises = users.reduce(function (arr, user) {
                if (user.settings.notifs.email[field]) {
                    let lastSenderToken = Message.generateRedisToken('MSG', requestID);

                    arr.push(
                        Q.all([
                            Redis.pget(lastSenderToken)
                        ]).then(function (responses) {
                            //let notExpired = responses[0];
                            let lastSender = responses[0] && responses[0].toString();

                            // checking whether the sender is sending follow up messages
                            // if the rcp never responded then they may be ignoring the sender - don't spam
                            let isRepeatSender = lastSender === String(senderID);

                            // Store the senderS ID for future checks
                            Redis.set(lastSenderToken, senderID);

                            // Don't send email if it's a repeat sender
                            return isRepeatSender ? null : user;
                        })
                    );
                }

                return arr;
            }, []);

            return promises.length ? Q.all(promises) : Q.when();
        })
        .catch(function (err) {
            logger.log('error:impl:message_user:emailNotify', 'Error getting to users\nerror\n', err);
            throw err;
        })

        .then(function (emailableUsers) {
            return _.filter(emailableUsers, _.identity);
        });
};

exports.getUser = User.getInsecure.bind(User);

exports.getUsers = User.mget.bind(User);

class MessageMandrillFields extends EmailNotification.MandrillFields {
    constructor() {
        super();
    }

    FROM_NAME(data) {
        return {
            name: 'FROM_NAME',
            content: (data.from.firstName + (data.from.lastName ? ' ' + data.from.lastName : '')).trim()
        };
    }

    FROM_ID(data) {
        return {
            name: 'FROM_ID',
            content: data.from.userID
        };
    }

    SENDER_PHOTO(data) {
        return {
            name: 'SENDER_PHOTO',
            content: commonUser.photoSrc(data.from, {
                width: 70,
                height: 70,
                thumb: true
            }, true)
        };
    }

    IS_CREATOR(user, data) {
        return {
            name: 'IS_CREATOR',
            content: user.userID === data.message.creator
        }
    }

    VENUE_NAME(data) {
        return {
            name: 'VENUE_NAME',
            content: data.message.venueDetails.name
        };
    }

    VENUE_FORMATTED(data) {
        return {
            name: 'VENUE_FORMATTED',
            content: commonVenue.objToAddress(data.message.venueDetails)
        };
    }

    VENUE_LINK(data) {
        return {
            name: 'VENUE_LINK',
            content: commonVenue.addressObjToUrl(
                data.message.venueDetails.serviceName,
                data.message.venueDetails
            )
        };
    }

    WHEN(data) {
        return {
            name: 'WHEN',
            content: this.format.date(data.message.when)
        };
    }

    MSG_ID(data) {
        return {
            name: 'MSG_ID',
            content: data.message.msgID
        };
    }

    REQ_ID(data) {
        return {
            name: 'REQ_ID',
            content: data.message.reqID
        }
    }

    /**
     * @description attaches just the first id in the request '1023-232_23' => '1023'
     */
    REQ_ID_SHORT(data) {
        return {
            name: 'REQ_ID_SHORT',
            content: data.message.reqID.match(/^\d+/)[0]
        }
    }

    OLD_REQ_ID_SHORT(data) {
        let oldReq = data.message.oldReqID;

        return {
            name: 'OLD_REQ_ID_SHORT',
            content: oldReq && oldReq.match(/^\d+/)[0]
        }
    }

    DENY(data) {
        return {
            name: 'DENY',
            content: data.message.status === 'deny'
        }
    }

    MESSAGE(data) {
        let message = data.message.message;

        // Trim the message down if too long
        //if (message && message.length > MAX_EMAIL_MESSAGE_LEN) {
        //    message = message.substr(0, MAX_EMAIL_MESSAGE_LEN - 1) + '…';
        //}

        return {
            name: 'MESSAGE',
            content: message
        };
    }

    NOTIFY_MESSAGE(data) {
        return {
            name: 'NOTIFY_MESSAGE',
            content: data.notifyMessage
        };
    }
}

/**
 * @param {object} eventData - see EmailNotification.Notifier#emailNotifyUsers for options
 * @returns {Promise}
 */
exports.emailNotify = function (messageData) {
    logger.log('req:impl:message_user:emailNotify', 'Notifying users by email');

    return Notifier.emailNotifyUsers(messageData, new MessageMandrillFields())
        .catch(function (err) {
            logger.log('error:impl:message_user:emailNotify',
                'Error emailing users using mesageData:', messageData,
                '\nerror:\n', err
            );
            throw 500;
        });
};

exports.notifyOnly = function (notifyData) {
    logger.log('req:impl:message_user:notifyOnly', 'called');
    return notificationController.notify(notifyData.users, notifyData.type, notifyData.objectID, {
        from: notifyData.fromID,
        snippet: notifyData.snippet,
        when: notifyData.when,
        sourceType: notifyData.sourceType
    })
        .catch(function (err) {
            logger.log('error:impl:message_user:emailNotify',
                'Error notifying users using notifyData:', notifyData,
                '\nerror:\n', err
            );
            throw 500;
        });
};
