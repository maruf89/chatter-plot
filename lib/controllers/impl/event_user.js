/**
 * @module bImplEventUser
 */

'use strict';

const _ = require('lodash-node/modern');
const logger = require('bragi');
const Q = require('q');

const Fetch = GLOBAL.Fetcher;

const utilEventUserSearch = Fetch.util('elasticsearch', 'event_user', 'index');
const User = Fetch.type('user');
const Event = Fetch.type('event');

const commonUtil = Fetch.common('util', 'general');

const utilSearchHelper = Fetch.util('elasticsearch', 'helper');
const esUserScript = Fetch.config('esScripts/user', true, 'js');

/**
 * @description The default fields to get of a user when getting the users of an event
 * @type {array<string>}
 */
const getUserSourceDefault = [
    'userID',
    'email',
    'details',
    'firstName',
    'lastName',
    'picture',
    'languages',
    'locations.coords',
    'settings.notifs.messaging',
    'settings.locale',
];

/**
 * @description builds a query to get all users that are both:
 *  - within a radius of the event
 *  - have their settings set to receive notification emails for the mentioned event OR all events
 * @param {object} event
 * @returns {Promise<array>} usersArr
 * @returns {array<object>} usersArr returns an array of users with just the fields necessary for e-mails
 */
exports.getRelevantEventUsers = function (event) {
    var query = utilEventUserSearch.relevantEventUsersSearch(event);

    return User.search({
        body: query,
        _source: ['userID', 'email', 'firstName', 'lastName', 'settings.locale'],
        size: 100,
        from: 0
    })

    .then(utilSearchHelper.searchExtractSource)
    .catch(function (err) {
        logger.log('error:impl:event_user:getRelevantEventUsers', 'Error getting relevant users to event\nerror\n', err);
        throw err;
    })
};

/**
 * @description Returns all users pertaining to an event
 *
 * @param {object} event
 * @param {array<number>} event.goingList
 * @param {array<number>=} event.waitList
 * @param {options=} options
 * @param {array<number>=} options.exclude - array of userIDs to exclude
 * @param {boolean=} options.waitList - if true then event.waitList must be available
 * @param {array<string>=} options._source - fields to get for the users
 * @returns {Promise<array<object>>} a formatted array of user objects
 */
exports.getEventUsers = function (event, options) {
    options = options || {};

    var userIDs = [].concat(event.goingList),
        method,
        format; // corresponds to a elasticsearch format result type in `utilSearchHelper`

    if (options.waitList) {
        userIDs = _.uniq(userIDs.concat(event.goingList));
    }

    // If we have any user IDs to exclude
    if (Array.isArray(options.exclude)) {
        _.each(options.exclude, _.partial(commonUtil.array.remove, userIDs));
    }

    if (!options._source) {
        options._source = getUserSourceDefault;
    }

    // if we have more than 1 user to notify use the bulk get endpoint
    if (userIDs.length > 1) {
        method = 'mget';
        format = 'mgetExtractSource';
    } else {
        method = 'get';
        format = 'getExtractSource';
    }

    return User[method](userIDs, options).then(utilSearchHelper[format]);
};

exports.getEventHosts = function (events) {
    let hostIDs = _.reduce(events, function (IDs, event) {
        if (event.creator) {
            IDs.push(event.creator);
        }

        return IDs;
    }, []);

    if (!hostIDs.length) {
        return Q.resolve([]);
    }

    hostIDs = _.uniq(hostIDs);

    return User.mget(hostIDs, { _source: User._fields.genaralFields })
        .then(utilSearchHelper.mgetExtractSource);
};

/**
 * @description Updates a user to include the eID (event ID) in their `attending` field
 * @name bImplEventUser#addEventToUser
 * @param {number} userID
 * @param {number} eID - event ID
 */
exports.addEventToUser = function (userID, eID, waitList) {
    var key, script;

    if (waitList) {
        key = 'attending.eventsWaitList';
        script = esUserScript.updateGoingWaitlist;
    } else {
        key = 'attending.events';
        script = esUserScript.updateGoing;
    }

    return User.scriptUpdate(userID, null, key, false, {
        script: script,
        params: {
            event_id: [parseInt(eID, 10)]
        }
    });
};


/**
 * @description Same arguments as {@link bImplEventUser#addEventToUser}` except this
 * removes an event from a user's profile
 * @name bImplEventUser#removeEventFromUser
 */
exports.removeEventFromUser = function (userID, eID, waitList) {
    var key = (waitList) ? 'attending.eventsWaitList' : 'attending.events';

    return User.scriptUpdate(userID, null, key, false, {
        script: esUserScript.updateNotGoing,
        params: {
            event_id: parseInt(eID, 10)
        }
    });
};
