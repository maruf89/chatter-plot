/**
 * @module bImplEventVenue
 */

'use strict';

const _ = require('lodash-node/modern');
const logger = require('bragi');
const Q = require('q');

const controller = exports;
const Fetch = GLOBAL.Fetcher;

const commonVenue = Fetch.common('util', 'venue');
const Venue = Fetch.type('venue');

exports.formatPlace = commonVenue.scrapePlace;

exports.getEventVenues = function (events, withSource) {
    logger.log('impl:event_venue:getEventVenues', 'called');
    const vIDs = _.uniq(
        _.map(events, function (event) {
            return event.venue.vID;
        })
    );

    if (!vIDs.length) {
        return Q.resolve([]);
    }

    let promise = Venue.mget(vIDs);

    if (withSource) {
        promise = promise.then(function (venues) {
            const opts = { fromSource: true };

            return Q.all(
                venues.docs.map(function (venue) {
                    return controller.getVenue(venue._source, opts);
                })
            );
        });
    }

    return promise;
};

/**
 * @description gets a google/yelp venue either from them directly or from the db
 *
 * @param venueObj
 * @param opts
 * @returns {*}
 */
exports.getVenue = function (venueObj, opts) {
    logger.log('impl:event_venue:getVenue', 'called');
    if (opts.fromSource) {
        return commonVenue.getPlace(venueObj);
    }

    let vID = venueObj.vID;

    if (!vID) {
        return Q.reject([]);
    }

    return Venue.get(vID, opts);
};
