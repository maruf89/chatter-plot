"use strict";

const Q = require('q'),
    moment = require('moment'),
    logger = require('bragi'),

    Fetch = GLOBAL.Fetcher,

    User = Fetch.type('user'),
    crypter = Fetch.util('encryptDecrypt'),
    Mandrill = Fetch.service('mandrill'),

    mandrillKey = Fetch.config('templates/mandrillKey', true, 'js')[User.name],
    config = Fetch.config('config', true, 'js'),

    format = {
        date: function (time) {
            time = time.substr(0, 15);
            return moment(time, 'YYYYMMDDTHHmmss').format('MMMM Do YYYY, h:mm a');
        },

        subDomain: function (user) {
            let locale = user.settings && user.settings.locale,
                subdomain = config.prefix;

            if (locale) {
                let substr = locale.substr(0, 2);
                if (config.subDomains.indexOf(substr) !== -1) {
                    subdomain = substr;
                }
            }

            return subdomain;
        },
    },

    /**
     * @description A map of possible Mandrill merge variables
     * Each field receives:
     *      @param {object} event - a full event object
     *      @param {object} data - data passed to the notifier method
     *      @returns {{name: string, content: string}}
     * @type {object}
     */
    mergeFields = {
        'env': function (user) {
            return {
                name: 'env',
                content: format.subDomain(user)
            };
        }
    };

/**
 * @description notifies users by e-mail + notification of any event activity
 * TODO: Use the functionality from EmailNotification
 * @name bImplNotificationUser#notifyUsers
 * @param {object} data
 * @param {string} data.type - refers to template details in `mandrillKey.js` under `EVENT`
 * @param {array<string>} data.globalVars
 * @param {array<object>} data.users
 * @returns {*}
 * @deprecated
 */
exports.notifyUsers = function (data) {
    if (!data.users || !data.users.length) {
        return Q.resolve('No users to notify');
    }

    data.globalVars = data.globalVars || [];

    var emailOpts = mandrillKey[data.type],

        /**
         * Array of users to e-mail
         * @type {Array<object>}
         */
        users2Email = [],

        mandrillOptions = {
            track_clicks: true,
            subject: emailOpts.subject,

            // Build the global Variables
            global_merge_vars: data.globalVars.reduce(function (arr, string) {
                if (mergeFields[string]) {
                    arr.push(mergeFields[string](data));
                }

                return arr;
            }, []),

            // Build each users merge variable
            merge_vars: data.users.reduce(function (vars, user) {
                users2Email.push({
                    email: user.email,
                    name: user.firstName + ' ' + user.lastName
                });

                vars.push({
                    rcpt: user.email,
                    vars: [
                        {
                            name: 'FNAME',
                            content: user.firstName
                        }, {
                            name: 'emailKey',
                            content: crypter.encrypt(user.email)
                        }, {
                            name: 'USER_ID',
                            content: user.userID
                        }, {
                            name: 'env',
                            content: format.subDomain(user)
                        }
                    ]
                });

                return vars;
            }, [])
        };

    logger.log('req:impl:notification_user:notifyUsers', 'Notifying users of event activity');
    return Mandrill.sendTemplate(emailOpts.template, [], users2Email, mandrillOptions)

        .then(function () {
            logger.log('res:impl:notification_user:notifyUsers', 'Successfully notified users');
            return 200;
        }).catch(function (err) {
            logger.log('error:impl:notification_user:notifyUsers', 'Error notifying users\nerror\n', err);
            throw err;
        });
};
