/**
 * @module bImplEventVenue
 */

'use strict';

const _ = require('lodash-node/modern');
const logger = require('bragi');
const Q = require('q');

const Fetch = GLOBAL.Fetcher;

const Venue = Fetch.type('venue');
const venueCtrlImpl = Fetch.controller('ctrlImpl', 'venue');

const commonVenue = Fetch.common('util', 'venue');

const utilSearchHelper = Fetch.util('elasticsearch', 'helper');

// make sure its loaded and inited
Fetch.service('googleMaps');

exports.searchSaveVenue =

/**
 * @description gets a google/yelp venue either from them directly or from the db
 *
 * @param venueObj
 * @param opts
 * @returns {*}
 */
exports.getVenue = function (venueObj, opts) {
    if (opts.fromSource) {
        return commonVenue.getPlace(venueObj);
    }

    let vID = venueObj.vID;

    if (!vID) {
        return Q.reject([]);
    }

    return Venue.get(vID, opts);
};

exports.formatPlace = commonVenue.scrapePlace;
exports.formatBulk = commonVenue.formatBulk;

/**
 * @description Gets venues from the venues database
 *
 * @param {array<object>} venueObjects
 * @param {object} opts
 * @param {boolean?} opts.fromSource - whether to load the venue from yelp/google source
 * @returns {*}
 */
exports.getVenues = function (venueObjects, opts) {
    opts = opts || {};

    if (!venueObjects.length) {
        return Q.reject();
    }

    if (opts.fromSource) {
        return commonVenue.getPlaceBulk(venueObjects, opts.format);
    }

    let vIDs = _.pluck(venueObjects, 'vID');

    return Venue.mget(vIDs);
};

exports.expandVenueForRequest = function (request) {
    return venueCtrlImpl.safeSave([request.venue])
        .then(function (formattedArr) {
            const formatted = formattedArr[0];
            const venue = {
                vID: formatted.vID,
                coords: formatted.coords,
            };

            if (formatted.google) venue.google = formatted.google;
            if (formatted.yelp) venue.yelp = formatted.yelp;

            request.venue = venue;
            return request;
        });
};
