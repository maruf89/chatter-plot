"use strict";

const logger = require('bragi');
const _ = require('lodash-node/modern');
const Q = require('q');

const Fetch = GLOBAL.Fetcher;

const DataBus = Fetch.service('DataBus');

const eventController = Fetch.controller('event');
const Event = Fetch.type('event');

/**
 * @description called upon a user being deleted. Removes any traces of the user
 *
 * @param {object} data
 * @param {string} data.source
 * @param {number} data.userID
 * @param {object} data.userData
 */
const deleteUserTask = function (data) {
    const userID = data.userID;

    attendingEvents(userID);
    hostingEvents(data)
};

DataBus.on('/user/deleted', deleteUserTask);
logger.log('init:controller:event', 'Listening for deleted users');

const hostingEvents = function (data) {
    // TODO build task that:
    //          - finds all events that this user is hosting & hasn't occured yet
    //          - notifies the attendees that the host has deleted his account & event is off
    //          - removes the event from all of the users attending list
    //          - deletes the event
    Event.deleteUsersEvents(data);
};

const attendingEvents = function (userID) {
    logger.log('impl:userDeleted:attendingEvents', 'Deleting all current events that the user is attending')
    return eventController.search({
        filterTypes: [
            'current',
            'notHosting',
            'attending',
        ],
        opts: {
            attending: userID,
            notHosting: userID,
        },
        size: 100,
    })
        .then(eventController.formatSearchResults)
        .then(function (results) {
            if (!results.total) {
                return true;
            }

            return Q.map(
                _.map(results.events, function (event) {
                    return eventController.unattendUser(event, userID);
                })
            );
        })
};