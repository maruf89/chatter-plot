/**
 * @module bImplEventNotification
 */

'use strict';

//const _ = require('lodash-node/modern');
const Q = require('q');
const moment = require('moment');
const logger = require('bragi');

const Fetch = GLOBAL.Fetcher;

const commonVenue = Fetch.common('util', 'venue', 'index');
const notificationCtrl = Fetch.controller('notification');
const crypter = Fetch.util('encryptDecrypt');
const Mandrill = Fetch.service('mandrill');
const Event = Fetch.type('event');
const EmailNotification = Fetch.service('EmailNotification');

const Notifier = EmailNotification.notifier;

const mandrillKey = Fetch.config('templates/mandrillKey', true, 'js')[Event.name];
const config = Fetch.config('config', true, 'js');

const format = {
    languages: function (languages) {
        var langs, last;

        if (!languages.length) {
            return '';
        }
        langs = languages.map(function (language) {
            if (language.toUpperCase() === 'ALL') return 'all languages';

            // capitalize the language
            return language[0].toUpperCase() + language.slice(1);
        });

        // if more than 1 language interject with 'And'
        if (languages.length > 1) {
            last = langs.pop();
            return langs.join(', ') + ' and ' + last;
        }

        return langs[0];
    },

    date: function (time) {
        time = time.substr(0, 15);
        return moment(time, 'YYYYMMDDTHHmmss').format('MMMM Do YYYY, h:mm a');
    },

    /**
     * @description Sets the value to how many other people (besides this user) are attending
     *
     * @param {object} event
     * @returns {(number|string)} returns either a number of other people or 'no' if 0
     */
    otherParticipants: function (event) {
        var length;

        // Verify that we have a going List and it's greater than 1
        if (Array.isArray(event.goingList) &&
            (length = event.goingList) > 1
        ) {
            // We do not include ourselves in 'You and X other people are going'
            return length - 1;
        }

        // We are the only ones going
        return 'no';
    },

    subDomain: function (user) {
        let locale = user.settings && user.settings.locale,
            subdomain = config.prefix;

        if (locale) {
            let substr = locale.substr(0, 2);
            if (config.subDomains.indexOf(substr) !== -1) {
                subdomain = substr;
            }
        }

        return subdomain;
    },
};

class EventMandrillFields extends EmailNotification.MandrillFields {
    constructor() {
        super();
    }

    EVENT_ID(data) {
        return {
            name: 'EVENT_ID',
            content: data.event.eID
        };
    }

    EVENT_TITLE(data) {
        return {
            name: 'EVENT_TITLE',
            content: data.event.name
        };
    }

    EVENT_DATE(data) {
        return {
            name: 'EVENT_DATE',
            content: format.date(data.event.when)
        };
    }

    EVENT_ADDRESS(data) {
        return {
            name: 'EVENT_ADDRESS',
            content: commonVenue.objToAddress(data.venueDetails)
        };
    }

    EVENT_LOCATION(data) {
        return {
            name: 'EVENT_LOCATION',
            content: commonVenue.addressObjToUrl('google', data.venueDetails)
        }
    }

    EVENT_DESCRIPTION(data) {
        return {
            name: 'EVENT_DESCRIPTION',
            content: data.event.description
        };
    }

    EVENT_TYPE(data) {
        return {
            name: 'EVENT_TYPE',
            content: data.event.indexType + 's'
        }
    }

    /**
     * TODO: this needs to be hooked up
     * @param data
     * @returns {{name: string, content: *}}
     * @constructor
     */
    EVENT_LANGUAGES(data) {
        return {
            name: 'EVENT_LANGUAGES',
            content: format.languages(data.languages)
        };
    }

    OTHER_PARTICIPANTS(data) {
        return {
            name: 'OTHER_PARTICIPANTS',
            content: format.otherParticipants(data.event)
        }
    }

    NOTIFY_MESSAGE(data) {
        return {
            name: 'NOTIFY_MESSAGE',
            content: data.notifyMessage || data.message
        }
    }
}

/**
 * @description notifies users by email & optionally notification
 *
 * @param {object} eventData - see EmailNotification.Notifier#emailNotifyUsers for options
 * @returns {Promise}
 */
exports.notify = function (eventData) {
    logger.log('req:impl:message_user:emailNotify', 'Notifying users by email');

    return Notifier.emailNotifyUsers(eventData, new EventMandrillFields())
        .catch(function (err) {
            logger.log('error:impl:event_user:notify',
                'Error emailing users using eventData:', eventData,
                '\nerror:\n', err
            );

            throw 500;
        });
};

/**
 * @description A map of possible Mandrill merge variables
 * Each field receives:
 *      @param {object} event - a full event object
 *      @param {object} data - data passed to the notifier method
 *      @returns {{name: string, content: string}}
 * @type {object}
 * @deprecated
 */
const mergeFields = {
    'EVENT_ID': function (event) {
        return {
            name: 'EVENT_ID',
            content: event.eID
        };
    },
    'EVENT_TITLE': function (event) {
        return {
            name: 'EVENT_TITLE',
            content: event.name
        };
    },
    'EVENT_DATE': function (event) {
        return {
            name: 'EVENT_DATE',
            content: format.date(event.when)
        };
    },
    'EVENT_DESCRIPTION': function (event) {
        return {
            name: 'EVENT_DESCRIPTION',
            content: event.description
        };
    },
    'env': function () {
        return {
            name: 'env',
            content: config.prefix
        };
    },
    'EVENT_LANGUAGES': function (event, data) {
        return {
            name: 'EVENT_LANGUAGES',
            content: format.languages(data.languages)
        };
    },

    /**
     * Sets the value to how many other people (besides this user) are attending
     */
    'OTHER_PARTICIPANTS': function (event) {
        return {
            name: 'OTHER_PARTICIPANTS',
            content: format.otherParticipants(event)
        };
    }

};

/**
 * @description notifies users by e-mail + notification of any event activity
 * TODO: Build in an exclude variable to exclude users from receiving notifications
 * @name bImplEventNotification#notifyUsers
 * @param {object} event
 * @param {object} data
 * @param {string} data.type - refers to template details in `mandrillKey.js` under `EVENT`
 * @param {array<string>} data.globalVars
 * @param {array<object>} data.users
 * @returns {*}
 * @deprecated
 */
exports.notifyUsers = function (event, data) {
    if (!data.users || !data.users.length) {
        return Q.resolve('No users to notify');
    }

    var emailOpts = mandrillKey[data.type],

        /**
         * Array of users to e-mail
         * @type {Array<object>}
         */
        users2Email = [],

        /**
         * Array of userIDs
         * @type {Array<number>}
         */
        notifyTo = [],

        notificationType = event.indexType.toUpperCase(),

        mandrillOptions = {
            track_clicks: true,
            subject: emailOpts.subject,

            // Build the global Variables
            global_merge_vars: data.globalVars.reduce(function (arr, string) {
                if (mergeFields[string]) {
                    arr.push(mergeFields[string](event, data));
                }
                return arr;
            }, []),

            // Build each users merge variable
            merge_vars: data.users.reduce(function (vars, user) {
                if (user.userID === event.creator) {
                    return vars;
                }

                notifyTo.push(user.userID);

                users2Email.push({
                    email: user.email,
                    name: user.firstName + ' ' + user.lastName
                });

                vars.push({
                    rcpt: user.email,
                    vars: [
                        {
                            name: 'FNAME',
                            content: user.firstName
                        }, {
                            name: 'emailKey',
                            content: crypter.encrypt(user.email)
                        }, {
                            name: 'env',
                            content: format.subDomain(user)
                        },
                    ]
                });
                return vars;
            }, [])
        };

    logger.log('req:impl:event_notification:notifyUsers', 'Notifying users of event activity');
    return Q.all([
        Mandrill.sendTemplate(emailOpts.template, [], users2Email, mandrillOptions),
        notificationCtrl.notify(notifyTo, notificationType, event.eID, {
            from: event.creator,
            sourceType: Event.notificationTypes[data.type],
            snippet: event.name
        })
    ])

    .then(function () {
        logger.log('res:impl:event_notification:notifyUsers', 'Successfully notified users of event activity');
        return 200;
    }).catch(function (err) {
        logger.log('error:impl:event_notification:notifyUsers', 'Error notifying users event activity\nerror\n', err);
        throw err;
    });
};