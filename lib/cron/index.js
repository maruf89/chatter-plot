'use strict';

const _ = require('lodash-node/modern');
const logger = require('bragi');
const schedule = require('node-schedule');

const Fetch = GLOBAL.Fetcher;
const PID = String(process.pid);

const config = Fetch.config('config', true, 'js');
const CRON_KEY = config.cronRedisToken;

const RULE = new schedule.RecurrenceRule();
let checkJob = null; // will store reference to the job upon initiation

const MINUTE = 4;
const MINUTE_MS = MINUTE * 60 * 1000;
const SECOND = 0;

const REDIS_EXPIRE = MINUTE + 1;
const REDIS_EXPIRE_MS = REDIS_EXPIRE * 60 * 1000;

const SCRIPTS = [
    Fetch.cron('eventReminder'),
    Fetch.cron('requestReminder'),
];

const JOBS = [];

RULE.minute = MINUTE;
RULE.second = SECOND;

/**
 * @description how many times this process should check whether it should be the sole CRON runner
 * @type {number}
 */
const CANDIDATE_ATTEMPTS = 3;

let Redis = null;
let checkTimer = null;
let initid = false;

/**
 * @description heartbeat check to make sure that no other process took over control
 * In the case another process takes over, this destroys itself
 */
const statusCheck = function () {
    logger.log('cron:index:statusCheck', 'heartbeat');
    Redis.pget(CRON_KEY).then(function (val) {
        if (val && val.toString() !== PID) {
            return exports.destroy();
        }

        Redis.set(CRON_KEY, PID, 'PX', REDIS_EXPIRE_MS);
    });

    checkTimer = setTimeout(statusCheck, MINUTE_MS);
};

/**
 * @description called when this process has been chosen as the sole runner
 * and initiates all of the CRON scripts
 */
const instantiate = function () {
    if (initid) { return; }

    initid = true;
    logger.log('cron:index:instantiate', 'process:', PID, 'delegated as the cron runner. Initiating…');


    setTimeout(statusCheck, MINUTE_MS);

    _.each(SCRIPTS, function (script) {
        JOBS.push(script.init());
    });
};

/**
 * @description enters this process as a candidate to run the CRON jobs
 * Will run a check for CANDIDATE_ATTEMPTS # of times to see if no other
 * process is running CRON and will take over the responsibility
 */
exports.init = function () {
    logger.log('cron:index:init', 'called');
    let timesToCheck = CANDIDATE_ATTEMPTS;
    let timer = null;

    Fetch.database('redis').onClientReady().then(function (client) {
        Redis = client;
        initCheck();
    });

    const initCheck = function () {
        logger.log('cron:index:init-initCheck', 'testing for candidacy:', timesToCheck);
        if (timer) {
            clearTimeout(timer);
        }

        if (!timesToCheck--) {
            return false;
        }

        timer = setTimeout(initCheck, MINUTE_MS);

        // Check if there's an existing process running CRON
        Redis.pget(CRON_KEY).then(function (val) {
            // If no process is in control then…
            if (!val) {
                // Set our own process as the key
                Redis.set(CRON_KEY, PID, 'PX', REDIS_EXPIRE_MS);

                // Lastly give it a half-second safety check to make
                // sure that no other process is competing with this one
                setTimeout(function () {
                    Redis.pget(CRON_KEY).then(function (val) {
                        // if our own process is still there then this process will
                        // be the designated CRON runner
                        if (val && val.toString() === PID) {
                            instantiate();
                            clearTimeout(timer);
                        }
                    });
                }, 500);
            }
        });
    };
};

/**
 * @description cancels all CRON tasks scheduled to run
 */
exports.destroy = function () {
    checkTimer && clearTimeout(checkTimer);

    logger.log('cron:index:destroy', 'cancelling tasks');
    _.each(JOBS, function (job) {
        job.cancel();
    });
};
