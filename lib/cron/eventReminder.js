"use strict";

const _ = require('lodash-node/modern');
const logger = require('bragi');
const schedule = require('node-schedule');
const Q = require('q');

const Fetch = GLOBAL.Fetcher;

const eventNotificationImpl = Fetch.impl('event_notification');
const eventUserImpl = Fetch.impl('event_user');
const eventController = Fetch.controller('event');
const Event = Fetch.type('event');

const rule = new schedule.RecurrenceRule();

const HOURS = 24;
const MINUTE = 30;
const SECOND = 0;
const EVERY = 6;

rule.hour = (new Array(Math.round(HOURS / EVERY) + 1))
    .join('0')
    .split('')
    .map(function (entry, index) {
        return EVERY * index;
    });

rule.minute = MINUTE;
rule.second = SECOND;

const buildQuery = function (size) {
    var aheadStart = HOURS - EVERY,
        now = new Date(),
        start = new Date(now.getTime() + (aheadStart * 60 * 60 * 1000)),
        end = new Date(start.getTime() + (EVERY * 60 * 60 * 1000));

    return {
        from: 0,
        size: size,
        sort: null,
        filterTypes: ['published', 'dateRange'],
        opts: {
            dateRange: [
                {
                    date: start,
                    operator: 'gte'
                }, {
                    date: end,
                    operator: 'lt'
                }
            ]
        },
        _source: [
            'name',
            'when',
            'until',
            'eID',
            'description',
            'venue',
            'goingList',
            'creator'
        ]
    };
};

const notifyAttendees = function (results) {
    return Q.when(results.hits.hits.reduce(function (arr, event) {
        var source = event._source;
        if (source.goingList && source.goingList.length) {
            source.indexType = event._type;
            arr.push(source);
        }
        return arr;
    }, [])).then(eventController.extendEventWithVenuesHost).then(function (events) {
        return Q.all(events.map(function (event) {
            const INDEX_TYPE = event.indexType.toUpperCase();
            const sourceType = 'REMINDER';

            return eventUserImpl.getEventUsers(event)
                .then(function (users) {
                    const notifyData = {
                        event: event,
                        users: users,
                        venueDetails: event.venueDetails,
                        type: INDEX_TYPE,
                        emailKey: sourceType,
                        objectID: event.eID,
                        notificationType: INDEX_TYPE,
                        notificationSourceType: Event.notificationTypes[sourceType],
                    };

                    return eventNotificationImpl.notify(notifyData);
                });
        }));
    });
};

const doSearch = function () {
    const size = 25;
    const delay = 1000 * 60 * 5;
    const query = buildQuery(size);


    return eventController.search(query).then(function (results) {
        var fn, i, iterations, j, ref;
        iterations = (results.hits.total - size) > 0 ? Math.ceil((results.hits.total - size) / size) : 0;
        fn = function (i) {
            var _i;
            _i = i + 1;
            return setTimeout(function () {
                query.from = _i * size;
                return eventController.search(query).then(notifyAttendees);
            }, _i * delay);
        };
        for (i = j = 0, ref = iterations; j <= ref; i = j += 1) {
            fn(i);
        }
        return notifyAttendees(results);
    });
};

exports.rule = rule;

exports.init = function () {
    logger.log('cron:eventReminder', 'inited');
    return schedule.scheduleJob(rule, doSearch);
};
