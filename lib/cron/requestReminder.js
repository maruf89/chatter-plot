"use strict";

const _ = require('lodash-node/modern');
const logger = require('bragi');
const schedule = require('node-schedule');

const Fetch = GLOBAL.Fetcher;

const interactionController = Fetch.controller('interaction');

const rule = new schedule.RecurrenceRule();

const HOURS = 24;
const MINUTE = 30;
const SECOND = 0;
const EVERY = 6;

rule.hour = (new Array(Math.round(HOURS / EVERY) + 1))
    .join('0')
    .split('')
    .map(function (entry, index) {
        return EVERY * index;
    });

rule.minute = MINUTE;
rule.second = SECOND;

const buildQuery = function (size) {
    var aheadStart = HOURS - EVERY,
        now = new Date(),
        start = new Date(now.getTime() + (aheadStart * 60 * 60 * 1000)),
        end = new Date(start.getTime() + (EVERY * 60 * 60 * 1000));

    return {
        type: 'REQUEST',
        from: 0,
        size: size,
        sort: null,
        filterTypes: ['requestOfStatus', 'dateRange'],
        opts: {
            ofStatus: 'accept',
            dateRange: [
                {
                    date: start,
                    operator: 'gte'
                }, {
                    date: end,
                    operator: 'lt'
                }
            ]
        },
        _source: [
            'type',
            'when',
            'participants',
            'reqID',
            'venue',
        ]
    };
};

const notifyUsers = function (results) {
    _.each(results.results, interactionController.sendReminder);
};

const cronFn = function () {
    const size = 25;
    const delay = 1000 * 60 * 5;
    const query = buildQuery(size);

    interactionController.search(query)
        .then(function (results) {
            if (!results.total) { return; }

            const iterations = (results.total - size) > 0 ?
                Math.ceil((results.total - size) / size)
                : 0;

            const iterateGroups = function (i) {
                let mult = i + 1;
                setTimeout(function () {
                    query.from = mult * size;
                    interactionController.search(query)
                        .then(notifyUsers);
                }, mult * delay);
            };

            for (let i = 0; i < iterations; i++) {
                iterateGroups(i + 1);
            }

            notifyUsers(results);
        });
};

exports.rule = rule;

exports.init = function () {
    logger.log('cron:requestReminder', 'inited');
    return schedule.scheduleJob(rule, cronFn);
};
