'use strict';

var Fetch = GLOBAL.Fetcher,

    _ = require('lodash-node/modern'),
    index = require('./controllers'),
    locale = require('locale'),

    supported = new locale.Locales(Object.keys(Fetch.common('config', 'locales').supported)),
    adminEvent = Fetch.controller('admin', 'event', 'index');


/*
 Application routes
 */
module.exports = function (app) {
    app.get('/partials/*', index.partials);
    app.get('/views/*', index.views);
    app.get('/thankyou-translators', function (req, res) {
        return res.redirect(301, '/translate');
    });

    app.use(locale(supported));

    app.use('/auth', Fetch.controller('routes', 'auth').router);
    app.use('/email', Fetch.controller('routes', 'email').router);
    app.use('/event', Fetch.controller('routes', 'event').router);
    app.use('/user', Fetch.controller('routes', 'user').router);

    app.post('/admin/event/bulkAdd', adminEvent.bulkAdd);

    return app.get('/*', index.index);
};
