// TODO: Subjects are no longer needed as the template supports them. todo - remove them from here and where they're used

var opts = {
    "EVENT": {
        "NEW": {
            "template": "new-event-notification",
            "subject": "New {{EVENT_LANGUAGES}} event near you!",
            "global_vars": [
                "EVENT_ID",
                "EVENT_TITLE",
                "EVENT_DATE",
                "EVENT_DESCRIPTION",
                "EVENT_LANGUAGES",
                "EVENT_TYPE",
            ]
        },
        "DELETED": {
            "template": "delete-event",
            "subject": "Event cancelled: \"{{EVENT_TITLE}}\"",
            "global_vars": [
                "EVENT_ID",
                "EVENT_TITLE",
                "EVENT_DATE",
                "EVENT_ADDRESS",
                "EVENT_LOCATION",
                "NOTIFY_MESSAGE",
            ]
        },
        "UPDATED": {
            "template": "event-updated",
            "subject": "Event update: {{EVENT_TITLE}} has been edited by the host"
        },
        "OPEN_SPOT": {
            "template": "event-open-spot",
            "subject": "Event update: \"{{EVENT_TITLE}}\" has an open spot!"
        },
        "REMINDER": {
            "template": "event-reminder",
            "subject": "Tomorrow you and {{OTHER_PARTICIPANTS}} others are going to \"{{EVENT_TITLE}}\"",
            "global_vars": [
                "EVENT_ID",
                "EVENT_TITLE",
                "EVENT_DATE",
                "EVENT_DESCRIPTION",
                "EVENT_ADDRESS",
                "OTHER_PARTICIPANTS",
                "EVENT_LOCATION",
                "EVENT_TYPE",
            ]
        },
        "NOTIFY_ATTENDEES": {
            "template": "event-notify-attendees",
            "subject": "New message from your event host for {{EVENT_TITLE}}",
            "global_vars": [
                "EVENT_ID",
                "EVENT_TITLE",
                "EVENT_DATE",
                "EVENT_ADDRESS",
                "EVENT_LOCATION",
                "NOTIFY_MESSAGE",
                "EVENT_TYPE",
            ]
        }
    },
    "MESSAGE": {
        "FEEDBACK": {
            "template": "internal-feedback-form",
            "subject": "Feedback Form | {{CATEGORY}}"
        },
        "CONTACT": {
            "template": "internal-contact-form",
            "subject": "Contact Form | {{CATEGORY}}"
        },
        "CONTACT_AUTORESPONDER": {
            "template": "contactresponse"
        },
        "ABUSE": {
            "template": "internal-abuse-form",
            "subject": "Abuse Form | {{CATEGORY}}"
        },
        "USER": {
            "template": "new-user-message",
            "subject": "New Message from {{FROM_NAME}}",
            "global_vars": [
                "FROM_NAME",
                "FROM_ID",
                "MSG_ID",
                "MESSAGE"
            ]
        },
        "REQUEST_TANDEM": {
            "template": "request-tandem",
            "subject": "{{FROM_NAME}} sent a Tandem request",
            "global_vars": [
                "FROM_NAME",
                "FROM_ID",
                "REQ_ID",
                "REQ_ID_SHORT",
                "OLD_REQ_ID_SHORT",
                "MESSAGE",
                "SENDER_PHOTO",
                "VENUE_NAME",
                "VENUE_FORMATTED",
                "VENUE_LINK",
                "WHEN"
            ]
        },
        "REQUEST_TANDEM_ACCEPT": {
            "template": "request-tandem-accept",
            "subject": "{{FROM_NAME}} accepted your Tandem request",
            "global_vars": [
                "FROM_NAME",
                "FROM_ID",
                "REQ_ID",
                "REQ_ID_SHORT",
                "MESSAGE",
                "SENDER_PHOTO",
                "VENUE_NAME",
                "VENUE_FORMATTED",
                "VENUE_LINK",
                "WHEN"
            ]
        },
        "REQUEST_TANDEM_DENY": {
            "template": "request-tandem-deny",
            "subject": "{{FROM_NAME}} {{if DENY}}denied{{else}}cancelled{{end}} {{if IS_CREATOR}}your{{else}}their{{end}} Tandem request",
            "global_vars": [
                "FROM_NAME",
                "FROM_ID",
                "REQ_ID",
                "REQ_ID_SHORT",
                "MESSAGE",
                "SENDER_PHOTO",
                "VENUE_NAME",
                "VENUE_FORMATTED",
                "VENUE_LINK",
                "WHEN",
                "DENY"
            ],
            "user_vars": [
                "IS_CREATOR"
            ]
        },
        "REQUEST_TANDEM_REMINDER": {
            "template": "request-tandem-reminder",
            "subject": "Tandem session reminder with {{FROM_NAME}}",
            "global_vars": [
                "FROM_NAME",
                "FROM_ID",
                "REQ_ID",
                "REQ_ID_SHORT",
                "MESSAGE",
                "SENDER_PHOTO",
                "VENUE_NAME",
                "VENUE_FORMATTED",
                "VENUE_LINK",
                "WHEN"
            ]
        }
    },
    "USER": {
        "CONFIRMATION": {
            "template": "confirmation",
            "redisKey": "redisConfirm"
        },
        "PASS_RESET": {
            "template": "password-reset",
            "redisKey": "redisPasswordReset"
        },
        "WELCOME": {
            "template": "default-welcome"
        },
        "locs01": {
            "template": "update-desired-locations",
            "subject": "We have a new feature at Chatterplot!"
        }
    }
};

opts.LISTING = opts.EVENT;

module.exports = opts;
