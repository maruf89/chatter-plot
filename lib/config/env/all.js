'use strict';
var config, path;

path = require('path');

module.exports = config = {
    root: process.cwd(),
    angularVersion: '1.3.16',
    uploadDirectory: '/tmp',
    displayPort: '',
    hostname: 'chatterplot.com',
    secretKey: '2th823tH0nte023nth22NO042u3hvzoeLrpylao_sS',
    token: {
        ttl: 7 * 24 * 60 * 60 * 1000
    },
    cronRedisToken: 'cp-cron',
    redis: {
        port: 6379,
        host: 'localhost',
        options: {
            return_buffers: true
        }
    },
    elasticsearch: {
        port: 9200,
        host: 'localhost',
        options: {}
    },
    sitemap: process.cwd() + '/sitemap',
    defaults: {
        permissions: {
            "default": 'non_activated',
            activated: 'basic'
        }
    },
    defaultPermissionsGroup: 'non_activated',
    onSignup: {
        setPassword: false
    },
    releases: ['paidTags'],
    dynamicVars: {},
    forceAnalytics: false,
    emails: {
        feedback: 'support@chatterplot.com',
        contact: 'info@chatterplot.com',
        abuse: 'abuse@chatterplot.com'
    },
    messaging: {
        emailAfterXSeconds: 24 * 60 * 60 // 1 day
    },
    subDomains: [
        'eo',
        'ru',
        'fr',
        'de',
        'zh',
        'tw',
        'es',
    ],

    // Corresponds to any font families to use OTHER than the default: latin
    // http://i.imgur.com/mJQc6r7.png
    googleFontFamily: {
        'ru': 'cyrillic',
    },
};
