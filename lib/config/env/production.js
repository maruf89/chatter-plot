'use strict';

var path = require('path');

module.exports = {
    env: 'production',
    port: process.env.HTTP_PORT || 4000,
    prefix: 'www',
    directory: './',
    cdnBase: 'https://www.chatterplot.com/',
    sitemap: path.resolve(process.cwd(), '..', 'sitemap'),
};
