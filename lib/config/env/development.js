'use strict';

var config = {
    env: 'development',
    port: process.env.HTTP_PORT || 9000,
    prefix: 'dev',
    directory: './app',
    cdnBase: 'https://dev.chatterplot.com',
    hostname: '0.0.0.0',
    sitemap: process.cwd() + '/app/sitemap',
    elasticsearch: {
        options: {
            log: 'trace'
        }
    },
    forceAnalytics: false,
    emails: {
        feedback: 'marius@chatterplot.com',
        contact: 'marius@chatterplot.com'
    }
};

//config.cdnBase = config.cdnBase + ':' + config.httpsPort + '/';

module.exports = config;
