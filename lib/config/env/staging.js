'use strict';
var config;

module.exports = config = {
    env: 'staging',
    port: process.env.HTTP_PORT || 4000,
    cdnBase: 'https://staging.chatterplot.com/',
    prefix: 'staging',
    hostname: '0.0.0.0',
    directory: './app',
    releases: {
        dashboard: true
    },
    elasticsearch: {
        options: {
            log: 'trace'
        }
    },
    forceAnalytics: false
};
