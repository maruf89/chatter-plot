'use strict';

/**
 * Adds a userID to an events going list
 * @type {string}
 */
exports.appendGoing = 'if (!ctx._source.containsKey("goingList") || ctx._source.goingList == null) { ctx._source.goingList = newField } else if (!ctx._source.goingList.contains(newField[0])) { ctx._source.goingList += newField }';

/**
 * Adds a userID to an events wait list
 * @type {string}
 */
exports.appendToWaitlist = 'if (!ctx._source.containsKey("waitList") || ctx._source.waitList == null) { ctx._source.waitList = newField } else if (!ctx._source.waitList.contains(newField[0])) { ctx._source.waitList += newField }';

/**
 * Removes a user from an events userID going list if they exist, if not it checks the wait list
 * @type {string}
 */
exports.removeGoing = 'if (ctx._source.goingList != null && ctx._source.goingList.contains(user_id)) { ctx._source.goingList.remove((Object) user_id) } else if (ctx._source.waitList != null && ctx._source.waitList.contains(user_id)) { ctx._source.waitList.remove((Object) user_id) }';