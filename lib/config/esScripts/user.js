'use strict';

/**
 * Adds an event ID to a users attending.events array
 * @type {string}
 */
exports.updateGoing =
    'if (!ctx._source.attending.containsKey("events") || ctx._source.attending.events == null) {' +
        'ctx._source.attending.events = event_id' +
    '} else if (!ctx._source.attending.events.contains(event_id[0])) {' +
        'ctx._source.attending.events += event_id' +
    '}';

/**
 * Removes an event ID from a users attending.events array
 * @type {string}
 */
exports.updateNotGoing =
    'if (ctx._source.attending.events != null && ctx._source.attending.events.contains(event_id)) {' +
        'ctx._source.attending.events.remove((Object) event_id)' +
    '} else if (ctx._source.attending.eventsWaitList != null && ctx._source.attending.eventsWaitList.contains(event_id)) {' +
        'ctx._source.attending.eventsWaitList.remove((Object) event_id)' +
    '}';

/**
 * Adds an event ID to a user's attending.eventsWaitList array
 * @type {string}
 */
exports.updateGoingWaitlist =
    'if (!ctx._source.attending.containsKey("eventsWaitList") || ctx._source.attending.eventsWaitList == null) {' +
        'ctx._source.attending.eventsWaitList = event_id' +
    '} else if (!ctx._source.attending.eventsWaitList.contains(event_id[0])) {' +
        'ctx._source.attending.eventsWaitList += event_id' +
    '}';

/**
 * Adds a user to the affected users favorites for a specific folder
 * @type {string}
 */
exports.addToFavorite =
    'if (!ctx._source.favorite.containsKey(folder))' +
        '{ ctx._source.favorite[folder] = [userID]' +
    '} else if (!ctx._source.favorite[folder].contains((Object) userID)) {' +
        'ctx._source.favorite[folder] += userID' +
    '}';

/**
 * Removes a user from the affected users favorites for a specific folder
 * @type {string}
 */
exports.removeFromFavorite =
    'if (ctx._source.favorite.containsKey(folder) && ctx._source.favorite[folder].contains(userID)) {' +
        'ctx._source.favorite[folder].remove((Object) userID)' +
    '}';

/**
 * Bulk removes a user from the affected users favorites for a specific folder
 * @type {string}
 */
exports.bulkRemoveFromFavorites =
    'if (ctx._source.favorite.containsKey(folder)) {' +
        'targetIDs.each {' +
            'if (ctx._source.favorite[folder].contains((Object) it)) {' +
                'ctx._source.favorite[folder].remove((Object) it)' +
            '}' +
        '}' +
    '}';

exports.featureSeen =
    'if (ctx._source.features && ctx._source.features.contains(featureName)) {' +
        'ctx._source.features.remove(featureName);' +
    '}';

exports.featureAdd =
    'if (ctx._source.features) {' +
        'if (!ctx._source.features.contains(featureName)) {' +
            'ctx._source.features += featureName;' +
        '}' +
    '} else {' +
        'ctx._source.features = [featureName]' +
    '}';
