'use strict';

/*
Load environment configuration
 */
module.exports = require('lodash-node/modern').merge(
    require('./env/all'),
    require("./env/" + process.env.NODE_ENV) || {}
);
