'use strict';

const path = require('path');
const config = require('./config');
const session = require('express-session');
const RedisStore = require('connect-redis')(session);
const bodyParser = require('body-parser');

/**
 * @namespace express
 * @param app
 */
module.exports = function (app) {
    const sourcePath = 'public',
        env = process.env.NODE_ENV || 'development';

    app.use(require('compression')());
    app.set('env', env);

    if (env === 'development' || env === 'staging') {
        let serveStatic = require('serve-static');

        // Disable caching of scripts for easier testing
        app.use(function (req, res, next) {
            if (req.url.indexOf("/scripts/") === 0 ||
                req.url.indexOf("/styles/") === 0 ||
                req.url.indexOf("/public/") === 0
            ) {
                res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
                res.header('Pragma', 'no-cache');
                res.header('Expires', 0);
            }
            return next();
        });

        app.use(require('morgan')('dev'));

        /**
         * Until I get passenger working with nginx we have to manually set our static paths
         */
        app.use('/sitemap', serveStatic(path.join(config.root, 'sitemap')));
        app.use('/styles', serveStatic(path.join(config.root, sourcePath, 'styles')));
        app.use('/scripts', serveStatic(path.join(config.root, sourcePath, 'scripts')));
        app.use('/third_party', serveStatic(path.join(config.root, sourcePath, 'third_party')));
        app.use('/bower_components', serveStatic(path.join(config.root, sourcePath, 'bower_components')));
        app.use('/fonts', serveStatic(path.join(config.root, sourcePath, 'fonts')));
        app.use('/images', serveStatic(path.join(config.root, sourcePath, 'images')));
        app.use('/map', serveStatic(path.join(config.root, sourcePath, 'map')));
        app.use('/languages', serveStatic(path.join(config.root, sourcePath, 'languages')));
        app.use('/views', serveStatic(path.join(config.root, 'views')));
        app.use('/m', serveStatic(path.join(config.root, sourcePath, 'm')));
        app.use('/misc', serveStatic(path.join(config.root, sourcePath, 'misc')));

        app.set('views', path.join(config.root, 'views'));
        app.set('json spaces', 4);
    }

    if (env === 'production') {
        app.set('views', path.join(config.root, 'views'));
    }

    app.engine('html', require('jade').__express);
    app.set('view engine', 'jade');
    app.use(bodyParser.urlencoded({ extended: true }));

    app.use(bodyParser.json());
    app.use(require('method-override')());
    app.use(require('cookie-parser')(config.secretKey));
    app.use(session({
        store: new RedisStore(),
        secret: config.secretKey,
        resave: true,
        saveUninitialized: true
    }));

    app.use(require('prerender-node')
        .set('protocol', 'https')
        .set('prerenderToken', 'v4p9oOzqxMd9A4JRrGRr'));

    config.dynamicVars.deployVersion = Math.ceil((new Date).getTime() / 300000) * 300000;
};