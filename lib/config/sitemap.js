'use strict';
var DataBus, Fetch, MultiSitemap, Sitemap, _, config, flush, flushEvery, instance,
    instantiated, logger, pages, timer;

MultiSitemap = require('node-multi-sitemap');

_ = require('lodash-node/modern');

logger = require('bragi');

Fetch = GLOBAL.Fetcher;

Sitemap = exports;

DataBus = Fetch.service('DataBus');

config = Fetch.config('config', true, 'js');

pages = Fetch.config('misc/sitemapPages');

flushEvery = 60 * 1000;

instance = null;

timer = null;

exports.init = function () {
    var siteURL;
    siteURL = 'https://' + config.prefix + '.' + config.hostname + config.displayPort;
    instance = new MultiSitemap(siteURL, config.sitemap);
    instantiated(instance);
    return _.each(pages, instance.addSitemap, instance);
};

instantiated = function (instance) {
    Sitemap.instance = instance;
    DataBus.on('/sitemap/add', function (args) {
        return Sitemap.add.apply(Sitemap, args);
    });

    // initiate the timer in the flush 
    return flush();
};

exports.flush = flush = function () {
    clearTimeout(timer);
    instance.flush();
    return timer = setTimeout(flush, flushEvery);
};

exports.add = function () {
    logger.log('sitemap:add', 'adding to sitemap');
    return instance.add.apply(instance, arguments);
};
