'use strict';
const _ = require('lodash-node/modern');
const Q = require('q');
const socketioJwt = require('socketio-jwt');
const logger = require('bragi');

const Fetch = GLOBAL.Fetcher;

const config = Fetch.config('config', true, 'js');
const permissions = Fetch.service('permissions');
const SocketIo = Fetch.connection('socket.io');
const jwtFilter = Fetch.filter('jwt');

logger.log('init:s-socket:routes', 'init');

const socketControllers = [
    Fetch.socket('user'),
    Fetch.socket('email'),
    Fetch.socket('venue'),
    Fetch.socket('event'),
    Fetch.socket('account'),
    Fetch.socket('admin'),
    Fetch.socket('message'),
    Fetch.socket('notification'),
    Fetch.socket('favorite'),
    Fetch.socket('interaction'),
];

module.exports = function (io) {
    SocketIo.init(io, false);
    io.use(socketioJwt.authorize({
        secret: config.secretKey,
        handshake: true,
        success: jwtFilter.verifyToken
    }));

    return io.on('connection', function (socket) {
        const userID = socket.decoded_token.userID;
        logger.log('s-socket:routes', 'connection socketID:', socket.id, ' userID: ', userID);

        SocketIo.saveConnection(socket, userID);

        logger.log('req:s-socket:routes:getRoles', 'Getting user permissions');
        // Bind user permissions to socket handle then bind routes
        return permissions.getRoles(userID)

        .then(function (roles) {
            logger.log('res:s-socket:routes:getRoles', 'Received user permissions. Now binding listeners');

            // Save the permission
            socket.userPermissions = roles;

            // Iterate over each controller and bind it
            return _.each(socketControllers, function (controller) {
                return controller.onSecureSocketConnection.call(socket);
            });
        }).catch(function (err) {
            var error;

            if (err === 404) {
                error = {
                    message: 'User doesn\'t exist',
                    response: 404,
                    type: 'updates.ERROR-USER_NOT_FOUND'
                };
            } else {
                error = {
                    message: 'Error retrieving user permissions',
                    response: 501,
                    type: 'updates.ERROR-DATABASE_ERROR'
                };
            }
            logger.log('error:s-socket:routes:getRoles', error.message);
            return socket.emit('/disconnect/user', error);
        });
    });
};
