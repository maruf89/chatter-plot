'use strict';

const _ = require('lodash-node/modern');
const logger = require('bragi');

const Fetch = GLOBAL.Fetcher;

const SocketIo = Fetch.connection('socket.io');

logger.log('init:us-socket:routes', 'init');

const socketControllers = [
    Fetch.socket('api'),
    Fetch.socket('user'),
    Fetch.socket('email'),
    Fetch.socket('event'),
    Fetch.socket('auth'),
    Fetch.socket('venue'),
    Fetch.socket('pubSub'),
];

module.exports = function (io) {
    SocketIo.init(io, true);
    return io.on('connection', function (socket) {
        logger.log('us-socket:routes', 'connection', socket.id);
        _.each(socketControllers, function (controller) {
            controller.onSocketConnection.call(socket);
        });
    });
};
