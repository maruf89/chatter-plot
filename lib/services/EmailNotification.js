"use strict";

const Q = require('q'),
    _ = require('lodash-node/modern'),
    moment = require('moment'),
    logger = require('bragi'),

    Fetch = GLOBAL.Fetcher,

    notificationController = Fetch.controller('notification'),
    crypter = Fetch.util('encryptDecrypt'),
    Emailer = Fetch.service('EmailAdapter'),
    Event = Fetch.type('event'),
    config = Fetch.config('config', true, 'js'),

    mandrillSource = Fetch.config('templates/mandrillKey', true, 'js'),

    defaultUserVars = [
        'env',
        'emailKey',
        'FNAME'
    ];

exports.formatFunctions = {
    date: function (time) {
        time = time.substr(0, 15);
        return moment(time, 'YYYYMMDDTHHmmss').format('MMMM Do YYYY, h:mm a');
    },

    subDomain: function (user) {
        let locale = user.settings && user.settings.locale,
            subdomain = config.prefix;

        if (locale) {
            let substr = locale.substr(0, 2);
            if (config.subDomains.indexOf(substr) !== -1) {
                subdomain = substr;
            }
        }

        return subdomain;
    },
};

/**
 * @class
 */
class MandrillFields {
    constructor(formatFunctions) {
        this.config = config;

        this.format = formatFunctions || exports.formatFunctions;
    }

    /**
     * @description builds mandrill variables
     * @param {array<string>} variables - list of keys to add
     * @param {string} globalOrUser - one of (globalBase|userBase)
     * @param {object} mainArg
     * @param {object=} arg2 - optional second argument to pass
     */
    build(variables, globalOrUser, mainArg, arg2) {
        let baseArray = this.dynamic(mainArg, globalOrUser);

        if (!variables) {
            return baseArray;
        }

        return _.reduce(variables, function (arr, check) {
            let toLower = null;

            // check if there's a method to get the variable
            if (this[check]) {
                arr.push(this[check](mainArg, arg2));
            } else if (mainArg.hasOwnProperty(toLower = check.toLowerCase())) {
                // Otherwise check whether something like 'NAME' matches `mainArg.name`
                arr.push({
                    name: check,
                    content: mainArg[toLower]
                });
            }

            return arr;
        }, baseArray, this);
    }

    /**
     * @description builds a base variables object froma key:pair map
     *
     * @example
     *      // mandrillify an object
     *      mainArg.globalBase = {
     *          DISPLAY_TYPE: 'Message/Conversation',
     *          SUBTYPE: 'messages',
     *      }
     *
     * @param {object} mainArg
     * @param {string} globalOrUser - one of (globalBase|userBase)
     * @returns {array}
     */
    dynamic(mainArg, globalOrUser) {
        if (globalOrUser && mainArg[globalOrUser]) {
            return _.map(mainArg[globalOrUser], function (val, key) {
                return {
                    name: key,
                    content: val
                };
            }) || [];
        }

        return [];
    }

    env(user) {
        return {
            name: 'env',
            content: this.format.subDomain(user)
        };
    }

    emailKey(user) {
        return {
            name: 'emailKey',
            content: crypter.encrypt(user.email)
        };
    }

    FNAME(user) {
        return {
            name: 'FNAME',
            content: user.firstName
        };
    }
}

exports.MandrillFields = MandrillFields;

class Notifier {
    /**
     * @description notifies users by e-mail + notification of any event activity
     * TODO: Build in an exclude variable to exclude users from receiving notifications
     * @name Notifier#emailNotifyUsers
     * @param {object} data
     * @param {string} data.type - refers to the top level properties in `mandrillKey.js`
     * @param {string} data.emailKey - refers to the second level property type below data.index in `mandrillKey.js`
     * @param {array<object>} data.users
     * @param {string=} data.objectID -
     * @param {string=} data.notificationType - string corresponding to each types available types (EVENT|USER|INTERACTION…)
     * @param {number=} data.notificationSourceType - which sub type
     * @param {array<string>=} data.globalVars - @deprecated
     * @param {MandrillFields}
     * @returns {*}
     */
    emailNotifyUsers(data, fieldsInstance) {
        // Only notify in development
        if (process.env.NODE_ENV === 'development' && data.globalVars) {
            logger.log('error:deprecated:EmailNotification',
                'the use of data.globalVars is deprecated in favor of setting',
                'the values in mandrillKey.js as global_vars'
            );
        }

        if (!data.users || !data.users.length) {
            return Q.resolve('No users to notify');
        }

        logger.log('service:EmailNotification:emailNotifyUsers', 'called');
        if (!fieldsInstance) {
            fieldsInstance = new MandrillFields(exports.formatFunctions);
        }

        const emailOpts = mandrillSource[data.type][data.emailKey];

        if (typeof data.userVars === 'undefined' &&
            typeof emailOpts.user_vars === 'undefined'
        ) {
            data.userVars = defaultUserVars;
        } else if (emailOpts.user_vars) {
            data.userVars = _.extend(data.userVars, defaultUserVars);
        }

        // besides holding the type, this variable is used to check whether we're sending a notification also
        const notificationType = data.notificationSourceType && data.objectID && data.notificationType;

        /**
         * @description if we have a notification type then it will be an
         * Array of userIDs - otherwise an object with a push: noops function
         * @type {array<number>}
         */
        const notifyTo = notificationType ? [] : { push: function () {} };

        /**
         * Array of users to e-mail
         * @type {array<object>}
         */
        const users2Email = [];

        const globalVars = emailOpts.global_vars || data.globalVars;

        const mandrillOptions = {
            track_clicks: true,
            subject: emailOpts.subject,

            // Build the global Variables
            global_merge_vars: fieldsInstance.build(globalVars, 'globalBase', data),

            // Build each users merge variable
            merge_vars: data.users.reduce(function (vars, user) {
                notifyTo.push(user.userID);

                users2Email.push({
                    email: user.email,
                    name: user.firstName + ' ' + user.lastName
                });

                vars.push({
                    rcpt: user.email,
                    vars: fieldsInstance.build(data.userVars, 'userBase', user, data)
                });

                return vars;
            }, [])
        };

        const calls = [];

        if (data.mandrillOptions) {
            _.extend(mandrillOptions, data.mandrillOptions);
        }

        logger.log('req:service:EmailNotification:emailNotifyUsers', 'emailing user(s)');
        calls.push(Emailer.sendTemplate(emailOpts.template, [], users2Email, mandrillOptions));

        if (notificationType) {
            calls.push(notificationController.notify(notifyTo, notificationType, data.objectID, {
                from: data.fromID,
                sourceType: data.notificationSourceType,
                snippet: data.snippet,
                when: data.when
            }))
        }

        logger.log('req:service:EmailNotification:emailNotifyUsers', 'Notifying users');
        return Q.all(calls)
            .then(function () {
                logger.log('req:service:EmailNotification:emailNotifyUsers', 'Successfully notified users of event activity');
                return 200;
            }).catch(function (err) {
                logger.log('error:service:EmailNotification:emailNotifyUsers', 'Error notifying users event activity\nerror\n', err);
                throw err;
            });
    }
}

exports.Notifier = Notifier;

exports.notifier = new Notifier();
