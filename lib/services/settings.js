'use strict';

const _ = require('lodash-node/modern'),
    Q = require('q'),
    logger = require('bragi'),
    moment = require('moment'),

    Fetch = GLOBAL.Fetcher,

    config = Fetch.config('config', true, 'js'),
    userSettings = Fetch.config('userSettings'),
    esHelper = Fetch.util('elasticsearch', 'userHelper'),
    Constants = Fetch.config('misc/constants');

logger.log('init:service:settings', 'init');

class Settings {
    generateSettings(opts) {
        logger.log('init:service:settings:generateNewUser', 'called');
        var settings = _.cloneDeep(userSettings);

        // extend settings if options passed in 
        if (opts) {
            settings = _.extend(settings, opts);
        }

        return settings;
    }
}

module.exports = new Settings();
