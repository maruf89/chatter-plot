'use strict';

/**
 * This service differs from the controller in that it
 * provides services for creating confirmation emails,
 * lost passwords and more...
 */
const Q = require('q'),
    _ = require('lodash-node/modern'),
    StringDecoder = require('string_decoder').StringDecoder,
    logger = require('bragi'),
    
    Fetch = GLOBAL.Fetcher,
    
    config = Fetch.config('config', true, 'js'),
    Redis = Fetch.database('redis').client,

    User = Fetch.type('user'),
    Emailer = Fetch.service('EmailAdapter'),
    jwtHelper = Fetch.util('jwtHelper'),
    crypter = Fetch.util('encryptDecrypt'),
    sqlUser = Fetch.util('mysql', 'userHelper'),
    Util = Fetch.util('utility'),
    
    
    /**
     * Redis key pattern
     * @type {String}
     */
    _confirmPattern = 'user:verify:',
    
    _passwordResetPattern = 'user:passReset:',
    
    
    /**
     * Correllates to #_tokenEmail
     * @type {Object}
     */
    _tokenEmailTypes = {
        confirmation: {
            template: 'confirmation',
            redisKey: 'redisConfirm',
            tokenMethod: 'tokenFromUser'
        },
        passwordReset: {
            template: 'password-reset',
            redisKey: 'redisPasswordReset',
            tokenMethod: 'sign'
        },
        welcome: {
            template: 'default-welcome'
        }
    };


class Email {
    constructor() {
        logger.log('init:service:email', 'init');
    }

    /**
     * Returns the redis key for the user
     *
     * @param  {number} userID
     * @return {string}         the full redis key
     */
    redisConfirm(userID) {
        return _confirmPattern + userID;
    }

    redisPasswordReset(userID) {
        return _passwordResetPattern + userID;
    }

    /**
     * @description Does the action of actually sending the template e-mails
     *
     * @private
     * @param  {object}           userData   Contains:
     * @option userData {string}  email
     * @option userData {number}  userID
     * @option userData {String=} firstName  Optional as some templates may use it
     * @param  {string}           emailType  Types corresponding to `_tokenEmailTypes`
     * @param  {Object=}          opts       Can contain `additionalMergeVars` property with U know what
     * @return {Promise}
     */

    _templateEmail(userData, emailType, opts) {

        if (opts == null) {
            opts = {};
        }

        logger.log('service:email:_templateEmail', 'called');
        let contentTemplates = opts.contentTemplates || [],
            options = opts.options,
            emailAction = _tokenEmailTypes[emailType];

        if (!options) {
            let subdomain = (function (user) {
                let locale = user.settings && user.settings.locale,
                    subdomain = config.prefix;

                if (locale) {
                    let substr = locale.substr(0, 2);
                    if (config.subDomains.indexOf(substr) !== -1) {
                        subdomain = substr;
                    }
                }

                return subdomain;
            })(userData);

            let mergeVars = {
                rcpt: userData.email,
                vars: [{
                    name: 'FNAME',
                    content: userData.firstName || ''
                }, {
                    name: 'emailKey',
                    content: crypter.encrypt(userData.email)
                }, {
                    name: 'env',
                    content: subdomain
                }]
            };

            // Concatenate additional merge variables if passed in
            if (Array.isArray(opts.additionalMergeVars)) {
                mergeVars.vars = mergeVars.vars.concat(opts.additionalMergeVars);
            }

            options = {
                track_clicks: true,
                merge_vars: [mergeVars]
            };
        }

        logger.log('req:service:email:_templateEmail', 'Sending out template email');
        return Emailer.sendTemplate(emailAction.template, contentTemplates, userData.email, options);
    }

    /**
     * @description Sends out an email wih a JSON webtoken to the specified template with the specified data
     * @private
     * @param  {object}           userData   Contains:
     * @option userData {string}  email
     * @option userData {number}  userID
     * @option userData {String=} firstName  Optional as some templates may use it
     * @param  {string}           emailType  Types corresponding to `_tokenEmailTypes`
     * @param  {Object=}          opts       Can contain `additionalMergeVars` property with U know what
     * @return {Promise}                     Returns {object} with `token` - JSON Web token, `key` - Redis key, and `userData`
     */
    _tokenEmail(userData, emailType, opts) {

        if (opts == null) {
            opts = {};
        }

        logger.log('service:email:_tokenEmail', 'called');
        let self = this,
            expMin = opts.expMin || 48 * 60,
            emailAction = _tokenEmailTypes[emailType],
            verifyMethod = emailAction.tokenMethod,
            verifyToken;

        // Get the token
        return Q.when(jwtHelper[verifyMethod]({
            email: userData.email,
            userID: userData.userID
        }, expMin)).catch(function (err) {
            logger.log('error:service:email:_tokenEmail', 'creating token', err);
            throw err;
        })

        .then(function (_verifyToken) {
            opts.additionalMergeVars = opts.additionalMergeVars || [];
            opts.additionalMergeVars.push({
                name: 'token',
                content: verifyToken = _verifyToken
            });

            logger.log('req:service:email:_tokenEmail', 'Sending out activation email');
            return self._templateEmail(userData, emailType, opts)
        }).catch(function (err) {
            logger.log('error:service:email:_tokenEmail', 'trying to send email', err);
            throw err;
        })

        .then(function () {
            logger.log('res:service:email:_tokenEmail',
                'Activation email successfully sent - Saving verification token in Redis with 24 hour expiry');

            let redisKey = self[emailAction.redisKey](userData.userID);

            Redis.set(redisKey, verifyToken, 'EX', expMin * 60);

            return {
                token: verifyToken,
                key: redisKey,
                userData: userData
            };
        }).catch(function (err) {
            logger.log('error:service:email:_tokenEmail',
                'Error sending email to:', userData.email,
                'with options:\n', options, err);
            throw err;
        });
    }

    /**
     * @description Upon user creation, will generate a 1 time verification,
     * send it to the user and save it in Redis for 24 hours
     *
     * @param  {object} userData    Object containing `email` property
     */
    activation(userData, opts) {
        logger.log('req:service:email:activation', 'Sending email activation');

        return this._tokenEmail(userData, 'confirmation', opts)
            .then(function () {
            }, function (err) {
                logger.log('error:service:email:activation', 'Error sending email activation');
                throw err;
            });
    }

    /**
     * @description Send welcome e-mail to user
     *
     * @param  {object}  userData
     * @return {Promise}
     */
    welcomeEmail(userData) {
        logger.log('req:service:email:welcomeEmail', 'Sending `default-welcome` email template to user');

        return this._templateEmail(userData, 'welcome')
            .then(function (res) {
                return logger.log('res:service:email:welcomeEmail', 'successfully sent welcome e-mail');
            }, function (err) {
                return logger.log('error:service:email:welcomeEmail',
                    'Error sending `default-welcome` email template to:',
                    userData, '\nerror:', err);
            });
    }

    /**
     * @description Sends password reset email
     *
     * @param  {object} userData  Contains `email` & `userID` of user
     * @return {Promise}
     */
    resetPassword(userData) {
        logger.log('req:service:email:resetPassword',
            'Sending password reset email');

        return this._tokenEmail(userData, 'passwordReset')
            .then(function () {
                return true;
            }, function (err) {
                logger.log('error:service:email:activation', 'Error sending password reset');
                throw err;
            });
    }

    /**
     * @description Confirms a JSON Webtoken
     *
     * @param  {string} token       JSON Webtoken
     * @param  {string} emailType   Type of webtoken action to confirm against
     * @return {Promise<object>}     Response Object contains {string} `redToken` - Redis token & {object} `userData`
     */
    _tokenConfirm(token, emailType) {
        var decoded, emailAction, self, tokenKey, userID;

        logger.log('service:email:_tokenConfirm', 'called');

        self = this;
        decoded = jwtHelper.decode(token);

        if (!(_.isPlainObject(decoded) && (userID = decoded.userID))) {
            logger.log('warning:service:email:_tokenConfirm',
                'Invalid token missing `userID` - decoded object:', decoded);

            return Q.reject({
                type: 'auth.WARNING-INVALID_VERIFICATION',
                response: 405,
                errType: 'invalid_link'
            });
        }

        emailAction = _tokenEmailTypes[emailType];

        // Generate token
        tokenKey = self[emailAction.redisKey](userID);
        logger.log('req:service:email:_tokenConfirm', 'Getting matching token from Redis');

        return Redis.pget(tokenKey)
            .then(function (redToken) {
                logger.log('res:service:email:_tokenConfirm', 'Redis returned with a response');

                if (!redToken) {
                    logger.log('warning:service:email:_tokenConfirm',
                        'Verification code expired');
                    throw {
                        type: 'auth.WARNING-VERIFICATION_EXPIRED',
                        response: 405,
                        errType: 'expired_token'
                    };
                }

                let decoder = new StringDecoder('utf8');
                redToken = decoder.write(redToken);

                if (token !== redToken) {
                    logger.log('warning:service:email:_tokenConfirm',
                        'Invalid Verification Code');
                    throw {
                        type: 'auth.WARNING-INVALID_VERIFICATION',
                        response: 405,
                        errType: 'invalid_link'
                    };
                }

                logger.log('service:email:_tokenConfirm', 'Returning valid token');

                return {
                    tokenKey: tokenKey,
                    userData: decoded
                };
            });
    }

    /**
     * @description Verifies a token activated by a confirmation e-mail
     *
     * @param  {string} token       JSON Webtoken to verify
     * @return {Promise<object>}     Response Object contains {string} `redToken` - Redis token & {object} `userData`
     */

    verifyConfirmToken (token) {
        logger.log('req:service:email:verifyConfirmToken', 'Confirming user');
        return this._tokenConfirm(token, 'confirmation');
    }

    /**
     * @description Confirms password reset link
     *
     * @param  {string} token       JSON Webtoken to verify
     * @return {Promise<object>}     Response Object contains {string} `redToken` - Redis token & {object} `userData`
     */

    passwordResetConfirmed(token) {
        logger.log('service:email:passwordResetConfirmed', 'called');
        logger.log('req:service:email:passwordResetConfirmed',
            'Confirming user');
        return this._tokenConfirm(token, 'passwordReset');
    }
}

module.exports = new Email();
