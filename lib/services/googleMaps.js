"use strict";

const NAME = 'google',

    _ = require('lodash-node/modern'),
    gmAPI = require('googlemaps'),
    logger = require('bragi'),
    Q = require('q'),

    Fetch = GLOBAL.Fetcher,

    credentials = Fetch.config('socialServices')[NAME],
    commonVenue = Fetch.common('util', 'venue', 'index');

let Client;

let init = function () {
    Client = new gmAPI({
        key: credentials.serverKey,
        secure: true,
    });

    // Register the google get place api method
    commonVenue.serviceConfig(NAME, {
        getPlace: exports.getPlace,
        Q: Q,
    });
};

/**
 * @description loads a google place
 *
 * @param {string} sourceID - a google placeId
 * @returns {Promise.<object>}
 */
exports.getPlace = function (sourceID) {
    const deferred = Q.defer();

    Client.placeDetails({
        placeid: sourceID
    }, function (err, res) {
        if (err || res.status !== 'OK')
            return deferred.reject(err || res);

        return deferred.resolve(res.result);
    });

    return deferred.promise;
};

/**
 * @description gets an image url from a picture object
 *
 * @param {object} picture
 * @param {string} picture.photo_reference - photo id
 * @param {number} picture.maxWidth
 * @param {number} picture.maxHeight
 */
exports.photoSrc = function (picture) {
    const deferred = Q.defer();

    // TODO build out using https://developers.google.com/places/web-service/photos

    return deferred.promise;
};

_.defer(init);
