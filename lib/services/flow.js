var Combiner, EventEmitter, Flow, Stream, _, cleanIdentifier, combiners, config,
    formidable, fs, libDir, logger, path, util, validateRequest,
    extend = function (child, parent) {
        for (var key in parent) {
            if (hasProp.call(parent, key)) child[key] = parent[key];
        }

        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype;
        return child;
    },
    hasProp = {}.hasOwnProperty;

path = require('path');

fs = require('fs');

util = require('util');

Stream = require('stream')
    .Stream;

formidable = require('formidable');

EventEmitter = require('events')
    .EventEmitter;

_ = require('lodash-node/modern');

logger = require('bragi');

libDir = path.join(process.cwd(), 'lib');

config = require(path.join(libDir, 'config', 'config'));

combiners = {};

cleanIdentifier = function (identifier) {
    return identifier.replace(/[^0-9A-Za-z_-]/g, "");
};

validateRequest = function (chunkNumber, chunkSize, totalSize, identifier, filename,
    fileSize) {

    // Convert everything into integers 
    var numberOfChunks;
    chunkNumber = +chunkNumber;
    chunkSize = +chunkSize;
    totalSize = +totalSize;
    fileSize = +fileSize;

    // Clean up the identifier 
    identifier = cleanIdentifier(identifier);

    // Check if the request is sane 
    if (chunkNumber === 0 || chunkSize === 0 || totalSize === 0 || identifier.length ===
        0 || filename.length === 0) {
        return "non_flow_request";
    }
    numberOfChunks = Math.max(Math.floor(totalSize / (chunkSize * 1.0)), 1);
    if (chunkNumber > numberOfChunks) {
        return "invalid_flow_request1";
    }

    // Is the file too big? 
    if (this.maxFileSize && totalSize > this.maxFileSize) {
        return "invalid_flow_request2";
    }
    if (typeof fileSize !== "undefined") {

        // The chunk in the POST request isn't the correct size 
        if (chunkNumber < numberOfChunks && fileSize !== chunkSize) {
            return "invalid_flow_request3";
        }

        // The chunks in the POST is the last one, and the fil is not the correct size 
        if (numberOfChunks > 1 && chunkNumber === numberOfChunks && fileSize !==
            ((totalSize % chunkSize) + chunkSize)) {
            return "invalid_flow_request4";
        }

        // The file is only a single chunk, and the data size does not fit 
        if (numberOfChunks === 1 && fileSize !== totalSize) {
            return "invalid_flow_request5";
        }
    }
    return "valid";
};

exports.Flow = Flow = (function () {

    // Will hold references to all of the combiners 
    var self;

    combiners = {};

    self = null;

    function Flow(temporaryFolder) {
        this.temporaryFolder = temporaryFolder != null ? temporaryFolder :
            config.uploadDirectory;
        this.maxFileSize = null;
        this.fileParameterName = 'file';
        self = this;
        try {
            fs.mkdirSync(this.temporaryFolder);
        } catch (_error) {}
    }


    //'found', filename, original_filename, identifier 


    //'not_found', null, null, null 

    Flow.prototype.get = function (req, callback) {
        var chunkFilename, chunkNumber, chunkSize, filename, identifier,
            totalSize;
        chunkNumber = req.param("flowChunkNumber", 0);
        chunkSize = req.param("flowChunkSize", 0);
        totalSize = req.param("flowTotalSize", 0);
        identifier = req.param("flowIdentifier", "");
        filename = req.param("flowFilename", "");
        if (validateRequest.apply(self, [chunkNumber, chunkSize,
                totalSize, identifier, filename
            ]) === "valid") {
            chunkFilename = getChunkFilename(chunkNumber, identifier);
            fs.exists(chunkFilename, function (exists) {
                if (exists) {
                    callback("found", chunkFilename, filename,
                        identifier);
                } else {
                    callback("not_found", null, null, null);
                }
            });
        } else {
            callback("not_found", null, null, null);
        }
    };


    //'partly_done', filename, original_filename, identifier 


    //'done', filename, original_filename, identifier 


    //'invalid_flow_request', null, null, null 


    //'non_flow_request', null, null, null 

    Flow.prototype.post = function (req, res, callback) {
        var form;
        form = new formidable.IncomingForm();
        return form.parse(req, function (err, fields, files) {
            return self._post(err, fields, files, res, callback);
        });
    };

    Flow.prototype._post = function (err, fields, files, res, callback) {
        var chunkNumber, chunkSize, combiner, filename, identifier,
            numberOfChunks, original_filename, response, totalSize;
        response = {
            filename: null,
            response: 500,
            success: false,
            message: null,
            error: null
        };
        if (err) {
            logger.log('error:service:flow:_post',
                'Error parsing chunk\nerror:\n', err);
            response.error = new Error(err, 'Error parsing chunk');
            response.message = 'Error uplading chunking';
            return callback(response);
        }
        chunkNumber = +fields["flowChunkNumber"];
        chunkSize = +fields["flowChunkSize"];
        totalSize = +fields["flowTotalSize"];
        identifier = cleanIdentifier(fields["flowIdentifier"]);
        filename = response.filename = fields["flowFilename"];
        original_filename = fields["flowIdentifier"];
        numberOfChunks = Math.max(Math.floor(totalSize / (chunkSize *
            1.0)), 1);
        if (!files[this.fileParameterName] || !files[this.fileParameterName]
            .size) {
            response.message = 'invalid_flow_request';
            return callback(response);
        }

        // Initiate our combiner object which will assemble the finished file 
        combiner = combiners[identifier] || new Combiner({
            numberOfChunks: numberOfChunks,
            filename: filename,
            identifier: identifier,
            directory: this.temporaryFolder,
            callback: function () {

                // destroy reference as it's no longer needed 
                combiners[identifier] = null;
                return callback.apply(null, arguments);
            }
        });

        // set the combiner in case the previous action only created the object 
        combiners[identifier] = combiner;
        response.message = validateRequest.apply(self, [chunkNumber,
            chunkSize, totalSize, identifier, filename, files[
                this.fileParameterName].size
        ]);
        if (response.message === "valid") {

            // If the current chunk we're adding isn't the last, then continue sending 200's back 
            if (combiner.add(files[this.fileParameterName].path,
                    chunkNumber)) {
                return res.send(200);
            }
        } else {
            return callback(response);
        }
    };

    return Flow;

})();


/**
 * Assembles chunks into a final file
 * @class
 */

exports.Combiner = Combiner = (function (superClass) {
    var chunks, downloadExtension, requiredParams, self;

    extend(Combiner, superClass);

    chunks = null;

    self = null;

    requiredParams = ['numberOfChunks', 'identifier', 'directory'];

    downloadExtension = '.cfdownload';


    /**
     * Accepts options
     *
     * @param  {object} data                     has 3 required properties listed below
     * @options data {Integer}  numberOfChunks   total number of chunks
     * @options data {string}   identifier       file identifier
     * @options data {string}   directory        path of the temp files
     * @options data {Function} callback         optional callback when file is combined
     */

    function Combiner(data, _undefined) {
        var i, len, param;
        if (data == null) {
            data = {};
        }

        // Iterate over the required params and throw error if any are missing 
        for (i = 0, len = requiredParams.length; i < len; i++) {
            param = requiredParams[i];
            if (data[param] === _undefined) {
                logger.log('error:service:flow:constructor', 'Missing', data[
                    param], 'parameter');
                throw new Error('Missing' + data[param] +
                    'parameter from flow#constructor');
            }
        }
        self = this;
        _.each(data, function (val, key) {
            self[key] = val;
        });
        chunks = new Array(data.numberOfChunks);

        // Determine the file extension from the passed in filename 
        this.fileExtension = path.extname(this.filename);
        this.idle = true;
        this.downloadName = this.directory + "/" + this.identifier +
            downloadExtension;
        this.filename = this.directory + "/" + this.identifier + this.fileExtension;
        this.currentIndex = 0;

        // If file exists from previously, remove it 
        fs.exists(this.filename, (function (_this) {
            return function (exists) {
                _this.idle = !exists;
                if (exists) {
                    return fs.unlink(_this.filename,
                        function () {
                            return _this.emit('resume');
                        });
                }
            };
        })(this));

        // Bind the internal callbacks 
        self.on('idle', this.onIdle);
        self.on('written', this.onWritten);
        self.on('resume', this.onResume);
        self.on('error', this.onError);

        // setup callback if one is passed in 
        if (this.callback) {
            self.on('done', this.callback);
        }
    }


    /**
     * File has been uploaded and ready to append
     *
     * @param  {string}  file   path of the temp file
     * @param  {Integer} index  chunk number
     * @return {boolean}        whether the chunk is NOT the last chunk
     */

    Combiner.prototype.add = function (filePath, index) {

        // index is not 0 based, let's make it so 
        var arrIndex;
        arrIndex = index - 1;

        // Only write if we're idle, and this chunk is ready 
        if (this.idle && arrIndex === this.currentIndex) {
            this.write(filePath);
        } else {
            chunks[arrIndex] = filePath;
        }
        return index !== this.numberOfChunks;
    };

    Combiner.prototype.chunkReady = function (index) {
        if (index == null) {
            index = this.currentIndex;
        }
        return !!chunks[index];
    };


    /**
     * Proceeds to write/append the next file chunk to the final file
     *
     * @param  {string} filePath
     */

    Combiner.prototype.write = function (filePath, index) {
        var isEnd, readStream, writeStream;
        if (index == null) {
            index = this.currentIndex;
        }

        // exit early if no file sent 
        if (!filePath) {
            this.emit('resume');
            return false;
        }
        writeStream = this.getStream();
        this.idle = false;
        self = this;
        isEnd = index === this.numberOfChunks;
        readStream = fs.createReadStream(filePath, {
            flags: "r",
            encoding: null,
            fd: null,
            mode: '0666'
        });

        // When done remove the file and emit it's done writing 
        readStream.on('end', function () {
            return self.emit('written', filePath);
        });
        readStream.on('error', function (err) {
            return self.emit('error', err);
        });

        // append the guy 
        return readStream.pipe(writeStream, {
            end: isEnd
        });
    };


    /**
     * Returns a new write stream if one doesn't already exist
     *
     * @param  {string} file  the final file path
     * @return {Stream}       write stream for the final file
     */

    Combiner.prototype.getStream = function () {
        if (this.writeStream) {
            return this.writeStream;
        }
        return this.writeStream = fs.createWriteStream(this.downloadName, {
            flags: 'w',
            encoding: null,
            mode: '0666'
        });
    };


    /**
     * Called after onWritten, and waiting for chunks to finish
     */

    Combiner.prototype.onIdle = function () {
        return this.idle = true;
    };


    /**
     * Called after file has been appended
     * Remove the temp file and continue
     *
     * @callback
     * @param {string} filePath  the path of the file that was just read from
     */

    Combiner.prototype.onWritten = function (filePath) {
        return fs.unlink(filePath, (function (_this) {
            return function () {
                chunks[_this.currentIndex++] = null;
                if (_this.currentIndex === _this.numberOfChunks) {
                    return _this.onDone();
                }
                return _this.emit('resume');
            };
        })(this));
    };


    /**
     * Called when done with one action, and ready to move on
     */

    Combiner.prototype.onResume = function () {
        if (this.chunkReady()) {
            return this.write(chunks[this.currentIndex]);
        } else {
            return this.emit('idle');
        }
    };


    /**
     * If any errors are thrown, clean up and throw error
     *
     * @param  {Error} err
     */

    Combiner.prototype.onError = function (err) {
        var emitErr, error, message;
        message = "error uploading " + this.filename;
        logger.log('error:service:flow', message, '\nerror:\n', err);
        error = new Error(err, message);
        emitErr = function () {
            return self.emit('done', {
                filename: null,
                response: 500,
                success: false,
                message: message,
                error: error
            });
        };

        // Remove temp download file if exists 
        return fs.exists(this.downloadName, function (exists) {
            if (exists) {
                return fs.unlink(self.downloadName, emitErr);
            } else {
                return emitErr();
            }
        });
    };


    /**
     * Called after the last chunk has been appended
     */

    Combiner.prototype.onDone = function () {
        return fs.rename(this.downloadName, this.filename, (function (
            _this) {
            return function () {
                return _this.emit('done', {
                    filename: _this.filename,
                    response: 200,
                    success: true,
                    message: 'successfully uploaded!',
                    error: null
                });
            };
        })(this));
    };

    return Combiner;

})(EventEmitter);
