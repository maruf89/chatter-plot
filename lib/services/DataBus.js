'use strict';
var DataBus,
    extend = function (child, parent) {
        for (var key in parent) {
            if (hasProp.call(parent, key)) child[key] = parent[key];
        }

        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype;
        return child;
    },
    hasProp = {}.hasOwnProperty;

DataBus = (function (superClass) {
    extend(DataBus, superClass);

    function DataBus() {
        this.setMaxListeners(15);
    }

    return DataBus;

})(require('events')
    .EventEmitter);

module.exports = new DataBus();
