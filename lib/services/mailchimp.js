'use strict';

/**
 * TODO: Change service/mailchimp to service/mailchimpAPI, make util/mailchimpHelper into service/mailchimp and create a mailchimpHelper
 */
var Fetch, GroupIDs, MC, MailChimpAPI, Q, SERVICE_NAME, _, _minifyGroupData,
    _retrieveFromGroup, combineGroupWithLanguages, config, esHelper, eventEmitter,
    languageGroupingsKey, listID, lists, logger, mcData, mcGroups, onStartup;

SERVICE_NAME = 'mailchimp';

eventEmitter = require('events')
    .EventEmitter;

Q = require('q');

_ = require('lodash-node/modern');

MailChimpAPI = require('mailchimp-api')
    .Mailchimp;

logger = require('bragi');

Fetch = GLOBAL.Fetcher;

config = Fetch.config('socialServices')[SERVICE_NAME];

mcData = Fetch.config('mailchimp');

listID = config.listID;

esHelper = Fetch.util('elasticsearch', 'languageHelper');

MC = null;


/**
 * Store value of the Mailchimp groups here
 * @type {Object}
 */

exports.groups = mcGroups = {
    primary: {}
};

exports.status = 0;


/**
 * Group ID's correspond to what Mailchimp has set for the specific group
 * @type {Object}
 */

GroupIDs = mcData.groupIDs;


/**
 * The key that's appended to all language groupings with
 * the value of the Elasticsearch sourceID
 * @type {String}
 */

languageGroupingsKey = 'sourceID';


/**
 * Searches through an array of groups for a value matching the passed in key
 *
 * @private
 * @param  {array}   groups        Mailchimp group object
 * @param  {string}  key
 * @param  {string}  value
 * @param  {boolean} toLowerCase   Whether to lowercase the value to be checked
 * @return {object}                If found, the matching object
 */

_retrieveFromGroup = function (groups, key, value, toLowerCase) {
    var returnObj;
    returnObj = null;
    _.each(groups, function (group) {
        var testVal;
        testVal = group[key];
        if (!testVal) {
            return true;
        }
        if (toLowerCase) {
            testVal = testVal.toLowerCase();
        }
        if (testVal === value) {
            returnObj = group;
            return false;
        }
    });
    return returnObj;
};


/**
 * On script ready
 */

onStartup = function () {
    var error;
    try {
        logger.log('init:service:mailchimp', 'init');
        MC = new MailChimpAPI(config.apiKey);
    } catch (_error) {
        error = _error;
        logger.log('error:service:mailchimp', 'Error initiating MailChimpAPI',
            error);
    }

    // Get all of the groups from Mailchimp 
    return lists.interestGroupings()
        .then(combineGroupWithLanguages)
        .then(function (groups) {
            var primary;
            primary = {};
            _.each(groups, function (group) {
                return _.each(GroupIDs, function (id, key) {
                    if (+group.id === +id) {
                        primary[key] = group;
                        return false;
                    }
                });
            });
            mcGroups.primary = primary;
            return exports.ready = 200;
        }, function (err) {
            return exports.ready = 400;
        });
};


/**
 * Accepts a user object with the available fields:
 *
 * @param  {object} user
 * @option user {boolean} student   Whether they're a student
 * @option user {boolean} teacher   same
 * @option user {array}   learning  Array of Elasticsearch `languageID`s
 * @option user {array}   teaching  same
 * @return {object}                 Mailchimp `groupings` object
 */

exports.buildUserGroupings = function (user) {

    // Define the groups 
    var groupings, interests, source;
    source = mcGroups.primary;
    groupings = [];
    interests = null;
    _.each(user, function (value, key) {

        // exit early if value is falsey 
        var names, obj;
        if (!value) {
            return true;
        }
        switch (key) {
            case 'student':
            case 'teacher':
                obj = _retrieveFromGroup(source.interests.groups, 'name',
                    key, true);

                // Create the interests object if not created 
                if (!interests) {
                    interests = {
                        id: source.interests.id,
                        name: source.interests.name,
                        groups: []
                    };
                }

                // Push the name to the grouping 
                if (obj) {
                    return interests.groups.push(obj.name);
                }
                break;
            case 'learning':
            case 'teaching':
                names = [];

                // Retrieve each group object by the passed in languageID array 
                value.forEach(function (languageID) {
                    obj = _retrieveFromGroup(source[key].groups,
                        languageGroupingsKey, languageID);
                    if (obj) {
                        return names.push(obj.name);
                    }
                });
                return groupings.push({
                    id: source[key].id,
                    name: source[key].name,
                    groups: names
                });
        }
    });

    // Since 'student' + 'teacher' interests use the same grouping, we need to add it after 
    if (interests) {
        groupings.push(interests);
    }
    return groupings;
};

exports.lists = lists = {

    /**
     * Returns member info for users
     * 
     * @param  {string} list   Mailchimp list id
     * @param  {array} emails  Array of email structs: { email: 'example@email.com', leid: ..., euid: ... }
     * @return {Promise}       Array of Mailchimp user info
     */
    memberInfo: function (list, emails) {
        var deferred, request;
        if (list == null) {
            list = listID;
        }
        deferred = Q.defer();
        request = {
            id: list,
            emails: emails
        };
        MC.lists.memberInfo(request, deferred.resolve, deferred.reject);
        return deferred.promise;
    },

    /**
     * Same as add user but for updating an existing user
     * http://apidocs.mailchimp.com/api/2.0/lists/update-member.php for parameters
     *
     * @param  {string}   list             Mailchimp list id
     * @param  {String|Object}   emailStruct      Valid email address or object with `euid` or `leid` Ex. { euid: <euid> }
     * @param  {object}   mergeVars        Formatted Mailchimp merge_vars
     * @param  {String=}  emailType        change the email type preference for the member ("html" or "text")
     * @param  {Boolean=} replaceInterests flag to determine whether we replace the interest groups with the updated groups provided
     * @return {Promise}                   {}
     */
    updateMember: function (list, emailStruct, mergeVars, emailType,
        replaceInterests) {
        var deferred, request;
        if (list == null) {
            list = listID;
        }
        if (mergeVars == null) {
            mergeVars = {};
        }
        if (replaceInterests == null) {
            replaceInterests = false;
        }
        deferred = Q.defer();
        if (typeof emailStruct === 'string') {
            emailStruct = {
                email: email
            };
        }
        request = {
            id: list,
            email: emailStruct,
            merge_vars: mergeVars,
            email_type: emailType,
            replace_interests: replaceInterests
        };
        MC.lists.updateMember(request, deferred.resolve, deferred.reject);
        return deferred.promise;
    },

    /**
     * Subscribes the user to the Mailchimp mailing list
     * http://apidocs.mailchimp.com/api/2.0/lists/subscribe.php
     *
     * @param  {string}        list         Mailchimp list id
     * @param  {String|Object} emailStruct  Valid email address or object with `euid` or `leid` Ex. { euid: <euid> }
     * @param  {object}        mergeVars    Formatted Mailchimp merge_vars
     * @return {Promise}                    Response contains { email: {String}, euid: {String}, leid: {string} }
     */
    subscribe: function (list, emailStruct, mergeVars, doubleOptin, updateExisting, replaceInterests) {
        if (list == null) {
            list = listID;
        }

        if (mergeVars == null) {
            mergeVars = {};
        }

        if (doubleOptin == null) {
            doubleOptin = false;
        }

        if (replaceInterests == null) {
            replaceInterests = true;
        }

        logger.log('service:Mailchimp:lists-subscribe:', 'called');

        if (typeof emailStruct === 'string') {
            emailStruct = {
                email: emailStruct
            };
        }

        let deferred = Q.defer(),

            request = {
                id: list,
                email: emailStruct,
                merge_vars: mergeVars,
                double_optin: doubleOptin,
                update_existing: updateExisting,
                replace_interests: replaceInterests
            };

        MC.lists.subscribe(request, deferred.resolve, deferred.reject);

        return deferred.promise;
    },

    /**
     * Unsubscribes user from list
     * http://apidocs.mailchimp.com/api/2.0/lists/unsubscribe.php
     * 
     * @param  {string}         list            Mailchimp list id
     * @param  {String|Objecct} emailStruct     Valid email address or object with `euid` or `leid` Ex. { euid: <euid> }
     * @param  {Boolean=}       deleteMember    flag to completely delete the member from your list instead of just unsubscribing
     * @param  {Boolean=}       sendGoodbye     flag to send the goodbye email to the email address, defaults to true
     * @param  {Boolean=}       sendNotify      flag to send the unsubscribe notification email to the address defined in the list email notification settings, defaults to true
     * @return {Promise}                        See documentation response
     */
    unsubscribe: function (list, emailStruct, deleteMember, sendGoodbye,
        sendNotify) {
        var deferred, request;
        if (list == null) {
            list = listID;
        }
        deferred = Q.defer();
        if (typeof emailStruct === 'string') {
            emailStruct = {
                email: email
            };
        }
        request = {
            id: list,
            email: emailStruct,
            delete_member: deleteMember,
            send_goodbye: sendGoodbye,
            send_notify: sendNotify
        };
        return MC.lists.unsubscribe(request, deferred.resolve, deferred.reject);
    },

    /**
     * Returns all Mailchimp group data
     * http://apidocs.mailchimp.com/api/2.0/lists/interest-groupings.php
     *
     *  Groups Sample Data
     *  ------------------
     *  JSON.parse('[{"id":2581,"name":"I am interested in:","groups":[{"id":9793,"name":"Learning","displayOrder":1},{"id":9797,"name":"Teaching","displayOrder":2}],"displayOrder":0},{"id":2585,"name":"I am learning:","groups":[{"id":9805,"name":"english","displayOrder":4},{"id":9809,"name":"español (spanish)","displayOrder":5},{"id":9813,"name":"普通话 (mandarin)","displayOrder":6},{"id":9817,"name":"français (french)","displayOrder":7},{"id":9821,"name":"हिंदी (hindi)","displayOrder":8},{"id":9825,"name":"العربية (arabic)","displayOrder":9},{"id":9829,"name":"日本の (japanese)","displayOrder":10},{"id":9833,"name":"deutsch (german)","displayOrder":11},{"id":9837,"name":"português (portugese)","displayOrder":12},{"id":9841,"name":"русский (russian)","displayOrder":13},{"id":9845,"name":"basa jawa (javanese)","displayOrder":14},{"id":9849,"name":"한국의 (korean)","displayOrder":15},{"id":9901,"name":"italiano (italian)","displayOrder":28}],"displayOrder":0},{"id":2589,"name":"I speak:","groups":[{"id":9853,"name":"english","displayOrder":16},{"id":9857,"name":"español","displayOrder":17},{"id":9861,"name":"普通话","displayOrder":18},{"id":9865,"name":"français","displayOrder":19},{"id":9869,"name":"हिंदी","displayOrder":20},{"id":9873,"name":"العربية","displayOrder":21},{"id":9877,"name":"日本の","displayOrder":22},{"id":9881,"name":"deutsch","displayOrder":23},{"id":9885,"name":"português","displayOrder":24},{"id":9889,"name":"русский","displayOrder":25},{"id":9893,"name":"basa jawa","displayOrder":26},{"id":9897,"name":"한국의","displayOrder":27},{"id":9905,"name":"italiano","displayOrder":29}],"displayOrder":0}]')
     *
     * @param  {String=} list        The id of the Mailchimp list to use if not primary
     * @param  {boolean} unminified  Set to true to return full verbose data
     * @return {Promise}
     */
    interestGroupings: function (list, unminified) {
        var callback, deferred;
        if (list == null) {
            list = listID;
        }
        deferred = Q.defer();
        callback = !unminified ? _.partialRight(_minifyGroupData, deferred) :
            deferred.resolve;
        MC.lists.interestGroupings({
            id: list
        }, callback, function (err) {
            logger.log(
                'error:service:mailchimp:combineGroupWithLanguages',
                'Error retrieving groups from Mailchimp for list:',
                listID, err);
            return deferred.reject(err);
        });
        return deferred.promise;
    },

    /**
     * Returns all members associated with a Mailchimp list
     * http://apidocs.mailchimp.com/api/2.0/lists/members.php
     *
     * @param  {String=} list       The id of the Mailchimp list to use if not primary
     * @param  {String=} status     the status to get members for - one of(subscribed, unsubscribed, cleaned), defaults to subscribed
     * @param  {Object=} opts       various options for controlling returned data (see docs)
     * @return {object}             array containing # of users and a `data` property containing member data
     */
    members: function (list, status, opts) {
        var deferred, request;
        if (list == null) {
            list = listID;
        }
        deferred = Q.defer();
        request = {
            id: list,
            status: status,
            opts: opts
        };
        MC.lists.members(request, deferred.resolve, deferred.reject);
        return deferred.promise;
    }
};

exports.updateUser = function () {
    console.log('#updateUser is deprecated. Use #lists.updateMember instead.');
    return lists.updateMember.apply(null, arguments);
};

exports.addUser = function () {
    console.log('#addUser is deprecated. Use #lists.subscribe instead.');
    return lists.subscribe.apply(null, arguments);
};


/**
 * Accepts an Array of Mailchimp groups, and attaches Elasticsearch source languageID's
 * to each language in the learning/teaching section
 *
 * @param  {array} groups  Mailchimp groups
 * @return {Promise}       Updated Mailhchimp groups
 */

exports.combineGroupWithLanguages = combineGroupWithLanguages = function (groups) {
    var languages, learning, onError, teaching;
    onError = function (err) {
        logger.log('error:service:mailchimp:combineGroupWithLanguages',
            'Error combining Mailchimp groups with Elasticsearch source ID\'s',
            err);
        throw err;
    };
    learning = teaching = null;
    groups.forEach(function (group) {
        switch (group.id) {
            case GroupIDs.learning:
                return learning = group.groups;
            case GroupIDs.teaching:
                return teaching = group.groups;
        }
    });

    // Get all of the languages from both learning + teaching and only return uniques 
    languages = _.uniq(_.pluck(learning, 'name')
        .concat(_.pluck(teaching, 'name')));
    return esHelper.getIdForLanguages(languages)
        .then(_.partialRight(esHelper.respToLangObj, 'keyObject'), onError)
        .then(function (languageKeys) {
            var appendESKey;
            appendESKey = function (group) {
                return group[languageGroupingsKey] = languageKeys[group.name];
            };
            _.each(teaching, appendESKey);
            _.each(learning, appendESKey);

            // Return all + modified groups 
            return groups;
        }, onError);
};


/**
 * Parses a Mailchimp group object and returns only the essential fields
 *
 * @param  {array}     groups     List of groups
 * @param  {Deferred=} deferred   Optional deferred object
 * @return {array}                Minified list
 */

_minifyGroupData = function (groups, deferred) {
    var minified;
    minified = [];
    _.each(groups, function (group) {
        var minGroup;
        minGroup = {
            id: group.id,
            name: group.name,
            groups: [],
            displayOrder: +group.display_order
        };
        minified.push(minGroup);
        return _.each(group.groups, function (groupVal) {
            return minGroup.groups.push({
                id: groupVal.id,
                name: groupVal.name,
                displayOrder: +groupVal.display_order
            });
        });
    });
    if (deferred) {
        return deferred.resolve(minified);
    } else {
        return minified;
    }
};


// Initiate the module 

onStartup();
