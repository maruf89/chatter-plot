/**
 * @module permissions
 */
'use strict';
var Fetch, GroupsKey, Permissions, Q, SERVICE_NAME, UserGroups, _,
    _frozenPermissions, _frozenUserGroups, _permissionAction, config, dataFerry,
    extendGroupPermissions, extendPermissions, extendPermissionsArr, getPermissions,
    getUserGroups, logger, maria, onInit, rolesQuery, rolesToNumber,
    updatePermissions, updateUserGroups;

SERVICE_NAME = 'permissions';

Q = require('q');

_ = require('lodash-node/modern');

logger = require('bragi');

Fetch = GLOBAL.Fetcher;

config = Fetch.config('config', true, 'js');

maria = Fetch.database('mariadb');

dataFerry = Fetch.util('dataFerry');


/**
 * Contains all of the permissions from MySQL's `user_permissions` table
 * Looks like: {
 *                 'USER_EDIT': '1',
 *                 'USER_DELETE': '2',
 *                  etc...
 *             }
 * @type {Object}
 */

Permissions = {};

_frozenPermissions = null;


/**
 * Contains group roles
 * Looks like: {
 *                 'admin': {
 *                     'roles': ['USER_EDIT', 'USER_DELETE'],
 *                     'permissions': 127,
 *                     'id': 1 // MySQL row ID
 *                 }
 *             }
 * @type {Object}
 */

UserGroups = {};

_frozenUserGroups = null;


/**
 * Contains exact same data as UserGroups except the key is the MySQL row ID
 * instead of strings like 'admin' or 'basic'
 * @type {Object}
 */

GroupsKey = {};


/**
 * Handles different actions for permissions
 *
 * @private
 * @param  {string} action           One of `extend`, `revoke` or `verify`
 * @param  {number} editPermissions  The variable permissions
 * @param  {number} userPermissions  Users permissions which are being acted on
 * @return {number}                  Result from the actions
 */

_permissionAction = function (action, editPermissions, userPermissions) {
    switch (action) {
        case 'extend':
            return editPermissions | userPermissions;
            break;
        case 'revoke':
            return userPermissions - (userPermissions & editPermissions);
            break;
        case 'verify':
            return editPermissions & userPermissions;
    }
};


/**
 * Fetches the permissions from MySQL and passes them to the front-end dataFerry
 */

onInit = function () {
    logger.log('init:service:permissions', 'init');
    dataFerry.await(SERVICE_NAME);
    //dataFerry.await(SERVICE_NAME + "Groups");
    updatePermissions();
    updateUserGroups();
};

rolesQuery = maria.prepare('SELECT role FROM users WHERE user_id = :userID');

/**
 * @description Returns the user permissions for an individual user
 * 
 * @name permissions#getRoles
 * @param  {Number|String} userID     The users ID
 * @return {Promise<number>}           Returns the user's permission level
 */

exports.getRoles = function (userID) {
    var deferred = Q.defer();
    logger.log('req:service:permissions:getRoles', 'Fetching user\'s permissions');

    maria.query(rolesQuery({
        userID: userID
    }))
    .on('result', function (res) {
        res.on('row', function (row) {
            logger.log('res:service:permissions:getRoles', 'Received user\'s permissions');

            return deferred.resolve(parseInt(row.role, 10));
        });
        return res.on('end', function () {
            return deferred.reject(404);
        });
    })
    .on('error', function (err) {
        logger.log('error:service:permissions:getRoles',
            'Error fetching user\'s permissions - userID:', userID,
            'err:', err);
        return deferred.reject(501);
    });

    return deferred.promise;
};


/**
 * @description Checks a single capability against a users permissions
 *
 * @name permissions#can
 * @param  {string} role        Bitmask key/role for the action
 * @param  {number} permission  User's permissions
 * @return {number}             either a truthy or falsey value whether a user can do something
 */

exports.can = function (role, permission) {
    logger.log('service:permissions:can', 'Testing user\'s permission');
    if (typeof role !== 'string' || typeof permission !== 'number') {
        logger.log('error:service:permissions:can',
            'Expecting both `role` to be a String  and `permission` to be a Number. role:',
            typeof role, 'permission:', typeof permission);
        throw new Error();
    }
    if (isNaN(typeof + role)) {
        if (Array.isArray(role)) {
            role = rolesToNumber(role);
        } else {
            role = Permissions[role];
        }
    } else {
        role = +role;
    }
    return _permissionAction('verify', role, permission);
};


/**
 * @description Same as #can excepts accepts an array of roles
 *
 * @name permissions#canArray
 * @param  {array} role         Bitmask keys/roles for the action
 * @param  {number} permission  User's permissions
 * @return {number}             either a truthy or falsey value whether a user can do something
 */

exports.canArray = function (roles, permission) {
    var checkRoles;
    logger.log('service:permissions:canArray', 'Testing user\'s permission');
    if (!Array.isArray(role) || typeof permission !== 'number') {
        logger.log('error:service:permissions:canArray',
            'Expecting both `role` to be a String  and `permission` to be a Number. role:',
            Object.prototype.toString.call(roles), 'permission:', typeof permission
        );
    }
    checkRoles = rolesToNumber(roles);
    return _permissionAction('verify', checkRoles, permission);
};


/**
 * @description Returns an uneditable copy of the Permissions
 *
 * @name permissions#getPermissions
 * @return {object}     Read-only object containing user permissions
 */

exports.getPermissions = getPermissions = function () {
    logger.log('service:permissions:getPermissions', 'Fetching read-only Permissions object');
    return _frozenPermissions;
};


/**
 * @description Returns an uneditable copy of UserGroups
 *
 * @name permissions#getUserGroups
 * @return {object}     Read-only object containing all the user groups
 */

exports.getUserGroups = getUserGroups = function () {
    logger.log('service:permissions:getUserGroups', 'Fetching read-only UserGroups object');
    return _frozenUserGroups;
};


/**
 * @description Returns UserGroups object for MySQL row id of the group
 *
 * @name permissions#indexToUserGroup
 * @param  {string} index - MySQL row id
 * @return {object} - User Group
 */

exports.indexToUserGroup = function (index) {
    return GroupsKey[index];
};


/**
 * @description Converts and combines an array of string roles into an integer
 *
 * @name permissions#rolesToNumber
 * @param  {array} roles  Array of String roles
 * @return {Integer}      Combined Roles
 */

exports.rolesToNumber = rolesToNumber = _.memoize(function (roles) {
    return roles.reduce(function (add, role) {
        return add + parseInt(Permissions[role], 10);
    }, 0);
}, function (roles) {
    return roles.join('');
});


/**
 * @description Extends a users permissions with a preset group's permissions
 * If a String Number is passed in ('1', '2', etc...) then it assumes it's the MySQL index of the group
 * Else it assumes it's a group name ('admin' or 'nonActivated' etc...) (Also correllates to the MySQL row.name)
 *
 * @example
 *     extendGroupPermissions('admin', 15)
 *         // Is the same as
 *     extendGroupPermissions('1', 15)
 *
 * @name permissions#extendGroupPermissions
 * @param  {(string|number)} group            Permissions Group
 * @param  {number} userPermissions  User permissions to extend
 * @return {number}                  The new extended permissions
 */
exports.extendGroupPermissions = extendGroupPermissions = function (group, userPermissions) {
    var permissions;
    logger.log('service:permissions:extendGroupPermissions', 'called');
    if (typeof userPermissions !== 'number' || typeof group !== 'string') {
        logger.log('error:service:permissions:extendPermissions',
            'Expecting first parameter `group` to be a string and second `userPermissions` to be a number. group:',
            typeof group, ' userPermissions:', userPermissions);
    }

    // Check if a string integer is passed in. If not, then it's a group name 
    if (isNaN(parseInt(group, 10))) {
        // group name 
        group = UserGroups[group];
    } else {
        group = GroupsKey[group];
    }
    if (group) {
        permissions = group.permissions;
    } else {
        logger.log('error:service:permissions:extendPermissions',
            'Invalid group passed in. Does not exist. group:', group);

        throw new Error();
    }
    return _permissionAction('extend', permissions, userPermissions);
};


/**
 * @description Extends a users permissions with an Array of String capabities
 * * Differs from the above only that it accepts an array instead of a string
 *
 * @example
 *     extendPermissions(['USER_EDIT', 'SELF_VIEW'], 34)
 *
 * @name permissions#extendPermissionsArr
 * @param  {array}  toExtend         Array of Strings represting capabilities/roles
 * @param  {number} userPermissions  User permissions to extend
 * @return {number}                  The new extended permissions
 */

exports.extendPermissionsArr = extendPermissionsArr = function (toExtend,
    userPermissions) {
    var extension;
    logger.log('service:permissions:extendPermissionsArr', 'called');
    if (!Array.isArray(toExtend) || typeof userPermissions !== 'number') {
        logger.log('error:service:permissions:extendPermissionsArr',
            'Expecting first parameter `toExtend` to be an Array and the second `userPermissions` to be a Number. toExtend:',
            Object.prototype.toString.call(toExtend), 'userPermissions:',
            typeof userPermissions);
        throw new Error();
    }
    extension = rolesToNumber(toExtend);
    return _permissionAction('extend', extension, userPermissions);
};


/**
 * @description Extends a users permissions with another integer
 * * Differs from the above only that it accepts an integer for the extension parameter
 *
 * @example
 *     extendPermissions(['USER_EDIT', 'SELF_VIEW'], 34)
 *
 * @name permissions#extendPermissions
 * @param  {number} extension        Integer represting capabilities/roles
 * @param  {number} userPermissions  User permissions to extend
 * @return {number}                  The new extended permissions
 */

exports.extendPermissions = extendPermissions = function (extension, userPermissions) {
    logger.log('service:permissions:extendPermissions', 'called');
    if (typeof extension !== 'number' || typeof userPermissions !== 'number') {
        logger.log('error:service:permissions:extendPermissions',
            'Expecting both parameters to be Numbers. extension:', typeof extension,
            'userPermissions:', typeof userPermissions);
        throw new Error();
    }
    return _permissionAction('extend', extension, userPermissions);
};


/**
 * @description Pulls in the latest Permission Capabilities + Bits updating the local `Permissions` object
 * @name permissions#updatePermissions
 */

exports.updatePermissions = updatePermissions = function () {
    var query;
    logger.log('req:service:permissions:updatePermissions',
        'Fetching all user permissions');
    query = 'SELECT * FROM user_permissions';
    return maria.query(query)
        .on('result', function (res) {
            return res.on('row', function (row) {
                    return Permissions[row.name] = +row.bit;
                })
                .on('end', function () {
                    _frozenPermissions = Object.freeze(Permissions);
                    logger.log(
                        'res:service:permissions:updatePermissions',
                        'Received all user permissions');
                    return dataFerry.store(SERVICE_NAME, getPermissions());
                });
        })
        .on('error', function (err) {
            return logger.log('error:service:permissions:updatePermissions',
                'Error fetching all user_permissions using query:',
                query);
        });
};


/**
 * @description Updates the local `UserGroups` and `GroupsKey` objects with the latest Permission Groups
 * @name permissions#updateUserGroups
 */

exports.updateUserGroups = updateUserGroups = function () {
    logger.log('req:service:permissions:updateUserGroups',
        'querying MySQL for user groups and their roles');

    var query = 'SELECT * FROM user_groups';
    return maria.query(query)
        .on('result', function (res) {
            return res.on('row', function (row) {
                    var roles;
                    roles = row.roles.split(',');
                    UserGroups[row.name] = {
                        roles: roles,
                        permissions: rolesToNumber(roles),
                        id: row.id
                    };
                    if (row.on_activate) {
                        UserGroups[row.name].onActivate = row.on_activate;
                    }
                    GroupsKey[row.id] = UserGroups[row.name];
                })
                .on('end', function () {
                    _frozenUserGroups = Object.freeze(UserGroups);
                    logger.log('res:service:permissions:updateUserGroups',
                        'Received all user groups & roles');

                    //dataFerry.store(SERVICE_NAME + "Groups", getUserGroups());
                });
        })
        .on('error', function (err) {
            logger.log('error:service:permissions:updateUserGroups',
                'Error fetching all user groups & roles using query:', query,
                '\nerror:\n', err);
        });
};

onInit();
