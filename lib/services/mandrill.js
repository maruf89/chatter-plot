'use strict';

const SERVICE_NAME = 'mandrill';
const Q = require('q');
const _ = require('lodash-node/modern');
const logger = require('bragi');

const Fetch = GLOBAL.Fetcher;

const crypter = Fetch.util('encryptDecrypt');
const config = Fetch.config('socialServices')[SERVICE_NAME];
const apiKey = config[process.env.NODE_ENV + "_apiKey"] || config.apiKey;
const Mandrill = require('node-mandrill')(apiKey);
const mandrillSource = Fetch.config('templates/mandrillKey', true, 'js');

const _promiseCallback = function (err, response) {
    if (err || response.status === 'rejected') {
        return this.reject(err || response);
    }
    return this.resolve(response);
};

/**
 * @description a map of mandril global_vars that correspond to fields that need to
 * be retrieved from the database
 */
const SOURCE_MAP = {
    "FROM_NAME": ['firstName', 'lastName'],
    "FROM_ID": ['userID'],
    "SENDER_PHOTO": ['picture', 'details.gender'],
};

/**
 * @description a map of methods that build out an ES '_source' array of fields to get from the DB
 * @type {{user: Function}}
 */
exports.generateSourceFields = {
    reminder: function (globalVars) {

    },
    sender: function (globalVars) {
        let source = ['email', 'settings.locale'],
            i = 0, len = globalVars.length;

        for (;i < len; i++) {
            let field = globalVars[i];
            if (SOURCE_MAP[field]) {
                source = source.concat(SOURCE_MAP[field]);
            }
        }

        return source;
    },
    recipient: function (globalVars) {
        let source = ['email', 'settings.locale', 'firstName', 'lastName', 'userID'];

        return source;
    }
};

/**
 * @description accepts an ES type and an email key and returns the required ES _source fields
 * @param {string} esType - corresponds to an Elasticsearch type as well as a
 *                          key in exports.generateSourceFields
 * @param {string} emailSource - top level type in mandrillKey.js
 * @param {string} sourceType - second level subtype
 * @returns {array<string>} - returns an array of source fields for ES
 */
exports.generateSourceFieldsFromKey = function (esType, emailSource, sourceType) {
    let emailOpts = mandrillSource[emailSource][sourceType];

    return exports.generateSourceFields[esType](emailOpts.global_vars);
};

/**
 * Sends a template to users. Template must exist
 * https://mandrillapp.com/api/docs/messages.JSON.html#method=send-template
 * 
 * @param  {string} template - Name of the template
 * @param  {(array|null)} content - Array of objects w/ templates containing { "name": <name>, "content": <HTML> }
 * @param  {array<object>|string} to - Array of peeps or single email - if Arr: [{ email: <email>, name: <name> }]
 * @param  {object=} options - Additional merge var data
 * @return {Promise.<object>} email responses
 */
exports.sendTemplate = function (template, content, to, options) {
    logger.log('service:mandrill:sendTemplate', 'called');
    content = content || [];

    let deferred = Q.defer(),
        message;

    if (Array.isArray(to)) {
        message = { to: to };
    } else {
        message = {
            to: [{ email: to }]
        };
    }

    if (options) {
        _.extend(message, options);
    }

    Mandrill('/messages/send-template', {
        template_name: template,
        template_content: content,
        message: message
    }, _promiseCallback.bind(deferred));

    return deferred.promise;
};

// No e-mails in dev
if (process.env.NODE_ENV === 'development') {
    exports.sendTemplate = function () {
        logger.log('service:mandrill:sendTemplate:dev', 'not sending emails in dev…');
        return Q.resolve();
    }
}
