'use strict';

const SERVICE_NAME = 'sparkPost';
const Q = require('q');
const _ = require('lodash-node/modern');
const logger = require('bragi');

const Fetch = GLOBAL.Fetcher;

const crypter = Fetch.util('encryptDecrypt');
const config = Fetch.config('socialServices')[SERVICE_NAME];
const apiKey = config[process.env.NODE_ENV + "_apiKey"] || config.apiKey;
const SparkPost = require('sparkpost');

const SparkPostOptions = {

};

const client = new SparkPost(apiKey, SparkPostOptions);

const mandrillSource = Fetch.config('templates/mandrillKey', true, 'js');

const _promiseCallback = function (err, response) {
    if (err) {
        return this.reject(err || response);
    }
    return this.resolve(response);
};

/**
 * @description a map of mandril global_vars that correspond to fields that need to
 * be retrieved from the database
 */
const SOURCE_MAP = {
    "FROM_NAME": ['firstName', 'lastName'],
    "FROM_ID": ['userID'],
    "SENDER_PHOTO": ['picture', 'details.gender'],
};

/**
 * @description a map of methods that build out an ES '_source' array of fields to get from the DB
 * @type {{user: Function}}
 */
exports.generateSourceFields = {
    reminder: function (globalVars) {

    },
    sender: function (globalVars) {
        let source = ['email', 'settings.locale'],
            i = 0, len = globalVars.length;

        for (;i < len; i++) {
            let field = globalVars[i];
            if (SOURCE_MAP[field]) {
                source = source.concat(SOURCE_MAP[field]);
            }
        }

        return source;
    },
    recipient: function (globalVars) {
        let source = ['email', 'settings.locale', 'firstName', 'lastName', 'userID'];

        return source;
    }
};

/**
 * @description accepts an ES type and an email key and returns the required ES _source fields
 * @param {string} esType - corresponds to an Elasticsearch type as well as a
 *                          key in exports.generateSourceFields
 * @param {string} emailSource - top level type in mandrillKey.js
 * @param {string} sourceType - second level subtype
 * @returns {array<string>} - returns an array of source fields for ES
 */
exports.generateSourceFieldsFromKey = function (esType, emailSource, sourceType) {
    let emailOpts = mandrillSource[emailSource][sourceType];

    return exports.generateSourceFields[esType](emailOpts.global_vars);
};

// Temporary until the data can be formatted correctly from the source
const Mandrill2Sparkpost = function (templateID, to, options) {
    const body = {
        recipients: null,
        content: {
            template_id: templateID,
        },
    };

    // Build the to section
    if (typeof to === 'string') {
        body.recipients = [{
            address: { email: to }
        }]
    } else {
        body.recipients = _.map(to, function (user) {
            return {
                address: {
                    name: user.name,
                    email: user.email,
                }
            };
        });
    }

    if (options.global_merge_vars) {
        let obj = body.substitution_data = {}

        _.each(options.global_merge_vars, function (val) {
            obj[val.name] = val.content;
        });
    }

    if (options.merge_vars) {
        _.each(options.merge_vars, function (mergeVar) {
            _.each(body.recipients, function (user) {
                if (mergeVar.rcpt === user.address.email && mergeVar.vars) {
                    let userData = user.substitution_data = {}
                    _.each(mergeVar.vars, function (val) {
                        userData[val.name] = val.content;
                    });
                }
            });
        });
    }   

    return body; 
};

/**
 * Sends a template to users. Template must exist
 * https://mandrillapp.com/api/docs/messages.JSON.html#method=send-template
 * 
 * @param  {string} template - Name of the template
 * @param  {(array|null)} content - Array of objects w/ templates containing { "name": <name>, "content": <HTML> }
 * @param  {array<object>|string} to - Array of peeps or single email - if Arr: [{ email: <email>, name: <name> }]
 * @param  {object=} options - Additional merge var data
 * @return {Promise.<object>} email responses
 */
exports.sendTemplate = function (template, content, to, options) {
    logger.log('service:sparkPost:sendTemplate', 'called');
    content = content || [];

    let deferred = Q.defer(),
        message;

    if (Array.isArray(to)) {
        message = { to: to };
    } else {
        message = {
            to: [{ email: to }]
        };
    }

    if (options) {
        _.extend(message, options);
    }

    let transmissionBody = Mandrill2Sparkpost(template, to, options);

    client.transmissions.send({
        transmissionBody: transmissionBody
    }, _promiseCallback.bind(deferred));

    return deferred.promise;
};

// No e-mails in dev
// if (process.env.NODE_ENV === 'development') {
//     exports.sendTemplate = function () {
//         logger.log('service:sparkPost:sendTemplate:dev', 'not sending emails in dev…');
//         return Q.resolve();
//     }
// }
